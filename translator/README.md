Foxpro2Java 自动翻译机
===

How To
---
+ 运行 `make lexer` 自动重新根据 `raw/jfoxpro.l` 生成lexer
+ 运行 `make parser` 自动重新根据 `raw/jfoxpro.cup` 生成parser


依赖库
---

+ JLex(http://www.cs.princeton.edu/~appel/modern/java/JLex/)
+ CUP(http://www.cs.princeton.edu/~appel/modern/java/CUP/)
