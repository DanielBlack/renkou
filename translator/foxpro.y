%{
#include "utilities.h"
#include "parse.h"

extern char *curr_filename;

%}

%union {
char *error_msg;
//Symbol symbol;
}

%token MULTI_LINE INT_CONST ID GE LE
%token PUBLIC DIMENSION PROCEDURE CREATE TABLE FREE ADD ALL WITH 
%token STORE TO FOR ENDFOR IF ELSE ENDIF IIF DO CASE ENDCASE WHILE ENDDO
%token SELECT LOCATE COPY ARRAY RELEASE SUM APPEND BLANK REPLACE USE ALTER 
%token FROM ALTERNATE AND OR ERROR STR_CONST FIELD
%token PATH

%%
stmt: if_stmt {printf("if statement\n");}
| assign_stmt {printf("assign statement\n");}
| for_stmt {printf("for statement\n");}
| dowhile_stmt {printf("dowhile statement\n");}
| docase_stmt {printf("docase statement\n");}
| locate_stmt {printf("locate statement\n");}
| select_stmt {printf("select statement\n");}
| copy_stmt {printf("copy statement\n");}
| store_stmt {printf("store statement\n");}
| creat_stmt {printf("create statement\n");}
| call_stmt {printf("call statement\n");}
| replace_stmt {printf("replace statement\n");}
| release_stmt {printf("release statement\n");}
| append_stmt {printf("append statement\n");}
| alternate_stmt {printf("alternate statement\n");}
| use_stmt {printf("use statement\n");}
| functions {printf("functions\n");}
;

stmt_seq: stmt {printf("stmt_seq -> stmt\n");}
| stmt stmt_seq {printf("stmt_seq -> stmt stmt_seq\n");}
;

assign_stmt: ID '=' NUM {printf("assign_stmt -> ID '=' NUM\n");}
;

if_stmt: IF condition stmt_seq ENDIF {printf("if_stmt_noelse\n");}
| IF condition stmt_seq ELSE stmt_seq ENDIF {printf("if_stmt_else\n");}
;

for_stmt: FOR assign_stmt TO NUM stmt_seq ENDFOR 
;

dowhile_stmt: DO WHILE condition stmt_seq ENDDO
;

docase_stmt: DO CASE case_list ENDCASE
;

locate_stmt: LOCATE FOR condition
;

copy_stmt: COPY TO ARRAY ID FIELD ID FOR condition
| COPY TO PATH
;

store_stmt: STORE exp TO id_list
| STORE NUM TO ID
;

creat_stmt: CREATE TABLE ID FREE '(' dfn_list ')'
;

call_stmt:DO ID {printf("call)stmt\n");}
;

select_stmt: SELECT NUM
;

replace_stmt: REPLACE replace_cont FOR condition
| REPLACE replace_cont ALL
;

append_stmt: APPEND BLANK {printf("append_stmt -> APPEND BLANK\n");}
| APPEND FROM PATH 
;

alternate_stmt: ALTER TABLE ID ADD ID type
;

use_stmt: USE ID
| USE PATH
;

functions: COUNT TO ID FOR condition {printf("count function\n");}
| SUM exp TO ID
| SUM exp TO ID FOR condition
;
dfn_list: type
| type dfn_list
;

replace_cont: ID WITH exp
| ID WITH exp ',' replace_cont
;

release_stmt: RELEASE id_list
;

case_list: CASE condition stmt_seq
| CASE condition stmt_seq case_list
;

condition: NUM OP NUM {printf("conditon\n");}
;

id_list:ID
| ID id_list
;


NUM: ID  {printf("NUM->ID\n");}
;

exp: NUM {printf("exp->NUM\n");}
;

OP: GE
| LE
;

type: "CHAR"
;
%%
