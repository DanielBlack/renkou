package danielblack.name.parser;
import danielblack.name.lexer.Lexer;
import danielblack.name.ast.*;
import danielblack.name.struct.*;
import danielblack.name.utils.*;
import java_cup.runtime.*;

parser code {:
    int omerrs = 0;

    public int curr_lineno() {
        return ((Lexer)getScanner()).getLine();
    }

    public void syntax_error(Symbol cur_token) {
        int lineno = curr_lineno();
        System.err.print("line " + lineno + 
		         ": parse error at or near ");
        System.err.println(LexerHelper.dumpToken(cur_token));
	    omerrs++;
        if (omerrs>50) {
           System.err.println("More than 50 errors");
           System.exit(1);
        }
    }

    public void unrecovered_syntax_error(Symbol cur_token) {
    }
:}

terminal AbstractSymbol ID;
terminal IntSymbol INT_CONST;
terminal StringSymbol STR_CONST, PATH;
terminal MULTI_LINE, GE, LE, OTHERWISE, REPLACE, ON, RENAME, ALIAS;
terminal PUBLIC, DIMENSION, PROCEDURE, CREATE, TABLE, FREE, DELIMITER;
terminal STORE, TO, FOR, ENDFOR, IF, ELSE, ENDIF, IIF, DO, CASE, ENDCASE;
terminal SELECT, LOCATE, COPY, ARRAY, RELEASE, SUM, APPEND, BLANK;
terminal FROM, ALTER, AND, OR, ERR, FIELD, FIELDS, INDEX, RELATION;
terminal SAFE, BREAK, UNEQUAL, ALL, DECIMALS, DEFAULT, CLEAR, PATH, TALK;
terminal CLOSE, DATA, OFF, COUNT, SET, RETURN, STRUCTURE;
/*       [           ]           +     /    -      *      =         */
terminal L_SQR_BRAC, R_SQR_BRAC, PLUS, DIV, MINUS, TIMES, EQUAL;
/*       <             >             .    ~      ,      ;     :     */
terminal L_ANGLE_BRAC, R_ANGLE_BRAC, DOT, TILDE, COMMA, SEMI, COLON;
/*       (       )       @   &      {       }       ?         $     */
terminal L_PARE, R_PARE, AT, AMPER, L_BRAC, R_BRAC, QUESTION, DOLLER;


non terminal Program            program;
non terminal Expression         expression;
non terminal ExpressionList     expressions;
non terminal Expression         condition;
non terminal MainProcedure      main_procedure;
non terminal MainProcedure      main_procedure_opt;
non terminal Procedure          procedure;
non terminal ProcedureList      procedures;
non terminal ProcedureList      procedures_opt;
non terminal Statement          statement;
non terminal StatementList      statements;
non terminal StatementList      statements_opt;
non terminal Definition         definition;
non terminal VariableList       variable_list;
non terminal ExpressionList     else_expressions;
non terminal Expression         expression_statement;
non terminal Condition          if_then_statement;
non terminal Condition          if_then_else_statement;
non terminal PostfixExpression         postfix_expression;
non terminal Expression         unary_expression;
non terminal Expression         multiplicative_expression;
non terminal Expression         additive_expression;
non terminal Expression         relational_expression;
non terminal Expression         equality_expression;
non terminal Expression         and_expression;
non terminal Expression         or_expression;
non terminal Expression         iif_expression;
non terminal Expression         assignment_expression;
non terminal Expression         assignment;
non terminal Name               name;

start with program;

program ::= main_procedure_opt:m procedures_opt:p_list
    {: RESULT = new Program(parser.curr_lineno(), m, p_list); :}
;

main_procedure_opt ::= 
    {: RESULT = null; :}
    |   main_procedure:m
    {: RESULT = m; :}
;

main_procedure ::= 
        statements_opt:s_list RETURN 
    {: RESULT = new MainProcedure(parser.curr_lineno(), s_list); :}
;

procedures_opt ::=
    {: RESULT = new ProcedureList(parser.curr_lineno()); :}
    | procedures:p_list
    {: RESULT = p_list; :}
;

procedures ::= 
        procedure:p
    {: 
        ProcedureList procList = new ProcedureList(parser.curr_lineno()); 
        procList.add(p);
        RESULT = procList;
    :}
    |   procedures:p_list procedure:p
    {: RESULT = p_list.add(p); :}
;

procedure ::= PROCEDURE ID:name statements_opt:s_list RETURN
    {: RESULT = new Procedure(parser.curr_lineno(), name.getString(), s_list); :}
;

statements_opt ::=
    {: RESULT = new StatementList(parser.curr_lineno()); :}
    |   statements:s_list
    {: RESULT = s_list; :}
;

statements ::=
        statement:s
    {: 
        StatementList sList = new StatementList(parser.curr_lineno());
        sList.add(s);
        RESULT = sList;
    :}
    |   statements:s_list statement:s
    {: RESULT = s_list.add(s); :}
;

statement ::=
        if_then_statement:s
    {: RESULT = s; :}
    |   if_then_else_statement:s
    {: RESULT = s; :}
    |   expression_statement:s
    {: RESULT = s; :}
;

expression_statement ::=
        assignment:a
    {: RESULT = a; :}
;

if_then_statement ::= 
        IF expression:e statements_opt:s_list ENDIF
    {: RESULT = new Condition(parser.curr_lineno(), e, s_list, null); :}
;

if_then_else_statement ::= 
        IF expression:e statements_opt:then_s_list ELSE statements_opt:else_s_list ENDIF
    {: RESULT = new Condition(parser.curr_lineno(), e, then_s_list, else_s_list); :}
;

unary_expression ::=
        postfix_expression:e
    {: RESULT = e; :}
    |   MINUS unary_expression:e
    {: RESULT = new UnaryExpression(parser.curr_lineno(), e, sym.MINUS); :}
    |   PLUS unary_expression:e
    {: RESULT = new UnaryExpression(parser.curr_lineno(), e, sym.PLUS); :}
;

multiplicative_expression ::=
        unary_expression:e
    {: RESULT = e; :}
    |   multiplicative_expression:e1 TIMES unary_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.TIMES, e2); :}
    |   multiplicative_expression:e1 DIV unary_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.DIV, e2); :}
;

additive_expression ::=
        multiplicative_expression:e
    {: RESULT = e; :}
    |   additive_expression:e1 PLUS multiplicative_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.PLUS, e2); :}
    |   additive_expression:e1 MINUS multiplicative_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.MINUS, e2); :}
;

relational_expression ::=
        additive_expression:e
    {: RESULT = e; :}
    |   relational_expression:e1 L_ANGLE_BRAC additive_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.L_ANGLE_BRAC, e2); :}
    |   relational_expression:e1 R_ANGLE_BRAC additive_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.R_ANGLE_BRAC, e2); :}
    |   relational_expression:e1 LE additive_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.LE, e2); :}
    |   relational_expression:e1 GE additive_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.GE, e2); :}
;

equality_expression ::=
        relational_expression:e
    {: RESULT = e; :}
    |   equality_expression:e1 EQUAL relational_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.EQUAL, e2); :}
    |   equality_expression:e1 UNEQUAL relational_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.UNEQUAL, e2); :}
;
        
and_expression ::=
        equality_expression:e
    {: RESULT = e; :}
    |   and_expression:e1 AND equality_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.AND, e2); :}
;

or_expression ::=
        and_expression:e
    {: RESULT = e; :}
    |   or_expression:e1 OR and_expression:e2
    {: RESULT = new BinaryExpression(parser.curr_lineno(), e1, sym.OR, e2); :}
;

iif_expression ::=
        or_expression:e
    {: RESULT = e; :}
    |   IIF L_PARE expression:e1 COMMA expression:e2 COMMA iif_expression:e3 R_PARE
    {: RESULT = new ConditionExpression(parser.curr_lineno(), e1, e2, e3); :}
;

assignment_expression ::=
        iif_expression:iif_e
    {: RESULT = iif_e; :}
    |   assignment:a
    {: RESULT = a; :}
;

assignment ::=
        postfix_expression:postfix_e EQUAL expression:e
    {: RESULT = new Assignment(parser.curr_lineno(), postfix_e, e); :}
;

/** 这里开始就都TODO了!! **/

/** TODO 增加数组 **/
name ::= 
        ID:id
    {: RESULT = new Name(parser.curr_lineno(), id); :}
    |   name:n DOT ID:id
    {: RESULT = new Name(parser.curr_lineno(), n, id); :}
    |   AND ID:id /** 引用 **/
;

postfix_expression ::=
        name:n
    {: RESULT = new PostfixExpression(parser.curr_lineno(), n); :}
;

non terminal const_expression;
expression ::=
        assignment_expression:a_e
    {: RESULT = a_e; :}
    |   const_expression
    |   L_PARE expression R_PARE
;

const_expression ::=
;
    
        
non terminal variables;
non terminal variable;
non terminal array_variables;

definition ::=
        PUBLIC variables
    |   variables
    |   PUBLIC assignment
    |   assignment
    |   DIMENSION array_variables;
;

variables ::=
        variables:v_list COMMA variable:v
    |   Variable:v
;

array_variables ::=
        array_variables:a_list COMMA array_variable:a
    |   array_variable:a

non terminal arguments;
non terminal argument;
array_variable ::=
        ID:id L_PARE arguments R_PARE
;

/** 变量类型检测就交给运行时了 **/
arguments ::=
        arguments:a_list COMMA argument:a
    |   argument:a
;

argument ::=
        name:n
    |   INT_CONST
    |   STR_CONST
;


non terminal use;
non terminal alias_opt;
use ::= 
        USE PATH:path alias_opt
    |   USE
;

alias_opt ::=
    |   ALIAS ID:name
;
        
non terminal copy_to;
non terminal fields_opt;
non terminal fields;
non terminal Boolean delimiter_opt, free_opt;
non terminal Expression expression_opt;
non terminal Expression for_expression_opt;
copy_to ::=
        COPY fields_opt TO ARRAY expression FIELDS expression delimiter_opt for_expression_opt
    |   COPY STRUCTURE TO postfix_expression
;

expression_opt ::=
    {: RESULT = null; :}
    |   expression:e
    {: RESULT = e; :}
;

for_expression_opt ::=
    {: RESULT = null; :}
    |   FOR expression:e
    {: RESULT = e; :}
;

delimiter_opt ::=
    {: RESULT = false; :}
    | DELIMITER 
    {: RESULT = true; :}
;

fields_opt ::=
    {: RESULT = null; :}
    | fields:f
    {: RESULT = fields; :}
;

fields ::=
    FIELDS fields_expressions:f_e
;

fields_expressions ::=
        fields_expressions:f_e COMMA expression:e
    |   expression:e
;

non terminal store_to_statement;

store_to_statement ::=
        STORE INT_CONST TO names
;

names ::=
        names:ns COMMA name:n
    |   name:n
;

non terminal do_procedure_statement;
do_procedure_statement ::=
        DO ID:name
;

non terminal for_statement;

for_statement ::= 
        FOR name:n EQUAL expression:e1 TO expression:e2 statements_opt:s_list ENDFOR
;

non terminal locate_statement;

locate_statement ::=
        LOCATE FOR expression:e
;

non terminal sum_to_statement;

sum_to_statement ::=
        SUM expressions:e_list TO names:n_list for_expression_opt
;

non terminal case_statement;
non terminal cases;
non terminal case;
non terminal otherwise_opt;

case_statement ::=
        DO CASE cases otherwise_opt ENDCASE
;

cases ::=
        cases:cs case
    |   case
;

case ::=
        CASE expression statements_opt
;

otherwise_opt ::=
    {: RESULT = null; :}
    |   OTHERWISE statements_opt
;

non terminal release_statement;
non terminal ids;

release_statement ::=
    RELE ids:id_list
;

ids ::=
        ids:id_list COMMA ID:id
    |   ID:id
;

non terminal select_statement;

select_statement ::=
        SELECT expression:e
;

non terminal replace_statement;

replace_statement ::=
        REPLACE replace_body:rb ALL
    |   REPLACE replace_body:rb for_expression_opt:e
;

replace_body ::= 
        replace_body:rb COMMA replace_with:rw
    |   replace_with:rw
;

replace_with ::=
        name:n WITH expression:e
;

non terminal set_default_to_statement;

set_default_to_statement ::=
        SET DEFAULT TO PATH:path
;

non terminal create_table_statement;

create_table_statement ::=
        CREATE TABLE ID:name free_opt:f L_PARE create_table_arguments:a_list R_PARE
;

free_opt ::=
    {: RESULT = false; :}
    |   FREE
    {: RESULT = true; :}
;

create_table_arguments ::=
        create_table_arguments:a_list COMMA create_table_argument:a
;

non terminal column_type;

create_table_argument ::=
        ID:id column_type
;

column_type ::=
        ID:id L_PARE INT_CONST:i1 R_PARE
    |   ID:id L_PARE INT_CONST:i1 COMMA INT_CONST:i2 R_PARE
;

non terminal append_statement;

append_statement ::=
        APPEND BLANK
    |   APPEND FROM name fields_opt for_expression_opt
;

non terminal alter_statement;

alter_statement ::=
        ALTER TABLE ID:name ADD postfix_expression:e column_type
    |   ALTER TABLE ID:name RENAME postfix_expression:e TO postfix_expression
    |   ALTER TABLE ID:name ALTER postfix_expression:e column_type
;

non terminal index_on_statement;

index_on_statement ::=
        INDEX ON expression TO ID:id
;

non terminal set_relation_to_statement;

set_relation_to_statement ::=
        SET RELATION TO relations:r_list
;

relations ::= 
        relations:r_list COMMA relation:r
    |   relation:r
;

relation ::=
        expression INTO INT_CONST:i
    |   expression INTO ID:id
;

non terminal count_to_statement;

count_to_statement ::= 
        COUNT TO ID:id for_expression_opt:e_list
;
