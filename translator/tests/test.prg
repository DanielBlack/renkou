* 条件语句测试
IF A < B .AND. B > C    && 测试行内注释和嵌套表达式
    A = B + C           && 测试赋值
ELSE
* 嵌套测试
    C = -B * C + D       && 测试优先级和一元表达式
    C = -B + C * D       && 测试优先级和一元表达式
    IF C <> D           
        A = IIF(A > B, C, D) && 测试IIF语句
    ENDIF
ENDIF
RETURN 

* 测试PROCEDURE
PROCEDURE TEST
RETURN
