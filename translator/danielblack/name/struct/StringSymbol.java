package danielblack.name.struct;

/**
 * 字符串常量Symbol
 * @see AbstractSymbol
 * @author danielblack
 *
 */
public class StringSymbol extends AbstractSymbol{

	/**
	 * @see AbstractSymbol
	 */
	public StringSymbol(String str, int index) {
		super(str, index);
	}
	
	/**
	 * @see AbstractSymbol
	 */
	public String toString(){
		return "<STRING, " + str + ">";
	}
	

}
