package danielblack.name.struct;

import java.util.ArrayList;

/**
 * 抽象表。
 * 因为在一段程序中，肯定会有大量的变量名、字符串、整型变量的重复，
 * 所以需要对每种类型的字符串分别维护一张表，这样就可以大大减少重复字符串的量。
 * 注意idTable, intTable, stringTable三张表是全局的，以便于在Lexer, Parser中
 * 可以直接使用。
 * @author danielblack
 *
 */
public abstract class AbstractTable {
	/** 全局ID存储表 @see IdTable **/
	public static IdTable idTable = new IdTable();
	/** 全局整型常量存储表 @see IntTable **/
	public static IntTable intTable = new IntTable();
	/** 全局字符串常量存储表 @see StringTable **/
	public static StringTable stringTable = new StringTable();
	
	/** 
	 * 创建一张空表。
	 */
	protected AbstractTable(){
		tbl = new ArrayList<AbstractSymbol>();
	}
	/** 存储Symbol **/
	protected ArrayList<AbstractSymbol> tbl;
	
	/**
	 * 创建一个新的Symbol。
	 * 此方法必须被继承，用于保证在不同类型的表中构造类型正确的Symbol。
	 * 如IdTable必须实现一个构造一个IdSymbol的getNewSymbol方法。
	 * @param s Symbol的字面值
	 * @param index Symbol在表中的索引位置
	 * @return 新的Symbol
	 */
	protected abstract AbstractSymbol getNewSymbol(String s, 
			int index);
	
	
	/**
	 * 在表中加入一个新的字符串。
	 * 如果字符串已经在表中找到，直接返回，否则新建。
	 * @param str 新的字符串
	 * @return 新建或是已在表中找到的Symbol
	 */
	public AbstractSymbol addString(String str){
		for(AbstractSymbol sym : tbl){
			if(sym.getString().equals(str))
				return sym;
		}
		AbstractSymbol sym = getNewSymbol(str, tbl.size());
		tbl.add(sym);
		return sym;
	}
}
