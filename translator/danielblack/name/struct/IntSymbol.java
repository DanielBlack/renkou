package danielblack.name.struct;

public class IntSymbol extends AbstractSymbol{

	/**
	 * @see AbstractSymbol
	 */
	public IntSymbol(String str, int index) {
		super(str, index);
	}
	
	/**
	 * 获取整型的值。
	 * @return 整型值
	 */
	public int getInt(){
		return Integer.parseInt(str);
	}
	
	/**
	 * @see AbstractSymbol
	 */
	@Override
	public String toString(){
		return "<INT, " + str + ">";
	}
}
