package danielblack.name.struct;

import java.io.PrintStream;

/**
 * Symbol基类，无论是ID、String、Int，本质上都被解析成字符串，存储在Symbol中，
 * Symbol均被存储在记录表中。
 * @author danielblack
 *
 */
public class AbstractSymbol {
	protected String str;
	protected int index;
	
	/**
	 * 构造一个新的Symbol
	 * @param str Symbol的字面值
	 * @param index Symbol在记录表中的位置
	 */
	public AbstractSymbol(String str, int index){
		this.str = str;
		this.index = index;
	}
	
	/**
	 * 获取Symbol的字面值。
	 * @return Symbol的字面值
	 */
	public String getString(){
		return str;
	}
	
	/**
	 * 获取Symbol在表中的索引号。
	 * @return Symbol在表中的索引号
	 */
	public int getIndex(){
		return index;
	}
	
	/**
	 * 拷贝一个Symbol，为了和所有AST下的类兼容。
	 * @return 拷贝得到的新Symbol
	 */
	public AbstractSymbol copy(){
		return new AbstractSymbol(new String(str), index);
	}
	
	/**
	 * 用于调试查看Symbol的信息，格式为"<SYMBOL, value>"。
	 */
	public String toString(){
		return "<SYMBOL, " + str + ">";
	}
	
	/**
	 * 打印信息，为了和AST下的所有TreeNode保持兼容。
	 * @param out 输出流
	 */
	public void dump(PrintStream out){
		out.println(this);
	}
}
