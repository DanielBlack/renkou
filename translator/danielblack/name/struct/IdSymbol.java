package danielblack.name.struct;

public class IdSymbol extends AbstractSymbol{

	/**
	 * @see AbstractSymbol
	 */
	public IdSymbol(String str, int index) {
		super(str, index);
	}
	
	@Override
	public String toString(){
		return "<ID, " + str + ">";
	}

}
