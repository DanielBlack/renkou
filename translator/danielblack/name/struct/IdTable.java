package danielblack.name.struct;

public class IdTable extends AbstractTable {

	/**
	 * @see AbstractTable
	 */
	@Override
	protected AbstractSymbol getNewSymbol(String s, int index) {
		return new IdSymbol(s, index);
	}
	
	public IdSymbol addString(String str){
		return (IdSymbol)super.addString(str);
	}

}
