package danielblack.name.struct;

/**
 * 整型常量表
 * @see AbstractTable
 * @author danielblack
 *
 */
public class IntTable extends AbstractTable{

	/**
	 * @see AbstractTable
	 */
	@Override
	protected AbstractSymbol getNewSymbol(String s, int index) {
		return new IntSymbol(s, index);
	}
	
	/**
	 * @see AbstractTable
	 */
	public IntSymbol addString(String str){
		return (IntSymbol)super.addString(str);
	}
}
