package danielblack.name.struct;

/**
 * 字符串常量表
 * @author danielblack
 *
 */
public class StringTable extends AbstractTable{

	/**
	 * @see AbstractTable
	 */
	@Override
	public AbstractSymbol getNewSymbol(String s, int index) {
		return new StringSymbol(s, index);
	}
	
	/**
	 * @see AbstractTable
	 */
	public StringSymbol addString(String str){
		return (StringSymbol)super.addString(str);
	}

}
