package danielblack.name.test;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import java_cup.runtime.*;

import danielblack.name.lexer.Lexer;
import danielblack.name.utils.LexerHelper;
public class TestLexer{
	public static void main(String[] args) throws IOException{
		File file = new File(args[0]);
		Reader reader = null;
		try {
			/**
			 * 注意，此处的编码一定要对呀！！！
			 */
			reader = new InputStreamReader(new FileInputStream(file), "utf-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Lexer lexer = new Lexer(reader);
		
		try{
			while(true){
				Symbol token = lexer.next_token();
				System.out.print(lexer.getLine() + " " + 
					LexerHelper.dumpToken(token) + " " +
					lexer.getText() + "\n");
			}
		}catch(Exception e){
			e.printStackTrace();
			reader.close();
		}
		
	}
};