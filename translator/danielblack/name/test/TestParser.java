package danielblack.name.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import java_cup.runtime.Symbol;
import danielblack.name.ast.Program;
import danielblack.name.lexer.Lexer;
import danielblack.name.parser.parser;

public class TestParser {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Lexer scanner = null;
		try {
			scanner = new Lexer(new java.io.FileReader(args[0]));
		} catch (java.io.FileNotFoundException e) {
			System.err.println("File not found: \"" + args[0] + "\"");
			System.exit(1);
		} catch (java.io.IOException e) {
			System.err.println("Error opening file \"" + args[0] + "\"");
			System.exit(1);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Usage: java TestParser <inputfile>");
			System.exit(1);
		}

		PrintStream writer = null;
		// try {
		parser p = new parser(scanner);
		Object result;
		try {
			Symbol s = p.parse();
			result = s.value;
			writer = new PrintStream(new FileOutputStream(new File("tree.out")));
			((Program) result).dump(writer, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.close();
		/*
		 * }catch(Exception e){
		 * System.err.println("An I/O error ocurred while parsing: \n" + e);
		 * System.exit(1); }
		 */
	}

}
