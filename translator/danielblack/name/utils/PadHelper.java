package danielblack.name.utils;

import java.io.PrintStream;

/**
 * 打印辅助类
 * @author danielblack
 *
 */
public class PadHelper {
	/**
	 * 输出指定数目的填充空格
	 * @param num
	 */
	public static void padBlank(PrintStream out, int num){
		for(int i = 0; i != num; ++i){
			out.print(" ");
		}
	}
}
