package danielblack.name.utils;

import java_cup.runtime.Symbol;
import danielblack.name.parser.sym;

/**
 * 用于测试打印Symbol的对应名称
 * @author danielblack
 *
 */
public class LexerHelper {
	/**
	 * Wrapper方法。
	 * @param symbol symbol在parser.sym中的整型值
	 * @return 对应的Token名称
	 */
	public static String dumpToken(int symbol){
		return dumpToken(new Symbol(symbol));
	}
	
	/**
	 * 返回Token对应的字符串。
	 * @see Symbol
	 * @param symbol 需要打印的Symbol对象
	 * @return 对应字符串
	 */
	public static String dumpToken(Symbol symbol){
		switch(symbol.sym){
		case sym.ALL:
			return "ALL";
		case sym.ALTERNATE:
			return "ALTERNATE";
		case sym.AND:
			return "AND";
		case sym.APPEND:
			return "APPEND";
		case sym.ARRAY:
			return "ARRAY";
		case sym.BLANK:
			return "BLANK";
		case sym.BREAK:
			return "BREAK";
		case sym.CASE:
			return "CASE";
		case sym.CLEAR:
			return "CLEAR";
		case sym.CLOSE:
			return "CLOSE";
		case sym.COPY:
			return "COPY";
		case sym.COUNT:
			return "COUNT";
		case sym.CREATE:
			return "CREATE";
		case sym.DATA:
			return "DATA";
		case sym.DECIMALS:
			return "DECIMALS";
		case sym.DEFAULT:
			return "DEFAULT";
		case sym.DIMENSION:
			return "DIMENSION";
		case sym.DO:
			return "DO";
		case sym.ELSE:
			return "ELSE";
		case sym.ENDCASE:
			return "ENDCASE";
		case sym.ENDFOR:
			return "ENDFOR";
		case sym.ENDIF:
			return "ENDIF";
		case sym.EOF:
			return "EOF";
		case sym.ERR:
			return "ERR";
		case sym.FIELD:
			return "FIELD";
		case sym.FOR:
			return "FOR";
		case sym.FREE:
			return "FREE";
		case sym.FROM:
			return "FROM";
		case sym.GE:
			return "GE";
		case sym.ID:
			return "ID";
		case sym.IF:
			return "IF";
		case sym.IIF:
			return "IIF";
		case sym.INT_CONST:
			return "INT_CONST";
		case sym.LE:
			return "LE";
		case sym.LOCATE:
			return "LOCATE";
		case sym.MULTI_LINE:
			return "MULTI_LINE";
		case sym.OFF:
			return "OFF";
		case sym.OR:
			return "OR";
		case sym.PATH:
			return "PATH";
		case sym.PROCEDURE:
			return "PROCEDURE";
		case sym.PUBLIC:
			return "PUBLIC";
		case sym.RELEASE:
			return "RELEASE";
		case sym.SAFE:
			return "SAFE";
		case sym.SELECT:
			return "SELECT";
		case sym.SET:
			return "SET";
		case sym.STORE:
			return "STORE";
		case sym.STR_CONST:
			return "STR_CONST";
		case sym.SUM:
			return "SUM";
		case sym.TABLE:
			return "TABLE";
		case sym.TALK:
			return "TALK";
		case sym.TO:
			return "TO";
		case sym.UNEQUAL:
			return "UNEQUAL";
		case sym.RETURN:
			return "RETURN";
		case sym.L_SQR_BRAC: 
			return("'['"); 
		case sym.R_SQR_BRAC: 
			return("']'"); 
		case sym.PLUS: 
			return("'+'"); 
		case sym.DIV: 
			return("'/'"); 
		case sym.MINUS: 
			return("'-'"); 
		case sym.TIMES: 
			return("'*'"); 
		case sym.EQUAL: 
			return("'='"); 
		case sym.L_ANGLE_BRAC: 
			return("'<'"); 
		case sym.R_ANGLE_BRAC:
			return("'>'"); 
		case sym.DOT: 
			return("'.'"); 
		case sym.TILDE:
			return("'~'"); 
		case sym.COMMA: 
			return("','"); 
		case sym.SEMI: 
			return("';'"); 
		case sym.COLON: 
			return("':'"); 
		case sym.L_PARE: 
			return("'('"); 
		case sym.R_PARE: 
			return("')'"); 
		case sym.AT: 
			return("'@'"); 
		case sym.AMPER: 
			return("'&'"); 
		case sym.L_BRAC: 
			return("'{'"); 
		case sym.R_BRAC: 
			return("'}'"); 
		default:
			return "<INVALID>";
		}
	}

}
