package danielblack.name.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 将汉字转化成拼音与音标。
 * 如"你好123"能够被转换为"ni3hao3123"。
 * @author danielblack
 */
public class PinyinConverter {
	
	/**
	 * @param origin 输入的汉字字符串
	 * @return 转换后的字符串
	 */
	public static String convert(String origin) {
		char[] input = origin.toCharArray();
		String pinyinName = new String();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
		for (int i = 0; i < input.length; i++) {
			if (input[i] > 128) {
				try {
					pinyinName += PinyinHelper.toHanyuPinyinStringArray(
							input[i], defaultFormat)[0];
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}
			}else{
				pinyinName += input[i];
			}
		}
		return pinyinName;
	}

	public static void main(String[] args) {
		System.out.println(PinyinConverter.convert("豕分蛇断纠纷拉斯123"));
	}

}
