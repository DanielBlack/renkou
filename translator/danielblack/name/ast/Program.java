package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 主程序入口
 * 主程序主要包含主过程、其他过程列表、函数列表
 * 每一项都有可能为空。
 * @author danielblack
 *
 */
public class Program extends TreeNode{
	private MainProcedure mainProc;
	private ProcedureList procList;
	private FunctionList funcList;

	/**
	 * @see MainProcedure
	 * @see ProcedureList
	 * @see FunctionList
	 */
	public Program(int lineNumber, MainProcedure mainProc, 
			ProcedureList procList, FunctionList funcList) {
		super(lineNumber);
		this.mainProc = mainProc;
		this.procList = procList;
		this.funcList = funcList;
	}

	/**
	 * @see MainProcedure
	 * @see ProcedureList
	 */
	public Program(int lineNumber, MainProcedure mainProc, 
			ProcedureList procList) {
		super(lineNumber);
		this.mainProc = mainProc;
		this.procList = procList;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new Program(lineNumber, (MainProcedure)copyHelper(mainProc),
				(ProcedureList)copyHelper(procList), (FunctionList)copyHelper(funcList));
	}
	
	/**
	 * @see TreeNode
	 */
	public void dump(PrintStream out, int pad){
		PadHelper.padBlank(out, pad);
		out.println("+ Program");
		dumpHelper(out, pad+4, mainProc);
		dumpHelper(out, pad+4, procList);
		dumpHelper(out, pad+4, funcList);
	}
}
