package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.struct.AbstractSymbol;
import danielblack.name.utils.PadHelper;

/**
 * 属性或变量访问名
 * 		Name.id 
 * 		或
 * 		id
 * @see TreeNode
 * @author danielblack
 *
 */
public class Name extends TreeNode{
	AbstractSymbol id;
	Name name;

	/**
	 * 不加限定的变量名
	 * @see TreeNode
	 * @param id the identifier
	 */
	public Name(int lineNumber, AbstractSymbol id) {
		super(lineNumber);
		this.id = id;
	}
	
	/**
	 * 限定的属性访问，如A.b
	 * @param name 限定的变量名
	 * @param id 访问的属性名
	 * @see TreeNode
	 * @see AbstractSymbol
	 */
	public Name(int lineNumber, Name name, AbstractSymbol id) {
		super(lineNumber);
		this.name = name;
		this.id = id;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new Name(lineNumber, (Name)copyHelper(name), 
				id.copy());
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Name");
		if(name != null){
			name.dump(out, pad);
		}
		PadHelper.padBlank(out, pad);
		out.println(id);
	}

}
