package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.LexerHelper;
import danielblack.name.utils.PadHelper;

/**
 * 二元表达式
 * 		expression symbol expression
 * @see Expression
 * @see TreeNode
 * @author danielblack
 *
 */
public class BinaryExpression extends Expression{
	int symbol;
	Expression e1;
	Expression e2;
	public BinaryExpression(int lineNumber, Expression e1, 
			int symbol, Expression e2) {
		super(lineNumber);
		this.e1 = e1;
		this.e2 = e2;
		this.symbol = symbol;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new BinaryExpression(lineNumber, (Expression)copyHelper(e1),
				symbol, (Expression)copyHelper(e2));
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ BinaryExpression");
		dumpHelper(out, pad+4, e1);
		PadHelper.padBlank(out, pad+4);
		out.println(LexerHelper.dumpToken(symbol));
		dumpHelper(out, pad+4, e2);
	}

}
