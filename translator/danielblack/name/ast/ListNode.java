package danielblack.name.ast;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * 列表AST节点抽象类
 * @see TreeNode
 * @author danielblack
 *
 */
public abstract class ListNode extends TreeNode{
	protected ArrayList<TreeNode> elements;
	
	/**
	 * 构造一个新的列表节点，并且加入已有的节点元素。
	 * @param lineNumber 所处文件行号
	 * @param elements 已有的AST节点列表
	 */
	protected ListNode(int lineNumber, ArrayList<TreeNode> elements){
		super(lineNumber);
		this.elements = elements;
	}
	
	/**
	 * 构造一个空的列表节点。
	 * @param lineNumber 所处文件行号
	 */
	protected ListNode(int lineNumber){
		super(lineNumber);
		this.elements = new ArrayList<TreeNode>();
	}
	
	/**
	 * 添加一个节点，并返回这个列表节点。
	 * 返回当前的列表节点纯粹为了语法上的简便。
	 * @param node 新添加的节点
	 * @return 当前的列表节点
	 */
	public ListNode add(TreeNode node){
		elements.add(node);
		return this;
	}
	
	/**
	 * 获取指定索引位置的节点。
	 * @param index 索引位置
	 * @return 所检索的节点
	 */
	public TreeNode get(int index){
		return elements.get(index);
	}
	
	/**
	 * Deep copy现有的节点元素。
	 * @return 复制得到的新节点数组
	 */
	protected ArrayList<TreeNode> copyElements(){
		ArrayList<TreeNode> cp = new ArrayList<TreeNode>();
		for(TreeNode e : elements){
			cp.add(e.copy());
		}
		return cp;
	}
	
	/**
	 * 使用子类的构造器构造一个新的拷贝节点。
	 * 因为所有的继承自ListNode的子类都是一样形式的构造器，所以这些错误暂时直接忽略了。
	 * @return 复制得到的新节点。
	 */
	public TreeNode copy(){
		try {
			return getClass().getConstructor(int.class, ArrayList.class).newInstance(
					lineNumber, copyElements());
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 打印输出每个节点信息。
	 * @param out 输出流
	 */
	public void dump(PrintStream out, int pad) {
		for(TreeNode e: elements){
			dumpHelper(out, pad, e);
		}
	}
	
	/**
	 * 获取列表节点中的节点数目。
	 * @return 节点数目
	 */
	public int len(){
		return elements.size();
	}
}
