package danielblack.name.ast;

import java.util.ArrayList;

/**
 * 语句列表类
 * @see ListNode
 * @author danielblack
 *
 */
public class StatementList extends ListNode{

	/**
	 * @see ListNode
	 */
	public StatementList(int lineNumber, ArrayList<TreeNode> elements) {
		super(lineNumber, elements);
	}
	
	/**
	 * @see ListNode
	 */
	public StatementList(int lineNumber){
		super(lineNumber);
	}

	/**
	 * @see ListNode
	 */
	public StatementList add(Statement e){
		super.add(e);
		return this;
	}
}
