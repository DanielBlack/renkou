package danielblack.name.ast;

import java.util.ArrayList;

/**
 * 表达式列表
 * @see Statement
 * @see StatementList
 * @see TreeNode
 * @see ListNode
 * @author danielblack
 *
 */
public class ExpressionList extends StatementList{

	/**
	 * @see StatementList
	 */
	public ExpressionList(int lineNumber, ArrayList<TreeNode> elements) {
		super(lineNumber, elements);
	}
	/**
	 * @see StatementList
	 */
	public ExpressionList(int lineNumber){
		super(lineNumber);
	}
	
	/**
	 * @see ListNode
	 */
	public ExpressionList add(Expression e){
		super.add(e);
		return this;
	}
}
