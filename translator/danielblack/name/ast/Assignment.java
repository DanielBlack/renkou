package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 赋值语句
 * 		postfix_expression = expression
 * @see PostfixExpression
 * @see Expression
 * @see TreeNode
 * @author danielblack
 *
 */
public class Assignment extends Expression{
	private PostfixExpression name;
	private Expression init;

	/**
	 * @see PostfixExpression
	 * @see Expression
	 */
	public Assignment(int lineNumber, PostfixExpression name, Expression init) {
		super(lineNumber);
		this.name = name;
		this.init = init;
	}

	/**
	 * @see TreeNode
	 * @see Expression
	 */
	@Override
	public TreeNode copy() {
		return new Assignment(lineNumber, (PostfixExpression)copyHelper(name), 
				(Expression)copyHelper(init));
	}

	/**
	 * @see TreeNode
	 * @see Expression
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Assignment");
		dumpHelper(out, pad+4, name);
		dumpHelper(out, pad+4, init);
	}

}
