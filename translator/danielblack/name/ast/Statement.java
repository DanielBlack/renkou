package danielblack.name.ast;

/**
 * 语句的抽象类
 * TODO
 * 	 包含
 * 		1. 条件语句
 * 		2. 表达式语句
 * @author danielblack
 *
 */
public abstract class Statement extends TreeNode{

	/**
	 * @see TreeNode
	 */
	protected Statement(int lineNumber) {
		super(lineNumber);
	}

}
