package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 条件语句
 * 		IF expression statement_list ENDIF
 * 		或
 * 		IF expression statement_list ELSE statement_list ENDIF
 * @see Statement
 * @see StatementList
 * @see Expression
 * @see TreeNode
 * @author danielblack
 *
 */
public class Condition extends Statement{

	private Expression pred;
	private StatementList then_stmts;
	private StatementList else_stmts;
	
	/**
	 * 构造一个新的条件语句表达式
	 * @param lineNumber 行号
	 * @param pred 先决条件
	 * @param then_stmts 成立执行语句
	 * @param else_stmts 不成立执行语句
	 */
	public Condition(int lineNumber, Expression pred, 
			StatementList then_stmts, StatementList else_stmts) {
		super(lineNumber);
		this.pred = pred;
		this.then_stmts = then_stmts;
		this.else_stmts = else_stmts;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new Condition(lineNumber, (Expression)copyHelper(pred), 
				(StatementList)copyHelper(then_stmts), 
				(StatementList)copyHelper(else_stmts));
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Condition");
		PadHelper.padBlank(out, pad+4);
		out.println("<IF>");
		dumpHelper(out, pad+4, pred);
		PadHelper.padBlank(out, pad+4);
		out.println("<THEN>");
		dumpHelper(out, pad+4, then_stmts);
		PadHelper.padBlank(out, pad+4);
		out.println("<ELSE>");
		dumpHelper(out, pad+4, else_stmts);
	}

}
