package danielblack.name.ast;

import java.io.PrintStream;
import java.util.ArrayList;

/**
 * 过程列表
 * @see Procedure
 * @see ListNode
 * @author danielblack
 *
 */
public class ProcedureList extends ListNode{

	/**
	 * @see ListNode
	 * @param lineNumber
	 * @param elements
	 */
	public ProcedureList(int lineNumber, ArrayList<TreeNode> elements) {
		super(lineNumber, elements);
	}
	
	/**
	 * @see ListNode
	 * @param lineNumber
	 */
	public ProcedureList(int lineNumber) {
		super(lineNumber);
	}
	
	/**
	 * @see ListNode
	 */
	public ProcedureList add(Procedure e) {
		super.add(e);
		return this;
	}
}
