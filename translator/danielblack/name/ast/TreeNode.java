package danielblack.name.ast;

import java.io.PrintStream;

/**
 * AST节点的基类，任何类型的节点（包括列表节点）都应继承自此节点，
 * 考虑不用接口的原因是避免重复再写一些私有变量、函数。
 * @author danielblack
 */
public abstract class TreeNode {
	/** 节点在文件中的行号 **/
	protected int lineNumber;
	
	/** 
	 * 构造一个新的AST节点。
	 * @param lineNumber 节点在目标源文件中的行号
	 */
	protected TreeNode(int lineNumber){
		this.lineNumber = lineNumber;
	}
	
	/**
	 * 拷贝节点。
	 * 所有子类必须继承，便于以后的内存管理。
	 * @return 拷贝后的新节点
	 */
	public abstract TreeNode copy();
	
	/**
	 * 获取行号。
	 * @return 节点在源文件中的行号
	 */
	public int getLineNumber(){
		return lineNumber;
	}
	
	/**
	 * 拷贝辅助函数。
	 * 由于可能存在null节点，因此不能直接调用node.copy()，可能会出现Null Pointer。
	 *进行拷贝的时候使用此辅助函数可以避免此问题。
	 * @param node 所需要拷贝的抽象节点
	 * @return 拷贝所得的新节点，或者null
	 */
	public static TreeNode copyHelper(TreeNode node){
		if(node != null){
			return node.copy();
		}
		return null;
	}
	
	/**
	 * 打印辅助函数。
	 * 由于可能存在null节点，因此不能直接调用node.dump(out)，可能出现Null Pointer。
	 * 进行打印的时候使用此辅助函数可以避免此问题。
	 * @param out 输出流
	 * @param node 所需要打印的节点
	 */
	public static void dumpHelper(PrintStream out, int pad, TreeNode node){
		if(node != null){
			node.dump(out, pad);
		}
	}
	/**
	 * 打印函数，用于输出节点的调试信息。
	 * TODO 为了便于观察调试，输出的时候应该输出成树状。
	 * @param out 输出流
	 */
	public abstract void dump(PrintStream out, int pad);

}
