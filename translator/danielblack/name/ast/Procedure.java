package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 过程
 * 		PROCEDURE proc_name
 * 				statement_list
 * 		RETURN
 * 
 * @see TreeNode
 * @author danielblack
 *
 */
public class Procedure extends TreeNode{
	/**
	 * @see StatementList
	 * @param lineNumber 节点所处行号
	 * @param procName 过程名
	 * @param statementList 语句列表
	 */
	public Procedure(int lineNumber, String procName, StatementList statementList) {
		super(lineNumber);
		this.procName = procName;
		this.statementList = statementList;
	}
	private String procName;
	private StatementList statementList;
	
	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new Procedure(lineNumber, procName, (StatementList) copyHelper(statementList));
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Procdure " + procName);
		dumpHelper(out, pad+4, statementList);
	}
}
