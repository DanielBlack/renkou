package danielblack.name.ast;

import java.io.PrintStream;

import java_cup.runtime.Symbol;
import danielblack.name.utils.LexerHelper;
import danielblack.name.utils.PadHelper;

/**
 * 一元表达式
 * 		-expression
 * 		+expression
 * @see Expression
 * @author danielblack
 *
 */
public class UnaryExpression extends Expression{

	private Expression expr;
	private int symbol;
	/**
	 * @see Expression
	 * @param lineNumber 行号
	 * @param expr 表达式
	 * @param symbol 符号
	 */
	public UnaryExpression(int lineNumber, Expression expr, int symbol) {
		super(lineNumber);
		this.expr = expr;
		this.symbol = symbol;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new UnaryExpression(lineNumber, 
				(Expression)copyHelper(expr), symbol);
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Unary Expression");
		PadHelper.padBlank(out, pad+4);
		out.println(LexerHelper.dumpToken(symbol));
		dumpHelper(out, pad+4, expr);
	}

}
