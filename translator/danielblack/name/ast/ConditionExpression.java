package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 条件表达式
 * 		IIF ( expression, expression, expression )
 * @see Expression
 * @see TreeNode
 * @author danielblack
 *
 */
public class ConditionExpression extends Expression{

	private Expression pred;
	private Expression then_expr;
	private Expression else_expr;
	
	/**
	 * 构造一个新的条件表达式
	 * @param lineNumber 行号
	 * @param pred 条件
	 * @param then_expr 条件成立返回表达式
	 * @param else_expr 条件不成立返回表达式
	 */
	public ConditionExpression(int lineNumber, Expression pred, 
			Expression then_expr, Expression else_expr) {
		super(lineNumber);
		this.pred = pred;
		this.then_expr = then_expr;
		this.else_expr = else_expr;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new ConditionExpression(lineNumber, (Expression)copyHelper(pred), 
				(Expression)copyHelper(then_expr), 
				(Expression)copyHelper(else_expr));
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Condition");
		PadHelper.padBlank(out, pad+4);
		out.println("<IF>");
		dumpHelper(out, pad+4, pred);
		PadHelper.padBlank(out, pad+4);
		out.println("<THEN>");
		dumpHelper(out, pad+4, then_expr);
		PadHelper.padBlank(out, pad+4);
		out.println("<ELSE>");
		dumpHelper(out, pad+4, else_expr);
	}

}
