package danielblack.name.ast;

import java.io.PrintStream;

import danielblack.name.utils.PadHelper;

/**
 * 后缀表达式，用于作为左值
 * @see Expression
 * @author danielblack
 *
 */
public class PostfixExpression extends Expression{
	private Name name;
	
	/**
	 * @see Name
	 * @see Expression
	 * @param lineNumber
	 * @param name
	 */
	public PostfixExpression(int lineNumber, Name name) {
		super(lineNumber);
		this.name = name;
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public TreeNode copy() {
		return new PostfixExpression(lineNumber, (Name)copyHelper(name));
	}

	/**
	 * @see TreeNode
	 */
	@Override
	public void dump(PrintStream out, int pad) {
		PadHelper.padBlank(out, pad);
		out.println("+ Postfix Expression");
		dumpHelper(out, pad+4, name);
	}

}
