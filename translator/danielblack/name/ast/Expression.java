package danielblack.name.ast;

/**
 * 表达式抽象类, 表达式是有值的特殊语句
 * @see Statement
 * @see TreeNode
 * @author danielblack
 *
 */
public abstract class Expression extends Statement{
	protected Expression(int lineNumber) {
		super(lineNumber);
	}
}
