package danielblack.name.ast;

import java.io.PrintStream;

/**
 * 主过程，继承自普通的过程
 * 		statement_list
 * 		RETURN
 * TODO 应该使用单例模式防止多个Main出现。
 * @see Procedure
 * @see TreeNode
 * @author danielblack
 *
 */
public class MainProcedure extends Procedure{

	/**
	 * 构造一个新的主过程
	 * 主过程命名为"__MAIN__"，这儿偷懒下吧...其他过程不应该命名成这个名字！
	 * @param lineNumber 节点所处行号
	 * @param statementList 过程中的语句列表
	 * @see StatementList
	 */
	public MainProcedure(int lineNumber, StatementList statementList) {
		super(lineNumber, "__MAIN__", statementList);
	}
	
}
