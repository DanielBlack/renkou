定义
---

+ 全局变量  
    **PUBL** *NAME*
+ 数组
    **DIMENSION ID** '(' *INT*, *INT* ')' | **DIMENSION ID** '(' *INT* ')' | **PUBL** *ID* '(' *INT* ')' | **PUBL** *ID* '(' *INT*, *INT* ')'
+ 过程
    **PROC** *procedure\_id* *dummy\_expr\_list* **RETURN**

创建
---

+ **CREATE TABLE** *table\_id* **FREE** '(' *dfn_list* ')'     // 不创建新文件
+ **CREATE TABLE** *table\_id* '(' *dfn_list* ')'          // 创建新文件

赋值
---

+ 批量赋值 
	store_stmt: **STORE** NUMBER **TO** id\_list | **STORE** value **TO** array_id
+ 直接赋值
    assign_stmt: variable_id = expr

调用
---

+ **DO** *procedure\_id*
+ *ID* '(' *dummy_expr_list* ')'

循环
---

+ **FOR** *variable_id* '=' *INT* **TO** *INT* *dummy_expr_list* **ENDFOR**

+ **DO WHILE** *condition* *dummy\_expr\_list* **ENDDO**

条件
---

+ if_stmt: **IF** *expr* *dummy\_expr\_list* **ELSE** *dummy\_expr\_list* **ENDIF**
+ iff_stmt: **IIF** '(' *expr* ',' *expr* ',' *expr* ')'
+ docase_stmt: **DO** **CASE** case\_list **ENDCASE**

  case\_list: **CASE** *condition* *expr_list* | **CASE** *condition* *expr_list* case_list

选择
---

+ **SELECT** *ID*
+ **LOCATE** **FOR** *ID* '=' *ID*

拷贝
---

+ **COPY TO ARRAY** *array\_id* **FIELD** *field\_id* **FOR** *condition*
+ **COPY TO** *path* //复制表
+ **COPY STRUCTURE TO** *filename*

引用
---

+ macro: '&' *ID*

替换
---

+ *replace\_stmt*: **REPLACE** *replace\_cont* **FOR** *condition* | **REPLACE** *replace\_cont* **ALL** 

+ *replace\_cont:*  *field_id* **WITH** *expr* | *field_id* **WITH** *expr* ',' *replace\_cont*


释放内存
---

+ release_stmt: **RELEASE** *id\_list*

函数
---
+ function:

+ **COUNT TO** *variable_id* **FOR** *condition* //计数

+ **SUM** expr **TO** *variable\_id* | **SUM** *expr* **TO** *id* **FOR** *expr* //求和

+ **CALCULATE MIN() TO** *variable\_id* **FOR** *condition*

APPEND
----
+ append_stmt: **APPEND BLANK** | **APPEND FROM** *PATH*

+ **APPEND BLANK**
    追加一条空白记录
+ **APPEND FROM** PATH

ALTERNATE
----

+ **ALTER TABLE** *table\_id* **ADD** *field\_id* *TYPE*

USE
----

+ use_stmt: **USE** *table\_id* | **USE** *path*

其他
----

+ condition: *expr* *op* *expr*

+ op: "=" | ">=" | "<=" | "<>" | "<" | ">"

+ PATH: STRING | *macro*

+ id\_list: *ID* | *ID*, *id\_list*

+ ID: table\_id | field\_id | variable\_id | array\_id

+ expr: *NUMBER* | *statement* | 

??
---

+ **RELE** *id\_list*
    *id\_list*: *ID* | *ID* ',' *id\_list*

+ => 是 >= ...

+ **BROWSE FIELDS** *fieldname*

+ **LIST OFF FIELDS** *fieldname*

+ **INDEX ON** *fieldname* **TO** xxx
