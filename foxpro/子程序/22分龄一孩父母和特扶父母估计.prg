&& 统计独生子女死亡父母，允许生再生一个，不能生的（大于49岁）则获得政府补贴（特扶）
PROCEDURE 分龄一孩父母和特扶父母估计
*****一孩父母估计
FOR CX=1 TO 4

	ALI1=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区城镇农业子女',IIF(CX=3,'地区农村非农子女','地区城镇非农子女')))
	ALI2=IIF(CX=1,'地区农村农业父母',IIF(CX=2,'地区城镇农业父母',IIF(CX=3,'地区农村非农父母','地区城镇非农父母')))
	
	*仅作为字符串值？？-------原来是四个全局变量。。。。。。。。。。。。。
	新生一孩=IIF(CX=1,农村农业一孩,IIF(CX=2,农村非农一孩,IIF(CX=3,城镇农业一孩,城镇非农一孩)))
	
	IF ND0=ND1 && 第一年 ，准备数据，全部独生子女家庭数据
	
		SELECT A
		RELEASE ARD
		CALCULATE MAX(X) TO MANL FOR &ALI1..D<>0  &&独生子女
		COPY TO ARRAY ARD FIELDS &ALI1..D  FOR X<=MANL
		COPY TO ARRAY ARFR1 FIELDS &ALI1..FR1 FOR X>=15.AND.X<=49 && FR1 是生第一胎时母亲的年龄分布
		
		&& X=15的记录在 ARFR1(1)
		
		*****推算一孩母亲 年龄（非死亡）
		FOR NL=1 TO MANL+1
			IF ARD(NL)=0
				LOOP
			ENDIF 
			DS=ARD(NL)
			母亲MINL=NL+14
			母亲MANL=NL+48
			REPLACE  &ALI2..&FND2 WITH &ALI2..&FND2+DS*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
		ENDFOR
		
		*****推算一孩父亲
		COPY TO ARRAY ARFND2 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
		FOR NL=15 TO 64
			妻子RS=ARFND2(NL-14)
			妻子NL='妻子'-allt(STR(NL,3))
			REPLACE &ALI2..&MND2 WITH &ALI2..&MND2+妻子RS*需夫模式.&妻子NL ALL
		ENDFOR
	ELSE  &&非第一年，做推算！！！
		*********************
		*	新增一孩父母	*
		*********************
		*****推算新增一孩母亲
		SELECT A
		REPLACE  &ALI2..&FND2 WITH &ALI2..&FND2+新生一孩*&ALI1..FR1 FOR X>=15.AND.X<=49	&&新生一孩
		*****推算新增一孩父亲
		FOR NL=15 TO 64
			LOCATE FOR X=NL
			妻子RS=新生一孩*&ALI1..FR1
			妻子NL='妻子'-allt(STR(NL,3))
			REPLACE &ALI2..&MND2 WITH &ALI2..&MND2+妻子RS*需夫模式.&妻子NL ALL
		ENDFOR
		*****减去独转非母亲
		RELEASE AR独转非
		CALCULATE MAX(X) TO MANL FOR &ALI1..独转非<>0
		COPY TO ARRAY AR独转非 FIELDS &ALI1..独转非  FOR X<=MANL
		COPY TO ARRAY ARFR1 FIELDS &ALI1..FR1 FOR X>=15.AND.X<=49
		COPY TO ARRAY ARFND1 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
		FOR NL=1 TO MANL+1
			IF AR独转非(NL)=0
				LOOP
			ENDIF 
			D独转非=AR独转非(NL)
			母亲MINL=NL+14
			母亲MANL=NL+48
			REPLACE &ALI2..&FND2  WITH &ALI2..&FND2-D独转非*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
		ENDFOR
		COPY TO ARRAY ARFND2 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
		*****减去独转非父亲
		FOR NL=15 TO 64
			独转非母亲RS=ARFND1(nl-14)-ARFND2(nl-14)
			妻子NL='妻子'-allt(STR(NL,3))
			REPLACE &ALI2..&MND2 WITH &ALI2..&MND2-独转非母亲RS*需夫模式.&妻子NL ALL
		ENDFOR
	ENDIF
	
	
	DO CASE 
	CASE CX=1
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村农业独子父,农村农业独子母
	CASE CX=2
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇农业独子父,城镇农业独子母
	CASE CX=3
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村非农独子父,农村非农独子母
	CASE CX=4
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇非农独子父,城镇非农独子母
	ENDCASE
SUM &ALI2..&MND2,&ALI2..&FND2 TO S独子父,S独子母


*?S独子父,S独子母
ENDFOR

*****特扶父母
FOR CX=1 TO 4
	ALI1=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区城镇农业子女',IIF(CX=3,'地区农村非农子女','地区城镇非农子女')))
	ALI2=IIF(CX=1,'地区农村农业特扶',IIF(CX=2,'地区城镇农业特扶',IIF(CX=3,'地区农村非农特扶','地区城镇非农特扶')))
	SELECT A
	RELEASE ARSWD
	CALCULATE MAX(X) TO MANL FOR &ALI1..SWD<>0
	COPY TO ARRAY ARSWD FIELDS &ALI1..SWD  FOR X<=MANL && SWD死亡独生子女人数
	COPY TO ARRAY ARFR1 FIELDS &ALI1..FR1 FOR X>=15.AND.X<=49
	COPY TO ARRAY ARFND2 FIELDS &ALI2..&FND2 FOR X>=15
	*****推算丧子女母亲
	FOR NL=1 TO MANL+1
		IF ARSWD(NL)=0
			LOOP && 相当于 continue;
		ENDIF 
		DSW=ARSWD(NL)
		母亲MINL=NL+14
		母亲MANL=NL+48
		REPLACE  &ALI2..&FND2 WITH &ALI2..&FND2+DSW*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
	ENDFOR
	*****推算丧子女父亲
	FOR NL2=15 TO 64
		LOCATE FOR X=NL2
		新增丧子母亲=&ALI2..&FND2-ARFND2(NL2-14)
		妻子NL='妻子'-ALLT(STR(NL2,3))
		REPLACE &ALI2..&MND2 WITH &ALI2..&MND2+新增丧子母亲*需夫模式.&妻子NL ALL
	ENDFOR
	DO CASE 
	CASE CX=1
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村农业特扶父,农村农业特扶母
	CASE CX=2
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇农业特扶父,城镇农业特扶母
	CASE CX=3
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村非农特扶父,农村非农特扶母
	CASE CX=4
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇非农特扶父,城镇非农特扶母
	ENDCASE
ENDFOR


RETURN 