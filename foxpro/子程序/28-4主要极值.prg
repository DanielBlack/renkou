PROC   
CALC MAX(总人口),MAX(净迁入数),MIN(总人口),MIN(净迁入数),MAX(可实现TFR) TO MA1,MA2,MI1,MI2,MATFR0 FOR 年份>ND1
LOCA FOR 总人口=MA1.AND.年份>ND1
MAXND1=年份
LOCA FOR 净迁入数=MA2.AND.年份>ND1
MAXND2=年份
LOCA FOR 总人口=MI1.AND.年份>ND1
MIND1=年份
LOCA FOR 净迁入数=MI2.AND.年份>ND1
MIND2=年份
LOCA FOR 可实现TFR=MATFR0.AND.年份>ND1
MAND3=年份
LOCA FOR 年份=2020
ZRK20=总人口
CSRK20=出生
CSL20=出生率
TFR20=可实现TFR
JQR20=净迁入率
ZZL20=总增长率
CSH20=城镇人口比
SEL=STR(SELE(),3)
IF CX=1
	*?概率估算法,ALLT(STR(FN政策1,1)),ALLT(STR(FN时机1,1)),ALLT(STR(NY政策1,1)),ALLT(STR(NY时机1,1)),ALLT(STR(QY,2)),;
	ALLT(城乡B)-','-FA-','-JA-',',ALLT(STR(MAXND1)),',',ALLT(STR(MA1,10,2)),',',ALLT(STR(MIND1)),',',ALLT(STR(MI1,10,2)),',',;
	ALLT(STR(MAXND2)),',',ALLT(STR(MA2,10,2)),',',ALLT(STR(MIND2)),',',ALLT(STR(ROUN(MI2,2),8,2)),',',;
	ROUN(总人口,2),',',ALLT(STR(出生,10)),',',ALLT(STR(出生率,8,2)),',',ALLT(STR(可实现TFR,8,2)),',',ALLT(STR(净迁入率,8,2)),',',;
	ALLT(STR(总增长率,8,2)),',',ALLT(STR(城镇人口比,8,2)),',',ALLT(STR(MAND3,4)),',',ALLT(STR(MATFR0,10,2)),TIME()
*	?概率估算法,ALLT(STR(FN政策1,1)),ALLT(STR(FN时机1,1)),ALLT(STR(NY政策1,1)),ALLT(STR(NY时机1,1)),ALLT(STR(QY,2)),;
	ALLT(城乡B)-',',ROUN(总人口,2),',',ALLT(STR(出生,10)),',',ALLT(STR(城镇人口比,8,2)),',',ALLT(STR(MAND3,4)),',',ALLT(STR(MATFR0,10,2)),TIME()
ENDIF
SELE 401
USE &生育政策模拟摘要
APPE BLANK
REPL 堆释模式 WITH 释放模式,;
	政策1 WITH 非农ZC1,时机1 WITH 非农时机1,政策2 WITH 非农ZC2,时机2 WITH 非农时机2,政策3 WITH IIF(调整方式时机='多地区渐进普二',非农ZC3,''),时机3 WITH IIF(调整方式时机='多地区渐进普二',非农时机3,0),;
	地区别 WITH DQB,城乡别 WITH 城乡B,生育方案 WITH FA,迁移方案 WITH IIF(QY=1,'低',IIF(QY=2,'中','高')),;
	最大规模ND WITH MAXND1,最大规模 WITH MA1,最小规模ND WITH MIND1,最小规模 WITH MI1,最大迁移ND WITH MAXND2,最大迁移 WITH MA2,最小迁移ND WITH MIND2,最小迁移 WITH MI2,;
	最高TFRND  WITH MAND3,最高TFR WITH MATFR0,总人口20 WITH ZRK20,出生20 WITH CSRK20,出生率20 WITH CSL20,生育率20 WITH TFR20,;
	净迁入率20 WITH JQR20,总增长率20 WITH ZZL20,城市化20 WITH CSH20
SELE &SEL


RETU
