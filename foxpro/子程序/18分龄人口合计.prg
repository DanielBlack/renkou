PROCEDURE 分龄人口合计 && 最全面的一次合计
SELECT A
REPL 地区农村农业子女.D    WITH 地区农村农业子女.DF+地区农村农业子女.DM,地区农村农业子女.N    WITH 地区农村农业子女.NF+地区农村农业子女.NM ALL
REPL 地区农村非农子女.D    WITH 地区农村非农子女.DF+地区农村非农子女.DM,地区农村非农子女.N    WITH 地区农村非农子女.NF+地区农村非农子女.NM ALL
REPL 地区城镇农业子女.D    WITH 地区城镇农业子女.DF+地区城镇农业子女.DM,地区城镇农业子女.N    WITH 地区城镇农业子女.NF+地区城镇农业子女.NM ALL
REPL 地区城镇非农子女.D    WITH 地区城镇非农子女.DF+地区城镇非农子女.DM,地区城镇非农子女.N    WITH 地区城镇非农子女.NF+地区城镇非农子女.NM ALL

REPL 地区农村非农子女.HJM WITH 地区农村非农子女.DM+地区农村非农子女.DDBM+地区农村非农子女.NM,;
	 地区农村非农子女.HJF WITH 地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF,;
	 地区农村非农子女.HJ WITH 地区农村非农子女.HJM+地区农村非农子女.HJF ALL


REPL 地区农村农业子女.HJM WITH 地区农村农业子女.DM+地区农村农业子女.DDBM+地区农村农业子女.NM,;
	 地区农村农业子女.HJF WITH 地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF,;
	 地区农村农业子女.HJ WITH 地区农村农业子女.HJM+地区农村农业子女.HJF ALL


REPL 地区城镇非农子女.HJM WITH 地区城镇非农子女.DM+地区城镇非农子女.DDBM+地区城镇非农子女.NM,;
	 地区城镇非农子女.HJF WITH 地区城镇非农子女.DF+地区城镇非农子女.DDBF+地区城镇非农子女.NF,;
	 地区城镇非农子女.HJ WITH 地区城镇非农子女.HJM+地区城镇非农子女.HJF ALL


REPL 地区城镇农业子女.HJM WITH 地区城镇农业子女.DM+地区城镇农业子女.DDBM+地区城镇农业子女.NM,;
	 地区城镇农业子女.HJF WITH 地区城镇农业子女.DF+地区城镇农业子女.DDBF+地区城镇农业子女.NF,;
	 地区城镇农业子女.HJ WITH 地区城镇农业子女.HJM+地区城镇农业子女.HJF ALL

REPL 地区非农子女.DF  WITH 地区城镇非农子女.DF+地区农村非农子女.DF,地区非农子女.DM  WITH  地区城镇非农子女.DM+地区农村非农子女.DM,地区非农子女.D  WITH  地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.NF  WITH 地区城镇非农子女.NF+地区农村非农子女.NF,地区非农子女.NM  WITH  地区城镇非农子女.NM+地区农村非农子女.NM,地区非农子女.N  WITH  地区非农子女.NM+地区非农子女.NF ALL
REPL 地区非农子女.N   WITH 地区非农子女.NM+地区非农子女.NF,地区非农子女.D WITH 地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.HJF WITH 地区城镇非农子女.HJF+地区农村非农子女.HJF,地区非农子女.HJM  WITH  地区城镇非农子女.HJM+地区农村非农子女.HJM,地区非农子女.HJ WITH  地区非农子女.HJF+地区非农子女.HJM ALL

REPL 地区农业子女.DF  WITH  地区城镇农业子女.DF+地区农村农业子女.DF,地区农业子女.DM  WITH  地区城镇农业子女.DM+地区农村农业子女.DM,地区农业子女.D  WITH  地区农业子女.DM+地区农业子女.DF ALL
REPL 地区农业子女.NF  WITH  地区城镇农业子女.NF+地区农村农业子女.NF,地区农业子女.NM  WITH  地区城镇农业子女.NM+地区农村农业子女.NM,地区农业子女.N  WITH  地区农业子女.NM+地区农业子女.NF ALL
REPL 地区农业子女.HJF WITH  地区城镇农业子女.HJF+地区农村农业子女.HJF,地区农业子女.HJM  WITH  地区城镇农业子女.HJM+地区农村农业子女.HJM,地区农业子女.HJ WITH  地区农业子女.HJF+地区农业子女.HJM ALL

REPL 地区子女.DF  WITH  地区城镇非农子女.DF+地区城镇农业子女.DF+地区农村非农子女.DF+地区农村农业子女.DF,;
	 地区子女.DM  WITH  地区城镇非农子女.DM+地区城镇农业子女.DM+地区农村非农子女.DM+地区农村农业子女.DM,地区子女.D  WITH  地区子女.DM+地区子女.DF ALL
REPL 地区子女.NF  WITH  地区城镇非农子女.NF+地区城镇农业子女.NF+地区农村非农子女.NF+地区农村农业子女.NF,;
 						 地区子女.NM  WITH  地区城镇非农子女.NM+地区城镇农业子女.NM+地区农村非农子女.NM+地区农村农业子女.NM,地区子女.N  WITH  地区子女.NM+地区子女.NF ALL
REPL 地区子女.HJF WITH  地区子女.DF+地区子女.NF,地区子女.HJM  WITH  地区子女.DM+地区子女.NM,地区子女.HJ  WITH  地区子女.HJM+地区子女.HJF ALL

REPL 地区农村非农分龄.&MND2 WITH 地区农村非农子女.HJM,地区农村非农分龄.&FND2 WITH 地区农村非农子女.HJF ALL
REPL 地区城镇非农分龄.&MND2 WITH 地区城镇非农子女.HJM,地区城镇非农分龄.&FND2 WITH 地区城镇非农子女.HJF ALL
REPL 地区非农分龄.&MND2 WITH 地区农村非农分龄.&MND2+地区城镇非农分龄.&MND2,地区非农分龄.&FND2 WITH 地区农村非农分龄.&FND2+地区城镇非农分龄.&FND2 ALL

REPL 地区农村农业分龄.&MND2 WITH 地区农村农业子女.HJM,地区农村农业分龄.&FND2 WITH 地区农村农业子女.HJF ALL
REPL 地区城镇农业分龄.&MND2 WITH 地区城镇农业子女.HJM,地区城镇农业分龄.&FND2 WITH 地区城镇农业子女.HJF ALL
REPL 地区农业分龄.&MND2 WITH 地区农村农业分龄.&MND2+地区城镇农业分龄.&MND2,地区农业分龄.&FND2 WITH 地区农村农业分龄.&FND2+地区城镇农业分龄.&FND2 ALL

REPL 地区农村分龄.&MND2 WITH 地区农村非农分龄.&MND2+地区农村农业分龄.&MND2,地区农村分龄.&FND2 WITH 地区农村非农分龄.&FND2+地区农村农业分龄.&FND2 ALL
REPL 地区城镇分龄.&MND2 WITH 地区城镇非农分龄.&MND2+地区城镇农业分龄.&MND2,地区城镇分龄.&FND2 WITH 地区城镇非农分龄.&FND2+地区城镇农业分龄.&FND2 ALL
REPL 地区分龄.&MND2 WITH 地区城镇分龄.&MND2+地区农村分龄.&MND2,地区分龄.&FND2 WITH 地区城镇分龄.&FND2+地区农村分龄.&FND2 ALL

RETU
