PROCEDURE 全国迁移平衡

STORE 0 TO 全国QY  &&给全局变量赋值——废弃变量，不用翻译

FOR DQX=DQ1 TO DQ2	 && 分省循环
*	DQX=IIF(dqx=12,DQ2,dqx)
	STORE 0 TO SQ入0M,SQ出0M,SQ入0F,SQ出0F,SDQQY  &&都是废弃变量！！！！
    DQB=ALLT(ARDQ出生XBB(DQX,2))	&&省代码,DQ,出生XBB05,出生XBB00
    DQ夹=ALLT(ARDQ出生XBB(DQX,1))-DQB

    && 调用上一个方法的数据——分省迁移初算的结果
 	地区农村农业子女婚配预测表='D:\分省生育政策仿真1102\生育政策仿真\&调整方式时机\&DQ夹\农村\\分龄婚配_农业\\&实现程度与释放模式\'-DQB-'农村农业子女婚配预测表'-'_'-FA-'_'-JA
 	地区农村非农子女婚配预测表='D:\分省生育政策仿真1102\生育政策仿真\&调整方式时机\&DQ夹\农村\\分龄婚配_非农\\&实现程度与释放模式\'-DQB-'农村非农子女婚配预测表'-'_'-FA-'_'-JA
 	地区城镇农业子女婚配预测表='D:\分省生育政策仿真1102\生育政策仿真\&调整方式时机\&DQ夹\城镇\\分龄婚配_农业\\&实现程度与释放模式\'-DQB-'城镇农业子女婚配预测表'-'_'-FA-'_'-JA
 	地区城镇非农子女婚配预测表='D:\分省生育政策仿真1102\生育政策仿真\&调整方式时机\&DQ夹\城镇\\分龄婚配_非农\\&实现程度与释放模式\'-DQB-'城镇非农子女婚配预测表'-'_'-FA-'_'-JA
 	
	SELE 113
		USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表  && 仿真结果_全国迁移平衡表结构	taskid
	   	INDEX ON X TO IN113
	SELE 61 
 	 	USE &地区农村农业子女婚配预测表 ALIAS  地区农村农业子女		&&农村婚配，带QY的都是迁移数   仿真结果_婚配预测表  ，level1=dqx,level2=123,level3=132,level4=144,level5=150 taskid
   		INDEX ON X TO IN61
 	SELE 62 
 	 	USE &地区农村非农子女婚配预测表 ALIAS  地区农村非农子女	&&农村婚配			仿真结果_婚配预测表  ，level1=dqx,level2=123,level3=130,level4=144,level5=150 taskid
    	INDEX ON X TO IN62
 	SELE 63 
 	 	USE &地区城镇农业子女婚配预测表 ALIAS  地区城镇农业子女		&&城镇婚配		仿真结果_婚配预测表  ，level1=dqx,level2=120,level3=132,level4=144,level5=150 taskid
    	INDEX ON X TO IN63
 	SELE 64 
 	 	USE &地区城镇非农子女婚配预测表 ALIAS  地区城镇非农子女	&&城镇非农子女		仿真结果_婚配预测表  ，level1=dqx,level2=120,level3=130,level4=144,level5=150 taskid
   		INDEX ON X TO IN64
	SELECT 61
	SET RELA TO X INTO 62,X INTO 63,X INTO 64,X INTO 113
	*****************
	*	迁移平衡	*
 	*****************
	FOR CX=1 TO 2
		城乡=IIF(CX=1,'城镇','农村')
		QYGL=IIF(CX=1,'CQYGL','XQYGL')
		FOR NFX=1 TO 2
			农非=IIF(NFX=1,'农业','非农')
			ALIA1='地区'-城乡-农非-'子女'  
			
			&& 接下来对6个字段进行循环
			FOR ZN=1 TO 3
				ZNB=IIF(ZN=1,'D',IIF(ZN=2,'DDB','N'))
				FOR XBX=1 TO 2
					XB=IIF(XBX=1,'M','F')
					
					子女=ZNB-XB
					子女QY=ZNB-'QY'-XB
					迁移平衡=IIF(XBX=1,'迁移平衡M','迁移平衡F')
					迁入=IIF(XBX=1,'迁入M','迁入F')
					迁出=IIF(XBX=1,'迁出M','迁出F')
					平衡迁入=IIF(XBX=1,'平衡迁入M','平衡迁入F')
					平衡迁出=IIF(XBX=1,'平衡迁出M','平衡迁出F')
					迁入和0=IIF(XBX=1,迁入和M0,迁入和F0)
					迁出和0=IIF(XBX=1,迁出和M0,迁出和F0)
					
					* ALIA1：   地区城镇农业子女 地区城镇非农子女 地区农村农业子女 地区农村非农子女
					* 子女QY： 	DQYM DQYF DDBQYM DDBQYF NQYM NQYF
					* 迁移平衡：迁移平衡M','迁移平衡F'
					* 迁入：	'迁入M','迁入F'
					* 迁出：	'迁出M','迁出F'
					* 平衡迁入：'平衡迁入M','平衡迁入F'
					* 平衡迁出：'平衡迁出M','平衡迁出F'
					
					&& 关键计算
					REPLACE &ALIA1..&子女QY WITH 迁移平衡表.&迁移平衡*&ALIA1..&子女QY/迁移平衡表.&迁入 FOR &ALIA1..&子女QY>0.and.迁移平衡表.&迁入<>0
					&& 设计： 在for(X:0~110)循环中，计算 provincArr并保存到provincArr中  需要创建 迁移平衡Arr 并从数据库中获取数据
					
					&& 累计
					REPLACE 迁移平衡表.&平衡迁入 WITH 迁移平衡表.&平衡迁入+&ALIA1..&子女QY FOR &ALIA1..&子女QY>0
					&& 设计：同上，需要创建 平衡迁入ARR(初始化为0) 
					
					&& 对特殊记录进行重写
					REPLACE &ALIA1..&子女QY WITH -(MIN(迁移平衡表.&迁移平衡*&ALIA1..&子女QY/迁移平衡表.&迁出,&ALIA1..&子女)) FOR &ALIA1..&子女QY<0.and.迁移平衡表.&迁出<>0
					&&设计：同上,
					
					&& 累计
					REPLACE 迁移平衡表.&平衡迁出 WITH 迁移平衡表.&平衡迁出+&ALIA1..&子女QY FOR &ALIA1..&子女QY<0
					&& 同上上
					
					&& 累计地区成全国迁移
					SUM &ALIA1..&子女QY TO DQQY  &&都是废弃变量！！！！
					SDQQY=SDQQY+ DQQY	         &&都是废弃变量！！！！
				ENDFOR && XBX
			ENDFOR && ZN
			
			&&对若干个字段进行分别处理
			
			&& 合计男女成不分性别的总数
		 	REPLACE &ALIA1..DQY WITH &ALIA1..DQYF+&ALIA1..DQYM,
		 			&ALIA1..NQY WITH &ALIA1..NQYF+&ALIA1..NQYM,;
		 			&ALIA1..DDBQYS WITH &ALIA1..DDBQYM+&ALIA1..DDBQYF,
		 			&ALIA1..HJQYF WITH &ALIA1..DQYF+&ALIA1..NQYF+&ALIA1..DDBQYF,;
		 			&ALIA1..HJQYM WITH &ALIA1..DQYM+&ALIA1..NQYM+&ALIA1..DDBQYM,
		 			&ALIA1..HJQY WITH &ALIA1..HJQYF+&ALIA1..HJQYM ALL
		 	&& 创建数组来计算
		 			
		ENDFOR  && NFX
	ENDFOR  && CX
	SUM &ALIA1..HJQY,&ALIA1..HJ TO SQY,SHJ  &&SQY有用，SHJ废弃
	DQJQL(DQX)=SQY/SHJ*100  && 废弃变量
*	?DQB,SDQQY
*	全国QY=全国QY+SDQQY
ENDFOR  && DQX
*SUM 迁移平衡表.平衡迁入M,迁移平衡表.平衡迁出M,迁移平衡表.平衡迁入F,迁移平衡表.平衡迁出F TO 平衡迁入M,平衡迁出M,平衡迁入F,平衡迁出F
*?平衡迁入M,平衡迁出M,平衡迁入F,平衡迁出F
*?平衡迁入M-ABS(平衡迁出M),平衡迁入F-ABS(平衡迁出F)
*?全国QY
RETURN 