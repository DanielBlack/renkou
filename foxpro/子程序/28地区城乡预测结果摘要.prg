PROCEDURE 地区城乡预测结果摘要
??',',STR(N1D2C,4,2),',',STR(N2D1C,4,2),',',STR(NNFC,4,2),',',STR(N1D2N,4,2),',',STR(N2D1N,4,2),',',STR(NNFN,4,2),',',;
STR(地区政策TFR0,4,2),',',STR(城镇政策TFR0,4,2),',',STR(非农政策TFR0,4,2),',',STR(农村政策TFR0,4,2),',',STR(农业政策TFR0,4,2)

*?STR(CQYB,6,2),STR(XQYB,6,2),STR(C总迁率M,6,2),STR(C总迁率F,6,2),STR(X总迁率M,6,2),STR(X总迁率F,6,2)

*REPLACE 年份 WITH ND0,地区 WITH DQB,政策方案 WITH fa-ja,可实现TFR WITH 地区实现TFR0,政策生育率 WITH 地区政策TFR0,超生生育率 WITH 地区超生TFR,超生孩子数 WITH 超生S
											
											
SELECT 402	&&各地区生育率与迁移摘要 	
	REPLACE FN女单终身 WITH N1D2C,FN男单终身 WITH  N2D1C,FN双非终身 WITH NNFC,NY女单终身 WITH  N1D2N,NY男单终身 WITH  N2D1N,NY双非终身 WITH NNFN
	REPLACE	DQ时期TFR  WITH 地区政策TFR0,CZ时期TFR WITH 城镇政策TFR0,FN时期TFR WITH 非农政策TFR0 ,NC时期TFR  WITH 农村政策TFR0,NY时期TFR  WITH 农业政策TFR0

SELECT A
DQQY=0
FOR CX=1 TO 5
	
	城乡B=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
	分龄预测='地区'-城乡B-'分龄'
	迁移人口='地区'-城乡B-IIF(CX=3.OR.CX=5,'子女','迁移')
	
	*死亡概率M=城乡B-'死亡概率M'------ 
	*死亡概率F=城乡B-'死亡概率F'------
	
	城乡死亡='地区'-城乡B-'死亡'
	
	SELE A
	LOCA FOR X=0
	出生人口=(&分龄预测..&MND2+&分龄预测..&FND2)/10000
	出生人口M=&分龄预测..&MND2/10000
	出生人口F=&分龄预测..&FND2/10000
	
	IF CX<=2.OR.CX=4
		SUM (&迁移人口..&MND2+&迁移人口..&FND2)/10000 TO 	净迁入RK
		DO 死亡人口计算
  		DO 计算预期寿命
  		DO CASE 
  		CASE CX=2.OR.CX=3
  			CEM0=EM0
  			CEF0=EF0
  		CASE CX=4.OR.CX=5
  			XEM0=EM0
  			XEF0=EF0
  		ENDCASE
  	ELSE
		SUM &迁移人口..HJQY/10000 TO 净迁入RK
		
  	ENDIF  
  	??',',IIF(CX=1,'净迁',''),STR(净迁入RK,6,2)	
  	IF CX=1
	  	??'与上年比：',STR(净迁入RK/DQQYND0,6,2)
    ENDIF 
  	FIE1=IIF(CX=1,'DQ迁移合计',ALLTRIM(城乡B)-'迁移')
  	SELECT 402
  	REPLACE &FIE1 WITH 净迁入RK
  	IF CX=1
  		SQY=SQY+净迁入RK
  		SDQQY=净迁入RK
  	ENDIF
  	IF CX=2.OR.CX=4
  		DQQY=DQQY+净迁入RK
  	ENDIF
	SELE IIF(CX=1,1,IIF(CX=2,2,IIF(CX=3,3,IIF(CX=4,4,5))))	    &&SELE 1  地区分龄；SELE 2  地区城镇分龄；SELE 3 地区非农分龄；SELE 4:地区农村分龄；SELE 5  地区农业分龄；
  		EM0=IIF(CX=2.OR.CX=3,CEM0,IIF(CX=4.OR.CX=5,XEM0,EM0))
  		EF0=IIF(CX=2.OR.CX=3,CEF0,IIF(CX=4.OR.CX=5,XEF0,EF0))
		DO 年龄分组和年龄结构
	SELE IIF(CX=1,51,IIF(CX=2,52,IIF(CX=3,53,IIF(CX=4,54,55)))) &&SELE 51 地区概要；SELE 52 地区城镇概要;SELE 53 地区非农概要;SELE 54 地区农村概要；SELE 55  地区农业概要；							   	
		DQB=IIF(CX=1,DQB,城乡B)
		SEL=STR(SELECT(),4)
		*LOCATE FOR 年份=ND0
		DO 预测结果摘要
		IF CX=1
			DQQYL=净迁入率
		ENDIF
		*?'地区',地区,'年份',STR(年份,4),'出生-出生一孩-出生二孩',STR(出生-出生一孩-出生二孩,8,2),'生育率',STR(生育率,8,2),'自增率',STR(自增率,8,2),'净迁入数',STR(净迁入数,8,2),'城镇人口比',STR(城镇人口比,8,2)
	IF ND0=ND2
		DO 主要极值
	ENDIF
ENDFOR	&&分城乡概况摘要循环
RETURN 
