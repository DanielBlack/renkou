PROCEDURE 地区特扶父母统计
SELECT A
REPL 地区城镇特扶.&MND2 WITH 地区城镇非农特扶.&MND2+地区城镇农业特扶.&MND2,地区城镇特扶.&FND2 WITH 地区城镇非农特扶.&FND2+地区城镇农业特扶.&FND2 ALL
REPL 地区农村特扶.&MND2 WITH 地区农村非农特扶.&MND2+地区农村农业特扶.&MND2,地区农村特扶.&FND2 WITH 地区农村非农特扶.&FND2+地区农村农业特扶.&FND2 ALL
REPL 地区非农特扶.&MND2 WITH 地区城镇非农特扶.&MND2+地区城镇非农特扶.&MND2,地区非农特扶.&FND2 WITH 地区城镇非农特扶.&FND2+地区城镇非农特扶.&FND2 ALL
REPL 地区农业特扶.&MND2 WITH 地区农村农业特扶.&MND2+地区农村农业特扶.&MND2,地区农业特扶.&FND2 WITH 地区农村农业特扶.&FND2+地区农村农业特扶.&FND2 ALL
REPL 地区特扶.&MND2 WITH 地区城镇特扶.&MND2+地区农村特扶.&MND2,地区特扶.&FND2 WITH 地区城镇特扶.&FND2+地区农村特扶.&FND2 ALL
REPL 地区城镇父母.&MND2 WITH 地区城镇非农父母.&MND2+地区城镇农业父母.&MND2,地区城镇父母.&FND2 WITH 地区城镇非农父母.&FND2+地区城镇农业父母.&FND2 ALL
REPL 地区农村父母.&MND2 WITH 地区农村非农父母.&MND2+地区农村农业父母.&MND2,地区农村父母.&FND2 WITH 地区农村非农父母.&FND2+地区农村农业父母.&FND2 ALL
REPL 地区非农父母.&MND2 WITH 地区城镇非农父母.&MND2+地区城镇非农父母.&MND2,地区非农父母.&FND2 WITH 地区城镇非农父母.&FND2+地区城镇非农父母.&FND2 ALL
REPL 地区农业父母.&MND2 WITH 地区农村农业父母.&MND2+地区农村农业父母.&MND2,地区农业父母.&FND2 WITH 地区农村农业父母.&FND2+地区农村农业父母.&FND2 ALL
REPL 地区父母.&MND2 WITH 地区城镇父母.&MND2+地区农村父母.&MND2,地区父母.&FND2 WITH 地区城镇父母.&FND2+地区农村父母.&FND2 ALL
FOR CX=1 TO 4
	ALI1=IIF(CX=1,'地区农村农业特扶',IIF(CX=2,'地区城镇农业特扶',IIF(CX=3,'地区农村非农特扶','地区城镇非农特扶')))
	ALI2=IIF(CX=1,'地区农村农业父母',IIF(CX=2,'地区城镇农业父母',IIF(CX=3,'地区农村非农父母','地区城镇非农父母')))
	SELECT A
	SUM &ALI1..&MND2,&ALI1..&FND2 TO TFM,TFF FOR X>=49
	SUM &ALI2..&MND2,&ALI2..&FND2 TO S一孩父,S一孩母 FOR X>=60
	SELE IIF(CX=1,41,IIF(CX=2,42,IIF(CX=3,43,44)))
	LOCATE FOR 年份=ND0
	REPLACE 特扶F WITH TFF,特扶M WITH TFM,特扶 WITH TFF+TFM,一孩父 WITH S一孩父,一孩母  WITH S一孩母 ,一孩父母 WITH S一孩父+S一孩母
*	?特扶F,特扶M,特扶,一孩父,一孩母,一孩父母
ENDFOR
RETURN 	