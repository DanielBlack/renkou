PROCEDURE 基础数据和基本参数
*****以地区和年份为关键字的文件*****

*--lqz-- AAA开头的表是临时表
&& index on 字段名 to 文件名 表示建立一个文件名为"文件名"的索引文件，索引在"索引名"字段中

SELECT 301 &&选择工作区
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\D人口\00各地区\迁移\各地区城乡人口迁移估计96_09.DBF 
	COPY TO AAA各地区变动 FOR 年份>=2006
	USE AAA各地区变动 ALIAS 各地区变动
	INDEX ON 地区-STR(年份,4) TO IN301
SELECT 302
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\d人口\00各地区\概要\各地区户数人口数性别比和户规模06_08.dbf ALIA 抽样性别
	INDEX ON 地区-STR(年份,4) TO IN302
SELECT 303
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\D人口\00各地区\概要\各地区人口城乡构成05_09 ALIA 地区城乡构成
	COPY TO AAA地区城乡构成2008 FIELDS 地区,CZRKB FOR 年份=2008
	INDEX ON 地区-STR(年份,4) TO IN303
SELECT 304
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\D人口\00各地区\概要\各地区人口四率96_09.DBF ALIA 地区人口四率
	INDEX ON 地区-STR(年份,4) TO IN304
SELECT 301

*--lqz--在表中间建立联系，在主工作区可以调用其他表的数据
SET RELATION TO 地区-STR(年份,4) INTO 302,地区-STR(年份,4) INTO 303,地区-STR(年份,4) INTO 304
REPLACE 地区城乡构成.CSL WITH 地区人口四率.CSL, 地区城乡构成.SWL WITH 地区人口四率.SWL, 地区城乡构成.ZZL WITH 地区人口四率.ZZL FOR 地区城乡构成.CSL=0
*****以地区为关键字的文件*****
SELECT 199
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\迁移概率\分省净迁移率估计2008	ALIA 净迁率08 &&依据 D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\D人口\00各地区\迁移\各地区城乡人口迁移估计96_09.DBF 计算
	INDEX ON 省代码 TO IN199
SELECT 200
	USE AAA地区城乡构成2008 ALIA 城乡构成
	ALTER TABLE  AAA地区城乡构成2008 ADD 省代码 C(2)
	INDEX ON 地区 TO IN200
SELECT 201
	USE &死亡率修正系数 ALIAS 死亡修正系数
	SET RELATION TO 地区 INTO 200
	REPLACE 城乡构成.省代码 WITH 死亡修正系数.省代码 ALL
	INDEX ON 省代码 TO IN201
SELECT 200
	USE AAA地区城乡构成2008 ALIA 城乡构成
	INDEX ON 省代码 TO IN200
SELECT 202
	USE &迁移修正系数0428 ALIAS  迁移修正系数
	INDEX ON 省代码 TO IN202
SELECT 203
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\生育模式\各地区50岁及以上妇女无活产子女比例 ALIAS 无活产比例
	INDEX ON 省代码 TO IN203
SELECT 204
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\d人口\00各地区\出生\各地区代码和出生性别比90_05.dbf ALIAS 性别比
	INDEX ON 省代码 TO IN204
SELECT 205
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\人口抽样调查2005\各地区\分地区平均初婚年龄2005.dbf ALIAS 初婚年龄
	INDEX ON 省代码 TO IN205
SELECT 206
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\分省代码	 ALIAS 分省代码
	INDEX ON 省代码 TO IN206
	
&& 经审核 与 基本参数_各地区终身政策生育率   批次id=1
SELECT 207
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\生育政策\各地区终身政策生育率.dbf	 ALIAS 终身政策生育率
	INDEX ON 省代码 TO IN207
SELECT 208
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\现状估计1227\现实生育率与政策生育率比较06_09  ALIAS 生育率实现比
	&&现政比06=现实生育率/当年本地政策生育率（当年本地政策偏离度）,现政比年增=(2009年现政比-2006年现政比)/3
	&&实国B06=现实生育率/当年国家政策生育率（当年国均水平偏离度）,实国B年增=(2009年实国B-2006年实国B)/3
	
	&&？？？？？？？？？什么用？？？
	CALCULATE MIN(现实MI) TO MITFR
	
	LOCATE FOR 地区='全国'
	全国实现TFR0=现实08
	*CALCULATE MAX(现实06),MAX(现实07),MAX(现实08),MAX(现实09),MIN(现实06),MIN(现实07),MIN(现实08),MIN(现实09) TO MA06,MA07,MA08,MA09,MI06,MI07,MI08,MI09
	*MATFR=MAX(MA06,MA07,MA08,MA09)
	*MITFR=MIN(MI06,MI07,MI08,MI09)
	INDEX ON 省代码 TO IN208
	
	
SELECT 201
&& set relation to 字段名 into 工作区x ：在本工作区中建立联系，此联系是联系到工作区x中(该工作区中已打开某个表)，且外键为“字段名”
SET RELATION TO 省代码 INTO 199,省代码 INTO 200,省代码 INTO 202,省代码 INTO 203,省代码 INTO 204,省代码 INTO 205,省代码 INTO 206,省代码 INTO 207,省代码 INTO 208	&&,省代码 INTO 209
******地区别与出生性别比*****
COPY TO ARRA ARDQ出生XBB FIEL 省代码,地区,性别比.出生XBB05,性别比.出生XBB90,生育率实现比.现政比09,生育率实现比.现政比年增,生育率实现比.现实09,;
	生育率实现比.政策09,城乡构成.CZRKB,净迁率08.迁移率,生育率实现比.现实mi,生育率实现比.现政比09,生育率实现比.现政比年增,分省代码.三地带 FOR 地区<>'全国'	&&.AND.年份=2008
*COPY TO ARRA ARDQ出生XBB FIEL 省代码,地区,性别比.出生XBB05,性别比.出生XBB90,生育率实现比.现政比08,生育率实现比.现政比年增,生育率实现比.现实08,;
	生育率实现比.政策08,城乡构成.CZRKB,净迁率08.迁移率,生育率实现比.现实mi,生育率实现比.现政比08,生育率实现比.现政比年增,分省代码.三地带 FOR 地区<>'全国'	&&.AND.年份=2008
COUN TO DQS	 FOR 地区<>'全国'	&&.AND.年份=2008



*****************************
SELECT 181
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\现状估计1227\00全国\合计\人口概要\全国人口概要_非农现行农业现行_中迁
	*--lqz--select的条件 相当sql的where子句
	LOCATE FOR 年份=2009	&&ND0-1  
	*--lqz--给全局变量赋值：全国城市化水平
	QGCSH08=城镇人口比
SELE 75
 	USE	D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\全国普查01\夫妻年龄\全国按丈夫年龄分的妻子年龄分布模式（修匀）
 	COPY TO BBB对妻需求分布
 	USE  BBB对妻需求分布 ALIA 对妻需求
 	
 	ALTE TABL BBB对妻需求分布 RENA 妻子年龄 TO X
 	ALTE TABL BBB对妻需求分布 ALTE X N(4)
 	ALTE TABL BBB对妻需求分布 ADD DD妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NMDF妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NFDM妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NN妻 N(8)
 	COPY TO BBB对妻需求分布模式
 	*--lqz--建立索引是为了建立关联做准备
    INDEX ON X TO IN75
SELE 76
 	USE BBB对妻需求分布模式 ALIA 需妻模式
    INDEX ON X TO IN76
SELE 77
	USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\全国普查01\夫妻年龄\全国按妻子年龄分的丈夫年龄分布模式（修匀）.DBF
 	COPY TO BBB对夫需求分布
 	USE  BBB对夫需求分布 ALIA 需夫模式
 	ALTE TABL BBB对夫需求分布 RENA 丈夫年龄 TO X
 	ALTE TABL BBB对夫需求分布 ALTE X N(4)
 	ALTE TABL BBB对夫需求分布 ADD DD夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NMDF夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NFDM夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NN夫 N(8)
    INDEX ON X TO IN77