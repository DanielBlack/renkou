PROCEDURE 地区多种政策和可实现时期生育率
*******************************************************************************************************************
*	说明:NFDMB=政策NFDMB+超生NFDMB,NMDFB=政策NMDFB+超生NMDFB。NFDMB,NMDFB即为实现生育数，以此计算的TFR即实现生育率
*	超生生育+政策生育=(DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B),即实现生育
*******************************************************************************************************************
*农村农业TFR,农村非农TFR,城镇农业TFR,城镇非农TFR即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区农村农业分龄.&FND1
SUM  (地区农村农业子女.DDB+地区农村农业子女.NMDFB+地区农村农业子女.NFDMB+地区农村农业子女.NNB+地区农村农业子女.单独释放B+地区农村农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村农业分龄.&FND2)/2) ;
	 TO 农村农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农村农业分龄.&FND2)/2)<>0		&&农村农业生育率
COPY TO ARRAY ARFND1 FIELDS 地区农村非农分龄.&FND1
SUM  (地区农村非农子女.DDB+地区农村非农子女.NMDFB+地区农村非农子女.NFDMB+地区农村非农子女.NNB+地区农村非农子女.单独释放B+地区农村非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村非农分龄.&FND2)/2) ;
	 TO 农村非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农村非农分龄.&FND2)/2)<>0		&&农村非农生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇农业分龄.&FND1
SUM  (地区城镇农业子女.DDB+地区城镇农业子女.NMDFB+地区城镇农业子女.NFDMB+地区城镇农业子女.NNB+地区城镇农业子女.单独释放B+地区城镇农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇农业分龄.&FND2)/2) ;
	 TO 城镇农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区城镇农业分龄.&FND2)/2)<>0		&&城镇农业生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇非农分龄.&FND1
SUM  (地区城镇非农子女.DDB+地区城镇非农子女.NMDFB+地区城镇非农子女.NFDMB+地区城镇非农子女.NNB+地区城镇非农子女.单独释放B+地区城镇非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇非农分龄.&FND2)/2) ;
	 TO 城镇非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区城镇非农分龄.&FND2)/2)<>0		&&城镇非农生育率

*农业政策TFR,农业实现TFR0,农业实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区农业分龄.&FND1
SUM  (地区农业子女.政策生育)/((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2) TO 农业政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2)<>0			&&地区农业政策生育率
SUM  (地区农业子女.DDB+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2) ;
	 TO 农业实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2)<>0		&&农业实现生育率
SUM (地区农业子女.DDB+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)/地区农业子女.夫妇合计 ;
	 TO 农业实婚TFR0 FOR 地区农业子女.夫妇合计<>0.AND.(地区农业子女.DDB+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)>0

*地区非农TFR,非农实现TFR0,非农实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区非农分龄.&FND1
SUM  (地区非农子女.政策生育)/((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2) TO 非农政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2)<>0			&&地区非农政策生育率
SUM  (地区非农子女.DDB+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2) ;
	 TO 非农实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2)<>0		&&非农实现生育率
SUM  (地区非农子女.DDB+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)/(地区非农子女.夫妇合计) ;
	 TO 非农实婚TFR0 FOR 地区非农子女.夫妇合计<>0.AND.(地区非农子女.DDB+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)>0		&&非农实婚生育率

*农村TFR0,农村实现TFR0,农村实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 地区农村分龄.&FND1
SUM  地区农村子女.政策生育/((ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2)  TO 农村政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2<>0			&&农村政策生育率
SUM  (地区农村子女.DDB+地区农村子女.NMDFB+地区农村子女.NFDMB+地区农村子女.NNB+地区农村子女.单独释放B+地区农村子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2) TO 农村实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2<>0		&&农村可实现TFR
SUM  (地区农村子女.DDB+地区农村子女.NMDFB+地区农村子女.NFDMB+地区农村子女.NNB+地区农村子女.单独释放B+地区农村子女.双非释放B)/地区农村子女.夫妇合计  TO 农村实婚TFR0 FOR 地区农村子女.夫妇合计<>0.AND.地区农村生育.&BND2>0

*城镇TFR0,城镇实现TFR0,城镇实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇分龄.&FND1
SUM  地区城镇子女.政策生育/((ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2) TO 城镇政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2<>0			&&城镇政策生育率
SUM  (地区城镇子女.DDB+地区城镇子女.NMDFB+地区城镇子女.NFDMB+地区城镇子女.NNB+地区城镇子女.单独释放B+地区城镇子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2) TO 城镇实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2<>0		&&城镇实现生育率
SUM  (地区城镇子女.DDB+地区城镇子女.NMDFB+地区城镇子女.NFDMB+地区城镇子女.NNB+地区城镇子女.单独释放B+地区城镇子女.双非释放B)/(地区城镇农业子女.夫妇合计+地区城镇非农子女.夫妇合计) ;
	 TO 城镇实婚TFR0 FOR 地区城镇农业子女.夫妇合计+地区城镇非农子女.夫妇合计<>0.AND.(地区城镇生育.&BND2)>0		&&城镇实婚生育率

*地区政策TFR,地区实现TFR0,地区实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 地区分龄.&FND1
SUM  地区分龄政策生育.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地区分龄政策生育率
SUM  (地区子女.DDB+地区子女.NMDFB+地区子女.NFDMB+地区子女.NNB+地区子女.单独释放B+地区子女.双非释放B)/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地s区可实现TFR
SUM  (地区子女.DDB+地区子女.NMDFB+地区子女.NFDMB+地区子女.NNB+地区子女.单独释放B+地区子女.双非释放B)/地区子女.夫妇合计  TO 地区实婚TFR0 FOR (地区子女.夫妇合计)<>0.AND.地区生育.&BND2>0

SUM 地区分龄超生.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区超生TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)>0
SUM 地区城镇分龄.&MND2+地区城镇分龄.&FND2,地区农村分龄.&MND2+地区农村分龄.&FND2 TO CRK,XRK
CSH水平=CRK/(CRK+XRK)*100
SUM 地区非农分龄.&MND2+地区非农分龄.&FND2,地区农业分龄.&MND2+地区农业分龄.&FND2 TO FNRK,NYRK

SELE 65 &&地区农村子女?
	DO 各类婚配夫妇分年龄生育率
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农村婚内TFR FOR (DD+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 66 &&地区城镇子女
	DO 各类婚配夫妇分年龄生育率
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 城镇婚内TFR FOR (DD+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 67	&&地区农业子女
	DO 各类婚配夫妇分年龄生育率
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农业婚内TFR FOR (DD+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 68	&&地区非农子女
	DO 各类婚配夫妇分年龄生育率
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 非农婚内TFR FOR (DD+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 69	&&地区子女
	DO 各类婚配夫妇分年龄生育率
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 地区婚内TFR FOR (DD+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
RETURN 