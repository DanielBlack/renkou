PROCEDURE 分婚配模式按意愿生育的孩子数

FOR CX=1 TO 4
	
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女')))
	DO CASE 
	CASE  CX=1.OR.CX=3   &&农村
		NMDFR=N1D2N
		NFDMR=N2D1N
		NNR=NNFN
		城乡生育模式='农村生育模式'
	CASE  CX=2.OR.CX=4
		NMDFR=N1D2C
		NFDMR=N2D1C
		NNR=NNFC
		城乡生育模式='城镇生育模式'
	ENDCASE
	
	SELECT A
	REPLACE &子女..夫妇合计 WITH &子女..DD+&子女..NMDF+&子女..NFDM+&子女..NN+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非 ALL
	
	&& 相当于调用 procedure 各类夫妇分年龄生育孩子数，但乘上 想生二胎比 系数！
	DDFR1=IIF(DDFR<1,DDFR,1)
	NMDFFR1=IIF(NMDFR<1,NMDFR,1)
	NFDMFR1=IIF(NFDMR<1,NFDMR,1)
	NNFR1=IIF(NNR<1,NNR,1)
	DDFR2=IIF(DDFR<1,0,DDFR-1)
	NMDFFR2=IIF(NMDFR<1,0,NMDFR-1)
	NFDMFR2=IIF(NFDMR<1,0,NFDMR-1)
	NNFR2=IIF(NNR<1,0,NNR-1)
	REPL &子女..DDB WITH &子女..DD*DDFR1*&子女..FR1+&子女..DD*DDFR2*&子女..FR2*想生二胎比.&地带1    ALL  			&&双独及按双独及其所生子女和享受同等政策的其他婚配模式所生子女，城乡无差异
	REPL &子女..NNB WITH &子女..NN*NNFR1*&子女..FR1+&子女..NN*NNFR2*&子女..FR2*想生二胎比.&地带1 	ALL				&&双非分一、二孩。一孩:NN*FR1(=NN*1*FR1)，二孩:(NNR-1)*TF2
	REPL &子女..NMDFB WITH  &子女..NMDF*NMDFFR1*&子女..FR1+&子女..NMDF*NMDFFR2*&子女..FR2*想生二胎比.&地带1  ALL		&&独女分一、二孩
	REPL &子女..NFDMB WITH  &子女..NFDM*NFDMFR1*&子女..FR1+&子女..NFDM*NFDMFR2*&子女..FR2*想生二胎比.&地带1  ALL		&&独男分一、二孩
	SUM  &子女..FRD,&子女..DDB,&子女..NMDFB,&子女..NFDMB,&子女..NNB TO A,DB,B2,B3,B4          	&&总和生育率及各婚配模式生育的孩子数(含第一个与第二个)
	SUM  &子女..DD*DDFR1*&子女..FR1,&子女..NMDF*NMDFFR1*&子女..FR1,&子女..NFDM*NFDMFR1*&子女..FR1,&子女..NN*NNFR1*&子女..FR1 TO DD1,NMDF1,NFDM1,NN1    &&农村\城镇婚配各模式生育的第一个孩子数
	SUM  &子女..DD*DDFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NMDF*NMDFFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NFDM*NFDMFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NN*NNFR2*&子女..FR2*想生二胎比.&地带1  TO DD2,NMDF2,NFDM2,NN2	&&农村\城镇婚配各模式生育的第二个孩子数
	***********************
	
	
	REPL &子女..超生DDB WITH &子女..DDB-&子女..政策DDB,&子女..超生NNB WITH &子女..NNB-&子女..政策NNB,&子女..超生NMDFB WITH &子女..NMDFB-&子女..政策NMDFB,;
		 &子女..超生NFDMB WITH &子女..NFDMB-&子女..政策NFDMB,&子女..超生释放B WITH &子女..单独释放B+&子女..双非释放B-&子女..政策释放B,;
		 &子女..超生生育 WITH &子女..超生DDB+&子女..超生NNB+&子女..超生NMDFB+&子女..超生NFDMB+&子女..超生释放B ALL 
	
	IF CX=4
		SELECT A
		REPLACE 地区农村超生.&BND2 WITH  地区农村非农子女.超生生育+地区农村农业子女.超生生育 ALL
		REPLACE 地区城镇超生.&BND2 WITH  地区城镇非农子女.超生生育+地区城镇农业子女.超生生育 ALL
		REPLACE 地区农业超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
		REPLACE 地区非农超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育 ALL
		REPLACE 地区分龄超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育+地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
   		    REPL 地区农业子女.超生生育 WITH 地区农村农业子女.超生生育+地区城镇农业子女.超生生育,;
		 地区非农子女.超生生育 WITH 地区农村非农子女.超生生育+地区城镇非农子女.超生生育,;
		 地区城镇子女.超生生育 WITH 地区城镇农业子女.超生生育+地区城镇非农子女.超生生育,;
		 地区农村子女.超生生育 WITH 地区农村农业子女.超生生育+地区农村非农子女.超生生育,;
		 地区子女.超生生育 WITH 地区农业子女.超生生育+地区非农子女.超生生育 ALL
		SUM 地区分龄超生.&BND2 TO 超生S
		SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	ENDIF
	******分释放模式堆积释放计算*****
	 AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
	 AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
	 AR突释模式=IIF(CX=1.OR.CX=3, 'AR农业突释模式','AR非农突释模式')
	 SUM 单独堆积+双非堆积  TO S堆积  FOR X<=49
	 IF S堆积<>0.and.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		 SELECT A
		 DO 不同政策实现程度下的堆积生育释放法
	 ENDIF
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类婚配夫妇分年龄生育率
	 DO 各类堆积释放数及后代分类
	 
ENDFOR
RETURN