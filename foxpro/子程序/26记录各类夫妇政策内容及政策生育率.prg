PROCEDURE 记录各类夫妇政策内容及政策生育率
SELE 151		
LOCA FOR 年份=ND0
REPL 地区政策生育率.非农政策1 WITH ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4))),;
	 地区政策生育率.农业政策1 WITH ALLT(农业ZC1)-IIF(农业时机1=0,'',ALLT(STR(农业时机1,4))),;
	 地区政策生育率.非农政策2 WITH ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))),;
	 地区政策生育率.农业政策2 WITH ALLT(农业ZC2)-IIF(农业时机2=0,'',ALLT(STR(农业时机2,4))),;
	 地区政策生育率.非农政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(非农ZC3)-ALLT(STR(非农时机3,4)),''),;
	 地区政策生育率.农业政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(农业ZC3)-ALLT(STR(农业时机3,4)),'')
REPL 地区政策生育率.非农双独 WITH DDFR,地区政策生育率.农业双独 WITH DDFR,地区政策生育率.非农单独 WITH N2D1C,地区政策生育率.农业单独 WITH N2D1N,;
	地区政策生育率.非农女单 WITH N1D2C,地区政策生育率.农业女单 WITH N1D2N,地区政策生育率.非农男单 WITH N2D1C,地区政策生育率.农业男单 WITH N2D1N,;
	地区政策生育率.非农双非 WITH NNFC,地区政策生育率.农业双非 WITH NNFN,;
	地区政策生育率.农村NYTFR WITH 农村农业TFR,地区政策生育率.农村FNTFR WITH 农村非农TFR,地区政策生育率.农村TFR WITH 农村政策TFR0,;
	地区政策生育率.城镇NYTFR WITH 城镇农业TFR,地区政策生育率.城镇FNTFR WITH 城镇非农TFR,地区政策生育率.城镇TFR WITH 城镇政策TFR0,;
	地区政策生育率.地区NYTFR WITH 农业政策TFR0,地区政策生育率.地区FNTFR WITH 非农政策TFR0,地区政策生育率.地区TFR WITH 地区政策TFR0,;
	地区政策生育率.NCNY婚TFR WITH NCNY婚内TFR,地区政策生育率.NCFN婚TFR WITH NCFN婚内TFR,地区政策生育率.NC婚TFR WITH 农村婚内TFR,;
	地区政策生育率.CZNY婚TFR WITH CZNY婚内TFR,地区政策生育率.CZFN婚TFR WITH CZFN婚内TFR,地区政策生育率.CZ婚TFR WITH 城镇婚内TFR,;
	地区政策生育率.QSNY婚TFR WITH 农业婚内TFR,地区政策生育率.QSFN婚TFR WITH 非农婚内TFR,地区政策生育率.QS婚TFR WITH 地区婚内TFR
FOR FI1=20 TO 86	&&FCOUN()
	FIE1=FIELD(FI1)
	REPL 地区农村夫妇.&FIE1 WITH 地区农村农业夫妇.&FIE1+地区农村非农夫妇.&FIE1,地区城镇夫妇.&FIE1 WITH 地区城镇农业夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;
		 地区农业夫妇.&FIE1 WITH 地区农村农业夫妇.&FIE1+地区城镇农业夫妇.&FIE1,地区非农夫妇.&FIE1 WITH 地区农村非农夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;
		 地区夫妇.&FIE1 WITH 地区农村夫妇.&FIE1+地区城镇夫妇.&FIE1 FOR 年份=ND0
ENDFOR

RETURN 