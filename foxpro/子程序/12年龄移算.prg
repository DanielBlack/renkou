PROCEDURE 年龄移算
************农村\城镇分独生子女和非独生子女年龄移算***************
*****子女年龄移算******
FOR DN=1  TO  4  &&循环各种字段名  D独生，N非独，DDB双独后代,HJ合计
 	RELE NCNY,NCFN,CZNY,CZFN
 	
 	&&形成字段名
 	FIEM=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DDB','HJ')))-'M'  && HJ:合计
 	FIEF=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DDB','HJ')))-'F'
 	
 	&&先拷贝到数组——其实没必要，反过来算就可以了=。=
 	COPY TO ARRA NCNY FIEL 地区农村农业子女.&FIEM,地区农村农业子女.&FIEF  && NCNY:农村农业
 	COPY TO ARRA NCFN FIEL 地区农村非农子女.&FIEM,地区农村非农子女.&FIEF
 	COPY TO ARRA CZNY FIEL 地区城镇农业子女.&FIEM,地区城镇农业子女.&FIEF
 	COPY TO ARRA CZFN FIEL 地区城镇非农子女.&FIEM,地区城镇非农子女.&FIEF

 	&&年龄移算
 	REPL 地区农村农业子女.&FIEM WITH NCNY(RECN()-1,1)*(1-NYQM(RECN()-1)),  && NYQM->农村
 		 地区农村农业子女.&FIEF WITH NCNY(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
 		 
 	REPL 地区农村非农子女.&FIEM WITH NCFN(RECN()-1,1)*(1-FNQM(RECN()-1)),  && FNQM->城镇
 		 地区农村非农子女.&FIEF WITH NCFN(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
 		 
 	REPL 地区城镇农业子女.&FIEM WITH CZNY(RECN()-1,1)*(1-NYQM(RECN()-1)), 
 		 地区城镇农业子女.&FIEF WITH CZNY(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
 		 
 	REPL 地区城镇非农子女.&FIEM WITH CZFN(RECN()-1,1)*(1-FNQM(RECN()-1)), 
 		 地区城镇非农子女.&FIEF WITH CZFN(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
 	
 	&& 0 岁组人数为0
 	REPL 地区农村农业子女.&FIEM WITH 0, 
 		 地区农村农业子女.&FIEF WITH 0,
 		 地区农村非农子女.&FIEM WITH 0, 
 		 地区农村非农子女.&FIEF WITH 0 ,;
 	     地区城镇农业子女.&FIEM WITH 0, 
 	     地区城镇农业子女.&FIEF WITH 0,
 	     地区城镇非农子女.&FIEM WITH 0, 
 	     地区城镇非农子女.&FIEF WITH 0 FOR RECN()=1
 	
 	&&合计
 	REPL 地区农业子女.&FIEM WITH 地区农村农业子女.&FIEM+地区城镇农业子女.&FIEM,
 		 地区农业子女.&FIEF WITH 地区农村农业子女.&FIEF+地区城镇农业子女.&FIEF,;
 		 地区非农子女.&FIEM WITH 地区农村非农子女.&FIEM+地区城镇非农子女.&FIEM,
 		 地区非农子女.&FIEF WITH 地区农村非农子女.&FIEF+地区城镇非农子女.&FIEF ALL
 	REPL 地区子女.&FIEM WITH 地区农业子女.&FIEM+地区非农子女.&FIEM,
 		 地区子女.&FIEF WITH 地区农业子女.&FIEF+地区非农子女.&FIEF ALL
 	
 	&&&不需要合计 城镇 和 农村 子女—— 在  子程序：分龄人口合计 中做
ENDFOR



FOR CX=1 TO 2 &&城乡循环：堆积生育特殊人群的年龄移算
	SELE A	
	农业子女='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	非农子女='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	*****可二后年龄移算******
 	RELE DJM
 	&& 堆积丈夫 来自后面的计算，不是来自分省迁移初算！！！故在初次运行，第一年，这个字段应该都是为0
 	COPY TO ARRA DJM  FIEL &农业子女..堆积丈夫,&非农子女..堆积丈夫
 	&& 堆积丈夫先死一部分！ 0 岁的数量肯定为0
 	REPL &农业子女..堆积丈夫 WITH DJM(RECN()-1,1)*(1-NYQM(RECN()-1)),  && NYQM-农村
 		 &非农子女..堆积丈夫 WITH DJM(RECN()-1,2)*(1-FNQM(RECN()-1)) FOR RECN()>1  &&FYQM-城镇
 		 
	*****单独\双非堆积年龄移算，单独\双非堆积年龄移算,因按女性年龄和人数计算堆积人群的生育数和生育率,故按女性死亡概率进行年龄移算*****
	FOR DJ=1  TO  9	&&堆积字段
	 	堆积FI1=IIF(DJ=1,'男单堆积',IIF(DJ=2,'已2男单',IIF(DJ=3,'女单堆积',IIF(DJ=4,'已2女单',IIF(DJ=5,'双非堆积',IIF(DJ=6,'已2双非',IIF(DJ=7,'单独堆积15',IIF(DJ=8,'单独堆积30','单独堆积35'))))))))
		RELE NCNY,NCFN
	 	COPY TO ARRA NCNY FIEL &农业子女..&堆积FI1
	 	COPY TO ARRA NCFN FIEL &非农子女..&堆积FI1
	 	REPL &农业子女..&堆积FI1 WITH NCNY(RECN()-1)*(1-NYQF(RECN()-1)) FOR RECN()>1  && 为什么用 女性 的死亡概率——生育率、堆积 都用女性的参数
	 	REPL &非农子女..&堆积FI1 WITH NCFN(RECN()-1)*(1-FNQF(RECN()-1)) FOR RECN()>1
	ENDFOR
	
	REPL &农业子女..单独堆积 WITH &农业子女..女单堆积+&农业子女..男单堆积,
		 &农业子女..已2单独 WITH &农业子女..已2男单+&农业子女..已2女单 ALL
	REPL &非农子女..单独堆积 WITH &非农子女..女单堆积+&非农子女..男单堆积,
		 &非农子女..已2单独 WITH &非农子女..已2男单+&非农子女..已2女单 ALL
ENDFOR


&& 移算完以后，把迁移的人数加上
*********************
SELECT A
FOR SE=61 TO 64  && 地区农村农业子女，地区农村非农子女，地区城镇农业子女，地区城镇非农子女
	*SELECT IIF(SE=SE,SE,1)  && 这个判断的意义是  select (SE)
	SELECT SE 
	REPLACE DM WITH DM+DQYM FOR DM+DQYM>0  && ？要是 不大于 零 难道就不变？---迁出的人一定不大于现有人口数 ——可以尝试不写 For....
	REPLACE NM WITH NM+NQYM FOR NM+NQYM>0
	REPLACE DDBM WITH DDBM+DDBQYM FOR DDBM+DDBQYM>0
	
	REPLACE DF WITH DF+DQYF FOR DF+DQYF>0
	REPLACE NF WITH NF+NQYF FOR NF+NQYF>0
	REPLACE DDBF WITH DDBF+DDBQYF FOR DDBF+DDBQYF>0
ENDFOR

FOR SE=61 TO 69  && *，*，*，*，地区城镇子女，地区农村子女，地区农业子女，地区非农子女，地区子女
&& 地区城镇子女，地区农村子女 都是0 ？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？——未知答案，先按照这个翻译
	SELECT IIF(SE=SE,SE,1)
 	REPLACE DQY WITH DQYF+DQYM,
 			NQY WITH NQYF+NQYM,
 			DDBQYS WITH DDBQYM+DDBQYF,
 			HJQYF WITH DQYF+NQYF+DDBQYF,
 			HJQYM WITH DQYM+NQYM+DDBQYM,
 			HJQY WITH HJQYF+HJQYM ALL
ENDFOR

*TND2='T'+STR(ND0,4)    &&当年
*MND2='M'+STR(ND0,4)
*FND2='F'+STR(ND0,4)
&& 迁移人口统计
SELECT A
REPL 地区城镇迁移.&MND2 WITH 地区城镇非农子女.DQYM+地区城镇非农子女.NQYM+地区城镇非农子女.DDBQYM+地区城镇农业子女.DQYM+地区城镇农业子女.NQYM+地区城镇农业子女.DDBQYM,;
	 地区城镇迁移.&FND2 WITH 地区城镇非农子女.DQYF+地区城镇非农子女.NQYF+地区城镇非农子女.DDBQYF+地区城镇农业子女.DQYF+地区城镇农业子女.NQYF+地区城镇农业子女.DDBQYF ALL
REPL 地区农村迁移.&MND2 WITH 地区农村非农子女.DQYM+地区农村非农子女.NQYM+地区农村非农子女.DDBQYM+地区农村农业子女.DQYM+地区农村农业子女.NQYM+地区农村农业子女.DDBQYM,;
	 地区农村迁移.&FND2 WITH 地区农村非农子女.DQYF+地区农村非农子女.NQYF+地区农村非农子女.DDBQYF+地区农村农业子女.DQYF+地区农村农业子女.NQYF+地区农村农业子女.DDBQYF ALL
REPL 地区迁移.&MND2 WITH 地区城镇迁移.&MND2+地区农村迁移.&MND2,
	 地区迁移.&FND2 WITH 地区城镇迁移.&FND2+地区农村迁移.&FND2 ALL

&& 独生子女夭折的父母 -特扶群体的年龄移算
*****一孩父母和特扶父母年龄移算***************
FOR FM=1 TO 2
	FOR CX=1 TO 4
		&& 地区农村(城镇)农业(非农)父母(特扶)
		&& 地区农村农业父母
		ALI2=IIF(CX=1,'地区农村农业',IIF(CX=2,'地区城镇农业',IIF(CX=3,'地区农村非农','地区城镇非农')))-IIF(FM=1,'父母','特扶')
		&& TND1='T'+STR(ND0-1,4)  && 上一年  ND:年度
		&& MND1='M'+STR(ND0-1,4)
		&& FND1='F'+STR(ND0-1,4) 
		COPY TO ARRAY ARFND1 FIEL &ALI2..&FND1
		COPY TO ARRAY ARMND1 FIEL &ALI2..&MND1
		IF CX<=2 && 农业 
	 		REPL &ALI2..&MND2 WITH ARMND1(RECN()-1,1)*(1-NYQM(RECN()-1)), 
	 		     &ALI2..&FND2 WITH ARFND1(RECN()-1,1)*(1-NYQF(RECN()-1)) FOR RECN()>1
	 	ELSE   && 非农
	 		REPL &ALI2..&MND2 WITH ARMND1(RECN()-1,1)*(1-FNQM(RECN()-1)), 
	 		     &ALI2..&FND2 WITH ARFND1(RECN()-1,1)*(1-FNQF(RECN()-1)) FOR RECN()>1
	 	ENDIF
	ENDFOR
ENDFOR


RETURN 
