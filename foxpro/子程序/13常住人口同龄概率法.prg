PROC 常住人口同龄概率法
FOR CX=1 TO 4 
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
	DO CASE
	CASE 	结婚率法='全员婚配'
		REPL &子女..DF0 WITH &子女..DF+&子女..DDBF,&子女..DM0 WITH &子女..DM+&子女..DDBM,&子女..NF0 WITH &子女..NF,&子女..NM0 WITH &子女..NM ALL
	CASE 	结婚率法='有偶率婚配'
		有偶率=IIF(农非=1,'乡村','城镇')-'有偶率'
		REPL &子女..DF0 WITH (&子女..DF+&子女..DDBF)*&有偶率..F有偶B,&子女..DM0 WITH (&子女..DM+&子女..DDBM)*&有偶率..M有偶B,&子女..NF0 WITH &子女..NF*&有偶率..F有偶B,&子女..NM0 WITH &子女..NM*&有偶率..M有偶B ALL
	ENDCASE
	REPL &子女..DF0 WITH 0 FOR &子女..DF0<0
	REPL &子女..DM0 WITH 0 FOR &子女..DM0<0
	REPL &子女..NF0 WITH 0 FOR &子女..NF0<0
	REPL &子女..NM0 WITH 0 FOR &子女..NM0<0
	RELE AR1
	COPY TO ARRA AR1 FIEL &子女..DF0,&子女..NF0
	REPL &子女..双独概率M WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..女单概率M WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..男单概率M WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..双非概率M WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	RELE AR1
	COPY TO ARRA AR1 FIEL &子女..DM0,&子女..NM0
	REPL &子女..双独概率F WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..男单概率F WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..女单概率F WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..双非概率F WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..DD   WITH MIN(&子女..DM0*&子女..双独概率M,&子女..DF0*&子女..双独概率F)  ALL
	REPL &子女..NMDF WITH MIN(&子女..NM0*&子女..女单概率M,&子女..DF0*&子女..女单概率F)  ALL
	REPL &子女..NFDM WITH MIN(&子女..DM0*&子女..男单概率M,&子女..NF0*&子女..男单概率F) ALL
	REPL &子女..NN   WITH MIN(&子女..NM0*&子女..双非概率M,&子女..NF0*&子女..双非概率F)  ALL
	*****对同龄概率可以广使用如下语句，因为同年龄同性别应该平衡*****
	REPL &子女..DF10 WITH &子女..DF0-&子女..DD-&子女..NMDF ALL
	REPL &子女..DM10 WITH &子女..DM0-&子女..DD-&子女..NFDM ALL
	REPL &子女..NF10 WITH &子女..NF0-&子女..NN-&子女..NFDM ALL
	REPL &子女..NM10 WITH &子女..NM0-&子女..NN-&子女..NMDF ALL
	***对剩余女性为负数的进行调整
	RELE ARDF,ARNF
	COPY TO ARRA ARDF FIEL &子女..DD,&子女..NMDF
	REPL &子女..DD WITH &子女..DF0*(ARDF(RECN(),1)/(ARDF(RECN(),1)+ARDF(RECN(),2))),&子女..NMDF WITH &子女..DF0*(ARDF(RECN(),2)/(ARDF(RECN(),1)+ARDF(RECN(),2))) FOR &子女..DF0-(ARDF(RECN(),1)+ARDF(RECN(),2))<0
	COPY TO ARRA ARNF FIEL &子女..NN,&子女..NFDM
	REPL &子女..NN WITH &子女..NF0*(ARNF(RECN(),1)/(ARNF(RECN(),1)+ARNF(RECN(),2))),&子女..NFDM WITH &子女..NF0*(ARNF(RECN(),2)/(ARNF(RECN(),1)+ARNF(RECN(),2))) FOR &子女..NF0-(ARNF(RECN(),1)+ARNF(RECN(),2))<0
	***对剩余男性为负数的进行调整
	RELE ARDM,ARNM
	COPY TO ARRA ARDM FIEL &子女..DD,&子女..NFDM
	REPL &子女..DD WITH &子女..DM0*(ARDM(RECN(),1)/(ARDM(RECN(),1)+ARDM(RECN(),2))),&子女..NFDM WITH &子女..DM0*(ARDM(RECN(),2)/(ARDM(RECN(),1)+ARDM(RECN(),2))) FOR &子女..DM0-(ARDM(RECN(),1)+ARDM(RECN(),2))<0
	COPY TO ARRA ARNM FIEL &子女..NN,&子女..NMDF
	REPL &子女..NN WITH &子女..NM0*(ARNM(RECN(),1)/(ARNM(RECN(),1)+ARNM(RECN(),2))),&子女..NMDF WITH &子女..NM0*(ARNM(RECN(),2)/(ARNM(RECN(),1)+ARNM(RECN(),2))) FOR &子女..NM0-(ARNM(RECN(),1)+ARNM(RECN(),2))<0
	*****记录平均初婚年龄的各种婚配概率:双独概率M,双独概率F,男单概率M,男单概率F,女单概率M,女单概率F
	DO CASE	
		&&AR初婚年龄 FIEL 地区,城市M,城市F,乡村M,乡村F,
		&&子女=IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		CASE CX=1	&&CX=1,'农村农业'
			LOCA FOR X=ROUN(X初婚NLM,0)
			乡NY双独GLM=&子女..双独概率M
			乡NY男单GLM=&子女..男单概率M
			乡NY女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(X初婚NLF,0)
			乡NY双独GLF=&子女..双独概率F
			乡NY男单GLF=&子女..男单概率F
			乡NY女单GLF=&子女..女单概率F
		CASE CX=2	&&CX=2,'农村非农'
			LOCA FOR X=ROUN(C初婚NLM,0)
			乡FN双独GLM=&子女..双独概率M
			乡FN男单GLM=&子女..男单概率M
			乡FN女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(C初婚NLF,0)
			乡FN双独GLF=&子女..双独概率F
			乡FN男单GLF=&子女..男单概率F
			乡FN女单GLF=&子女..女单概率F
		CASE CX=3	&&IIF(CX=3,'城镇农业'
			LOCA FOR X=ROUN(X初婚NLM,0)
			城NY双独GLM=&子女..双独概率M
			城NY男单GLM=&子女..男单概率M
			城NY女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(X初婚NLF,0)
			城NY双独GLF=&子女..双独概率F
			城NY男单GLF=&子女..男单概率F
			城NY女单GLF=&子女..女单概率F
		CASE CX=4	&&(CX=4,'城镇非农'
			LOCA FOR X=ROUN(C初婚NLM,0)
			城FN双独GLM=&子女..双独概率M
			城FN男单GLM=&子女..男单概率M
			城FN女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(C初婚NLF,0)
			城FN双独GLF=&子女..双独概率F
			城FN男单GLF=&子女..男单概率F
			城FN女单GLF=&子女..女单概率F
	ENDCASE
ENDFOR
*********************************
*	农村农业与非农业子女婚配	*
*	城镇农业与非农业子女婚配	*
*********************************
SELE A
FOR CX=1 TO 2
	非农子女='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	农业子女='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	***** 1.非农独男、农业独女
	RELE AR农业F,AR非农M,AR非农F,AR农业M
	COPY TO ARRA AR农业F FIEL &农业子女..DF10,&农业子女..NF10
	COPY TO ARRA AR非农M FIEL &非农子女..DM10,&非农子女..NM10
	COPY TO ARRA AR非农F FIEL &非农子女..DF10,&非农子女..NF10
	COPY TO ARRA AR农业M FIEL &农业子女..DM10,&农业子女..NM10

	REPL &农业子女..NF双独GLF WITH AR非农M(RECN(),1)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &农业子女..NF双独GLM WITH AR非农F(RECN(),1)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &非农子女..NF双独GLF WITH AR农业M(RECN(),1)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0
	REPL &非农子女..NF双独GLM WITH AR农业F(RECN(),1)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &农业子女..DD0  WITH  MIN(&非农子女..DM10*&非农子女..NF双独GLM,&农业子女..DF10*&农业子女..NF双独GLF) ALL		&&1.非农独男、农业独女
	REPL &非农子女..DD0  WITH  MIN(&农业子女..DM10*&农业子女..NF双独GLM,&非农子女..DF10*&非农子女..NF双独GLF) ALL		&&2.非农独女、农业独男
	REPL &农业子女..DD   WITH  &农业子女..DD+&农业子女..DD0 ALL
	REPL &非农子女..DD   WITH  &非农子女..DD+&非农子女..DD0 ALL

	REPL &农业子女..NF女单GLM WITH AR非农F(RECN(),1)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &农业子女..NF男单GLM WITH AR非农F(RECN(),2)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &非农子女..NF女单GLM WITH AR农业F(RECN(),1)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &非农子女..NF男单GLM WITH AR农业F(RECN(),2)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0

	REPL &农业子女..NF女单GLF WITH AR非农M(RECN(),1)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &农业子女..NF男单GLF WITH AR非农M(RECN(),2)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &非农子女..NF女单GLF WITH AR农业M(RECN(),1)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0
	REPL &非农子女..NF男单GLF WITH AR农业M(RECN(),2)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0

	REPL &农业子女..NFDM WITH &农业子女..NFDM+MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL		&&非农独男、农业非独女
	REPL &非农子女..NMDF WITH &非农子女..NMDF+MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM) ALL		&&非农独女、农业非独男
	REPL &农业子女..NMDF WITH &农业子女..NMDF+MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM) ALL		&&非农非独男、农业独女
	REPL &非农子女..NFDM WITH &非农子女..NFDM+MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL		&&非农非独女、农业独男

	REPL &农业子女..NF双非GLM WITH AR非农F(RECN(),2)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &农业子女..NF双非GLF WITH AR非农M(RECN(),2)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &非农子女..NF双非GLM WITH AR农业F(RECN(),2)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &非农子女..NF双非GLF WITH AR农业M(RECN(),2)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0

	REPL  &农业子女..NN   WITH  &农业子女..NN+MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)  ALL		&&非农非独男、农业非独女
	REPL  &非农子女..NN   WITH  &非农子女..NN+MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF)  ALL		&&农业非独男、非农非独女
	*****农业和非农业夫妇后未婚*****
	REPL &农业子女..DF20 WITH &农业子女..DF10-&农业子女..DD0-MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM) ALL
	REPL &农业子女..DM20 WITH &农业子女..DM10-&非农子女..DD0-MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL
	REPL &农业子女..NF20 WITH &农业子女..NF10-MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)-MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL
	REPL &农业子女..NM20 WITH &农业子女..NM10-MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM)-MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF) ALL

	****
	REPL &非农子女..DM20 WITH &非农子女..DM10-&农业子女..DD0-MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL
	REPL &非农子女..DF20 WITH &非农子女..DF10-&非农子女..DD0-MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM) ALL
	REPL &非农子女..NF20 WITH &非农子女..NF10-MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF)-MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL
	REPL &非农子女..NM20 WITH &非农子女..NM10-MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)-MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM)  ALL

	SUM  &农业子女..DF20,&农业子女..DM20,&农业子女..NF20,&农业子女..NM20,&非农子女..DF20,&非农子女..DM20,&非农子女..NF20,&非农子女..NM20 ;
		TO NY未婚DF,NY未婚DM,NY未婚NF,NY未婚NM,FN未婚DF,FN未婚DM,FN未婚NF,FN未婚NM FOR X>=30.AND.X<=49
	*****************************
	REPL &农业子女..NMDF WITH 0 FOR &农业子女..NMDF<0
	REPL &农业子女..NFDM WITH 0 FOR &农业子女..NFDM<0				
	REPL &非农子女..NFDM WITH 0 FOR &非农子女..NFDM<0
	REPL &非农子女..NMDF WITH 0 FOR &非农子女..NMDF<0
ENDFOR
RETU