&& 计算 分年龄生育率
PROCEDURE 各类婚配夫妇分年龄生育率  && 公用代码段
	REPL 合J分龄SYL WITH (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/HJF FOR HJF<>0 &&所有婚配模式合计的分年龄生育率
	REPL 双D分龄SYL WITH DDB/DD FOR DD<>0						&&双独生育率
	REPL 双N分龄SYL WITH (NNB+双非释放B)/(NN+双非堆积+已2双非) FOR (NN+双非堆积+已2双非)<>0							&&单独生育率
	REPL 单D分龄SYL WITH (NMDFB+NFDMB+单独释放B)/(NMDF+NFDM+单独堆积+已2单独) FOR (NMDF+NFDM+单独堆积+已2单独)<>0	&&双非生育率
SUM 合J分龄SYL,双D分龄SYL,双N分龄SYL,单D分龄SYL TO 合JTFR,双DTFR,双NTFR,单DTFR

RETURN 