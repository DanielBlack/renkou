PROC 女性最大多龄概率法  && 


&& 第一阶段、农村和城镇各自的农业、非农优先婚配
*****************************
*	农村农业与农村农业婚配	*
*	农村非农与农村非农婚配	*
*	城镇农业与城镇农业婚配	*
*	城镇非农与城镇非农婚配	*
*****************************
FOR CX=1 TO 4    &&
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',
				IIF(CX=2,'农村非农',
				IIF(CX=3,'城镇农业',
						 '城镇非农')))-'子女'
	
	&& 1. 准备婚配的基础数据
	DO CASE
	CASE 	结婚率法='全员婚配'  
	
		&& DF0:婚配前的数据
		REPL &子女..DF0 WITH &子女..DF+&子女..DDBF,
			 &子女..DM0 WITH &子女..DM+&子女..DDBM,
			 &子女..NF0 WITH &子女..NF,
			 &子女..NM0 WITH &子女..NM ALL
			 
		&& 垃圾代码！好像用不到啊！！！！	 
		REPL &子女..HJF WITH &子女..DF0+&子女..NF0,   
			 &子女..HJM WITH &子女..DM0+&子女..NM0,
			 &子女..HJ WITH &子女..HJF+&子女..HJM ALL
			 
	CASE 	结婚率法='有偶率婚配' &&可以先不翻译
		有偶率=IIF(农非=1,'乡村','城镇')-'有偶率'
		REPL &子女..DF0 WITH (&子女..DF+&子女..DDBF) * &有偶率..F有偶B,
			 &子女..DM0 WITH (&子女..DM+&子女..DDBM) * &有偶率..M有偶B,
			 &子女..NF0 WITH &子女..NF*&有偶率..F有偶B,
			 &子女..NM0 WITH &子女..NM*&有偶率..M有偶B ALL
			 
		REPL &子女..HJF0 WITH &子女..DF0+&子女..NF0,
			 &子女..HJM0 WITH &子女..DM0+&子女..NM0 ALL
			 
	ENDCASE
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	&&	此处将“&子女..DF”改为“&子女..DF+&子女..DDBF”，将“&子女..DM”改为“&子女..DM+&子女..DDBM”与删除下句相配合：
	&&	*REPL D WITH D+DDBS,DM WITH DM+DDBM,DF WITH DF+DDBF FOR X=0 &&DDBS他们之间婚配可以象双独夫妇一样生育二个孩子的人数（只对零岁组操作）
	&&	删除后一个语句，是为了将可以象双独婚配允许生育二个孩子的后代（不一定是独生子女）与本人是独生子女的人数分开，以便于计算“四二一”家庭数量
	&&  修改前一个语句，是可以简化这部份人的婚配计算。如不将“&子女..DF”改为“&子女..DF+&子女..DDBF”，“&子女..DM”改为“&子女..DM+&子女..DDBM”，就不该
	&&	删除“REPL D WITH D+DDBS,……”。
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	&& 不允许出现负数（多判断一次）
	REPL &子女..DF0 WITH 0 FOR &子女..DF<0
	REPL &子女..DM0 WITH 0 FOR &子女..DM<0
	REPL &子女..NF0 WITH 0 FOR &子女..NF<0
	REPL &子女..NM0 WITH 0 FOR &子女..NM<0
	
	
	
	******从女性求夫角度考察的婚配
	
	&& 全国按妻子年龄分的丈夫年龄分布模式（修匀）
	REPL 需夫模式.DD夫 WITH 0,
		 需夫模式.NMDF夫 WITH 0,
		 需夫模式.NFDM夫 WITH 0,
		 需夫模式.NN夫 WITH 0 ALL
	
	FOR NL=15 TO 64
		LOCA FOR X=NL
		
	   &&计算局部变量
		求夫DF=&子女..DF0
		求夫NF=&子女..NF0
		夫FB='妻子'-allt(STR(NL,3))
		&& 找出分布不为0的X的上下限——是否可以直接去 Max1=110 minx1=15？？可以，没有问题
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0 && 夫FB 跟着 NL 变化
		SUM &子女..DM0+&子女..NM0 TO SM  &&应该可以放在NL循环外面计算？——可能可以，还是判断刚才的问题，我晕！！
		
		IF SM<>0  && DD夫等字段都是在原来基础上新增的字段，可相当于4个临时数组，丈夫年龄为索引
		
			                               && 累加        概率  *    （） ；判断条件
			REPL 需夫模式.DD夫 		WITH  需夫模式.DD夫 + 求夫DF* ( &子女..DM0 / (&子女..DM0+&子女..NM0) * 需夫模式.&夫FB ) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) 			FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) 			FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB)				FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0

			
			SUM 求夫DF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			SUM 求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			
			&& XXXX_F 好像是 冗余的字段，可以直接存到 DD中？ 
			REPLACE &子女..DD_F WITH HDFDM,
					&子女..NMDF_F WITH HNMDF,
					&子女..NFDM_F WITH HNFDM,
					&子女..NN_F  WITH HNN FOR X=NL
		ENDIF
	ENDFOR
	
	REPLACE &子女..DD WITH &子女..DD_F,
			&子女..NMDF WITH &子女..NMDF_F,
			&子女..NFDM WITH &子女..NFDM_F,
			&子女..NN WITH &子女..NN_F ALL
			
	*****计算剩男剩女\各类未婚人数******
	REPLACE &子女..DF10 WITH &子女..DF0-&子女..DD-&子女..NMDF,
			&子女..NF10 WITH &子女..NF0-&子女..NN-&子女..NFDM,
			&子女..DM10 WITH &子女..DM0-需夫模式.DD夫-需夫模式.NFDM夫,
			&子女..NM10 WITH &子女..NM0-需夫模式.NN夫-需夫模式.NMDF夫 ALL
	
ENDFOR  && 城乡循环结束


*****************************************************************************************************************************	
*	以上程序段逻辑检验：
*	SUM（&子女..DD+&子女..NMDF+&子女..NFDM+&子女..NN-需夫模式.DD丈夫-需夫模式.NMDF夫-需夫模式.NFDM夫-需夫模式.NN夫）=0
*	各年龄：&子女..DF0-&子女..DD-&子女..NMDF>0,&子女..NF0-&子女..NN-&子女..NFDM>0
*****************************************************************************************************************************	


&& 第二阶段、剩余的，农村中农业和非农婚配，城镇中农业与非农婚配
*********************************
*	农村农业与非农业子女婚配	*
*	城镇农业与非农业子女婚配	*
*********************************
SELE A
FOR CX=1 TO 2
	非农='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	农业='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	*************************
	*	非农女与农业男婚配	*
	*************************
	SELE A
	******从女性求夫角度考察的婚配
	
	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 	WITH 0,需夫模式.NN夫 	WITH 0 ALL
	
	FOR NL=15 TO 64
		LOCA FOR X=NL
		求夫DF=&非农..DF10
		求夫NF=&非农..NF10
		夫FB='妻子'-allt(STR(NL,3))  && FB:分布
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
		
		&&将上次婚配剩余的总数加起来
		SUM &农业..DM10+&农业..NM10 TO SM
		
		IF SM<>0
			&& 10：第一次婚配剩余   20：第二次婚配剩余 30：第三次婚配剩余(没用上)
			REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0

			SUM 求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			SUM 求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			
			REPLACE &非农..DD非F_F   WITH HDFDM,
					&非农..NMDF非F_F WITH HNMDF,
					&非农..NFDM非F_F WITH HNFDM,
					&非农..NN非F_F 	 WITH HNN FOR X=NL
			
		ENDIF
	ENDFOR
	
	*****取小，求女性年龄分的婚配成功对数
	REPLACE &非农..DD非F 	WITH MIN(&非农..DD非F_F,需妻模式.DD妻),
			&非农..NMDF非F 	WITH MIN(&非农..NMDF非F_F,需妻模式.NMDF妻),;
			&非农..NFDM非F 	WITH MIN(&非农..NFDM非F_F,需妻模式.NFDM妻),
			&非农..NN非F 	WITH MIN(&非农..NN非F_F,需妻模式.NN妻) ALL
			
	***********************
	REPLACE &非农..DD WITH &非农..DD+&非农..DD非F,
			&非农..NMDF WITH &非农..NMDF+&非农..NMDF非F,
			&非农..NFDM WITH &非农..NFDM+&非农..NFDM非F,
			&非农..NN WITH &非农..NN+&非农..NN非F ALL 
			
	*****计算剩男剩女\各类未婚人数******
	REPLACE &非农..DF20 WITH &非农..DF0-&非农..DD-&非农..NMDF,
			&非农..NF20 WITH &非农..NF0-&非农..NN-&非农..NFDM,;
			&农业..DM20 WITH &农业..DM10-需夫模式.DD夫-需夫模式.NFDM夫,
			&农业..NM20 WITH &农业..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
			
	*************************
	*	农业女与非农男婚配	*
	*************************
	SELE A
	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 	WITH 0,需夫模式.NN夫 	WITH 0 ALL
	FOR NL=15 TO 64
		LOCA FOR X=NL
		求夫DF=&农业..DF10
		求夫NF=&农业..NF10
		夫FB='妻子'-allt(STR(NL,3))
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
		SUM &非农..DM0+&非农..NM0 TO SM
		IF SM<>0
			REPL 需夫模式.DD夫 	    WITH 需夫模式.DD夫+求夫DF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NN夫 	    WITH 需夫模式.NN夫+求夫NF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0

			SUM 求夫DF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			SUM 求夫NF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			
			REPLACE &农业..DD WITH &农业..DD+HDFDM,
					&农业..NMDF WITH &农业..NMDF+HNMDF,
					&农业..NFDM WITH &农业..NFDM+HNFDM,
					&农业..NN  WITH &农业..NN+HNN FOR X=NL
					
		ENDIF
	ENDFOR
	*****各类未婚人数******
	REPLACE &农业..DF20 WITH &农业..DF0-&农业..DD-&农业..NMDF,
			&农业..NF20 WITH &农业..NF0-&农业..NN-&农业..NFDM,;
			&农业..DM20 WITH &农业..DM10-需夫模式.DD夫-需夫模式.NFDM夫,
			&农业..NM20 WITH &农业..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
ENDFOR


&& 第三阶段、统计剩余的人数
*******对婚配剩余女性处理：独生女计入女单，非独生女计入双非
REPLACE 地区农村非农子女.NMDF WITH 地区农村非农子女.NMDF+地区农村非农子女.DF20,
		地区农村农业子女.NMDF WITH 地区农村农业子女.NMDF+地区农村农业子女.DF20,;
		地区城镇非农子女.NMDF WITH 地区城镇非农子女.NMDF+地区城镇非农子女.DF20,
		地区城镇农业子女.NMDF WITH 地区城镇农业子女.NMDF+地区城镇农业子女.DF20,;
		地区农村非农子女.NN WITH 地区农村非农子女.NN+地区农村非农子女.NF20,
		地区农村农业子女.NN WITH 地区农村农业子女.NN+地区农村农业子女.NF20,;
		地区城镇非农子女.NN WITH 地区城镇非农子女.NN+地区城镇非农子女.NF20,
		地区城镇农业子女.NN WITH 地区城镇农业子女.NN+地区城镇农业子女.NF20 FOR X>=15.AND.X<=49




**************************************************************************************************************************************************************	
*	以上程序段逻辑检验：
*	BROWSE FIELDS FC=地区农村非农子女.DF0-地区农村非农子女.DD-地区农村非农子女.NMDF,MC=地区农村非农子女.NF0-地区农村非农子女.NN-地区农村非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区农村农业子女.DF0-地区农村农业子女.DD-地区农村农业子女.NMDF,MC=地区农村农业子女.NF0-地区农村农业子女.NN-地区农村农业子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇非农子女.DF0-地区城镇非农子女.DD-地区城镇非农子女.NMDF,MC=地区城镇非农子女.NF0-地区城镇非农子女.NN-地区城镇非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇农业子女.DF0-地区城镇农业子女.DD-地区城镇农业子女.NMDF,MC=地区城镇农业子女.NF0-地区城镇农业子女.NN-地区城镇农业子女.NFDM	&&>=0
*	经以下语句检验，经过以上两次婚配，育龄夫妇已经完全婚配完毕，无一剩余，也无负数出现
*	BROWSE FIELDS x,地区农村非农子女.DM20,地区农村农业子女.DM20,地区城镇非农子女.DM20,地区城镇农业子女.DM20,地区农村非农子女.NM20,地区农村农业子女.NM20,地区城镇非农子女.NM20,地区城镇农业子女.NM20
*	BROWSE FIELDS x,地区农村非农子女.DF20,地区农村农业子女.DF20,地区城镇非农子女.DF20,地区城镇农业子女.DF20,地区农村非农子女.NF20,地区农村农业子女.NF20,地区城镇非农子女.NF20,地区城镇农业子女.NF20
*	lkm
***************************************************************************************************************************************************************	
RETURN 