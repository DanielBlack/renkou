CREA TABL 地区生育率与迁移摘要 	FREE (地区 C(6),年份 N(4),政策方案 C(20),可实现TFR N(8,2),政策生育率 N(8,2),超生生育率 N(8,2),超生孩子数 N(8,2),;
FN女单终身 N(8,2),FN男单终身 N(8,2),FN双非终身 N(8,2),NY女单终身 N(8,2),NY男单终身 N(8,2),NY双非终身 N(8,2),;
DQ时期TFR N(8,2),CZ时期TFR N(8,2),FN时期TFR N(8,2),NC时期TFR N(8,2),NY时期TFR N(8,2),;
DQ迁移合计 N(8,2),城镇迁移 N(8,2),非农迁移 N(8,2),农村迁移 N(8,2),农业迁移 N(8,2),本地迁移 N(8,2),QG累计迁移 N(8,2))

&& ？？？？？？？？？？？？
USE 
*****迁移修正系数*****
迁移修正系数0428='D:\分省生育政策仿真1102\分省生育政策仿真基础数据\迁移概率\各地区城乡迁移修正系数0428'
*迁移修正系数='D:\分省生育政策仿真1102\分省生育政策仿真基础数据\迁移概率\各地区城乡迁移修正系数'
*CREATE TABLE &迁移修正系数 FREE (省代码 C(2),地区 C(6),C迁06B N(8,5),X迁06B N(8,5),C迁07B N(8,5),X迁07B N(8,5),C迁08B N(8,5),X迁08B N(8,5),C迁09B N(8,5),X迁09B N(8,5))
*APPEND FROM D:\分省生育政策仿真1102\分省生育政策仿真基础数据\\数据库（全国）\全国分省代码
*****死亡修正系数*****
死亡率修正系数='D:\分省生育政策仿真1102\分省生育政策仿真基础数据\死亡概率\各地区死亡概率修正系数'
CREATE TABLE &死亡率修正系数 FREE (省代码 C(2),地区 C(6),SWL修正B N(8,5))
APPEND FROM D:\分省生育政策仿真1102\分省生育政策仿真基础数据\分省代码

*****分婚配模式夫妇基本表结构*****
CREA TABL 婚配预测表结构 FREE (X N(4),合J分龄SYL N(10,4),双D分龄SYL N(10,4),单D分龄SYL N(10,4),双N分龄SYL N(10,4),FRD N(10,4),FR N(10,6),FR1 N(10,6),FR2 N(10,6),;
	独转非 N(10,0),DDB N(10,0),NMDFB N(10,0),NFDMB N(10,0),NNB N(10,0),;
	政策生育 N(10,0),政策DDB N(10,0),政策NMDFB N(10,0),政策NFDMB N(10,0),政策NNB N(10,0),政策释放B N(10,0),;
	超生生育 N(10,0),超生DDB N(10,0),超生NMDFB N(10,0),超生NFDMB N(10,0),超生NNB N(10,0),超生释放B N(10,0),;
	HJ N(12,0),HJM N(12,0),HJF N(12,0),D N(12,0),DM N(12,0),DF N(12,0),N N(12,0),NM N(12,0),NF N(12,0),DDBS N(10,0),DDBM N(10,0),DDBF N(10,0),NMDFBS N(10,0),NFDMBS N(10,0),NNBS N(10,0),;
	HJQY N(12,0),HJQYM N(12,0),HJQYF N(12,0),DQY N(12,0),DQYM N(12,0),DQYF N(12,0),NQY N(12,0),NQYM N(12,0),NQYF N(12,0),DDBQYS N(10,0),DDBQYM N(10,0),DDBQYF N(10,0),;
	DM0 N(10,0),DF0 N(10,0),NM0 N(10,0),NF0 N(10,0),MIDM0 N(10,0),MIDF0 N(10,0),MINM0 N(10,0),MINF0 N(10,0),;
	夫妇合计 N(10,0),DD N(10,0),NMDF N(10,0),NFDM N(10,0),NN N(10,0),;
	DD_F N(10,0),NMDF_F N(10,0),NFDM_F N(10,0),NN_F N(10,0),DD_M N(10,0),NMDF_M N(10,0),NFDM_M N(10,0),NN_M N(10,0),;
	DD非F N(10,0),NMDF非F N(10,0),NFDM非F N(10,0),NN非F N(10,0),DD非F_F N(10,0),NMDF非F_F N(10,0),NFDM非F_F N(10,0),NN非F_F N(10,0),DD非F_M N(10,0),NMDF非F_M N(10,0),NFDM非F_M N(10,0),NN非F_M N(10,0),;
	DD农F N(10,0),NMDF农F N(10,0),NFDM农F N(10,0),NN农F N(10,0),DD农F_F N(10,0),NMDF农F_F N(10,0),NFDM农F_F N(10,0),NN农F_F N(10,0),DD农F_M N(10,0),NMDF农F_M N(10,0),NFDM农F_M N(10,0),NN农F_M N(10,0),;
	DD_CFXF N(10,0),NMDF_CFXF N(10,0),NFDM_CFXF N(10,0),NN_CFXF N(10,0),DD_CFXN N(10,0),NMDF_CFXN N(10,0),NFDM_CFXN N(10,0),NN_CFXN N(10,0),;
	DD_CNXF N(10,0),NMDF_CNXF N(10,0),NFDM_CNXF N(10,0),NN_CNXF N(10,0),DD_CNXN N(10,0),NMDF_CNXN N(10,0),NFDM_CNXN N(10,0),NN_CNXN N(10,0),;
	DD_XFCF N(10,0),NMDF_XFCF N(10,0),NFDM_XFCF N(10,0),NN_XFCF N(10,0),DD_XFCN N(10,0),NMDF_XFCN N(10,0),NFDM_XFCN N(10,0),NN_XFCN N(10,0),;
	DD_XNCF N(10,0),NMDF_XNCF N(10,0),NFDM_XNCF N(10,0),NN_XNCF N(10,0),DD_XNCN N(10,0),NMDF_XNCN N(10,0),NFDM_XNCN N(10,0),NN_XNCN N(10,0),;
	DD0 N(10,0),DM10 N(10,0),DM20 N(10,0),DM30 N(10,0),DM40 N(10,0),DF10 N(10,0),DF20 N(10,0),DF30 N(10,0),DF40 N(10,0),;
	NM10 N(10,0),NM20 N(10,0),NM30 N(10,0),NM40 N(10,0),NF10 N(10,0),NF20 N(10,0),NF30 N(10,0),NF40 N(10,0),;
	FR201 N(10,8),FR202 N(10,8),FR203 N(10,8),FR204 N(10,8),FR205 N(10,8),FR206 N(10,8),FR207 N(10,8),FR208 N(10,8),FR209 N(10,8),FR210 N(10,8),FR211 N(10,8),FR212 N(10,8),;
	SWD N(12,0),SWDM N(12,0),SWDF N(12,0),单独释放B N(10),双非释放B N(10),;
	单独堆积 N(10),单独堆积15 N(10),单独堆积30 N(10),单独堆积35 N(10),堆积丈夫 N(10),女单堆积 N(10),男单堆积 N(10),已2单独 N(10),已2女单 N(10),已2男单 N(10),;
	双非堆积 N(10),双非堆积15 N(10),双非堆积30 N(10),双非堆积35 N(10),已2双非 N(10),堆积比例 N(8,4),释放模式 N(8,4),;
	双独概率M N(8,4),双独概率F N(8,4),男单概率M N(8,4),男单概率F N(8,4),女单概率M N(8,4),女单概率F N(8,4),双非概率M N(8,4),双非概率F N(8,4),;
	NF双独GLM N(8,4),NF双独GLF N(8,4),NF男单GLM N(8,4),NF男单GLF N(8,4),NF女单GLM N(8,4),NF女单GLF N(8,4),NF双非GLM N(8,4),NF双非GLF N(8,4))
	
	*--lqz--分号把长命令换行，每个命令在结束时没有符号
	*--lqz--appe->append   repl-->replace
	FOR NL=0 TO 110
	   APPE BLANK
	   REPL X WITH NL
	ENDFOR
AFIEL(AR子女婚配预测表结构) &&将表结构保存到数组中

*****分婚配模式生育政策参数基本表结构*****
CREA TABL 政策及政策生育率 FREE (年份 N(4),非农政策1 C(10),农业政策1 C(10),非农政策2 C(10),农业政策2 C(10),非农政策3 C(10),农业政策3 C(10),;
	非农双独 N(6,2),农业双独 N(6,2),非农单独 N(6,2),农业单独 N(6,2),非农女单 N(6,2),;
	农业女单 N(6,2),非农男单 N(6,2),农业男单 N(6,2),非农双非 N(6,2),农业双非 N(6,2),;
	农村NYTFR N(8,2),农村FNTFR N(8,2),农村TFR N(8,2),城镇NYTFR N(8,2),城镇FNTFR N(8,2),城镇TFR N(8,2),地区NYTFR N(8,2),地区FNTFR N(8,2),地区TFR N(8,2),;
	NCNY婚TFR N(8,2),NCFN婚TFR N(8,2),NC婚TFR N(8,2),CZNY婚TFR N(8,2),CZFN婚TFR N(8,2),CZ婚TFR N(8,2),QSNY婚TFR N(8,2),QSFN婚TFR N(10,2),QS婚TFR N(10,2))	&&农村NY概率等"概率"字段,均为双独概率 
	FOR ND=2005 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR

CREA TABL 夫妇及子女表结构 FREE (年份 N(4),非农政策1 C(10),农业政策1 C(10),非农政策2 C(10),农业政策2 C(10),非农政策3 C(10),农业政策3 C(10),;
	TFR N(10, 4),双独TFR N(10,4),单独TFR N(10,4),双非TFR N(10,4),;
	非农双独 N(6,2),非农男单 N(6,2),非农女单 N(6,2),非农双非 N(6,2),农业双独 N(6,2),农业男单 N(6,2),农业女单 N(6,2),农业双非 N(6,2),;
	合计夫妇数 N(10), 双独夫妇数 N(10), 单独夫妇数 N(10), 女单夫妇数 N(10),男单夫妇数 N(10),单独堆积 N(10),已2单独 N(10),双非夫妇数 N(10),双非堆积 N(10),已2双非 N(10),;
	双独生育数 N(10),单独生育数 N(10),女单生育数 N(10),男单生育数 N(10), 双非生育数 N(10),C超生 N(10,0),X超生 N(10,0), ;
	D N(10), DM N(10), DF N(10), N N(10), NM N(10), NF N(10),可二后 N(10),DDBM N(10), DDBF N(10),;
	独生子女30 N(10), 独生子30 N(10), 独生女30 N(10), 非独子女30 N(10), 非独子30 N(10), 非独女30 N(10), 可二后30 N(10),可二后30M N(10), 可二后30F N(10),;
	独生子女SW N(10), 独生子SW N(10), 独生女SW N(10), 非独子女SW N(10), 非独子SW N(10), 非独女SW N(10), 可二后SW N(10),可二后SWM N(10), 可二后SWF N(10),;
	一孩父母 N(10), 一孩父 N(10), 一孩母 N(10),特扶 N(10), 特扶M N(10), 特扶F N(10),;
	未婚F N(10,2),未婚M N(10,2),未婚DF N(10,2),未婚DM N(10,2),未婚NF N(10,2),未婚NM N(10,2),农业未婚F N(10,2),农业未婚M N(10,2),非农未婚F N(10,2),非农未婚M N(10,2),;
	农业未婚DF N(8,2),农业未婚DM N(8,2),农业未婚NF N(8,2),农业未婚NM N(8,2),非农未婚DF N(8,2),非农未婚DM N(8,2),非农未婚NF N(8,2),非农未婚NM N(8,2),;				
	双独概率M N(8,4),双独概率F N(8,4),男单概率M N(8,4),男单概率F N(8,4),女单概率M N(8,4),女单概率F N(8,4))
	AFIEL(AR各类夫妇) 	&&16_74
	FOR ND=2005 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR

CREA TABL 分婚配模式生育孩次数 FREE (年份 N(4),总HJB N(10),城HJB N(10),乡HJB N(10),总HJB1 N(10),城HJB1 N(10),乡HJB1 N(10),;
	总FNB1 N(10),总NYB1 N(10),总HJB2 N(10),城HJB2 N(10),乡HJB2 N(10),总FNB2  N(10),总NYB2  N(10),;	
	总HJ双独B  N(10),城HJ双独B  N(10),乡HJ双独B  N(10),总FN双独B  N(10),城FN双独B  N(10),乡FN双独B  N(10),总NY双独B  N(10),城NY双独B  N(10),乡NY双独B  N(10),;
	总HJ双独B1 N(10),城HJ双独B1 N(10),乡HJ双独B1 N(10),总FN双独B1 N(10),城FN双独B1 N(10),乡FN双独B1 N(10),总NY双独B1 N(10),城NY双独B1 N(10),乡NY双独B1 N(10),;
	总HJ双独B2 N(10),城HJ双独B2 N(10),乡HJ双独B2 N(10),总FN双独B2 N(10),城FN双独B2 N(10),乡FN双独B2 N(10),总NY双独B2 N(10),城NY双独B2 N(10),乡NY双独B2 N(10),;
	总HJ单独B  N(10),城HJ单独B  N(10),乡HJ单独B  N(10),总FN单独B  N(10),城FN单独B  N(10),乡FN单独B  N(10),总NY单独B  N(10),城NY单独B  N(10),乡NY单独B  N(10),;
	总HJ单独B1 N(10),城HJ单独B1 N(10),乡HJ单独B1 N(10),总FN单独B1 N(10),城FN单独B1 N(10),乡FN单独B1 N(10),总NY单独B1 N(10),城NY单独B1 N(10),乡NY单独B1 N(10),;
	总HJ单独B2 N(10),城HJ单独B2 N(10),乡HJ单独B2 N(10),总FN单独B2 N(10),城FN单独B2 N(10),乡FN单独B2 N(10),总NY单独B2 N(10),城NY单独B2 N(10),乡NY单独B2 N(10),;
	总HJ双非B  N(10),城HJ双非B  N(10),乡HJ双非B  N(10),总FN双非B  N(10),城FN双非B  N(10),乡FN双非B  N(10),总NY双非B  N(10),城NY双非B  N(10),乡NY双非B  N(10),;
	总HJ双非B1 N(10),城HJ双非B1 N(10),乡HJ双非B1 N(10),总FN双非B1 N(10),城FN双非B1 N(10),乡FN双非B1 N(10),总NY双非B1 N(10),城NY双非B1 N(10),乡NY双非B1 N(10),;
	总HJ双非B2 N(10),城HJ双非B2 N(10),乡HJ双非B2 N(10),总FN双非B2 N(10),城FN双非B2 N(10),乡FN双非B2 N(10),总NY双非B2 N(10),城NY双非B2 N(10),乡NY双非B2 N(10))
*--lqz--把表结构存放在数组中 数组命名统一在前面加“AR”
AFIELDS(AR生育孩次)
	FOR ND=2005 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
*****人口预测结果概要 基本表结构*****

CREA TABL  AAA概要 FREE (年份 N(4),地区 C(6),总人口 N(10,2),男 N(10,2),女 N(10,2),年均人口 N(10,2),出生 N(10,2),出生男 N(10,2),出生女 N(10,2),出生XBB N(10,2),;
	出生一孩 N(10,2),出生二孩 N(10,2),政策二孩 N(10,2),政策生育数 N(10,2),超生数 N(10,2),堆积夫妇数 N(10,2),堆积生育 N(10,2),非堆生育 N(10,2),;
	出生率 N(8,2),死亡率 N(8,2),自增率 N(8,2),净迁入率 N(8,2),总增长率 N(8,2),可实现TFR N(8,2),政策生育率 N(8,2),实婚生育率 N(8,2),婚N政策TFR N(8,2),;
	死亡人数 N(10,2),死亡男 N(10,2),死亡女 N(10,2),自增人口 N(10,2),预期寿命M  N(10,2),预期寿命F  N(10,2),;
	净迁入数 N(10,2),净迁男 N(10,2),净迁女 N(10,2),新增人数 N(10,2),非农人口 N(10,2),农业人口 N(10,2),非农人口B N(10,2),城镇人口 N(10,2),农村人口 N(10,2),城镇人口比 N(10,2),;
	婴幼儿 N(10,2),学前 N(10,2),小学 N(10,2),初中 N(10,2),高中 N(10,2),大学 N(10,2),;
	国标劳前 N(10,2),国标劳龄 N(10,2),国标劳龄M N(10,2),国标劳龄F N(10,2),国标青劳 N(10,2),国标中劳 N(10,2),国标中劳M N(10,2),国标中劳F N(10,2),;
	国标劳后 N(10,2),国标劳后M N(10,2),国标劳后F N(10,2),公安劳龄 N(10,2),公安劳龄M N(10,2),公安劳龄F N(10,2),;
	国标少年 N(10,2),国标劳年 N(10,2),国标劳年M N(10,2),国标劳年F N(10,2),	国标老年 N(10,2),国标老年M N(10,2),国标老年F N(10,2),;
	高龄 N(10,2),高龄M N(10,2),高龄F N(10,2),长寿 N(10,2),长寿M N(10,2),长寿F N(10,2),中位 N(10,2),年龄差异度 N(10,2),;
	国标少年比 N(10,2),国标老年比 N(10,2),国标老少比 N(10,2),国标总负 N(6,2),国标负老 N(10,2),国标负少 N(10,2),国标劳年B N(10,2),国标青劳B N(10,2),国标高龄B N(10,2),国标长寿B N(10,2),;
	国际少年 N(10,2),国际劳年 N(10,2),国际青劳 N(10,2),国际老年 N(10,2),国际老年M N(10,2),国际老年F N(10,2),;
	国际少年比 N(10,2),国际老年比 N(10,2),国际老少比 N(10,2),国际总负 N(6,2),国际负老 N(10,2),国际负少 N(10,2),国际劳年B N(10,2),国际青劳B N(10,2),国际高龄B N(10,2),国际长寿B N(10,2),;
	育龄女15 N(10,2),婚育男15 N(10,2),育龄女20 N(10,2),婚育男20 N(10,2),婚育XBB15 N(10,4),婚育XBB20 N(10,4),;
	人口密度 N(10,2),人均水资源 N(10,2),人均耕地 N(10,2))
	FOR ND=2000 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR

CREA TABL  AAA婚配概率 FREE (年份 N(4),地区 C(6),;
	X农双独GLM N(8,4),X农双独GLF N(8,4),X非双独GLM N(8,4),X非双独GLF N(8,4),X农男单GLM N(8,4),X农男单GLF N(8,4),X农女单GLM N(8,4),X农女单GLF N(8,4),;
	X非男单GLM N(8,4),X非男单GLF N(8,4),X非女单GLM N(8,4),X非女单GLF N(8,4),C农双独GLM N(8,4),C农双独GLF N(8,4),C非双独GLM N(8,4),C非双独GLF N(8,4),;
	C农男单GLM N(8,4),C农男单GLF N(8,4),C农女单GLM N(8,4),C农女单GLF N(8,4),C非男单GLM N(8,4),C非男单GLF N(8,4),C非女单GLM N(8,4),C非女单GLF N(8,4))
	FOR ND=2005 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR

*****分年龄预测基本表结构*****
CREA TABL AAA分年龄人口预测 (X N(4))	
	FOR ND0=2005 TO ND2
	    MND='M'+STR(ND0,4)
	    FND='F'+STR(ND0,4)
	    ALTE TABL AAA分年龄人口预测 ADD &MND N(10)
	    ALTE TABL AAA分年龄人口预测 ADD &FND N(10)
	ENDFOR
	FOR X0=0 TO 110
	   APPE BLANK
	   REPL X WITH X0
	ENDFOR

*****AAA分年龄丧子人口预测基本表结构*****
CREA TABL AAA分年龄丧子人口预测 (X N(4))	
	FOR ND0=2005 TO ND2
	    MND='M'+STR(ND0,4)
	    FND='F'+STR(ND0,4)
	    ALTE TABL AAA分年龄丧子人口预测 ADD &MND N(10,6)
	    ALTE TABL AAA分年龄丧子人口预测 ADD &FND N(10,6)
	ENDFOR
	FOR X0=0 TO 110
	   APPE BLANK
	   REPL X WITH X0
	ENDFOR

*****全国迁移平衡表结构*****
CREA TABL AAA全国迁移平衡表结构 (X N(4),迁出T N(8),迁出M N(8),迁出F N(8),迁入T N(8),迁入M N(8),迁入F N(8),迁移平衡T N(8),迁移平衡M N(8),迁移平衡F N(8),;
平衡迁出T N(8),平衡迁出M N(8),平衡迁出F N(8),平衡迁入T N(8),平衡迁入M N(8),平衡迁入F N(8))	
FOR X0=0 TO 110
   APPE BLANK
   REPL X WITH X0
ENDFOR
*****分年龄生育孩子数 基本表结构*****
CREA TABL AAA分年龄生育预测 FREE (X N(4))
	FOR ND0=2005 TO ND2
		BX='B'-STR(ND0,4)
		ALTE TABL AAA分年龄生育预测 ADD &BX N(10)
	ENDFOR
	FOR X1=15 TO 49
		APPE BLANK
		REPL X WITH X1
	ENDFOR

*****分年龄生育率 基本表结构*****
CREA TABL AAA妇女分年龄生育率预测 FREE (X N(4))
	FOR ND0=2005 TO ND2
		FX='F'-STR(ND0,4)
		ALTE TABL AAA妇女分年龄生育率预测 ADD &FX N(8,4)
	ENDFOR
	FOR X1=15 TO 49
		APPE BLANK
		REPL X WITH X1
	ENDFOR