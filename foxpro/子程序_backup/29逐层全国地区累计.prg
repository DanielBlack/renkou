PROCEDURE 逐层全国地区累计
*************************
*	  逐层全国累计		*
*********************************
*	  分年龄数据全国累计		*
*********************************
*?'省区迁移和:',SQY
SELECT A
FOR LBX=1 TO 6
	RK类别=IIF(LBX=1,'分龄',IIF(LBX=2,'特扶',IIF(LBX=3,'父母',IIF(LBX=4,'迁移',IIF(LBX=5,'死亡','生育')))))
	FOR XBX=1 TO IIF(LBX=6,1,2)
		XB=IIF(XBX=1,'M','F')
		IF LBX=6
			FIEXB=BND2
		ELSE
			FIEXB=IIF(XBX=1,MND2,FND2)
		ENDIF
		地区分龄='地区'-RK类别
		全国分龄='全国'-RK类别
		REPLACE &全国分龄..&FIEXB WITH  &全国分龄..&FIEXB+&地区分龄..&FIEXB ALL
		FOR CXX=1 TO 2	&&3
			DO CASE
			CASE LBX<=3
				城乡=IIF(CXX=1,'城镇','农村')
				地区城乡分龄='地区'-城乡-RK类别
				全国城乡分龄='全国'-城乡-RK类别
				REPLACE &全国城乡分龄..&FIEXB WITH  &全国城乡分龄..&FIEXB+&地区城乡分龄..&FIEXB ALL
				农非=IIF(CXX=1,'农业','非农')
				地区农非分龄='地区'-农非-RK类别
				全国农非分龄='全国'-农非-RK类别
				REPLACE &全国农非分龄..&FIEXB WITH  &全国农非分龄..&FIEXB+&地区农非分龄..&FIEXB ALL
				FOR NFX=1 TO 2
					农非=IIF(NFX=1,'农业','非农')
					地区城乡农非分龄='地区'-城乡-农非-RK类别
					全国城乡农非分龄='全国'-城乡-农非-RK类别
					REPLACE &全国城乡农非分龄..&FIEXB WITH  &全国城乡农非分龄..&FIEXB+&地区城乡农非分龄..&FIEXB ALL
				ENDFOR
			CASE LBX=6
				城乡=IIF(CXX=1,'城镇','农村')
				地区城乡分龄='地区'-城乡-RK类别
				全国城乡分龄='全国'-城乡-RK类别
				REPLACE &全国城乡分龄..&FIEXB WITH  &全国城乡分龄..&FIEXB+&地区城乡分龄..&FIEXB ALL
				农非=IIF(CXX=1,'非农','农业')
				地区农非分龄='地区'-农非-RK类别
				全国农非分龄='全国'-农非-RK类别
				REPLACE &全国农非分龄..&FIEXB WITH  &全国农非分龄..&FIEXB+&地区农非分龄..&FIEXB ALL
			ENDCASE
		ENDFOR
	ENDFOR
ENDFOR

*SUM 全国分龄.&MND1+全国分龄.&FND1 TO QGZRK
*?'全国总人口:',QGZRK/10000
*kj

*****超生累计*****
REPLACE 全国分龄政策生育.&BND2 WITH 全国分龄政策生育.&BND2 +地区分龄政策生育.&BND2  ALL
REPLACE 全国分龄超生.&BND2 WITH 全国分龄超生.&BND2 +地区分龄超生.&BND2,;
	全国城镇超生.&BND2 WITH 全国城镇超生.&BND2 +地区城镇超生.&BND2,;
	全国农村超生.&BND2 WITH 全国农村超生.&BND2 +地区农村超生.&BND2,;
	全国农业超生.&BND2 WITH 全国农业超生.&BND2 +地区农业超生.&BND2,;
	全国非农超生.&BND2 WITH 全国非农超生.&BND2 +地区非农超生.&BND2 ALL
*****婚配预测累计*****
FOR F1=10 TO 179	&&89
	FIE1=AR子女婚配预测表结构(F1,1)
	REPLACE 全国子女.&FIE1 WITH IIF(DQX=DQ1,地区子女.&FIE1,全国子女.&FIE1+地区子女.&FIE1) ALL
	FOR CXX=1 TO 2
		城乡=IIF(CXX=1,'城镇','农村')
		FOR NFX=1 TO 3
			农非=IIF(NFX=1,'农业',IIF(NFX=2,'非农',''))
			地区城乡农非子女='地区'-城乡-农非-'子女'
			全国城乡农非子女='全国'-城乡-农非-'子女'
			REPLACE &全国城乡农非子女..&FIE1 WITH   IIF(DQX=DQ1,&地区城乡农非子女..&FIE1,&全国城乡农非子女..&FIE1+&地区城乡农非子女..&FIE1) ALL
		ENDFOR
		农非=IIF(CXX=1,'农业','非农')
		地区农非子女='地区'-农非-'子女'
		全国农非子女='全国'-农非-'子女'
		REPLACE &全国农非子女..&FIE1 WITH  IIF(DQX=DQ1,&地区农非子女..&FIE1,&全国农非子女..&FIE1+&地区农非子女..&FIE1) ALL
	ENDFOR
ENDFOR
***********分年龄婚配结果***************
IF (SFMS=1.AND.ND0=调整时机).OR.(SFMS=1.AND.ND0<=2050.AND.MOD(ND0,5)=0)
	FOR SE=1 TO 4
		SELE IIF(SE=1,161,IIF(SE=2,162,IIF(SE=3,163,164)))
			FIL1=SUBS(DBF(),1,LEN(DBF())-4)-'_'-FA-'_'-JA-STR(ND0,4)
			COPY TO &FIL1 FIEL &子女FIE1
		SELE IIF(SE=1,165,IIF(SE=2,166,IIF(SE=3,167,168)))
			FIL1=SUBS(DBF(),1,LEN(DBF())-4)-'_'-FA-'_'-JA-STR(ND0,4)
			COPY TO &FIL1 FIEL &子女FIE2
	ENDFOR
	SELECT 169	
	FIL1=SUBS(DBF(),1,LEN(DBF())-4)-'_'-FA-'_'-JA-STR(ND0,4)
	COPY TO &FIL1 FIEL &子女FIE2
ENDIF
*********************************
*	  分年度数据全国累计		*
*********************************
IF ND0=ND2
	SELECT 151
	*LOCATE FOR 年份=ND0
	FOR F1=2 TO 91
		FIE1=AR生育孩次(F1,1)
		REPLACE 全国生育孩次.&FIE1 WITH 全国生育孩次.&FIE1+地区生育孩次.&FIE1 ALL && FOR 年份=ND0
	ENDFOR
	******分年度各类夫妇及子女合计******
	FOR F1=20 TO 87
		FIE1=AR各类夫妇(F1,1)
		REPLACE  全国农村农业夫妇.&FIE1 WITH 全国农村农业夫妇.&FIE1+地区农村农业夫妇.&FIE1,;
		 全国农村非农夫妇.&FIE1 WITH 全国农村非农夫妇.&FIE1+地区农村非农夫妇.&FIE1,;
		 全国城镇农业夫妇.&FIE1 WITH 全国城镇农业夫妇.&FIE1+地区城镇农业夫妇.&FIE1,;
		 全国城镇非农夫妇.&FIE1 WITH 全国城镇非农夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;	
		 全国农业夫妇.&FIE1 WITH 全国农业夫妇.&FIE1+地区农业夫妇.&FIE1,;
		 全国非农夫妇.&FIE1 WITH 全国非农夫妇.&FIE1+地区非农夫妇.&FIE1,;
	     全国农村夫妇.&FIE1 WITH 全国农村夫妇.&FIE1+地区农村夫妇.&FIE1,;	
		 全国城镇夫妇.&FIE1 WITH 全国城镇夫妇.&FIE1+地区城镇夫妇.&FIE1,;
		 全国夫妇.&FIE1 WITH 全国夫妇.&FIE1+地区夫妇.&FIE1 ALL
	ENDFOR 
ENDIF
RETURN