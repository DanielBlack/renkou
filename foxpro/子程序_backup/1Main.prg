&& ？？？？ 单独的USE空命令的作用是什么——打开当前工作区中的表：一个工作区只能有一个表

SET DEFAULT TO D:\分省生育政策仿真1102\生育政策仿真\多地区渐进普二\TMP
SET SAFE OFF
SET TALK OFF
SET DECI TO 4
CLOS DATA
CLEA ALL
CLEA
*SET ALTE TO AAA2
*SET ALTE ON
*************************
*	参数和表结构准备*
*************************
概率估算法='多龄概率法'
结婚率法='全员婚配'
TJ=3
堆积估算法=IIF(TJ=1,'生育模式堆积估计法',IIF(TJ=2,'平均存活堆积估计法','存活一孩堆积估计法'))


ND1=2010
ND2=2100
DQ1=1
DQ2=31
*********************
&& 默认工作区，工作区1，一个工作区只能打开一个文件——默认为dbf文件。工作区相当文件指针。
USE D:\分省生育政策仿真1102\分省生育政策仿真基础数据\数据库（全国）\人口普查1990\分年龄死亡人口
COPY TO ARRA AXM FIEL Man	&&死亡人口死亡当年存活年数(男)  
&&这条命令针对当前工作区
COPY TO ARRA AXF FIEL Fan	&&死亡人口死亡当年存活年数(男)
* 从dbf表中获取数据到数组

*****定义全局变量*****
PUBL TFR,总人口T,总人口M,总人口F,年中人口,新增人口,人口增长L,T少年,T劳年,T青劳,T老年,T高龄,T老年M,T高龄M,T老年F,T高龄F,NLZ,FND2,MND2,;
	 育龄F15,婚育M15,育龄F20,婚育M20,长寿M,长寿F,T婴幼儿,XQ,XX,CZ,GZ,DX,JYM,JYF,寿命M,寿命F,长寿M0,长寿F0,EM0,EF0,MAND3,MATFR0,;
	 GB劳前,GB劳龄M,GB劳龄F,GB青劳,GB中劳M,GB中龄F,GB劳后M,GB劳后F,G劳年M,G老年M,G少年,G劳年F,G老年F,;
	 城FN男单GLF,城FN男单GLM,城FN女单GLF,城FN女单GLM,城FN双独GLF,城FN双独GLM,城NY男单GLF,城NY男单GLM,城NY女单GLF,;
	 城NY女单GLM,城NY双独GLF,城NY双独GLM,乡FN男单GLF,乡FN男单GLM,乡FN女单GLF,乡FN女单GLM,乡FN双独GLF,乡FN双独GLM,;
	 乡NY男单GLF,乡NY男单GLM,乡NY女单GLF,乡NY女单GLM,乡NY双独GLF,乡NY双独GLM,;
	 XNY未婚DF,XNY未婚DM,XNY未婚NF,XNY未婚NM,XFN未婚DF,XFN未婚DM,XFN未婚NF,XFN未婚NM,;
	 CNY未婚DF,CNY未婚DM,CNY未婚NF,CNY未婚NM,CFN未婚DF,CFN未婚DM,CFN未婚NF,CFN未婚NM,;
	 DQ死亡人口M,DQ死亡人口F,C死亡人口M,C死亡人口F,X死亡人口M,X死亡人口F,MINLM,MINLF,;
	 QXM,QXF,ST,AV,城超生,乡超生,迁入和0,迁出和0,迁移平衡和,迁入和M0,迁出和M0,迁入和F0,迁出和F0,;
	城镇政策TFR0,城镇实现TFR0,城镇实婚TFR0,农村政策TFR0,农村实现TFR0,农村实婚TFR0,地区政策TFR0,地区实现TFR0,地区实婚TFR0,;
	农业政策TFR0,农业实现TFR0,农业实婚TFR0,非农政策TFR0,非农实现TFR0,非农实婚TFR0,;
	城镇非农TFR0,农村非农TFR0,城镇农业TFR0,农村农业TFR0,民族B,死亡率修正系数,迁移修正系数0428,调整时机,政策,LL,现政比1,CSH水平1,CSH水平2,地区政策TFR1

*定义变量 初值为空
PUBL N1D2C,N2D1C,NNFC,N1D2N,N2D1N,NNFN,DDFR,ND0,DQB,可实现TFR0,地区政策TFR,地区超生TFR,超生TFR,超生S,实现比,DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育,;
	 b2,b3,b4,A,DB,DDFR,DDFR,N1D2C,N1D2N,N2D1C,N2D1N,NNFC,NNFN,DS,NS,DMS,DFS,NMS,NFS,;
	 DDBSS,DDBMS,DDBFS,DS30,DMS30,DFS30,NS30,NMS30,NFS30,DDBSS30,DDBMS30,DDBFS30,DDS,;
	 DD1,NMDF1,NFDM1,NN1,DD2,NMDF2,NFDM2,NN2,农村农业一孩,农村非农一孩,城镇农业一孩,城镇非农一孩,;
	农村农业独子父,农村农业独子母,城镇农业独子父,城镇农业独子母,农村非农独子父,农村非农独子母,城镇非农独子父,城镇非农独子母,;
	农村农业特扶父,农村农业特扶母,城镇农业特扶父,城镇农业特扶母,农村非农特扶父,农村非农特扶母,城镇非农特扶父,城镇非农特扶母,;
	 NMDFS,NFDMS,堆积单独,单独已2,堆积双非,双非已2,NNS,SWDM,SWDF,SWNM,SWNF,SWDDM,SWDDF,独生子SW,独生女SW,非独子SW,非独女SW,可二后SWM,可二后SWF,;
	 CZFN婚内TFR,CZNY婚内TFR,DDFR,DDFR,N1D2C,N1D2N,N2D1C,N2D1C,N2D1N,N2D1N,NCFN婚内TFR,NCNY婚内TFR,NNFC,NNFN,城镇TFR0,城镇非农TFR,城镇婚内TFR,;
	 城镇农业TFR,地区非农TFR,地区婚内TFR,地区农业TFR,地区政策TFR,非农婚内TFR,农村TFR0,农村非农TFR,农村婚内TFR,农村农业TFR,农业婚内TFR,;
	 CRK,XRK,FNRK,NYRK,城镇TFR0,城镇非农TFR,城镇农业TFR,城镇实婚TFR0,城镇实现TFR0,;
	 地区非农TFR,地区农业TFR,地区实婚TFR0,地区实现TFR0,地区政策TFR,S堆积,;
	 非农实婚TFR0,非农实现TFR0,农村TFR0,农村非农TFR,农村农业TFR,农村实婚TFR0,农村实现TFR0,农业实婚TFR0,农业实现TFR0,MITFR,;
	 CSH水平,QGCSH08,SDQQY,SQY,出生人口M,出生人口F,城镇婚内TFR,地区婚内TFR,非农婚内TFR,农村婚内TFR,农业婚内TFR,地带,DQQYND0,各地区生育率与迁移摘要,生育政策模拟摘要,;
	 广东C总迁率M0,广东X总迁率M0,广东C总迁率F0,广东X总迁率F0,现行N1D2C,现行N2D1C,现行NNFC,现行N1D2N,现行N2D1N,现行NNFN

* 定义数组
DIMENSION 	AR非农释放模式(40),AR非农缓释模式(40),AR非农突释模式(40),AR农业释放模式(40),AR农业缓释模式(40),;
	AR农业突释模式(40),ARDQ出生XBB(31,14),AR子女婚配预测表结构(1),AR生育孩次(1),AR各类夫妇(1),ARFND1(115),DQJQL(DQ2),ARFND1(111)
	&& ARR(行数，列数)

*批量赋值
STORE 0 TO AR非农释放模式,AR非农缓释模式,AR非农突释模式,AR农业释放模式,AR农业缓释模式,AR农业突释模式,ARFND1,DQJQL,LL,现政比1,CSH水平1,CSH水平,地区政策TFR1,ARFND1

PUBL QXM(101),QXF(101),XSWRM(101),CSWRM(101),XSWRF(101),CSWRF(101)
ARFIE11='(AR1(RECN()+5,1)+AR1(RECN()+4,1)+AR1(RECN()+3,1)+AR1(RECN()+2,1)+AR1(RECN()+1,1)+AR1(RECN(),1)+'
ARFIE12='AR1(RECN()-1,1)+AR1(RECN()-2,1)+AR1(RECN()-3,1)+AR1(RECN()-4,1)+AR1(RECN()-5,1))/'
ARFIE21='(AR1(RECN()+5,2)+AR1(RECN()+4,2)+AR1(RECN()+3,2)+AR1(RECN()+2,2)+AR1(RECN()+1,2)+AR1(RECN(),2)+'
ARFIE22='AR1(RECN()-1,2)+AR1(RECN()-2,2)+AR1(RECN()-3,2)+AR1(RECN()-4,2)+AR1(RECN()-5,2))/'
ARFIE13='(AR1(RECN()+5,1)+AR1(RECN()+4,1)+AR1(RECN()+3,1)+AR1(RECN()+2,1)+AR1(RECN()+1,1)+AR1(RECN(),1)+'
ARFIE14='AR1(RECN()-1,1)+AR1(RECN()-2,1)+AR1(RECN()-3,1)+AR1(RECN()-4,1)+AR1(RECN()-5,1)+'
ARFIE15='AR1(RECN()+5,2)+AR1(RECN()+4,2)+AR1(RECN()+3,2)+AR1(RECN()+2,2)+AR1(RECN()+1,2)+AR1(RECN(),2)+'
ARFIE16='AR1(RECN()-1,2)+AR1(RECN()-2,2)+AR1(RECN()-3,2)+AR1(RECN()-4,2)+AR1(RECN()-5,2))'
ARFIE1='&ARFIE11'+'&ARFIE12'+'&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'
ARFIE2='&ARFIE21'+'&ARFIE22'+'&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'
ARFOR='&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'   
*字符串 宏替代
子女FIE1='X'-','-'合J分龄SYL'-','-'双D分龄SYL'-','-'单D分龄SYL'-','-'双N分龄SYL'-','-'FR1'-','-'FR2'-','-'独转非'-','-'DDB'-','-'NMDFB'-','-;
	'NFDMB'-','-'NNB'-','-'HJ'-','-'HJM'-','-'HJF'-','-'D'-','-'DM'-','-'DF'-','-'N'-','-'NM'-','-'NF'-','-'NF20'-','-'DF20'-','-;
	'NM20'-','-'DM20'-','-'HJQY'-','-'HJQYM'-','-'HJQYF'-','-'DQY'-','-'DQYM'-','-'DQYF'-','-'NQY'-','-'NQYM'-','-'NQYF'-','-;
	'DDBS'-','-'DDBM'-','-'DDBF'-','-'DD'-','-'NMDF'-','-'NFDM'-','-'NN'-','-'单独释放B'-','-'双非释放B'-','-'单独堆积'-','-;
	'堆积丈夫'-','-'女单堆积'-','-'男单堆积'-','-'已2单独'-','-'已2女单'-','-'已2男单'-','-'双非堆积'-','-;
	'已2双非'-','-'堆积比例'-','-'释放模式'-','-'双独概率M'-','-'双独概率F'-','-'男单概率M'-','-'男单概率F'-','-;
	'女单概率M'-','-'女单概率F'-','-'双非概率M'-','-'双非概率F'-','-'NF双独GLM'-','-'NF双独GLF'-','-'NF男单GLM'-','-;
	'NF男单GLF'-','-'NF女单GLM'-','-'NF女单GLF'-','-'NF双非GLM'-','-'NF双非GLF'
子女FIE2='X'-','-'合J分龄SYL'-','-'双D分龄SYL'-','-'单D分龄SYL'-','-'双N分龄SYL'-','-'独转非'-','-'DDB'-','-'NMDFB'-','-'NFDMB'-','-'NNB'-','-;
	'HJ'-','-'HJM'-','-'HJF'-','-'D'-','-'DM'-','-'DF'-','-'N'-','-'NM'-','-'NF'-','-'NF20'-','-'DF20'-','-'NM20'-','-'DM20'-','-;
	'HJQY'-','-'HJQYM'-','-'HJQYF'-','-'DQY'-','-'DQYM'-','-'DQYF'-','-'NQY'-','-'NQYM'-','-'NQYF'-;
	','-'DDBS'-','-'DDBM'-','-'DDBF'-','-'DD'-','-'NMDF'-','-'NFDM'-','-'NN'-','-'单独释放B'-','-;
	'双非释放B'-','-'单独堆积'-','-'堆积丈夫'-','-'女单堆积'-','-'男单堆积'-','-'已2单独'-','-'已2女单'-','-'已2男单'-','-'双非堆积'-','-'已2双非'
STORE 0 TO 单放B2,非放B2,单独B2,农业单放B2,农业非放B2,农业单独堆积,农业双非堆积,非农单放B2,非农非放B2,非农单独堆积,非农双非堆积,实现比
STORE 0 TO 城镇非农单独堆积,城镇非农单放B2,城镇非农非放B2,城镇非农双非堆积,城镇农业单独堆积,城镇农业单放B2,城镇农业非放B2,城镇农业双非堆积,;
		   农村非农单独堆积,农村非农单放B2,农村非农非放B2,农村非农双非堆积,农村农业单独堆积,农村农业单放B2,农村农业非放B2,农村农业双非堆积

**--lqz--选择主程序
*DO 多地区迁移平衡单时机调整仿真
DO 多地区迁移平衡渐进普二政策仿真  && DO调用子程序  

SET ALTE TO 
CLOS ALTE
*MODI COMM AAA.TXT
SET SAFE ON
SET TALK ON

RETU

