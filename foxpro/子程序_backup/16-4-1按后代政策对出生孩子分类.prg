PROCEDURE 按后代政策对出生孩子分类
DO CASE   

&& 政策 是 当前的政策，但是用来为后代进行分类——如果子女生育时政策有变化再根据新政策调整
CASE  政策='现行'
    &&独生子女间婚配所生子女按非独生子女生育（现行政策）。因为双独、单独都只管一代，其后代都按非独处理，所以都适用这一情况，即MN1=1和MN1=2)处理是一样的
	MS='全部现行(独后按非独)'	&&单独后也按非独 ——只是为了显示，不用翻译
	NB2=DD2+NMDF2+NFDM2+NN2    &&各模式生育的第二个孩子数
	
	LOCA FOR X=0
	
	&& DDBS 是指享受双独政策的人, 但不一定是所有独生子女都能享受到——与政策有关
	REPL DDBS  WITH 0 , 
		 DDBM WITH 0*XBB/(1+XBB),
		 DDBF WITH 0/(1+XBB)  &&独生子女间婚配所生子女数和享受独生子女间婚配同等政策的其他婚配模式所生子女，城乡无差异
	
	REPL D  WITH  DD1+NMDF1+NFDM1+NN1, 
		 DM  WITH  D*XBB/(1+XBB),
		 DF  WITH  D/(1+XBB)  &&非独生子女间婚配生的第一个孩子总计(零岁组）
	
	REPL N  WITH  NB2, 
		 NM  WITH  NB2*XBB/(1+XBB),
		 NF  WITH  NB2/(1+XBB) &&非双独婚配生的第二个孩子总计(零岁组）
	
CASE  政策='单独'  
    &&独生子女间婚配所生子女按非独生子女生育（现行政策）。因为双独、单独都只管一代，其后代都按非独处理，所以都适用这一情况，即MN1=1和MN1=2)处理是一样的
	MS='全部现行(独后按非独)'	&&单独后也按非独
	NB2=DD2+单独B2+NN2    &&各模式生育的第二个孩子数
	LOCA FOR X=0
	
	REPL DDBS  WITH 0 , 
		 DDBM WITH 0*XBB/(1+XBB),
		 DDBF WITH 0/(1+XBB)  &&独生子女间婚配所生子女数和享受独生子女间婚配同等政策的其他婚配模式所生子女，城乡无差异
	
	REPL D  WITH  DD1+NMDF1+NFDM1+NN1, 
		 DM  WITH  D*XBB/(1+XBB),
		 DF  WITH  D/(1+XBB)  &&非独生子女间婚配生的第一个孩子总计(零岁组）
	
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB),NF  WITH  NB2/(1+XBB) &&非双独婚配生的第二个孩子总计(零岁组）
	
CASE 政策='单独后'	&&(双独后代、单独后代可生二个)
	MS='双独单独后2，其他现行'
	NB2=NN2    &&非独生子女间婚配生育的第二个孩子数
	LOCA FOR X=0
	
	REPL DDBS  WITH DB+NMDF1+NFDM1+单独B2, 
		 DDBM WITH (DB+NMDF1+NFDM1+单独B2)*XBB/(1+XBB),
		 DDBF WITH (DB+NMDF1+NFDM1+单独B2)/(1+XBB)  &&独生子女间婚配所生子女数
	
	REPL D  WITH  NN1, 
		 DM  WITH  NN1*XBB/(1+XBB),
		 DF  WITH  (NN1)/(1+XBB)&&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, 
		 NM  WITH  (NB2)*XBB/(1+XBB),
		 NF  WITH  (NB2)/(1+XBB) &&非独生子女间婚配生的第二个孩子总计(零岁组）
		 
	&& 就是由于政策会变化，会影响到后代的生育政策，故后代的生育政策应该随着改变	 
	*****************************************************调整单独第一个孩子的属性.2009.8.29新加程序段**********************************************
	*	全面的子女属性调整，还应该包括对后代属性的调整。在单独后政策下，单独夫妇的所有子女都应该加入DDBS队列，包括新生的，也包括政策调整前生育的。*
	*	特别是堆积的单独夫妇，生育第二个孩子后，他们原来已经生育的第一个孩子，应该和新生的第二个孩子，一起并入DDBS系列。做法是：堆积的单独夫妇原有
	*	的第一个孩子，按堆积夫妇的数量和第一个孩子的年龄分布，移入DDBS队列。而新的，还没有生育第一个孩子的单独夫妇，则可按上面语句处理
	**************************************************************************************************************************************
	&& 仅在政策调整当年做此工作
	IF ND0=调整时机
		CALCULATE MIN(X) TO MIX FOR 单独堆积<>0
		
		SUM DM+DF TO SDM_F FOR X>0.AND.X<=MIX
		
		SUM NM+NF TO SNM_F FOR X>0.AND.X<=MIX
		
		SUM 单独堆积,已2单独 TO S单独堆积,S已2单独  FOR X>=15.AND.X<=49
		
		REPL DDBM WITH DDBM+S单独堆积*DM/SDM_F,
			 DDBF WITH DDBF+S单独堆积*DF/SDM_F FOR X>0.AND.X<=MIX	&&MIX:堆积母亲的最小年龄，也是第一个孩子的最大年龄
			 
		REPL DDBM WITH DDBM+S已2单独*NM/SNM_F,
			 DDBF WITH DDBF+S已2单独*NF/SNM_F FOR X>0.AND.X<=MIX
			 
		REPL DM WITH DM-S单独堆积*DM/SDM_F,
			 DF WITH DF-S单独堆积*DF/SDM_F FOR X>0.AND.X<=MIX
		
		REPL NM WITH NM-S已2单独*NM/SNM_F,
			 NF WITH NF-S已2单独*NF/SNM_F FOR X>0.AND.X<=MIX
	ENDIF

CASE  政策='普二'
	MS='普二'
	NB2=0    &&
	LOCA FOR X=0
	REPL DDBS  WITH DB+NMDF1+NFDM1+单独B2+NN1+NN2, DDBM WITH DDBS*XBB/(1+XBB),DDBF WITH DDBS/(1+XBB)       
	REPL D  WITH  0, DM  WITH  0,DF  WITH  0 &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  0, NM  WITH  0,NF  WITH  0 &&非独生子女间婚配生的第二个孩子总计(零岁组）

CASE  政策='双独后' && 暂不翻译
	MS='双独后2，其他现行'
	NB2=单独B2+NN2    &&农村非独生子女间婚配第一个孩子为女孩的各模式生育的第二个孩子数
	LOCA FOR X=0
	REPL DDBS  WITH DB , DDBM WITH DB*XBB/(1+XBB),DDBF WITH DB/(1+XBB)  &&独生子女间婚配所生子女数和享受同等政策的其他婚配模式所生子女，城乡无差异
	REPL D  WITH  NMDF1+NFDM1+NN1, DM  WITH  (NMDF1+NFDM1+NN1)*XBB/(1+XBB),DF  WITH  (NMDF1+NFDM1+NN1)/(1+XBB)       &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB), NF  WITH  NB2/(1+XBB)       &&非独生子女婚配生的第二个孩子总计(零岁组）

CASE  政策='双独后单独' && 暂不翻译
	MS='双独后单独2，单独后及其他现行'
	NB2=(NFDM2+NMDF2+NN2 )          &&非独生女与独生男、非独生男与独生女、非独生子女间婚配生育的第二个孩子数
	LOCA FOR X=0
	*REPL DDBS  WITH DB+NMDF1+NFDM1,DDBM WITH (DB+NMDF1+NFDM1)*XBB/(1+XBB),DDBF WITH (DB+NMDF1+NFDM1)/(1+XBB)
	REPL DDBS  WITH DB,
		 DDBM WITH DB*XBB/(1+XBB),
		 DDBF WITH DB/(1+XBB)  &&独生子女间婚配所生子女数
	REPL D  WITH  NMDF1+NFDM1+NN1, DM  WITH  D*XBB/(1+XBB),DF  WITH  D/(1+XBB)   &&单独婚配、非独生子女间婚配生的第一个孩子总计(零岁组，当时计为独生子女）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB),NF  WITH  NB2/(1+XBB)      &&非独生女与独生男、非独生男与独生女、非独生子女间婚配生育的第二个孩子数生的第二个孩子总计(零岁组）
ENDCASE     

RETURN 