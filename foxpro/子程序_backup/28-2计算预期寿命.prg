PROC 计算预期寿命	
*****M*****
REC=MINLM
DIME 生存人数M(REC)
生存人数M(1)=1-QXM(1)
FOR I0=2 TO REC
	生存人数M(I0)=生存人数M(I0-1)*(1-QXM(I0))
ENDFOR
DIME 生存年数M(REC)
生存年数M(REC)=生存人数M(REC)	&&*AXM(REC)
FOR I0=2 TO REC
	生存年数M(REC+1-I0)=生存年数M(REC+1-I0+1)+生存人数M(REC+1-I0)+生存人数M(REC+1-I0)*QXM(REC+1-I0)	&&*AXM(REC+1-I0)
ENDFOR
EM0=生存年数M(1)
*****F*****
REC=MINLF
DIME 生存人数F(REC)
生存人数F(1)=1-QXF(1)
FOR I0=2 TO REC
	生存人数F(I0)=生存人数F(I0-1)*(1-QXF(I0))
ENDFOR
DIME 生存年数F(REC)
生存年数F(REC)=生存人数F(REC)	&&*AXF(REC)
FOR I0=2 TO REC
	生存年数F(REC+1-I0)=生存年数F(REC+1-I0+1)+生存人数F(REC+1-I0)+生存人数F(REC+1-I0)*QXF(REC+1-I0)	&&*AXF(REC+1-I0)
ENDFOR
EF0=生存年数F(1)
RETU