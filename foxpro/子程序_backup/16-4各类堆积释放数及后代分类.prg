&& 将后代进行分类
PROCEDURE 各类堆积释放数及后代分类 && 公用代码段
	****************************
	SUM  合J分龄SYL,双D分龄SYL,单D分龄SYL,双N分龄SYL TO STFR,DDTFR, 单DTFR,NNTFR
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非) TO 婚内TFR	FOR (DD+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非)<>0								&&所有婚配模式合计的分年龄生育率
	DO CASE		&&记录堆积释放结果
	CASE CX=1	 &&SELE 61 :地区农村农业子女
	  	农村农业一孩=DD1+NMDF1+NFDM1+NN1
	  	农村农业单放B2=单放B2
	  	农村农业非放B2=非放B2
	 	NCNY婚内TFR=婚内TFR
	 	SUM 单独堆积,双非堆积 TO 农村农业单独堆积,农村农业双非堆积  FOR X>=15.AND.X<=49
	CASE CX=2	 &&SELE62 :地区农村非农子女
	  	农村非农一孩=DD1+NMDF1+NFDM1+NN1
	  	农村非农单放B2=单放B2
	  	农村非农非放B2=非放B2
	  	NCFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 农村非农单独堆积,农村非农双非堆积 FOR X>=15.AND.X<=49
	CASE CX=3	 &&SELE63 :地区城镇农业子女
	  	城镇农业一孩=DD1+NMDF1+NFDM1+NN1
	  	城镇农业单放B2=单放B2
	  	城镇农业非放B2=非放B2
	  	CZNY婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇农业单独堆积,城镇农业双非堆积 FOR X>=15.AND.X<=49
	CASE CX=4	 &&SELE64 :地区城镇非农子女
	  	城镇非农一孩=DD1+NMDF1+NFDM1+NN1
	  	城镇非农单放B2=单放B2
	  	城镇非农非放B2=非放B2
	  	CZFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇非农单独堆积,城镇非农双非堆积 FOR X>=15.AND.X<=49
	ENDCASE
	*********************************************************************
	*	对所生孩子进行分类，确定下一代享受不同政策的人数（只对零岁组操作）   *
	*********************************************************************
	DO 按后代政策对出生孩子分类
	****************************
	SELE 50		&&USE &生育孩次数 ALIA 生育孩次
	LOCA FOR 年份=ND0
	DO CASE
	CASE CX=1	&&SELE 61 :地区农村农业子女
		REPL 乡NY双独B1 WITH DD1,乡NY双独B2 WITH DD2,乡NY单独B1 WITH NMDF1+NFDM1,乡NY单独B2 WITH 单独B2,乡NY双非B1 WITH NN1,乡NY双非B2 WITH NN2
	CASE CX=2	&&SELE 62 :地区农村非农子女
		REPL 乡FN双独B1 WITH DD1,乡FN双独B2 WITH DD2,乡FN单独B1 WITH NMDF1+NFDM1,乡FN单独B2 WITH 单独B2,乡FN双非B1 WITH NN1,乡FN双非B2 WITH NN2								
	CASE CX=3	&&SELE 63 :地区城镇农业子女
		REPL 城NY双独B1 WITH DD1,城NY双独B2 WITH DD2,城NY单独B1 WITH NMDF1+NFDM1,城NY单独B2 WITH 单独B2,城NY双非B1 WITH NN1,城NY双非B2 WITH NN2
	CASE CX=4	&&SELE 64 :地区城镇非农子女							
		REPL 城FN双独B1 WITH DD1,城FN双独B2 WITH DD2,城FN单独B1 WITH NMDF1+NFDM1,城FN单独B2 WITH 单独B2,城FN双非B1 WITH NN1,城FN双非B2 WITH NN2
	ENDCASE 
RETURN 