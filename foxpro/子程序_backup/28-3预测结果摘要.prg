PROC 预测结果摘要
APPEND BLANK 
REPL 年份 WITH ND0,地区 WITH IIF(CX=1,DQB,IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业')))),总人口 WITH 总人口T,男 WITH 总人口M,女 WITH 总人口F,;
	死亡男 WITH IIF(CX=1,DQ死亡人口M,IIF(CX=2,C死亡人口M,IIF(CX=3,0,IIF(CX=4,X死亡人口M,0)))),; 
	死亡女 WITH IIF(CX=1,DQ死亡人口F,IIF(CX=2,C死亡人口F,IIF(CX=3,0,IIF(CX=4,X死亡人口F,0)))),; 
	政策生育率 WITH IIF(CX=1,地区政策TFR0,IIF(CX=2,城镇政策TFR0,IIF(CX=3,非农政策TFR0,IIF(CX=4,农村政策TFR0,农业政策TFR0)))),;
	婚N政策TFR WITH IIF(CX=1,地区婚内TFR,IIF(CX=2,城镇婚内TFR,IIF(CX=3,非农婚内TFR,IIF(CX=4,农村婚内TFR,农业婚内TFR)))),;
	可实现TFR WITH IIF(CX=1.AND.ZC实现=2,地区实现TFR0,IIF(CX=2.AND.ZC实现=2,城镇实现TFR0,IIF(CX=3.AND.ZC实现=2,非农实现TFR0,IIF(CX=4.AND.ZC实现=2,农村实现TFR0,IIF(CX=5.AND.ZC实现=2,农业实现TFR0,0))))),;
	实婚生育率 WITH IIF(CX=1.AND.ZC实现=2,地区实婚TFR0,IIF(CX=2.AND.ZC实现=2,城镇实婚TFR0,IIF(CX=3.AND.ZC实现=2,非农实婚TFR0,IIF(CX=4.AND.ZC实现=2,农村实婚TFR0,IIF(CX=5.AND.ZC实现=2,农业实婚TFR0,0))))),;
	出生 WITH 出生人口,出生男 WITH 出生人口M,出生女 WITH 出生人口F,出生率 WITH 出生人口/年中人口*1000,超生数 WITH IIF(ND0>2008.and.ZC实现=2,超生S/10000,0),;
	政策生育数 WITH IIF(CX=1,DQ政策生育,IIF(CX=2,XFN政策生育,IIF(CX=3,CFN政策生育,IIF(CX=4,XNY政策生育,CNY政策生育)))),;
	死亡人数 WITH 死亡男+死亡女,死亡率 WITH 死亡人数/年中人口*1000,自增率 WITH 出生率-死亡率,预期寿命M WITH EM0,预期寿命F WITH EF0,;
	年均人口 WITH 年中人口,新增人数 WITH 新增人口,总增长率 WITH 新增人口/年中人口*1000,出生XBB WITH XBB*100,净迁入数 WITH 净迁入RK,净迁入率 WITH 净迁入RK/年中人口*1000,;
	城镇人口 WITH CRK/10000,农村人口 WITH XRK/10000,城镇人口比 WITH CSH水平,非农人口 WITH FNRK/10000,农业人口 WITH NYRK/10000,非农人口B WITH FNRK/(FNRK+NYRK)*100,;
	婴幼儿 WITH T婴幼儿,学前 WITH XQ,小学 WITH XX,初中 WITH CZ,高中 WITH GZ,大学 WITH DX,公安劳龄 WITH JYM+JYF,公安劳龄M WITH JYM,公安劳龄F WITH JYF,;
	国际少年 WITH T少年,国际劳年 WITH T劳年,国际青劳 WITH T青劳,国际老年 WITH T老年,国际老年M WITH T老年M,国际老年F WITH T老年F,;
	高龄 WITH T高龄,高龄M WITH T高龄M,高龄F WITH T高龄F,长寿 WITH 长寿M0+长寿F0,长寿M WITH 长寿M0,长寿F WITH 长寿F0,中位 WITH NLZ,年龄差异度 WITH ST/AV*100,;
	国标少年  WITH G少年,国标劳年F WITH G劳年F,国标老年F  WITH G老年F,国标劳年M WITH G劳年M,国标老年M  WITH G老年M,;
	国标劳前 WITH GB劳前,国标劳龄M WITH GB劳龄M,国标劳龄F WITH GB劳龄F,国标青劳 WITH GB青劳,国标中劳M WITH GB中劳M,国标中劳F WITH GB中龄F,国标劳后M WITH GB劳后M,国标劳后F WITH GB劳后F,;
	育龄女15  WITH 育龄F15,育龄女20  WITH 育龄F20, 婚育男15 WITH 婚育M15,婚育男20  WITH 婚育M20,婚育XBB15  WITH IIF(育龄女15=0,0,婚育男15/育龄女15*100),婚育XBB20  WITH IIF(育龄女20=0,0,婚育男20/育龄女20*100)
IF ND0>=调整时机.AND.(ND0-调整时机+1)<=35
	REPL 堆积生育 WITH IIF(CX=1,农村农业单放B2+农村农业非放B2+农村非农单放B2+农村非农非放B2+城镇农业单放B2+城镇农业非放B2+城镇非农单放B2+城镇非农非放B2,;
		IIF(CX=2,城镇农业单放B2+城镇农业非放B2+城镇非农单放B2+城镇非农非放B2,IIF(CX=3,城镇非农单放B2+城镇非农非放B2+农村非农单放B2+农村非农非放B2,;
		IIF(CX=4,农村农业单放B2+农村农业非放B2+农村非农单放B2+农村非农非放B2,农村农业单放B2+农村农业非放B2+城镇农业单放B2+城镇农业非放B2))))/10000
	REPL 堆积夫妇数 WITH IIF(CX=1,农村农业单独堆积+农村农业双非堆积+农村非农单独堆积+农村非农双非堆积+城镇农业单独堆积+城镇农业双非堆积+城镇非农单独堆积+城镇非农双非堆积,;
		IIF(CX=2,城镇农业单独堆积+城镇农业双非堆积+城镇非农单独堆积+城镇非农双非堆积,IIF(CX=3,城镇非农单独堆积+城镇非农双非堆积+农村非农单独堆积+农村非农双非堆积,;
		IIF(CX=4,农村农业单独堆积+农村农业双非堆积+农村非农单独堆积+农村非农双非堆积,农村农业单独堆积+农村农业双非堆积+城镇农业单独堆积+城镇农业双非堆积))))/10000
ENDIF
IF CX>5
	STORE 0 TO 城镇非农单独堆积,城镇非农单放B2,城镇非农非放B2,城镇非农双非堆积,城镇农业单独堆积,城镇农业单放B2,城镇农业非放B2,;
	城镇农业双非堆积,农村非农单独堆积,农村非农单放B2,农村非农非放B2,农村非农双非堆积,农村农业单独堆积,农村农业单放B2,农村农业非放B2,农村农业双非堆积
ENDIF

IF ND0=ND2	&&IIF(SFMS=1,2100,2100)
	REPL 总人口 WITH 男+女 FOR 总人口=0
	REPL 国标劳龄 WITH 国标劳龄M+国标劳龄F ,国标中劳 WITH 国标中劳M+国标中劳F,国标劳后 WITH 国标劳后M+国标劳后F,国标劳年 WITH 国标劳年M+国标劳年F,国标老年 WITH 国标老年M+国标老年F  ALL
	REPL 国标少年比 WITH 国标少年/总人口*100,国标老年比 WITH 国标老年/总人口*100, 国标劳年B WITH 国标劳年/总人口*100,国际少年比 WITH 国际少年/总人口*100,国际老年比 WITH 国际老年/总人口*100, 国际劳年B WITH 国际劳年/总人口*100  FOR 总人口<>0
	REPL 国标高龄B  WITH 高龄/国标老年*100,国标长寿B WITH 长寿/国标老年*100 FOR 国标老年<>0
	REPL 国际高龄B  WITH 高龄/国际老年*100,国际长寿B WITH 长寿/国际老年*100 FOR 国际老年<>0
	REPL 国际老少比 WITH IIF(国际少年<>0,国际老年/国际少年*100,0) FOR 国际少年<>0
	REPL 国标老少比 WITH IIF(国标少年<>0,国标老年/国标少年*100,0) FOR 国标少年<>0
	REPL 国际负老 WITH IIF(国际劳年<>0,国际老年/国际劳年*100,0),国际负少 WITH IIF(国际劳年<>0,国际少年/国际劳年*100,0),国际总负 WITH 国际负老+国际负少,国际青劳B WITH IIF(国际劳年<>0,国际青劳/国际劳年*100,0) FOR 国际劳年<>0
	REPL 国标负老 WITH IIF(国标劳年<>0,国标老年/国标劳年*100,0),国标负少 WITH IIF(国标劳年<>0,国标少年/国标劳年*100,0),国标总负 WITH 国标负老+国标负少,国标青劳B WITH IIF(国标劳年<>0,国标青劳/国标劳年*100,0) FOR 国标劳年<>0
	REPL 人口密度 WITH 总人口*10000/(土地MJ*1000/100),人均水资源 WITH (水资源*10000)/总人口,人均耕地 WITH (耕地MJ*1000*15)/(总人口*10000) FOR 总人口<>0	&&原单位:千公顷,单位:亿立方米,计算后:人/平方公里,立方米/人,亩/人
	REPL 非堆生育 WITH 出生-堆积生育,出生二孩 WITH 政策二孩+超生数,自增人口 WITH 出生-死亡人数 ALL
ENDIF
*IF CX=1
*	SELE 56	&&地区婚配概率 ALIAS 地区婚配概率
*	APPEND BLANK
*	REPL 地区 WITH DQB,年份 WITH ND0,;
*	C非男单GLF WITH 城FN男单GLF,C非男单GLM WITH 城FN男单GLM,C非女单GLF WITH 城FN女单GLF,C非女单GLM WITH 城FN女单GLM,C非双独GLF WITH 城FN双独GLF,C非双独GLM WITH 城FN双独GLM,;
*	C农男单GLF WITH 城NY男单GLF,C农男单GLM WITH 城NY男单GLM,C农女单GLF WITH 城NY女单GLF,C农女单GLM WITH 城NY女单GLM,C农双独GLF WITH 城NY双独GLF,C农双独GLM WITH 城NY双独GLM,;
*	X非男单GLF WITH 乡FN男单GLF,X非男单GLM WITH 乡FN男单GLM,X非女单GLF WITH 乡FN女单GLF,X非女单GLM WITH 乡FN女单GLM,X非双独GLF WITH 乡FN双独GLF,X非双独GLM WITH 乡FN双独GLM,;
*	X农男单GLF WITH 乡NY男单GLF,X农男单GLM WITH 乡NY男单GLM,X农女单GLF WITH 乡NY女单GLF,X农女单GLM WITH 乡NY女单GLM,X农双独GLF WITH 乡NY双独GLF,X农双独GLM WITH 乡NY双独GLM
*ENDIF
SELE &SEL

RETU