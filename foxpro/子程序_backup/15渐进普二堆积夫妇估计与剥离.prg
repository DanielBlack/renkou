PROCEDURE 渐进普二堆积夫妇估计与剥离  && 原来的政策不允许生，新政策后，生育意愿不同，需要剥离——将历史上堆积的原政策不能生二胎而新政策可生二胎的夫妇，存到 堆积字段中 NN等
DO CASE 

&& 在调整时机那年，只做分离  
&& 分离出原政策不允许而新政策允许生育的夫妇
CASE ND0=调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		*SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM1,SF1 FOR &子女..堆积比例<>0
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			&& 堆积比例一开始为0，后面会计算
			REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N2D1N,现行N2D1C))  FOR &子女..堆积比例<>0
			REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N1D2N,现行N1D2C))   FOR &子女..堆积比例<>0
			REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积 ALL
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL &&FOR &子女..堆积比例<>0
		CASE 政策='普二'
	
			REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,NNFN,NNFC))  FOR &子女..堆积比例<>0
			REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0

		ENDCASE
	
	ENDFOR
	
&& 大于调整时机后，堆积人数一年年地减少 
CASE ND0>调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
		CASE 政策='普二'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
			REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL
		ENDCASE
	ENDFOR
ENDCASE 
RETURN 