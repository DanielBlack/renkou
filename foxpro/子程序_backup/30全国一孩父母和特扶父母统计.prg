PROCEDURE 全国一孩父母和特扶父母统计
SELECT A
REPL 全国城镇特扶.&MND2 WITH 全国城镇非农特扶.&MND2+全国城镇农业特扶.&MND2,全国城镇特扶.&FND2 WITH 全国城镇非农特扶.&FND2+全国城镇农业特扶.&FND2 ALL
REPL 全国农村特扶.&MND2 WITH 全国农村非农特扶.&MND2+全国农村农业特扶.&MND2,全国农村特扶.&FND2 WITH 全国农村非农特扶.&FND2+全国农村农业特扶.&FND2 ALL
REPL 全国非农特扶.&MND2 WITH 全国城镇非农特扶.&MND2+全国城镇非农特扶.&MND2,全国非农特扶.&FND2 WITH 全国城镇非农特扶.&FND2+全国城镇非农特扶.&FND2 ALL
REPL 全国农业特扶.&MND2 WITH 全国农村农业特扶.&MND2+全国农村农业特扶.&MND2,全国农业特扶.&FND2 WITH 全国农村农业特扶.&FND2+全国农村农业特扶.&FND2 ALL
REPL 全国特扶.&MND2 WITH 全国城镇特扶.&MND2+全国农村特扶.&MND2,全国特扶.&FND2 WITH 全国城镇特扶.&FND2+全国农村特扶.&FND2 ALL

REPL 全国城镇父母.&MND2 WITH 全国城镇非农父母.&MND2+全国城镇农业父母.&MND2,全国城镇父母.&FND2 WITH 全国城镇非农父母.&FND2+全国城镇农业父母.&FND2 ALL
REPL 全国农村父母.&MND2 WITH 全国农村非农父母.&MND2+全国农村农业父母.&MND2,全国农村父母.&FND2 WITH 全国农村非农父母.&FND2+全国农村农业父母.&FND2 ALL
REPL 全国非农父母.&MND2 WITH 全国城镇非农父母.&MND2+全国城镇非农父母.&MND2,全国非农父母.&FND2 WITH 全国城镇非农父母.&FND2+全国城镇非农父母.&FND2 ALL
REPL 全国农业父母.&MND2 WITH 全国农村农业父母.&MND2+全国农村农业父母.&MND2,全国农业父母.&FND2 WITH 全国农村农业父母.&FND2+全国农村农业父母.&FND2 ALL
REPL 全国父母.&MND2 WITH 全国城镇父母.&MND2+全国农村父母.&MND2,全国父母.&FND2 WITH 全国城镇父母.&FND2+全国农村父母.&FND2 ALL
FOR CX=1 TO 4
	ALI1=IIF(CX=1,'全国农村农业特扶',IIF(CX=2,'全国城镇农业特扶',IIF(CX=3,'全国农村非农特扶','全国城镇非农特扶')))
	ALI2=IIF(CX=1,'全国农村农业父母',IIF(CX=2,'全国城镇农业父母',IIF(CX=3,'全国农村非农父母','全国城镇非农父母')))
	SELECT A
	SUM &ALI1..&MND2,&ALI1..&FND2 TO TFM,TFF FOR X>=49
	SUM &ALI2..&MND2,&ALI2..&FND2 TO S一孩父,S一孩母 FOR X>=60
	SELE IIF(CX=1,151,IIF(CX=2,152,IIF(CX=3,153,154)))
	LOCATE FOR 年份=ND0
	REPLACE 特扶F WITH TFF,特扶M WITH TFM,特扶 WITH TFF+TFM,一孩父 WITH S一孩父,一孩母  WITH S一孩母 ,一孩父母 WITH S一孩父+S一孩母
ENDFOR