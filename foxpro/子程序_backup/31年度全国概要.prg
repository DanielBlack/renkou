PROCEDURE 年度全国概要
FOR CX=1 TO 4
	SELE IIF(CX=1,161,IIF(CX=2,162,IIF(CX=3,163,164)))
	***********************
	DO 各类婚配夫妇分年龄生育率
	***********************
	SUM  合J分龄SYL,双D分龄SYL,单D分龄SYL,双N分龄SYL TO STFR,DDTFR, 单DTFR,NNTFR
	SUM (DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(DD+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非) TO 婚内TFR	FOR (DD+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非)<>0								&&所有婚配模式合计的分年龄生育率
	SUM 单独释放B TO 单放B2
	SUM 双非释放B TO 非放B2
	DO CASE		&&记录堆积释放结果
	CASE CX=1	 &&SELE 61 :地区农村农业子女
	  	农村农业单放B2=单放B2
	  	农村农业非放B2=非放B2
	 	NCNY婚内TFR=婚内TFR
	 	SUM 单独堆积,双非堆积 TO 农村农业单独堆积,农村农业双非堆积  FOR X>=15.AND.X<=49
	CASE CX=2	 &&SELE62 :地区农村非农子女
	  	农村非农单放B2=单放B2
	  	农村非农非放B2=非放B2
	  	NCFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 农村非农单独堆积,农村非农双非堆积 FOR X>=15.AND.X<=49
	CASE CX=3	 &&SELE63 :地区城镇农业子女
	  	城镇农业单放B2=单放B2
	  	城镇农业非放B2=非放B2
	  	CZNY婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇农业单独堆积,城镇农业双非堆积 FOR X>=15.AND.X<=49
	CASE CX=4	 &&SELE64 :地区城镇非农子女
	  	城镇非农单放B2=单放B2
	  	城镇非农非放B2=非放B2
	  	CZFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇非农单独堆积,城镇非农双非堆积 FOR X>=15.AND.X<=49
	ENDCASE
ENDFOR
***********************
SELECT A
RELEASE ARFND1
COPY TO ARRAY ARFND1 FIELDS 全国分龄.&FND1
REPLACE 全国超生生育率.&FND2 WITH 全国分龄超生.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM (全国生育.&BND2+全国分龄超生.&BND2)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0

SUM (全国分龄超生.&BND2)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区超生TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM 全国分龄政策生育.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM 全国分龄政策生育.&BND2,全国农村非农子女.政策生育,全国城镇非农子女.政策生育,全国农村农业子女.政策生育,全国城镇农业子女.政策生育 TO DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育


COPY TO ARRAY ARFND1 FIELDS 全国城镇分龄.&FND1
SUM 全国城镇生育.&BND2/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) TO 城镇实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国城镇分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国农村分龄.&FND1
SUM 全国农村生育.&BND2/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2) TO 农村实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国农村分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国非农分龄.&FND1
SUM 全国非农生育.&BND2/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)  TO 非农实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国非农分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国农业分龄.&FND1
SUM 全国农业生育.&BND2/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) TO 农业实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国农业分龄.&FND2<>0

SUM 全国分龄超生.&BND2 TO 超生S

*********************************************************************
DO 全国多种生育率
DO 记录全国各类夫妇政策内容及政策生育率	
SELECT 402
APPEND BLANK 
REPLACE 地区 WITH '全国',年份 WITH ND0,政策方案 WITH fa-ja,可实现TFR WITH 地区实现TFR0,政策生育率 WITH 地区政策TFR0,超生生育率 WITH 地区超生TFR,超生孩子数 WITH 超生S
REPLACE	DQ时期TFR  WITH 地区政策TFR0,CZ时期TFR WITH 城镇政策TFR0,FN时期TFR WITH 非农政策TFR0 ,NC时期TFR  WITH 农村政策TFR0,NY时期TFR  WITH 农业政策TFR0,QG累计迁移  WITH SQY
全国实现TFR0=可实现TFR
?'全国'-IIF(LEN('全国')=4,'  ',''),',',STR(ND0,4),',',fa-ja,',',STR(地区实现TFR0,6,2),',',STR(地区政策TFR0,6,2),',',STR(地区超生TFR,6,2),',',STR(超生s,6,2),',',;
'全国城镇:',STR(城镇实现TFR0,6,2),'全国非农:',STR(非农实现TFR0,6,2),'全国农村:',STR(农村实现TFR0,6,2),'全国农业:',STR(农业实现TFR0,6,2)
*************************
*	全国预测结果摘要	*
*************************
SELECT A
	SUM (全国城镇分龄.&MND2+全国城镇分龄.&FND2)/10000,(全国农村分龄.&MND2+全国农村分龄.&FND2)/10000 TO CZRK,NCRK
	CSH水平=CZRK/(NCRK+CZRK)*100
	迁移累计=0
QGQY=0
FOR CX=1 TO 5
	城乡B=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
	城乡QY=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))-'QYT'
	分龄预测='全国'-城乡B-'分龄'
	迁移人口='全国'-城乡B-IIF(CX=3.OR.CX=5,'子女','迁移')
	城乡子女='全国'-城乡B-'子女'
	全国生育='全国'-城乡B-'生育'
	全国人口='全国'-城乡B-'分龄'
	城乡死亡='全国'-城乡B-'死亡'
	全国迁移='全国'-城乡B-'迁移'
	SELE A
    SUM (&全国生育..&BX+&城乡子女..超生生育)/10000 TO 出生人口
    COPY TO ARRAY ARFND1 FIELDS  &全国人口..&FND1
    SUM &全国生育..&BX/((ARFND1(RECNO()-1)+&全国人口..&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+&全国人口..&FND2)<>0
    SUM (&全国生育..&BX+&城乡子女..超生生育)/((ARFND1(RECNO()-1)+&全国人口..&FND2)/2) TO 可实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+&全国人口..&FND2)<>0
	DO CASE
	CASE CX=1
		地区实现TFR0=可实现TFR0
	CASE CX=2
		城镇实现TFR0=可实现TFR0
	CASE CX=3
		非农实现TFR0=可实现TFR0
	CASE CX=4
		农村实现TFR0=可实现TFR0
	CASE CX=5
		农业实现TFR0=可实现TFR0
	ENDCASE
	IF CX<=2.OR.CX=4
		SUM (&全国迁移..&MND2+&全国迁移..&FND2)/10000 TO 	净迁入RK
	    SUM &城乡死亡..&MND2/10000 TO 死亡人口M
	    SUM &城乡死亡..&FND2/10000 TO 死亡人口F
		DO 死亡人口计算
  		DO 计算预期寿命
  		DO CASE 
  		CASE CX=2.OR.CX=3
  			CEM0=EM0
  			CEF0=EF0
  		CASE CX=4.OR.CX=5
  			XEM0=EM0
  			XEF0=EF0
  		ENDCASE
  	ELSE
		SELE A
		SUM &迁移人口..HJQY/10000 TO 净迁入RK
		死亡人口M=0
		死亡人口F=0
  	ENDIF
	QGQY=QGQY+净迁入RK
  	FIE1=IIF(CX=1,'DQ迁移合计',ALLTRIM(城乡B)-'迁移')
  	SELECT 402
  	REPLACE &FIE1 WITH 净迁入RK
  	??',',STR(净迁入RK,6,2),TIME()
	SELE IIF(CX=1,101,IIF(CX=2,102,IIF(CX=3,103,IIF(CX=4,104,105)))) &&101 全国分龄，102 全国城镇分龄，103 全国非农分龄，104 全国农村分龄，105 全国农业分龄
  		EM0=IIF(CX=2.OR.CX=3,CEM0,IIF(CX=4.OR.CX=5,XEM0,EM0))
  		EF0=IIF(CX=2.OR.CX=3,CEF0,IIF(CX=4.OR.CX=5,XEF0,EF0))
		DO 年龄分组和年龄结构
	SELE IIF(CX=1,181,IIF(CX=2,182,IIF(CX=3,183,IIF(CX=4,184,185)))) &&SELE 181 全国概要；SELE 182 全国城镇概要;SELE 183 全国非农概要;SELE 184 全国农村概要；SELE 185  全国农业概要
		DQB=IIF(CX=1,'全国',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
		SEL=STR(SELECT(),4)
		DO 预测结果摘要
		IF CX=1
?总人口,男,女 
			REPLACE 男 WITH 男+1975*男/总人口,女 WITH 女+1975*女/总人口,总人口 WITH 总人口+1975	&&此处加上2008年全国超过分省合计的1975万人
?总人口,男,女 
		ENDIF
		**??年份,地区,'纪录数：',RECCOUNT()
	IF ND0=ND2
		DO 主要极值
	ENDIF
ENDFOR	&&全国分城乡\农非概要循环
RETURN 