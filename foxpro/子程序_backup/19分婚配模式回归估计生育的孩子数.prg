PROCEDURE 分婚配模式回归估计生育的孩子数
*****政策实现比回归估计**************

SELECT A
SUM 地区分龄.&MND2+地区分龄.&FND2,地区城镇分龄.&MND2+地区城镇分龄.&FND2 TO ZRK,CZRK
CSH水平2=CZRK/ZRK

DO CASE
&& 主要差别，政策的实现比为1,按模型计算
CASE 现政比1<0.99
	可实现TFR0=地区政策TFR*(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR1-地区政策TFR))
	实现比=(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR1-地区政策TFR))
CASE 现政比1>1.01
	可实现TFR0=地区政策TFR*(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR-地区政策TFR1))
	实现比=(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR-地区政策TFR1))
CASE 现政比1=>0.99.AND.现政比1<=1.01
	可实现TFR0=地区政策TFR
	实现比=1
ENDCASE

可实现TFR0=IIF((地带='西部'.OR.地带='中部').AND.可实现TFR0 < 地区政策TFR,地区政策TFR,
		   IIF(地带='东部'.AND.可实现TFR0<MITFR, MITFR,
		   		可实现TFR0))
		   		
实现比=可实现TFR0/地区政策TFR
*****回归估计生育的孩子数**************
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女')))
	DO CASE 
	CASE  CX=1.OR.CX=3   &&农村
		NMDFR=N1D2N*实现比
		NFDMR=N2D1N*实现比
		NNR=NNFN*实现比
		*MN1=NY政策1		&&农业ZC1=IIF(NY政策1=1,'现行',IIF(NY政策1=2,'单独',IIF(NY政策1=3,'双独后',IIF(NY政策1=4,'单独后',IIF(NY政策1=5,'普二','双独后单独')))))
		城乡生育模式='农村生育模式'
	CASE  CX=2.OR.CX=4
		NMDFR=N1D2C*实现比
		NFDMR=N2D1C*实现比
		NNR=NNFC*实现比
		城乡生育模式='城镇生育模式'
		*MN1=FN政策1		&&非农ZC1=IIF(FN政策1=1,'现行',IIF(FN政策1=2,'单独',IIF(FN政策1=3,'双独后',IIF(FN政策1=4,'单独后',IIF(FN政策1=5,'普二','双独后单独')))))
	ENDCASE
	*****************************************************************************************************
	DO 各类夫妇分年龄生育孩子数
	REPL 超生DDB WITH DDB-政策DDB,超生NNB WITH NNB-政策NNB,超生NMDFB WITH NMDFB-政策NMDFB,超生NFDMB WITH NFDMB-政策NFDMB,超生释放B WITH 单独释放B+双非释放B-政策释放B,超生生育 WITH 超生DDB+超生NNB+超生NMDFB+超生NFDMB+超生释放B ALL 
	IF CX=4
		SELECT A
		REPLACE 地区农村超生.&BND2 WITH  地区农村非农子女.超生生育+地区农村农业子女.超生生育 ALL
		REPLACE 地区城镇超生.&BND2 WITH  地区城镇非农子女.超生生育+地区城镇农业子女.超生生育 ALL
		REPLACE 地区农业超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
		REPLACE 地区非农超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育 ALL
		REPLACE 地区分龄超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育+地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
   		    REPL 地区农业子女.超生生育 WITH 地区农村农业子女.超生生育+地区城镇农业子女.超生生育,;
		 地区非农子女.超生生育 WITH 地区农村非农子女.超生生育+地区城镇非农子女.超生生育,;
		 地区城镇子女.超生生育 WITH 地区城镇农业子女.超生生育+地区城镇非农子女.超生生育,;
		 地区农村子女.超生生育 WITH 地区农村农业子女.超生生育+地区农村非农子女.超生生育,;
		 地区子女.超生生育 WITH 地区农业子女.超生生育+地区非农子女.超生生育 ALL
		SUM 地区分龄超生.&BND2 TO 超生S
		SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	ENDIF
	******分释放模式堆积释放计算*****
	 
	 
	 AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
	 AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
	 AR突释模式=IIF(CX=1.OR.CX=3, 'AR农业突释模式','AR非农突释模式')
	 
	 
	 SUM 单独堆积+双非堆积  TO S堆积  FOR X<=49
	 IF S堆积<>0.and.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		 SELECT A
		 DO 不同政策实现程度下的堆积生育释放法
	 ENDIF
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类婚配夫妇分年龄生育率
	 DO 各类堆积释放数及后代分类
ENDFOR
RETURN 