SET DEFAULT TO F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\TMP
SET SAFE OFF
SET TALK OFF
SET DECI TO 4
CLOS DATA
CLEA ALL
CLEA
*SET ALTE TO AAA2 
*SET ALTE ON
*************************
*	参数和表结构准备	*
*************************
概率估算法='多龄概率法'
结婚率法='全员婚配'
TJ=3
*堆积估算法=IIF(TJ=1,'生育模式堆积估计法',IIF(TJ=2,'平均存活堆积估计法','存活一孩堆积估计法'))
ND1=2011
ND2=2130
DQ1=1
DQ2=31	&&31
*********************
USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\普查参数\分年龄死亡人口_1990普查
COPY TO ARRA AXM FIEL Man	&&死亡人口死亡当年存活年数(男)
COPY TO ARRA AXF FIEL Fan	&&死亡人口死亡当年存活年数(男)
*****定义全局变量*****
PUBL TFR,总人口T,总人口M,总人口F,年中人口,新增人口,人口增长L,T少年,T劳年,T青劳,T老年,T高龄,T老年M,T高龄M,T老年F,T高龄F,NLZ,FND2,MND2,;
	 育龄F15,婚育M15,育龄F20,婚育M20,长寿M,长寿F,T婴幼儿,XQ,XX,CZ,GZ,DX,JYM,JYF,寿命M,寿命F,长寿M0,长寿F0,EM0,EF0,MAND3,MATFR0,;
	 GB劳前,GB劳龄M,GB劳龄F,GB青劳,GB中劳M,GB中龄F,GB劳后M,GB劳后F,G劳年M,G老年M,G少年,G劳年F,G老年F,;
	 城FN男单GLF,城FN男单GLM,城FN女单GLF,城FN女单GLM,城FN双独GLF,城FN双独GLM,城NY男单GLF,城NY男单GLM,城NY女单GLF,;
	 城NY女单GLM,城NY双独GLF,城NY双独GLM,乡FN男单GLF,乡FN男单GLM,乡FN女单GLF,乡FN女单GLM,乡FN双独GLF,乡FN双独GLM,;
	 乡NY男单GLF,乡NY男单GLM,乡NY女单GLF,乡NY女单GLM,乡NY双独GLF,乡NY双独GLM,;
	 XNY未婚DF,XNY未婚DM,XNY未婚NF,XNY未婚NM,XFN未婚DF,XFN未婚DM,XFN未婚NF,XFN未婚NM,;
	 CNY未婚DF,CNY未婚DM,CNY未婚NF,CNY未婚NM,CFN未婚DF,CFN未婚DM,CFN未婚NF,CFN未婚NM,;
	 DQ死亡人口M,DQ死亡人口F,C死亡人口M,C死亡人口F,X死亡人口M,X死亡人口F,MINLM,MINLF,;
	 QXM,QXF,ST,AV,城超生,乡超生,迁入和0,迁出和0,迁移平衡和,迁入和M0,迁出和M0,迁入和F0,迁出和F0,;
	城镇政策TFR0,城镇实现TFR0,城镇实婚TFR0,农村政策TFR0,农村实现TFR0,农村实婚TFR0,地区政策TFR0,地区实现TFR0,地区实婚TFR0,;
	农业政策TFR0,农业实现TFR0,农业实婚TFR0,非农政策TFR0,非农实现TFR0,非农实婚TFR0,;
	城镇非农TFR0,农村非农TFR0,城镇农业TFR0,农村农业TFR0,民族B,死亡率修正系数,迁移修正系数0428,调整时机,政策,LL,现政比1,CSH水平1,CSH水平2,地区政策TFR1,QYB,CQYB,XQYB,NYQF,FNQF,NYQYGL,FNQYGL
PUBL N1D2C,N2D1C,NNFC,N1D2N,N2D1N,NNFN,DDFR,ND0,DQB,可实现TFR0,地区政策TFR,地区超生TFR,超生TFR,超生S,实现比,DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育,;
	 SNMDFB,SNFDMB,SNNB,A,DB,DDFR,DDFR,N1D2C,N1D2N,N2D1C,N2D1N,NNFC,NNFN,DS,NS,DMS,DFS,NMS,NFS,;
	 DDBS,DDBMS,DDBFS,DS30,DMS30,DFS30,NS30,NMS30,NFS30,DDBS30,DDBMS30,DDBFS30,DDS,再生D,再生L,地区再生育L,城镇再生育L ,非农再生育L,农村再生育L,农业再生育L,;
	农村农业一孩,农村非农一孩,城镇农业一孩,城镇非农一孩,SDD1,SNMDF1,SNFDM1,SNN1,SDD2,SNMDF2,SNFDM2,SNN2,SFRD,S双独B,无活产比,;
	农村农业独子父,农村农业独子母,城镇农业独子父,城镇农业独子母,农村非农独子父,农村非农独子母,城镇非农独子父,城镇非农独子母,;
	农村农业特扶父,农村农业特扶母,城镇农业特扶父,城镇农业特扶母,农村非农特扶父,农村非农特扶母,城镇非农特扶父,城镇非农特扶母,;
	 NMDFS,NFDMS,堆积单独,单独已2,堆积双非,双非已2,NNS,SWDM,SWDF,SWNM,SWNF,SWDDM,SWDDF,独生子SW,独生女SW,非独子SW,非独女SW,可二后SWM,可二后SWF,;
	 CZFN婚内TFR,CZNY婚内TFR,DDFR,DDFR,N1D2C,N1D2N,N2D1C,N2D1C,N2D1N,N2D1N,NCFN婚内TFR,NCNY婚内TFR,NNFC,NNFN,城镇TFR0,城镇非农TFR,城镇婚内TFR,;
	 城镇农业TFR,地区非农TFR,地区婚内TFR,地区农业TFR,地区政策TFR,非农婚内TFR,农村TFR0,农村非农TFR,农村婚内TFR,农村农业TFR,农业婚内TFR,;
	 CRK,XRK,FNRK,NYRK,城镇TFR0,城镇非农TFR,城镇农业TFR,城镇实婚TFR0,城镇实现TFR0,;
	 地区非农TFR,地区农业TFR,地区实婚TFR0,地区实现TFR0,地区政策TFR,S堆积,B1ND1,B2ND1,B1ND2,B2ND2,SFRD,;
	 非农实婚TFR0,非农实现TFR0,农村TFR0,农村非农TFR,农村农业TFR,农村实婚TFR0,农村实现TFR0,农业实婚TFR0,农业实现TFR0,MITFR,;
	 CSH水平,DQCSHYC,SDQQY,SQY,出生人口M,出生人口F,城镇婚内TFR,地区婚内TFR,非农婚内TFR,农村婚内TFR,农业婚内TFR,地带,DQQYND0,;
	 各地区生育率与迁移摘要,生育政策模拟摘要,基年子女,城乡总和迁移M,城乡总和迁移F,;
	 广东C总迁率M0,广东X总迁率M0,广东C总迁率F0,广东X总迁率F0,现行N1D2C,现行N2D1C,现行NNFC,现行N1D2N,现行N2D1N,现行NNFN,城镇迁移概率,农村迁移概率,城乡迁移概率

PUBL 	AR非农释放模式(40),AR非农缓释模式(40),AR非农突释模式(40),AR农业释放模式(40),AR农业缓释模式(40),AR农业突释模式(40),;
	ARDQ多种参数(31,16),AR子女婚配预测表结构(1),AR生育孩次(1),AR各类夫妇(1),ARFND1(115),DQJQL(DQ2),ARFND1(111)
DIMENSION QXM(111),QXF(111)
STORE 0 TO AR非农释放模式,AR非农缓释模式,AR非农突释模式,AR农业释放模式,AR农业缓释模式,AR农业突释模式,ARFND1,DQJQL,LL,现政比1,CSH水平1,CSH水平,地区政策TFR1,ARFND1
STORE 0 TO ARFND1,DQJQL,LL,现政比1,CSH水平1,CSH水平,地区政策TFR1,ARFND1,再生D
PUBL QXM(101),QXF(101),XSWRM(101),CSWRM(101),XSWRF(101),CSWRF(101)
ARFIE11='(AR1(RECN()+5,1)+AR1(RECN()+4,1)+AR1(RECN()+3,1)+AR1(RECN()+2,1)+AR1(RECN()+1,1)+AR1(RECN(),1)+'
ARFIE12='AR1(RECN()-1,1)+AR1(RECN()-2,1)+AR1(RECN()-3,1)+AR1(RECN()-4,1)+AR1(RECN()-5,1))/'
ARFIE21='(AR1(RECN()+5,2)+AR1(RECN()+4,2)+AR1(RECN()+3,2)+AR1(RECN()+2,2)+AR1(RECN()+1,2)+AR1(RECN(),2)+'
ARFIE22='AR1(RECN()-1,2)+AR1(RECN()-2,2)+AR1(RECN()-3,2)+AR1(RECN()-4,2)+AR1(RECN()-5,2))/'
ARFIE13='(AR1(RECN()+5,1)+AR1(RECN()+4,1)+AR1(RECN()+3,1)+AR1(RECN()+2,1)+AR1(RECN()+1,1)+AR1(RECN(),1)+'
ARFIE14='AR1(RECN()-1,1)+AR1(RECN()-2,1)+AR1(RECN()-3,1)+AR1(RECN()-4,1)+AR1(RECN()-5,1)+'
ARFIE15='AR1(RECN()+5,2)+AR1(RECN()+4,2)+AR1(RECN()+3,2)+AR1(RECN()+2,2)+AR1(RECN()+1,2)+AR1(RECN(),2)+'
ARFIE16='AR1(RECN()-1,2)+AR1(RECN()-2,2)+AR1(RECN()-3,2)+AR1(RECN()-4,2)+AR1(RECN()-5,2))'
ARFIE1='&ARFIE11'+'&ARFIE12'+'&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'
ARFIE2='&ARFIE21'+'&ARFIE22'+'&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'
ARFOR='&ARFIE13'+'&ARFIE14'+'&ARFIE15'+'&ARFIE16'
子女FIE1='X'-','-'合J分龄SYL'-','-'双D分龄SYL'-','-'单D分龄SYL'-','-'双N分龄SYL'-','-'FR1'-','-'FR2'-','-'独转非'-','-'双独B'-','-'NMDFB'-','-;
	'NFDMB'-','-'NNB'-','-'HJ'-','-'HJM'-','-'HJF'-','-'D'-','-'DM'-','-'DF'-','-'N'-','-'NM'-','-'NF'-','-'NF20'-','-'DF20'-','-;
	'NM20'-','-'DM20'-','-'QYHJ'-','-'QYHJM'-','-'QYHJF'-','-'DQY'-','-'DMQY'-','-'DFQY'-','-'NQY'-','-'NMQY'-','-'NFQY'-','-;
	'DDB'-','-'DDBM'-','-'DDBF'-','-'双独'-','-'NMDF'-','-'NFDM'-','-'NN'-','-'单独释放B'-','-'双非释放B'-','-'单独堆积'-','-;
	'堆积丈夫'-','-'女单堆积'-','-'男单堆积'-','-'已2单独'-','-'已2女单'-','-'已2男单'-','-'双非堆积'-','-;
	'已2双非'-','-'堆积比例'-','-'释放模式'-','-'双独概率M'-','-'双独概率F'-','-'男单概率M'-','-'男单概率F'-','-;
	'女单概率M'-','-'女单概率F'-','-'双非概率M'-','-'双非概率F'-','-'NF双独GLM'-','-'NF双独GLF'-','-'NF男单GLM'-','-;
	'NF男单GLF'-','-'NF女单GLM'-','-'NF女单GLF'-','-'NF双非GLM'-','-'NF双非GLF'
子女FIE2='X'-','-'合J分龄SYL'-','-'双D分龄SYL'-','-'单D分龄SYL'-','-'双N分龄SYL'-','-'FRD'-','-'FR'-','-'FR1'-','-'FR2'-','-;
'独转非'-','-'双独b'-','-'NMDFB'-','-'NFDMB'-','-'NNB'-','-;
'政策生育'-','-'政策双独b'-','-'政策NMDFB'-','-'政策NFDMB'-','-'政策NNB'-','-'政策释放B'-','-'再生育'-','-;
'超生生育'-','-'超生双独b'-','-'超生NMDFB'-','-'超生NFDMB'-','-'超生NNB'-','-'超生释放B'-','-;
'HJ'-','-'HJM'-','-'HJF'-','-'D'-','-'DM'-','-'DF'-','-'N'-','-'NM'-','-'NF'-','-'DDB'-','-'DDBM'-','-'DDBF'-','-'NMDFBS'-','-'NFDMBS'-','-'NNBS'-','-;
'夫妇合计'-','-'双独'-','-'NMDF'-','-'NFDM'-','-'NN'-','-;
'SWD'-','-'SWDM'-','-'SWDF'-','-'单独释放B'-','-'双非释放B'-','-;
'单独堆积'-','-'单独堆积15'-','-'单独堆积30'-','-'单独堆积35'-','-'堆积丈夫'-','-'女单堆积'-','-'男单堆积'-','-'已2单独'-','-'已2女单'-','-'已2男单'-','-;
'双非堆积'-','-'双非堆积15'-','-'双非堆积30'-','-'双非堆积35'-','-'已2双非'

STORE 0 TO 单放B2,非放B2,单独B2,农业单放B2,农业非放B2,农业单独堆积,农业双非堆积,非农单放B2,非农非放B2,非农单独堆积,非农双非堆积
STORE 0 TO 城镇非农单独堆积,城镇非农单放B2,城镇非农非放B2,城镇非农双非堆积,城镇农业单独堆积,城镇农业单放B2,城镇农业非放B2,城镇农业双非堆积,;
		   农村非农单独堆积,农村非农单放B2,农村非农非放B2,农村非农双非堆积,农村农业单独堆积,农村农业单放B2,农村农业非放B2,农村农业双非堆积
FOR F0=1 TO 2
	FIL1='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\'-IIF(F0=1,'方案简码','区域简码')
	USE &FIL1
	*f1=IIF(F0=1,1,2)
	f1=1
	DO WHILE f1<=FCOUNT()
		ARFI='AR'-FIELD(F1)
		SF='S'-FIELD(F1)
		FIE1=FIELD(F1)
		FIE2=FIELD(F1+1)
		FIE3=FIELD(F1+2)
		FIE4=FIELD(F1+3)
		IF F0=1
			COPY TO ARRAY &ARFI FIELDS &FIE1,&FIE2 FOR LEN(ALLTRIM(&FIE2))<>0
		ELSE
			COPY TO ARRAY &ARFI FIELDS &FIE1,&FIE2,&FIE3,&FIE4 FOR LEN(ALLTRIM(&FIE2))<>0
		ENDIF
		COUN TO &SF FOR LEN(ALLTRIM(&FIE2))<>0
		F1=F1+IIF(F0=1,2,4)
		*?ARFI,SF
	ENDDO
ENDFOR
****************************************************

DO 全国多地区迁移平衡多方案仿真
*QUIT 

**SET ALTE TO 
CLOS ALTE
*MODI COMM AAA2.TXT
SET SAFE ON
SET TALK ON
RETU



*****************************************************************************
PROC 全国多地区迁移平衡多方案仿真
调整方式时机='多地区渐进普二'
*********************
DO 创建基本表
*DO 全国迁移平衡表结构
*********************
DO 基础数据和基本参数
*************
*	预测	*
*************
FOR FN政策1=1 TO 1														&&全方案循环语句1
	非农ZC1=AR调整方案(FN政策1,1)
	农业ZC1=非农ZC1
	FNZC1=AR调整方案(FN政策1,2) 
	NYZC1=FNZC1
	FOR FN时机1=1 TO 1													&&全方案循环语句2
		非农时机1=IIF(非农ZC1='现行',0,AR调整时机(FN时机1,1))
		农业时机1=非农时机1
		FNSJ1=AR调整时机(FN时机1,2)
		NYSJ1=FNSJ1
		*FOR FN政策2=1 TO 4												&&全方案循环语句3
		FOR FN政策2=1 TO 2												&&5方案循环语句
			非农ZC2=AR调整方案(FN政策2,1) 
			农业ZC2=非农ZC2
			FNZC2=AR调整方案(FN政策2,2) 
			NYZC2=FNZC2
			*FOR FN时机2=IIF(FN政策2=1,1,FN时机1) TO IIF(FN政策2=1,1,9)	&&全方案循环语句4
			FOR FN时机2=IIF(FN政策2=1,1,FN时机1) TO 1
				非农时机2=IIF(非农ZC2='现行',0,AR调整时机(FN时机2,1))	
				农业时机2=非农时机2
				FNSJ2=AR调整时机(FN时机2,2)	
				NYSJ2=FNSJ2
				FOR FN政策3=FN政策2 TO 4								&&全方案循环语句5
					DO CASE												&&全方案循环控制语句
					CASE FN政策2=2.AND.FN政策3=3
						LOOP
					CASE FN政策2=3.AND.FN政策3=2
						LOOP
					ENDCASE
					非农ZC3=AR调整方案(FN政策3,1)
					农业ZC3=非农ZC3
					FNZC3=AR调整方案(FN政策3,2)
					NYZC3=FNZC3
					*FOR FN时机3=IIF(FN政策2=1,1,FN时机2+1) TO IIF(FN政策3=1,1,IIF(FN政策3=FN政策2,FN时机2+1,9))	&&全方案循环语句6
					FOR FN时机3=IIF(FN政策2=1,1,FN时机2+1) TO IIF(FN政策3=1,1,IIF(FN政策3=FN政策2,FN时机2+1,4))	&&5方案循环语句
						DO CASE											&&全方案循环控制语句
						CASE FN政策2=1.AND.FN时机2=1.AND.FN政策3>1
							LOOP
						CASE FN时机3>9
							LOOP
						CASE FN政策2>FN政策3.OR.FN时机2>FN时机3
							LOOP
						ENDCASE
						**********以下2012.08修改,***************
						非农时机3=IIF(非农ZC3='现行',0,IIF(FNZC3=FNZC2,非农时机2,AR调整时机(FN时机3,1)))
						*农业时机3=非农时机3
						非农ZC3=IIF(非农时机3=非农时机2,非农ZC2,ALLT(AR调整方案(FN政策3,1)))	&&2011.10.04+
						FNZC3=IIF(非农时机3=非农时机2,FNZC2,ALLT(AR调整方案(FN政策3,2)))	&&2011.10.04+
						FNSJ3=IIF(非农时机3=非农时机2,FNSJ2,AR调整时机(FN时机3,2))
						农业ZC3=非农ZC3
						NYZC3=FNZC3
						NYSJ3=FNSJ3
						**********以上2011.10.04加***************
						FA=ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4)))-;
						   IIF(FNZC2=FNZC1,'',ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))))-;
						   IIF(FNZC3=FNZC2,'',ALLT(非农ZC3)-IIF(非农时机3=0,'',ALLT(STR(非农时机3,4))))
						FA2='非'-ALLT(FNZC1)-ALLT(FNSJ1)-ALLT(FNZC2)-ALLT(FNSJ2)-ALLT(FNZC3)-ALLT(FNSJ3)-'农'-ALLT(NYZC1)-ALLT(NYSJ1)-ALLT(NYZC2)-ALLT(NYSJ2)-ALLT(NYZC3)-ALLT(NYSJ3)
						*?FA,FN政策2,FN时机2,FN政策3,FN时机3	&&,fa2
						FA3='农'-IIF(ALLT(NYZC2)-ALLT(NYSJ2)='X11','X行',ALLT(NYZC2)-ALLT(NYSJ2))-IIF(NYZC2=NYZC3.AND.NYSJ2=NYSJ3,'',ALLT(NYZC3)-ALLT(NYSJ3))-'_'-;
							'非'-IIF(ALLT(FNZC2)-ALLT(FNSJ2)='X11','X行',ALLT(FNZC2)-ALLT(FNSJ2))-IIF(FNZC3=FNZC2.AND.FNSJ2=FNSJ3,'',ALLT(FNZC3)-ALLT(FNSJ3))
						
						*?FA,FN政策2,FN时机2,FN政策3,FN时机3	,fa2
						?'*	',FA2,FA
						FOR QY=3 TO 3	&&3
						*FOR QY=1 TO 1	&&3
							JA=AR迁移强度(QY,1)	&& S迁移强度&&JA=IIF(QY=1,'无迁',IIF(QY=2,'低迁',IIF(QY=3,'中迁','高迁')))
							JA2=AR迁移强度(QY,2)
							FOR ZC实现=1 TO 2	&&1
								实现方式=IIF(ZC实现=1,'政策生育',IIF(ZC实现=2,'可能生育','意愿生育'))	
								FOR SFMS=2 TO 2	&&5
									释放模式=IIF(SFMS=1,'意愿模式',IIF(SFMS=2,'分释模式',IIF(SFMS=3,'缓释模式',IIF(SFMS=4,'突释模式','正常模式'))))
									实现程度与释放模式=实现方式-'\'-释放模式	&&-'\'
									生育政策模拟摘要='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00摘要\生育政策模拟摘要'-FA2-JA2-释放模式
							   		FOR ND0=ND1 TO ND2
							 	 		*****通用年份字段*****
										IF ND0=ND2
											DO 创建生育政策模拟摘要表结构
										ENDIF
										Q=IIF(ND0<2101,'Q'+STR(ND0,4), 'Q2100')    &&死亡概率准备
								        TND1='T'+STR(ND0-1,4)
								        MND1='M'+STR(ND0-1,4)
								        FND1='F'+STR(ND0-1,4)
								        TND2='T'+STR(ND0,4)
								        MND2='M'+STR(ND0,4)
								        FND2='F'+STR(ND0,4)
								        B1ND1='B1'+STR(ND0-1,4)
								        B2ND1='B2'+STR(ND0-1,4)
								        B1ND2='B1'+STR(ND0,4)
								        B2ND2='B2'+STR(ND0,4)
								        *QYMND2='M'+IIF(ND0<=2100,STR(ND0,4),'2100')
								        *QYFND2='F'+IIF(ND0<=2100,STR(ND0,4),'2100')
								        BND2='B'+STR(ND0,4)
										*QCM='QCM'-IIF(ND0<=2100,STR(ND0,4),'2100')
										*QCF='QCF'-IIF(ND0<=2100,STR(ND0,4),'2100')
										*QRM='QRM'-IIF(ND0<=2100,STR(ND0,4),'2100')
										*QRF='QRF'-IIF(ND0<=2100,STR(ND0,4),'2100')
								        BX='B'+STR(ND0,4)	&&分年龄生育孩子数
								        FX='F'+STR(ND0,4)	&&分年龄妇女人数
							   			迁移累计=0
										*?'DQB','农村农业TFR','农村非农TFR','城镇农业TFR','城镇非农TFR','城镇非农TFR','地区农业TFR','地区非农TFR','城镇TFR0','地区政策TFR'
										*?'地区','ND0','CQYB','XQYB','C总迁率M','C总迁率F','X总迁率M','X总迁率F',
										*?'DQB','ND0','终身生育率:','N1D2C','N2D1C','NNFC','N1D2N','N2D1N','NNFN'
										*?'地区','ND0','年份','出生C=出生-出生一孩-出生二孩','出生C2=出生-出生男-出生女','生育率','自增率','净迁入数','城镇人口比'
										*IF QY>1
							    		*************************
							    		*	全国分省迁移初算	*
							    		*************************
							    		DO 分省迁移初算		&&迁移参数的格式进行了较大修改,也考虑了无迁移的情况（无迁移时，CQYB=0,XQYB=0）
							    		*************************
										IF qy>1
											DO 全国迁移平衡
										ENDIF 
							    		*************************
							    		DO 全国预测数据表
							    		*************************
							    		STORE 0 TO SQ入0,SQ出0,SQY,QGRKLJ
							    		FOR DQX=DQ1 TO DQ2	&&DQS
										    DQB=ALLT(ARDQ多种参数(DQX,2))	&&省代码,DQ,出生XBB05,出生XBB00
										    DQ夹=ALLT(ARDQ多种参数(DQX,1))-DQB
								    		*Csh水平=IIF(ND0=ND1,ARDQ多种参数(DQX,9),csh水平)
											*IF QY=1
											*	DO 分省无迁婚配预测表
											*ENDIF 
											*********************
											*	地区基本参数	*
											*********************
											******迁移量*****
											*地区历年迁移估计='H:\D\数据库（全国）\D人口\00各地区\迁移\'-DQB-'城乡人口迁移估计96_09'
											地区历年迁移估计='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\年鉴数据\各地区城乡人口迁移估计96_10.DBF '
											*SELECT 301	&&USE H:\D\数据库（全国）\D人口\00各地区\迁移\各地区城乡人口迁移估计96_09.DBF
											*COPY TO &地区历年迁移估计 FOR 地区=DQB
											******基本资源*****
											SELECT 201
												LOCA FOR 地区=DQB
												土地MJ=分省代码.土地面积		&&单位:千公顷
												耕地MJ=分省代码.耕地06			&&单位:千公顷
												水资源=分省代码.水资源均量		&&单位:亿立方米
												城镇无活产比=无活产比例.城镇活产0B
												乡村无活产比=无活产比例.乡村活产0B
												C初婚NLM=初婚年龄.城市M
												C初婚NLF=初婚年龄.城市F
												X初婚NLM=初婚年龄.乡村M
												X初婚NLF=初婚年龄.乡村F
											SUM 分省代码.土地面积,分省代码.水资源均量,分省代码.耕地06 TO 全国土地MJ,全国水ZY,全国耕地MJ&&单位:亿立方米
									        *****出生性别比参数*****
									        DO CASE 
									        CASE ARDQ多种参数(DQX,3)>ARDQ多种参数(DQX,4)	&& 省代码,DQ,出生XBB10,出生XBB05
									        	 XBB0=ARDQ多种参数(DQX,3)-(ARDQ多种参数(DQX,3)-ARDQ多种参数(DQX,4))/10*(ND0-2010)	&&按20年降至1990年水平的速度下降,低于108时保持不变
									        CASE ARDQ多种参数(DQX,3)<ARDQ多种参数(DQX,4)	&& 省代码,DQ,出生XBB10,出生XBB05
									        	 XBB0=ARDQ多种参数(DQX,3)-(ARDQ多种参数(DQX,4)-ARDQ多种参数(DQX,3))/10*(ND0-2010)	&&按1990_2000年的速度降低,低于108时保持不变
											ENDCASE
											XBB=(IIF(XBB0>108,XBB0,108))/100
									    	*********************
											DO 地区基础数据和预测文件
									    	*********************
											*DIMENSION AR非农释放模式(40),AR非农缓释模式(40),AR非农突释模式(40),AR农业释放模式(40),AR农业缓释模式(40),AR农业突释模式(40)
											*STORE 0 TO AR非农释放模式,AR非农缓释模式,AR非农突释模式,AR农业释放模式,AR农业缓释模式,AR农业突释模式
											*********************
											DO 释放模式确定
									    	*********************
									    	RELE NYQM,NYQF,FNQM,FNQF
											COPY TO ARRA NYQM FIEL 农村死亡概率M.&Q
											COPY TO ARRA NYQF FIEL 农村死亡概率F.&Q
											COPY TO ARRA FNQM FIEL 城镇死亡概率M.&Q
											COPY TO ARRA FNQF FIEL 城镇死亡概率F.&Q
									    	*****死亡率修正*****
										    SWB=1
								    		SELECT A
									    	RELE CND1,XND1
									    	COPY TO ARRA CND1 FIEL 地区城镇分龄.&MND1,地区城镇分龄.&FND1
									    	COPY TO ARRA XND1 FIEL 地区农村分龄.&MND1,地区农村分龄.&FND1
											REPL 地区城镇死亡.&MND2 WITH CND1(RECN(),1)*FNQM(RECN()),地区城镇死亡.&FND2 WITH CND1(RECN(),2)*FNQF(RECN()) ALL
											REPL 地区农村死亡.&MND2 WITH XND1(RECN(),1)*NYQM(RECN()),地区农村死亡.&FND2 WITH XND1(RECN(),2)*NYQF(RECN()) ALL
											REPL 地区死亡.&MND2 WITH 地区城镇死亡.&MND2+地区农村死亡.&MND2,地区死亡.&FND2 WITH 地区城镇死亡.&FND2+地区农村死亡.&FND2 ALL
											*******************************************************************************************************************************************
											*********************
											*	终身政策生育率	*
											*********************
											IF FN政策2=FN政策1.OR.FN政策3=FN政策2.OR.非农时机2=非农时机3	&&'多地区单时机'
												DO 单时机调整终身政策生育率
											ELSE 
												DO 渐进普二终身政策生育率			&&'多地区渐进普二'
											ENDIF 
											*********************
											SELECT A
											DO 年龄移算
											*************************
											*	计算各类夫妇人数	*
											*************************
											DO CASE
											CASE 概率估算法='同龄概率法'
												DO 常住人口同龄概率法
											CASE 概率估算法='多龄概率法'
												*DO 两性平衡多龄概率法
												DO 女性最大多龄概率法
											ENDCASE
											IF ND0=>调整时机
												IF FN政策2=FN政策1.OR.FN政策3=FN政策2.OR.非农时机2=非农时机3	&&'多地区单时机'
													DO 单时机调整堆积夫妇估计与剥离
												ELSE 
													DO 渐进普二堆积夫妇估计与剥离
												ENDIF 
											ENDIF
											****************************
											*	分婚配模式生育孩子数   *
											****************************
											实现比=1
											DO 分婚配模式按政策生育的孩子数		&&含“各类夫妇分年龄生育孩子数”、“DO 不同政策实现程度下的堆积生育释放法”、“DO 各类堆积释放数及后代分类”
											****************************
											SELE A
											***********************
											DO 各类夫妇生育孩子数  				&&第一次计算是按政策生育的孩子数
											DO 分龄人口合计
											***********************
											RELEASE ARFND1
											COPY TO ARRAY ARFND1 FIELDS 地区分龄.&FND1
											SUM  地区生育.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) ;
												 TO 地区政策TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地区生育率
											*****************************
											*	政策实现程度和超生估计	*
											*****************************
											DO CASE	
											CASE 实现方式='可能生育'
												DO 分婚配模式可能生育生育的孩子数	&&含“各类夫妇分年龄生育孩子数”、“DO 不同政策实现程度下的堆积生育释放法”、“DO 各类堆积释放数及后代分类”
											CASE 实现方式='意愿生育'
												DO 分婚配模式按意愿生育的孩子数		&&含“各类夫妇分年龄生育孩子数”、“DO 不同政策实现程度下的堆积生育释放法”、“DO 各类堆积释放数及后代分类”
											ENDCASE  	
											DO 分龄人口合计
											*********各地区人口现状评估摘要*****************
											*******************************
											*DO 独生子女和非独生子女人数调整（生二孩高峰年龄法）
											DO 独生子女和非独生子女人数调整（孩次递进法）
											*******************************
											DO 分龄一孩父母和特扶父母估计
											DO 地区特扶父母统计
											*********地区城镇迁移、地区农村迁移已经分别分配于城镇子女、农村子女各类人口中，以下即含迁移、分婚配模式生育后的城乡及地区总人口********
											********************************
											*DO 城乡独生子女和非独生子女整理
											********************************
											******调整后重新合计*******
											DO 分龄人口合计
											DO 各类夫妇生育孩子数  &&&第二次计算是可实现的生育孩子数,各类夫妇可实现生育孩子数									
											*****地区分年龄各类夫妇子女合计
											SELECT A 
											FOR FI1=10 TO 180	&&89
												FIE1=AR子女婚配预测表结构(FI1,1)
					   							REPL 地区农业子女.&FIE1 WITH 地区农村农业子女.&FIE1+地区城镇农业子女.&FIE1,;
												 地区非农子女.&FIE1 WITH 地区农村非农子女.&FIE1+地区城镇非农子女.&FIE1,;
												 地区城镇子女.&FIE1 WITH 地区城镇农业子女.&FIE1+地区城镇非农子女.&FIE1,;
												 地区农村子女.&FIE1 WITH 地区农村农业子女.&FIE1+地区农村非农子女.&FIE1,;
												 地区子女.&FIE1 WITH 地区农业子女.&FIE1+地区非农子女.&FIE1 ALL
											ENDFOR
											*****妇女分龄存活子女动态调整*****
											FOR CX=1 TO 2
												城乡1=IIF(CX=1,'城镇','农村')
												FOR NF1=1 TO 2
													农非=IIF(NF1=1,'农业','非农')
													婚配子女='地区'-城乡1-农非-'子女'
													存活子女=SUBSTR(城乡1,1,2)-SUBSTR(农非,1,2)-'存活子女'
													REPLACE &存活子女..&B1ND2 WITH &存活子女..&B1ND2+(&婚配子女..双独B1+&婚配子女..单独B1+&婚配子女..NNB1)-(&婚配子女..双独B2+&婚配子女..单独B2+&婚配子女..NNB2+&婚配子女..单独释放B+&婚配子女..双非释放B),;
														&存活子女..&B2ND2 WITH &存活子女..&B2ND2+&婚配子女..双独B2+(&婚配子女..单独B2+&婚配子女..NNB2+&婚配子女..单独释放B+&婚配子女..双非释放B) ALL 
												ENDFOR
											ENDFOR 
											REPLACE 城镇存活子女.&B1ND2 WITH 城农存活子女.&B1ND2+城非存活子女.&B1ND2,农村存活子女.&B1ND2 WITH 农农存活子女.&B1ND2+农非存活子女.&B1ND2,;
													非农存活子女.&B1ND2 WITH 城非存活子女.&B1ND2+农非存活子女.&B1ND2,农业存活子女.&B1ND2 WITH 城农存活子女.&B1ND2+农农存活子女.&B1ND2,;
													地区存活子女.&B1ND2 WITH 城镇存活子女.&B1ND2+农村存活子女.&B1ND2 ALL
											REPLACE 城镇存活子女.&B2ND2 WITH 城农存活子女.&B2ND2+城非存活子女.&B2ND2,农村存活子女.&B2ND2 WITH 农农存活子女.&B2ND2+农非存活子女.&B2ND2,;
													非农存活子女.&B2ND2 WITH 城非存活子女.&B2ND2+农非存活子女.&B2ND2,农业存活子女.&B2ND2 WITH 城农存活子女.&B2ND2+农农存活子女.&B2ND2,;
													地区存活子女.&B2ND2 WITH 城镇存活子女.&B2ND2+农村存活子女.&B2ND2 ALL
											*****多种生育率*****
											DO 地区多种政策和可实现时期生育率
											********************
											SELECT 402	&&各地区生育率与迁移摘要
											APPEND BLANK  	
											REPLACE 年份 WITH ND0,地区 WITH DQB,政策方案 WITH fa-ja,可实现TFR WITH 地区实现TFR0,政策生育率 WITH 地区政策TFR0,超生生育率 WITH 地区超生TFR,超生孩子数 WITH 超生S
											*?实现方式,释放模式,DQB-IIF(LEN(DQB)=4,'  ',''),',',STR(ND0,4),',',fa-ja,',',STR(地区实现TFR0,4,2),',',STR(地区政策TFR0,4,2),',',STR(地区超生TFR,4,2),',',STR(超生s,8,0),','
											?实现方式,释放模式,DQB-IIF(LEN(DQB)=4,'  ',''),STR(ND0,4),fa-ja,STR(地区实现TFR0,6,3),STR(地区政策TFR0,6,3),STR(地区超生TFR,6,3),STR(超生s,8,0),','
											***********考贝地区分年龄婚配结果***************
											DO 考贝地区分年龄婚配结果
											************************************************
											*****记录各类夫妇政策内容及政策生育率,“农村NY概率”等“概率”字段,均为双独概率 
											DO 记录各类夫妇政策内容及政策生育率
											**************************************************
											IF ND0=ND2
												DO 生育孩次合计
											ENDIF
											*?ND0,农村农业TFR,农村非农TFR,农村TFR0,城镇农业TFR,城镇非农TFR,城镇TFR0,地区农业TFR,地区非农TFR,地区政策TFR
									 	 	*********************
									 	 	*	预测结果摘要	*
									 	 	*********************
									 	 	DO 地区城乡预测结果摘要
									 	 	************************
											*??'本地区合计',SDQQY,DQQY,SDQQY-DQQY,'地区累计:',SQY
											*??STR(SDQQY,8,2),',',STR(SQY,8,2),TIME()
											??',',STR(SQY,8,2),TIME()
											SELECT 402
											REPLACE 本地迁移  WITH SDQQY,QG累计迁移  WITH SQY
											*?ALLT(STR(FN政策1,1)),ALLT(STR(FN时机2,1)),ALLT(STR(NY政策1,1)),ALLT(STR(NY时机2,1)),ALLT(STR(QY,1)),'*		结果文件：',DBF()
											*?MS,'双独2',独女政策0,独男政策0,双非政策0,'GL='+STR(GL11,4,2),'城:'+str(TFRC,6,4),'乡:'+str(TFRX,6,4),'地区',地区TFR 
											*****************
											DO 逐层全国地区累计
											*****************
										ENDFOR	&&多地区单时机年度循环结束
										*********计算该年度全国概要************
										SELECT A
										DO 全国一孩父母和特扶父母统计
										**************************
										DO 年度全国概要
										**************************
									ENDFOR &&多地区各年度结束
								ENDFOR &&理论政策生育率和可实现生育率
							ENDFOR	&&迁移循环
						ENDFOR	&&FN时机3
					ENDFOR	&&FN政策3
				ENDFOR	&&FN时机2
			ENDFOR	&&FN政策2
		ENDFOR	&&FN时机1
	ENDFOR	&&FN政策1
ENDFOR	&&释放模式
**SET ALTE TO 
CLOS ALTE
SELE 402
*BROW FOR 地区='全国'
CLOS DATA
RETU





PROCEDURE 创建生育政策模拟摘要表结构
*********************
*	创建基本表结构	*
*********************
CREA TABL &生育政策模拟摘要 FREE (堆释模式 C(8),概率估算法 C(10),实现程度 C(10),政策1 C(6),时机1 N(4),政策2 C(6),时机2 N(4),政策3 C(6),时机3 N(4),迁移方案 C(2),生育方案 C(40),;
地区别 C(6),城乡别 C(4),最大规模ND N(4),最大规模 N(10,2),最小规模ND N(4),最小规模 N(10,2),;
最大迁移ND N(4),最大迁移 N(10,2),最小迁移ND N(4),最小迁移 N(10,2),最高TFRND N(4),最高TFR N(8,3),;
总人口20 N(10,2),出生20 N(10,2),出生率20 N(10,2),生育率20 N(10,2),净迁入率20 N(10,2),总增长率20 N(10,2),城市化20 N(10,2))
USE
SELE 401
	USE &生育政策模拟摘要
RETURN 

************************
PROCEDURE 创建基本表
CREA TABL 地区生育率与迁移摘要 	FREE (地区 C(6),年份 N(4),政策方案 C(20),可实现TFR N(8,3),政策生育率 N(8,3),超生生育率 N(8,3),超生孩子数 N(8,2),;
FN女单终身 N(8,2),FN男单终身 N(8,2),FN双非终身 N(8,2),NY女单终身 N(8,2),NY男单终身 N(8,2),NY双非终身 N(8,2),;
DQ时期TFR N(8,3),CZ时期TFR N(8,3),FN时期TFR N(8,3),NC时期TFR N(8,3),NY时期TFR N(8,3),;
DQ迁移合计 N(8,2),城镇迁移 N(8,2),非农迁移 N(8,2),农村迁移 N(8,2),农业迁移 N(8,2),本地迁移 N(8,2),QG累计迁移 N(8,2))
USE 
*****迁移修正系数*****
*迁移修正系数='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城乡迁移修正系数__基于留存率的分析'
*迁移修正系数='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\迁移概率\各地区城乡迁移修正系数'
*CREATE TABLE &迁移修正系数 FREE (省代码 C(2),地区 C(6),C迁06B N(8,5),X迁06B N(8,5),C迁07B N(8,5),X迁07B N(8,5),C迁08B N(8,5),X迁08B N(8,5),C迁09B N(8,5),X迁09B N(8,5))
*APPEND FROM F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\\数据库（全国）\全国分省代码
*****死亡修正系数*****
死亡率修正系数='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\死亡概率\各地区死亡概率修正系数'
CREATE TABLE &死亡率修正系数 FREE (省代码 C(2),地区 C(6),SWL修正B N(8,5))
APPEND FROM F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\分省代码
USE 
迁移率修正系数='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区迁移概率修正系数'
*CREATE TABLE &迁移率修正系数 FREE (省代码 C(2),地区 C(6),QYL修正B N(8,5))
*APPEND FROM F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\分省代码
*USE 
*****分婚配模式夫妇基本表结构*****
CREA TABL 婚配预测表结构 FREE (X N(4),合J分龄SYL N(10,4),双D分龄SYL N(10,4),单D分龄SYL N(10,4),双N分龄SYL N(10,4),FRD N(10,4),FR N(10,6),FR1 N(10,6),FR2 N(10,6),;
	B N(10,0),双独B N(10,0),单独B N(8,0),NMDFB N(10,0),NFDMB N(10,0),NNB N(10,0),;
	B1 N(10,0),双独B1 N(10,0),单独B1 N(8,0),NMDFB1 N(10,0),NFDMB1 N(10,0),NNB1 N(10,0),;
	B2 N(10,0),双独B2 N(10,0),单独B2 N(8,0),NMDFB2 N(10,0),NFDMB2 N(10,0),NNB2 N(10,0),;
	政策生育 N(10,0),政策双独B N(10,0),政策NMDFB N(10,0),政策NFDMB N(10,0),政策NNB N(10,0),政策释放B N(10,0),再生育 N(10,4),;
	超生生育 N(10,0),超生双独B N(10,0),超生NMDFB N(10,0),超生NFDMB N(10,0),超生NNB N(10,0),超生释放B N(10,0),;
	HJ N(12,0),HJM N(12,0),HJF N(12,0),D N(12,0),DM N(12,0),DF N(12,0),N N(12,0),NM N(12,0),NF N(12,0),独转非 N(10,0),DDB N(10,0),DDBM N(10,0),DDBF N(10,0),NMDFBS N(10,0),NFDMBS N(10,0),NNBS N(10,0),;
	夫妇合计 N(10,0),双独 N(10,0),NMDF N(10,0),NFDM N(10,0),NN N(10,0),单独堆积 N(10),双非堆积 N(10),一女户B N(8),一女户B1 N(8),一女户B2 N(8),;
	QYHJ N(12,0),QYHJM N(12,0),QYHJF N(12,0),DQY N(12,0),DMQY N(12,0),DFQY N(12,0),NQY N(12,0),NMQY N(12,0),NFQY N(12,0),DDBQY N(10,0),DDBMQY N(10,0),DDBFQY N(10,0),;
	DM0 N(10,0),DF0 N(10,0),NM0 N(10,0),NF0 N(10,0),MIDM0 N(10,0),MIDF0 N(10,0),MINM0 N(10,0),MINF0 N(10,0),;
	DD_F N(10,0),NMDF_F N(10,0),NFDM_F N(10,0),NN_F N(10,0),DD_M N(10,0),NMDF_M N(10,0),NFDM_M N(10,0),NN_M N(10,0),;
	DD非F N(10,0),NMDF非F N(10,0),NFDM非F N(10,0),NN非F N(10,0),DD非F_F N(10,0),NMDF非F_F N(10,0),NFDM非F_F N(10,0),NN非F_F N(10,0),DD非F_M N(10,0),NMDF非F_M N(10,0),NFDM非F_M N(10,0),NN非F_M N(10,0),;
	DD农F N(10,0),NMDF农F N(10,0),NFDM农F N(10,0),NN农F N(10,0),DD农F_F N(10,0),NMDF农F_F N(10,0),NFDM农F_F N(10,0),NN农F_F N(10,0),DD农F_M N(10,0),NMDF农F_M N(10,0),NFDM农F_M N(10,0),NN农F_M N(10,0),;
	DD_CFXF N(10,0),NMDF_CFXF N(10,0),NFDM_CFXF N(10,0),NN_CFXF N(10,0),DD_CFXN N(10,0),NMDF_CFXN N(10,0),NFDM_CFXN N(10,0),NN_CFXN N(10,0),;
	DD_CNXF N(10,0),NMDF_CNXF N(10,0),NFDM_CNXF N(10,0),NN_CNXF N(10,0),DD_CNXN N(10,0),NMDF_CNXN N(10,0),NFDM_CNXN N(10,0),NN_CNXN N(10,0),;
	DD_XFCF N(10,0),NMDF_XFCF N(10,0),NFDM_XFCF N(10,0),NN_XFCF N(10,0),DD_XFCN N(10,0),NMDF_XFCN N(10,0),NFDM_XFCN N(10,0),NN_XFCN N(10,0),;
	DD_XNCF N(10,0),NMDF_XNCF N(10,0),NFDM_XNCF N(10,0),NN_XNCF N(10,0),DD_XNCN N(10,0),NMDF_XNCN N(10,0),NFDM_XNCN N(10,0),NN_XNCN N(10,0),;
	DD0 N(10,0),DM10 N(10,0),DM20 N(10,0),DM30 N(10,0),DM40 N(10,0),DF10 N(10,0),DF20 N(10,0),DF30 N(10,0),DF40 N(10,0),;
	NM10 N(10,0),NM20 N(10,0),NM30 N(10,0),NM40 N(10,0),NF10 N(10,0),NF20 N(10,0),NF30 N(10,0),NF40 N(10,0),;
	FR201 N(10,8),FR202 N(10,8),FR203 N(10,8),FR204 N(10,8),FR205 N(10,8),FR206 N(10,8),FR207 N(10,8),FR208 N(10,8),FR209 N(10,8),FR210 N(10,8),FR211 N(10,8),FR212 N(10,8),;
	SWD N(12,0),SWDM N(12,0),SWDF N(12,0),单独释放B N(10),双非释放B N(10),一女H释放B N(8),;
	单独堆积15 N(10),单独堆积30 N(10),单独堆积35 N(10),堆积丈夫 N(10),女单堆积 N(10),男单堆积 N(10),已2单独 N(10),已2女单 N(10),已2男单 N(10),双非堆积15 N(10),双非堆积30 N(10),双非堆积35 N(10),已2双非 N(10),;
	堆积比例 N(8,4),释放模式 N(8,4),双独概率M N(8,4),双独概率F N(8,4),男单概率M N(8,4),男单概率F N(8,4),女单概率M N(8,4),女单概率F N(8,4),双非概率M N(8,4),双非概率F N(8,4),;
	NF双独GLM N(8,4),NF双独GLF N(8,4),NF男单GLM N(8,4),NF男单GLF N(8,4),NF女单GLM N(8,4),NF女单GLF N(8,4),NF双非GLM N(8,4),NF双非GLF N(8,4))
	FOR NL=0 TO 110
	   APPE BLANK
	   REPL X WITH NL
	ENDFOR
AFIEL(AR子女婚配预测表结构)
*****分婚配模式生育政策参数基本表结构*****
CREA TABL 政策及政策生育率 FREE (年份 N(4),非农政策1 C(10),农业政策1 C(10),非农政策2 C(10),农业政策2 C(10),非农政策3 C(10),农业政策3 C(10),;
	非农双独 N(6,2),农业双独 N(6,2),非农单独 N(6,2),农业单独 N(6,2),非农女单 N(6,2),;
	农业女单 N(6,2),非农男单 N(6,2),农业男单 N(6,2),非农双非 N(6,2),农业双非 N(6,2),;
	农村NYTFR N(8,3),农村FNTFR N(8,3),农村TFR N(8,3),城镇NYTFR N(8,3),城镇FNTFR N(8,3),城镇TFR N(8,3),地区NYTFR N(8,3),地区FNTFR N(8,3),地区TFR N(8,3),;
	NCNY婚TFR N(8,3),NCFN婚TFR N(8,3),NC婚TFR N(8,3),CZNY婚TFR N(8,3),CZFN婚TFR N(8,3),CZ婚TFR N(8,3),QSNY婚TFR N(8,3),QSFN婚TFR N(8,3),QS婚TFR N(8,3))	&&农村NY概率等"概率"字段,均为双独概率 
	FOR ND=2010 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
CREA TABL 夫妇及子女表结构 FREE (年份 N(4),非农政策1 C(10),农业政策1 C(10),非农政策2 C(10),农业政策2 C(10),非农政策3 C(10),农业政策3 C(10),;
	TFR N(10, 4),双独TFR N(10,4),单独TFR N(10,4),双非TFR N(10,4),;
	非农双独 N(6,2),非农男单 N(6,2),非农女单 N(6,2),非农双非 N(6,2),农业双独 N(6,2),农业男单 N(6,2),农业女单 N(6,2),农业双非 N(6,2),;
	合计夫妇数 N(10), 双独夫妇数 N(10), 单独夫妇数 N(10), 女单夫妇数 N(10),男单夫妇数 N(10),单独堆积 N(10),已2单独 N(10),双非夫妇数 N(10),双非堆积 N(10),已2双非 N(10),;
	双独生育数 N(10),单独生育数 N(10),女单生育数 N(10),男单生育数 N(10), 双非生育数 N(10),C超生 N(10,0),X超生 N(10,0), ;
	D N(10), DM N(10), DF N(10), N N(10), NM N(10), NF N(10),可二后 N(10),DDBM N(10), DDBF N(10),;
	独生子女30 N(10), 独生子30 N(10), 独生女30 N(10), 非独子女30 N(10), 非独子30 N(10), 非独女30 N(10), 可二后30 N(10),可二后30M N(10), 可二后30F N(10),;
	独生子女SW N(10), 独生子SW N(10), 独生女SW N(10), 非独子女SW N(10), 非独子SW N(10), 非独女SW N(10), 可二后SW N(10),可二后SWM N(10), 可二后SWF N(10),;
	一孩父母 N(10), 一孩父 N(10), 一孩母 N(10),特扶 N(10), 特扶M N(10), 特扶F N(10),;
	未婚F N(10,2),未婚M N(10,2),未婚DF N(10,2),未婚DM N(10,2),未婚NF N(10,2),未婚NM N(10,2),农业未婚F N(10,2),农业未婚M N(10,2),非农未婚F N(10,2),非农未婚M N(10,2),;
	农业未婚DF N(8,2),农业未婚DM N(8,2),农业未婚NF N(8,2),农业未婚NM N(8,2),非农未婚DF N(8,2),非农未婚DM N(8,2),非农未婚NF N(8,2),非农未婚NM N(8,2),;				
	双独概率M N(8,4),双独概率F N(8,4),男单概率M N(8,4),男单概率F N(8,4),女单概率M N(8,4),女单概率F N(8,4))
	AFIEL(AR各类夫妇) 	&&16_74
	FOR ND=2010 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
CREA TABL 分婚配模式生育孩次数 FREE (年份 N(4),总HJB N(10),城HJB N(10),乡HJB N(10),总HJB1 N(10),城HJB1 N(10),乡HJB1 N(10),;
	总FNB1 N(10),总NYB1 N(10),总HJB2 N(10),城HJB2 N(10),乡HJB2 N(10),总FNB2  N(10),总NYB2  N(10),;	
	总HJ双独B  N(10),城HJ双独B  N(10),乡HJ双独B  N(10),总FN双独B  N(10),城FN双独B  N(10),乡FN双独B  N(10),总NY双独B  N(10),城NY双独B  N(10),乡NY双独B  N(10),;
	总HJ双独B1 N(10),城HJ双独B1 N(10),乡HJ双独B1 N(10),总FN双独B1 N(10),城FN双独B1 N(10),乡FN双独B1 N(10),总NY双独B1 N(10),城NY双独B1 N(10),乡NY双独B1 N(10),;
	总HJ双独B2 N(10),城HJ双独B2 N(10),乡HJ双独B2 N(10),总FN双独B2 N(10),城FN双独B2 N(10),乡FN双独B2 N(10),总NY双独B2 N(10),城NY双独B2 N(10),乡NY双独B2 N(10),;
	总HJ单独B  N(10),城HJ单独B  N(10),乡HJ单独B  N(10),总FN单独B  N(10),城FN单独B  N(10),乡FN单独B  N(10),总NY单独B  N(10),城NY单独B  N(10),乡NY单独B  N(10),;
	总HJ单独B1 N(10),城HJ单独B1 N(10),乡HJ单独B1 N(10),总FN单独B1 N(10),城FN单独B1 N(10),乡FN单独B1 N(10),总NY单独B1 N(10),城NY单独B1 N(10),乡NY单独B1 N(10),;
	总HJ单独B2 N(10),城HJ单独B2 N(10),乡HJ单独B2 N(10),总FN单独B2 N(10),城FN单独B2 N(10),乡FN单独B2 N(10),总NY单独B2 N(10),城NY单独B2 N(10),乡NY单独B2 N(10),;
	总HJ双非B  N(10),城HJ双非B  N(10),乡HJ双非B  N(10),总FN双非B  N(10),城FN双非B  N(10),乡FN双非B  N(10),总NY双非B  N(10),城NY双非B  N(10),乡NY双非B  N(10),;
	总HJ双非B1 N(10),城HJ双非B1 N(10),乡HJ双非B1 N(10),总FN双非B1 N(10),城FN双非B1 N(10),乡FN双非B1 N(10),总NY双非B1 N(10),城NY双非B1 N(10),乡NY双非B1 N(10),;
	总HJ双非B2 N(10),城HJ双非B2 N(10),乡HJ双非B2 N(10),总FN双非B2 N(10),城FN双非B2 N(10),乡FN双非B2 N(10),总NY双非B2 N(10),城NY双非B2 N(10),乡NY双非B2 N(10))
AFIELDS(AR生育孩次)
	FOR ND=2010 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
*****人口预测结果概要 基本表结构*****
CREA TABL  AAA概要 FREE (年份 N(4),地区 C(6),总人口 N(10,2),男 N(10,2),女 N(10,2),年均人口 N(10,2),出生 N(10,2),出生男 N(10,2),出生女 N(10,2),出生XBB N(10,2),;
	出生一孩 N(10,2),出生二孩 N(10,2),政策二孩 N(10,2),政策生育数 N(10,2),再生育数 N(10,2),超生数 N(10,2),堆积夫妇数 N(10,2),堆积生育 N(10,2),非堆生育 N(10,2),;
	出生率 N(8,2),死亡率 N(8,2),自增率 N(8,2),净迁入率 N(8,2),总增长率 N(8,2),可实现TFR N(8,3),政策生育率 N(8,3),实婚生育率 N(8,3),再生育率 N(8,3),婚N政策TFR N(8,3),;
	死亡人数 N(10,2),死亡男 N(10,2),死亡女 N(10,2),自增人口 N(10,2),预期寿命M  N(10,2),预期寿命F  N(10,2),;
	净迁入数 N(10,2),净迁男 N(10,2),净迁女 N(10,2),迁移修正数 N(10,2),新增人数 N(10,2),非农人口 N(10,2),农业人口 N(10,2),非农人口B N(10,2),城镇人口 N(10,2),农村人口 N(10,2),城镇人口比 N(10,2),;
	婴幼儿 N(10,2),学前 N(10,2),小学 N(10,2),初中 N(10,2),高中 N(10,2),大学 N(10,2),;
	国标劳前 N(10,2),国标劳龄 N(10,2),国标劳龄M N(10,2),国标劳龄F N(10,2),国标青劳 N(10,2),国标中劳 N(10,2),国标中劳M N(10,2),国标中劳F N(10,2),;
	国标劳后 N(10,2),国标劳后M N(10,2),国标劳后F N(10,2),公安劳龄 N(10,2),公安劳龄M N(10,2),公安劳龄F N(10,2),;
	国标少年 N(10,2),国标劳年 N(10,2),国标劳年M N(10,2),国标劳年F N(10,2),	国标老年 N(10,2),国标老年M N(10,2),国标老年F N(10,2),;
	高龄 N(10,2),高龄M N(10,2),高龄F N(10,2),长寿 N(10,2),长寿M N(10,2),长寿F N(10,2),中位 N(10,2),年龄差异度 N(10,2),;
	国标少年比 N(10,2),国标老年比 N(10,2),国标老少比 N(10,2),国标总负 N(6,2),国标负老 N(10,2),国标负少 N(10,2),国标劳年B N(10,2),国标青劳B N(10,2),国标高龄B N(10,2),国标长寿B N(10,2),;
	国际少年 N(10,2),国际劳年 N(10,2),国际青劳 N(10,2),国际老年 N(10,2),国际老年M N(10,2),国际老年F N(10,2),;
	国际少年比 N(10,2),国际老年比 N(10,2),国际老少比 N(10,2),国际总负 N(6,2),国际负老 N(10,2),国际负少 N(10,2),国际劳年B N(10,2),国际青劳B N(10,2),国际高龄B N(10,2),国际长寿B N(10,2),;
	育龄女15 N(10,2),婚育男15 N(10,2),育龄女20 N(10,2),婚育男20 N(10,2),婚育XBB15 N(10,4),婚育XBB20 N(10,4),;
	人口密度 N(10,2),人均水资源 N(10,2),人均耕地 N(10,2))
	FOR ND=2000 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
CREA TABL  AAA婚配概率 FREE (年份 N(4),地区 C(6),;
	X农双独GLM N(8,4),X农双独GLF N(8,4),X非双独GLM N(8,4),X非双独GLF N(8,4),X农男单GLM N(8,4),X农男单GLF N(8,4),X农女单GLM N(8,4),X农女单GLF N(8,4),;
	X非男单GLM N(8,4),X非男单GLF N(8,4),X非女单GLM N(8,4),X非女单GLF N(8,4),C农双独GLM N(8,4),C农双独GLF N(8,4),C非双独GLM N(8,4),C非双独GLF N(8,4),;
	C农男单GLM N(8,4),C农男单GLF N(8,4),C农女单GLM N(8,4),C农女单GLF N(8,4),C非男单GLM N(8,4),C非男单GLF N(8,4),C非女单GLM N(8,4),C非女单GLF N(8,4))
	FOR ND=2010 TO ND2
	   APPE BLANK
	   REPL 年份 WITH ND
	ENDFOR
*****分年龄预测基本表结构*****
CREA TABL AAA分年龄人口预测 (X N(4))	
	FOR ND0=2010 TO ND2
	    MND='M'+STR(ND0,4)
	    FND='F'+STR(ND0,4)
	    ALTE TABL AAA分年龄人口预测 ADD &MND N(10)
	    ALTE TABL AAA分年龄人口预测 ADD &FND N(10)
	ENDFOR
	FOR X0=0 TO 110
	   APPE BLANK
	   REPL X WITH X0
	ENDFOR
*****AAA分年龄丧子人口预测基本表结构*****
CREA TABL AAA分年龄丧子人口预测 (X N(4))	
	FOR ND0=2010 TO ND2
	    MND='M'+STR(ND0,4)
	    FND='F'+STR(ND0,4)
	    ALTE TABL AAA分年龄丧子人口预测 ADD &MND N(10,6)
	    ALTE TABL AAA分年龄丧子人口预测 ADD &FND N(10,6)
	ENDFOR
	FOR X0=0 TO 110
	   APPE BLANK
	   REPL X WITH X0
	ENDFOR
*****分年龄生育孩子数 基本表结构*****
CREA TABL AAA分年龄生育预测 FREE (X N(4))
	FOR ND0=2010 TO ND2
		BX='B'-STR(ND0,4)
		ALTE TABL AAA分年龄生育预测 ADD &BX N(10)
	ENDFOR
	FOR X1=15 TO 49
		APPE BLANK
		REPL X WITH X1
	ENDFOR
CREA TABL AAA分年龄存活子女 FREE (X N(4))
	FOR ND0=2010 TO ND2
		B1X='B1'-STR(ND0,4)
		B2X='B2'-STR(ND0,4)
		ALTE TABL AAA分年龄存活子女 ADD &B1X N(10)
		ALTE TABL AAA分年龄存活子女 ADD &B2X N(10)
	ENDFOR
	FOR X1=15 TO 110
		APPE BLANK
		REPLACE X WITH X1
	ENDFOR
*****分年龄生育率 基本表结构*****
CREA TABL AAA妇女分年龄生育率预测 FREE (X N(4))
	FOR ND0=2010 TO ND2
		FX='F'-STR(ND0,4)
		ALTE TABL AAA妇女分年龄生育率预测 ADD &FX N(8,4)
	ENDFOR
	FOR X1=15 TO 49
		APPE BLANK
		REPL X WITH X1
	ENDFOR
************************************
CREA TABL AAA迁移概率 free (X n(4),净迁GLFBM n(8,6),净迁GLFBF n(8,6),净迁概率M n(8,6),净迁概率F n(8,6))
FOR X0=0 TO 110
   APPE BLANK
   REPL X WITH X0
ENDFOR
USE 
*********************************************************
SELECT A
RELEASE AR地区
USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\分省代码	&&地区别（无“全国”）,三地带,六大区,重点区域1,重点区域2
COPY TO ARRA AR地区 FIEL 地区
COUN TO DQS
USE   
CREA TABL AAA全国迁移平衡表结构 (X N(4),迁出T N(8),迁出M N(8),迁出F N(8),迁入T N(8),迁入M N(8),迁入F N(8),迁移平衡T N(8),迁移平衡M N(8),迁移平衡F N(8),;
平衡迁出T N(8),平衡迁出M N(8),平衡迁出F N(8),平衡迁入T N(8),平衡迁入M N(8),平衡迁入F N(8))
FOR D1=1 TO DQS
	DQM=AR地区(D1)-'M'
	DQF=AR地区(D1)-'F'
	ALTER TABLE AAA全国迁移平衡表结构 ADD &DQM N(8)
	ALTER TABLE AAA全国迁移平衡表结构 ADD &DQF N(8)
ENDFOR
FOR X0=0 TO 110
   APPE BLANK
   REPL X WITH X0
ENDFOR
*********************************************************
CREA TABL AAA各类夫妇子女年龄分组 FREE (年份 N(4),妇子类别 C(10),合计 n(10),;
X0 n(10),X1_4 n(10),X5_9 n(10),X10_14 n(10),X15_19 n(10),X20_24 n(10),X25_29 n(10),X30_34 n(10),X35_39 n(10),X40_44 n(10),X45_49 n(10),;
X50_54 n(10),X55_59 n(10),X60_64 n(10),X65_69 n(10),X70_74 n(10),X75_79 n(10),X80_84 n(10),X85_89 n(10),X90_94 n(10),X95_99 n(10),X100 n(10))
*****分年龄堆积妇女基本表结构*****
CREA TABL AAA妇女分年龄堆积妇女预测 FREE (X N(4))
	FOR ND0=2008 TO ND2
		FX='F'-STR(ND0,4)
		ALTE TABL AAA妇女分年龄堆积妇女预测 ADD &FX N(8)
	ENDFOR
	FOR X1=15 TO 49
		APPE BLANK
		REPLACE X WITH X1
	ENDFOR
CLOS DATA
RETURN 

*************************************************
PROCEDURE 基础数据和基本参数
*****以地区和年份为关键字的文件*****
SELECT 301
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00各地区\概要\各地区城乡人口迁移估计96_10.DBF 
	COPY TO AAA各地区变动 FOR 年份>=2006
	USE AAA各地区变动 ALIAS 各地区变动
	INDEX ON 地区-STR(年份,4) TO IN301
SELECT 302
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00各地区\概要\各地区户数人口数性别比和户规模06_10.dbf ALIA 抽样性别
	INDEX ON 地区-STR(年份,4) TO IN302
SELECT 303
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00各地区\概要\各地区人口城乡构成05_10 ALIA 地区城乡构成
	COPY TO AAA地区城乡构成2009 FIELDS 地区,CZRKB FOR 年份=2009
	INDEX ON 地区-STR(年份,4) TO IN303
SELECT 304
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00各地区\概要\各地区人口四率96_10.DBF ALIA 地区人口四率
	INDEX ON 地区-STR(年份,4) TO IN304
SELECT 301
SET RELATION TO 地区-STR(年份,4) INTO 302,地区-STR(年份,4) INTO 303,地区-STR(年份,4) INTO 304
REPLACE 地区城乡构成.CSL WITH 地区人口四率.CSL, 地区城乡构成.SWL WITH 地区人口四率.SWL, 地区城乡构成.ZZL WITH 地区人口四率.ZZL FOR 地区城乡构成.CSL=0

*****以地区为关键字的文件*****
SELECT 199
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\分省净迁移率估计2008 ALIA 净迁率08 &&依据 H:\D\数据库（全国）\D人口\00各地区\迁移\各地区城乡人口迁移估计96_09.DBF 计算
	&&由USE H:\分省生育政策仿真1102\分省生育政策仿真基础数据01\迁移概率\分省净迁移率估计2008.dbf考贝 &&此处净迁率08，计算中没有使用。保留于此是为了保持ARDQ多种参数数组元素原有次序
		&&依据 H:\D\数据库（全国）\D人口\00各地区\迁移\各地区城乡人口迁移估计96_09.DBF 计算
	INDEX ON 省代码 TO IN199
SELECT 200
	USE AAA地区城乡构成2009 ALIA 城乡构成
	ALTER TABLE  AAA地区城乡构成2009 ADD 省代码 C(2)
	INDEX ON 地区 TO IN200
SELECT 201
	USE &死亡率修正系数 ALIAS 死亡修正系数
	SET RELATION TO 地区 INTO 200
	REPLACE 城乡构成.省代码 WITH 死亡修正系数.省代码 ALL
	INDEX ON 省代码 TO IN201
SELECT 200
	USE AAA地区城乡构成2009 ALIA 城乡构成
	INDEX ON 省代码 TO IN200
SELECT 202
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城乡迁移修正系数__基于留存率的分析 ALIAS 迁移修正系数
	INDEX ON 省代码 TO IN202
SELECT 203
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\普查数据\各地区50岁及以上妇女无活产子女比例 ALIAS 无活产比例
		&&由 H:\分省生育政策仿真1102\分省生育政策仿真基础数据01\生育模式\各地区50岁及以上妇女无活产子女比例.dbf  考贝，普查年份待查
	INDEX ON 省代码 TO IN203
SELECT 204
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\普查数据\各地区82总抚养比和普查出生性别比90_10.dbf ALIAS 性别比
		&&由 H:\D\数据库（全国）\d人口\00各地区\出生\各地区82总抚养比和普查出生性别比90_10.dbf 考贝
		&&后者由 H:\D\数据库（全国）\人口普查2010\人口普查原始数据\出生性别\L6-02各地区分性别、孩次的出生人口.dbf和H:\D\数据库（全国）\d人口\00各地区\出生\各地区代码和出生性别比90_05.dbf 合并
	INDEX ON 省代码 TO IN204
SELECT 205
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\普查数据\分地区平均初婚年龄2005.dbf	 ALIAS 初婚年龄	&&初婚年龄待更新
	&&由 H:\D\数据库（全国）\人口抽样调查2005\各地区\分地区平均初婚年龄2005.dbf考贝
	INDEX ON 省代码 TO IN205
SELECT 206
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\分省代码	 ALIAS 分省代码
	INDEX ON 省代码 TO IN206
SELECT 207
		USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\生育政策\各地区终身政策生育率.dbf ALIAS 终身政策生育率
	INDEX ON 省代码 TO IN207
SELECT 208
	*USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计1227\现实生育率与政策生育率比较06_09  ALIAS 生育率实现比
	*USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计\各地区现实生育率与政策生育率迭代06_10  ALIAS 生育率实现比
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\基于实现比的可能生育率预测优选模型  ALIAS 生育率实现比	&&20121021
	&&F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\00各地区\各地区现实生育率与政策生育率迭代06_10.DBF
	&&现政比06=现实生育率/当年本地政策生育率（当年本地政策偏离度）,现政比年增=(2009年现政比-2006年现政比)/3
	&&实国B06=现实生育率/当年国家政策生育率（当年国均水平偏离度）,实国B年增=(2009年实国B-2006年实国B)/3
	*CALCULATE MIN(现实MI) TO MITFR
	CALCULATE MIN(户籍06_10) TO MITFR	&&20121021
	LOCATE FOR 地区='全国'	&&.and.年份=2009
	*全国实现TFR0=现实09
	*CALCULATE MAX(现实06),MAX(现实07),MAX(现实08),MAX(现实09),MIN(现实06),MIN(现实07),MIN(现实08),MIN(现实09) TO MA06,MA07,MA08,MA09,MI06,MI07,MI08,MI09
	*MATFR=MAX(MA06,MA07,MA08,MA09)
	*MITFR=MIN(MI06,MI07,MI08,MI09)
	INDEX ON 省代码 TO IN208
*SELECT 209
*	USE &迁移率修正系数 ALIAS 迁移率修正B
*	INDEX ON 地区-STR(年份,4) TO IN300
SELECT 201
SET RELATION TO 省代码 INTO 199,省代码 INTO 200,省代码 INTO 202,省代码 INTO 203,省代码 INTO 204,省代码 INTO 205,省代码 INTO 206,省代码 INTO 207,省代码 INTO 208	&&,省代码 INTO 209
******地区别与出生性别比*****
*COPY TO ARRA ARDQ多种参数 FIEL 省代码,地区,性别比.出生XBB10,性别比.出生XBB00,生育率实现比.现政比10,生育率实现比.现政比年增,生育率实现比.现实10,;
	生育率实现比.政策10,城乡构成.CZRKB,净迁率08.迁移率,生育率实现比.现实mi,生育率实现比.现政比10,生育率实现比.现政比年增,分省代码.地带四分法 FOR 地区<>'全国'	&&.AND.年份=2008
COPY TO ARRA ARDQ多种参数 FIEL 省代码,地区,;
	性别比.出生XBB10,;
	性别比.出生XBB00,;
	生育率实现比.户籍实现比,;
	生育率实现比.常czb增量,;
	生育率实现比.户籍06_10,;
	生育率实现比.政策生育率,;
	城乡构成.CZRKB,;
	净迁率08.迁移率,;
	生育率实现比.户籍生育率,;
	生育率实现比.户实现比ZL,;
	生育率实现比.MI误差模型,;
	分省代码.地带四分法,;
	生育率实现比.修正差值,;
	生育率实现比.户修正差值	 FOR 地区<>'全国'	&&	&&20121021
COUN TO DQS	 FOR 地区<>'全国'	&&.AND.年份=2008
*****************************
SELECT 180
	*USE F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\00全国平衡城乡现行2010\&实现方式\00地区合计\合计\人口概要\全国人口概要_非农现行农业现行_中迁
	*USE F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\00全国平衡城乡现行2010\&实现方式\00地区合计\合计\人口概要\可能生育\分释模式\全国人口概要_现行现行_中迁
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城市化水平预测2010_2160.DBF
SELE 76
 	*USE	H:\D\数据库（全国）\全国普查01\夫妻年龄\全国按丈夫年龄分的妻子年龄分布模式（修匀）
 	USE	F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\全国普查01\夫妻年龄\全国按丈夫年龄分的妻子年龄分布模式（修匀）
 	COPY TO BBB对妻需求分布
 	USE  BBB对妻需求分布 ALIA 对妻需求
 	ALTE TABL BBB对妻需求分布 RENA 妻子年龄 TO X
 	ALTE TABL BBB对妻需求分布 ALTE X N(4)
 	ALTE TABL BBB对妻需求分布 ADD DD妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NMDF妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NFDM妻 N(8)
 	ALTE TABL BBB对妻需求分布 ADD NN妻 N(8)
 	COPY TO BBB对妻需求分布模式
    INDEX ON X TO IN76
SELE 77
 	USE BBB对妻需求分布模式 ALIA 需妻模式
    INDEX ON X TO IN77
SELE 78
	*USE H:\D\数据库（全国）\全国普查01\夫妻年龄\全国按妻子年龄分的丈夫年龄分布模式（修匀）.DBF
	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\全国普查01\夫妻年龄\全国按妻子年龄分的丈夫年龄分布模式（修匀）.DBF
 	COPY TO BBB对夫需求分布
 	USE  BBB对夫需求分布 ALIA 需夫模式
 	ALTE TABL BBB对夫需求分布 RENA 丈夫年龄 TO X
 	ALTE TABL BBB对夫需求分布 ALTE X N(4)
 	ALTE TABL BBB对夫需求分布 ADD DD夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NMDF夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NFDM夫 N(8)
 	ALTE TABL BBB对夫需求分布 ADD NN夫 N(8)
    INDEX ON X TO IN78
RETURN

********************************
PROCEDURE 全国预测数据表
	*****全国现状文件*****
	*全国PATH='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\00全国'
	全国PATH='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\00区域合计\10全国合计\'

	全国人口概要现状=全国PATH-'合计\人口概要'-'\全国人口概要'
	全国城镇人口概要现状  =全国PATH-'\城镇\合计\人口概要'-'\全国城镇人口概要'
	全国农村人口概要现状  =全国PATH-'\农村\合计\人口概要'-'\全国农村人口概要'
	全国非农人口概要现状  =全国PATH-'非农\人口概要'-'\全国非农人口概要'
	全国农业人口概要现状  =全国PATH-'农业\人口概要'-'\全国农业人口概要'

	全国分年龄人口现状=全国PATH-'合计\分龄人口\全国分年龄人口迭代'
	全国城镇分年龄人口现状=全国PATH-'城镇\合计\分龄人口\全国城镇分年龄人口迭代'
	全国农村分年龄人口现状=全国PATH-'农村\合计\分龄人口\全国农村分年龄人口迭代'
	全国非农人口现状=全国PATH-'非农\分龄人口\全国非农分年龄人口迭代'
	全国农业人口现状=全国PATH-'农业\分龄人口\全国农业分年龄人口迭代'
	
	全国城镇农业人口现状=全国PATH-'城镇\农业\分龄人口\全国城镇农业分年龄人口迭代'
	全国城镇非农人口现状=全国PATH-'城镇\非农\分龄人口\全国城镇非农分年龄人口迭代'
	全国农村农业人口现状=全国PATH-'农村\农业\分龄人口\全国农村农业分年龄人口迭代'
	全国农村非农人口现状=全国PATH-'农村\非农\分龄人口\全国农村非农分年龄人口迭代'

	全国农村农业子女婚配现状表=全国PATH-'农村\农业\分龄婚配\全国农村农业子女迭代'
	全国农村非农子女婚配现状表=全国PATH-'农村\非农\分龄婚配\全国农村非农子女迭代'
	全国城镇农业子女婚配现状表=全国PATH-'城镇\农业\分龄婚配\全国城镇农业子女迭代'
	全国城镇非农子女婚配现状表=全国PATH-'城镇\非农\分龄婚配\全国城镇非农子女迭代'
 	全国农村子女婚配现状表=全国PATH-'\农村\合计\分龄婚配\全国农村子女迭代'
 	全国城镇子女婚配现状表=全国PATH-'\城镇\合计\分龄婚配\全国城镇子女迭代'
 	全国农业子女婚配现状表=全国PATH-'\农业\分龄婚配\全国农业子女迭代' 
 	全国非农子女婚配现状表=全国PATH-'\非农\分龄婚配\全国非农子女迭代'
 	全国子女婚配现状表=全国PATH-'\合计\分龄婚配\全国子女'
	
	*****全国预测结果文件*****
	各地区生育率与迁移摘要='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00摘要\各地区生育率与迁移摘要'-'_'-FA2-'_'-JA2
	全国人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\人口概要\全国人口概要'-'_'-FA2-'_'-JA2
	全国城镇人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\人口概要\全国城镇人口概要'-'_'-FA2-'_'-JA2
	全国农村人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\人口概要\全国农村人口概要'-'_'-FA2-'_'-JA2
	全国非农人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\人口概要\全国非农人口概要'-'_'-FA2-'_'-JA2
	全国农业人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\人口概要\全国农业人口概要'-'_'-FA2-'_'-JA2
	
	全国非农分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄生育\全国非农分年龄生育预测'-'_'-FA2-'_'-JA2
	全国农业分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\分龄生育\全国农业分年龄生育预测'-'_'-FA2-'_'-JA2
	全国城镇分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄生育\全国城镇分年龄生育预测'-'_'-FA2-'_'-JA2
	全国农村分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄生育\全国农村分年龄生育预测'-'_'-FA2-'_'-JA2
	全国分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄生育\全国分年龄生育预测'-'_'-FA2-'_'-JA2
	全国分年龄政策生育='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄政策生育\全国分年龄政策生育'-'_'-FA2-'_'-JA2

	全国分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄人口\全国分年龄人口预测'-'_'-FA2-'_'-JA2

	全国城镇分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄人口\全国城镇分年龄人口预测'-'_'-FA2-'_'-JA2
	全国非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄人口\全国非农分年龄人口预测'-'_'-FA2-'_'-JA2
	全国农村分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄人口\全国农村分年龄人口预测'-'_'-FA2-'_'-JA2
	全国农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\分龄人口\全国农业分年龄人口预测'-'_'-FA2-'_'-JA2
	全国城镇农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\农业\分龄人口\全国城镇农业分年龄人口预测'-'_'-FA2-'_'-JA2
	全国城镇非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\非农\分龄人口\全国城镇非农分年龄人口预测'-'_'-FA2-'_'-JA2
	全国农村农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\农业\分龄人口\全国农村农业分年龄人口预测'-'_'-FA2-'_'-JA2
	全国农村非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\非农\分龄人口\全国农村非农分年龄人口预测'-'_'-FA2-'_'-JA2
	
	全国分年龄超生预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄超生\全国分年龄超生预测'-'_'-FA2-'_'-JA2
	全国非农分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄超生\全国非农分年龄超生预测'-'_'-FA2-'_'-JA2
	全国农业分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\分龄超生\全国农业分年龄超生预测'-'_'-FA2-'_'-JA2
	全国城镇分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄超生\全国城镇分年龄超生预测'-'_'-FA2-'_'-JA2
	全国农村分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄超生\全国农村分年龄超生预测'-'_'-FA2-'_'-JA2
	全国分年龄超生生育率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\超生生育\全国分年龄超生生育率预测'-'_'-FA2-'_'-JA2

	全国农村分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄死亡\全国农村分年龄死亡人口'-'_'-FA2-'_'-JA2
	全国城镇分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄死亡\全国城镇分年龄死亡人口'-'_'-FA2-'_'-JA2
	全国分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄死亡\全国分年龄死亡人口'-'_'-FA2-'_'-JA2

	
	全国城镇非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\非农\分龄特扶\全国城镇非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国城镇农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\农业\分龄特扶\全国城镇农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\非农\分龄特扶\全国农村非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\农业\分龄特扶\全国农村农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国城镇特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄特扶\全国城镇特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄特扶\全国农村特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄特扶\全国非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\分龄特扶\全国农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	全国特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄特扶\全国特扶父母分龄人数'-'_'-FA2-'_'-JA2


	全国城镇非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\非农\分龄父母\全国城镇非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国城镇农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\农业\分龄父母\全国城镇农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\非农\分龄父母\全国农村非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\农业\分龄父母\全国农村农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国城镇一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄父母\全国城镇一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国农村一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄父母\全国农村一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄父母\全国非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\\农业\分龄父母\全国农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	全国一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄父母\全国一孩父母分龄人数'-'_'-FA2-'_'-JA2


	全国城镇分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄迁移\\全国城镇分年龄迁移人口'-'_'-FA2-'_'-JA2
	全国农村分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄迁移\全国农村分年龄迁移人口'-'_'-FA2-'_'-JA2
	全国分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄迁移\全国分年龄迁移人口'-'_'-FA2-'_'-JA2


 	全国农村农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\农业\分龄婚配\\全国农村农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	全国农村非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\非农\分龄婚配\\全国农村非农子女婚配预测表'-'_'-FA2-'_'-JA2 
 	全国农村子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\合计\分龄婚配\\全国农村子女婚配预测表'-'_'-FA2-'_'-JA2 
 	 	
 	全国城镇农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\农业\分龄婚配\\全国城镇农业子女婚配预测表'-'_'-FA2-'_'-JA2 
 	全国城镇非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\非农\分龄婚配\\全国城镇非农子女婚配预测表'-'_'-FA2-'_'-JA2 
 	全国城镇子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\合计\分龄婚配\\全国城镇子女婚配预测表'-'_'-FA2-'_'-JA2 
 	 	
 	全国农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\分龄婚配\全国农业子女婚配预测表'-'_'-FA2-'_'-JA2  
 	全国非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\分龄婚配\全国非农子女婚配预测表'-'_'-FA2-'_'-JA2 
 	全国子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄婚配\全国子女婚配预测表'-'_'-FA2-'_'-JA2 

	全国农村农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\农业\夫妇子女\全国农村农业夫妇及子女'-'_'-FA2-'_'-JA2
	全国农村非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\非农\夫妇子女\全国农村非农夫妇及子女'-'_'-FA2-'_'-JA2

	全国城镇农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\农业\夫妇子女\全国城镇农业夫妇及子女'-'_'-FA2-'_'-JA2
	全国城镇非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\非农\夫妇子女\全国城镇非农夫妇及子女'-'_'-FA2-'_'-JA2

	全国农村合计夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\\合计\夫妇子女\全国农村夫妇及子女'-'_'-FA2-'_'-JA2
	全国城镇合计夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\\合计\夫妇子女\全国城镇夫妇及子女'-'_'-FA2-'_'-JA2

	全国农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农业\夫妇子女\全国农业夫妇及子女'-'_'-FA2-'_'-JA2
	全国非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\非农\夫妇子女\全国非农夫妇及子女'-'_'-FA2-'_'-JA2
	全国农村夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\农村\\合计\夫妇子女\全国农村夫妇及子女'-'_'-FA2-'_'-JA2
	全国城镇夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\城镇\\合计\夫妇子女\全国城镇夫妇及子女'-'_'-FA2-'_'-JA2
	全国夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\夫妇子女\全国夫妇及子女'-'_'-FA2-'_'-JA2

	全国政策生育率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\分龄政策生育\全国政策及政策生育率'-'_'-FA2-'_'-JA2
 	全国生育孩次数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\生育孩次\全国分婚配模式生育孩次数'-'_'-FA2-'_'-JA2
	全国婚配概率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\00地区合计\合计\婚配概率\全国分城乡农非的婚配概率'-'_'-FA2-'_'-JA2
	
	*****全国预测结果文件准备*****
	
	IF ND0=ND1
		SELECT 402
		USE 
		SELE A
		USE 地区生育率与迁移摘要
		COPY TO &各地区生育率与迁移摘要
		USE AAA概要
		COPY  TO &全国城镇人口概要
		COPY  TO &全国农村人口概要
		COPY  TO &全国非农人口概要
		COPY  TO &全国农业人口概要
		COPY  TO &全国人口概要
		*****************************
		USE AAA分年龄人口预测
		COPY TO &全国分年龄人口预测
		COPY TO &全国城镇分年龄人口预测
		COPY TO &全国农村分年龄人口预测
		COPY TO &全国农业人口预测  
		COPY TO &全国非农人口预测  
		COPY TO &全国城镇农业人口预测  
		COPY TO &全国城镇非农人口预测  
		COPY TO &全国农村农业人口预测  
		COPY TO &全国农村非农人口预测  
		COPY TO &全国城镇分年龄迁移人口
		COPY TO &全国城镇分年龄死亡人口
		COPY TO &全国农村分年龄迁移人口
		COPY TO &全国农村分年龄死亡人口
		COPY TO &全国分年龄迁移人口
		COPY TO &全国分年龄死亡人口
		COPY TO &全国城镇非农特扶父母分龄人数
		COPY TO &全国城镇农业特扶父母分龄人数
		COPY TO &全国农村非农特扶父母分龄人数
		COPY TO &全国农村农业特扶父母分龄人数
		COPY TO &全国城镇特扶父母分龄人数
		COPY TO &全国农村特扶父母分龄人数
		COPY TO &全国非农特扶父母分龄人数
		COPY TO &全国农业特扶父母分龄人数
		COPY TO &全国特扶父母分龄人数
		
		COPY TO &全国城镇非农一孩父母分龄人数
		COPY TO &全国城镇农业一孩父母分龄人数
		COPY TO &全国农村非农一孩父母分龄人数
		COPY TO &全国农村农业一孩父母分龄人数
		COPY TO &全国城镇一孩父母分龄人数
		COPY TO &全国农村一孩父母分龄人数
		COPY TO &全国非农一孩父母分龄人数
		COPY TO &全国农业一孩父母分龄人数
		COPY TO &全国一孩父母分龄人数
		*****************************
		USE AAA分年龄生育预测
		COPY TO &全国非农分年龄生育预测
		COPY TO &全国农业分年龄生育预测
		COPY TO &全国城镇分年龄生育预测
		COPY TO &全国农村分年龄生育预测
		COPY TO &全国分年龄生育预测
		COPY TO &全国分年龄超生预测
		COPY TO &全国非农分年龄超生
		COPY TO &全国农业分年龄超生
		COPY TO &全国城镇分年龄超生
		COPY TO &全国农村分年龄超生
		COPY TO &全国分年龄政策生育
		USE AAA妇女分年龄生育率预测
		COPY TO &全国分年龄超生生育率
		*****************************
	 	USE 夫妇及子女表结构
		COPY TO &全国农村农业夫妇及子女
		COPY TO &全国农村非农夫妇及子女
		COPY TO &全国城镇农业夫妇及子女
		COPY TO &全国城镇非农夫妇及子女
		COPY TO &全国农村合计夫妇及子女
		COPY TO &全国城镇合计夫妇及子女
		COPY TO &全国农业夫妇及子女
		COPY TO &全国非农夫妇及子女
		COPY TO &全国农村夫妇及子女	
		COPY TO &全国城镇夫妇及子女
		COPY TO &全国夫妇及子女
		*****************************
	 	USE 婚配预测表结构								 	 	
		COPY TO &全国农村农业子女婚配预测表
		COPY TO &全国农村非农子女婚配预测表
		COPY TO &全国城镇农业子女婚配预测表
		COPY TO &全国城镇非农子女婚配预测表
		COPY TO &全国农村子女婚配预测表
		COPY TO &全国城镇子女婚配预测表
		COPY TO &全国农业子女婚配预测表
		COPY TO &全国非农子女婚配预测表
		COPY TO &全国子女婚配预测表
		*****************************
		USE 政策及政策生育率
		COPY TO &全国政策生育率
		USE 分婚配模式生育孩次数
		COPY TO &全国生育孩次数
		USE AAA婚配概率
		COPY TO &全国婚配概率
		*********************全国******************************
	ENDIF
	SELECT 402
		USE &各地区生育率与迁移摘要
	SELE 101
		USE &全国分年龄人口预测 ALIAS 全国分龄
		IF ND0=ND1
		*	APPEND FROM &全国分年龄人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
		ENDIF 
		DO 中间年龄零人口处理
	   	INDEX ON X TO IN101
	SELE 102
		USE &全国城镇分年龄人口预测 ALIAS 全国城镇分龄
		IF ND0=ND1
		*	APPEND FROM &全国城镇分年龄人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
		ENDIF 
		DO 中间年龄零人口处理
	   	INDEX ON X TO IN102
	SELE 103
		USE &全国非农人口预测 ALIAS 全国非农分龄
		IF ND0=ND1
		*	APPEND FROM &全国非农人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
		ENDIF 
		DO 中间年龄零人口处理
	   	INDEX ON X TO IN103
	SELE 104
		USE &全国农村分年龄人口预测 ALIAS 全国农村分龄
		IF ND0=ND1
		*	APPEND FROM &全国农村分年龄人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
		ENDIF 
		DO 中间年龄零人口处理
	   	INDEX ON X TO IN104
	SELE 105
		USE &全国农业人口预测 ALIAS 全国农业分龄
		IF ND0=ND1
		*	APPEND FROM &全国农业人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
		ENDIF 
		DO 中间年龄零人口处理
	   	INDEX ON X TO IN105
	SELE 106
		USE &全国分年龄迁移人口 ALIAS 全国迁移
	   	INDEX ON X TO IN106
	SELE 107
		USE &全国分年龄死亡人口 ALIAS 全国死亡
	   	INDEX ON X TO IN107
	SELE 108
		USE &全国分年龄生育预测 ALIAS 全国生育
	   	INDEX ON X TO IN108
	SELE 109
		USE &全国非农分年龄生育预测 ALIAS 全国非农生育
	   	INDEX ON X TO IN109
	SELE 110
		USE &全国农业分年龄生育预测 ALIAS 全国农业生育
	   	INDEX ON X TO IN110
	SELE 111
		USE &全国分年龄超生预测 ALIAS 全国分龄超生
	   	INDEX ON X TO IN111

	SELE 132
		USE &全国非农分年龄超生 ALIAS 全国非农超生
	   	INDEX ON X TO IN132
	SELE 133
		USE &全国农业分年龄超生 ALIAS 全国农业超生
	   	INDEX ON X TO IN133
	SELE 134
		USE &全国城镇分年龄超生 ALIAS 全国城镇超生
	   	INDEX ON X TO IN134
	SELE 135
		USE &全国农村分年龄超生 ALIAS 全国农村超生
	   	INDEX ON X TO IN135
	SELE 112
		USE &全国分年龄超生生育率 ALIAS 全国超生生育率
	   	INDEX ON X TO IN112
	SELE 113
		USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表
	   	INDEX ON X TO IN113
	SELE 114
		USE &全国分年龄政策生育 ALIAS 全国分龄政策生育
	   	INDEX ON X TO IN114
	*******************************城镇******************************************
	SELE 116
		USE &全国城镇分年龄迁移人口 ALIAS 全国城镇迁移
		INDEX ON X TO IN116
	SELE 117
		USE &全国城镇分年龄生育预测 ALIAS 全国城镇生育
	   	INDEX ON X TO IN117
	SELE 118
		USE &全国城镇分年龄死亡人口 ALIAS 全国城镇死亡
		INDEX ON X TO IN118
	SELE 126
		USE &全国农村分年龄迁移人口 ALIAS 全国农村迁移
		INDEX ON X TO IN126
	SELE 127
		USE &全国农村分年龄生育预测 ALIAS 全国农村生育
	   	INDEX ON X TO IN127
	SELE 128
		USE &全国农村分年龄死亡人口 ALIAS 全国农村死亡
		INDEX ON X TO IN128
	***********************************************
	SELE 130
		USE  &全国城镇农业人口预测 ALIAS 全国城镇农业分龄
		*IF ND0=ND1
		*	APPEND FROM &全国城镇农业人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
	   	*ENDIF
		*DO 中间年龄零人口处理
		INDEX ON X TO IN130
	SELE 131
		USE  &全国城镇非农人口预测 ALIAS 全国城镇非农分龄
		*IF ND0=ND1
		*	APPEND FROM &全国城镇非农人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
	   	*ENDIF
		*DO 中间年龄零人口处理
		INDEX ON X TO IN131
	SELE 140
		USE  &全国农村农业人口预测 ALIAS 全国农村农业分龄
		*IF ND0=ND1
		*	APPEND FROM &全国农村农业人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
	   	*ENDIF
		*DO 中间年龄零人口处理
		INDEX ON X TO IN140
	SELE 141
		USE  &全国农村非农人口预测 ALIAS 全国农村非农分龄
		*IF ND0=ND1
		*	APPEND FROM &全国农村非农人口现状 && FIELDS X,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009
	   	*ENDIF
		*DO 中间年龄零人口处理
		INDEX ON X TO IN141
	******************记录各婚配模式夫妇人数,及生育的孩子数预测结果******************************
 	SELE 150
 	 	USE &全国政策生育率   ALIA 全国政策生育率 &&记录各类婚配夫妇的政策内容及政策生育率
 	 	INDEX ON 年份 TO IN150
 	SELE 151	
		USE &全国农村农业夫妇及子女 ALIA 全国农村农业夫妇  &&记录各婚配模式夫妇人数,及生育的孩子数
 	 	INDEX ON 年份 TO IN151
 	SELE 152	
		USE &全国农村非农夫妇及子女 ALIA 全国农村非农夫妇
 	 	INDEX ON 年份 TO IN152
 	SELE 153	
		USE &全国城镇农业夫妇及子女 ALIA 全国城镇农业夫妇
 	 	INDEX ON 年份 TO IN153
 	SELE 154	
		USE &全国城镇非农夫妇及子女 ALIA 全国城镇非农夫妇	
 	 	INDEX ON 年份 TO IN154
 	SELE 155	
		USE &全国农业夫妇及子女 ALIA 全国农业夫妇
 	 	INDEX ON 年份 TO IN155
 	SELE 156	
		USE &全国非农夫妇及子女 ALIA 全国非农夫妇
 	 	INDEX ON 年份 TO IN156
 	SELE 157
		USE &全国农村夫妇及子女 ALIA 全国农村夫妇	
 	 	INDEX ON 年份 TO IN157
 	SELE 158	
		USE &全国城镇夫妇及子女 ALIA 全国城镇夫妇
 	 	INDEX ON 年份 TO IN158
  	SELE 159	
		USE &全国夫妇及子女 ALIA 全国夫妇
 	 	INDEX ON 年份 TO IN159
  	SELE 160
		USE &全国生育孩次数 ALIA 全国生育孩次
 	 	INDEX ON 年份 TO IN160
	***************************年份数据******************************************
	SELECT 180
		USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城市化水平预测2010_2160.DBF
		IF QY>1
			CSHYC=DQB-'YC'
			LOCATE FOR 年份=ND0
			DQCSHYC=&CSHYC
		ENDIF 
	   	INDEX ON 年份 TO IN180
	SELE 181
		USE &全国人口概要  ALIAS 全国概要
		IF ND0=ND1
		*	APPEND FROM &全国人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
		*LOCATE FOR 年份=2008
		*DQCSHYC=城镇人口比
	   	INDEX ON 年份 TO IN181
	SELE 182
		USE &全国城镇人口概要 ALIAS 全国城镇概要		
		IF ND0=ND1
		*	APPEND FROM &全国城镇人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN182
	SELE 183
		USE &全国非农人口概要 ALIAS 全国非农概要		
		IF ND0=ND1
		*	APPEND FROM &全国非农人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN183
	SELE 184
		USE &全国农村人口概要 ALIAS 全国农村概要		
		IF ND0=ND1
		*	APPEND FROM &全国农村人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN184
	SELE 185
		USE &全国农业人口概要 ALIAS 全国农业概要		
		IF ND0=ND1
		*	APPEND FROM &全国农业人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN185
	SELE 186
		USE &全国婚配概率 ALIAS 全国婚配概率	
	   	INDEX ON 年份 TO IN186
	SELE 190
	  	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00全国\概要\全国人口城乡构成78_10.dbf ALIAS 全国城乡构成
	  	INDEX ON 年份 TO IN190
	SELE 191
	  	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00全国\年龄\全国人口年龄结构和抚养比.dbf ALIAS 全国年龄结构
	   	INDEX ON 年份 TO IN191
	SELE 192
	  	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00全国\概要\全国人口出生率死亡率和自然增长率78_10 ALIAS 全国自然变动
	   	INDEX ON 年份 TO IN192
	*************基年年龄分组和年龄结构****************
	*FOR CX=1 TO 3
	*	SELE IIF(CX=1,1,IIF(CX=2,11,21))	&&SELE 1:全国分龄;SELE 11  城镇分龄预测况;SELE 21:地区农村分龄
	*		DO 基年年龄分组和年龄结构计算
	*	SELE IIF(CX=1,181,IIF(CX=2,182,183))	&&SELE 181：全国概要;SELE 182：全国城镇概要;SELE 183：全国农村概要
	*		DO 基年年龄分组和年龄结构保存
	*ENDFOR
	*************************************************
 	SELE 151
	SET RELA TO 年份 INTO 150,年份 INTO 152,年份 INTO 153,年份 INTO 154,年份 INTO 155,年份 INTO 156,年份 INTO 157,年份 INTO 158,年份 INTO 159,年份 INTO 160,;
	年份 INTO 180,年份 INTO 181,年份 INTO 182,年份 INTO 183,年份 INTO 184,年份 INTO 185,年份 INTO 190,年份 INTO 191,年份 INTO 192
	IF ND0=ND1
		REPL 全国概要.总人口  WITH 全国城乡构成.ZRK, 全国概要.男 WITH 全国城乡构成.ZRKM,全国概要.女 WITH 全国城乡构成.ZRKF,;
			 全国概要.出生率  WITH 全国自然变动.出生率,全国概要.死亡率  WITH 全国自然变动.死亡率,全国概要.自增率  WITH 全国自然变动.自然增长率,;
			 全国概要.城镇人口比 WITH 全国城乡构成.CZRKB,全国概要.城镇人口  WITH 全国城乡构成.CZRK,全国概要.农村人口  WITH 全国城乡构成.XCRK,;
			 全国概要.国际少年   WITH 全国年龄结构.国际少年,全国概要.国际劳年 WITH 全国年龄结构.国际劳年,全国概要.国际老年 WITH  全国年龄结构.国际老年,;
			 全国概要.国际少年比 WITH 全国年龄结构.国际少年比,全国概要.国际老年比 WITH  全国年龄结构.国际老年比,;
			 全国概要.国际总负   WITH 全国年龄结构.国际总抚比,全国概要.国际负老 WITH  全国年龄结构.国际老抚比,全国概要.国际负少 WITH  全国年龄结构.国际少抚比,全国概要.国际劳年B WITH 全国年龄结构.国际劳年比  ALL
		REPL 全国概要.国际老少比 WITH  全国年龄结构.国际老年/全国年龄结构.国际少年 FOR 全国年龄结构.国际少年<>0
		REPL 全国城镇概要.总人口 WITH 全国城乡构成.CZRK,全国农村概要.总人口 WITH 全国城乡构成.XCRK  ALL
	ENDIF 
 	SELE 161 
 	 	USE &全国农村农业子女婚配预测表 ALIAS 全国农村农业子女		&&农村婚配
	    INDEX ON X TO IN161
 	SELE 162 
 	 	USE &全国农村非农子女婚配预测表 ALIAS 全国农村非农子女	&&农村婚配
	    INDEX ON X TO IN162
 	SELE 163 
 	 	USE &全国城镇农业子女婚配预测表 ALIAS 全国城镇农业子女		&&城镇婚配
	    INDEX ON X TO IN163
 	SELE 164 
 	 	USE &全国城镇非农子女婚配预测表 ALIAS 全国城镇非农子女	&&城镇非农子女
	    INDEX ON X TO IN164
 	SELE 165
		USE &全国农村子女婚配预测表 ALIAS 全国农村子女
	    INDEX ON X TO IN165
 	SELE 166
		USE &全国城镇子女婚配预测表 ALIAS 全国城镇子女
	    INDEX ON X TO IN166
 	SELE 167
 	 	USE &全国农业子女婚配预测表 ALIAS 全国农业子女		
	    INDEX ON X TO IN167
 	SELE 168
 	 	USE &全国非农子女婚配预测表 ALIAS 全国非农子女
	    INDEX ON X TO IN168
 	SELE 169
 	 	USE &全国子女婚配预测表 ALIAS 全国子女
	    INDEX ON X TO IN169
	SELE 801
		USE &全国城镇非农特扶父母分龄人数 ALIAS 全国城镇非农特扶
	    INDEX ON X TO IN801
	SELE 802
		USE &全国城镇农业特扶父母分龄人数 ALIAS 全国城镇农业特扶
	    INDEX ON X TO IN802
	SELE 803
		USE &全国农村非农特扶父母分龄人数 ALIAS 全国农村非农特扶
	    INDEX ON X TO IN803
	SELE 804
		USE &全国农村农业特扶父母分龄人数 ALIAS 全国农村农业特扶
	    INDEX ON X TO IN804
	SELE 805
		USE &全国城镇特扶父母分龄人数 ALIAS 全国城镇特扶
	    INDEX ON X TO IN805
	SELE 806
		USE &全国农村特扶父母分龄人数 ALIAS 全国农村特扶
	    INDEX ON X TO IN806
	SELE 807
		USE &全国非农特扶父母分龄人数 ALIAS 全国非农特扶
	    INDEX ON X TO IN807
	SELE 808
		USE &全国农业特扶父母分龄人数 ALIAS 全国农业特扶
	    INDEX ON X TO IN808
	SELE 809
		USE &全国特扶父母分龄人数 ALIAS 全国特扶
	    INDEX ON X TO IN809

	SELE 901
		USE &全国城镇非农一孩父母分龄人数 ALIAS 全国城镇非农父母
	    INDEX ON X TO IN901
	SELE 902
		USE &全国城镇农业一孩父母分龄人数 ALIAS 全国城镇农业父母
	    INDEX ON X TO IN902
	SELE 903
		USE &全国农村非农一孩父母分龄人数 ALIAS 全国农村非农父母
	    INDEX ON X TO IN903
	SELE 904
		USE &全国农村农业一孩父母分龄人数 ALIAS 全国农村农业父母
	    INDEX ON X TO IN904
	SELE 905
		USE &全国城镇一孩父母分龄人数 ALIAS 全国城镇父母
	    INDEX ON X TO IN905
	SELE 906
		USE &全国农村一孩父母分龄人数 ALIAS 全国农村父母
	    INDEX ON X TO IN906
	SELE 907
		USE &全国非农一孩父母分龄人数 ALIAS 全国非农父母
	    INDEX ON X TO IN907
	SELE 908
		USE &全国农业一孩父母分龄人数 ALIAS 全国农业父母
	    INDEX ON X TO IN908
	SELE 909
		USE &全国一孩父母分龄人数 ALIAS 全国父母
	    INDEX ON X TO IN909
IF ND0=ND1
	?'地区',',','年份',',','政策方案',',','可实现TFR',',','政策生育率',',','超生生育率',',','超生孩子数',',','非农女单终身TFR',',',;
	'非农男单终身TFR',',','非农双非终身TFR',',','农业女单终身TFR',',','农业男单终身TFR',',','农业双非终身TFR',',','地区时期TFR',',',;
	'城镇时期TFR',',','非农时期TFR',',','农村时期TFR',',','农业时期TFR',',','迁移合计',',','城镇迁移',',','非农迁移',',','农村迁移',',','农业迁移',',','本地迁移',',','各地区累计迁移'
ENDIF
RETURN 
******************************
PROCEDURE 中间年龄零人口处理
		COPY TO ARRAY ARNL10 FIELDS M2010,F2010
		REPLACE M2010 WITH (ARNL10(RECNO(),1)+ARNL10(RECNO()-1,1)+ARNL10(RECNO()+1,1))/3 FOR RECNO()>1.AND.RECN()<RECCOUNT().AND.ARNL10(RECNO(),1)=0
		REPLACE F2010 WITH (ARNL10(RECNO(),2)+ARNL10(RECNO()-1,2)+ARNL10(RECNO()+1,2))/3 FOR RECNO()>1.AND.RECN()<RECCOUNT().AND.ARNL10(RECNO(),2)=0
RETURN 

*******************************************
PROCEDURE 分省迁移初算
SELE 113
USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表
FOR F1=2 TO FCOUNT()
	FIE1=FIELD(F1)
	REPLACE &FIE1 WITH 0 ALL
ENDFOR
地区历年迁移估计='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\年鉴数据\各地区城乡人口迁移估计96_10.DBF '
FOR DQX=DQ1 TO DQ2	&&DQS
    DQB=ALLT(ARDQ多种参数(DQX,2))	&&省代码,DQ,出生XBB05,出生XBB00
    DQ夹=ALLT(ARDQ多种参数(DQX,1))-DQB
	地带=ARDQ多种参数(DQX,14)

	*城乡总和迁移M='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-DQB-'总和迁移人次预测M'	&&-IIF(xb=1,'T',IIF(xb=2,'M','F'))-'.dbf'
	*城乡总和迁移F='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-DQB-'总和迁移人次预测F'	&&-IIF(xb=1,'T',IIF(xb=2,'M','F'))-'.dbf'
	*总和迁移分布M='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-DQB-'省际省内城乡总和迁移人次年龄分布模式M'
	*总和迁移分布F='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-DQB-'省际省内城乡总和迁移人次年龄分布模式F'
	*迁移留存率='h:\D\数据库（全国）\人口普查2010\人口普查特汇数据\分地区\&DQ夹\QY34'-DQB-'按城乡户口性质性别年龄分的迁移留存率.DBF'

	总和迁移分布='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\QY45'-DQB-'按城乡户口性质性别分的净迁移概率年龄性别分布模式_基于留存率的分析'
	总和迁移预测='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-'QY44'-DQB-'分城乡农非性别的总和迁移人次预测_基于留存率的分析.dbf'
	
	地区人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\人口概要\'-DQB-'人口概要'-'_'-FA2-'_'-JA2
	地区农村农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\农业\分龄婚配\\\'-DQB-'农村农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农村非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\非农\分龄婚配\\'-DQB-'农村非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\农业\分龄婚配\\\'-DQB-'城镇农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\非农\分龄婚配\\'-DQB-'城镇非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄婚配\'-DQB-'农业子女婚配预测表' 
 	地区非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄婚配\'-DQB-'非农子女婚配预测表'
	
	*地区农村农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计1227\&DQ夹\农村\\分龄婚配\农业\\'-DQB-'农村农业子女婚配预测表_非农现行农业现行_中迁2008'
	*地区农村非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计1227\&DQ夹\农村\合计\分龄婚配\非农\'-DQB-'农村非农子女婚配预测表_非农现行农业现行_中迁2008'
	*地区城镇农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计1227\&DQ夹\城镇\\分龄婚配\农业\\'-DQB-'城镇农业子女婚配预测表_非农现行农业现行_中迁2008'
	*地区城镇非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\现状估计1227\&DQ夹\城镇\合计\分龄婚配\非农\'-DQB-'城镇非农子女婚配预测表_非农现行农业现行_中迁2008'								
 
 	地区农村农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'农村农业子女'
	地区农村非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'农村非农子女'
	地区城镇农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'城镇农业子女'
	地区城镇非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'城镇非农子女'
	基年子女='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'

 	IF ND0=ND1
 	 	SELECT A
 	 	USE 婚配预测表结构
		COPY STRUCTURE TO &地区农村农业子女婚配预测表 
		COPY STRUCTURE TO &地区农村非农子女婚配预测表
		COPY STRUCTURE TO &地区城镇农业子女婚配预测表
		COPY STRUCTURE TO &地区城镇非农子女婚配预测表
	ENDIF

	*****分年龄性别的总人口、独生子女和非独生子女*****
	SELE 113
		USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表
	   	INDEX ON X TO IN113
 	SELE 61
 		USE &基年子女	&&='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'
 		COPY TO AAA农村农业 DELIMITED FIELDS X,农农HJ,农农HJM,农农HJF,农农D,农农DM,农农DF,农农N,农农NM,农农NF
 		COPY TO AAA农村非农 DELIMITED FIELDS X,农非HJ,农非HJM,农非HJF,农非D,农非DM,农非DF,农非N,农非NM,农非NF 
 		COPY TO AAA城镇农业 DELIMITED FIELDS X,城农HJ,城农HJM,城农HJF,城农D,城农DM,城农DF,城农N,城农NM,城农NF 
 		COPY TO AAA城镇非农 DELIMITED FIELDS X,城非HJ,城非HJM,城非HJF,城非D,城非DM,城非DF,城非N,城非NM,城非NF
 	 	USE &地区农村农业子女婚配预测表 ALIAS  地区农村农业子女		&&农村婚配
		IF ND0=ND1
			APPE FROM AAA农村农业 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
   		INDEX ON X TO IN61
 	SELE 62 
 	 	USE &地区农村非农子女婚配预测表 ALIAS  地区农村非农子女	&&农村婚配
		IF ND0=ND1
			APPE FROM  AAA农村非农 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
    	INDEX ON X TO IN62
 	SELE 63 
 	 	USE &地区城镇农业子女婚配预测表 ALIAS  地区城镇农业子女		&&城镇婚配
		IF ND0=ND1
			APPE FROM  AAA城镇农业 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
    	INDEX ON X TO IN63
 	SELE 64 
 	 	USE &地区城镇非农子女婚配预测表 ALIAS  地区城镇非农子女	&&城镇非农子女
		IF ND0=ND1
			APPE FROM  AAA城镇非农 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
   		INDEX ON X TO IN64
	*****迁移概率参数*****
	SELE 84
		USE &总和迁移分布
		COPY TO AAA总和迁移分布与概率
		USE  AAA总和迁移分布与概率 ALIAS 城乡迁移概率
	   	INDEX ON X TO IN84
	SELECT 61
	SET RELA TO X INTO 62,X INTO 63,X INTO 64,X INTO 84,X INTO 113	&&,X INTO 87,X INTO 88,X INTO 86,X INTO 85
	*REPLACE 城镇迁移概率.净迁GLFBM WITH 总和迁移分布M.城总迁GL,城镇迁移概率.净迁GLFBF WITH 总和迁移分布F.城总迁GL ALL 
	*REPLACE 农村迁移概率.净迁GLFBM WITH 总和迁移分布M.乡总迁GL,农村迁移概率.净迁GLFBF WITH 总和迁移分布F.乡总迁GL ALL 
	*SUM 城镇迁移概率.净迁GLFBM,城镇迁移概率.净迁GLFBF,农村迁移概率.净迁GLFBM,农村迁移概率.净迁GLFBF TO SCM,SCF,SXM,SXF
	*REPLACE 城镇迁移概率.净迁GLFBM WITH 城镇迁移概率.净迁GLFBM/SCM,城镇迁移概率.净迁GLFBF WITH 城镇迁移概率.净迁GLFBF/SCF,;
			农村迁移概率.净迁GLFBM WITH 农村迁移概率.净迁GLFBM/SXM,农村迁移概率.净迁GLFBF WITH 农村迁移概率.净迁GLFBF/SXF ALL
	*************************************************************
	*	迁移变动及其对城乡分年龄性别独生子女\非独生子女数的影响	*
 	*************************************************************
	SELECT 59
		USE &总和迁移预测	&&='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-'QY44'-DQB-'分城乡农非性别的总和迁移人次预测_基于留存率的分析.dbf'
		LOCATE FOR 年份=ND0
		城农总迁T=城农T
		城非总迁T=城非T
		农农总迁T=农农T
		农非总迁T=农非T

		城农总迁M=城农M
		城非总迁M=城非M
		农农总迁M=农农M
		农非总迁M=农非M
		
		城农总迁F=城农F
		城非总迁F=城非F
		农农总迁F=农农F
		农非总迁F=农非F
	SELECT 61
	REPLACE 城乡迁移概率.城农T WITH 城农总迁T*城乡迁移概率.城农T分布,城乡迁移概率.城非T WITH 城非总迁T*城乡迁移概率.城非T分布 ALL
	REPLACE 城乡迁移概率.农农T WITH 农农总迁T*城乡迁移概率.农农T分布,城乡迁移概率.农非T WITH 农非总迁T*城乡迁移概率.农非T分布 ALL
	REPLACE 城乡迁移概率.城农M WITH 城农总迁M*城乡迁移概率.城农M分布,城乡迁移概率.城非M WITH 城非总迁M*城乡迁移概率.城非M分布 ALL
	REPLACE 城乡迁移概率.农农M WITH 农农总迁M*城乡迁移概率.农农M分布,城乡迁移概率.农非M WITH 农非总迁M*城乡迁移概率.农非M分布 ALL
	REPLACE 城乡迁移概率.城农F WITH 城农总迁F*城乡迁移概率.城农F分布,城乡迁移概率.城非F WITH 城非总迁F*城乡迁移概率.城非F分布 ALL
	REPLACE 城乡迁移概率.农农F WITH 农农总迁F*城乡迁移概率.农农F分布,城乡迁移概率.农非F WITH 农非总迁F*城乡迁移概率.农非F分布 ALL
	*****************
	*	迁移参数	*
	*****************
	DO CASE
	CASE DQB<>'西藏'.AND. QY>1
		SELECT 180
		CSHYC=DQB-'YC'
		LOCATE FOR 年份=ND0
		DQCSHYC=&CSHYC
		SELECT 202
		USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城乡迁移修正系数__基于留存率的分析 ALIAS 迁移修正系数	&&
		LOCATE FOR 地区=dqb
		qy修正b=IIF(JA='低迁','qy修正bd',IIF(JA='中迁','qy修正bz',IIF(JA='高迁','qy修正bg',0)))
		QYB=&qy修正b
		*CQYB=迁移修正系数.C迁10B	&&*IIF(QY=1,0.8,IIF(QY=2,1,1.2))/平衡系数
		*XQYB=迁移修正系数.X迁10B	&&*IIF(QY=1,0.8,IIF(QY=2,1,1.2))/平衡系数
		*****城镇农业\非农业迁移人口的分年龄性别独生子女\非独生子女数,按地区农业\非农业分年龄性别独生子女\非独生子女比例推算*******
		DQBM=DQB-'M'
		DQBF=DQB-'F'
		IF nd0=nd1
			SELECT 58
			USE &地区历年迁移估计
			CALCULATE MAX(迁入),Min(迁入),AVG(迁入) TO ma,mi,av FOR 年份>2005.AND.年份<2010.AND.年份<>2005.AND.地区=DQB
			IF av>0
				QY05_10=IIF(JA='低迁',MI,IIF(JA='中迁',(AV+MI)/2,IIF(JA='高迁',AV,0)))*10000
			ELSE 
				QY05_10=IIF(JA='低迁',MI/3.5,IIF(JA='中迁',(AV/2+MI/2)/2,IIF(JA='高迁',AV,0)))*10000	&&MI/4：19.63
			ENDIF 
			QYRK=0
			*DQBM=DQB-'M'
			*DQBF=DQB-'F'
			DO WHILE ABS(qyrk-QY05_10)>100
				SELECT 61
				REPLACE 迁移平衡表.&DQBM WITH 0,迁移平衡表.&DQBF WITH 0  ALL 
				FOR CX=1 TO 2	&&城乡
					城乡=IIF(CX=1,'城镇','农村')
					*城乡迁移概率=IIF(CX=1,'城镇','农村')-'迁移概率'
					QYGL=IIF(CX=1,'CQYGL','XQYGL')
					FOR NFX=1 TO 2 &&农非
						农非=IIF(NFX=1,'农业','非农')
						ALIA1='地区'-城乡-农非-'子女'
						FOR ZN=1 TO 3 &&子女
							ZNB=IIF(ZN=1,'D',IIF(ZN=2,'DDB','N'))
							*FOR XBX=1 TO 2	&&此处的另一算法是：先计算不分性别的迁移，然后按按迁移人口分年龄的性别结构，将迁移人口分成性别
							*	XB=IIF(XBX=1,'M','F')
							*	迁移概率=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-XB
							*	DQMF=DQB-XB
							*	子女=ZNB-XB
							*	子女QY='QY'-ZNB-XB
							*	迁出='迁出'-XB
							*	迁入='迁入'-XB
							*	*SUM &城乡迁移概率..&性别迁移概率 TO SQY
							*	*REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*IIF((&城乡迁移概率..&性别迁移概率*IIF(SQY<0.AND.QYB<0,ABS(QYB),QYB))>1,1,&城乡迁移概率..&性别迁移概率*IIF(SQY<0.AND.QYB<0,ABS(QYB),QYB)) ALL
							*	REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*城乡迁移概率.&迁移概率*QYB ALL
							*	REPLACE 迁移平衡表.&DQMF WITH 迁移平衡表.&DQMF+&ALIA1..&子女QY ALL
							*ENDFOR &&XBX
							******另一算法：先计算不分性别的迁移，然后按迁移人口分年龄的性别结构分配迁移男女
							迁移概率=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'T'
							迁移MB=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'MB'
							迁移FB=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'FB'
							DQM=DQB-'M'
							DQF=DQB-'F'
							子女=ZNB
							子女QY=ZNB-'QY'
							子女QYM=ZNB-'M'-'QY'
							子女QYF=ZNB-'F'-'QY'
							迁出M='迁出'-'M'
							迁出F='迁出'-'F'
							迁入M='迁入'-'M'
							迁入F='迁入'-'F'
							REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*城乡迁移概率.&迁移概率*QYB,&ALIA1..&子女QYM WITH &ALIA1..&子女QY*城乡迁移概率.&迁移MB,&ALIA1..&子女QYF WITH &ALIA1..&子女QY*城乡迁移概率.&迁移FB ALL
							REPLACE 迁移平衡表.&DQM WITH 迁移平衡表.&DQM+&ALIA1..&子女QYM,迁移平衡表.&DQF WITH 迁移平衡表.&DQF+&ALIA1..&子女QYF ALL
							*REPLACE 迁移平衡表.&迁入M WITH 迁移平衡表.&迁入M+&ALIA1..&子女QYM FOR &ALIA1..&子女QYM>0
							*REPLACE 迁移平衡表.&迁入F WITH 迁移平衡表.&迁入F+&ALIA1..&子女QYF FOR &ALIA1..&子女QYF>0
							*REPLACE 迁移平衡表.&迁出M WITH 迁移平衡表.&迁出M+&ALIA1..&子女QYM FOR &ALIA1..&子女QYM<0
							*REPLACE 迁移平衡表.&迁出F WITH 迁移平衡表.&迁出F+&ALIA1..&子女QYF FOR &ALIA1..&子女QYF<0
						ENDFOR &&子女
					ENDFOR &&农非
				ENDFOR &&城乡
				SUM 迁移平衡表.&DQBM+迁移平衡表.&DQBF TO QYRK
				DO CASE
				CASE QYRK>0.AND.QY05_10-QYRK>100
					QYB=QYB*1.1
				CASE QYRK>0.AND.QY05_10>0.AND.QY05_10-QYRK<-100
					QYB=QYB*0.9
				CASE QYRK>0.AND.QY05_10<0.AND.QY05_10-QYRK<-100
					QYB=-QYB*1.1
				CASE QYRK<0.AND.QY05_10>0.and.abs(QY05_10)-ABS(QYRK)>100
					QYB=-QYB*1.1
				CASE QYRK<0.AND.QY05_10>0.AND.QY05_10-QYRK>100	&&吉林
					QYB=-QYB*1.1
				CASE QYRK<0.AND.QY05_10<0.and.QY05_10-QYRK>100
					QYB=QYB*0.9
				CASE QYRK<0.AND.QY05_10-QYRK<-100
					QYB=QYB*1.1
				ENDCASE
			ENDDO
			*DQBM=DQB-'M'
			*DQBF=DQB-'F'
			REPLACE 迁移平衡表.迁入M WITH 迁移平衡表.迁入M+迁移平衡表.&DQBM FOR 迁移平衡表.&DQBM>0
			REPLACE 迁移平衡表.迁出M WITH 迁移平衡表.迁出M+迁移平衡表.&DQBM FOR 迁移平衡表.&DQBM<0
			REPLACE 迁移平衡表.迁入F WITH 迁移平衡表.迁入F+迁移平衡表.&DQBF FOR 迁移平衡表.&DQBF>0
			REPLACE 迁移平衡表.迁出F WITH 迁移平衡表.迁出F+迁移平衡表.&DQBF FOR 迁移平衡表.&DQBF<0

			SELECT 202
			LOCATE FOR  地区=DQB
			REPLACE &QY修正B WITH QYB
		ELSE
			SELECT 61
			FOR CX=1 TO 2	&&城乡
				城乡=IIF(CX=1,'城镇','农村')
				*城乡迁移概率=IIF(CX=1,'城镇','农村')-'迁移概率'
				QYGL=IIF(CX=1,'CQYGL','XQYGL')
				FOR NFX=1 TO 2 &&农非
					农非=IIF(NFX=1,'农业','非农')
					ALIA1='地区'-城乡-农非-'子女'
					FOR ZN=1 TO 3 &&子女
						ZNB=IIF(ZN=1,'D',IIF(ZN=2,'DDB','N'))
						*FOR XBX=1 TO 2
						*	XB=IIF(XBX=1,'M','F')
						*	*性别迁移概率='净迁概率'-XB
						*	迁移概率=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-XB
						*	DQMF=DQB-XB
						*	子女=ZNB-XB
						*	子女QY=ZNB-XB-'QY'
						*	迁出='迁出'-XB
						*	迁入='迁入'-XB
						*	*SUM &城乡迁移概率..&性别迁移概率 TO SQY
						*	*REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*IIF((&城乡迁移概率..&性别迁移概率*IIF(SQY<0.AND.QYB<0,ABS(QYB),QYB))>1,1,&城乡迁移概率..&性别迁移概率*IIF(SQY<0.AND.QYB<0,ABS(QYB),QYB)) ALL
						*	REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*城乡迁移概率.&迁移概率*QYB ALL
						*	REPLACE 迁移平衡表.&DQMF WITH IIF(x>90,0,迁移平衡表.&DQMF+&ALIA1..&子女QY) ALL
						*	REPLACE 迁移平衡表.&迁入 WITH 迁移平衡表.&迁入+&ALIA1..&子女QY FOR &ALIA1..&子女QY>0
						*	REPLACE 迁移平衡表.&迁出 WITH 迁移平衡表.&迁出+&ALIA1..&子女QY FOR &ALIA1..&子女QY<0
						*ENDFOR &&XBX
						迁移概率=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'T'
						迁移MB=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'MB'
						迁移FB=SUBSTR(城乡,1,2)-SUBSTR(农非,1,2)-'FB'
						DQM=DQB-'M'
						DQF=DQB-'F'
						子女=ZNB
						子女QY=ZNB-'QY'
						子女QYM=ZNB-'M'-'QY'
						子女QYF=ZNB-'F'-'QY'
						迁出M='迁出'-'M'
						迁出F='迁出'-'F'
						迁入M='迁入'-'M'
						迁入F='迁入'-'F'
						REPLACE &ALIA1..&子女QY WITH &ALIA1..&子女*城乡迁移概率.&迁移概率*QYB,&ALIA1..&子女QYM WITH &ALIA1..&子女QY*城乡迁移概率.&迁移MB,&ALIA1..&子女QYF WITH &ALIA1..&子女QY*城乡迁移概率.&迁移FB ALL
						REPLACE 迁移平衡表.&DQM WITH 迁移平衡表.&DQM+&ALIA1..&子女QYM,迁移平衡表.&DQF WITH 迁移平衡表.&DQF+&ALIA1..&子女QYF ALL
						
						REPLACE 迁移平衡表.迁入M WITH 迁移平衡表.迁入M+迁移平衡表.&DQBM FOR 迁移平衡表.&DQBM>0
						REPLACE 迁移平衡表.迁出M WITH 迁移平衡表.迁出M+迁移平衡表.&DQBM FOR 迁移平衡表.&DQBM<0
						REPLACE 迁移平衡表.迁入F WITH 迁移平衡表.迁入F+迁移平衡表.&DQBF FOR 迁移平衡表.&DQBF>0
						REPLACE 迁移平衡表.迁出F WITH 迁移平衡表.迁出F+迁移平衡表.&DQBF FOR 迁移平衡表.&DQBF<0
						
					ENDFOR &&子女
				ENDFOR &&农非
			ENDFOR &&城乡
		ENDIF 
	CASE QY=1.or.DQB='西藏'
		STORE 0 TO QYB
	ENDCASE
ENDFOR &&地区
SELECT 61
REPLACE 迁移平衡表.迁移平衡M WITH (迁移平衡表.迁入M+ABS(迁移平衡表.迁出M))/2,迁移平衡表.迁移平衡F WITH (迁移平衡表.迁入F+ABS(迁移平衡表.迁出F))/2  ALL
SUM 迁移平衡表.迁入M,迁移平衡表.迁出M,迁移平衡表.迁入F,迁移平衡表.迁出F TO 迁入和M0,迁出和M0,迁入和F0,迁出和F0
SUM 迁移平衡表.迁移平衡M,迁移平衡表.迁移平衡F TO 迁移平衡和M,迁移平衡和F
*?(迁入和M0+ABS(迁出和M0))/2,迁移平衡和M,(迁入和F0+ABS(迁出和F0))/2,迁移平衡和F
RETURN 

*******************************************
PROCEDURE 分省无迁婚配预测表	&&用于无迁移
SELE 113
USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表
FOR F1=2 TO FCOUNT()
	FIE1=FIELD(F1)
	REPLACE &FIE1 WITH 0 ALL
ENDFOR

 	地区农村农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\农业\分龄婚配\\\'-DQB-'农村农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农村非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\非农\分龄婚配\\'-DQB-'农村非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\农业\分龄婚配\\\'-DQB-'城镇农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\非农\分龄婚配\\'-DQB-'城镇非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄婚配\\\'-DQB-'农业子女婚配预测表' 
 	地区非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄婚配\\'-DQB-'非农子女婚配预测表'
	地区农村农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'农村农业子女'
	地区农村非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'农村非农子女'
	地区城镇农业子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'城镇农业子女'
	地区城镇非农子女婚配现状表='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\'-DQB-'城镇非农子女'
	基年子女='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'

 	IF ND0=ND1
 	 	SELECT A
 	 	USE 婚配预测表结构
		COPY STRUCTURE TO &地区农村农业子女婚配预测表 
		COPY STRUCTURE TO &地区农村非农子女婚配预测表
		COPY STRUCTURE TO &地区城镇农业子女婚配预测表
		COPY STRUCTURE TO &地区城镇非农子女婚配预测表
	ENDIF
	SELECT 180
	*USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\各地区城市化水平预测2010_2160.DBF
		CSHYC=DQB-'YC'
		LOCATE FOR 年份=ND0
		DQCSHYC=&CSHYC
 	SELE 61
 		USE &基年子女	&&='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'
 		COPY TO AAA农村农业 DELIMITED FIELDS X,农农HJ,农农HJM,农农HJF,农农D,农农DM,农农DF,农农N,农农NM,农农NF
 		COPY TO AAA农村非农 DELIMITED FIELDS X,农非HJ,农非HJM,农非HJF,农非D,农非DM,农非DF,农非N,农非NM,农非NF 
 		COPY TO AAA城镇农业 DELIMITED FIELDS X,城农HJ,城农HJM,城农HJF,城农D,城农DM,城农DF,城农N,城农NM,城农NF 
 		COPY TO AAA城镇非农 DELIMITED FIELDS X,城非HJ,城非HJM,城非HJF,城非D,城非DM,城非DF,城非N,城非NM,城非NF
 	 	USE &地区农村农业子女婚配预测表 ALIAS  地区农村农业子女		&&农村婚配
		IF ND0=ND1
			APPE FROM AAA农村农业 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
   		INDEX ON X TO IN61
 	SELE 62 
 	 	USE &地区农村非农子女婚配预测表 ALIAS  地区农村非农子女	&&农村婚配
		IF ND0=ND1
			APPE FROM  AAA农村非农 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
    	INDEX ON X TO IN62
 	SELE 63 
 	 	USE &地区城镇农业子女婚配预测表 ALIAS  地区城镇农业子女		&&城镇婚配
		IF ND0=ND1
			APPE FROM  AAA城镇农业 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
    	INDEX ON X TO IN63
 	SELE 64 
 	 	USE &地区城镇非农子女婚配预测表 ALIAS  地区城镇非农子女	&&城镇非农子女
		IF ND0=ND1
			APPE FROM  AAA城镇非农 DELIMITED  FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF
		ENDIF
   		INDEX ON X TO IN64
RETURN 


*************************************
PROCEDURE 地区基础数据和预测文件
	*****地区基本参数文件*****
	城镇死亡概率M='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\死亡概率\'-DQB-'城镇死亡概率预测M'
	城镇死亡概率F='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\死亡概率\'-DQB-'城镇死亡概率预测F'
	乡村死亡概率M='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\死亡概率\'-DQB-'农村死亡概率预测M'
	乡村死亡概率F='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\死亡概率\'-DQB-'农村死亡概率预测F'
	城乡预期寿命='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\预期寿命\'-DQB-'城乡别预期寿命插值与预测1981_2160'

	城镇生育模式与释放模式='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\生育模式\'-DQB-'生育及释放模式_存活一孩法（城镇）'
	乡村生育模式与释放模式='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\生育模式\'-DQB-'生育及释放模式_存活一孩法（乡村）'
	
	想生二胎比='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\五省调查\汇总结果\五省比较\B本人想生T'
	突释二胎比='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\五省调查\汇总结果\五省比较\B二年内生T'
	缓释二胎比='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\五省调查\汇总结果\五省比较\B三五年生T'
	晚释二胎比='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\五省调查\汇总结果\五省比较\B五年后生T'
	
	*总和迁移分布='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\QY45'-DQB-'按城乡户口性质性别分的净迁移概率年龄性别分布模式_基于留存率的分析'
	*总和迁移预测='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-'QY44'-DQB-'分城乡农非性别的总和迁移人次预测_基于留存率的分析.dbf'

	*****基础数据文件*****
	*地区乡村分龄05='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\分龄人口\'-DQB-'分年龄性别人口'-'（乡村）'
	*地区城镇分龄05='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\分龄人口\'-DQB-'分年龄性别人口'-'（城镇）'
	*地区分龄05='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\分龄人口\'-DQB-'分年龄性别人口'
	
	*****2010年地区现状******
	基年子女='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'
	地区城镇人口概要现状  ='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\城镇\合计\人口概要\'-DQB-'城镇人口概要'
	地区农村人口概要现状  ='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\农村\合计\人口概要\'-DQB-'农村人口概要'
	地区非农人口概要现状  ='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\非农\人口概要\'-DQB-'非农人口概要'
	地区农业人口概要现状  ='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\农业\人口概要\'-DQB-'农业人口概要'
	地区人口概要现状 	  ='F:\分省生育政策仿真_基于普查\分省现状迭代\抽样出生率迭代法\&DQ夹\合计\人口概要\'-DQB-'人口概要'
	基年城乡存活孩子数='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\生育模式\SY42'-DQB-'常住人口分城乡农非年龄存活孩子数的育龄妇女人数'

	PATH1='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\独生子女父母回推\&DQ夹\'
    城镇非农独子父母现状=PATH1-DQB-'城镇非农独生子女父母回推.dbf'
    城镇农业独子父母现状=PATH1-DQB-'城镇农业独生子女父母回推.dbf'
    乡村非农独子父母现状=PATH1-DQB-'乡村非农独生子女父母回推.dbf'
    乡村农业独子父母现状=PATH1-DQB-'乡村农业独生子女父母回推.dbf'

    城镇非农特扶现状=PATH1-DQB-'城镇非农独生子女特扶父母'
    城镇农业特扶现状=PATH1-DQB-'城镇农业独生子女特扶父母'
    乡村非农特扶现状=PATH1-DQB-'乡村非农独生子女特扶父母'
    乡村农业特扶现状=PATH1-DQB-'乡村农业独生子女特扶父母'
	*****地区预测结果文件*****
	地区城镇人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\人口概要\'-DQB-'城镇人口概要'-'_'-FA2-'_'-JA2
	地区农村人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\人口概要\'-DQB-'农村人口概要'-'_'-FA2-'_'-JA2
	地区非农人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\人口概要\'-DQB-'非农人口概要'-'_'-FA2-'_'-JA2
	地区农业人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\人口概要\'-DQB-'农业人口概要'-'_'-FA2-'_'-JA2
	地区人口概要  ='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\人口概要\'-DQB-'人口概要'-'_'-FA2-'_'-JA2

	地区非农分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄生育\'-DQB-'非农分年龄生育预测'-'_'-FA2-'_'-JA2
	地区农业分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄生育\'-DQB-'农业分年龄生育预测'-'_'-FA2-'_'-JA2
	地区城镇分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄生育\'-DQB-'城镇分年龄生育预测'-'_'-FA2-'_'-JA2
	地区农村分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄生育\'-DQB-'农村分年龄生育预测'-'_'-FA2-'_'-JA2
	
	地区分年龄生育预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄生育\'-DQB-'分年龄生育预测'-'_'-FA2-'_'-JA2
	
	地区分年龄超生预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄超生\'-DQB-'分年龄超生预测'-'_'-FA2-'_'-JA2
	地区城镇分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄超生\'-DQB-'城镇分年龄超生预测'-'_'-FA2-'_'-JA2
	地区农村分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄超生\'-DQB-'农村分年龄超生预测'-'_'-FA2-'_'-JA2
	地区非农分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄超生\'-DQB-'非农分年龄超生预测'-'_'-FA2-'_'-JA2
	地区农业分年龄超生='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄超生\'-DQB-'农业分年龄超生预测'-'_'-FA2-'_'-JA2
	
	地区分龄政策生育='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄政策生育\'-DQB-'分年龄政策生育预测'-'_'-FA2-'_'-JA2
	地区分年龄超生生育率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\超生生育\'-DQB-'分年龄超生生育率'-'_'-FA2-'_'-JA2

	地区城镇分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄人口\'-DQB-'城镇分年龄人口预测'-'_'-FA2-'_'-JA2
	地区城镇分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄迁移\\'-DQB-'城镇分年龄迁移人口'-'_'-FA2-'_'-JA2
	地区城镇分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄死亡\'-DQB-'城镇分年龄死亡人口'-'_'-FA2-'_'-JA2

	地区农村分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄人口\'-DQB-'农村分年龄人口预测'-'_'-FA2-'_'-JA2
	地区农村分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄迁移\'-DQB-'农村分年龄迁移人口'-'_'-FA2-'_'-JA2
	地区农村分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄死亡\'-DQB-'农村分年龄死亡人口'-'_'-FA2-'_'-JA2

	地区分年龄人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄人口\'-DQB-'分年龄人口预测'-'_'-FA2-'_'-JA2
	地区分年龄迁移人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄迁移\'-DQB-'分年龄迁移人口'-'_'-FA2-'_'-JA2
	地区分年龄死亡人口='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄死亡\'-DQB-'分年龄死亡人口'-'_'-FA2-'_'-JA2

	地区城镇非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分龄特扶\'-DQB-'城镇非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区城镇农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分龄特扶\'-DQB-'城镇农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分龄特扶\'-DQB-'农村非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分龄特扶\'-DQB-'农村农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区城镇特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄特扶\'-DQB-'城镇特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄特扶\'-DQB-'农村特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区非农特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄特扶\'-DQB-'非农特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区农业特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄特扶\'-DQB-'农业特扶父母分龄人数'-'_'-FA2-'_'-JA2
	地区特扶父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄特扶\'-DQB-'特扶父母分龄人数'-'_'-FA2-'_'-JA2

	地区城镇非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分龄父母\'-DQB-'城镇非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区城镇农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分龄父母\'-DQB-'城镇农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分龄父母\'-DQB-'农村非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分龄父母\'-DQB-'农村农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区城镇一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄父母\'-DQB-'城镇一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区农村一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄父母\'-DQB-'农村一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区非农一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄父母\'-DQB-'非农一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区农业一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄父母\'-DQB-'农业一孩父母分龄人数'-'_'-FA2-'_'-JA2
	地区一孩父母分龄人数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄父母\'-DQB-'一孩父母分龄人数'-'_'-FA2-'_'-JA2

	地区城镇农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分龄人口\'-DQB-'城镇农业分年龄人口预测'-'_'-FA2-'_'-JA2
	地区城镇非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分龄人口\'-DQB-'城镇非农分年龄人口预测'-'_'-FA2-'_'-JA2
	地区农村农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分龄人口\'-DQB-'农村农业分年龄人口预测'-'_'-FA2-'_'-JA2
	地区农村非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分龄人口\'-DQB-'农村非农分年龄人口预测'-'_'-FA2-'_'-JA2
	地区农业人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄人口\'-DQB-'农业分年龄人口预测'-'_'-FA2-'_'-JA2
	地区非农人口预测='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄人口\'-DQB-'非农分年龄人口预测'-'_'-FA2-'_'-JA2

 	地区农村农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分龄婚配\\\'-DQB-'农村农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农村非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分龄婚配\\'-DQB-'农村非农子女婚配预测表'-'_'-FA2-'_'-JA2 
 	地区城镇农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分龄婚配\\\'-DQB-'城镇农业子女婚配预测表'-'_'-FA2-'_'-JA2 
 	地区城镇非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分龄婚配\\'-DQB-'城镇非农子女婚配预测表'-'_'-FA2-'_'-JA2 
 	 	
 	地区农村子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄婚配\\'-DQB-'农村子女婚配预测表' -'_'-FA2-'_'-JA2
 	*地区城镇子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄婚配\\'-DQB-'非农子女婚配预测表' -'_'-FA2-'_'-JA2
 	地区城镇子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄婚配\\'-DQB-'城镇子女婚配预测表' -'_'-FA2-'_'-JA2
 	地区农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄婚配\'-DQB-'农业子女婚配预测表' -'_'-FA2-'_'-JA2
 	地区非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄婚配\'-DQB-'非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄婚配\'-DQB-'子女婚配预测表'-'_'-FA2-'_'-JA2

	地区城镇农业分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分龄存活子女\'-DQB-'城镇农业分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区城镇非农分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分龄存活子女\'-DQB-'城镇非农分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区农村农业分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分龄存活子女\'-DQB-'农村农业分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区农村非农分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分龄存活子女\'-DQB-'农村非农分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区城镇分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\分龄存活子女\'-DQB-'城镇分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区农村分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\分龄存活子女\'-DQB-'农村分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区农业分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\分龄存活子女\'-DQB-'农业分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区非农分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\分龄存活子女\'-DQB-'非农分年龄存活子女数'-'_'-FA2-'_'-JA2
	地区分年龄存活子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄存活子女\'-DQB-'分年龄存活子女数'-'_'-FA2-'_'-JA2


	地区城镇农业分年龄组夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\分组夫妇\'-DQB-'城镇农业分年龄组夫妇及子女数'-'_'-FA2-'_'-JA2
	地区城镇非农分年龄组夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\分组夫妇\'-DQB-'城镇非农分年龄组夫妇及子女数'-'_'-FA2-'_'-JA2
	地区农村农业分年龄组夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\分组夫妇\'-DQB-'农村农业分年龄组夫妇及子女数'-'_'-FA2-'_'-JA2
	地区农村非农分年龄组夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\分组夫妇\'-DQB-'农村非农分年龄组夫妇及子女数'-'_'-FA2-'_'-JA2


	地区农村农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\农业\夫妇子女\'-DQB-'农村农业夫妇及子女'-'_'-FA2-'_'-JA2
	地区农村非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\非农\夫妇子女\'-DQB-'农村非农夫妇及子女'-'_'-FA2-'_'-JA2

	地区城镇农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\农业\夫妇子女\'-DQB-'城镇农业夫妇及子女'-'_'-FA2-'_'-JA2
	地区城镇非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\非农\夫妇子女\'-DQB-'城镇非农夫妇及子女'-'_'-FA2-'_'-JA2

	地区农村合计夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\夫妇子女\'-DQB-'农村合计夫妇及子女'-'_'-FA2-'_'-JA2
	地区城镇合计夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\夫妇子女\'-DQB-'城镇合计夫妇及子女'-'_'-FA2-'_'-JA2

	地区农业夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农业\夫妇子女\'-DQB-'农业夫妇及子女'-'_'-FA2-'_'-JA2
	地区非农夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\非农\夫妇子女\'-DQB-'非农夫妇及子女'-'_'-FA2-'_'-JA2

	地区农村夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\合计\夫妇子女\'-DQB-'农村夫妇及子女'-'_'-FA2-'_'-JA2
	地区城镇夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\合计\夫妇子女\'-DQB-'城镇夫妇及子女'-'_'-FA2-'_'-JA2
	地区夫妇及子女='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\夫妇子女\'-DQB-'夫妇及子女'-'_'-FA2-'_'-JA2

	地区政策生育率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\分龄政策生育\'-DQB-'政策及政策生育率'-'_'-FA2-'_'-JA2
 	地区生育孩次数='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\生育孩次\'-DQB-'分婚配模式生育孩次数'-'_'-FA2-'_'-JA2
	地区婚配概率='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\合计\婚配概率\'-DQB-'分城乡农非的婚配概率'-'_'-FA2-'_'-JA2

	*****地区预测结果文件准备*****
	IF ND0=ND1
		SELE A
		USE AAA概要
		COPY STRUCTURE TO &地区城镇人口概要
		COPY STRUCTURE TO &地区农村人口概要
		COPY STRUCTURE TO &地区非农人口概要
		COPY STRUCTURE TO &地区农业人口概要
		COPY STRUCTURE TO &地区人口概要
		*****************************
		USE AAA分年龄人口预测
		COPY STRUCTURE TO &地区分年龄人口预测
		COPY STRUCTURE TO &地区城镇分年龄人口预测
		COPY STRUCTURE TO &地区农村分年龄人口预测
		COPY STRUCTURE TO &地区农业人口预测  
		COPY STRUCTURE TO &地区非农人口预测  
		COPY STRUCTURE TO &地区城镇农业人口预测  
		COPY STRUCTURE TO &地区城镇非农人口预测  
		COPY STRUCTURE TO &地区农村农业人口预测  
		COPY STRUCTURE TO &地区农村非农人口预测  
		COPY TO &地区城镇分年龄迁移人口
		COPY TO &地区城镇分年龄死亡人口
		COPY TO &地区农村分年龄迁移人口
		COPY TO &地区农村分年龄死亡人口
		COPY TO &地区分年龄迁移人口
		COPY TO &地区分年龄死亡人口
		
		USE AAA分年龄丧子人口预测
		COPY TO &地区城镇非农特扶父母分龄人数
		COPY TO &地区城镇农业特扶父母分龄人数
		COPY TO &地区农村非农特扶父母分龄人数
		COPY TO &地区农村农业特扶父母分龄人数
		COPY TO &地区城镇特扶父母分龄人数
		COPY TO &地区农村特扶父母分龄人数
		COPY TO &地区非农特扶父母分龄人数
		COPY TO &地区农业特扶父母分龄人数
		COPY TO &地区特扶父母分龄人数
		
		COPY TO &地区城镇非农一孩父母分龄人数
		COPY TO &地区城镇农业一孩父母分龄人数
		COPY TO &地区农村非农一孩父母分龄人数
		COPY TO &地区农村农业一孩父母分龄人数
		COPY TO &地区城镇一孩父母分龄人数
		COPY TO &地区农村一孩父母分龄人数
		COPY TO &地区非农一孩父母分龄人数
		COPY TO &地区农业一孩父母分龄人数
		COPY TO &地区一孩父母分龄人数
		*****************************
		USE AAA分年龄生育预测
		COPY TO &地区非农分年龄生育预测
		COPY TO &地区农业分年龄生育预测
		COPY TO &地区城镇分年龄生育预测
		COPY TO &地区农村分年龄生育预测
		COPY TO &地区分年龄生育预测
		COPY TO &地区分年龄超生预测
		COPY TO &地区城镇分年龄超生
		COPY TO &地区农村分年龄超生
		COPY TO &地区非农分年龄超生
		COPY TO &地区农业分年龄超生
		COPY TO &地区分龄政策生育
		*****************************
		USE AAA妇女分年龄生育率预测
		COPY TO &地区分年龄超生生育率
		*****************************
 	 	USE 夫妇及子女表结构
		COPY TO &地区农村农业夫妇及子女
		COPY TO &地区农村非农夫妇及子女
		COPY TO &地区城镇农业夫妇及子女
		COPY TO &地区城镇非农夫妇及子女
		COPY TO &地区农村合计夫妇及子女
		COPY TO &地区城镇合计夫妇及子女
		COPY TO &地区农业夫妇及子女
		COPY TO &地区非农夫妇及子女
		COPY TO &地区农村夫妇及子女
		COPY TO &地区城镇夫妇及子女
		COPY TO &地区夫妇及子女
		*****************************
 	 	USE AAA分年龄存活子女
		COPY TO &地区城镇农业分年龄存活子女
		COPY TO &地区城镇非农分年龄存活子女
		COPY TO &地区农村农业分年龄存活子女
		COPY TO &地区农村非农分年龄存活子女
		COPY TO &地区城镇分年龄存活子女
		COPY TO &地区农村分年龄存活子女
		COPY TO &地区农业分年龄存活子女
		COPY TO &地区非农分年龄存活子女
		COPY TO &地区分年龄存活子女
		*****************************
 	 	USE 婚配预测表结构								 	 	
		COPY STRUCTURE TO &地区农村子女婚配预测表
		COPY STRUCTURE TO &地区城镇子女婚配预测表
		COPY STRUCTURE TO &地区农业子女婚配预测表
		COPY STRUCTURE TO &地区非农子女婚配预测表
		COPY STRUCTURE TO &地区子女婚配预测表
		*****************************
		USE AAA各类夫妇子女年龄分组
		COPY TO &地区城镇农业分年龄组夫妇及子女
		COPY TO &地区城镇非农分年龄组夫妇及子女
		COPY TO &地区农村农业分年龄组夫妇及子女
		COPY TO &地区农村非农分年龄组夫妇及子女
		*****************************
		USE 政策及政策生育率
		COPY TO &地区政策生育率
		USE 分婚配模式生育孩次数
		COPY TO &地区生育孩次数
		USE AAA婚配概率
		COPY TO &地区婚配概率
	ENDIF
	*****年度结果数据*****
	*****各婚配模式夫妇人数及生育的孩子数预测结果*****
 	 	SELE 40
 	 		USE &地区政策生育率 ALIA 地区政策生育率 		&&记录各类夫妇政策内容及政策生育率
 	 		INDEX ON 年份 TO IN40
 	 	SELE 41	
		USE &地区农村农业夫妇及子女 ALIAS  地区农村农业夫妇  &&记录各婚配模式夫妇人数,及生育的孩子数
 	 		INDEX ON 年份 TO IN41
 	 	SELE 42	
		USE &地区农村非农夫妇及子女 ALIAS  地区农村非农夫妇
 	 		INDEX ON 年份 TO IN42
 	 	SELE 43	
		USE &地区城镇农业夫妇及子女 ALIAS  地区城镇农业夫妇
 	 		INDEX ON 年份 TO IN43
 	 	SELE 44	
		USE &地区城镇非农夫妇及子女 ALIAS  地区城镇非农夫妇	
 	 		INDEX ON 年份 TO IN44
 	 	SELE 45	
		USE &地区农业夫妇及子女 ALIAS  地区农业夫妇
 	 		INDEX ON 年份 TO IN45
 	 	SELE 46	
		USE &地区非农夫妇及子女 ALIAS  地区非农夫妇
 	 		INDEX ON 年份 TO IN46
 	 	SELE 47
		USE &地区农村夫妇及子女 ALIAS  地区农村夫妇	
 	 		INDEX ON 年份 TO IN47
 	 	SELE 48	
		USE &地区城镇夫妇及子女 ALIAS  地区城镇夫妇
 	 		INDEX ON 年份 TO IN48
 	 	SELE 49	
		USE &地区夫妇及子女 ALIA 地区夫妇
 	 		INDEX ON 年份 TO IN49
 	 	SELE 50
		USE &地区生育孩次数 ALIAS  地区生育孩次
 	 		INDEX ON 年份 TO IN50
	SELE 51
		USE &地区人口概要  ALIAS 地区概要		
	   	IF ND0=ND1
	   	*	APPEND FROM &地区人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	LOCATE FOR 年份=ND0-1
		Csh水平1=IIF(ND0=ND1,ARDQ多种参数(DQX,9),城镇人口比)
		地区政策TFR1=ARDQ多种参数(DQX,8)	&&政策生育率
		现政比1=IIF(ND0=ND1,ARDQ多种参数(DQX,5),可实现TFR/政策生育率)	&&20121021
	   	DQQYL=净迁入率
	   	DQQYND0=净迁入数
	   	*?DQB,年份,净迁入数,城镇人口比
	   	INDEX ON 年份 TO IN51
	SELE 52
		USE &地区城镇人口概要 ALIAS 地区城镇概要		
	   	IF ND0=ND1
	   	*	APPEND FROM &地区城镇人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN52
	SELE 53
		USE &地区非农人口概要 ALIAS 地区非农概要		
	   	IF ND0=ND1
	   	*	APPEND FROM &地区非农人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN53
	SELE 54
		USE &地区农村人口概要 ALIAS 地区农村概要		
	   	IF ND0=ND1
	   	*	APPEND FROM &地区农村人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN54
	SELE 55
		USE &地区农业人口概要 ALIAS 地区农业概要		
	   	IF ND0=ND1
	   	*	APPEND FROM &地区农业人口概要现状 &&  FOR 年份<=2009
	   	ENDIF
	   	INDEX ON 年份 TO IN55
	SELE 56
		USE &地区婚配概率 ALIAS 地区婚配概率
	   	INDEX ON 年份 TO IN56
	SELE 57
	  	USE F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\数据库（全国）\D人口\00各地区\年龄\各地区人口年龄构成和抚养比03_10
	  	COPY TO BBB年龄结构 FOR 地区=DQB
	  	USE BBB年龄结构 ALIAS 地区年龄结构
	   	INDEX ON 年份 TO IN57
	SELE 58
	  	USE &地区历年迁移估计  ALIA 地区历年常住
		CALC AVG(城迁入),STD(城迁入),AVG(乡迁入),STD(乡迁入)TO CAV05_08,CST05_08,XAV05_08,XST05_08 FOR 年份>=2005.and.年份<=2009
		CALC AVG(迁入),STD(迁入) TO DQAV05_08,DQST05_08 FOR 年份>=2005.and.年份<=2009	&&and.地区=DQB
	   	INDEX ON 年份 TO IN58
	*****年度参数*****
	*SELE 59
	*	USE &城乡总和迁移 ALIAS 总和迁移率
	*   	INDEX ON 年份 TO IN59

	总和迁移分布='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\QY45'-DQB-'按城乡户口性质性别分的净迁移概率年龄性别分布模式_基于留存率的分析'
	总和迁移预测='F:\分省生育政策仿真_基于普查\分省生育政策仿真基本参数01\迁移概率\'-'QY44'-DQB-'分城乡农非性别的总和迁移人次预测_基于留存率的分析.dbf'

	SELECT 60
		USE &城乡预期寿命 ALIAS 预期寿命预测
	   	INDEX ON 年份 TO IN60
	SELE 151
	SET RELA TO 年份 INTO 40,年份 INTO 41,年份 INTO 42,年份 INTO 43,年份 INTO 44,年份 INTO 45,年份 INTO 46,年份 INTO 47,年份 INTO 48,年份 INTO 49,年份 INTO 50,;
		年份 INTO 51,年份 INTO 52,年份 INTO 53,年份 INTO 54,年份 INTO 55,年份 INTO 56,年份 INTO 57,年份 INTO 58,年份 INTO 60,;
		年份 INTO 150,年份 INTO 152,年份 INTO 153,年份 INTO 154,年份 INTO 155,年份 INTO 156,年份 INTO 157,年份 INTO 158,年份 INTO 159,年份 INTO 160,;
		年份 INTO 181,年份 INTO 182,年份 INTO 183,年份 INTO 184,年份 INTO 185,年份 INTO 186,年份 INTO 190,年份 INTO 191,年份 INTO 192	&&年份 INTO 591,年份 INTO 592,
	LOCA FOR 年份=ND0
	EM0=预期寿命预测.城乡M
	EF0=预期寿命预测.城乡F
	CEM0=预期寿命预测.城镇M
	CEF0=预期寿命预测.城镇F
	XEM0=预期寿命预测.农村M
	XEF0=预期寿命预测.农村F
	**********年份多项结果************
	SELECT 85
		USE &地区城镇农业分年龄组夫妇及子女
		INDEX ON STR(年份,4)-妇子类别 TO IN85
	SELECT 86
		USE &地区城镇非农分年龄组夫妇及子女
		INDEX ON STR(年份,4)-妇子类别 TO IN86
	SELECT 87
		USE &地区农村农业分年龄组夫妇及子女
		INDEX ON STR(年份,4)-妇子类别 TO IN87
	SELECT 88
		USE &地区农村非农分年龄组夫妇及子女
		INDEX ON  STR(年份,4)-妇子类别  TO IN88
	SET RELATION TO STR(年份,4)-妇子类别 INTO 85,STR(年份,4)-妇子类别 INTO 86,STR(年份,4)-妇子类别 INTO 87

	*********************************
	*	分年龄预测文件和结果文件	*
	*********************************
	*****分年龄*****
	SELE 1
		USE &基年子女
		COPY TO AAA地区分龄 DELIMITED  FIEL X,HJM,HJF
		COPY TO AAA城镇分龄 DELIMITED FIEL X,城镇HJM,城镇HJF
		COPY TO AAA农村分龄 DELIMITED FIEL X,农村HJM,农村HJF
		COPY TO AAA非农分龄 DELIMITED FIEL X,非农HJM,非农HJF
		COPY TO AAA农业分龄 DELIMITED FIEL X,农业HJM,农业HJF

		COPY TO AAA城农分龄 DELIMITED FIEL X,城农HJM,城农HJF
		COPY TO AAA城非分龄 DELIMITED FIEL X,城非HJM,城非HJF
		COPY TO AAA农农分龄 DELIMITED FIEL X,农农HJM,农农HJF
		COPY TO AAA农非分龄 DELIMITED FIEL X,农非HJM,农非HJF

		USE &地区分年龄人口预测 ALIAS 地区分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA地区分龄 DELIMITED  FIELDS X,M2010,F2010
	   	ENDIF
	   	INDEX ON X TO IN01
	SELE 2
		USE &地区城镇分年龄人口预测 ALIAS 地区城镇分龄
	   	INDEX ON X TO IN2
	   	IF ND0=ND1
	   		APPEND FROM AAA城镇分龄  DELIMITED  FIELDS X,M2010,F2010
	   	ENDIF
	SELE 3
		USE &地区非农人口预测 ALIAS 地区非农分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA非农分龄 DELIMITED  FIELDS X,M2010,F2010
	   	ENDIF
	   	INDEX ON X TO IN3
	SELE 4
		USE &地区农村分年龄人口预测 ALIAS 地区农村分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA农村分龄 DELIMITED  FIELDS X,M2010,F2010
	   	ENDIF
	   	INDEX ON X TO IN4
	SELE 5
		USE &地区农业人口预测 ALIAS 地区农业分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA农业分龄 DELIMITED  FIELDS X,M2010,F2010
	   	ENDIF
	   	INDEX ON X TO IN5
	SELE 6
		USE &地区分年龄迁移人口 ALIAS 地区迁移
	   	INDEX ON X TO IN6
	SELE 7
		USE &地区分年龄死亡人口 ALIAS 地区死亡
	   	INDEX ON X TO IN7
	SELE 8
		USE &地区分年龄生育预测 ALIAS 地区生育
	   	INDEX ON X TO IN8
	SELE 9
		USE &地区非农分年龄生育预测 ALIAS 地区非农生育
	   	INDEX ON X TO IN9
	SELE 10
		USE &地区农业分年龄生育预测 ALIAS 地区农业生育
	   	INDEX ON X TO IN10
	*****城镇分年龄*****
	SELE 11
		USE  &地区城镇农业人口预测 ALIAS 地区城镇农业分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA城农分龄 DELIMITED FIEL X,M2010,F2010
	   	ENDIF
		INDEX ON X TO IN11
	SELE 12
		USE  &地区城镇非农人口预测 ALIAS 地区城镇非农分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA城非分龄 DELIMITED FIEL X,M2010,F2010
	   	ENDIF
		INDEX ON X TO IN12
	SELE 13
		USE &地区城镇分年龄迁移人口 ALIAS 地区城镇迁移
		INDEX ON X TO IN13
	SELE 14
		USE &地区城镇分年龄生育预测 ALIAS 地区城镇生育
	   	INDEX ON X TO IN14
	SELE 15
		USE &地区城镇分年龄死亡人口 ALIAS 地区城镇死亡
		INDEX ON X TO IN15
	*****农村分年龄*****
	SELE 21
		USE  &地区农村农业人口预测 ALIAS 地区农村农业分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA农农分龄 DELIMITED FIEL X,M2010,F2010
	   	ENDIF
		INDEX ON X TO IN21
	SELE 22
		USE  &地区农村非农人口预测 ALIAS 地区农村非农分龄
	   	IF ND0=ND1
	   		APPEND FROM AAA农非分龄 DELIMITED FIEL X,M2010,F2010
	   	ENDIF
		INDEX ON X TO IN22
	SELE 23
		USE &地区农村分年龄迁移人口 ALIAS 地区农村迁移
		INDEX ON X TO IN23
	SELE 24
		USE &地区农村分年龄生育预测 ALIAS 地区农村生育
	   	INDEX ON X TO IN24
	SELE 25
		USE &地区农村分年龄死亡人口 ALIAS 地区农村死亡
		INDEX ON X TO IN25
	*********地区超生***********
	SELE 30
		USE &地区分年龄超生预测 ALIAS 地区分龄超生
		INDEX ON X TO IN30
	SELE 31
		USE &地区分龄政策生育 ALIAS 地区分龄政策生育
		INDEX ON X TO IN31
	SELE 32
		USE &地区分年龄超生生育率 ALIAS 地区超生生育率
		INDEX ON X TO IN32
	SELE 33
		USE &地区城镇分年龄超生 ALIAS 地区城镇超生
		INDEX ON X TO IN33
	SELE 34
		USE &地区农村分年龄超生 ALIAS 地区农村超生
		INDEX ON X TO IN34
	SELE 35
		USE &地区非农分年龄超生 ALIAS 地区非农超生
		INDEX ON X TO IN35
	SELE 36
		USE  &地区农业分年龄超生 ALIAS 地区农业超生
		INDEX ON X TO IN36
	*************************
	*	分年龄性别基础数据	*
	*************************
	*****分年龄性别的总人口、独生子女和非独生子女*****
 	 	SELE 61 
 	 		USE &地区农村农业子女婚配预测表 ALIAS  地区农村农业子女		&&农村婚配
	    	INDEX ON X TO IN61
 	 	SELE 62 
 	 		USE &地区农村非农子女婚配预测表 ALIAS  地区农村非农子女	&&农村婚配
	    	INDEX ON X TO IN62
 	 	SELE 63 
 	 		USE &地区城镇农业子女婚配预测表 ALIAS  地区城镇农业子女		&&城镇婚配
	    	INDEX ON X TO IN63
 	 	SELE 64 
 	 		USE &地区城镇非农子女婚配预测表 ALIAS  地区城镇非农子女	&&城镇非农子女
		    INDEX ON X TO IN64
 	 	SELE 65
	 		USE &基年子女	&&='F:\分省生育政策仿真_基于普查\分省生育政策仿真基础数据01\推算数据\分龄人口\'-DQB-'分城乡户口性质性别年龄独生子女与非独生子女（2010）.DBF'
	 		COPY TO AAA地区 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
	 		COPY TO AAA农村 DELIMITED FIELDS X,农村HJ,农村HJM,农村HJF,农村D,农村DM,农村DF,农村N,农村NM,农村NF
	 		COPY TO AAA城镇 DELIMITED FIELDS X,城镇HJ,城镇HJM,城镇HJF,城镇D,城镇DM,城镇DF,城镇N,城镇NM,城镇NF 
	 		COPY TO AAA农业 DELIMITED FIELDS X,农业hj,农业hjm,农业hjf,农业d,农业dm,农业df,农业n,农业nm,农业nf
	 		COPY TO AAA非农 DELIMITED FIELDS X,非农hj,非农hjm,非农hjf,非农d,非农dm,非农df,非农n,非农nm,非农nf
	 		
			USE &地区农村子女婚配预测表 ALIAS  地区农村子女
			IF nd0=nd1
				APPEND FROM AAA农村 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
			ENDIF 
		    INDEX ON X TO IN65
 	 	SELE 66
			USE &地区城镇子女婚配预测表 ALIAS  地区城镇子女
			IF nd0=nd1
				APPEND FROM AAA城镇 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
			ENDIF 
		    INDEX ON X TO IN66
 	 	SELE 67
 	 		USE &地区农业子女婚配预测表 ALIAS 地区农业子女		
			IF nd0=nd1
				APPEND FROM AAA农业 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
			ENDIF 
		    INDEX ON X TO IN67
 	 	SELE 68
 	 		USE &地区非农子女婚配预测表 ALIAS 地区非农子女
			IF nd0=nd1
				APPEND FROM AAA非农 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
			ENDIF 
			INDEX ON X TO IN68
 	 	SELE 69
 	 		USE &地区子女婚配预测表 ALIAS 地区子女
			IF nd0=nd1
	 			APPEND FROM AAA地区 DELIMITED FIELDS X,HJ,HJM,HJF,D,DM,DF,N,NM,NF 
			ENDIF 
		    INDEX ON X TO IN69
	 FOR SE=61 TO 69
	 	SELECT IIF(SE=SE,SE,1)
	 	REPLACE HJF WITH DF+NF,HJM WITH DM+NM,HJ WITH HJF+HJM ALL
	 ENDFOR
	*****基本参数*****
	SELE 74
		USE &城镇生育模式与释放模式	ALIA 城镇生育模式 &&含堆积比例,释放模式，SYMSB1,SYMSB2
	    INDEX ON X TO IN74
	SELE 75
	    USE &乡村生育模式与释放模式	ALIA 农村生育模式 
	    INDEX ON X TO IN75
	*****死亡概率参数*****
	SELE 80
		USE &城镇死亡概率M ALIAS 城镇死亡概率M
	   	INDEX ON X TO IN80
	SELE 81
		USE &城镇死亡概率F ALIAS 城镇死亡概率F
	   	INDEX ON X TO IN81
	SELE 82
		USE &乡村死亡概率M ALIAS 农村死亡概率M
	   	INDEX ON X TO IN82
	SELE 83
		USE &乡村死亡概率F ALIAS 农村死亡概率F
	   	INDEX ON X TO IN83
	SELE 90
		USE &想生二胎比 ALIAS 想生二胎比
	   	INDEX ON X TO IN90
	SELE 91
		USE &突释二胎比 ALIAS 突释二胎比
	   	INDEX ON X TO IN91
	SELE 92
		USE &缓释二胎比 ALIAS 缓释二胎比 
	   	INDEX ON X TO IN92
	SELE 93
		USE &晚释二胎比 ALIAS 晚释二胎比
	   	INDEX ON X TO IN93
	SELE 601
		USE &地区农村农业特扶父母分龄人数 ALIAS  地区农村农业特扶
	   	INDEX ON X TO IN601
	SELE 602
		USE &地区城镇农业特扶父母分龄人数 ALIAS  地区城镇农业特扶
	   	INDEX ON X TO IN602
	SELE 603
		USE &地区农村非农特扶父母分龄人数 ALIAS  地区农村非农特扶
	   	INDEX ON X TO IN603
	SELE 604
		USE &地区城镇非农特扶父母分龄人数 ALIAS  地区城镇非农特扶
	   	INDEX ON X TO IN604
	SELE 605
		USE &地区城镇特扶父母分龄人数 ALIAS 地区城镇特扶
	   	INDEX ON X TO IN605
	SELE 606
		USE &地区农村特扶父母分龄人数 ALIAS 地区农村特扶
	   	INDEX ON X TO IN606
	SELE 607
		USE &地区非农特扶父母分龄人数 ALIAS 地区非农特扶
	   	INDEX ON X TO IN607
	SELE 608
		USE &地区农业特扶父母分龄人数 ALIAS 地区农业特扶
	   	INDEX ON X TO IN608
	SELE 609
		USE &地区特扶父母分龄人数 ALIAS 地区特扶
	   	INDEX ON X TO IN609
	SELE 701
		USE &地区农村农业一孩父母分龄人数 ALIAS  地区农村农业父母
	   	INDEX ON X TO IN701
	SELE 702
		USE &地区城镇农业一孩父母分龄人数 ALIAS  地区城镇农业父母
	   	INDEX ON X TO IN702
	SELE 703
		USE &地区农村非农一孩父母分龄人数 ALIAS  地区农村非农父母
	   	INDEX ON X TO IN703
	SELE 704
		USE &地区城镇非农一孩父母分龄人数 ALIAS  地区城镇非农父母
	   	INDEX ON X TO IN704
	SELE 705
		USE &地区城镇一孩父母分龄人数 ALIAS 地区城镇父母
	   	INDEX ON X TO IN705
	SELE 706
		USE &地区农村一孩父母分龄人数 ALIAS 地区农村父母
	   	INDEX ON X TO IN706
	SELE 707
		USE &地区非农一孩父母分龄人数 ALIAS 地区非农父母
	   	INDEX ON X TO IN707
	SELE 708
		USE &地区农业一孩父母分龄人数 ALIAS 地区农业父母
	   	INDEX ON X TO IN708
	SELE 709
		USE &地区一孩父母分龄人数 ALIAS 地区父母
	   	INDEX ON X TO IN709
	SELE 500
		USE &基年城乡存活孩子数 ALIAS 基年存活孩子
	   	INDEX ON X TO IN500
	SELE 501
		USE &地区城镇农业分年龄存活子女 ALIAS 城农存活子女
	   	INDEX ON X TO IN501
	SELE 502
		USE &地区城镇非农分年龄存活子女 ALIAS 城非存活子女
	   	INDEX ON X TO IN502
	SELE 503
		USE &地区农村农业分年龄存活子女 ALIAS 农农存活子女
	   	INDEX ON X TO IN503
	SELE 504
		USE &地区农村非农分年龄存活子女 ALIAS 农非存活子女
	   	INDEX ON X TO IN504
	SELE 505
		USE &地区城镇分年龄存活子女 ALIAS 城镇存活子女
	   	INDEX ON X TO IN505
	SELE 506
		USE &地区农村分年龄存活子女 ALIAS 农村存活子女
	   	INDEX ON X TO IN506
	SELE 507
		USE &地区农业分年龄存活子女 ALIAS 农业存活子女
	   	INDEX ON X TO IN507
	SELE 508
		USE &地区非农分年龄存活子女 ALIAS 非农存活子女
	   	INDEX ON X TO IN508
	SELE 509
		USE &地区分年龄存活子女 ALIAS 地区存活子女
	   	INDEX ON X TO IN509
	SELECT 191
	    USE &城镇非农独子父母现状 ALIAS 城镇非农独子父母现状
	   	INDEX ON X TO IN191
	SELECT 192
	    USE &城镇农业独子父母现状 ALIAS 城镇农业独子父母现状
	   	INDEX ON X TO IN192
	SELECT 193
	    USE &乡村非农独子父母现状 ALIAS 农村非农独子父母现状
	   	INDEX ON X TO IN193
	SELECT 194
	    USE &乡村农业独子父母现状 ALIAS 农村农业独子父母现状
	   	INDEX ON X TO IN194
	SELECT 195
	    USE &城镇非农特扶现状 ALIAS 城镇非农特扶现状
	   	INDEX ON X TO IN195
	SELECT 196
	    USE &城镇农业特扶现状 ALIAS 城镇农业特扶现状
	   	INDEX ON X TO IN196
	SELECT 197
	    USE &乡村非农特扶现状 ALIAS 农村非农特扶现状
	   	INDEX ON X TO IN197
	SELECT 198
	    USE &乡村农业特扶现状 ALIAS 农村农业特扶现状
	   	INDEX ON X TO IN198
	SELECT A
	SET RELA TO X INTO 2,X INTO 3,X INTO 4,X INTO 5,X INTO 6,X INTO 7,X INTO 8,X INTO 9,X INTO 10,;
				X INTO 11,X INTO 12,X INTO 13,X INTO 14,X INTO 15,;
				X INTO 21,X INTO 22,X INTO 23,X INTO 24,X INTO 25,X INTO 30,X INTO 31,X INTO 32,X INTO 33,X INTO 34,X INTO 35,;
				X INTO 61,X INTO 62,X INTO 63,X INTO 64,X INTO 65,X INTO 66,X INTO 67,X INTO 68,X INTO 69,;
				X INTO 74,X INTO 75,X INTO 76,X INTO 77,X INTO 78,;
				X INTO 80,X INTO 81,X INTO 82,X INTO 83,X INTO 84,X INTO 90,X INTO 91,X INTO 92,X INTO 93,;
				X INTO 101,X INTO 102,X INTO 103,X INTO 104,X INTO 105,X INTO 106,X INTO 107,X INTO 108,X INTO 109,X INTO 110,X INTO 111,X INTO 112,X INTO 113,X INTO 114,;
				X INTO 116,X INTO 117,X INTO 118,X INTO 126,X INTO 127,X INTO 128,X INTO 130,X INTO 131,X INTO 132,X INTO 133,X INTO 134,X INTO 135,X INTO 140,X INTO 141,;
				X INTO 161,X INTO 162,X INTO 163,X INTO 164,X INTO 165,X INTO 166,X INTO 167,X INTO 168,X INTO 169,;
				x INTO 500,X INTO 501,X INTO 502,X INTO 503,X INTO 504,X INTO 505,X INTO 506,X INTO 507,X INTO 508,X INTO 509,;
				X INTO 601,X INTO 602,X INTO 603,X INTO 604,X INTO 605,X INTO 606,X INTO 607,X INTO 608,X INTO 609,;
				X INTO 701,X INTO 702,X INTO 703,X INTO 704,X INTO 705,X INTO 706,X INTO 707,X INTO 708,X INTO 709,;
				X INTO 801,X INTO 802,X INTO 803,X INTO 804,X INTO 805,X INTO 806,X INTO 807,X INTO 808,X INTO 809,;
				X INTO 901,X INTO 902,X INTO 903,X INTO 904,X INTO 905,X INTO 906,X INTO 907,X INTO 908,X INTO 909,;
				X INTO 191,X INTO 192,X INTO 193,X INTO 194,X INTO 195,X INTO 196,X INTO 197,X INTO 198	&&X INTO 84,X INTO 85,X INTO 86,X INTO 87
	 	IF nd0=nd1
			REPLACE 地区存活子女.b12010 WITH 基年存活孩子.b1,地区存活子女.b22010 WITH 基年存活孩子.b2 ALL 
			REPLACE 城镇存活子女.b12010 WITH 基年存活孩子.城镇b1,城镇存活子女.b22010 WITH 基年存活孩子.城镇b2 ALL 
			REPLACE 农村存活子女.b12010 WITH 基年存活孩子.农村b1,农村存活子女.b22010 WITH 基年存活孩子.农村b2 ALL 
			REPLACE 城农存活子女.b12010 WITH 基年存活孩子.城农b1,城农存活子女.b22010 WITH 基年存活孩子.城农b2 ALL 
			REPLACE 城非存活子女.b12010 WITH 基年存活孩子.城非b1,城非存活子女.b22010 WITH 基年存活孩子.城非b2 ALL 
			REPLACE 农农存活子女.b12010 WITH 基年存活孩子.农农b1,农农存活子女.b22010 WITH 基年存活孩子.农农b2 ALL 
			REPLACE 农非存活子女.b12010 WITH 基年存活孩子.农非b1,农非存活子女.b22010 WITH 基年存活孩子.农非b2 ALL 
			REPLACE 农业存活子女.b12010 WITH 基年存活孩子.城农b1+基年存活孩子.农农b1,农业存活子女.b22010 WITH 基年存活孩子.城农b2+基年存活孩子.农农b2 ALL 
			REPLACE 非农存活子女.b12010 WITH 基年存活孩子.农非b1+基年存活孩子.城非b1,非农存活子女.b22010 WITH 基年存活孩子.农非b2+基年存活孩子.城非b2 ALL 
		ENDIF
		
	 	
 	 	REPL 地区农村农业子女.FR WITH 农村生育模式.SYMSB,地区农村农业子女.FR1 WITH 农村生育模式.SYMS1修匀,地区农村农业子女.FR2 WITH 农村生育模式.SYMS2修匀 ALL
 	 	REPL 地区城镇农业子女.FR WITH 农村生育模式.SYMSB,地区城镇农业子女.FR1 WITH 农村生育模式.SYMS1修匀,地区城镇农业子女.FR2 WITH 农村生育模式.SYMS2修匀 ALL
 	 	REPL 地区农村非农子女.FR WITH 城镇生育模式.SYMSB,地区农村非农子女.FR1 WITH 城镇生育模式.SYMS1修匀,地区农村非农子女.FR2 WITH 城镇生育模式.SYMS2修匀 ALL
 	 	REPL 地区城镇非农子女.FR WITH 城镇生育模式.SYMSB,地区城镇非农子女.FR1 WITH 城镇生育模式.SYMS1修匀,地区城镇非农子女.FR2 WITH 城镇生育模式.SYMS2修匀 ALL
	 	REPL 地区子女.HJF WITH 地区农业子女.HJF+地区非农子女.HJF ALL
	 	
	 	REPLACE 地区城镇非农父母.m2010 WITH 城镇非农独子父母现状.M2010,地区城镇非农父母.F2010  WITH 城镇非农独子父母现状.F2010,地区城镇非农特扶.M2010 WITH 城镇非农特扶现状.M2010,地区城镇非农特扶.F2010 WITH 城镇非农特扶现状.F2010 ALL
	 	REPLACE 地区城镇农业父母.m2010 WITH 城镇农业独子父母现状.M2010,地区城镇农业父母.F2010  WITH 城镇农业独子父母现状.F2010,地区城镇农业特扶.M2010 WITH 城镇农业特扶现状.M2010,地区城镇农业特扶.F2010 WITH 城镇农业特扶现状.F2010 ALL
	 	REPLACE 地区农村非农父母.m2010 WITH 农村非农独子父母现状.M2010,地区农村非农父母.F2010  WITH 农村非农独子父母现状.F2010,地区农村非农特扶.M2010 WITH 农村非农特扶现状.M2010,地区农村非农特扶.F2010 WITH 农村非农特扶现状.F2010 ALL
	 	REPLACE 地区农村农业父母.m2010 WITH 农村农业独子父母现状.M2010,地区农村农业父母.F2010  WITH 农村农业独子父母现状.F2010,地区农村农业特扶.M2010 WITH 农村农业特扶现状.M2010,地区农村农业特扶.F2010 WITH 农村农业特扶现状.F2010 ALL

		REPL 地区城镇父母.M2010 WITH 地区城镇非农父母.M2010+地区城镇农业父母.M2010,地区城镇父母.F2010 WITH 地区城镇非农父母.F2010+地区城镇农业父母.F2010 ALL
		REPL 地区农村父母.M2010 WITH 地区农村非农父母.M2010+地区农村农业父母.M2010,地区农村父母.F2010 WITH 地区农村非农父母.F2010+地区农村农业父母.F2010 ALL
		REPL 地区非农父母.M2010 WITH 地区城镇非农父母.M2010+地区农村非农父母.M2010,地区非农父母.F2010 WITH 地区城镇非农父母.F2010+地区农村非农父母.F2010 ALL
		REPL 地区农业父母.M2010 WITH 地区农村农业父母.M2010+地区城镇农业父母.M2010,地区农业父母.F2010 WITH 地区农村农业父母.F2010+地区城镇农业父母.F2010 ALL
		REPL 地区父母.M2010 WITH 地区城镇父母.M2010+地区农村父母.M2010,地区父母.F2010 WITH 地区城镇父母.F2010+地区农村父母.F2010 ALL

		REPL 地区城镇特扶.M2010 WITH 地区城镇非农特扶.M2010+地区城镇农业特扶.M2010,地区城镇特扶.F2010 WITH 地区城镇非农特扶.F2010+地区城镇农业特扶.F2010 ALL
		REPL 地区农村特扶.M2010 WITH 地区农村非农特扶.M2010+地区农村农业特扶.M2010,地区农村特扶.F2010 WITH 地区农村非农特扶.F2010+地区农村农业特扶.F2010 ALL
		REPL 地区非农特扶.M2010 WITH 地区城镇非农特扶.M2010+地区农村非农特扶.M2010,地区非农特扶.F2010 WITH 地区城镇非农特扶.F2010+地区农村非农特扶.F2010 ALL
		REPL 地区农业特扶.M2010 WITH 地区农村农业特扶.M2010+地区城镇农业特扶.M2010,地区农业特扶.F2010 WITH 地区农村农业特扶.F2010+地区城镇农业特扶.F2010 ALL
		REPL 地区特扶.M2010 WITH 地区城镇特扶.M2010+地区农村特扶.M2010,地区特扶.F2010 WITH 地区城镇特扶.F2010+地区农村特扶.F2010 ALL

RETURN 

*******************************************

PROCEDURE 释放模式确定
*DO CASE
*CASE 释放模式='分释模式'.or.释放模式='意愿模式'	&&SFMS=1		&&30岁以下:正常模式;30--35岁:缓释模式;35岁以上:突释模式

*	RELEASE AR农业缓释模式,AR农业突释模式,AR非农缓释模式,AR非农突释模式


	*REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.堆积比例 ,地区农村农业子女.释放模式 WITH IIF(X>=30.AND.X<=35,农村生育模式.缓释模式,IIF(X>=35,农村生育模式.突释模式,农村生育模式.正常模式))	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.堆积比例 ,地区农村非农子女.释放模式 WITH IIF(X>=30.AND.X<=35,城镇生育模式.缓释模式,IIF(X>=35,城镇生育模式.突释模式,城镇生育模式.正常模式))	ALL
	*REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.堆积比例 ,地区城镇农业子女.释放模式 WITH IIF(X>=30.AND.X<=35,农村生育模式.缓释模式,IIF(X>=35,农村生育模式.突释模式,农村生育模式.正常模式))	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.堆积比例 ,地区城镇非农子女.释放模式 WITH IIF(X>=30.AND.X<=35,城镇生育模式.缓释模式,IIF(X>=35,城镇生育模式.突释模式,城镇生育模式.正常模式))	ALL

	*REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区农村农业子女.释放模式 WITH IIF(X>=30.AND.X<=35,农村生育模式.缓释模式,IIF(X>=35,农村生育模式.突释模式,农村生育模式.正常模式))	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区农村非农子女.释放模式 WITH IIF(X>=30.AND.X<=35,城镇生育模式.缓释模式,IIF(X>=35,城镇生育模式.突释模式,城镇生育模式.正常模式))	ALL
	*REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区城镇农业子女.释放模式 WITH IIF(X>=30.AND.X<=35,农村生育模式.缓释模式,IIF(X>=35,农村生育模式.突释模式,农村生育模式.正常模式))	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区城镇非农子女.释放模式 WITH IIF(X>=30.AND.X<=35,城镇生育模式.缓释模式,IIF(X>=35,城镇生育模式.突释模式,城镇生育模式.正常模式))	ALL

	*REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 	ALL
	*REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	*REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 	ALL

	REPL 地区农村农业子女.堆积比例 WITH 农农存活子女.&B1ND1/地区农村农业子女.HJF FOR 地区农村农业子女.HJF<>0					&&农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	REPL 地区农村非农子女.堆积比例 WITH 农非存活子女.&B1ND1/地区农村非农子女.HJF FOR 地区农村非农子女.HJF<>0					&&城镇生育模式.待生二孩比 	ALL
	REPL 地区城镇农业子女.堆积比例 WITH 城农存活子女.&B1ND1/地区城镇农业子女.HJF FOR 地区城镇农业子女.HJF<>0					&&农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	REPL 地区城镇非农子女.堆积比例 WITH 农农存活子女.&B1ND1/地区城镇非农子女.HJF FOR 地区城镇非农子女.HJF<>0					&&城镇生育模式.待生二孩比 	ALL

	
	COUN TO AR农业缓释N  FOR 农村生育模式.缓释模式<>0
	COPY TO ARRA AR农业缓释模式 FIEL 农村生育模式.缓释模式 FOR 农村生育模式.缓释模式<>0
	COPY TO ARRA AR农业突释模式 FIEL 农村生育模式.突释模式 FOR 农村生育模式.突释模式<>0
	
	COUN TO AR非农缓释N  FOR 城镇生育模式.缓释模式<>0
	COPY TO ARRA AR非农缓释模式 FIEL 城镇生育模式.缓释模式 FOR 城镇生育模式.缓释模式<>0
	COPY TO ARRA AR非农突释模式 FIEL 城镇生育模式.突释模式 FOR 城镇生育模式.突释模式<>0

*DISPLAY MEMORY LIKE AR非农缓释模式

*JNMK

*CASE 释放模式='缓释模式'.or.释放模式='突释模式'    &&SFMS=2.OR.SFMS=3	&&
*	REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.堆积比例 ,地区农村农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.堆积比例 ,地区农村非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
*	REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.堆积比例 ,地区城镇农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.堆积比例 ,地区城镇非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
*	REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区农村农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区农村非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
*	REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区城镇农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区城镇非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
	*COPY TO ARRA AR释放模式 FIEL 堆积参数.&释放模式 FOR 堆积参数.&释放模式<>0
	*COUN TO ARN  FOR 堆积参数.&释放模式<>0
*	COPY TO ARRA AR农业释放模式 FIEL 农村生育模式.&释放模式 FOR 农村生育模式.&释放模式<>0
*	COUN TO AR农业释放N  FOR 农村生育模式.&释放模式<>0
*	COPY TO ARRA AR非农释放模式 FIEL 城镇生育模式.&释放模式 FOR 城镇生育模式.&释放模式<>0
*	COUN TO AR非农释放N  FOR 城镇生育模式.&释放模式<>0
*CASE 释放模式='正常模式'	&&SFMS=4
*	REPL 地区农村农业子女.堆积比例 WITH 0 ,地区农村农业子女.释放模式 WITH 0	ALL 
*	REPL 地区农村非农子女.堆积比例 WITH 0 ,地区农村非农子女.释放模式 WITH 0	ALL
*	REPL 地区城镇农业子女.堆积比例 WITH 0 ,地区城镇农业子女.释放模式 WITH 0	ALL
*	REPL 地区城镇非农子女.堆积比例 WITH 0 ,地区城镇非农子女.释放模式 WITH 0	ALL
	*COPY TO ARRA AR释放模式 FIEL 堆积参数.&释放模式 FOR 堆积参数.&释放模式<>0
	*COUN TO ARN  FOR 堆积参数.&释放模式<>0
*	COPY TO ARRA AR农业释放模式 FIEL 农村生育模式.&释放模式 FOR 农村生育模式.&释放模式<>0
*	COUN TO AR农业释放N  FOR 农村生育模式.&释放模式<>0
*	COPY TO ARRA AR非农释放模式 FIEL 城镇生育模式.&释放模式 FOR 城镇生育模式.&释放模式<>0
*	COUN TO AR非农释放N  FOR 城镇生育模式.&释放模式<>0
*ENDCASE
RETURN 
		
*******************************************
PROCEDURE 单时机调整终身政策生育率
SELECT 207
LOCATE FOR 地区=DQB
民族B=1-IIF(地区='广西',汉族B+壮族B,汉族B)
现行N1D2C=城居
现行N2D1C=城居
现行NNFC=城居
现行N1D2N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
现行N2D1N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
现行NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
*****
DDFR=2.000
DO CASE
CASE ND0<非农时机2
	政策='现行'
	调整时机=非农时机2
CASE  ND0>=非农时机2
	政策=非农ZC2
	调整时机=非农时机2
ENDCASE
*********终身生育率********
DO CASE 
	CASE 政策='现行'
 		 N1D2C=现行N1D2C
 		 N2D1C=现行N2D1C
 		 NNFC=现行NNFC
 		 N1D2N=现行N1D2N
 		 N2D1N=现行N2D1N
 		 NNFN =现行NNFN 
 	CASE 政策='单独'
 		 STORE 2 TO N1D2C,N2D1C,N1D2N,N2D1N
 		 NNFC=1
 		 NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
 	CASE 政策='单独后'
 		 STORE 2 TO N1D2C,N2D1C,N1D2N,N2D1N
 		 NNFC=1
 		 NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
 	CASE 政策='普二'
 		 STORE 2 TO N1D2C,N2D1C,NNFC,N1D2N,N2D1N,NNFN
 	ENDCASE
RETURN 

*******************************************
PROCEDURE 渐进普二终身政策生育率
*********************
*	终身政策生育率	*
*********************
SELECT 207
LOCATE FOR 地区=DQB
民族B=1-IIF(地区='广西',汉族B+壮族B,汉族B)
现行N1D2C=城居
现行N2D1C=城居
现行NNFC=城居
现行N1D2N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
现行N2D1N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
现行NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
*****
DDFR=2.000
****政策及调整时机***
DO CASE
CASE ND0<非农时机2.AND.非农时机1=0
	政策='现行'
CASE  ND0>=非农时机1.AND.ND0<非农时机2
	政策=非农ZC1
CASE  ND0>=非农时机2.AND.ND0<非农时机3
	政策=非农ZC2
CASE  ND0>=非农时机3
	政策=非农ZC3
ENDCASE
调整时机=IIF(ND0>=非农时机1.and.ND0<非农时机2,非农时机1,IIF(ND0>=非农时机2.and.ND0<非农时机3,非农时机2,IIF(ND0>=非农时机3,非农时机3,0)))
*********终身生育率********
DO CASE 
CASE 政策='现行'
	 N1D2C=城居
	 N2D1C=城居
	 NNFC=城居
	 N1D2N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
	 N2D1N=IIF(双少民族户=2.OR.少数民族=2,农村单独*(1-民族B)+2*民族B,农村单独)
	 NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
CASE 政策='单独'
	 STORE 2 TO N1D2C,N2D1C,N1D2N,N2D1N
	 NNFC=1
	 NNFN=IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
CASE 政策='单独后'
	 STORE 2 TO N1D2C,N2D1C,N1D2N,N2D1N
	 NNFC=1
	 NNFN =IIF(双少民族户=2.OR.少数民族=2,农村双非*(1-民族B)+2*民族B,农村双非)
CASE 政策='普二'
	 STORE 2 TO N1D2C,N2D1C,NNFC,N1D2N,N2D1N,NNFN
ENDCASE
RETURN 


*******************************************
PROCEDURE 年龄移算
************农村\城镇分独生子女和非独生子女年龄移算***************
*****子女年龄移算******
FOR DN=1  TO  4
 	RELE NCNY,NCFN,CZNY,CZFN
 	*FIEM=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DQY',IIF(DN=4,'NQY',IIF(DN=5,'DDB','HJ')))))-'M'
 	*FIEF=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DQY',IIF(DN=4,'NQY',IIF(DN=5,'DDB','HJ')))))-'F'
 	FIEM=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DDB','HJ')))-'M'
 	FIEF=IIF(DN=1,'D',IIF(DN=2,'N',IIF(DN=3,'DDB','HJ')))-'F'
 	COPY TO ARRA NCNY FIEL 地区农村农业子女.&FIEM,地区农村农业子女.&FIEF
 	COPY TO ARRA NCFN FIEL 地区农村非农子女.&FIEM,地区农村非农子女.&FIEF
 	COPY TO ARRA CZNY FIEL 地区城镇农业子女.&FIEM,地区城镇农业子女.&FIEF
 	COPY TO ARRA CZFN FIEL 地区城镇非农子女.&FIEM,地区城镇非农子女.&FIEF
 	REPL 地区农村农业子女.&FIEM WITH NCNY(RECN()-1,1)*(1-NYQM(RECN()-1)), 地区农村农业子女.&FIEF WITH NCNY(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
 	REPL 地区农村非农子女.&FIEM WITH NCFN(RECN()-1,1)*(1-FNQM(RECN()-1)), 地区农村非农子女.&FIEF WITH NCFN(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
 	REPL 地区城镇农业子女.&FIEM WITH CZNY(RECN()-1,1)*(1-NYQM(RECN()-1)), 地区城镇农业子女.&FIEF WITH CZNY(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
 	REPL 地区城镇非农子女.&FIEM WITH CZFN(RECN()-1,1)*(1-FNQM(RECN()-1)), 地区城镇非农子女.&FIEF WITH CZFN(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
 	REPL 地区农村农业子女.&FIEM WITH 0, 地区农村农业子女.&FIEF WITH 0,地区农村非农子女.&FIEM WITH 0, 地区农村非农子女.&FIEF WITH 0 ,;
 	     地区城镇农业子女.&FIEM WITH 0, 地区城镇农业子女.&FIEF WITH 0,地区城镇非农子女.&FIEM WITH 0, 地区城镇非农子女.&FIEF WITH 0 FOR RECN()=1
 	REPL 地区农业子女.&FIEM WITH 地区农村农业子女.&FIEM+地区城镇农业子女.&FIEM,地区农业子女.&FIEF WITH 地区农村农业子女.&FIEF+地区城镇农业子女.&FIEF,;
 		 地区非农子女.&FIEM WITH 地区农村非农子女.&FIEM+地区城镇非农子女.&FIEM,地区非农子女.&FIEF WITH 地区农村非农子女.&FIEF+地区城镇非农子女.&FIEF ALL
 	REPL 地区子女.&FIEM WITH 地区农业子女.&FIEM+地区非农子女.&FIEM,地区子女.&FIEF WITH 地区农业子女.&FIEF+地区非农子女.&FIEF ALL
ENDFOR
FOR CX=1 TO 2 
	SELE A	
	农业子女='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	非农子女='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	*****可二后年龄移算******
 	RELE DJM
 	COPY TO ARRA DJM  FIEL &农业子女..堆积丈夫,&非农子女..堆积丈夫
 	REPL &农业子女..堆积丈夫 WITH DJM(RECN()-1,1)*(1-NYQM(RECN()-1)),&非农子女..堆积丈夫 WITH DJM(RECN()-1,2)*(1-FNQM(RECN()-1)) FOR RECN()>1
	*****单独\双非堆积年龄移算，单独\双非堆积年龄移算,因按女性年龄和人数计算堆积人群的生育数和生育率,故按女性死亡概率进行年龄移算*****
	FOR DJ=1  TO  12
	 	堆积FI1=IIF(DJ=1,'男单堆积',IIF(DJ=2,'已2男单',IIF(DJ=3,'女单堆积',IIF(DJ=4,'已2女单',IIF(DJ=5,'双非堆积',IIF(DJ=6,'已2双非',;
	 	IIF(DJ=7,'单独堆积15',IIF(DJ=8,'单独堆积30',IIF(DJ=9,'单独堆积35',IIF(DJ=10,'双非堆积15',IIF(DJ=11,'双非堆积30','双非堆积35')))))))))))
		RELE NCNY,NCFN
	 	COPY TO ARRA NCNY FIEL &农业子女..&堆积FI1
	 	COPY TO ARRA NCFN FIEL &非农子女..&堆积FI1
	 	REPL &农业子女..&堆积FI1 WITH NCNY(RECN()-1)*(1-NYQF(RECN()-1)) FOR RECN()>1
	 	REPL &非农子女..&堆积FI1 WITH NCFN(RECN()-1)*(1-FNQF(RECN()-1)) FOR RECN()>1
	ENDFOR
	REPL &农业子女..单独堆积 WITH &农业子女..女单堆积+&农业子女..男单堆积,&农业子女..已2单独 WITH &农业子女..已2男单+&农业子女..已2女单 ALL
	REPL &非农子女..单独堆积 WITH &非农子女..女单堆积+&非农子女..男单堆积,&非农子女..已2单独 WITH &非农子女..已2男单+&非农子女..已2女单 ALL
ENDFOR
*********************
SELECT A
FOR SE=61 TO 64
	SELECT IIF(SE=SE,SE,1)
	REPLACE DM WITH DM+DMQY FOR DM+DMQY>0
	REPLACE NM WITH NM+NMQY FOR NM+NMQY>0
	REPLACE DDBM WITH DDBM+DDBMQY FOR DDBM+DDBMQY>0
	
	REPLACE DF WITH DF+DFQY FOR DF+DFQY>0
	REPLACE NF WITH NF+NFQY FOR NF+NFQY>0
	REPLACE DDBF WITH DDBF+DDBFQY FOR DDBF+DDBFQY>0
ENDFOR

FOR SE=61 TO 69
	SELECT IIF(SE=SE,SE,1)
 	REPLACE DQY WITH DFQY+DMQY,NQY WITH NFQY+NMQY,DDBQY WITH DDBMQY+DDBFQY,QYHJF WITH DFQY+NFQY+DDBFQY,QYHJM WITH DMQY+NMQY+DDBMQY,QYHJ WITH QYHJF+QYHJM ALL
ENDFOR
SELECT A
REPL 地区城镇迁移.&MND2 WITH 地区城镇非农子女.DMQY+地区城镇非农子女.NMQY+地区城镇非农子女.DDBMQY+地区城镇农业子女.DMQY+地区城镇农业子女.NMQY+地区城镇农业子女.DDBMQY,;
	 地区城镇迁移.&FND2 WITH 地区城镇非农子女.DFQY+地区城镇非农子女.NFQY+地区城镇非农子女.DDBFQY+地区城镇农业子女.DFQY+地区城镇农业子女.NFQY+地区城镇农业子女.DDBFQY ALL
REPL 地区农村迁移.&MND2 WITH 地区农村非农子女.DMQY+地区农村非农子女.NMQY+地区农村非农子女.DDBMQY+地区农村农业子女.DMQY+地区农村农业子女.NMQY+地区农村农业子女.DDBMQY,;
	 地区农村迁移.&FND2 WITH 地区农村非农子女.DFQY+地区农村非农子女.NFQY+地区农村非农子女.DDBFQY+地区农村农业子女.DFQY+地区农村农业子女.NFQY+地区农村农业子女.DDBFQY ALL
REPL 地区迁移.&MND2 WITH 地区城镇迁移.&MND2+地区农村迁移.&MND2,地区迁移.&FND2 WITH 地区城镇迁移.&FND2+地区农村迁移.&FND2 ALL


***按存活子女数分的妇女年龄移算及迁移估计*****
FOR  SE=1 TO 4
	ALI1= IIF(se=1,'城农',IIF(se=2,'城非',IIF(se=3,'农农','城非')))-'存活子女'
	SWGL=IIF(se=1.or.SE=3,'NYQF','FNQF')
	*QYGL=IIF(se=1.or.SE=3,'农村迁移概率.净迁概率F','城镇迁移概率.净迁概率F')
	QYGL=IIF(se=1,'城农',IIF(se=2,'城非',IIF(se=3,'农农','城非')))-'F'
	*QYB0=IIF(se=1.or.SE=3,XQYB,CQYB)
	COPY TO ARRAY ARQYGL FIELDS 城乡迁移概率.&QYGL
	COPY TO ARRAY ARB FIELDS &ALI1..&B1ND1,&ALI1..&B2ND1
 	REPLACE &ALI1..&B1ND2 WITH ARB(RECN()-1,1)*(1-&SWGL(RECN()-1))+ARB(RECN()-1,1)*ARQYGL(RECN()-1,1)*QYB,&ALI1..&B2ND2 WITH ARB(RECN()-1,2)*(1-&SWGL(RECN()-1))+ARB(RECN()-1,2)*ARQYGL(RECN()-1,2)*QYB  FOR RECN()>1
ENDFOR 
SELECT A
REPLACE 城镇存活子女.&B1ND2 WITH 城农存活子女.&B1ND2+城非存活子女.&B1ND2,;
		农村存活子女.&B1ND2 WITH 农农存活子女.&B1ND2+农非存活子女.&B1ND2,;
		非农存活子女.&B1ND2 WITH 城非存活子女.&B1ND2+农非存活子女.&B1ND2,;
		农业存活子女.&B1ND2 WITH 城农存活子女.&B1ND2+农农存活子女.&B1ND2,;
		地区存活子女.&B1ND2 WITH 城镇存活子女.&B1ND2+农村存活子女.&B1ND2 ALL
REPLACE 城镇存活子女.&B2ND2 WITH 城农存活子女.&B2ND2+城非存活子女.&B2ND2,;
		农村存活子女.&B2ND2 WITH 农农存活子女.&B2ND2+农非存活子女.&B2ND2,;
		非农存活子女.&B2ND2 WITH 城非存活子女.&B2ND2+农非存活子女.&B2ND2,;
		农业存活子女.&B2ND2 WITH 城农存活子女.&B2ND2+农农存活子女.&B2ND2,;
		地区存活子女.&B2ND2 WITH 城镇存活子女.&B2ND2+农村存活子女.&B2ND2 ALL
RETURN 

**************************
PROCEDURE 堆积生育模式释放法	&&
IF S堆积<>0
 	DO CASE
 	 	CASE 释放模式='分释模式'.AND.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
			REPL &子女..单独释放B WITH &子女..单独堆积30*&AR缓释模式(ND0-调整时机+1)+&子女..单独堆积35*&AR突释模式(ND0-调整时机+1)+&子女..单独堆积15*&子女..FR2 FOR X<=49
			REPL &子女..双非释放B WITH &子女..双非堆积30*&AR缓释模式(ND0-调整时机+1)+&子女..双非堆积35*&AR突释模式(ND0-调整时机+1)+&子女..双非堆积15*&子女..FR2 FOR X<=49
			SUM  &子女..单独释放B,&子女..双非释放B TO 单放B2,非放B2
			单独B2=SNMDF2+SNFDM2+单放B2
			SNN2=SNN2+非放B2
 	CASE 释放模式='缓释模式'.AND.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
			REPL &子女..单独释放B WITH &子女..单独堆积*&AR缓释模式(ND0-调整时机+1) FOR X<=49
			REPL &子女..双非释放B WITH &子女..双非堆积*&AR缓释模式(ND0-调整时机+1) FOR X<=49
			SUM  &子女..单独释放B TO 单放B2
			SUM  &子女..双非释放B TO 非放B2
			单独B2=SNMDF2+SNFDM2+单放B2
			SNN2=SNN2+非放B2
  	CASE 释放模式='突释模式'.AND.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
			REPL &子女..单独释放B WITH &子女..单独堆积*&AR突释模式(ND0-调整时机+1) FOR X<=49
			REPL &子女..双非释放B WITH &子女..双非堆积*&AR突释模式(ND0-调整时机+1) FOR X<=49
			SUM  &子女..单独释放B TO 单放B2
			SUM  &子女..双非释放B TO 非放B2
			单独B2=SNMDF2+SNFDM2+单放B2
			SNN2=SNN2+非放B2
		ENDIF
	CASE 释放模式='正常模式'.AND.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		STORE 0 TO 单放B2,非放B2,农业单独堆积,农业双非堆积,非农单放B2,非农非放B2,非农单独堆积,非农双非堆积
		单独B2=SNMDF2+SNFDM2
		农业单放B2=单放B2
		农业非放B2=非放B2
	OTHERWISE 
		STORE 0 TO 单放B2,非放B2
		单独B2=SNMDF2+SNFDM2
		SNN2=SNN2
 	ENDCASE

1
BROWSE FIELDS  	&子女..单独释放B,&子女..双非释放B,
 ?3
 VF	
ENDIF
?单放B2,非放B2
RETURN 
**************************

**************************
PROCEDURE 不同政策实现程度下的堆积生育释放法	&&替代“PROCEDURE 堆积生育意愿释放法”和“PROC 堆积生育模式释放法”
地带=ARDQ多种参数(dqX,14)
地带1=IIF(地带='西部','四川',IIF(地带='东北','东部',地带))
实现比=IIF(实现方式='可能生育',实现比,1)
RELEASE AR非农释放模式,AR非农缓释模式,AR非农突释模式,AR农业释放模式,AR农业缓释模式,AR农业突释模式
*DO CASE
*CASE 释放模式='分释模式'.or.释放模式='意愿模式'	&&SFMS=1		&&30岁以下:正常模式;30--35岁:缓释模式;35岁以上:突释模式
	REPL 地区农村农业子女.堆积比例 WITH 农农存活子女.&B1ND1/地区农村农业子女.HJF FOR 地区农村农业子女.HJF<>0			&&农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	REPL 地区农村非农子女.堆积比例 WITH 农非存活子女.&B1ND1/地区农村非农子女.HJF FOR 地区农村非农子女.HJF<>0					&&城镇生育模式.待生二孩比 	ALL
	REPL 地区城镇农业子女.堆积比例 WITH 城农存活子女.&B1ND1/地区城镇农业子女.HJF FOR 地区城镇农业子女.HJF<>0					&&农村生育模式.待生二孩比 	ALL	&&,单独堆积 N(8),双非堆积 
	REPL 地区城镇非农子女.堆积比例 WITH 农农存活子女.&B1ND1/地区城镇非农子女.HJF FOR 地区城镇非农子女.HJF<>0					&&城镇生育模式.待生二孩比 	ALL
	
	COPY TO ARRA AR农业缓释模式 FIEL 农村生育模式.缓释模式 FOR 农村生育模式.缓释模式<>0
	COUN TO AR农业缓释N  FOR 农村生育模式.缓释模式<>0
	
	COPY TO ARRA AR农业突释模式 FIEL 农村生育模式.突释模式 FOR 农村生育模式.突释模式<>0
	COUN TO AR农业突释N  FOR 农村生育模式.突释模式<>0
	
	COPY TO ARRA AR非农缓释模式 FIEL 城镇生育模式.缓释模式 FOR 城镇生育模式.缓释模式<>0
	COUN TO AR非农缓释N  FOR 城镇生育模式.缓释模式<>0
	
	COPY TO ARRA AR非农突释模式 FIEL 城镇生育模式.突释模式 FOR 城镇生育模式.突释模式<>0
	COUN TO AR非农突释N  FOR 城镇生育模式.突释模式<>0

*CASE 释放模式='缓释模式'.or.释放模式='突释模式'    &&SFMS=2.OR.SFMS=3	&&
*	REPL 地区农村农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区农村农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区农村非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区农村非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
*	REPL 地区城镇农业子女.堆积比例 WITH 农村生育模式.待生二孩比 ,地区城镇农业子女.释放模式 WITH 农村生育模式.&释放模式	ALL
*	REPL 地区城镇非农子女.堆积比例 WITH 城镇生育模式.待生二孩比 ,地区城镇非农子女.释放模式 WITH 城镇生育模式.&释放模式	ALL
*	COPY TO ARRA AR农业释放模式 FIEL 农村生育模式.&释放模式 FOR 农村生育模式.&释放模式<>0
*	COUN TO AR农业释放N  FOR 农村生育模式.&释放模式<>0
*	COPY TO ARRA AR非农释放模式 FIEL 城镇生育模式.&释放模式 FOR 城镇生育模式.&释放模式<>0
*	COUN TO AR非农释放N  FOR 城镇生育模式.&释放模式<>0
*CASE 释放模式='正常模式'	&&SFMS=4
*	REPL 地区农村农业子女.堆积比例 WITH 0 ,地区农村农业子女.释放模式 WITH 0	ALL 
*	REPL 地区农村非农子女.堆积比例 WITH 0 ,地区农村非农子女.释放模式 WITH 0	ALL
*	REPL 地区城镇农业子女.堆积比例 WITH 0 ,地区城镇农业子女.释放模式 WITH 0	ALL
*	REPL 地区城镇非农子女.堆积比例 WITH 0 ,地区城镇非农子女.释放模式 WITH 0	ALL
*	COPY TO ARRA AR农业释放模式 FIEL 农村生育模式.&释放模式 FOR 农村生育模式.&释放模式<>0
*	COUN TO AR农业释放N  FOR 农村生育模式.&释放模式<>0
*	COPY TO ARRA AR非农释放模式 FIEL 城镇生育模式.&释放模式 FOR 城镇生育模式.&释放模式<>0
*	COUN TO AR非农释放N  FOR 城镇生育模式.&释放模式<>0
*ENDCASE
AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
AR突释模式=IIF(CX=1.OR.CX=3,'AR农业突释模式','AR非农突释模式')
*AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
*IF (释放模式='分释模式'.or.释放模式='意愿模式')
	 AR缓N=IIF(CX=1.OR.CX=3,AR农业缓释N,AR非农缓释N)
	 AR突N=IIF(CX=1.OR.CX=3,AR农业突释N,AR非农突释N)
	* AR释N=IIF(CX=1.OR.CX=3,AR农业释放N,AR非农释放N)
*ELSE
*	 AR缓N=IIF(CX=1.OR.CX=3,AR农业缓释N,AR非农缓释N)
*	 AR突N=IIF(CX=1.OR.CX=3,AR农业突释N,AR非农突释N)
	* AR释N=IIF(CX=1.OR.CX=3,AR农业释放N,AR非农释放N)
*ENDIF
DO CASE
CASE 实现方式='政策生育'.AND.释放模式='意愿模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*晚释二胎比.&地带1*&子女..FR2 ALL
		REPL &子女..双非释放B WITH &子女..双非堆积*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*晚释二胎比.&地带1*&子女..FR2 ALL
		REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
CASE 实现方式='政策生育'.AND.释放模式='分释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积30*&AR缓释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积35*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积15*&子女..FR2 ALL
		REPL &子女..双非释放B WITH &子女..双非堆积30*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积35*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积15*&子女..FR2 ALL
		REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
CASE 实现方式='政策生育'.AND.释放模式='缓释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&AR缓释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非堆积*&AR缓释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR缓N
		REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
CASE 实现方式='政策生育'.AND.释放模式='突释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&AR突释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非堆积*&AR突释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR突N
		REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
CASE 实现方式='政策生育'.AND.释放模式='正常模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&子女..FR2*实现比 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积*&子女..FR2*实现比 FOR X<=49
		REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
CASE 实现方式='可能生育'.AND.释放模式='意愿模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*晚释二胎比.&地带1*&子女..FR2*实现比 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*晚释二胎比.&地带1*&子女..FR2*实现比 FOR X<=49
CASE 实现方式='可能生育'.AND.释放模式='分释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积30*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积35*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积15*&子女..FR2*实现比 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积30*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积35*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积15*&子女..FR2*实现比 FOR X<=49
CASE 实现方式='可能生育'.AND.释放模式='缓释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&AR缓释模式(ND0-调整时机+1)*实现比  FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非堆积*&AR缓释模式(ND0-调整时机+1)*实现比  FOR ND0-调整时机+1<=AR缓N
CASE 实现方式='可能生育'.AND.释放模式='突释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&AR突释模式(ND0-调整时机+1)*实现比  FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非堆积*&AR突释模式(ND0-调整时机+1)*实现比  FOR ND0-调整时机+1<=AR突N
CASE 实现方式='可能生育'.AND.释放模式='正常模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*&子女..FR2*实现比 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积*&子女..FR2*实现比 FOR X<=49
CASE 实现方式='意愿生育'.AND.释放模式='意愿模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*想生二胎比.&地带1*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*想生二胎比.&地带1*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积*想生二胎比.&地带1*晚释二胎比.&地带1*&子女..FR2 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积*想生二胎比.&地带1*突释二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*想生二胎比.&地带1*缓释二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积*想生二胎比.&地带1*晚释二胎比.&地带1*&子女..FR2 FOR X<=49
CASE 实现方式='意愿生育'.AND.释放模式='分释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积30*想生二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积35*想生二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..单独释放B WITH &子女..单独释放B+&子女..单独堆积15*想生二胎比.&地带1*&子女..FR2 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积30*想生二胎比.&地带1*&AR缓释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积35*想生二胎比.&地带1*&AR突释模式(ND0-调整时机+1) FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非释放B+&子女..双非堆积15*想生二胎比.&地带1*&子女..FR2 FOR X<=49
CASE 实现方式='意愿生育'.AND.释放模式='缓释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*想生二胎比.&地带1*&AR缓释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR缓N
		REPL &子女..双非释放B WITH &子女..双非堆积*想生二胎比.&地带1*&AR缓释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR缓N
CASE 实现方式='意愿生育'.AND.释放模式='突释模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*想生二胎比.&地带1*&AR突释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR突N
		REPL &子女..双非释放B WITH &子女..双非堆积*想生二胎比.&地带1*&AR突释模式(ND0-调整时机+1)  FOR ND0-调整时机+1<=AR突N
CASE 实现方式='意愿生育'.AND.释放模式='正常模式'
		REPL &子女..单独释放B WITH &子女..单独堆积*想生二胎比.&地带1*&子女..FR2 FOR X<=49
		REPL &子女..双非释放B WITH &子女..双非堆积*想生二胎比.&地带1*&子女..FR2 FOR X<=49
ENDCASE
REPL &子女..超生双独B   WITH &子女..双独B-&子女..政策双独B,;
	 &子女..超生NNB   WITH &子女..NNB-&子女..政策NNB,;
	 &子女..超生NMDFB WITH &子女..NMDFB-&子女..政策NMDFB,;
	 &子女..超生NFDMB WITH &子女..NFDMB-&子女..政策NFDMB,;
	 &子女..超生释放B WITH &子女..单独释放B+&子女..双非释放B-&子女..政策释放B,;
	 &子女..超生生育  WITH &子女..超生双独B+&子女..超生NNB+&子女..超生NMDFB+&子女..超生NFDMB+&子女..超生释放B ALL
REPL &子女..政策释放B WITH &子女..单独释放B+&子女..双非释放B,&子女..政策生育 WITH &子女..政策双独B+&子女..政策NNB+&子女..政策NMDFB+&子女..政策NFDMB+&子女..政策释放B ALL 
SUM  &子女..单独释放B,&子女..双非释放B TO 单放B2,非放B2
单独B2=SNMDF2+SNFDM2+单放B2
SNN2=SNN2+非放B2
*&子女..单独堆积*想生二胎比.&地带：实际可能生育数
*&子女..单独堆积*1				：政策可能生育数
*这种意愿，不只是对堆积夫妇，也对其他所有夫妇
*这种意愿，不只有生第二个孩子的意愿，还有想生第三个孩子意愿等，如何估计？如何估超生计？,按回归法
*如何与超生S对接？
RETURN 

*******************************************
PROCEDURE 按后代政策对出生孩子分类
DO CASE   
CASE  政策='现行'
    &&独生子女间婚配所生子女按非独生子女生育（现行政策）。因为双独、单独都只管一代，其后代都按非独处理，所以都适用这一情况，即MN1=1和MN1=2)处理是一样的
	MS='全部现行(独后按非独)'	&&单独后也按非独
	NB2=SDD2+SNMDF2+SNFDM2+SNN2    &&各模式生育的第二个孩子数
	LOCA FOR X=0
	REPL DDB  WITH 0 , DDBM WITH 0*XBB/(1+XBB),DDBF WITH 0/(1+XBB)  &&独生子女间婚配所生子女数和享受独生子女间婚配同等政策的其他婚配模式所生子女，城乡无差异
	REPL D  WITH  SDD1+SNMDF1+SNFDM1+SNN1, DM  WITH  D*XBB/(1+XBB),DF  WITH  D/(1+XBB)  &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB),NF  WITH  NB2/(1+XBB) &&非双独婚配生的第二个孩子总计(零岁组）
CASE  政策='单独'
    &&独生子女间婚配所生子女按非独生子女生育（现行政策）。因为双独、单独都只管一代，其后代都按非独处理，所以都适用这一情况，即MN1=1和MN1=2)处理是一样的
	MS='全部现行(独后按非独)'	&&单独后也按非独
	NB2=SDD2+单独B2+SNN2    &&各模式生育的第二个孩子数
	LOCA FOR X=0
	REPL DDB  WITH 0 , DDBM WITH 0*XBB/(1+XBB),DDBF WITH 0/(1+XBB)  &&独生子女间婚配所生子女数和享受独生子女间婚配同等政策的其他婚配模式所生子女，城乡无差异
	REPL D  WITH  SDD1+SNMDF1+SNFDM1+SNN1, DM  WITH  D*XBB/(1+XBB),DF  WITH  D/(1+XBB)  &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB),NF  WITH  NB2/(1+XBB) &&非双独婚配生的第二个孩子总计(零岁组）
CASE 政策='单独后'	&&(双独后代、单独后代可生二个)
	MS='双独单独后2，其他现行'
	NB2=SNN2    &&非独生子女间婚配生育的第二个孩子数
	LOCA FOR X=0
	REPL DDB  WITH S双独B+SNMDF1+SNFDM1+单独B2+再生D, DDBM WITH (SDB+SNMDF1+SNFDM1+单独B2)*XBB/(1+XBB),DDBF WITH (SDB+SNMDF1+SNFDM1+单独B2)/(1+XBB)  &&独生子女间婚配所生子女数
	REPL D  WITH  SNN1, DM  WITH  SNN1*XBB/(1+XBB),DF  WITH  (SNN1)/(1+XBB)&&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, NM  WITH  (NB2)*XBB/(1+XBB),NF  WITH  (NB2)/(1+XBB) &&非独生子女间婚配生的第二个孩子总计(零岁组）
	*****************************************************调整单独第一个孩子的属性.2009.8.29新加程序段**********************************************
	*	全面的子女属性调整，还应该包括对后代属性的调整。在单独后政策下，单独夫妇的所有子女都应该加入DDB队列，包括新生的，也包括政策调整前生育的。*
	*	特别是堆积的单独夫妇，生育第二个孩子后，他们原来已经生育的第一个孩子，应该和新生的第二个孩子，一起并入DDB系列。做法是：堆积的单独夫妇原有
	*	的第一个孩子，按堆积夫妇的数量和第一个孩子的年龄分布，移入DDB队列。而新的，还没有生育第一个孩子的单独夫妇，则可按上面语句处理
	**************************************************************************************************************************************
	IF ND0=调整时机
		CALCULATE MIN(X) TO MIX FOR 单独堆积<>0
		SUM DM+DF TO SDM_F FOR X>0.AND.X<=MIX
		SUM NM+NF TO SNM_F FOR X>0.AND.X<=MIX
		SUM 单独堆积,已2单独 TO S单独堆积,S已2单独  FOR X>=15.AND.X<=49
		REPL DDBM WITH DDBM+S单独堆积*DM/SDM_F,DDBF WITH DDBF+S单独堆积*DF/SDM_F FOR X>0.AND.X<=MIX	&&MIX:堆积母亲的最小年龄，也是第一个孩子的最大年龄
		REPL DDBM WITH DDBM+S已2单独*NM/SNM_F,DDBF WITH DDBF+S已2单独*NF/SNM_F FOR X>0.AND.X<=MIX
		REPL DM WITH DM-S单独堆积*DM/SDM_F,DF WITH DF-S单独堆积*DF/SDM_F FOR X>0.AND.X<=MIX
		REPL NM WITH NM-S已2单独*NM/SNM_F,NF WITH NF-S已2单独*NF/SNM_F FOR X>0.AND.X<=MIX
	ENDIF
CASE  政策='普二'
	MS='普二'
	NB2=0    &&
	LOCA FOR X=0
	REPL DDB  WITH S双独B+SNMDF1+SNFDM1+单独B2+SNN1+SNN2+再生D, DDBM WITH DDB*XBB/(1+XBB),DDBF WITH DDB/(1+XBB)       
	REPL D  WITH  0, DM  WITH  0,DF  WITH  0 &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  0, NM  WITH  0,NF  WITH  0 &&非独生子女间婚配生的第二个孩子总计(零岁组）
CASE  政策='双独后'
	MS='双独后2，其他现行'
	NB2=单独B2+SNN2    &&农村非独生子女间婚配第一个孩子为女孩的各模式生育的第二个孩子数
	LOCA FOR X=0
	REPL DDB  WITH S双独B , DDBM WITH S双独B*XBB/(1+XBB),DDBF WITH S双独B/(1+XBB)  &&独生子女间婚配所生子女数和享受同等政策的其他婚配模式所生子女，城乡无差异
	REPL D  WITH  SNMDF1+SNFDM1+SNN1, DM  WITH  (SNMDF1+SNFDM1+SNN1)*XBB/(1+XBB),DF  WITH  (SNMDF1+SNFDM1+SNN1)/(1+XBB)       &&非独生子女间婚配生的第一个孩子总计(零岁组）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB), NF  WITH  NB2/(1+XBB)       &&非独生子女婚配生的第二个孩子总计(零岁组）
CASE  政策='双独后单独'
	MS='双独后单独2，单独后及其他现行'
	NB2=(SNFDM2+SNMDF2+SNN2 )          &&非独生女与独生男、非独生男与独生女、非独生子女间婚配生育的第二个孩子数
	LOCA FOR X=0
	*REPL DDB  WITH S双独B+NMDF1+NFDM1,DDBM WITH (DB+NMDF1+NFDM1)*XBB/(1+XBB),DDBF WITH (DB+NMDF1+NFDM1)/(1+XBB)
	REPL DDB  WITH S双独B,DDBM WITH S双独B*XBB/(1+XBB),DDBF WITH S双独B/(1+XBB)  &&独生子女间婚配所生子女数
	REPL D  WITH  SNMDF1+SNFDM1+SNN1, DM  WITH  D*XBB/(1+XBB),DF  WITH  D/(1+XBB)   &&单独婚配、非独生子女间婚配生的第一个孩子总计(零岁组，当时计为独生子女）
	REPL N  WITH  NB2, NM  WITH  NB2*XBB/(1+XBB),NF  WITH  NB2/(1+XBB)      &&非独生女与独生男、非独生男与独生女、非独生子女间婚配生育的第二个孩子数生的第二个孩子总计(零岁组）
ENDCASE     
RETURN 



*********************************
PROCEDURE 超生生育率和超生人数估计（超生幅度法）
超生TFR=可实现TFR0-地区政策TFR
SELECT A
DO CASE
CASE 超生TFR>0
	超生修正系数=1
	COPY TO ARRAY ARFND1 FIELDS 地区分龄.&FND1
	REPLACE 地区农村农业子女.超生生育 WITH 地区农村农业子女.HJF*超生TFR*地区农村农业子女.fr,地区城镇农业子女.超生生育 WITH 地区城镇农业子女.HJF*超生TFR*地区城镇农业子女.fr ALL
	REPLACE 地区分龄超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
	SUM 地区分龄超生.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区超生TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地区生育率
	DO WHILE ABS(超生TFR-地区超生TFR0)>0.01
		DO CASE
		CASE 超生TFR-地区超生TFR0>0.01
			超生修正系数=超生修正系数*1.2
		CASE 超生TFR-地区超生TFR0<0
			超生修正系数=超生修正系数*0.8
		ENDCASE
		REPLACE 地区农村农业子女.超生生育 WITH 地区农村农业子女.HJF*超生TFR*超生修正系数*地区农村农业子女.FR,地区城镇农业子女.超生生育 WITH 地区城镇农业子女.HJF*超生TFR*超生修正系数*地区城镇农业子女.FR ALL
		REPLACE 地区分龄超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
		SUM 地区分龄超生.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区超生TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地区生育率
	ENDDO
	SUM 地区农村农业子女.超生生育,地区城镇农业子女.超生生育 TO X超生S,C超生S
	LOCA FOR X=0
	REPL  地区农村农业子女.NM  WITH 地区农村农业子女.NM+X超生S*XBB/(1+XBB),地区农村农业子女.NF  WITH 地区农村农业子女.NF+X超生S/(1+XBB),地区农村农业子女.N  WITH 地区农村农业子女.NM+地区农村农业子女.NF 
	REPL  地区城镇农业子女.NM  WITH 地区城镇农业子女.NM+C超生S*XBB/(1+XBB),地区城镇农业子女.NF  WITH 地区城镇农业子女.NF+C超生S/(1+XBB),地区城镇农业子女.N  WITH 地区城镇农业子女.NM+地区城镇农业子女.NF 
	REPL  地区农业子女.NM WITH 地区农村农业子女.NM+地区城镇农业子女.NM,地区农业子女.NF WITH 地区农村农业子女.NF+地区城镇农业子女.NF,地区农业子女.N WITH 地区农村农业子女.N+地区城镇农业子女.N
CASE 超生TFR<0
	FOR CXNF=1 TO 4
		子女=IIF(CXNF=1,'地区农村非农子女',IIF(CXNF=2,'地区城镇非农子女',IIF(CXNF=3,'地区农村农业子女','地区城镇农业子女')))
		子女HJ='地区子女'
		REPLACE &子女..超生生育 WITH &子女..HJF*超生TFR*&子女..FR2 ALL
		SUM &子女..超生生育 TO 超生S
		LOCA FOR X=0
		REPL  &子女..NM  WITH &子女..NM+超生S*XBB/(1+XBB),&子女..NF  WITH &子女..NF+超生S/(1+XBB),&子女..N  WITH &子女..NM+&子女..NF 
	ENDFOR
	REPLACE 地区分龄超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育+地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
	SUM 地区分龄超生.&BND2 TO 超生S
	LOCATE FOR X=0
	DO WHILE 地区农村非农子女.NM<0.OR.地区农村非农子女.NF<0.OR.地区城镇非农子女.NM<0.OR.地区城镇非农子女.NF<0.OR.地区农村农业子女.NM<0.OR.地区农村农业子女.NF<0.OR.地区城镇农业子女.NM<0.OR.地区城镇农业子女.NF<0.OR.;
	   地区农村非农子女.DM<0.OR.地区农村非农子女.DF<0.OR.地区城镇非农子女.DM<0.OR.地区城镇非农子女.DF<0.OR.地区农村农业子女.DM<0.OR.地区农村农业子女.DF<0.OR.地区城镇农业子女.DM<0.OR.地区城镇农业子女.DF<0
		*************************
		* 非独生子女间消除负值	*
		*************************
		IF 地区农村非农子女.NM<0.OR.地区农村非农子女.NF<0.OR.地区城镇非农子女.NM<0.OR.地区城镇非农子女.NF<0.OR.地区农村农业子女.NM<0.OR.地区农村农业子女.NF<0.OR.地区城镇农业子女.NM<0.OR.地区城镇农业子女.NF<0
			*****农业非独生子女在农业非独生子女间消除负值,非农业非独生子女在非农业非独生子女间消除负值
			FOR CX=1 TO 2
				农村子女=IIF(CX=1,'地区农村非农子女','地区农村农业子女')
				城镇子女=IIF(CX=1,'地区城镇非农子女','地区城镇农业子女')
				FOR XB=1 TO 2
					非独子女='N'-IIF(XB=1,'M','F')
					DO CASE
					CASE &农村子女..&非独子女<0.AND.&城镇子女..&非独子女>0
						REPL  &城镇子女..&非独子女  WITH &城镇子女..&非独子女+ &农村子女..&非独子女,&农村子女..&非独子女  WITH 0 
					CASE &农村子女..&非独子女>0.AND.&城镇子女..&非独子女<0
						REPL  &农村子女..&非独子女  WITH &城镇子女..&非独子女+ &农村子女..&非独子女,&城镇子女..&非独子女  WITH 0 
					ENDCASE
				ENDFOR
			ENDFOR
			*****城镇非独生子女在城镇非独生子女间消除负值,农村非独生子女在农村非独生子女间消除负值
			FOR 农非=1 TO 2
				非农子女=IIF(农非=1,'地区农村非农子女','地区城镇非农子女')
				农业子女=IIF(农非=1,'地区农村农业子女','地区城镇农业子女')
				FOR XB=1 TO 2
					非独子女='N'-IIF(XB=1,'M','F')
					DO CASE
					CASE &农业子女..&非独子女<0.AND.&非农子女..&非独子女>0
						REPL  &非农子女..&非独子女  WITH &非农子女..&非独子女+ &农业子女..&非独子女,&农业子女..&非独子女  WITH 0 
					CASE &农业子女..&非独子女>0.AND.&非农子女..&非独子女<0
						REPL  &农业子女..&非独子女  WITH &非农子女..&非独子女+ &农业子女..&非独子女,&非农子女..&非独子女  WITH 0 
					ENDCASE
				ENDFOR
			ENDFOR
		ENDIF		
		*************************************
		* 非独生子女与独生子女间消除负值	*
		*************************************
		IF 地区农村非农子女.NM<0.OR.地区农村非农子女.NF<0.OR.地区城镇非农子女.NM<0.OR.地区城镇非农子女.NF<0.OR.地区农村农业子女.NM<0.OR.地区农村农业子女.NF<0.OR.地区城镇农业子女.NM<0.OR.地区城镇农业子女.NF<0
			*****农业非独生子女在农业非独生子女间消除负值,非农业非独生子女在非农业非独生子女间消除负值
			FOR CX=1 TO 4
				子女=IIF(CX=1,'地区农村非农子女',IIF(CX=2,'地区农村农业子女',IIF(CX=3,'地区城镇非农子女','地区城镇农业子女')))
				IF &子女..NM<0
					REPL  &子女..DM  WITH &子女..DM+&子女..NM,&子女..NM  WITH 0 
				ENDIF
				IF &子女..NF<0
					REPL  &子女..DF  WITH &子女..DF+&子女..NF,&子女..NF  WITH 0
				ENDIF
			ENDFOR
		ENDIF
		*********************************
		* 独生子女与独生子女间消除负值	*
		*********************************
		IF 地区农村非农子女.DM<0.OR.地区农村非农子女.DF<0.OR.地区城镇非农子女.DM<0.OR.地区城镇非农子女.DF<0.OR.地区农村农业子女.DM<0.OR.地区农村农业子女.DF<0.OR.地区城镇农业子女.DM<0.OR.地区城镇农业子女.DF<0
			*****农业非独生子女在农业非独生子女间消除负值,非农业非独生子女在非农业非独生子女间消除负值
			FOR CX=1 TO 2
				农村子女=IIF(CX=1,'地区农村非农子女','地区农村农业子女')
				城镇子女=IIF(CX=1,'地区城镇非农子女','地区城镇农业子女')
				FOR XB=1 TO 2
					独生子女='D'-IIF(XB=1,'M','F')
					DO CASE
					CASE &农村子女..&独生子女<0.AND.&城镇子女..&独生子女>0
						REPL  &城镇子女..&独生子女  WITH &城镇子女..&独生子女+ &农村子女..&独生子女,&农村子女..&独生子女  WITH 0 
					CASE &农村子女..&独生子女>0.AND.&城镇子女..&独生子女<0
						REPL  &农村子女..&独生子女  WITH &城镇子女..&独生子女+ &农村子女..&独生子女,&城镇子女..&独生子女  WITH 0 
					ENDCASE
				ENDFOR
			ENDFOR
			*****城镇非独生子女在城镇非独生子女间消除负值,农村非独生子女在农村非独生子女间消除负值
			FOR 农非=1 TO 2
				非农子女=IIF(农非=1,'地区农村非农子女','地区城镇非农子女')
				农业子女=IIF(农非=1,'地区农村农业子女','地区城镇农业子女')
				FOR XB=1 TO 2
					独生子女='D'-IIF(XB=1,'M','F')
					DO CASE
					CASE &农业子女..&独生子女<0.AND.&非农子女..&独生子女>0
						REPL  &非农子女..&独生子女  WITH &非农子女..&独生子女+ &农业子女..&独生子女,&农业子女..&独生子女  WITH 0 
					CASE &农业子女..&独生子女>0.AND.&非农子女..&独生子女<0
						REPL  &农业子女..&独生子女  WITH &非农子女..&独生子女+ &农业子女..&独生子女,&非农子女..&独生子女  WITH 0 
					ENDCASE
				ENDFOR
			ENDFOR
		ENDIF
*?'非独生子女',地区农村非农子女.NM,地区农村非农子女.NF,地区城镇非农子女.NM,地区城镇非农子女.NF,地区农村农业子女.NM,地区农村农业子女.NF,地区城镇农业子女.NM,地区城镇农业子女.NF
*?'独生子女',地区农村非农子女.DM,地区农村非农子女.DF,地区城镇非农子女.DM,地区城镇非农子女.DF,地区农村农业子女.DM,地区农村农业子女.DF,地区城镇农业子女.DM,地区城镇农业子女.DF
		
	ENDDO
ENDCASE
SUM 地区分龄超生.&BND2,地区农村农业子女.超生生育+地区农村非农子女.超生生育,地区城镇农业子女.超生生育+地区城镇非农子女.超生生育 TO 超生S,X超生S,C超生S
RETURN 

****************************************
PROCEDURE 独生子女和非独生子女人数调整（孩次递进法）
SELECT A
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	*************************************
	*	调整独生子女人数和非独生子女人数	*
	*************************************
	REPLACE 独转非 WITH 0 ALL
	LOCATE FOR X=0
	SB1=0
	NB2=N
	COPY fiel x,fr1,fr2 TO ARRA ARFR FOR X>=15.AND.X<=49
	CALCULATE MAX(FR1),MAX(FR2) TO MAFR1,MAFR2
	LOCATE FOR FR1=MAFR1
	MAFR1X=X
	LOCATE FOR FR2=MAFR2
	MAFR2X=X
	RELEASE ARFR1
	COPY FIEL FR1 TO ARRA ARFR1 FOR X<=MAFR2X.AND.FR1<>0
	COUNT TO SX1  	 FOR X<=MAFR2X.AND.FR1<>0
	SUM FR1 TO SFR1  FOR X<=MAFR2X.AND.FR1<>0
	FOR X2=1 TO 35
		B2X=ARFR(X2,1)		&&生育第二个孩子年龄
		B2S=NB2*ARFR(X2,3)	&&B2X生育第二个孩子数
		SB2=0
		IF B2S<>0
			SB1=0
			*****累计生育第一个孩子的比例
			DO CASE
			CASE B2X>=25
				FOR X1=1 TO X2-1	&&X1：B2X生育第二个孩子的妇女中生育第一个孩子的年龄
					SB1=SB1+ARFR(X1,2)
				ENDFOR
				FOR X1=1 TO X2-1	&&MAFRC
					LOCATE FOR X=ARFR(X2,1)-ARFR(X1,1)	&&母亲生第二孩子时第一个孩子的年龄
					REPLACE  独转非 WITH 独转非+B2S*ARFR(X1,2)/SB1	&&x1岁生育第一个孩子数，这个孩子的年龄是X2-X1
					SB2=SB2+B2S*ARFR(X1,2)/SB1
				ENDFOR
			CASE  B2X<25
				FOR X1=1 TO X2-1	&&X1：B2X生育第二个孩子的妇女中生育第一个孩子的年龄
					SB1=SB1+ARFR1(X1)
				ENDFOR
				FOR X1=1 TO X2-1	&&MAFRCSX1	&&SX1
					LOCATE FOR X=ARFR(X2,1)-ARFR(X1,1)	&&母亲生第二孩子时第一个孩子的年龄
					REPLACE  独转非 WITH 独转非+B2S*ARFR1(X1)/SB1	&&x1岁生育第一个孩子数，这个孩子的年龄是X2-X1
					SB2=SB2+B2S*ARFR1(X1)/SFR1
				ENDFOR
			ENDCASE 
*			?ARFR(X2,1),X2,B2S,SB2,B2S-SB2
		ENDIF 
	ENDFOR
	SUM 独转非 TO S独转非
	REPL NF  WITH   NF+独转非*1/(1+XBB),NM  WITH   NM+独转非*XBB/(1+XBB)  FOR X>0    &&因生第二个孩子，第一个孩子变为非独生子
	REPL DF  WITH   DF-独转非*1/(1+XBB),DM  WITH   DM-独转非*XBB/(1+XBB)  FOR X>0 
	COUNT TO S0 FOR NF<0.OR.NM<0.OR.DM<0.OR.DF<0
	SUM df+dm,Nf+Nm TO SD1,SN1  FOR X>0
	DI=1
	DO WHILE S0>0
		RELEASE ARS0
		COPY TO ARRAY ARS0 FIELDS DM,DF,NM,NF
		IF S0=1
			REPLACE DM WITH (ARS0(RECNO()-1,1)+ARS0(RECNO(),1)+ARS0(RECNO()+1,1))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),1)<0
			REPLACE DF WITH (ARS0(RECNO()-1,2)+ARS0(RECNO(),2)+ARS0(RECNO()+1,2))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),2)<0
			REPLACE NM WITH (ARS0(RECNO()-1,3)+ARS0(RECNO(),3)+ARS0(RECNO()+1,3))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),3)<0
			REPLACE NF WITH (ARS0(RECNO()-1,4)+ARS0(RECNO(),4)+ARS0(RECNO()+1,4))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),4)<0
		ELSE			
			REPLACE DM WITH (ARS0(RECNO()-2,1)+ARS0(RECNO()-1,1)+ARS0(RECNO(),1)+ARS0(RECNO()+1,1)+ARS0(RECNO()+2,1))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),1)<0
			REPLACE DF WITH (ARS0(RECNO()-2,2)+ARS0(RECNO()-1,2)+ARS0(RECNO(),2)+ARS0(RECNO()+1,2)+ARS0(RECNO()+2,2))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),2)<0
			REPLACE NM WITH (ARS0(RECNO()-2,3)+ARS0(RECNO()-1,3)+ARS0(RECNO(),3)+ARS0(RECNO()+1,3)+ARS0(RECNO()+2,3))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),3)<0
			REPLACE NF WITH (ARS0(RECNO()-2,4)+ARS0(RECNO()-1,4)+ARS0(RECNO(),4)+ARS0(RECNO()+1,4)+ARS0(RECNO()+2,4))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),4)<0
		ENDIF
		COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
		DI=DI+1
		IF DI>10
			FOR MF=1 TO 4
				FIE1=IIF(MF=1,'DM',IIF(MF=2,'DF',IIF(MF=3,'NM','NF')))
				SUM &FIE1 TO SMI FOR &FIE1<0.AND.X>1
				SUM &FIE1 TO SMA FOR &FIE1>0.AND.X>1
				REPLACE &FIE1 WITH &FIE1+SMI*&FIE1/SMA FOR &FIE1>0.AND.X>1
				REPLACE &FIE1 WITH 0 FOR &FIE1<0.AND.X>1
			ENDFOR
			COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
			IF S0>0
				REPLACE NM WITH NM+DM,DM WITH 0 FOR DM<0
				REPLACE NF WITH NF+DF,DF WITH 0 FOR DF<0
				REPLACE DM WITH DM+NM,NM WITH 0 FOR NM<0
				REPLACE DF WITH DF+NF,NF WITH 0 FOR NF<0
			ENDIF
		ENDIF
*IF S0>0
*	?2,'独生子女和非独生子女人数调整（孩次递进法）',S0
*ENDIF
	ENDDO
	COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
IF S0>0
	LIST OFF FIELDS X,DM,DF,NF,NM FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
1
	BROWSE FIELDS X,DM,DF,NF,NM
HB	
ENDIF
	
	SUM df+dm,Nf+Nm TO SD2,SN2  FOR X>0 
	REPLACE DM WITH IIF(SD2<>0,SD1*DM/SD2,0),DF WITH IIF(SD2<>0,SD1*DF/SD2,0),NM WITH IIF(SN2<>0,SN1*NM/SN2,0),NF WITH IIF(SN2<>0,SN1*NF/SN2,0)  FOR X>0
	IF S0>0
		?S0
*		MK
	ENDIF
	REPL D WITH DM+DF ALL
	SUM D,DM,DF,N,NM,NF,DDB,DDBM,DDBF TO  DS,DMS,DFS,NS,NMS,NFS,DDBS,DDBMS,DDBFS 
	SUM D,DM,DF,N,NM,NF,DDB,DDBM,DDBF TO  DS30,DMS30,DFS30,NS30,NMS30,NFS30,DDBS30,DDBMS30,DDBFS30 FOR X<=30
	SUM 双独,NMDF,NFDM,NN TO  DDS,NMDFS,NFDMS,NNS  FOR X>=15.AND.X<=49    &&只记录育龄期人数,户籍人口为(20岁_49岁)
	SUM 单独堆积,已2单独,双非堆积,已2双非 TO 堆积单独,单独已2,堆积双非,双非已2  FOR X>=20.AND.X<=49    &&只记录育龄期人数
	DO CASE
	 CASE '农业子女'$ALIAS()	&&SELE 61 :地区农村农业子女；62 :地区农村非农子女；63 :地区城镇农业子女；64 :地区城镇非农子女；65 ;地区农业子女；66 :地区非农子女
	 	REPLACE SWDM WITH DM*NYQM(RECN()),SWDF WITH DF*NYQF(RECN()),SWD WITH SWDM+SWDF ALL
		SUM DM*NYQM(RECN()),NM*NYQM(RECN()),DDBM*NYQM(RECN()) TO SWDM,SWNM,SWDDM FOR X>=15.AND.X<60
		SUM DF*NYQF(RECN()),NF*NYQF(RECN()),DDBF*NYQF(RECN()) TO SWDF,SWNF,SWDDF FOR X>=15.AND.X<60
	 CASE '非农子女'$ALIAS()
	 	REPLACE SWDM WITH DM*FNQM(RECN()),SWDF WITH DF*FNQF(RECN()),SWD WITH SWDM+SWDF ALL
		SUM DM*FNQM(RECN()),NM*FNQM(RECN()),DDBM*FNQM(RECN()) TO SWDM,SWNM,SWDDM FOR X>=15.AND.X<60
		SUM DF*FNQF(RECN()),NF*FNQF(RECN()),DDBF*FNQF(RECN()) TO SWDF,SWNF,SWDDF FOR X>=15.AND.X<60
	ENDCASE
	REPL D WITH DM+DF,N WITH NM+NF ALL
	
	
*SUM DM+DF+NM+NF+DDBM+DDBF TO  SDN2
*?'独生子女和非独生子女调整结果：',SDN1,SDN2,SDN1-SDN2

	SELE IIF(CX=1,41,IIF(CX=2,42,IIF(CX=3,43,44)))	&&记录各婚配模式夫妇人数,及生育的孩子数
	LOCA FOR 年份=ND0
	*DO 记录各婚配模式夫妇人数
	REPL 非农政策1 WITH ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4))),;
	农业政策1 WITH ALLT(农业ZC1)-IIF(农业时机1=0,'',ALLT(STR(农业时机1,4))),;
	非农政策2 WITH ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))),;
	农业政策2 WITH ALLT(农业ZC2)-IIF(农业时机2=0,'',ALLT(STR(农业时机2,4))),;
	非农政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(非农ZC3)-ALLT(STR(非农时机3,4)),''),;
	农业政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(农业ZC3)-ALLT(STR(农业时机3,4)),''),;
	女单生育数 WITH SNMDFB, 男单生育数 WITH SNMDFB,单独生育数  WITH 女单生育数+男单生育数,双非生育数 WITH SNNB,TFR WITH SFRD,双独生育数  WITH S双独B,;
	非农双独 WITH DDFR,农业双独 WITH DDFR,非农女单 WITH N1D2C,农业女单 WITH N1D2N,;
	非农男单 WITH N2D1C,农业男单 WITH N2D1N,非农双非 WITH NNFC,农业双非 WITH NNFN,;
	D WITH DS,N WITH NS,DM WITH DMS,DF WITH DFS,NM WITH NMS,NF WITH NFS,可二后 WITH DDBS,DDBM WITH DDBMS,DDBF WITH DDBFS,;
	独生子女30  WITH DS30, 独生子30  WITH DMS30,  独生女30  WITH DFS30,  非独子女30  WITH NS30,  非独子30  WITH NMS30,;
	非独女30  WITH NFS30, 可二后30  WITH DDBS30, 可二后30M  WITH DDBMS30, 可二后30F  WITH DDBFS30,;
	双独夫妇数 WITH DDS,女单夫妇数 WITH NMDFS,男单夫妇数 WITH NFDMS,;
	单独堆积 WITH 堆积单独,已2单独 WITH 单独已2,单独夫妇数 WITH 女单夫妇数+男单夫妇数+单独堆积+已2单独,;
	双非堆积 WITH 堆积双非,已2双非 WITH 双非已2,双非夫妇数 WITH NNS+堆积双非+双非已2,合计夫妇数 WITH 双独夫妇数+单独夫妇数+双非夫妇数,;
	独生子SW WITH SWDM,独生女SW WITH SWDF,非独子SW WITH SWNM,非独女SW WITH SWNF,可二后SWM WITH SWDDM,可二后SWF WITH SWDDF,;
	独生子女SW WITH 独生子SW+独生女SW,非独子女SW WITH 非独子SW+非独女SW,可二后SW WITH 可二后SWM+可二后SWF	&&独生子女SW:独生子女死亡户
	*双独概率F WITH IIF(CX=1,乡NY双独GLF,IIF(CX=2,乡FN双独GLF,IIF(CX=3,城NY双独GLF,城FN双独GLF))),;
	*双独概率M WITH IIF(CX=1,乡NY双独GLM,IIF(CX=2,乡FN双独GLM,IIF(CX=3,城NY双独GLM,城FN双独GLM))),;
	*男单概率M  WITH IIF(CX=1,乡NY男单GLM,IIF(CX=2,乡FN男单GLM,IIF(CX=3,城NY男单GLM,城FN男单GLM))),;
	*男单概率F  WITH IIF(CX=1,乡NY男单GLF,IIF(CX=2,乡FN男单GLF,IIF(CX=3,城NY男单GLF,城FN男单GLF))),;
	*女单概率M  WITH IIF(CX=1,乡NY女单GLM,IIF(CX=2,乡FN女单GLM,IIF(CX=3,城NY女单GLM,城FN女单GLM))),;
	*女单概率F  WITH IIF(CX=1,乡NY女单GLF,IIF(CX=2,乡FN女单GLF,IIF(CX=3,城NY女单GLF,城FN女单GLF)))
	*****记录分孩次的生育数

	
ENDFOR
RETURN 		


*******************************************
PROCEDURE 	独生子女和非独生子女人数调整（生二孩高峰年龄法）
SELECT A
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	*************************************
	*	调整独生子女人数和非独生子女人数	*
	*************************************
	LOCATE FOR X=0
	NB2=N
	CALC MAX(FR2) TO MFR2
	LOCA FOR FR2=MFR2
	MX2=X
	SUM FR1 TO S30 FOR X<=MX2
	REPL 独转非 WITH NB2*(FR1/S30) FOR X<=MX2		&&生了第二个孩子后,需要独转非的第一个孩子数与当年生的第二个孩子数相等，只是年龄不同。母亲生育这些独转非孩子的年龄分布,与生育所有第一个孩子的年龄分布相同
	RELE ARNB2
	COPY TO ARRA ARNB2 FIEL 独转非 FOR X<=MX2		&&假定母亲在生第二个孩子的高峰年龄之前,这些母亲都生完了第一个孩子
	REPL 独转非 WITH 0 ALL
	REPL 独转非 WITH ARNB2(MX2+1-X) FOR (MX2+1-X)>0.AND.(MX2+1-X)<=30  &&将按母亲年龄分布独转非孩子，转换成按孩子年龄分布的独转非孩子
	REPL NF  WITH   NF+独转非*1/(1+XBB)    FOR X>0    &&因生第二个孩子，第一个孩子变为非独生女
	REPL NM  WITH   NM+独转非*XBB/(1+XBB)  FOR X>0    &&因生第二个孩子，第一个孩子变为非独生子
	REPL N WITH NM+NF ALL
	REPL DF  WITH   DF-独转非*1/(1+XBB)    FOR X>0   
	REPL DM  WITH   DM-独转非*XBB/(1+XBB)  FOR X>0 
	******检验并修正调整独生子女人数和非独生子女人数时造成的负数
	COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0

		COUNT TO S0 FOR NF<0.OR.NM<0.OR.DM<0.OR.DF<0
		IF S0>0
			BROWSE FIELDS X,NF,NM,DM,DF
			JF
		ENDIF
	SUM df+dm,Nf+Nm TO SD1,SN1  FOR X>0
	DI=1
	DO WHILE S0>0
		RELEASE ARS0
		COPY TO ARRAY ARS0 FIELDS DM,DF,NM,NF
		IF S0=1
			REPLACE DM WITH (ARS0(RECNO()-1,1)+ARS0(RECNO(),1)+ARS0(RECNO()+1,1))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),1)<0
			REPLACE DF WITH (ARS0(RECNO()-1,2)+ARS0(RECNO(),2)+ARS0(RECNO()+1,2))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),2)<0
			REPLACE NM WITH (ARS0(RECNO()-1,3)+ARS0(RECNO(),3)+ARS0(RECNO()+1,3))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),3)<0
			REPLACE NF WITH (ARS0(RECNO()-1,4)+ARS0(RECNO(),4)+ARS0(RECNO()+1,4))/3 FOR RECNO()>1.AND.RECN()+1<RECCOUNT().AND.ARS0(RECNO(),4)<0
		ELSE			
			REPLACE DM WITH (ARS0(RECNO()-2,1)+ARS0(RECNO()-1,1)+ARS0(RECNO(),1)+ARS0(RECNO()+1,1)+ARS0(RECNO()+2,1))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),1)<0
			REPLACE DF WITH (ARS0(RECNO()-2,2)+ARS0(RECNO()-1,2)+ARS0(RECNO(),2)+ARS0(RECNO()+1,2)+ARS0(RECNO()+2,2))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),2)<0
			REPLACE NM WITH (ARS0(RECNO()-2,3)+ARS0(RECNO()-1,3)+ARS0(RECNO(),3)+ARS0(RECNO()+1,3)+ARS0(RECNO()+2,3))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),3)<0
			REPLACE NF WITH (ARS0(RECNO()-2,4)+ARS0(RECNO()-1,4)+ARS0(RECNO(),4)+ARS0(RECNO()+1,4)+ARS0(RECNO()+2,4))/5 FOR RECNO()>2.AND.RECN()+2<RECCOUNT().AND.ARS0(RECNO(),4)<0
		ENDIF
		COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
		DI=DI+1
		IF DI>10
			FOR MF=1 TO 4
				FIE1=IIF(MF=1,'DM',IIF(MF=2,'DF',IIF(MF=3,'NM','NF')))
				SUM &FIE1 TO SMI FOR &FIE1<0.AND.X>1
				SUM &FIE1 TO SMA FOR &FIE1>0.AND.X>1
				REPLACE &FIE1 WITH &FIE1+SMI*&FIE1/SMA FOR &FIE1>0.AND.X>1
				REPLACE &FIE1 WITH 0 FOR &FIE1<0.AND.X>1
			ENDFOR
			COUNT TO S0 FOR DM<0.OR.DF<0
			IF S0>0
				REPLACE NM WITH NM+DM,DM WITH 0 FOR DM<0
				REPLACE NF WITH NF+DF,DF WITH 0 FOR DF<0
			ENDIF
		ENDIF
	ENDDO
COUNT TO S0 FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
IF S0>0
	LIST OFF FIELDS X,DM,DF,NF,NM FOR DM<0.OR.DF<0.OR.NF<0.OR.NM<0
*1
*	BROWSE FIELDS X,DM,DF,NF,NM
*HB	
ENDIF
	
	SUM df+dm,Nf+Nm TO SD2,SN2  FOR X>0 
	REPLACE DM WITH IIF(SD2<>0,SD1*DM/SD2,0),DF WITH IIF(SD2<>0,SD1*DF/SD2,0),NM WITH IIF(SN2<>0,SN1*NM/SN2,0),NF WITH IIF(SN2<>0,SN1*NF/SN2,0)  FOR X>0
	IF S0>0
		?S0
*		MK
	ENDIF
	REPL D WITH DM+DF ALL
	SUM D,DM,DF,N,NM,NF,DDB,DDBM,DDBF TO  DS,DMS,DFS,NS,NMS,NFS,DDBS,DDBMS,DDBFS 
	SUM D,DM,DF,N,NM,NF,DDB,DDBM,DDBF TO  DS30,DMS30,DFS30,NS30,NMS30,NFS30,DDBS30,DDBMS30,DDBFS30 FOR X<=30
	SUM 双独,NMDF,NFDM,NN TO  DDS,NMDFS,NFDMS,NNS  FOR X>=15.AND.X<=49    &&只记录育龄期人数,户籍人口为(20岁_49岁)
	SUM 单独堆积,已2单独,双非堆积,已2双非 TO 堆积单独,单独已2,堆积双非,双非已2  FOR X>=20.AND.X<=49    &&只记录育龄期人数
	DO CASE
	 CASE '农业子女'$ALIAS()	&&SELE 61 :地区农村农业子女；62 :地区农村非农子女；63 :地区城镇农业子女；64 :地区城镇非农子女；65 ;地区农业子女；66 :地区非农子女
	 	REPLACE SWDM WITH DM*NYQM(RECN()),SWDF WITH DF*NYQF(RECN()),SWD WITH SWDM+SWDF ALL
		SUM DM*NYQM(RECN()),NM*NYQM(RECN()),DDBM*NYQM(RECN()) TO SWDM,SWNM,SWDDM FOR X>=15.AND.X<60
		SUM DF*NYQF(RECN()),NF*NYQF(RECN()),DDBF*NYQF(RECN()) TO SWDF,SWNF,SWDDF FOR X>=15.AND.X<60
	 CASE '非农子女'$ALIAS()
	 	REPLACE SWDM WITH DM*FNQM(RECN()),SWDF WITH DF*FNQF(RECN()),SWD WITH SWDM+SWDF ALL
		SUM DM*FNQM(RECN()),NM*FNQM(RECN()),DDBM*FNQM(RECN()) TO SWDM,SWNM,SWDDM FOR X>=15.AND.X<60
		SUM DF*FNQF(RECN()),NF*FNQF(RECN()),DDBF*FNQF(RECN()) TO SWDF,SWNF,SWDDF FOR X>=15.AND.X<60
	ENDCASE
	
	
*SUM DM+DF+NM+NF+DDBM+DDBF TO  SDN2
*?'独生子女和非独生子女调整结果：',SDN1,SDN2,SDN1-SDN2

	SELE IIF(CX=1,41,IIF(CX=2,42,IIF(CX=3,43,44)))	&&记录各婚配模式夫妇人数,及生育的孩子数
	LOCA FOR 年份=ND0
	*DO 记录各婚配模式夫妇人数
	REPL 非农政策1 WITH ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4))),;
	农业政策1 WITH ALLT(农业ZC1)-IIF(农业时机1=0,'',ALLT(STR(农业时机1,4))),;
	非农政策2 WITH ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))),;
	农业政策2 WITH ALLT(农业ZC2)-IIF(农业时机2=0,'',ALLT(STR(农业时机2,4))),;
	非农政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(非农ZC3)-ALLT(STR(非农时机3,4)),''),;
	农业政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(农业ZC3)-ALLT(STR(农业时机3,4)),''),;
	女单生育数 WITH SNMDFB, 男单生育数 WITH SNMDFB,单独生育数  WITH 女单生育数+男单生育数,双非生育数 WITH SNNB,TFR WITH SFRD,双独生育数  WITH S双独B,;
	非农双独 WITH DDFR,农业双独 WITH DDFR,非农女单 WITH N1D2C,农业女单 WITH N1D2N,;
	非农男单 WITH N2D1C,农业男单 WITH N2D1N,非农双非 WITH NNFC,农业双非 WITH NNFN,;
	D WITH DS,N WITH NS,DM WITH DMS,DF WITH DFS,NM WITH NMS,NF WITH NFS,可二后 WITH DDBS,DDBM WITH DDBMS,DDBF WITH DDBFS,;
	独生子女30  WITH DS30, 独生子30  WITH DMS30,  独生女30  WITH DFS30,  非独子女30  WITH NS30,  非独子30  WITH NMS30,;
	非独女30  WITH NFS30, 可二后30  WITH DDB30, 可二后30M  WITH DDBMS30, 可二后30F  WITH DDBFS30,;
	双独夫妇数 WITH DDS,女单夫妇数 WITH NMDFS,男单夫妇数 WITH NFDMS,;
	单独堆积 WITH 堆积单独,已2单独 WITH 单独已2,单独夫妇数 WITH 女单夫妇数+男单夫妇数+单独堆积+已2单独,;
	双非堆积 WITH 堆积双非,已2双非 WITH 双非已2,双非夫妇数 WITH NNS+堆积双非+双非已2,合计夫妇数 WITH 双独夫妇数+单独夫妇数+双非夫妇数,;
	独生子SW WITH SWDM,独生女SW WITH SWDF,非独子SW WITH SWNM,非独女SW WITH SWNF,可二后SWM WITH SWDDM,可二后SWF WITH SWDDF,;
	独生子女SW WITH 独生子SW+独生女SW,非独子女SW WITH 非独子SW+非独女SW,可二后SW WITH 可二后SWM+可二后SWF	&&独生子女SW:独生子女死亡户
	*双独概率F WITH IIF(CX=1,乡NY双独GLF,IIF(CX=2,乡FN双独GLF,IIF(CX=3,城NY双独GLF,城FN双独GLF))),;
	*双独概率M WITH IIF(CX=1,乡NY双独GLM,IIF(CX=2,乡FN双独GLM,IIF(CX=3,城NY双独GLM,城FN双独GLM))),;
	*男单概率M  WITH IIF(CX=1,乡NY男单GLM,IIF(CX=2,乡FN男单GLM,IIF(CX=3,城NY男单GLM,城FN男单GLM))),;
	*男单概率F  WITH IIF(CX=1,乡NY男单GLF,IIF(CX=2,乡FN男单GLF,IIF(CX=3,城NY男单GLF,城FN男单GLF))),;
	*女单概率M  WITH IIF(CX=1,乡NY女单GLM,IIF(CX=2,乡FN女单GLM,IIF(CX=3,城NY女单GLM,城FN女单GLM))),;
	*女单概率F  WITH IIF(CX=1,乡NY女单GLF,IIF(CX=2,乡FN女单GLF,IIF(CX=3,城NY女单GLF,城FN女单GLF)))
	*****记录分孩次的生育数
ENDFOR
RETURN
****************************************

****************************************
PROCEDURE 分龄一孩父母和特扶父母估计
*****一孩父母估计
FOR CX=1 TO 4
	ALI1=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区城镇农业子女',IIF(CX=3,'地区农村非农子女','地区城镇非农子女')))
	ALI2=IIF(CX=1,'地区农村农业父母',IIF(CX=2,'地区城镇农业父母',IIF(CX=3,'地区农村非农父母','地区城镇非农父母')))
	新生一孩=IIF(CX=1,农村农业一孩,  IIF(CX=2,城镇农业一孩,	     IIF(CX=3,     农村非农一孩,      城镇非农一孩)))
	*IF ND0=2010	&&ND0=ND1
	*	SELECT A
	*	RELEASE ARD
	*	CALCULATE MAX(X) TO MANL FOR &ALI1..D<>0
	*	COPY TO ARRAY ARD FIELDS &ALI1..D  FOR X<=MANL
	*	COPY TO ARRAY ARFR1 FIELDS &ALI1..FR1 FOR X>=15.AND.X<=49
	*	*****推算一孩母亲
	*	FOR NL=1 TO MANL+1
	*		IF ARD(NL)=0
	*			LOOP
	*		ENDIF 
	*		DS=ARD(NL)
	*		母亲MINL=NL+14
	*		母亲MANL=NL+48
	*		REPLACE  &ALI2..&FND2 WITH &ALI2..&FND2+DS*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
	*	ENDFOR
	*	*****推算一孩父亲
	*	COPY TO ARRAY ARFND2 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
	*	FOR NL=15 TO 64
	*		妻子RS=ARFND2(NL-14)
	*		妻子NL='妻子'-allt(STR(NL,3))
	*		REPLACE &ALI2..&MND2 WITH &ALI2..&MND2+妻子RS*需夫模式.&妻子NL ALL
	*	ENDFOR
	*ELSE
		*********************
		*	新增一孩父母	*
		*********************
		*****推算新增一孩母亲
		SELECT A
*		REPLACE  &ALI2..&FND2 WITH &ALI2..&FND2+新生一孩*&ALI1..FR1 FOR X>=15.AND.X<=49	&&新生一孩
		REPLACE  &ALI2..&FND2 WITH 新生一孩*&ALI1..FR1 FOR X>=15.AND.X<=49	&&新生一孩
		*****推算新增一孩父亲
		FOR NL=15 TO 49
			LOCATE FOR X=NL
			妻子RS=新生一孩*&ALI1..FR1
			妻子NL='妻子'-allt(STR(NL,3))
			REPLACE &ALI2..&MND2 WITH &ALI2..&MND2+妻子RS*需夫模式.&妻子NL ALL
		ENDFOR
		*COPY TO ARRAY AR一孩父母 FIELDS &ALI2..&MND2,&ALI2..&FND2  
		*****减去独转非母亲
		RELEASE AR独转非
		CALCULATE MAX(X) TO MANL FOR &ALI1..独转非<>0
		COPY TO ARRAY AR独转非 FIELDS &ALI1..独转非  FOR X<=MANL
		COPY TO ARRAY ARFR1  FIELDS &ALI1..FR1 FOR X>=15.AND.X<=49
		COPY TO ARRAY ARFND1 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
		FOR NL=1 TO MANL+1
			IF AR独转非(NL)=0
				LOOP
			ENDIF 
			D独转非=AR独转非(NL)
			母亲MINL=NL+14
			母亲MANL=NL+48
			REPLACE &ALI2..&FND2  WITH &ALI2..&FND2-D独转非*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
		ENDFOR
		COPY TO ARRAY ARFND2 FIELDS &ALI2..&FND2 FOR X>=15.AND.X<=110
		*****减去独转非父亲
		FOR NL=15 TO 64
			独转非母亲RS=ARFND1(nl-14)-ARFND2(nl-14)
			妻子NL='妻子'-allt(STR(NL,3))
			REPLACE &ALI2..&MND2 WITH &ALI2..&MND2-独转非母亲RS*需夫模式.&妻子NL ALL
		ENDFOR
		*****一孩父母年龄移算,	上一ND***************
		COPY TO ARRAY ARND1 FIEL &ALI2..&MND1,&ALI2..&FND1
 		REPL &ALI2..&MND2 WITH &ALI2..&MND2+ARND1(RECN()-1,1)*(1-IIF(CX<=2,NYQM(RECN()-1),FNQM(RECN()-1))),;
	 		 &ALI2..&FND2 WITH &ALI2..&FND2+ARND1(RECN()-1,2)*(1-IIF(CX<=2,NYQF(RECN()-1),FNQF(RECN()-1))) FOR RECN()>1
	*ENDIF
	DO CASE 
	CASE CX=1
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村农业独子父,农村农业独子母 FOR x>=60
	CASE CX=2
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇农业独子父,城镇农业独子母 FOR x>=60
	CASE CX=3
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 农村非农独子父,农村非农独子母 FOR x>=60
	CASE CX=4
		SUM &ALI2..&MND2,&ALI2..&FND2 TO 城镇非农独子父,城镇非农独子母 FOR x>=60
	ENDCASE
	SUM &ALI2..&MND2,&ALI2..&FND2 TO S独子父,S独子母
*	IF S独子父<0.or.S独子母<0
*		?'S独子父',S独子父,'S独子母',S独子母
*	ENDIF 
ENDFOR
*****特扶父母
FOR CX=1 TO 4
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区城镇农业子女',IIF(CX=3,'地区农村非农子女','地区城镇非农子女')))
	特扶=IIF(CX=1,'地区农村农业特扶',IIF(CX=2,'地区城镇农业特扶',IIF(CX=3,'地区农村非农特扶','地区城镇非农特扶')))
	SELECT A
	RELEASE ARSWD
	SUM &子女..SWD TO sw
	CALCULATE MAX(X) TO MANL FOR &子女..SWD<>0
	COPY TO ARRAY ARSWD FIELDS &子女..SWD  FOR X<=MANL
	COPY TO ARRAY ARFR1 FIELDS &子女..FR1 FOR X>=15.AND.X<=49
	COPY TO ARRAY ARFND2 FIELDS &特扶..&FND2 FOR X>=15
	*****推算当年丧子女母亲
	FOR NL=1 TO MANL+1
		IF ARSWD(NL)=0
			LOOP
		ENDIF 
		母亲MINL=NL+14
		母亲MANL=NL+48
		REPLACE  &特扶..&FND2 WITH &特扶..&FND2+ARSWD(NL)*ARFR1(RECNO()-母亲MINL) FOR (RECNO()-母亲MINL)>=1.AND.(RECNO()-母亲MANL)<=0
	ENDFOR
	*****推算当年丧子女父亲
	FOR NL2=15 TO 90
		LOCATE FOR X=NL2
		*新增丧子母亲=&特扶..&FND2-ARFND2(NL2-14)
		新增丧子母亲=&特扶..&FND2	&&-ARFND2(NL2-14)
		妻子NL='妻子'-ALLT(STR(NL2,3))
		REPLACE &特扶..&MND2 WITH &特扶..&MND2+新增丧子母亲*需夫模式.&妻子NL ALL
	ENDFOR
	*****特扶父母年龄移算***************
	COPY TO ARRAY ARND1 FIEL &特扶..&MND1,&特扶..&FND1
 	REPL &特扶..&MND2 WITH &特扶..&MND2+ARND1(RECN()-1,1)*(1-IIF(CX<=2,NYQM(RECN()-1),FNQM(RECN()-1))),;
 		 &特扶..&FND2 WITH &特扶..&FND2+ARND1(RECN()-1,2)*(1-IIF(CX<=2,NYQF(RECN()-1),FNQF(RECN()-1))) FOR RECN()>1
	*****丧子女母亲再SY
	REPL &子女..再生育 WITH &特扶..&FND2*IIF(CX=1.OR.CX=2,农村生育模式.分释模式30,城镇生育模式.分释模式30) FOR X>=30.AND.X<35
	REPL &子女..再生育 WITH &特扶..&FND2*IIF(CX=1.OR.CX=2,农村生育模式.分释模式35,城镇生育模式.分释模式35) FOR X>=35.AND.X=<49
	REPL &子女..再生育 WITH &特扶..&FND2*IIF(CX=1.OR.CX=2,农村生育模式.正常模式,城镇生育模式.正常模式) FOR X<30
	*****剔除再SY母亲
	REPL &特扶..&FND2 WITH &特扶..&FND2-&子女..再生育  ALL
	*****剔除再SY父亲
	FOR NL2=15 TO 49
		LOCATE FOR X=NL2
		再生育母亲=&子女..再生育
		妻子NL='妻子'-ALLT(STR(NL2,3))
		REPLACE &特扶..&MND2 WITH &特扶..&MND2-再生育母亲*需夫模式.&妻子NL ALL
	ENDFOR
	*****再SY子女加入独生子女
	SUM &子女..再生育 TO 再生D FOR X>=15.AND.X<=49
	IF ND0=ND1
		SUM &子女..再生育/&特扶..&FND2 TO 再生L FOR X>=15.AND.X<=49.AND.&特扶..&FND2<>0
	ELSE
		COPY TO ARRAY ARFND1 FIELDS &特扶..&FND1
		SUM &子女..再生育/((ARFND1(RECNO()-1)+&特扶..&FND2)/2) TO 再生L FOR X>=15.AND.X<=49.AND.&特扶..&FND2<>0
	ENDIF
	LOCATE FOR X=0
	REPLACE &子女..DM WITH &子女..DM+再生D*XBB/(1+XBB),&子女..DF WITH &子女..DF+再生D/(1+XBB),&子女..D WITH &子女..DM+&子女..DF
	REPLACE &子女..政策生育 WITH &子女..政策生育+&子女..再生育 ALL
	*************************************
	DO CASE 
	CASE CX=1
		SUM &特扶..&MND2,&特扶..&FND2 TO 农村农业特扶父,农村农业特扶母 FOR X>=50
	CASE CX=2
		SUM &特扶..&MND2,&特扶..&FND2 TO 城镇农业特扶父,城镇农业特扶母 FOR X>=50
	CASE CX=3
		SUM &特扶..&MND2,&特扶..&FND2 TO 农村非农特扶父,农村非农特扶母 FOR X>=50
	CASE CX=4
		SUM &特扶..&MND2,&特扶..&FND2 TO 城镇非农特扶父,城镇非农特扶母 FOR X>=50
	ENDCASE
ENDFOR
RETURN 



**********************************************
PROCEDURE 地区特扶父母统计
SELECT A
REPL 地区城镇特扶.&MND2 WITH 地区城镇非农特扶.&MND2+地区城镇农业特扶.&MND2,地区城镇特扶.&FND2 WITH 地区城镇非农特扶.&FND2+地区城镇农业特扶.&FND2 ALL
REPL 地区农村特扶.&MND2 WITH 地区农村非农特扶.&MND2+地区农村农业特扶.&MND2,地区农村特扶.&FND2 WITH 地区农村非农特扶.&FND2+地区农村农业特扶.&FND2 ALL
REPL 地区非农特扶.&MND2 WITH 地区城镇非农特扶.&MND2+地区农村非农特扶.&MND2,地区非农特扶.&FND2 WITH 地区城镇非农特扶.&FND2+地区农村非农特扶.&FND2 ALL
REPL 地区农业特扶.&MND2 WITH 地区农村农业特扶.&MND2+地区城镇农业特扶.&MND2,地区农业特扶.&FND2 WITH 地区农村农业特扶.&FND2+地区城镇农业特扶.&FND2 ALL
REPL 地区特扶.&MND2 WITH 地区城镇特扶.&MND2+地区农村特扶.&MND2,地区特扶.&FND2 WITH 地区城镇特扶.&FND2+地区农村特扶.&FND2 ALL
REPL 地区城镇父母.&MND2 WITH 地区城镇非农父母.&MND2+地区城镇农业父母.&MND2,地区城镇父母.&FND2 WITH 地区城镇非农父母.&FND2+地区城镇农业父母.&FND2 ALL
REPL 地区农村父母.&MND2 WITH 地区农村非农父母.&MND2+地区农村农业父母.&MND2,地区农村父母.&FND2 WITH 地区农村非农父母.&FND2+地区农村农业父母.&FND2 ALL
REPL 地区非农父母.&MND2 WITH 地区城镇非农父母.&MND2+地区农村非农父母.&MND2,地区非农父母.&FND2 WITH 地区城镇非农父母.&FND2+地区农村非农父母.&FND2 ALL
REPL 地区农业父母.&MND2 WITH 地区农村农业父母.&MND2+地区城镇农业父母.&MND2,地区农业父母.&FND2 WITH 地区农村农业父母.&FND2+地区城镇农业父母.&FND2 ALL
REPL 地区父母.&MND2 WITH 地区城镇父母.&MND2+地区农村父母.&MND2,地区父母.&FND2 WITH 地区城镇父母.&FND2+地区农村父母.&FND2 ALL

FOR CX=1 TO 4
	ALI1=IIF(CX=1,'地区农村农业特扶',IIF(CX=2,'地区城镇农业特扶',IIF(CX=3,'地区农村非农特扶','地区城镇非农特扶')))
	ALI2=IIF(CX=1,'地区农村农业父母',IIF(CX=2,'地区城镇农业父母',IIF(CX=3,'地区农村非农父母','地区城镇非农父母')))
	SELECT A
	SUM &ALI1..&MND2,&ALI1..&FND2 TO TFM,TFF FOR X>=49
	SUM &ALI2..&MND2,&ALI2..&FND2 TO S一孩父,S一孩母 FOR X>=60
	SELE IIF(CX=1,41,IIF(CX=2,42,IIF(CX=3,43,44)))
	LOCATE FOR 年份=ND0
	REPLACE 特扶F WITH TFF,特扶M WITH TFM,特扶 WITH TFF+TFM,一孩父 WITH S一孩父,一孩母  WITH S一孩母 ,一孩父母 WITH S一孩父+S一孩母
	*?'特扶F',特扶F,	'特扶M',特扶M,	'特扶',特扶,	'一孩父',一孩父,	'一孩母',一孩母,	'一孩父母',一孩父母
ENDFOR
RETURN 	


**********************************************
PROCEDURE 全国一孩父母和特扶父母统计
SELECT A
REPL 全国城镇特扶.&MND2 WITH 全国城镇非农特扶.&MND2+全国城镇农业特扶.&MND2,全国城镇特扶.&FND2 WITH 全国城镇非农特扶.&FND2+全国城镇农业特扶.&FND2 ALL
REPL 全国农村特扶.&MND2 WITH 全国农村非农特扶.&MND2+全国农村农业特扶.&MND2,全国农村特扶.&FND2 WITH 全国农村非农特扶.&FND2+全国农村农业特扶.&FND2 ALL
REPL 全国非农特扶.&MND2 WITH 全国城镇非农特扶.&MND2+全国城镇非农特扶.&MND2,全国非农特扶.&FND2 WITH 全国城镇非农特扶.&FND2+全国城镇非农特扶.&FND2 ALL
REPL 全国农业特扶.&MND2 WITH 全国农村农业特扶.&MND2+全国农村农业特扶.&MND2,全国农业特扶.&FND2 WITH 全国农村农业特扶.&FND2+全国农村农业特扶.&FND2 ALL
REPL 全国特扶.&MND2 WITH 全国城镇特扶.&MND2+全国农村特扶.&MND2,全国特扶.&FND2 WITH 全国城镇特扶.&FND2+全国农村特扶.&FND2 ALL

REPL 全国城镇父母.&MND2 WITH 全国城镇非农父母.&MND2+全国城镇农业父母.&MND2,全国城镇父母.&FND2 WITH 全国城镇非农父母.&FND2+全国城镇农业父母.&FND2 ALL
REPL 全国农村父母.&MND2 WITH 全国农村非农父母.&MND2+全国农村农业父母.&MND2,全国农村父母.&FND2 WITH 全国农村非农父母.&FND2+全国农村农业父母.&FND2 ALL
REPL 全国非农父母.&MND2 WITH 全国城镇非农父母.&MND2+全国城镇非农父母.&MND2,全国非农父母.&FND2 WITH 全国城镇非农父母.&FND2+全国城镇非农父母.&FND2 ALL
REPL 全国农业父母.&MND2 WITH 全国农村农业父母.&MND2+全国农村农业父母.&MND2,全国农业父母.&FND2 WITH 全国农村农业父母.&FND2+全国农村农业父母.&FND2 ALL
REPL 全国父母.&MND2 WITH 全国城镇父母.&MND2+全国农村父母.&MND2,全国父母.&FND2 WITH 全国城镇父母.&FND2+全国农村父母.&FND2 ALL
FOR CX=1 TO 4
	ALI1=IIF(CX=1,'全国农村农业特扶',IIF(CX=2,'全国城镇农业特扶',IIF(CX=3,'全国农村非农特扶','全国城镇非农特扶')))
	ALI2=IIF(CX=1,'全国农村农业父母',IIF(CX=2,'全国城镇农业父母',IIF(CX=3,'全国农村非农父母','全国城镇非农父母')))
	SELECT A
	SUM &ALI1..&MND2,&ALI1..&FND2 TO TFM,TFF FOR X>=49
	SUM &ALI2..&MND2,&ALI2..&FND2 TO S一孩父,S一孩母 FOR X>=60
	SELE IIF(CX=1,151,IIF(CX=2,152,IIF(CX=3,153,154)))
	LOCATE FOR 年份=ND0
	REPLACE 特扶F WITH TFF,特扶M WITH TFM,特扶 WITH TFF+TFM,一孩父 WITH S一孩父,一孩母  WITH S一孩母 ,一孩父母 WITH S一孩父+S一孩母
ENDFOR


****************************************
PROCEDURE 城乡独生子女和非独生子女整理
*****农村独生子女和非独生子女调整
SELECT A
SRK1=0
FOR CX=1 TO 4    &&
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
	SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM1,SF1 &&FOR &子女..堆积比例<>0
	SRK1=SRK1+SM1+SF1
*	?ND0,子女,SM1,SF1
ENDFOR

SUM 地区农村农业子女.DM+地区农村农业子女.NM+地区农村农业子女.DDBM+地区农村农业子女.堆积丈夫,;
	地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF+地区农村农业子女.单独堆积+;
	地区农村农业子女.双非堆积+地区农村农业子女.已2单独+地区农村农业子女.已2双非  TO 地区农村农业子女M1,地区农村农业子女F1 &&FOR &子女..堆积比例<>0
SUM 地区农村非农子女.DM+地区农村非农子女.NM+地区农村非农子女.DDBM,;
	地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF+地区农村非农子女.单独堆积+;
	地区农村非农子女.双非堆积+地区农村非农子女.已2单独+地区农村非农子女.已2双非  TO 地区农村非农子女M1,地区农村非农子女F1 &&FOR &子女..堆积比例<>0

*?地区农村农业子女M1,地区农村农业子女F1,地区农村非农子女M1,地区农村非农子女F1

REPL 地区农村农业子女.DM WITH 地区农村分龄.&MND2*地区农村农业子女.DM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村农业子女.NM WITH 地区农村分龄.&MND2*地区农村农业子女.NM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村农业子女.DDBM WITH 地区农村分龄.&MND2*地区农村农业子女.DDBM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村农业子女.堆积丈夫 WITH 地区农村分龄.&MND2*地区农村农业子女.堆积丈夫/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村非农子女.DM WITH 地区农村分龄.&MND2*地区农村非农子女.DM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村非农子女.NM WITH 地区农村分龄.&MND2*地区农村非农子女.NM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村非农子女.DDBM WITH 地区农村分龄.&MND2*地区农村非农子女.DDBM/(地区农村农业子女.HJM+地区农村非农子女.HJM),;
	 地区农村非农子女.堆积丈夫 WITH 地区农村分龄.&MND2*地区农村非农子女.堆积丈夫/(地区农村农业子女.HJM+地区农村非农子女.HJM) FOR 地区农村农业子女.HJM+地区农村非农子女.HJM<>0

REPL 地区农村农业子女.DF WITH 地区农村分龄.&FND2*地区农村农业子女.DF/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村农业子女.NF WITH 地区农村分龄.&FND2*地区农村农业子女.NF/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村农业子女.DDBF WITH 地区农村分龄.&FND2*地区农村农业子女.DDBF/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.DF WITH 地区农村分龄.&FND2*地区农村非农子女.DF/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.NF WITH 地区农村分龄.&FND2*地区农村非农子女.NF/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.DDBF WITH 地区农村分龄.&FND2*地区农村农业子女.DDBF/(地区农村农业子女.HJF+地区农村非农子女.HJF) FOR 地区农村农业子女.HJF+地区农村非农子女.HJF<>0

SUM 地区农村农业子女.DM+地区农村农业子女.NM+地区农村农业子女.DDBM+地区农村农业子女.堆积丈夫,;
	地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF+地区农村农业子女.单独堆积+;
	地区农村农业子女.双非堆积+地区农村农业子女.已2单独+地区农村农业子女.已2双非  TO 地区农村农业子女M2,地区农村农业子女F2 &&FOR &子女..堆积比例<>0
SUM 地区农村非农子女.DM+地区农村非农子女.NM+地区农村非农子女.DDBM,;
	地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF+地区农村非农子女.单独堆积+;
	地区农村非农子女.双非堆积+地区农村非农子女.已2单独+地区农村非农子女.已2双非  TO 地区农村非农子女M2,地区农村非农子女F2 &&FOR &子女..堆积比例<>0

*?地区农村农业子女M2,地区农村农业子女F2,地区农村非农子女M2,地区农村非农子女F2
*?地区农村农业子女M1-地区农村农业子女M2,地区农村农业子女F1-地区农村农业子女F2,

?nd0,'地区农村非农子女M1-地区农村非农子女M2',地区农村非农子女M1-地区农村非农子女M2
?nd0,'地区农村非农子女F1-地区农村非农子女F2',地区农村非农子女F1-地区农村非农子女F2

*?地区农村农业子女M1-地区农村农业子女M2,地区农村农业子女F1-地区农村农业子女F2
*?地区农村非农子女M1-地区农村非农子女M2,地区农村非农子女F1-地区农村非农子女F2

SUM 地区农村农业子女.单独堆积,地区农村农业子女.双非堆积 ,地区农村农业子女.已2单独,地区农村农业子女.已2双非 to s农村农业单独堆积1,s农村农业双非堆积1 ,s农村农业已2单独1,s农村农业已2双非1
SUM 地区农村非农子女.单独堆积,地区农村非农子女.双非堆积,地区农村非农子女.已2单独,地区农村非农子女.已2双非 to s农村非农单独堆积1,s农村非农双非堆积1 ,s农村非农已2单独1,s农村非农已2双非1


	 
REPL 地区农村农业子女.单独堆积 WITH 地区农村分龄.&FND2*地区农村农业子女.单独堆积/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村农业子女.双非堆积 WITH 地区农村分龄.&FND2*地区农村农业子女.双非堆积/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村农业子女.已2单独 WITH 地区农村分龄.&FND2*地区农村农业子女.已2单独/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村农业子女.已2双非 WITH 地区农村分龄.&FND2*地区农村农业子女.已2双非/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
 	 地区农村非农子女.单独堆积 WITH 地区农村分龄.&FND2*地区农村非农子女.单独堆积/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.双非堆积 WITH 地区农村分龄.&FND2*地区农村非农子女.双非堆积/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.已2单独 WITH 地区农村分龄.&FND2*地区农村非农子女.已2单独/(地区农村农业子女.HJF+地区农村非农子女.HJF),;
	 地区农村非农子女.已2双非 WITH 地区农村分龄.&FND2*地区农村非农子女.已2双非/(地区农村农业子女.HJF+地区农村非农子女.HJF) FOR 地区农村农业子女.HJF+地区农村非农子女.HJF<>0

SUM 地区农村农业子女.单独堆积,地区农村农业子女.双非堆积 ,地区农村农业子女.已2单独,地区农村农业子女.已2双非 to s农村农业单独堆积2,s农村农业双非堆积2 ,s农村农业已2单独2,s农村农业已2双非2
SUM 地区农村非农子女.单独堆积,地区农村非农子女.双非堆积,地区农村非农子女.已2单独,地区农村非农子女.已2双非  to s农村非农单独堆积2,s农村非农双非堆积2 ,s农村非农已2单独2,s农村非农已2双非2
?
*?s农村农业单独堆积1,s农村农业双非堆积1 ,s农村农业已2单独1,s农村农业已2双非1
*?s农村非农单独堆积2,s农村非农双非堆积2 ,s农村非农已2单独2,s农村非农已2双非2
?nd0,'s农村农业单独堆积1-s农村非农单独堆积2',s农村农业单独堆积1-s农村非农单独堆积2
?nd0,'s农村农业双非堆积1-s农村非农双非堆积2',s农村农业双非堆积1-s农村非农双非堆积2
?nd0,'s农村农业已2单独1-s农村非农已2单独2',s农村农业已2单独1-s农村非农已2单独2
?nd0,'s农村农业已2双非1-s农村非农已2双非2',s农村农业已2双非1-s农村非农已2双非2

jk
*SUM 地区农村农业子女.DM+地区农村农业子女.DDBM+地区农村农业子女.NM+地区农村农业子女.堆积丈夫,;
	地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF+地区农村农业子女.单独堆积+地区农村农业子女.双非堆积+地区农村农业子女.已2单独+地区农村农业子女.已2双非  TO 地区农村农业子女M2,地区农村农业子女F2 &&FOR &子女..堆积比例<>0
*SUM 地区农村非农子女.DM+地区农村非农子女.DDBM+地区农村非农子女.NM+地区农村非农子女.堆积丈夫,;
	地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF+地区农村非农子女.单独堆积+地区农村非农子女.双非堆积+地区农村非农子女.已2单独+地区农村非农子女.已2双非  TO 地区农村非农子女M2,地区农村非农子女F2 &&FOR &子女..堆积比例<>0



*****城镇独生子女和非独生子女调整
REPL 地区城镇农业子女.DM WITH 地区城镇分龄.&MND2*地区城镇农业子女.DM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇农业子女.NM WITH 地区城镇分龄.&MND2*地区城镇农业子女.NM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇农业子女.DDBM WITH 地区城镇分龄.&MND2*地区城镇农业子女.DDBM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇农业子女.堆积丈夫 WITH 地区城镇分龄.&MND2*地区城镇农业子女.堆积丈夫/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇非农子女.DM WITH 地区城镇分龄.&MND2*地区城镇非农子女.DM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇非农子女.NM WITH 地区城镇分龄.&MND2*地区城镇非农子女.NM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇非农子女.DDBM WITH 地区城镇分龄.&MND2*地区城镇非农子女.DDBM/(地区城镇农业子女.HJM+地区城镇非农子女.HJM),;
	 地区城镇非农子女.堆积丈夫 WITH 地区城镇分龄.&MND2*地区城镇非农子女.堆积丈夫/(地区城镇农业子女.HJM+地区城镇非农子女.HJM)  FOR 地区城镇农业子女.HJM+地区城镇非农子女.HJM<>0

REPL 地区城镇农业子女.DF WITH 地区城镇分龄.&FND2*地区城镇农业子女.DF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.NF WITH 地区城镇分龄.&FND2*地区城镇农业子女.NF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.DDBF WITH 地区城镇分龄.&FND2*地区城镇农业子女.DDBF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.单独堆积 WITH 地区城镇分龄.&FND2*地区城镇农业子女.单独堆积/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.双非堆积 WITH 地区城镇分龄.&FND2*地区城镇农业子女.双非堆积/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.已2单独 WITH 地区城镇分龄.&FND2*地区城镇农业子女.已2单独/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇农业子女.已2双非 WITH 地区城镇分龄.&FND2*地区城镇农业子女.已2双非/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.DF WITH 地区城镇分龄.&FND2*地区城镇非农子女.DF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.NF WITH 地区城镇分龄.&FND2*地区城镇非农子女.NF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.DDBF WITH 地区城镇分龄.&FND2*地区城镇农业子女.DDBF/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.单独堆积 WITH 地区城镇分龄.&FND2*地区城镇非农子女.单独堆积/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.双非堆积 WITH 地区城镇分龄.&FND2*地区城镇非农子女.双非堆积/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.已2单独 WITH 地区城镇分龄.&FND2*地区城镇非农子女.已2单独/(地区城镇农业子女.HJF+地区城镇非农子女.HJF),;
	 地区城镇非农子女.已2双非 WITH 地区城镇分龄.&FND2*地区城镇非农子女.已2双非/(地区城镇农业子女.HJF+地区城镇非农子女.HJF)  FOR 地区城镇农业子女.HJF+地区城镇非农子女.HJF<>0

RETURN
*******************************************
PROCEDURE 地区多种政策和可实现时期生育率
*******************************************************************************************************************
*	说明:NFDMB=政策NFDMB+超生NFDMB,NMDFB=政策NMDFB+超生NMDFB。NFDMB,NMDFB即为实现生育数，以此计算的TFR即实现生育率
*	超生生育+政策生育=(DDB+NMDFB+NFDMB+NNB+单独释放B+双非释放B),即实现生育
*******************************************************************************************************************
*农村农业TFR,农村非农TFR,城镇农业TFR,城镇非农TFR即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区农村农业分龄.&FND1
SUM  (地区农村农业子女.双独B+地区农村农业子女.NMDFB+地区农村农业子女.NFDMB+地区农村农业子女.NNB+地区农村农业子女.单独释放B+地区农村农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村农业分龄.&FND2)/2) ;
	 TO 农村农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农村农业分龄.&FND2)/2)<>0		&&农村农业生育率
COPY TO ARRAY ARFND1 FIELDS 地区农村非农分龄.&FND1
SUM  (地区农村非农子女.双独B+地区农村非农子女.NMDFB+地区农村非农子女.NFDMB+地区农村非农子女.NNB+地区农村非农子女.单独释放B+地区农村非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村非农分龄.&FND2)/2) ;
	 TO 农村非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农村非农分龄.&FND2)/2)<>0		&&农村非农生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇农业分龄.&FND1
SUM  (地区城镇农业子女.双独B+地区城镇农业子女.NMDFB+地区城镇农业子女.NFDMB+地区城镇农业子女.NNB+地区城镇农业子女.单独释放B+地区城镇农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇农业分龄.&FND2)/2) ;
	 TO 城镇农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区城镇农业分龄.&FND2)/2)<>0		&&城镇农业生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇非农分龄.&FND1
SUM  (地区城镇非农子女.双独B+地区城镇非农子女.NMDFB+地区城镇非农子女.NFDMB+地区城镇非农子女.NNB+地区城镇非农子女.单独释放B+地区城镇非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇非农分龄.&FND2)/2) ;
	 TO 城镇非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区城镇非农分龄.&FND2)/2)<>0		&&城镇非农生育率

*农业政策TFR,农业实现TFR0,农业实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区农业分龄.&FND1
SUM  (地区农业子女.政策生育)/((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2) TO 农业政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2)<>0			&&地区农业政策生育率
SUM  (地区农业子女.双独B+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)/((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2) ;
	 TO 农业实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区农业分龄.&FND2)/2)<>0		&&农业实现生育率
SUM (地区农业子女.双独B+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)/地区农业子女.夫妇合计 ;
	 TO 农业实婚TFR0 FOR 地区农业子女.夫妇合计<>0.AND.(地区农业子女.双独B+地区农业子女.NMDFB+地区农业子女.NFDMB+地区农业子女.NNB+地区农业子女.单独释放B+地区农业子女.双非释放B)>0

*地区非农TFR,非农实现TFR0,非农实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区非农分龄.&FND1
SUM  (地区非农子女.政策生育)/((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2) TO 非农政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2)<>0			&&地区非农政策生育率
SUM  (地区非农子女.双独B+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)/((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2) ;
	 TO 非农实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+地区非农分龄.&FND2)/2)<>0		&&非农实现生育率
SUM  (地区非农子女.双独B+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)/(地区非农子女.夫妇合计) ;
	 TO 非农实婚TFR0 FOR 地区非农子女.夫妇合计<>0.AND.(地区非农子女.双独B+地区非农子女.NMDFB+地区非农子女.NFDMB+地区非农子女.NNB+地区非农子女.单独释放B+地区非农子女.双非释放B)>0		&&非农实婚生育率

*农村TFR0,农村实现TFR0,农村实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 地区农村分龄.&FND1
SUM  地区农村子女.政策生育/((ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2)  TO 农村政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2<>0			&&农村政策生育率
SUM  (地区农村子女.双独B+地区农村子女.NMDFB+地区农村子女.NFDMB+地区农村子女.NNB+地区农村子女.单独释放B+地区农村子女.双非释放B)/((ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2) TO 农村实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农村分龄.&FND2)/2<>0		&&农村可实现TFR
SUM  (地区农村子女.双独B+地区农村子女.NMDFB+地区农村子女.NFDMB+地区农村子女.NNB+地区农村子女.单独释放B+地区农村子女.双非释放B)/地区农村子女.夫妇合计  TO 农村实婚TFR0 FOR 地区农村子女.夫妇合计<>0.AND.地区农村生育.&BND2>0

*城镇TFR0,城镇实现TFR0,城镇实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 地区城镇分龄.&FND1
SUM  地区城镇子女.政策生育/((ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2) TO 城镇政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2<>0			&&城镇政策生育率
SUM  (地区城镇子女.双独B+地区城镇子女.NMDFB+地区城镇子女.NFDMB+地区城镇子女.NNB+地区城镇子女.单独释放B+地区城镇子女.双非释放B)/((ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2) TO 城镇实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区城镇分龄.&FND2)/2<>0		&&城镇实现生育率
SUM  (地区城镇子女.双独B+地区城镇子女.NMDFB+地区城镇子女.NFDMB+地区城镇子女.NNB+地区城镇子女.单独释放B+地区城镇子女.双非释放B)/(地区城镇农业子女.夫妇合计+地区城镇非农子女.夫妇合计) ;
	 TO 城镇实婚TFR0 FOR 地区城镇农业子女.夫妇合计+地区城镇非农子女.夫妇合计<>0.AND.(地区城镇生育.&BND2)>0		&&城镇实婚生育率

*地区政策TFR,地区实现TFR0,地区实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 地区分龄.&FND1
SUM  地区分龄政策生育.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地区分龄政策生育率
SUM  (地区子女.双独B+地区子女.NMDFB+地区子女.NFDMB+地区子女.NNB+地区子女.单独释放B+地区子女.双非释放B)/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)/2<>0			&&地s区可实现TFR
SUM  (地区子女.双独B+地区子女.NMDFB+地区子女.NFDMB+地区子女.NNB+地区子女.单独释放B+地区子女.双非释放B)/地区子女.夫妇合计  TO 地区实婚TFR0 FOR (地区子女.夫妇合计)<>0.AND.地区生育.&BND2>0

SUM 地区分龄超生.&BND2/((ARFND1(RECNO()-1)+地区分龄.&FND2)/2) TO 地区超生TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区分龄.&FND2)>0
SUM 地区城镇分龄.&MND2+地区城镇分龄.&FND2,地区农村分龄.&MND2+地区农村分龄.&FND2 TO CRK,XRK
CSH水平=CRK/(CRK+XRK)*100
SUM 地区非农分龄.&MND2+地区非农分龄.&FND2,地区农业分龄.&MND2+地区农业分龄.&FND2 TO FNRK,NYRK

*再生育率
IF ND0=ND1
	SUM  地区城镇子女.再生育/地区城镇特扶.&FND2 TO 城镇再生育L  FOR 地区城镇特扶.&FND2<>0	
	SUM  地区非农子女.再生育/地区非农特扶.&FND2 TO 非农再生育L FOR 地区非农特扶.&FND2<>0	
	SUM  地区农村子女.再生育/地区农村特扶.&FND2 TO 农村再生育L FOR 地区农村特扶.&FND2<>0	
	SUM  地区农业子女.再生育/地区农业特扶.&FND2 TO 农业再生育L FOR 地区农业特扶.&FND2<>0	
	SUM  地区子女.再生育/地区特扶.&FND2 TO 地区再生育L FOR 地区特扶.&FND2<>0	
ELSE
	COPY TO ARRAY ARFND1 FIELDS 地区城镇特扶.&FND1
	SUM  地区城镇子女.再生育/((ARFND1(RECNO()-1)+地区城镇特扶.&FND2)/2) TO 城镇再生育L  FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区城镇特扶.&FND2)<>0	

	COPY TO ARRAY ARFND1 FIELDS 地区非农特扶.&FND1
	SUM  地区非农子女.再生育/((ARFND1(RECNO()-1)+地区非农特扶.&FND2)/2) TO 非农再生育L FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区非农特扶.&FND2)<>0	

	COPY TO ARRAY ARFND1 FIELDS 地区农村特扶.&FND1
	SUM  地区农村子女.再生育/((ARFND1(RECNO()-1)+地区农村特扶.&FND2)/2) TO 农村再生育L FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农村特扶.&FND2)<>0	

	COPY TO ARRAY ARFND1 FIELDS 地区农业特扶.&FND1
	SUM  地区农业子女.再生育/((ARFND1(RECNO()-1)+地区农业特扶.&FND2)/2) TO 农业再生育L FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区农业特扶.&FND2)<>0	

	COPY TO ARRAY ARFND1 FIELDS 地区特扶.&FND1
	SUM  地区子女.再生育/((ARFND1(RECNO()-1)+地区特扶.&FND2)/2) TO 地区再生育L FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+地区特扶.&FND2)<>0
ENDIF
*****************
SELE 65 &&地区农村子女�
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农村婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 66 &&地区城镇子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 城镇婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 67	&&地区农业子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农业婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 68	&&地区非农子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 非农婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 69	&&地区子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 地区婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
RETURN 

****************************************************************************
PROCEDURE 各类婚配夫妇分年龄生育率
	REPL 合J分龄SYL WITH (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/HJF FOR HJF<>0 &&所有婚配模式合计的分年龄生育率
	REPL 双D分龄SYL WITH 双独B/双独 FOR 双独<>0						&&双独生育率
	REPL 双N分龄SYL WITH (NNB+双非释放B)/(NN+双非堆积+已2双非) FOR (NN+双非堆积+已2双非)<>0							&&单独生育率
	REPL 单D分龄SYL WITH (NMDFB+NFDMB+单独释放B)/(NMDF+NFDM+单独堆积+已2单独) FOR (NMDF+NFDM+单独堆积+已2单独)<>0	&&双非生育率
SUM 合J分龄SYL,双D分龄SYL,双N分龄SYL,单D分龄SYL TO 合JTFR,双DTFR,双NTFR,单DTFR
RETURN 

****************************************************************************
*****记录各类夫妇分年龄组人数**********
PROCEDURE 考贝地区分年龄婚配结果	&&AAA各类夫妇子女年龄分组
FOR CX=1 TO 4
	FOR FI1=10 TO 64
		FIENL1=AR子女婚配预测表结构(FI1,1)
		FOR NL=0 TO 20
			DO CASE 
			CASE NL=0
				x1=0
				x2=0
				NLFZ='X0'
			CASE NL=1
				x1=1
				x2=4
				NLFZ='X'-STR(x1,1)-'_'-STR(x2,1)
			CASE NL=20
				x1=100
				x2=110
				NLFZ='X'-STR(x1,3)
			OTHERWISE 
				x1=NL*5
				x2=x1+4
				NLFZ='X'-ALLTRIM(STR(x1,2))-'_'-ALLTRIM(STR(x2,2))
			ENDCASE 
			SELECT IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
				SUM &FIENL1 TO SNLZ FOR x>=x1.and.x<=x2
			SELECT IIF(CX=1,85,IIF(CX=2,86,IIF(CX=3,87,88)))
				IF NL=0
					APPEND BLANK 
					REPLACE 年份 WITH nd0,妇子类别  WITH FIENL1,&NLFZ WITH snlz
				ELSE  
					REPLACE 妇子类别  WITH FIENL1,合计 WITH 合计+snlz,&NLFZ WITH snlz
				ENDIF 
		ENDFOR 
	ENDFOR
ENDFOR 
RETURN 
*******************************************
PROCEDURE 记录各类夫妇政策内容及政策生育率
SELE 151		
LOCA FOR 年份=ND0
REPL 地区政策生育率.非农政策1 WITH ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4))),;
	 地区政策生育率.农业政策1 WITH ALLT(农业ZC1)-IIF(农业时机1=0,'',ALLT(STR(农业时机1,4))),;
	 地区政策生育率.非农政策2 WITH ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))),;
	 地区政策生育率.农业政策2 WITH ALLT(农业ZC2)-IIF(农业时机2=0,'',ALLT(STR(农业时机2,4))),;
	 地区政策生育率.非农政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(非农ZC3)-ALLT(STR(非农时机3,4)),''),;
	 地区政策生育率.农业政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(农业ZC3)-ALLT(STR(农业时机3,4)),'')
REPL 地区政策生育率.非农双独 WITH DDFR,地区政策生育率.农业双独 WITH DDFR,地区政策生育率.非农单独 WITH N2D1C,地区政策生育率.农业单独 WITH N2D1N,;
	地区政策生育率.非农女单 WITH N1D2C,地区政策生育率.农业女单 WITH N1D2N,地区政策生育率.非农男单 WITH N2D1C,地区政策生育率.农业男单 WITH N2D1N,;
	地区政策生育率.非农双非 WITH NNFC,地区政策生育率.农业双非 WITH NNFN,;
	地区政策生育率.农村NYTFR WITH 农村农业TFR,地区政策生育率.农村FNTFR WITH 农村非农TFR,地区政策生育率.农村TFR WITH 农村政策TFR0,;
	地区政策生育率.城镇NYTFR WITH 城镇农业TFR,地区政策生育率.城镇FNTFR WITH 城镇非农TFR,地区政策生育率.城镇TFR WITH 城镇政策TFR0,;
	地区政策生育率.地区NYTFR WITH 农业政策TFR0,地区政策生育率.地区FNTFR WITH 非农政策TFR0,地区政策生育率.地区TFR WITH 地区政策TFR0,;
	地区政策生育率.NCNY婚TFR WITH NCNY婚内TFR,地区政策生育率.NCFN婚TFR WITH NCFN婚内TFR,地区政策生育率.NC婚TFR WITH 农村婚内TFR,;
	地区政策生育率.CZNY婚TFR WITH CZNY婚内TFR,地区政策生育率.CZFN婚TFR WITH CZFN婚内TFR,地区政策生育率.CZ婚TFR WITH 城镇婚内TFR,;
	地区政策生育率.QSNY婚TFR WITH 农业婚内TFR,地区政策生育率.QSFN婚TFR WITH 非农婚内TFR,地区政策生育率.QS婚TFR WITH 地区婚内TFR
FOR FI1=20 TO 86	&&FCOUN()
	FIE1=FIELD(FI1)
	REPL 地区农村夫妇.&FIE1 WITH 地区农村农业夫妇.&FIE1+地区农村非农夫妇.&FIE1,地区城镇夫妇.&FIE1 WITH 地区城镇农业夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;
		 地区农业夫妇.&FIE1 WITH 地区农村农业夫妇.&FIE1+地区城镇农业夫妇.&FIE1,地区非农夫妇.&FIE1 WITH 地区农村非农夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;
		 地区夫妇.&FIE1 WITH 地区农村夫妇.&FIE1+地区城镇夫妇.&FIE1 FOR 年份=ND0
ENDFOR

RETURN 

*******************************************
PROCEDURE 记录全国各类夫妇政策内容及政策生育率
SELE 151		
LOCA FOR 年份=ND0
REPL 全国政策生育率.非农双独 WITH DDFR,全国政策生育率.农业双独 WITH DDFR,全国政策生育率.非农单独 WITH N2D1C,全国政策生育率.农业单独 WITH N2D1N,;
	全国政策生育率.非农女单 WITH N1D2C,全国政策生育率.农业女单 WITH N1D2N,全国政策生育率.非农男单 WITH N2D1C,全国政策生育率.农业男单 WITH N2D1N,;
	全国政策生育率.非农双非 WITH NNFC,全国政策生育率.农业双非 WITH NNFN,;
	全国政策生育率.农村NYTFR WITH 农村农业TFR,全国政策生育率.农村FNTFR WITH 农村非农TFR,全国政策生育率.农村TFR WITH 农村政策TFR0,;
	全国政策生育率.城镇NYTFR WITH 城镇农业TFR,全国政策生育率.城镇FNTFR WITH 城镇非农TFR,全国政策生育率.城镇TFR WITH 城镇政策TFR0,;
	全国政策生育率.地区NYTFR WITH 农业政策TFR0,全国政策生育率.地区FNTFR WITH 非农政策TFR0,全国政策生育率.地区TFR WITH 地区政策TFR0,;
	全国政策生育率.NCNY婚TFR WITH NCNY婚内TFR,全国政策生育率.NCFN婚TFR WITH NCFN婚内TFR,全国政策生育率.NC婚TFR WITH 农村婚内TFR,;
	全国政策生育率.CZNY婚TFR WITH CZNY婚内TFR,全国政策生育率.CZFN婚TFR WITH CZFN婚内TFR,全国政策生育率.CZ婚TFR WITH 城镇婚内TFR,;
	全国政策生育率.QSNY婚TFR WITH 农业婚内TFR,全国政策生育率.QSFN婚TFR WITH 非农婚内TFR,全国政策生育率.QS婚TFR WITH 地区婚内TFR
RETURN 

*******************************************
PROCEDURE 生育孩次合计
SELECT 151
REPL 地区生育孩次.城FN双独B WITH 地区生育孩次.城FN双独B1+地区生育孩次.城FN双独B2,地区生育孩次.城FN单独B WITH 地区生育孩次.城FN单独B1+地区生育孩次.城FN单独B2,地区生育孩次.城FN双非B WITH 地区生育孩次.城FN双非B1+地区生育孩次.城FN双非B2,;
	地区生育孩次.城NY双独B WITH 地区生育孩次.城NY双独B1+地区生育孩次.城NY双独B2,地区生育孩次.城NY单独B WITH 地区生育孩次.城NY单独B1+地区生育孩次.城NY单独B2,地区生育孩次.城NY双非B WITH 地区生育孩次.城NY双非B1+地区生育孩次.城NY双非B2,;
	地区生育孩次.乡FN双独B WITH 地区生育孩次.乡FN双独B1+地区生育孩次.乡FN双独B2,地区生育孩次.乡FN单独B WITH 地区生育孩次.乡FN单独B1+地区生育孩次.乡FN单独B2,地区生育孩次.乡FN双非B WITH 地区生育孩次.乡FN双非B1+地区生育孩次.乡FN双非B2,;
	地区生育孩次.乡NY双独B WITH 地区生育孩次.乡NY双独B1+地区生育孩次.乡NY双独B2,地区生育孩次.乡NY单独B WITH 地区生育孩次.乡NY单独B1+地区生育孩次.乡NY单独B2,地区生育孩次.乡NY双非B WITH 地区生育孩次.乡NY双非B1+地区生育孩次.乡NY双非B2,;
	地区生育孩次.总NY双独B WITH 地区生育孩次.城NY双独B+地区生育孩次.乡NY双独B,地区生育孩次.总FN双独B WITH 地区生育孩次.城FN双独B+地区生育孩次.乡FN双独B,;
	地区生育孩次.总NY单独B WITH 地区生育孩次.城NY单独B+地区生育孩次.乡NY单独B,地区生育孩次.总FN单独B WITH 地区生育孩次.城FN单独B+地区生育孩次.乡FN单独B,;
	地区生育孩次.总FN双非B WITH 地区生育孩次.城FN双非B+地区生育孩次.乡FN双非B,地区生育孩次.总NY双非B WITH 地区生育孩次.城NY双非B+地区生育孩次.乡NY双非B,;
	地区生育孩次.总FN双独B1 WITH 地区生育孩次.城FN双独B1+地区生育孩次.乡FN双独B1,地区生育孩次.总NY双独B1 WITH 地区生育孩次.城NY双独B1+地区生育孩次.乡NY双独B1,;
	地区生育孩次.总FN单独B1 WITH 地区生育孩次.城FN单独B1+地区生育孩次.乡FN单独B1,地区生育孩次.总NY单独B1 WITH 地区生育孩次.城NY单独B1+地区生育孩次.乡NY单独B1,;
	地区生育孩次.总FN双非B1 WITH 地区生育孩次.城FN双非B1+地区生育孩次.乡FN双非B1,地区生育孩次.总NY双非B1 WITH 地区生育孩次.城NY双非B1+地区生育孩次.乡NY双非B1,;
	地区生育孩次.总FN双独B2 WITH 地区生育孩次.城FN双独B2+地区生育孩次.乡FN双独B2,地区生育孩次.总NY双独B2 WITH 地区生育孩次.城NY双独B2+地区生育孩次.乡NY双独B2,;
	地区生育孩次.总FN单独B2 WITH 地区生育孩次.城FN单独B2+地区生育孩次.乡FN单独B2,地区生育孩次.总NY单独B2 WITH 地区生育孩次.城NY单独B2+地区生育孩次.乡NY单独B2,;
	地区生育孩次.总FN双非B2 WITH 地区生育孩次.城FN双非B2+地区生育孩次.乡FN双非B2,地区生育孩次.总NY双非B2 WITH 地区生育孩次.城NY双非B2+地区生育孩次.乡NY双非B2,;
	地区生育孩次.乡HJ双独B WITH 地区生育孩次.乡FN双独B+地区生育孩次.乡NY双独B,地区生育孩次.乡HJ双独B1 WITH 地区生育孩次.乡FN双独B1+地区生育孩次.乡NY双独B1,地区生育孩次.乡HJ双独B2 WITH 地区生育孩次.乡FN双独B2+地区生育孩次.乡NY双独B2,;
	地区生育孩次.乡HJ单独B WITH 地区生育孩次.乡FN单独B+地区生育孩次.乡NY单独B ,地区生育孩次.乡HJ单独B1 WITH 地区生育孩次.乡FN单独B1+地区生育孩次.乡NY单独B1,地区生育孩次.乡HJ单独B2 WITH 地区生育孩次.乡FN单独B2+地区生育孩次.乡NY单独B2,;
	地区生育孩次.乡HJ双非B WITH 地区生育孩次.乡FN双非B+地区生育孩次.乡NY双非B ,地区生育孩次.乡HJ双非B1 WITH 地区生育孩次.乡FN双非B1+地区生育孩次.乡NY双非B1,地区生育孩次.乡HJ双非B2 WITH 地区生育孩次.乡FN双非B2+地区生育孩次.乡NY双非B2,;
	地区生育孩次.城HJ双独B WITH 地区生育孩次.城FN双独B+地区生育孩次.城NY双独B ,地区生育孩次.城HJ双独B1 WITH 地区生育孩次.城FN双独B1+地区生育孩次.城NY双独B1,地区生育孩次.城HJ双独B2 WITH 地区生育孩次.城FN双独B2+地区生育孩次.城NY双独B2,;
	地区生育孩次.城HJ单独B WITH 地区生育孩次.城FN单独B+地区生育孩次.城NY单独B ,地区生育孩次.城HJ单独B1 WITH 地区生育孩次.城FN单独B1+地区生育孩次.城NY单独B1,地区生育孩次.城HJ单独B2 WITH 地区生育孩次.城FN单独B2+地区生育孩次.城NY单独B2,;
	地区生育孩次.城HJ双非B WITH 地区生育孩次.城FN双非B+地区生育孩次.城NY双非B ,地区生育孩次.城HJ双非B1 WITH 地区生育孩次.城FN双非B1+地区生育孩次.城NY双非B1,地区生育孩次.城HJ双非B2 WITH 地区生育孩次.城FN双非B2+地区生育孩次.城NY双非B2,;
	地区生育孩次.总HJ双独B WITH 地区生育孩次.城HJ双独B+地区生育孩次.乡HJ双独B ,地区生育孩次.总HJ双独B1 WITH 地区生育孩次.城HJ双独B1+地区生育孩次.乡HJ双独B1,地区生育孩次.总HJ双独B2 WITH 地区生育孩次.城HJ双独B2+地区生育孩次.乡HJ双独B2,;
	地区生育孩次.总HJ单独B WITH 地区生育孩次.城HJ单独B+地区生育孩次.乡HJ单独B ,地区生育孩次.总HJ单独B1 WITH 地区生育孩次.城HJ单独B1+地区生育孩次.乡HJ单独B1,地区生育孩次.总HJ单独B2 WITH 地区生育孩次.城HJ单独B2+地区生育孩次.乡HJ单独B2,;
	地区生育孩次.总HJ双非B WITH 地区生育孩次.城HJ双非B+地区生育孩次.乡HJ双非B ,地区生育孩次.总HJ双非B1 WITH 地区生育孩次.城HJ双非B1+地区生育孩次.乡HJ双非B1,地区生育孩次.总HJ双非B2 WITH 地区生育孩次.城HJ双非B2+地区生育孩次.乡HJ双非B2,;
	地区生育孩次.乡HJB1 WITH 地区生育孩次.乡NY双独B1+地区生育孩次.乡NY单独B1+地区生育孩次.乡NY双非B1+地区生育孩次.乡FN双独B1+地区生育孩次.乡FN单独B1+地区生育孩次.乡FN双非B1,;
	地区生育孩次.乡HJB2 WITH 地区生育孩次.乡NY双独B2+地区生育孩次.乡NY单独B2+地区生育孩次.乡NY双非B2+地区生育孩次.乡FN双独B2+地区生育孩次.乡FN单独B2+地区生育孩次.乡FN双非B2,;
	地区生育孩次.城HJB1 WITH 地区生育孩次.城NY双独B1+地区生育孩次.城NY单独B1+地区生育孩次.城NY双非B1+地区生育孩次.城FN双独B1+地区生育孩次.城FN单独B1+地区生育孩次.城FN双非B1,;
	地区生育孩次.城HJB2 WITH 地区生育孩次.城NY双独B2+地区生育孩次.城NY单独B2+地区生育孩次.城NY双非B2+地区生育孩次.城FN双独B2+地区生育孩次.城FN单独B2+地区生育孩次.城FN双非B2,;
	地区生育孩次.乡HJB WITH 地区生育孩次.乡HJB1+地区生育孩次.乡HJB2,地区生育孩次.城HJB WITH 地区生育孩次.城HJB1+地区生育孩次.城HJB2,地区生育孩次.总HJB2 WITH 地区生育孩次.城HJB2+地区生育孩次.乡HJB2,;
	地区生育孩次.总HJB1 WITH 地区生育孩次.城HJB1+地区生育孩次.乡HJB1,地区生育孩次.总HJB WITH 地区生育孩次.总HJB1+地区生育孩次.总HJB2,;
	地区生育孩次.总FNB1 WITH 地区生育孩次.总FN双独B1+地区生育孩次.总FN单独B1+地区生育孩次.总FN双非B1,地区生育孩次.总FNB2 WITH 地区生育孩次.总FN双独B2+地区生育孩次.总FN单独B2+地区生育孩次.总FN双非B2,;
	地区生育孩次.总NYB1 WITH 地区生育孩次.总NY双独B1+地区生育孩次.总NY单独B1+地区生育孩次.总NY双非B1,地区生育孩次.总NYB2 WITH 地区生育孩次.总NY双独B2+地区生育孩次.总NY单独B2+地区生育孩次.总NY双非B2 ALL
RETURN 

*******************************************
PROCEDURE 逐层全国地区累计
*************************
*	  逐层全国累计		*
*********************************
*	  分年龄数据全国累计		*
*********************************
*?'省区迁移和:',SQY
SELECT A
FOR LBX=1 TO 6
	RK类别=IIF(LBX=1,'分龄',IIF(LBX=2,'特扶',IIF(LBX=3,'父母',IIF(LBX=4,'迁移',IIF(LBX=5,'死亡','生育')))))
	FOR XBX=1 TO IIF(LBX=6,1,2)
		XB=IIF(XBX=1,'M','F')
		IF LBX=6
			FIEXB=BND2
		ELSE
			FIEXB=IIF(XBX=1,MND2,FND2)
		ENDIF
		地区分龄='地区'-RK类别
		全国分龄='全国'-RK类别
		REPLACE &全国分龄..&FIEXB WITH  &全国分龄..&FIEXB+&地区分龄..&FIEXB ALL
		FOR CXX=1 TO 2	&&3
			DO CASE
			CASE LBX<=3
				城乡=IIF(CXX=1,'城镇','农村')
				地区城乡分龄='地区'-城乡-RK类别
				全国城乡分龄='全国'-城乡-RK类别
				REPLACE &全国城乡分龄..&FIEXB WITH  &全国城乡分龄..&FIEXB+&地区城乡分龄..&FIEXB ALL
				农非=IIF(CXX=1,'农业','非农')
				地区农非分龄='地区'-农非-RK类别
				全国农非分龄='全国'-农非-RK类别
				REPLACE &全国农非分龄..&FIEXB WITH  &全国农非分龄..&FIEXB+&地区农非分龄..&FIEXB ALL
				FOR NFX=1 TO 2
					农非=IIF(NFX=1,'农业','非农')
					地区城乡农非分龄='地区'-城乡-农非-RK类别
					全国城乡农非分龄='全国'-城乡-农非-RK类别
					REPLACE &全国城乡农非分龄..&FIEXB WITH  &全国城乡农非分龄..&FIEXB+&地区城乡农非分龄..&FIEXB ALL
				ENDFOR
			CASE LBX=6
				城乡=IIF(CXX=1,'城镇','农村')
				地区城乡分龄='地区'-城乡-RK类别
				全国城乡分龄='全国'-城乡-RK类别
				REPLACE &全国城乡分龄..&FIEXB WITH  &全国城乡分龄..&FIEXB+&地区城乡分龄..&FIEXB ALL
				农非=IIF(CXX=1,'非农','农业')
				地区农非分龄='地区'-农非-RK类别
				全国农非分龄='全国'-农非-RK类别
				REPLACE &全国农非分龄..&FIEXB WITH  &全国农非分龄..&FIEXB+&地区农非分龄..&FIEXB ALL
			ENDCASE
		ENDFOR
	ENDFOR
ENDFOR

*SUM 全国分龄.&MND1+全国分龄.&FND1 TO QGZRK
*?'全国总人口:',QGZRK/10000
*kj

*****超生累计*****
REPLACE 全国分龄政策生育.&BND2 WITH 全国分龄政策生育.&BND2 +地区分龄政策生育.&BND2  ALL
REPLACE 全国分龄超生.&BND2 WITH 全国分龄超生.&BND2 +地区分龄超生.&BND2,;
	全国城镇超生.&BND2 WITH 全国城镇超生.&BND2 +地区城镇超生.&BND2,;
	全国农村超生.&BND2 WITH 全国农村超生.&BND2 +地区农村超生.&BND2,;
	全国农业超生.&BND2 WITH 全国农业超生.&BND2 +地区农业超生.&BND2,;
	全国非农超生.&BND2 WITH 全国非农超生.&BND2 +地区非农超生.&BND2 ALL
*****婚配预测累计*****
FOR F1=10 TO 179	&&89
	FIE1=AR子女婚配预测表结构(F1,1)
	REPLACE 全国子女.&FIE1 WITH IIF(DQX=DQ1,地区子女.&FIE1,全国子女.&FIE1+地区子女.&FIE1) ALL
	FOR CXX=1 TO 2
		城乡=IIF(CXX=1,'城镇','农村')
		FOR NFX=1 TO 3
			农非=IIF(NFX=1,'农业',IIF(NFX=2,'非农',''))
			地区城乡农非子女='地区'-城乡-农非-'子女'
			全国城乡农非子女='全国'-城乡-农非-'子女'
			REPLACE &全国城乡农非子女..&FIE1 WITH   IIF(DQX=DQ1,&地区城乡农非子女..&FIE1,&全国城乡农非子女..&FIE1+&地区城乡农非子女..&FIE1) ALL
		ENDFOR
		农非=IIF(CXX=1,'农业','非农')
		地区农非子女='地区'-农非-'子女'
		全国农非子女='全国'-农非-'子女'
		REPLACE &全国农非子女..&FIE1 WITH  IIF(DQX=DQ1,&地区农非子女..&FIE1,&全国农非子女..&FIE1+&地区农非子女..&FIE1) ALL
	ENDFOR
ENDFOR



*********************************
*	  分年度数据全国累计		*
*********************************
IF ND0=ND2
	SELECT 151
	*LOCATE FOR 年份=ND0
	FOR F1=2 TO 91
		FIE1=AR生育孩次(F1,1)
		REPLACE 全国生育孩次.&FIE1 WITH 全国生育孩次.&FIE1+地区生育孩次.&FIE1 ALL && FOR 年份=ND0
	ENDFOR
	******分年度各类夫妇及子女合计******
	FOR F1=20 TO 87
		FIE1=AR各类夫妇(F1,1)
		REPLACE  全国农村农业夫妇.&FIE1 WITH 全国农村农业夫妇.&FIE1+地区农村农业夫妇.&FIE1,;
		 全国农村非农夫妇.&FIE1 WITH 全国农村非农夫妇.&FIE1+地区农村非农夫妇.&FIE1,;
		 全国城镇农业夫妇.&FIE1 WITH 全国城镇农业夫妇.&FIE1+地区城镇农业夫妇.&FIE1,;
		 全国城镇非农夫妇.&FIE1 WITH 全国城镇非农夫妇.&FIE1+地区城镇非农夫妇.&FIE1,;	
		 全国农业夫妇.&FIE1 WITH 全国农业夫妇.&FIE1+地区农业夫妇.&FIE1,;
		 全国非农夫妇.&FIE1 WITH 全国非农夫妇.&FIE1+地区非农夫妇.&FIE1,;
	     全国农村夫妇.&FIE1 WITH 全国农村夫妇.&FIE1+地区农村夫妇.&FIE1,;	
		 全国城镇夫妇.&FIE1 WITH 全国城镇夫妇.&FIE1+地区城镇夫妇.&FIE1,;
		 全国夫妇.&FIE1 WITH 全国夫妇.&FIE1+地区夫妇.&FIE1 ALL
	ENDFOR 
ENDIF
RETURN
*********************************************

PROCEDURE 地区城乡预测结果摘要
??',',STR(N1D2C,4,2),',',STR(N2D1C,4,2),',',STR(NNFC,4,2),',',STR(N1D2N,4,2),',',STR(N2D1N,4,2),',',STR(NNFN,4,2),',',;
STR(地区政策TFR0,4,2),',',STR(城镇政策TFR0,4,2),',',STR(非农政策TFR0,4,2),',',STR(农村政策TFR0,4,2),',',STR(农业政策TFR0,4,2)
*?STR(CQYB,6,2),STR(XQYB,6,2),STR(C总迁率M,6,2),STR(C总迁率F,6,2),STR(X总迁率M,6,2),STR(X总迁率F,6,2)
SELECT 402	&&各地区生育率与迁移摘要 	
	REPLACE FN女单终身 WITH N1D2C,FN男单终身 WITH  N2D1C,FN双非终身 WITH NNFC,NY女单终身 WITH  N1D2N,NY男单终身 WITH  N2D1N,NY双非终身 WITH NNFN
	REPLACE	DQ时期TFR  WITH 地区政策TFR0,CZ时期TFR WITH 城镇政策TFR0,FN时期TFR WITH 非农政策TFR0 ,NC时期TFR  WITH 农村政策TFR0,NY时期TFR  WITH 农业政策TFR0
SELECT A
DQQY=0
FOR CX=1 TO 5
	城乡B=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
	分龄预测='地区'-城乡B-'分龄'
	迁移人口='地区'-城乡B-IIF(CX=3.OR.CX=5,'子女','迁移')
	死亡概率M=城乡B-'死亡概率M'
	死亡概率F=城乡B-'死亡概率F'
	城乡死亡='地区'-城乡B-'死亡'
	SELE A
	LOCA FOR X=0
	出生人口=(&分龄预测..&MND2+&分龄预测..&FND2)/10000
	出生人口M=&分龄预测..&MND2/10000
	出生人口F=&分龄预测..&FND2/10000
	IF CX<=2.OR.CX=4
		SUM (&迁移人口..&MND2+&迁移人口..&FND2)/10000 TO 净迁入RK
		SUM &迁移人口..&MND2/10000 TO 净迁入M
		SUM &迁移人口..&FND2/10000 TO 净迁入F
		DO 死亡人口和预期寿命计算
  		*DO 计算预期寿命
  		DO CASE 
  		CASE CX=2.OR.CX=3
  			CEM0=EM0
  			CEF0=EF0
  		CASE CX=4.OR.CX=5
  			XEM0=EM0
  			XEF0=EF0
  		ENDCASE
  	ELSE
		SUM &迁移人口..QYHJ/10000 TO 净迁入RK
 		SUM &迁移人口..QYHJM/10000 TO 净迁入M
		SUM &迁移人口..QYHJF/10000 TO 净迁入F
 	ENDIF
  	??',',IIF(CX=1,'净迁',''),STR(净迁入RK,6,2)	
  	*IF CX=1
	*  	??'与上年比：',STR(净迁入RK/DQQYND0,6,2)
    *ENDIF 
  	FIE1=IIF(CX=1,'DQ迁移合计',ALLTRIM(城乡B)-'迁移')
  	SELECT 402
  	REPLACE &FIE1 WITH 净迁入RK
  	IF CX=1
  		SQY=SQY+净迁入RK
  		SDQQY=净迁入RK
  	ENDIF
  	IF CX=2.OR.CX=4
  		DQQY=DQQY+净迁入RK
  	ENDIF
	SELE IIF(CX=1,1,IIF(CX=2,2,IIF(CX=3,3,IIF(CX=4,4,5))))	    &&SELE 1  地区分龄；SELE 2  地区城镇分龄；SELE 3 地区非农分龄；SELE 4:地区农村分龄；SELE 5  地区农业分龄；
  		EM0=IIF(CX=2.OR.CX=3,CEM0,IIF(CX=4.OR.CX=5,XEM0,EM0))
  		EF0=IIF(CX=2.OR.CX=3,CEF0,IIF(CX=4.OR.CX=5,XEF0,EF0))
		DO 年龄分组和年龄结构
	SELE IIF(CX=1,51,IIF(CX=2,52,IIF(CX=3,53,IIF(CX=4,54,55)))) &&SELE 51 地区概要；SELE 52 地区城镇概要;SELE 53 地区非农概要;SELE 54 地区农村概要；SELE 55  地区农业概要；							   	
		DQB=IIF(CX=1,DQB,城乡B)
		SEL=STR(SELECT(),4)
		*LOCATE FOR 年份=ND0
		DO 预测结果摘要
		IF CX=1
			DQQYL=净迁入率
		ENDIF
		*?'地区',地区,'年份',STR(年份,4),'出生-出生一孩-出生二孩',STR(出生-出生一孩-出生二孩,8,2),'生育率',STR(生育率,8,2),'自增率',STR(自增率,8,2),'净迁入数',STR(净迁入数,8,2),'城镇人口比',STR(城镇人口比,8,2)
	IF ND0=ND2
		DO 主要极值
	ENDIF
ENDFOR	&&分城乡概况摘要循环
IF ND0=ND2
	SELECT 151
	REPLACE 地区概要.出生一孩 WITH  地区生育孩次.总HJB1/10000,地区概要.政策二孩 WITH  地区生育孩次.总HJB2/10000,;
		地区城镇概要.出生一孩 WITH  (地区生育孩次.城HJB1)/10000,地区城镇概要.政策二孩 WITH  地区生育孩次.城HJB2/10000,;
		地区农村概要.出生一孩 WITH  (地区生育孩次.乡HJB1)/10000,地区农村概要.政策二孩 WITH  地区生育孩次.乡HJB2/10000,;
		地区非农概要.出生一孩 WITH  (地区生育孩次.总FNB1)/10000,地区非农概要.政策二孩 WITH  地区生育孩次.总FNB2/10000,;
		地区农业概要.出生一孩 WITH  (地区生育孩次.总NYB1)/10000,地区农业概要.政策二孩 WITH  地区生育孩次.总NYB2/10000,;
		地区概要.出生二孩 	  WITH 地区概要.政策二孩+地区概要.超生数,地区城镇概要.出生二孩 		   WITH 地区城镇概要.政策二孩+地区城镇概要.超生数,;
		地区农村概要.出生二孩 WITH 地区农村概要.政策二孩+地区农村概要.超生数,地区非农概要.出生二孩 WITH 地区非农概要.政策二孩+地区非农概要.超生数,地区农业概要.出生二孩 WITH 地区农业概要.政策二孩+地区农业概要.超生数 ALL
ENDIF
RETURN 

*********************************************
PROCEDURE 年度全国概要
FOR CX=1 TO 4
	SELE IIF(CX=1,161,IIF(CX=2,162,IIF(CX=3,163,164)))
	***********************
	DO 各类婚配夫妇分年龄生育率
	***********************
	SUM  合J分龄SYL,双D分龄SYL,单D分龄SYL,双N分龄SYL TO STFR,DDTFR, 单DTFR,NNTFR
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非) TO 婚内TFR	FOR (双独+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非)<>0								&&所有婚配模式合计的分年龄生育率
	SUM 单独释放B TO 单放B2
	SUM 双非释放B TO 非放B2
	DO CASE		&&记录堆积释放结果
	CASE CX=1	 &&SELE 61 :地区农村农业子女
	  	农村农业单放B2=单放B2
	  	农村农业非放B2=非放B2
	 	NCNY婚内TFR=婚内TFR
	 	SUM 单独堆积,双非堆积 TO 农村农业单独堆积,农村农业双非堆积  FOR X>=15.AND.X<=49
	CASE CX=2	 &&SELE62 :地区农村非农子女
	  	农村非农单放B2=单放B2
	  	农村非农非放B2=非放B2
	  	NCFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 农村非农单独堆积,农村非农双非堆积 FOR X>=15.AND.X<=49
	CASE CX=3	 &&SELE63 :地区城镇农业子女
	  	城镇农业单放B2=单放B2
	  	城镇农业非放B2=非放B2
	  	CZNY婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇农业单独堆积,城镇农业双非堆积 FOR X>=15.AND.X<=49
	CASE CX=4	 &&SELE64 :地区城镇非农子女
	  	城镇非农单放B2=单放B2
	  	城镇非农非放B2=非放B2
	  	CZFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇非农单独堆积,城镇非农双非堆积 FOR X>=15.AND.X<=49
	ENDCASE
ENDFOR
***********************
SELECT A
RELEASE ARFND1
COPY TO ARRAY ARFND1 FIELDS 全国分龄.&FND1
REPLACE 全国超生生育率.&FND2 WITH 全国分龄超生.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM (全国生育.&BND2+全国分龄超生.&BND2)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0

SUM (全国分龄超生.&BND2)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区超生TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM 全国分龄政策生育.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM 全国分龄政策生育.&BND2,全国农村非农子女.政策生育,全国城镇非农子女.政策生育,全国农村农业子女.政策生育,全国城镇农业子女.政策生育 TO DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育


COPY TO ARRAY ARFND1 FIELDS 全国城镇分龄.&FND1
SUM 全国城镇生育.&BND2/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) TO 城镇实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国城镇分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国农村分龄.&FND1
SUM 全国农村生育.&BND2/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2) TO 农村实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国农村分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国非农分龄.&FND1
SUM 全国非农生育.&BND2/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)  TO 非农实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国非农分龄.&FND2<>0

COPY TO ARRAY ARFND1 FIELDS 全国农业分龄.&FND1
SUM 全国农业生育.&BND2/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) TO 农业实现TFR0 FOR RECNO()>1.AND.ARFND1(RECNO()-1)+全国农业分龄.&FND2<>0

SUM 全国分龄超生.&BND2 TO 超生S

*********************************************************************
DO 全国多种生育率
DO 记录全国各类夫妇政策内容及政策生育率	
SELECT 402
APPEND BLANK 
REPLACE 地区 WITH '全国',年份 WITH ND0,政策方案 WITH fa-ja,可实现TFR WITH 地区实现TFR0,政策生育率 WITH 地区政策TFR0,超生生育率 WITH 地区超生TFR,超生孩子数 WITH 超生S
REPLACE	DQ时期TFR  WITH 地区政策TFR0,CZ时期TFR WITH 城镇政策TFR0,FN时期TFR WITH 非农政策TFR0 ,NC时期TFR  WITH 农村政策TFR0,NY时期TFR  WITH 农业政策TFR0,QG累计迁移  WITH SQY
全国实现TFR0=可实现TFR
*?'全国'-IIF(LEN('全国')=4,'  ',''),',',STR(ND0,4),',',fa-ja,',',STR(地区实现TFR0,6,2),',',STR(地区政策TFR0,6,2),',',STR(地区超生TFR,6,2),',',STR(超生s,6,2),',',;
'全国城镇:',STR(城镇实现TFR0,6,2),'全国非农:',STR(非农实现TFR0,6,2),'全国农村:',STR(农村实现TFR0,6,2),'全国农业:',STR(农业实现TFR0,6,2)
*************************
*	全国预测结果摘要	*
*************************
SELECT A
	SUM (全国城镇分龄.&MND2+全国城镇分龄.&FND2)/10000,(全国农村分龄.&MND2+全国农村分龄.&FND2)/10000 TO CZRK,NCRK
	CSH水平=CZRK/(NCRK+CZRK)*100
	迁移累计=0
QGQY=0
FOR CX=1 TO 5
	城乡B=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
	城乡QY=IIF(CX=1,'',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))-'QYT'
	分龄预测='全国'-城乡B-'分龄'
	迁移人口='全国'-城乡B-IIF(CX=3.OR.CX=5,'子女','迁移')
	城乡子女='全国'-城乡B-'子女'
	全国生育='全国'-城乡B-'生育'
	全国人口='全国'-城乡B-'分龄'
	城乡死亡='全国'-城乡B-'死亡'
	全国迁移='全国'-城乡B-'迁移'
	SELE A
    SUM (&全国生育..&BX+&城乡子女..超生生育)/10000 TO 出生人口
    COPY TO ARRAY ARFND1 FIELDS  &全国人口..&FND1
    SUM &全国生育..&BX/((ARFND1(RECNO()-1)+&全国人口..&FND2)/2) TO 地区政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+&全国人口..&FND2)<>0
    SUM (&全国生育..&BX+&城乡子女..超生生育)/((ARFND1(RECNO()-1)+&全国人口..&FND2)/2) TO 可实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+&全国人口..&FND2)<>0
	DO CASE
	CASE CX=1
		地区实现TFR0=可实现TFR0
	CASE CX=2
		城镇实现TFR0=可实现TFR0
	CASE CX=3
		非农实现TFR0=可实现TFR0
	CASE CX=4
		农村实现TFR0=可实现TFR0
	CASE CX=5
		农业实现TFR0=可实现TFR0
	ENDCASE
	IF CX<=2.OR.CX=4
		SUM (&全国迁移..&MND2+&全国迁移..&FND2)/10000 TO 净迁入RK
		SUM &全国迁移..&MND2/10000 TO 净迁入M
		SUM &全国迁移..&FND2/10000 TO 净迁入F
	    SUM &城乡死亡..&MND2/10000 TO 死亡人口M
	    SUM &城乡死亡..&FND2/10000 TO 死亡人口F
		DO 死亡人口和预期寿命计算
  		*DO 计算预期寿命
  		DO CASE 
  		CASE CX=2.OR.CX=3
  			CEM0=EM0
  			CEF0=EF0
  		CASE CX=4.OR.CX=5
  			XEM0=EM0
  			XEF0=EF0
  		ENDCASE
  	ELSE
		SELE A
		SUM &迁移人口..QYHJ/10000 TO 净迁入RK
		SUM &迁移人口..QYHJM/10000 TO 净迁入M
		SUM &迁移人口..QYHJF/10000 TO 净迁入F
		死亡人口M=0
		死亡人口F=0
  	ENDIF
	QGQY=QGQY+净迁入RK
  	FIE1=IIF(CX=1,'DQ迁移合计',ALLTRIM(城乡B)-'迁移')
  	SELECT 402
  	REPLACE &FIE1 WITH 净迁入RK
 * 	??',',STR(净迁入RK,6,2),TIME()
	SELE IIF(CX=1,101,IIF(CX=2,102,IIF(CX=3,103,IIF(CX=4,104,105)))) &&101 全国分龄，102 全国城镇分龄，103 全国非农分龄，104 全国农村分龄，105 全国农业分龄
  		EM0=IIF(CX=2.OR.CX=3,CEM0,IIF(CX=4.OR.CX=5,XEM0,EM0))
  		EF0=IIF(CX=2.OR.CX=3,CEF0,IIF(CX=4.OR.CX=5,XEF0,EF0))
		DO 年龄分组和年龄结构
	SELE IIF(CX=1,181,IIF(CX=2,182,IIF(CX=3,183,IIF(CX=4,184,185)))) &&SELE 181 全国概要；SELE 182 全国城镇概要;SELE 183 全国非农概要;SELE 184 全国农村概要；SELE 185  全国农业概要
		DQB=IIF(CX=1,'全国',IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业'))))
		SEL=STR(SELECT(),4)
		DO 预测结果摘要
	IF ND0=ND2
		DO 主要极值
	ENDIF
ENDFOR	&&全国分城乡\农非概要循环
IF ND0=ND2
	SELECT 151
	REPLACE 全国概要.出生一孩 WITH  全国生育孩次.总HJB1/10000,全国概要.政策二孩 WITH  全国生育孩次.总HJB2/10000,;
		全国城镇概要.出生一孩 WITH  (全国生育孩次.城HJB1)/10000,全国城镇概要.政策二孩 WITH  全国生育孩次.城HJB2/10000,;
		全国农村概要.出生一孩 WITH  (全国生育孩次.乡HJB1)/10000,全国农村概要.政策二孩 WITH  全国生育孩次.乡HJB2/10000,;
		全国非农概要.出生一孩 WITH  (全国生育孩次.总FNB1)/10000,全国非农概要.政策二孩 WITH  全国生育孩次.总FNB2/10000,;
		全国农业概要.出生一孩 WITH  (全国生育孩次.总NYB1)/10000,全国农业概要.政策二孩 WITH  全国生育孩次.总NYB2/10000,;
		全国概要.出生二孩 	  WITH 全国概要.政策二孩+全国概要.超生数,全国城镇概要.出生二孩 		   WITH 全国城镇概要.政策二孩+全国城镇概要.超生数,;
		全国农村概要.出生二孩 WITH 全国农村概要.政策二孩+全国农村概要.超生数,全国非农概要.出生二孩 WITH 全国非农概要.政策二孩+全国非农概要.超生数,全国农业概要.出生二孩 WITH 全国农业概要.政策二孩+全国农业概要.超生数 ALL
ENDIF
RETURN 

*******************************************
*******************************************
PROCEDURE 全国多种生育率
*******************************************************************************************************************
*	说明:NFDMB=政策NFDMB+超生NFDMB,NMDFB=政策NMDFB+超生NMDFB。NFDMB,NMDFB即为实现生育数，以此计算的TFR即实现生育率
*	超生生育+政策生育=(双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B),即实现生育
*******************************************************************************************************************
*农村农业TFR,农村非农TFR,城镇农业TFR,城镇非农TFR即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 全国农村农业分龄.&FND1
SUM  (全国农村农业子女.双独B+全国农村农业子女.NMDFB+全国农村农业子女.NFDMB+全国农村农业子女.NNB+全国农村农业子女.单独释放B+全国农村农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国农村农业分龄.&FND2)/2) ;
	 TO 农村农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农村农业分龄.&FND2)/2)<>0		&&农村农业生育率
COPY TO ARRAY ARFND1 FIELDS 全国农村非农分龄.&FND1
SUM  (全国农村非农子女.双独B+全国农村非农子女.NMDFB+全国农村非农子女.NFDMB+全国农村非农子女.NNB+全国农村非农子女.单独释放B+全国农村非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国农村非农分龄.&FND2)/2) ;
	 TO 农村非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农村非农分龄.&FND2)/2)<>0		&&农村非农生育率
COPY TO ARRAY ARFND1 FIELDS 全国城镇农业分龄.&FND1
SUM  (全国城镇农业子女.双独B+全国城镇农业子女.NMDFB+全国城镇农业子女.NFDMB+全国城镇农业子女.NNB+全国城镇农业子女.单独释放B+全国城镇农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国城镇农业分龄.&FND2)/2) ;
	 TO 城镇农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国城镇农业分龄.&FND2)/2)<>0		&&城镇农业生育率
COPY TO ARRAY ARFND1 FIELDS 全国城镇非农分龄.&FND1
SUM  (全国城镇非农子女.双独B+全国城镇非农子女.NMDFB+全国城镇非农子女.NFDMB+全国城镇非农子女.NNB+全国城镇非农子女.单独释放B+全国城镇非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国城镇非农分龄.&FND2)/2) ;
	 TO 城镇非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国城镇非农分龄.&FND2)/2)<>0		&&城镇非农生育率

*农业政策TFR,农业实现TFR0,农业实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 全国农业分龄.&FND1
SUM  (全国农业子女.政策生育)/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) TO 农业政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2)<>0			&&全国农业政策生育率
SUM  (全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) ;
	 TO 农业实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2)<>0		&&农业实现生育率
SUM (全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)/全国农业子女.夫妇合计 ;
	 TO 农业实婚TFR0 FOR 全国农业子女.夫妇合计<>0.AND.(全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)>0

*全国非农TFR,非农实现TFR0,非农实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 全国非农分龄.&FND1
SUM  (全国非农子女.政策生育)/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2) TO 非农政策TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)<>0			&&全国非农政策生育率
SUM  (全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2) ;
	 TO 非农实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)<>0		&&非农实现生育率
SUM  (全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)/(全国非农子女.夫妇合计) ;
	 TO 非农实婚TFR0 FOR 全国非农子女.夫妇合计<>0.AND.(全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)>0		&&非农实婚生育率

*农村TFR0,农村实现TFR0,农村实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 全国农村分龄.&FND1
SUM  全国农村子女.政策生育/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2)  TO 农村政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2<>0			&&农村政策生育率
SUM  (全国农村子女.双独B+全国农村子女.NMDFB+全国农村子女.NFDMB+全国农村子女.NNB+全国农村子女.单独释放B+全国农村子女.双非释放B)/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2) TO 农村实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2<>0		&&农村可实现TFR
SUM  (全国农村子女.双独B+全国农村子女.NMDFB+全国农村子女.NFDMB+全国农村子女.NNB+全国农村子女.单独释放B+全国农村子女.双非释放B)/全国农村子女.夫妇合计  TO 农村实婚TFR0 FOR 全国农村子女.夫妇合计<>0.AND.全国农村生育.&BND2>0

*城镇TFR0,城镇实现TFR0,城镇实婚TFR0即可实现生育率
COPY TO ARRAY ARFND1 FIELDS 全国城镇分龄.&FND1
SUM  全国城镇子女.政策生育/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) TO 城镇政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2<>0			&&城镇政策生育率
SUM  (全国城镇子女.双独B+全国城镇子女.NMDFB+全国城镇子女.NFDMB+全国城镇子女.NNB+全国城镇子女.单独释放B+全国城镇子女.双非释放B)/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) TO 城镇实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2<>0		&&城镇实现生育率
SUM  (全国城镇子女.双独B+全国城镇子女.NMDFB+全国城镇子女.NFDMB+全国城镇子女.NNB+全国城镇子女.单独释放B+全国城镇子女.双非释放B)/(全国城镇农业子女.夫妇合计+全国城镇非农子女.夫妇合计) ;
	 TO 城镇实婚TFR0 FOR 全国城镇农业子女.夫妇合计+全国城镇非农子女.夫妇合计<>0.AND.(全国城镇生育.&BND2)>0		&&城镇实婚生育率

*全国政策TFR,全国实现TFR0,全国实婚TFR0
COPY TO ARRAY ARFND1 FIELDS 全国分龄.&FND1
SUM  全国分龄政策生育.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 全国政策TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)/2<>0			&&全国分龄政策生育率
SUM  (全国子女.双独B+全国子女.NMDFB+全国子女.NFDMB+全国子女.NNB+全国子女.单独释放B+全国子女.双非释放B)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)/2<>0			&&地s区可实现TFR
SUM  (全国子女.双独B+全国子女.NMDFB+全国子女.NFDMB+全国子女.NNB+全国子女.单独释放B+全国子女.双非释放B)/全国子女.夫妇合计  TO 地区实婚TFR0 FOR (全国子女.夫妇合计)<>0.AND.全国生育.&BND2>0

SUM 全国分龄超生.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) TO 地区超生TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)>0
SUM 全国城镇分龄.&MND2+全国城镇分龄.&FND2,全国农村分龄.&MND2+全国农村分龄.&FND2 TO CRK,XRK
CSH水平=CRK/(CRK+XRK)*100
SUM 全国非农分龄.&MND2+全国非农分龄.&FND2,全国农业分龄.&MND2+全国农业分龄.&FND2 TO FNRK,NYRK

SELE 165 &&全国农村子女�
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农村婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 166 &&全国城镇子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 城镇婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELE 167	&&全国农业子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 农业婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 168	&&全国非农子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 非农婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
SELECT 169	&&全国子女
	DO 各类婚配夫妇分年龄生育率
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积)  TO 地区婚内TFR FOR (双独+ NMDF+ NFDM+ NN+ 单独堆积+ 双非堆积)<>0
RETURN 

*******************************************
PROCEDURE 全国多种生育率0
	*****全国多种生育率*****
	COPY TO ARRAY ARFND1 FIELDS 全国农村农业分龄.&FND1
	SUM  (全国农村农业子女.双独B+全国农村农业子女.NMDFB+全国农村农业子女.NFDMB+全国农村农业子女.NNB+全国农村农业子女.单独释放B+全国农村农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国农村农业分龄.&FND2)/2) ;
		 TO 农村农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农村农业分龄.&FND2)/2)<>0		&&农村农业生育率
	COPY TO ARRAY ARFND1 FIELDS 全国农村非农分龄.&FND1
	SUM  (全国农村非农子女.双独B+全国农村非农子女.NMDFB+全国农村非农子女.NFDMB+全国农村非农子女.NNB+全国农村非农子女.单独释放B+全国农村非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国农村非农分龄.&FND2)/2) ;
		 TO 农村非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农村非农分龄.&FND2)/2)<>0		&&农村非农生育率
	COPY TO ARRAY ARFND1 FIELDS 全国城镇农业分龄.&FND1
	SUM  (全国城镇农业子女.双独B+全国城镇农业子女.NMDFB+全国城镇农业子女.NFDMB+全国城镇农业子女.NNB+全国城镇农业子女.单独释放B+全国城镇农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国城镇农业分龄.&FND2)/2) ;
		 TO 城镇农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国城镇农业分龄.&FND2)/2)<>0		&&城镇农业生育率
	COPY TO ARRAY ARFND1 FIELDS 全国城镇非农分龄.&FND1
	SUM  (全国城镇非农子女.双独B+全国城镇非农子女.NMDFB+全国城镇非农子女.NFDMB+全国城镇非农子女.NNB+全国城镇非农子女.单独释放B+全国城镇非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国城镇非农分龄.&FND2)/2) ;
		 TO 城镇非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国城镇非农分龄.&FND2)/2)<>0		&&城镇非农生育率
	
	COPY TO ARRAY ARFND1 FIELDS 全国农业分龄.&FND1
	SUM  (全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) ;
		 TO 地区农业TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2)<>0		&&全国农业政策生育率
	SUM  (全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)/((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2) ;
		 TO 农业实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国农业分龄.&FND2)/2)<>0		&&农业实现生育率
	SUM (全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)/全国农业子女.夫妇合计 ;
		 TO 农业实婚TFR0 FOR 全国农业子女.夫妇合计<>0.AND.(全国农业子女.双独B+全国农业子女.NMDFB+全国农业子女.NFDMB+全国农业子女.NNB+全国农业子女.单独释放B+全国农业子女.双非释放B)>0
	
	COPY TO ARRAY ARFND1 FIELDS 全国非农分龄.&FND1
	SUM  (全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2) ;
		 TO 地区非农TFR FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)<>0			&&全国非农政策生育率
	SUM  (全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)/((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2) ;
		 TO 非农实现TFR0 FOR RECNO()>1.AND.((ARFND1(RECNO()-1)+全国非农分龄.&FND2)/2)<>0		&&非农实现生育率
	SUM  (全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)/(全国非农子女.夫妇合计) ;
		 TO 非农实婚TFR0 FOR 全国非农子女.夫妇合计<>0.AND.(全国非农子女.双独B+全国非农子女.NMDFB+全国非农子女.NFDMB+全国非农子女.NNB+全国非农子女.单独释放B+全国非农子女.双非释放B)>0		&&非农实婚生育率
	
	COPY TO ARRAY ARFND1 FIELDS 全国农村分龄.&FND1
	SUM  全国农村生育.&BND2/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2);
		 TO 农村TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2<>0			&&农村政策生育率
	SUM  (全国农村生育.&BND2)/((ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2);
		 TO 农村实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国农村分龄.&FND2)/2<>0		&&农村可实现TFR
	SUM (全国农村生育.&BND2)/全国农村子女.夫妇合计  TO 农村实婚TFR0 FOR (全国农村子女.夫妇合计)<>0.AND.全国农村生育.&BND2>0

	COPY TO ARRAY ARFND1 FIELDS 全国城镇分龄.&FND1
	SUM  全国城镇生育.&BND2/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) ;
		 TO 城镇TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2<>0			&&城镇政策生育率
	SUM  (全国城镇生育.&BND2)/((ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2) ;
		 TO 城镇实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国城镇分龄.&FND2)/2<>0		&&城镇实现生育率
	SUM  (全国城镇生育.&BND2)/(全国城镇农业子女.夫妇合计+全国城镇非农子女.夫妇合计) ;
		 TO 城镇实婚TFR0 FOR 全国城镇农业子女.夫妇合计+全国城镇非农子女.夫妇合计<>0.AND.(全国城镇生育.&BND2)>0		&&城镇实婚生育率
	COPY TO ARRAY ARFND1 FIELDS 全国分龄.&FND1
	SUM  全国生育.&BND2/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) ;
		 TO 地区政策TFR FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)/2<>0			&&全国政策生育率
	SUM  (全国分龄超生.&BND2+全国生育.&BND2)/((ARFND1(RECNO()-1)+全国分龄.&FND2)/2) ;
		 TO 地区实现TFR0 FOR RECNO()>1.AND.(ARFND1(RECNO()-1)+全国分龄.&FND2)/2<>0			&&全国可实现TFR
	SUM (全国分龄超生.&BND2+全国生育.&BND2)/全国子女.夫妇合计  TO 地区实婚TFR0 FOR (全国子女.夫妇合计)<>0.AND.全国分龄超生.&BND2+全国生育.&BND2>0
	SUM 全国城镇分龄.&MND2+全国城镇分龄.&FND2,全国农村分龄.&MND2+全国农村分龄.&FND2 TO CRK,XRK
	
RETURN 


*******************************
*******************************
PROCEDURE 全国迁移平衡
STORE 0 TO 全国QY
FOR DQX=DQ1 TO DQ2	
*	DQX=IIF(dqx=12,DQ2,dqx)
	STORE 0 TO SQ入0M,SQ出0M,SQ入0F,SQ出0F,SDQQY
    DQB=ALLT(ARDQ多种参数(DQX,2))	&&省代码,DQ,出生XBB05,出生XBB00
    DQ夹=ALLT(ARDQ多种参数(DQX,1))-DQB
 	地区农村农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\农业\分龄婚配\\'-DQB-'农村农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区农村非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\农村\\非农\分龄婚配\\'-DQB-'农村非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇农业子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\农业\分龄婚配\\'-DQB-'城镇农业子女婚配预测表'-'_'-FA2-'_'-JA2
 	地区城镇非农子女婚配预测表='F:\分省生育政策仿真_基于普查\分省政策仿真\&实现方式\&DQ夹\城镇\\非农\分龄婚配\\'-DQB-'城镇非农子女婚配预测表'-'_'-FA2-'_'-JA2
 	
	SELE 113
		USE AAA全国迁移平衡表结构  ALIAS 迁移平衡表
	   	INDEX ON X TO IN113
	SELE 61 
 	 	USE &地区农村农业子女婚配预测表 ALIAS  地区农村农业子女		&&农村婚配
   		INDEX ON X TO IN61
 	SELE 62 
 	 	USE &地区农村非农子女婚配预测表 ALIAS  地区农村非农子女	&&农村婚配
    	INDEX ON X TO IN62
 	SELE 63 
 	 	USE &地区城镇农业子女婚配预测表 ALIAS  地区城镇农业子女		&&城镇婚配
    	INDEX ON X TO IN63
 	SELE 64 
 	 	USE &地区城镇非农子女婚配预测表 ALIAS  地区城镇非农子女	&&城镇非农子女
   		INDEX ON X TO IN64
	SELECT 61
	SET RELA TO X INTO 62,X INTO 63,X INTO 64,X INTO 113
	*****************
	*	迁移平衡	*
 	*****************
	FOR CX=1 TO 2
		城乡=IIF(CX=1,'城镇','农村')
		QYGL=IIF(CX=1,'CQYGL','XQYGL')
		FOR NFX=1 TO 2
			农非=IIF(NFX=1,'农业','非农')
			ALIA1='地区'-城乡-农非-'子女'
			FOR ZN=1 TO 3
				ZNB=IIF(ZN=1,'D',IIF(ZN=2,'DDB','N'))
				FOR XBX=1 TO 2
					XB=IIF(XBX=1,'M','F')
					子女=ZNB-XB
					子女QY=ZNB-XB-'QY'
					迁移平衡=IIF(XBX=1,'迁移平衡M','迁移平衡F')
					迁入=IIF(XBX=1,'迁入M','迁入F')
					迁出=IIF(XBX=1,'迁出M','迁出F')
					平衡迁入=IIF(XBX=1,'平衡迁入M','平衡迁入F')
					平衡迁出=IIF(XBX=1,'平衡迁出M','平衡迁出F')
					迁入和0=IIF(XBX=1,迁入和M0,迁入和F0)
					迁出和0=IIF(XBX=1,迁出和M0,迁出和F0)
					REPLACE &ALIA1..&子女QY WITH 迁移平衡表.&迁移平衡*&ALIA1..&子女QY/迁移平衡表.&迁入 FOR &ALIA1..&子女QY>0.and.迁移平衡表.&迁入<>0
					REPLACE 迁移平衡表.&平衡迁入 WITH 迁移平衡表.&平衡迁入+&ALIA1..&子女QY FOR &ALIA1..&子女QY>0
					REPLACE &ALIA1..&子女QY WITH -(MIN(迁移平衡表.&迁移平衡*&ALIA1..&子女QY/迁移平衡表.&迁出,&ALIA1..&子女)) FOR &ALIA1..&子女QY<0.and.迁移平衡表.&迁出<>0
					REPLACE 迁移平衡表.&平衡迁出 WITH 迁移平衡表.&平衡迁出+&ALIA1..&子女QY FOR &ALIA1..&子女QY<0
					SUM &ALIA1..&子女QY TO DQQY
					SDQQY=SDQQY+ DQQY	  
				ENDFOR
			ENDFOR
		 	REPLACE &ALIA1..DQY 	WITH &ALIA1..DFQY+&ALIA1..DMQY,&ALIA1..NQY WITH &ALIA1..NFQY+&ALIA1..NMQY,;
		 			&ALIA1..DDBQY 	WITH &ALIA1..DDBMQY+&ALIA1..DDBFQY,&ALIA1..QYHJF WITH &ALIA1..DFQY+&ALIA1..NFQY+&ALIA1..DDBFQY,;
		 			&ALIA1..QYHJM 	WITH &ALIA1..DMQY+&ALIA1..NMQY+&ALIA1..DDBMQY,&ALIA1..QYHJ WITH &ALIA1..QYHJF+&ALIA1..QYHJM ALL
		ENDFOR
	ENDFOR
	SUM &ALIA1..QYHJ,&ALIA1..HJ TO SQY,SHJ
	DQJQL(DQX)=SQY/SHJ*100
*	?DQB,SDQQY
*	全国QY=全国QY+SDQQY
ENDFOR
*SUM 迁移平衡表.平衡迁入M,迁移平衡表.平衡迁出M,迁移平衡表.平衡迁入F,迁移平衡表.平衡迁出F TO 平衡迁入M,平衡迁出M,平衡迁入F,平衡迁出F
*?平衡迁入M,平衡迁出M,平衡迁入F,平衡迁出F
*?平衡迁入M-ABS(平衡迁出M),平衡迁入F-ABS(平衡迁出F)
??全国QY
RETURN 





**********************************
PROCEDURE 各类迁移
SELECT A
FOR SE=61 TO 69
	SELECT IIF(SE=SE,SE,1)
 	REPLACE DQY WITH DFQY+DMQY,NQY WITH NFQY+NMQY,DDBQY WITH DDBMQY+DDBFQY,QYHJF WITH DFQY+NFQY+DDBFQY,QYHJM WITH DMQY+NMQY+DDBMQY,QYHJ WITH QYHJF+QYHJM ALL
ENDFOR
SELECT A
REPL 地区城镇迁移.&MND2 WITH 地区城镇非农子女.DMQY+地区城镇非农子女.NMQY+地区城镇非农子女.DDBMQY+地区城镇农业子女.DMQY+地区城镇农业子女.NMQY+地区城镇农业子女.DDBMQY,;
	 地区城镇迁移.&FND2 WITH 地区城镇非农子女.DFQY+地区城镇非农子女.NFQY+地区城镇非农子女.DDBFQY+地区城镇农业子女.DFQY+地区城镇农业子女.NFQY+地区城镇农业子女.DDBFQY ALL
REPL 地区农村迁移.&MND2 WITH 地区农村非农子女.DMQY+地区农村非农子女.NMQY+地区农村非农子女.DDBMQY+地区农村农业子女.DMQY+地区农村农业子女.NMQY+地区农村农业子女.DDBMQY,;
	 地区农村迁移.&FND2 WITH 地区农村非农子女.DFQY+地区农村非农子女.NFQY+地区农村非农子女.DDBFQY+地区农村农业子女.DFQY+地区农村农业子女.NFQY+地区农村农业子女.DDBFQY ALL
REPL 地区迁移.&MND2 WITH 地区城镇迁移.&MND2+地区农村迁移.&MND2,地区迁移.&FND2 WITH 地区城镇迁移.&FND2+地区农村迁移.&FND2 ALL
RETURN 
**********************************

*******************************************
PROCEDURE 	各类夫妇生育孩子数  &&可实现生育孩子数	&&
SELECT A
REPL 地区非农子女.双独B WITH 地区农村非农子女.双独B+地区城镇非农子女.双独B,地区非农子女.NMDFB WITH 地区农村非农子女.NMDFB+地区城镇非农子女.NMDFB ALL
REPL 地区非农子女.NFDMB WITH 地区农村非农子女.NFDMB+地区城镇非农子女.NFDMB,地区非农子女.NNB WITH 地区农村非农子女.NNB+地区城镇非农子女.NNB ALL
REPL 地区农业子女.双独B WITH 地区农村农业子女.双独B+地区城镇农业子女.双独B,地区农业子女.NMDFB WITH 地区农村农业子女.NMDFB+地区城镇农业子女.NMDFB ALL
REPL 地区农业子女.NFDMB WITH 地区农村农业子女.NFDMB+地区城镇农业子女.NFDMB,地区农业子女.NNB WITH 地区农村农业子女.NNB+地区城镇农业子女.NNB ALL

REPL 地区非农子女.单独释放B WITH 地区城镇非农子女.单独释放B+地区农村非农子女.单独释放B ALL
REPL 地区非农子女.双非释放B WITH 地区城镇非农子女.双非释放B+地区农村非农子女.双非释放B ALL
REPL 地区农业子女.单独释放B WITH 地区城镇农业子女.单独释放B+地区农村农业子女.单独释放B ALL
REPL 地区农业子女.双非释放B WITH 地区城镇农业子女.双非释放B+地区农村农业子女.双非释放B ALL

REPL 地区非农生育.&BND2 WITH (地区农村非农子女.双独B+地区农村非农子女.NMDFB+地区农村非农子女.NFDMB+地区农村非农子女.NNB)+(地区农村非农子女.单独释放B+地区农村非农子女.双非释放B)+;
	 (地区城镇非农子女.双独B+地区城镇非农子女.NMDFB+地区城镇非农子女.NFDMB+地区城镇非农子女.NNB)+(地区城镇非农子女.单独释放B+地区城镇非农子女.双非释放B) ALL
REPL 地区农业生育.&BND2 WITH (地区农村农业子女.双独B+地区农村农业子女.NMDFB+地区农村农业子女.NFDMB+地区农村农业子女.NNB)+(地区农村农业子女.单独释放B+地区农村农业子女.双非释放B)+;
	 (地区城镇农业子女.双独B+地区城镇农业子女.NMDFB+地区城镇农业子女.NFDMB+地区城镇农业子女.NNB)+(地区城镇农业子女.单独释放B+地区城镇农业子女.双非释放B) ALL
REPL 地区农村生育.&BND2 WITH  (地区农村农业子女.双独B+地区农村农业子女.NMDFB+地区农村农业子女.NFDMB+地区农村农业子女.NNB)+;
	 (地区农村非农子女.双独B+地区农村非农子女.NMDFB+地区农村非农子女.NFDMB+地区农村非农子女.NNB)+(地区农村农业子女.单独释放B+地区农村农业子女.双非释放B+地区农村非农子女.单独释放B+地区农村非农子女.双非释放B) ALL
REPL 地区城镇生育.&BND2 WITH  (地区城镇农业子女.双独B+地区城镇农业子女.NMDFB+地区城镇农业子女.NFDMB+地区城镇农业子女.NNB)+;
	 (地区城镇非农子女.双独B+地区城镇非农子女.NMDFB+地区城镇非农子女.NFDMB+地区城镇非农子女.NNB)+(地区城镇农业子女.单独释放B+地区城镇农业子女.双非释放B+地区城镇非农子女.单独释放B+地区城镇非农子女.双非释放B) ALL
REPL 地区生育.&BND2 WITH 地区农村生育.&BND2+地区城镇生育.&BND2 ALL
RETURN 




*******************
PROCEDURE 分龄人口合计
SELECT A
REPL 地区农村农业子女.D    WITH 地区农村农业子女.DF+地区农村农业子女.DM,地区农村农业子女.N    WITH 地区农村农业子女.NF+地区农村农业子女.NM ALL
REPL 地区农村非农子女.D    WITH 地区农村非农子女.DF+地区农村非农子女.DM,地区农村非农子女.N    WITH 地区农村非农子女.NF+地区农村非农子女.NM ALL
REPL 地区城镇农业子女.D    WITH 地区城镇农业子女.DF+地区城镇农业子女.DM,地区城镇农业子女.N    WITH 地区城镇农业子女.NF+地区城镇农业子女.NM ALL
REPL 地区城镇非农子女.D    WITH 地区城镇非农子女.DF+地区城镇非农子女.DM,地区城镇非农子女.N    WITH 地区城镇非农子女.NF+地区城镇非农子女.NM ALL
*REPL 地区农村非农子女.HJM WITH 地区农村非农子女.DM+地区农村非农子女.DDBM+地区农村非农子女.NM+地区农村非农子女.堆积丈夫,;
	 地区农村非农子女.HJF WITH 地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF+地区农村非农子女.单独堆积+地区农村非农子女.双非堆积+地区农村非农子女.已2单独+地区农村非农子女.已2双非,;
	 地区农村非农子女.HJ WITH 地区农村非农子女.HJM+地区农村非农子女.HJF ALL
REPL 地区农村非农子女.HJM WITH 地区农村非农子女.DM+地区农村非农子女.DDBM+地区农村非农子女.NM,;
	 地区农村非农子女.HJF WITH 地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF,;
	 地区农村非农子女.HJ WITH 地区农村非农子女.HJM+地区农村非农子女.HJF ALL

*REPL 地区农村农业子女.HJM WITH 地区农村农业子女.DM+地区农村农业子女.DDBM+地区农村农业子女.NM+地区农村农业子女.堆积丈夫,;
	 地区农村农业子女.HJF WITH 地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF+地区农村农业子女.单独堆积+地区农村农业子女.双非堆积+地区农村农业子女.已2单独+地区农村农业子女.已2双非,;
	 地区农村农业子女.HJ WITH 地区农村农业子女.HJM+地区农村农业子女.HJF ALL
REPL 地区农村农业子女.HJM WITH 地区农村农业子女.DM+地区农村农业子女.DDBM+地区农村农业子女.NM,;
	 地区农村农业子女.HJF WITH 地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF,;
	 地区农村农业子女.HJ WITH 地区农村农业子女.HJM+地区农村农业子女.HJF ALL

*REPL 地区城镇非农子女.HJM WITH 地区城镇非农子女.DM+地区城镇非农子女.DDBM+地区城镇非农子女.NM+地区城镇非农子女.堆积丈夫,;
	 地区城镇非农子女.HJF WITH 地区城镇非农子女.DF+地区城镇非农子女.DDBF+地区城镇非农子女.NF+地区城镇非农子女.单独堆积+地区城镇非农子女.双非堆积+地区城镇非农子女.已2单独+地区城镇非农子女.已2双非,;
	 地区城镇非农子女.HJ WITH 地区城镇非农子女.HJM+地区城镇非农子女.HJF ALL
REPL 地区城镇非农子女.HJM WITH 地区城镇非农子女.DM+地区城镇非农子女.DDBM+地区城镇非农子女.NM,;
	 地区城镇非农子女.HJF WITH 地区城镇非农子女.DF+地区城镇非农子女.DDBF+地区城镇非农子女.NF,;
	 地区城镇非农子女.HJ WITH 地区城镇非农子女.HJM+地区城镇非农子女.HJF ALL

*REPL 地区城镇农业子女.HJM WITH 地区城镇农业子女.DM+地区城镇农业子女.DDBM+地区城镇农业子女.NM+地区城镇农业子女.堆积丈夫,;
	 地区城镇农业子女.HJF WITH 地区城镇农业子女.DF+地区城镇农业子女.DDBF+地区城镇农业子女.NF+地区城镇农业子女.单独堆积+地区城镇农业子女.双非堆积+地区城镇农业子女.已2单独+地区城镇农业子女.已2双非,;
	 地区城镇农业子女.HJ WITH 地区城镇农业子女.HJM+地区城镇农业子女.HJF ALL
REPL 地区城镇农业子女.HJM WITH 地区城镇农业子女.DM+地区城镇农业子女.DDBM+地区城镇农业子女.NM,;
	 地区城镇农业子女.HJF WITH 地区城镇农业子女.DF+地区城镇农业子女.DDBF+地区城镇农业子女.NF,;
	 地区城镇农业子女.HJ WITH 地区城镇农业子女.HJM+地区城镇农业子女.HJF ALL

REPL 地区非农子女.DF  WITH 地区城镇非农子女.DF+地区农村非农子女.DF,地区非农子女.DM  WITH  地区城镇非农子女.DM+地区农村非农子女.DM,地区非农子女.D  WITH  地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.NF  WITH 地区城镇非农子女.NF+地区农村非农子女.NF,地区非农子女.NM  WITH  地区城镇非农子女.NM+地区农村非农子女.NM,地区非农子女.N  WITH  地区非农子女.NM+地区非农子女.NF ALL
REPL 地区非农子女.N   WITH 地区非农子女.NM+地区非农子女.NF,地区非农子女.D WITH 地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.HJF WITH 地区城镇非农子女.HJF+地区农村非农子女.HJF,地区非农子女.HJM  WITH  地区城镇非农子女.HJM+地区农村非农子女.HJM,地区非农子女.HJ WITH  地区非农子女.HJF+地区非农子女.HJM ALL

REPL 地区农业子女.DF  WITH  地区城镇农业子女.DF+地区农村农业子女.DF,地区农业子女.DM  WITH  地区城镇农业子女.DM+地区农村农业子女.DM,地区农业子女.D  WITH  地区农业子女.DM+地区农业子女.DF ALL
REPL 地区农业子女.NF  WITH  地区城镇农业子女.NF+地区农村农业子女.NF,地区农业子女.NM  WITH  地区城镇农业子女.NM+地区农村农业子女.NM,地区农业子女.N  WITH  地区农业子女.NM+地区农业子女.NF ALL
REPL 地区农业子女.HJF WITH  地区城镇农业子女.HJF+地区农村农业子女.HJF,地区农业子女.HJM  WITH  地区城镇农业子女.HJM+地区农村农业子女.HJM,地区农业子女.HJ WITH  地区农业子女.HJF+地区农业子女.HJM ALL

REPL 地区子女.DF  WITH  地区城镇非农子女.DF+地区城镇农业子女.DF+地区农村非农子女.DF+地区农村农业子女.DF,;
	 地区子女.DM  WITH  地区城镇非农子女.DM+地区城镇农业子女.DM+地区农村非农子女.DM+地区农村农业子女.DM,地区子女.D  WITH  地区子女.DM+地区子女.DF ALL
REPL 地区子女.NF  WITH  地区城镇非农子女.NF+地区城镇农业子女.NF+地区农村非农子女.NF+地区农村农业子女.NF,;
 						 地区子女.NM  WITH  地区城镇非农子女.NM+地区城镇农业子女.NM+地区农村非农子女.NM+地区农村农业子女.NM,地区子女.N  WITH  地区子女.NM+地区子女.NF ALL
REPL 地区子女.HJF WITH  地区子女.DF+地区子女.NF,地区子女.HJM  WITH  地区子女.DM+地区子女.NM,地区子女.HJ  WITH  地区子女.HJM+地区子女.HJF ALL

REPL 地区农村非农分龄.&MND2 WITH 地区农村非农子女.HJM,地区农村非农分龄.&FND2 WITH 地区农村非农子女.HJF ALL
REPL 地区城镇非农分龄.&MND2 WITH 地区城镇非农子女.HJM,地区城镇非农分龄.&FND2 WITH 地区城镇非农子女.HJF ALL
REPL 地区非农分龄.&MND2 WITH 地区农村非农分龄.&MND2+地区城镇非农分龄.&MND2,地区非农分龄.&FND2 WITH 地区农村非农分龄.&FND2+地区城镇非农分龄.&FND2 ALL

REPL 地区农村农业分龄.&MND2 WITH 地区农村农业子女.HJM,地区农村农业分龄.&FND2 WITH 地区农村农业子女.HJF ALL
REPL 地区城镇农业分龄.&MND2 WITH 地区城镇农业子女.HJM,地区城镇农业分龄.&FND2 WITH 地区城镇农业子女.HJF ALL
REPL 地区农业分龄.&MND2 WITH 地区农村农业分龄.&MND2+地区城镇农业分龄.&MND2,地区农业分龄.&FND2 WITH 地区农村农业分龄.&FND2+地区城镇农业分龄.&FND2 ALL

REPL 地区农村分龄.&MND2 WITH 地区农村非农分龄.&MND2+地区农村农业分龄.&MND2,地区农村分龄.&FND2 WITH 地区农村非农分龄.&FND2+地区农村农业分龄.&FND2 ALL
REPL 地区城镇分龄.&MND2 WITH 地区城镇非农分龄.&MND2+地区城镇农业分龄.&MND2,地区城镇分龄.&FND2 WITH 地区城镇非农分龄.&FND2+地区城镇农业分龄.&FND2 ALL
REPL 地区分龄.&MND2 WITH 地区城镇分龄.&MND2+地区农村分龄.&MND2,地区分龄.&FND2 WITH 地区城镇分龄.&FND2+地区农村分龄.&FND2 ALL

REPLACE ;
	地区城镇特扶.&MND2 WITH 地区城镇农业特扶.&MND2+地区城镇非农特扶.&MND2,地区农村特扶.&MND2 WITH 地区农村农业特扶.&MND2+地区农村非农特扶.&MND2,;
	地区非农特扶.&MND2 WITH 地区农村非农特扶.&MND2+地区城镇非农特扶.&MND2,地区农业特扶.&MND2 WITH 地区农村农业特扶.&MND2+地区城镇农业特扶.&MND2,地区特扶.&MND2 WITH 地区城镇特扶.&MND2+地区农村特扶.&MND2,;
	地区城镇特扶.&FND2 WITH 地区城镇农业特扶.&FND2+地区城镇非农特扶.&FND2,地区农村特扶.&FND2 WITH 地区农村农业特扶.&FND2+地区农村非农特扶.&FND2,;
	地区非农特扶.&FND2 WITH 地区农村非农特扶.&FND2+地区城镇非农特扶.&FND2,地区农业特扶.&FND2 WITH 地区农村农业特扶.&FND2+地区城镇农业特扶.&FND2,地区特扶.&FND2 WITH 地区城镇特扶.&FND2+地区农村特扶.&FND2 ALL
RETU



*******************************************************************************
PROC 两性平衡多龄概率法
*****************************
*	农村农业与农村农业婚配	*
*	农村非农与农村非农婚配	*
*	城镇农业与城镇农业婚配	*
*	城镇非农与城镇非农婚配	*
*****************************
FOR CX=1 TO 4    &&
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
	DO CASE
	CASE 	结婚率法='全员婚配'
		REPL &子女..DF0 WITH &子女..DF+&子女..DDBF,&子女..DM0 WITH &子女..DM+&子女..DDBM,&子女..NF0 WITH &子女..NF,&子女..NM0 WITH &子女..NM ALL
		REPL &子女..HJF WITH &子女..DF0+&子女..NF0,&子女..HJM WITH &子女..DM0+&子女..NM0,&子女..HJ WITH &子女..HJF+&子女..HJM ALL
	CASE 	结婚率法='有偶率婚配'
		有偶率=IIF(农非=1,'乡村','城镇')-'有偶率'
		REPL &子女..DF0 WITH (&子女..DF+&子女..DDBF)*&有偶率..F有偶B,&子女..DM0 WITH (&子女..DM+&子女..DDBM)*&有偶率..M有偶B,&子女..NF0 WITH &子女..NF*&有偶率..F有偶B,&子女..NM0 WITH &子女..NM*&有偶率..M有偶B ALL
		REPL &子女..HJF0 WITH &子女..DF0+&子女..NF0,&子女..HJM0 WITH &子女..DM0+&子女..NM0 ALL
	ENDCASE
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	&&	此处将“&子女..DF”改为“&子女..DF+&子女..DDBF”，将“&子女..DM”改为“&子女..DM+&子女..DDBM”与删除下句相配合：
	&&	*REPL D WITH D+DDB,DM WITH DM+DDBM,DF WITH DF+DDBF FOR X=0 &&DDB他们之间婚配可以象双独夫妇一样生育二个孩子的人数（只对零岁组操作）
	&&	删除后一个语句，是为了将可以象双独婚配允许生育二个孩子的后代（不一定是独生子女）与本人是独生子女的人数分开，以便于计算“四二一”家庭数量
	&&  修改前一个语句，是可以简化这部份人的婚配计算。如不将“&子女..DF”改为“&子女..DF+&子女..DDBF”，“&子女..DM”改为“&子女..DM+&子女..DDBM”，就不该
	&&	删除“REPL D WITH D+DDB,……”。
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	REPL &子女..DF0 WITH 0 FOR &子女..DF<0
	REPL &子女..DM0 WITH 0 FOR &子女..DM<0
	REPL &子女..NF0 WITH 0 FOR &子女..NF<0
	REPL &子女..NM0 WITH 0 FOR &子女..NM<0
	S负M=-10000
	DS=1
	DO WHILE  S负M<0
		******从女性求夫角度考察的婚配
		REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求夫DF=IIF(DS=1,&子女..DF0,&子女..DD+&子女..NMDF)
			求夫NF=IIF(DS=1,&子女..NF0,&子女..NFDM+&子女..NN)
			夫FB='妻子'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
			SUM &子女..DM0+&子女..NM0 TO SM
			IF SM<>0
				REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0
				REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0
				REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0
				REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0

				SUM 求夫DF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0
				SUM 求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0
				REPLACE &子女..DD_F WITH HDFDM,&子女..NMDF_F WITH HNMDF,&子女..NFDM_F WITH HNFDM,&子女..NN_F  WITH HNN FOR X=NL
			ENDIF
		ENDFOR
		******从男性求妻角度考察的婚配
		REPL 需妻模式.DD妻 WITH 0,需妻模式.NMDF妻 WITH 0,需妻模式.NFDM妻 	WITH 0,需妻模式.NN妻	WITH 0 ALL
		*REPLACE &子女..DD WITH 0,&子女..NMDF WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求妻DM=MIN(&子女..DM0,需夫模式.DD夫+需夫模式.NFDM夫)
			求妻NM=MIN(&子女..NM0,需夫模式.NN夫+需夫模式.NMDF夫)
			妻FB='丈夫'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需妻模式.&妻FB<>0
			SUM &子女..DF0+&子女..NF0 TO SF FOR  X>=MIX1.AND.X<=MAX1
			IF SF<>0
				REPL 需妻模式.DD妻 		WITH 需妻模式.DD妻+求妻DM*(&子女..DF0/(&子女..DF0+&子女..NF0)*需妻模式.&妻FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DF0+&子女..NF0)<>0	&&此处‘需妻模式.DD妻’等按妻子年龄分
				REPL 需妻模式.NMDF妻 	WITH 需妻模式.NMDF妻+求妻DM*(&子女..NF0/(&子女..DF0+&子女..NF0)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DF0+&子女..NF0)<>0
				REPL 需妻模式.NFDM妻 	WITH 需妻模式.NFDM妻+求妻NM*(&子女..DF0/(&子女..DF0+&子女..NF0)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DF0+&子女..NF0)<>0
				REPL 需妻模式.NN妻 		WITH 需妻模式.NN妻+求妻NM*(&子女..NF0/(&子女..DF0+&子女..NF0)*需妻模式.&妻FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DF0+&子女..NF0)<>0
			ENDIF
		ENDFOR
		*****取小，求女性年龄分的婚配成功对数
		REPLACE &子女..双独 WITH MIN(&子女..DD_F,需妻模式.DD妻),&子女..NMDF WITH MIN(&子女..NMDF_F,需妻模式.NMDF妻),;
				&子女..NFDM WITH MIN(&子女..NFDM_F,需妻模式.NFDM妻),&子女..NN WITH MIN(&子女..NN_F,需妻模式.NN妻) ALL
		*****计算剩男剩女\各类未婚人数******
		REPLACE &子女..DF10 WITH &子女..DF0-&子女..DD-&子女..NMDF,&子女..NF10 WITH &子女..NF0-&子女..NN-&子女..NFDM,&子女..DM10 WITH &子女..DM0-需夫模式.DD夫-需夫模式.NFDM夫,&子女..NM10 WITH &子女..NM0-需夫模式.NN夫-需夫模式.NMDF夫 ALL
		SUM &子女..DM10 TO SDM10 FOR &子女..DM10<0
		SUM &子女..DF10 TO SDF10 FOR &子女..DF10<0
		SUM &子女..NM10 TO SNM10 FOR &子女..NM10<0
		SUM &子女..NF10 TO SNF10 FOR &子女..NF10<0
		S负M=SDM10+SNM10+SNM10+SNF10
		DS=DS+1
	ENDDO
ENDFOR
*BROWSE FIELDS x,地区农村非农子女.DM10,地区农村农业子女.DM10,地区城镇非农子女.DM10,地区城镇农业子女.DM10,地区农村非农子女.NM10,地区农村农业子女.NM10,地区城镇非农子女.NM10,地区城镇农业子女.NM10
*BROWSE FIELDS x,地区农村非农子女.双独,地区农村农业子女.双独,地区城镇非农子女.双独,地区城镇农业子女.双独,地区农村非农子女.NN,地区农村农业子女.NN,地区城镇非农子女.NN,地区城镇农业子女.NN
*BROWSE FIELDS x,地区农村非农子女.NFDM,地区农村农业子女.NFDM,地区城镇非农子女.NFDM,地区城镇农业子女.NFDM,地区农村非农子女.NMDF,地区农村农业子女.NMDF,地区城镇非农子女.NMDF,地区城镇农业子女.NMDF
*****************************************************************************************************************************	
*	以上程序段逻辑检验：
*	SUM（&子女..DD+&子女..NMDF+&子女..NFDM+&子女..NN-需夫模式.DD丈夫-需夫模式.NMDF夫-需夫模式.NFDM夫-需夫模式.NN夫）=0
*	各年龄：&子女..DF0-&子女..DD-&子女..NMDF>0,&子女..NF0-&子女..NN-&子女..NFDM>0
*****************************************************************************************************************************	
*********************************
*	农村农业与非农业子女婚配	*
*	城镇农业与非农业子女婚配	*
*********************************
SELE A
FOR CX=1 TO 2
	非农='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	农业='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	*************************
	*	非农女与农业男婚配	*
	*************************
	SELE A
	S负M=-10000
	DS=1
	DO WHILE  S负M<0
		******从女性求夫角度考察的婚配
		REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 	WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求夫DF=IIF(DS=1,&非农..DF10,&非农..DD非F+&非农..NMDF非F)
			求夫NF=IIF(DS=1,&非农..NF10,&非农..NFDM非F+&非农..NN非F)
			夫FB='妻子'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
			SUM &农业..DM10+&农业..NM10 TO SM
			IF SM<>0
				REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0
				REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0
				REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0
				REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0

				SUM 求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0
				SUM 求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0
				REPLACE &非农..DD非F_F WITH HDFDM,&非农..NMDF非F_F WITH HNMDF,&非农..NFDM非F_F WITH HNFDM,&非农..NN非F_F  WITH HNN FOR X=NL
					&&非F_F:意即从女性(非农业人口女性)角度(_F)考察的非农业人口女性(非F),与农业人口男性婚配
			ENDIF
		ENDFOR
		******从男性求妻角度考察的婚配
		REPL 需妻模式.DD妻 WITH 0,需妻模式.NMDF妻 WITH 0,需妻模式.NFDM妻 WITH 0,需妻模式.NN妻 WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求妻DM=MIN(&农业..DM10,需夫模式.DD夫+需夫模式.NFDM夫)
			求妻NM=MIN(&农业..NM10,需夫模式.NN夫+需夫模式.NMDF夫)
			妻FB='丈夫'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需妻模式.&妻FB<>0
			SUM &非农..DF10+&非农..NF10 TO SF
			IF SF<>0
				REPL 需妻模式.DD妻 		WITH 需妻模式.DD妻+求妻DM*(&非农..DF10/(&非农..DF10+&非农..NF10)*需妻模式.&妻FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DF10+&非农..NF10)<>0
				REPL 需妻模式.NMDF妻 	WITH 需妻模式.NMDF妻+求妻DM*(&非农..NF10/(&非农..DF10+&非农..NF10)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DF10+&非农..NF10)<>0
				REPL 需妻模式.NFDM妻 	WITH 需妻模式.NFDM妻+求妻NM*(&非农..DF10/(&非农..DF10+&非农..NF10)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DF10+&非农..NF10)<>0
				REPL 需妻模式.NN妻 		WITH 需妻模式.NN妻+求妻NM*(&非农..NF10/(&非农..DF10+&非农..NF10)*需妻模式.&妻FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DF10+&非农..NF10)<>0
			ENDIF
		ENDFOR
		*****取小，求女性年龄分的婚配成功对数
		REPLACE &非农..DD非F 	WITH MIN(&非农..DD非F_F,需妻模式.DD妻),&非农..NMDF非F WITH MIN(&非农..NMDF非F_F,需妻模式.NMDF妻),;
				&非农..NFDM非F 	WITH MIN(&非农..NFDM非F_F,需妻模式.NFDM妻),&非农..NN非F WITH MIN(&非农..NN非F_F,需妻模式.NN妻) ALL
		*****计算剩男剩女\各类未婚人数******
		REPLACE &非农..DF20 WITH &非农..DF10-&非农..DD非F-&非农..NMDF非F,;
				&非农..NF20 WITH &非农..NF10-&非农..NN非F-&非农..NFDM非F,;
				&农业..DM20 WITH &农业..DM10-需夫模式.DD夫-需夫模式.NFDM夫,;
				&农业..NM20 WITH &农业..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
		SUM &非农..DF20 TO SDF20 FOR &非农..DF20<0
		SUM &非农..NF20 TO SNF20 FOR &非农..NF20<0
		SUM &农业..DM20 TO SDM20 FOR &农业..DM20<0
		SUM &农业..NM20 TO SNM20 FOR &农业..NM20<0
		S负M=SDM20+SNM20+SNM20+SNF20
		DS=DS+1
	ENDDO
	*************************
	*	农业女与非农男婚配	*
	*************************
	SELE A
	S负M=-10000
	DS=1
	DO WHILE  S负M<0
		******从女性求夫角度考察的婚配
		REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 	WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求夫DF=IIF(DS=1,&农业..DF10,&农业..DD农F+&农业..NMDF农F)
			求夫NF=IIF(DS=1,&农业..NF10,&农业..NFDM农F+&农业..NN农F)
			夫FB='妻子'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
			SUM &非农..DM10+&非农..NM10 TO SM
			IF SM<>0
				REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&非农..DM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&非农..NM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB)  FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&非农..DM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB)  FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&非农..NM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				SUM 求夫DF*(&非农..DM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫DF*(&非农..NM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0	&&婚配成功的NL岁妇女人数
				SUM 求夫NF*(&非农..DM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				SUM 求夫NF*(&非农..NM10/(&非农..DM10+&非农..NM10)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0
				REPLACE &农业..DD农F_F WITH HDFDM,&农业..NMDF农F_F WITH HNMDF,&农业..NFDM农F_F WITH HNFDM,&农业..NN农F_F  WITH HNN FOR X=NL
					&&农F_F:意即从女性(农业人口女性)角度(_F)考察的农业人口女性(农F),与农业人口男性婚配
			ENDIF
		ENDFOR
		******从男性求妻角度考察的婚配
		REPL 需妻模式.DD妻 WITH 0,需妻模式.NMDF妻 WITH 0,需妻模式.NFDM妻 WITH 0,需妻模式.NN妻 WITH 0 ALL
		FOR NL=15 TO 64
			LOCA FOR X=NL
			求妻DM=MIN(&非农..DM10,需夫模式.DD夫+需夫模式.NFDM夫)
			求妻NM=MIN(&非农..NM10,需夫模式.NN夫+需夫模式.NMDF夫)
			妻FB='丈夫'-allt(STR(NL,3))
			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需妻模式.&妻FB<>0
			SUM &农业..DF10+&农业..NF10 TO SF
			IF SF<>0
				REPL 需妻模式.DD妻 		WITH 需妻模式.DD妻+求妻DM*(&农业..DF10/(&农业..DF10+&农业..NF10)*需妻模式.&妻FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DF10+&农业..NF10)<>0
				REPL 需妻模式.NMDF妻 	WITH 需妻模式.NMDF妻+求妻DM*(&农业..NF10/(&农业..DF10+&农业..NF10)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DF10+&农业..NF10)<>0
				REPL 需妻模式.NFDM妻 	WITH 需妻模式.NFDM妻+求妻NM*(&农业..DF10/(&农业..DF10+&农业..NF10)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DF10+&农业..NF10)<>0
				REPL 需妻模式.NN妻 		WITH 需妻模式.NN妻+求妻NM*(&农业..NF10/(&农业..DF10+&农业..NF10)*需妻模式.&妻FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DF10+&农业..NF10)<>0
			ENDIF
		ENDFOR
		*****取小，求女性年龄分的婚配成功对数
		REPLACE &农业..DD农F 	WITH MIN(&农业..DD农F_F,需妻模式.DD妻),&农业..NMDF农F WITH MIN(&农业..NMDF农F_F,需妻模式.NMDF妻),;
				&农业..NFDM农F 	WITH MIN(&农业..NFDM农F_F,需妻模式.NFDM妻),&农业..NN农F WITH MIN(&农业..NN农F_F,需妻模式.NN妻) ALL
		*****计算剩男剩女\各类未婚人数******
		REPLACE &农业..DF20 WITH &农业..DF10-&农业..DD农F-&农业..NMDF农F,;
				&农业..NF20 WITH &农业..NF10-&农业..NN农F-&农业..NFDM农F,;
				&非农..DM20 WITH &非农..DM10-需夫模式.DD夫-需夫模式.NFDM夫,;
				&非农..NM20 WITH &非农..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
		SUM &农业..DF20 TO SDF20 FOR &农业..DF20<0
		SUM &农业..NF20 TO SNF20 FOR &农业..NF20<0
		SUM &非农..DM20 TO SDM20 FOR &非农..DM20<0
		SUM &非农..NM20 TO SNM20 FOR &非农..NM20<0
		S负M=SDM20+SNM20+SNM20+SNF20
		DS=DS+1
	ENDDO
*	BROWSE FIELDS X,&非农..DM20,&非农..NM20,&农业..DF20,&农业..NF20
ENDFOR
**********************************************
*	地区城镇非农子女F 地区农村非农子女M CFXF
*	地区农村非农子女F 地区城镇非农子女M XFCF
*	地区城镇农业子女F 地区农村非农子女M CNXF
*	地区农村非农子女F 地区城镇农业子女M XFCN
*	地区城镇非农子女F 地区农村农业子女M CFXN
*	地区农村农业子女F 地区城镇非农子女M XNCF
*	地区城镇农业子女F 地区农村农业子女M CNXN
*	地区农村农业子女F 地区城镇农业子女M XNCN
**********************************************
FOR CX=1 TO 1
	城乡F='地区'-IIF(CX=1,'农村','城镇')
	城乡M='地区'-IIF(CX=1,'城镇','农村')
	FOR 农非1=1 TO 2
		农非B1=IIF(农非1=1,'非农','农业')
		FOR 农非2=1 TO 2
			农非B2=IIF(农非2=1,'非农','农业')
			FOR XB=1 TO 2
				XB1=IIF(XB=1,'M','F')
				XB2=IIF(XB=1,'F','M')
				FIL1=城乡F-农非B1-'子女'	&&-XB1
				FIL2=城乡M-农非B2-'子女'	&&-XB2
				FIL妻=IIF(XB=1,FIL2,FIL1)
				FIL夫=IIF(XB=1,FIL1,FIL2)
				妻夫=IIF(SUBSTR(FIL妻,5,2)='城','C','X')-IIF(SUBSTR(FIL妻,9,2)='非','F','N')-IIF(SUBSTR(FIL夫,5,2)='城','C','X')-IIF(SUBSTR(FIL夫,9,2)='非','F','N')
				DD妻夫='DD'-'_'-妻夫
				NMDF妻夫='NMDF'-'_'-妻夫
				NFDM妻夫='NFDM'-'_'-妻夫
				NN妻夫='NN'-'_'-妻夫
				SELE A
				S负M=-10000
				DS=1
				DO WHILE  S负M<0
					******从女性求夫角度考察的婚配
					REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 	WITH 0 ALL
					FOR NL=15 TO 64
						LOCA FOR X=NL
						求夫DF=IIF(DS=1,&FIL妻..DF20,&FIL妻..&DD妻夫+&FIL妻..&NMDF妻夫)
						求夫NF=IIF(DS=1,&FIL妻..NF20,&FIL妻..&NFDM妻夫+&FIL妻..&NN妻夫)
						夫FB='妻子'-allt(STR(NL,3))
						CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
						SUM &FIL夫..DM20+&FIL夫..NM20 TO SM
						IF SM<>0
							REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&FIL夫..DM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&FIL夫..NM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB)  FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&FIL夫..DM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB)  FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&FIL夫..NM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							SUM 求夫DF*(&FIL夫..DM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0	&&婚配成功的NL岁妇女人数
							SUM 求夫DF*(&FIL夫..NM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0	&&婚配成功的NL岁妇女人数
							SUM 求夫NF*(&FIL夫..DM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							SUM 求夫NF*(&FIL夫..NM20/(&FIL夫..DM20+&FIL夫..NM20)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL夫..DM20+&FIL夫..NM20)<>0
							REPLACE &FIL妻..&DD妻夫 WITH HDFDM,&FIL妻..&NMDF妻夫 WITH HNMDF,&FIL妻..&NFDM妻夫 WITH HNFDM,&FIL妻..&NN妻夫  WITH HNN FOR X=NL
								&&妻夫:意即从女性(FIL妻人口女性)角度(_F)考察的FIL妻人口女性(妻夫),与FIL妻人口男性婚配
						ENDIF
					ENDFOR
					******从男性求妻角度考察的婚配
					REPL 需妻模式.DD妻 WITH 0,需妻模式.NMDF妻 WITH 0,需妻模式.NFDM妻 WITH 0,需妻模式.NN妻 WITH 0 ALL
					FOR NL=15 TO 64
						LOCA FOR X=NL
						求妻DM=MIN(&FIL夫..DM20,需夫模式.DD夫+需夫模式.NFDM夫)
						求妻NM=MIN(&FIL夫..NM20,需夫模式.NN夫+需夫模式.NMDF夫)
						妻FB='丈夫'-allt(STR(NL,3))
						CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需妻模式.&妻FB<>0
						SUM &FIL妻..DF20+&FIL妻..NF20 TO SF
						IF SF<>0
							REPL 需妻模式.DD妻 		WITH 需妻模式.DD妻+求妻DM*(&FIL妻..DF20/(&FIL妻..DF20+&FIL妻..NF20)*需妻模式.&妻FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL妻..DF20+&FIL妻..NF20)<>0
							REPL 需妻模式.NMDF妻 	WITH 需妻模式.NMDF妻+求妻DM*(&FIL妻..NF20/(&FIL妻..DF20+&FIL妻..NF20)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL妻..DF20+&FIL妻..NF20)<>0
							REPL 需妻模式.NFDM妻 	WITH 需妻模式.NFDM妻+求妻NM*(&FIL妻..DF20/(&FIL妻..DF20+&FIL妻..NF20)*需妻模式.&妻FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL妻..DF20+&FIL妻..NF20)<>0
							REPL 需妻模式.NN妻 		WITH 需妻模式.NN妻+求妻NM*(&FIL妻..NF20/(&FIL妻..DF20+&FIL妻..NF20)*需妻模式.&妻FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&FIL妻..DF20+&FIL妻..NF20)<>0
						ENDIF
					ENDFOR
					*****取小，求女性年龄分的婚配成功对数
					REPLACE &FIL妻..&DD妻夫 	WITH MIN(&FIL妻..&DD妻夫,需妻模式.DD妻),&FIL妻..&NMDF妻夫 WITH MIN(&FIL妻..&NMDF妻夫,需妻模式.NMDF妻),;
							&FIL妻..&NFDM妻夫 	WITH MIN(&FIL妻..&NFDM妻夫,需妻模式.NFDM妻),&FIL妻..&NN妻夫 WITH MIN(&FIL妻..&NN妻夫,需妻模式.NN妻) ALL
					*****计算剩男剩女\各类未婚人数******
					REPLACE &FIL妻..DF30 WITH &FIL妻..DF20-&FIL妻..&DD妻夫-&FIL妻..&NMDF妻夫,;
							&FIL妻..NF30 WITH &FIL妻..NF20-&FIL妻..&NN妻夫-&FIL妻..&NFDM妻夫,;
							&FIL夫..DM30 WITH &FIL夫..DM20-需夫模式.DD夫-需夫模式.NFDM夫,;
							&FIL夫..NM30 WITH &FIL夫..NM20-需夫模式.NN夫-需夫模式.NMDF夫 ALL
					SUM &FIL妻..DF30 TO SDF30 FOR &FIL妻..DF30<0
					SUM &FIL妻..NF30 TO SNF30 FOR &FIL妻..NF30<0
					SUM &FIL夫..DM30 TO SDM30 FOR &FIL夫..DM30<0
					SUM &FIL夫..NM30 TO SNM30 FOR &FIL夫..NM30<0
					S负M=SDM30+SNM30+SNM30+SNF30
					DS=DS+1
*					?FIL夫,FIL妻,S负M
				ENDDO
			ENDFOR
		ENDFOR
	ENDFOR
ENDFOR
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	REPLACE 双独 WITH DD+DD非F+DD农F+DD_CFXF+DD_CFXN+DD_CNXF+DD_CNXN+DD_XFCF+DD_XFCN+DD_XNCF+DD_XNCN,;
		NMDF WITH NMDF+NMDF非F+NMDF农F+NMDF_CFXF+NMDF_CFXN+NMDF_CNXF+NMDF_CNXN+NMDF_XFCF+NMDF_XFCN+NMDF_XNCF+NMDF_XNCN,;
		NFDM WITH NFDM+NFDM非F+NFDM农F+NFDM_CFXF+NFDM_CFXN+NFDM_CNXF+NFDM_CNXN+NFDM_XFCF+NFDM_XFCN+NFDM_XNCF+NFDM_XNCN,;
		NN	 WITH NN+NN非F+NN农F+NN_CFXF+NN_CFXN+NN_CNXF+NN_CNXN+NN_XFCF+NN_XFCN+NN_XNCF+NN_XNCN ALL
ENDFOR
*BROWSE FIELDS x,地区农村非农子女.DM20,地区农村农业子女.DM20,地区城镇非农子女.DM20,地区城镇农业子女.DM20,地区农村非农子女.NM20,地区农村农业子女.NM20,地区城镇非农子女.NM20,地区城镇农业子女.NM20
**************************************************************************************************************************************************************	
*	以上程序段逻辑检验：
*	BROWSE FIELDS FC=地区农村非农子女.DF0-地区农村非农子女.DD-地区农村非农子女.NMDF,MC=地区农村非农子女.NF0-地区农村非农子女.NN-地区农村非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区农村农业子女.DF0-地区农村农业子女.DD-地区农村农业子女.NMDF,MC=地区农村农业子女.NF0-地区农村农业子女.NN-地区农村农业子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇非农子女.DF0-地区城镇非农子女.DD-地区城镇非农子女.NMDF,MC=地区城镇非农子女.NF0-地区城镇非农子女.NN-地区城镇非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇农业子女.DF0-地区城镇农业子女.DD-地区城镇农业子女.NMDF,MC=地区城镇农业子女.NF0-地区城镇农业子女.NN-地区城镇农业子女.NFDM	&&>=0
*	经以下语句检验，经过以上两次婚配，育龄夫妇已经完全婚配完毕，无一剩余，也无负数出现
*	BROWSE FIELDS x,地区农村非农子女.DF20,地区农村农业子女.DF20,地区城镇非农子女.DF20,地区城镇农业子女.DF20,地区农村非农子女.NF20,地区农村农业子女.NF20,地区城镇非农子女.NF20,地区城镇农业子女.NF20
***************************************************************************************************************************************************************	
RETU

*******************************************************************************
PROC 女性最大多龄概率法
*****************************
*	农村农业与农村农业婚配	*
*	农村非农与农村非农婚配	*
*	城镇农业与城镇农业婚配	*
*	城镇非农与城镇非农婚配	*
*****************************
FOR CX=1 TO 4    &&
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
	DO CASE
	CASE 	结婚率法='全员婚配'
		REPL &子女..DF0 WITH &子女..DF+&子女..DDBF,&子女..DM0 WITH &子女..DM+&子女..DDBM,&子女..NF0 WITH &子女..NF,&子女..NM0 WITH &子女..NM ALL
		REPL &子女..HJF WITH &子女..DF0+&子女..NF0,&子女..HJM WITH &子女..DM0+&子女..NM0,&子女..HJ WITH &子女..HJF+&子女..HJM ALL
	CASE 	结婚率法='有偶率婚配'
		有偶率=IIF(农非=1,'乡村','城镇')-'有偶率'
		REPL &子女..DF0 WITH (&子女..DF+&子女..DDBF)*&有偶率..F有偶B,&子女..DM0 WITH (&子女..DM+&子女..DDBM)*&有偶率..M有偶B,&子女..NF0 WITH &子女..NF*&有偶率..F有偶B,&子女..NM0 WITH &子女..NM*&有偶率..M有偶B ALL
		REPL &子女..HJF0 WITH &子女..DF0+&子女..NF0,&子女..HJM0 WITH &子女..DM0+&子女..NM0 ALL
	ENDCASE
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	&&	此处将“&子女..DF”改为“&子女..DF+&子女..DDBF”，将“&子女..DM”改为“&子女..DM+&子女..DDBM”与删除下句相配合：
	&&	*REPL D WITH D+DDB,DM WITH DM+DDBM,DF WITH DF+DDBF FOR X=0 &&DDB他们之间婚配可以象双独夫妇一样生育二个孩子的人数（只对零岁组操作）
	&&	删除后一个语句，是为了将可以象双独婚配允许生育二个孩子的后代（不一定是独生子女）与本人是独生子女的人数分开，以便于计算“四二一”家庭数量
	&&  修改前一个语句，是可以简化这部份人的婚配计算。如不将“&子女..DF”改为“&子女..DF+&子女..DDBF”，“&子女..DM”改为“&子女..DM+&子女..DDBM”，就不该
	&&	删除“REPL D WITH D+DDB,……”。
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	REPL &子女..DF0 WITH 0 FOR &子女..DF<0
	REPL &子女..DM0 WITH 0 FOR &子女..DM<0
	REPL &子女..NF0 WITH 0 FOR &子女..NF<0
	REPL &子女..NM0 WITH 0 FOR &子女..NM<0
	******从女性求夫角度考察的婚配
	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
	FOR NL=15 TO 64
		LOCA FOR X=NL
		求夫DF=&子女..DF0
		求夫NF=&子女..NF0
		夫FB='妻子'-allt(STR(NL,3))
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
		SUM &子女..DM0+&子女..NM0 TO SM
		IF SM<>0
			REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0

			SUM 求夫DF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&子女..DM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			SUM 求夫NF*(&子女..NM0/(&子女..DM0+&子女..NM0)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&子女..DM0+&子女..NM0)<>0.AND.&子女..DM0/(&子女..DM0+&子女..NM0)>0
			REPLACE &子女..DD_F WITH HDFDM,&子女..NMDF_F WITH HNMDF,&子女..NFDM_F WITH HNFDM,&子女..NN_F  WITH HNN FOR X=NL
		ENDIF
	ENDFOR
	REPLACE &子女..双独 WITH &子女..DD_F,&子女..NMDF WITH &子女..NMDF_F,&子女..NFDM WITH &子女..NFDM_F,&子女..NN WITH &子女..NN_F ALL
	*****计算剩男剩女\各类未婚人数******
	REPLACE &子女..DF10 WITH &子女..DF0-&子女..双独-&子女..NMDF,&子女..NF10 WITH &子女..NF0-&子女..NN-&子女..NFDM,&子女..DM10 WITH &子女..DM0-需夫模式.DD夫-需夫模式.NFDM夫,&子女..NM10 WITH &子女..NM0-需夫模式.NN夫-需夫模式.NMDF夫 ALL
ENDFOR
*****************************************************************************************************************************	
*	以上程序段逻辑检验：
*	SUM（&子女..DD+&子女..NMDF+&子女..NFDM+&子女..NN-需夫模式.DD丈夫-需夫模式.NMDF夫-需夫模式.NFDM夫-需夫模式.NN夫）=0
*	各年龄：&子女..DF0-&子女..DD-&子女..NMDF>0,&子女..NF0-&子女..NN-&子女..NFDM>0
*****************************************************************************************************************************	
*********************************
*	农村农业与非农业子女婚配	*
*	城镇农业与非农业子女婚配	*
*********************************
SELE A
FOR CX=1 TO 2
	非农='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	农业='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	*************************
	*	非农女与农业男婚配	*
	*************************
	SELE A
	******从女性求夫角度考察的婚配
	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 	WITH 0,需夫模式.NN夫 	WITH 0 ALL
	FOR NL=15 TO 64
		LOCA FOR X=NL
		求夫DF=&非农..DF10
		求夫NF=&非农..NF10
		夫FB='妻子'-allt(STR(NL,3))
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
		SUM &农业..DM10+&农业..NM10 TO SM
		IF SM<>0
			REPL 需夫模式.DD夫 		WITH 需夫模式.DD夫+求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0

			SUM 求夫DF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&农业..DM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			SUM 求夫NF*(&农业..NM10/(&农业..DM10+&农业..NM10)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&农业..DM10+&农业..NM10)<>0.AND.(&农业..DM10/(&农业..DM10+&农业..NM10))>0
			REPLACE &非农..DD非F_F WITH HDFDM,&非农..NMDF非F_F WITH HNMDF,&非农..NFDM非F_F WITH HNFDM,&非农..NN非F_F  WITH HNN FOR X=NL
		ENDIF
	ENDFOR
	*****取小，求女性年龄分的婚配成功对数
	REPLACE &非农..DD非F WITH MIN(&非农..DD非F_F,需妻模式.DD妻),&非农..NMDF非F WITH MIN(&非农..NMDF非F_F,需妻模式.NMDF妻),;
			&非农..NFDM非F WITH MIN(&非农..NFDM非F_F,需妻模式.NFDM妻),&非农..NN非F WITH MIN(&非农..NN非F_F,需妻模式.NN妻) ALL
	***********************
	REPLACE &非农..双独 WITH &非农..双独+&非农..DD非F,&非农..NMDF WITH &非农..NMDF+&非农..NMDF非F,&非农..NFDM WITH &非农..NFDM+&非农..NFDM非F,&非农..NN WITH &非农..NN+&非农..NN非F ALL 
	*****计算剩男剩女\各类未婚人数******
	REPLACE &非农..DF20 WITH &非农..DF0-&非农..双独-&非农..NMDF,&非农..NF20 WITH &非农..NF0-&非农..NN-&非农..NFDM,;
			&农业..DM20 WITH &农业..DM10-需夫模式.DD夫-需夫模式.NFDM夫,&农业..NM20 WITH &农业..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
	*************************
	*	农业女与非农男婚配	*
	*************************
	SELE A
	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 	WITH 0,需夫模式.NN夫 	WITH 0 ALL
	FOR NL=15 TO 64
		LOCA FOR X=NL
		求夫DF=&农业..DF10
		求夫NF=&农业..NF10
		夫FB='妻子'-allt(STR(NL,3))
		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
		SUM &非农..DM0+&非农..NM0 TO SM
		IF SM<>0
			REPL 需夫模式.DD夫 	WITH 需夫模式.DD夫+求夫DF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) 	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫DF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPL 需夫模式.NN夫 	WITH 需夫模式.NN夫+求夫NF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB)	FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0

			SUM 求夫DF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HDFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫DF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNMDF FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0	&&婚配成功的NL岁妇女人数
			SUM 求夫NF*(&非农..DM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNFDM FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			SUM 求夫NF*(&非农..NM0/(&非农..DM0+&非农..NM0)*需夫模式.&夫FB) TO HNN FOR  X>=MIX1.AND.X<=MAX1.AND.(&非农..DM10+&非农..NM10)<>0.AND.(&非农..DM0/(&非农..DM0+&非农..NM0))>0
			REPLACE &农业..双独 WITH &农业..双独+HDFDM,&农业..NMDF WITH &农业..NMDF+HNMDF,&农业..NFDM WITH &农业..NFDM+HNFDM,&农业..NN  WITH &农业..NN+HNN FOR X=NL
		ENDIF
	ENDFOR
	*****各类未婚人数******
	REPLACE &农业..DF20 WITH &农业..DF0-&农业..双独-&农业..NMDF,&农业..NF20 WITH &农业..NF0-&农业..NN-&农业..NFDM,;
			&农业..DM20 WITH &农业..DM10-需夫模式.DD夫-需夫模式.NFDM夫,&农业..NM20 WITH &农业..NM10-需夫模式.NN夫-需夫模式.NMDF夫 ALL
ENDFOR
*******对婚配剩余女性处理：独生女计入女单，非独生女计入双非
REPLACE 地区农村非农子女.NMDF WITH 地区农村非农子女.NMDF+地区农村非农子女.DF20,地区农村农业子女.NMDF WITH 地区农村农业子女.NMDF+地区农村农业子女.DF20,;
		地区城镇非农子女.NMDF WITH 地区城镇非农子女.NMDF+地区城镇非农子女.DF20,地区城镇农业子女.NMDF WITH 地区城镇农业子女.NMDF+地区城镇农业子女.DF20,;
		地区农村非农子女.NN WITH 地区农村非农子女.NN+地区农村非农子女.NF20,地区农村农业子女.NN WITH 地区农村农业子女.NN+地区农村农业子女.NF20,;
		地区城镇非农子女.NN WITH 地区城镇非农子女.NN+地区城镇非农子女.NF20,地区城镇农业子女.NN WITH 地区城镇农业子女.NN+地区城镇农业子女.NF20 FOR X>=15.AND.X<=49

**************************************************************************************************************************************************************	
*	以上程序段逻辑检验：
*	BROWSE FIELDS FC=地区农村非农子女.DF0-地区农村非农子女.双独-地区农村非农子女.NMDF,MC=地区农村非农子女.NF0-地区农村非农子女.NN-地区农村非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区农村农业子女.DF0-地区农村农业子女.双独-地区农村农业子女.NMDF,MC=地区农村农业子女.NF0-地区农村农业子女.NN-地区农村农业子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇非农子女.DF0-地区城镇非农子女.双独-地区城镇非农子女.NMDF,MC=地区城镇非农子女.NF0-地区城镇非农子女.NN-地区城镇非农子女.NFDM	&&>=0
*	BROWSE FIELDS FC=地区城镇农业子女.DF0-地区城镇农业子女.双独-地区城镇农业子女.NMDF,MC=地区城镇农业子女.NF0-地区城镇农业子女.NN-地区城镇农业子女.NFDM	&&>=0
*	经以下语句检验，经过以上两次婚配，育龄夫妇已经完全婚配完毕，无一剩余，也无负数出现
*	BROWSE FIELDS x,地区农村非农子女.DM20,地区农村农业子女.DM20,地区城镇非农子女.DM20,地区城镇农业子女.DM20,地区农村非农子女.NM20,地区农村农业子女.NM20,地区城镇非农子女.NM20,地区城镇农业子女.NM20
*	BROWSE FIELDS x,地区农村非农子女.DF20,地区农村农业子女.DF20,地区城镇非农子女.DF20,地区城镇农业子女.DF20,地区农村非农子女.NF20,地区农村农业子女.NF20,地区城镇非农子女.NF20,地区城镇农业子女.NF20
*	lkm
***************************************************************************************************************************************************************	
RETURN 

*************************************
*	单时机调整堆积夫妇估计与剥离	*
*************************************
PROCEDURE 单时机调整堆积夫妇估计与剥离
DO CASE 
CASE ND0=调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对，对已经生过二孩的夫妇单独列出,只进行年龄移算*****
			REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N2D1N,现行N2D1C))  FOR &子女..堆积比例<>0
			REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N1D2N,现行N1D2C))   FOR &子女..堆积比例<>0
			REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积 ALL
			REPL &子女..单独堆积15  WITH &子女..单独堆积 FOR x<=29
			REPL &子女..单独堆积30  WITH &子女..单独堆积 FOR x>=30.AND.X<35
			REPL &子女..单独堆积35  WITH &子女..单独堆积 FOR x>=35
			
			*****剔除堆积后的非堆积单独。
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
			*REPL &子女..DF0 WITH &子女..DF0-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
			*REPL &子女..NF0 WITH &子女..NF0-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			*REPL &子女..DF WITH &子女..DF-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
			*REPL &子女..NF WITH &子女..NF-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0

			*****剔除堆积后的非堆积单独*****
	*		REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积-&子女..已2女单,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL	&&FOR &子女..堆积比例<>0	&&待2单独
		CASE 政策='普二'
			REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N2D1N,现行N2D1C)) ,&子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N1D2N,现行N1D2C)),&子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积 ALL
			REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,NNFN,NNFC))  FOR &子女..堆积比例<>0
			REPL &子女..单独堆积15  WITH &子女..单独堆积,&子女..双非堆积15  WITH &子女..双非堆积 FOR x<=29
			REPL &子女..单独堆积30  WITH &子女..单独堆积,&子女..双非堆积30  WITH &子女..双非堆积 FOR x>=30.AND.X<35
			REPL &子女..单独堆积35  WITH &子女..单独堆积,&子女..单独堆积35  WITH &子女..双非堆积 FOR x>=35
			
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
			*REPL &子女..NF0 WITH &子女..NF0-&子女..男单堆积-&子女..已2男单-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
			*REPL &子女..DF0 WITH &子女..DF0-&子女..女单堆积-&子女..已2女单  FOR &子女..堆积比例<>0
			*REPL &子女..NF WITH &子女..NF-&子女..男单堆积-&子女..已2男单-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
			*REPL &子女..DF WITH &子女..DF-&子女..女单堆积-&子女..已2女单  FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独和双非*****
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积,&子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0
		ENDCASE
		*************************************************************************
		*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
		*	以下剥离方法,不完善的地方是：
		*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
		***************************************************************************************************************************
	*	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
	* 	RELE AR1
	*	COPY TO ARRA AR1 FIEL X,&子女..女单堆积,&子女..男单堆积,&子女..双非堆积  FOR &子女..堆积比例<>0
	*	COUN TO TS  FOR &子女..堆积比例<>0
	*	FOR NL=1 TO TS
	*		求夫NMDF=AR1(NL,2)
	*		求夫NFDM=AR1(NL,3)
	*		求夫NN=IIF((农业ZC1='普二').AND.(非农ZC1='普二'),AR1(NL,4),0)
	*		夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*		SUM &子女..DM0+&子女..NM0 TO SM
	*		IF SM<>0
	*			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫NMDF*需夫模式.&夫FB ALL
	*			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NFDM*需夫模式.&夫FB ALL
	*			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NN*需夫模式.&夫FB ALL
	*		ENDIF
	*	ENDFOR
	*	REPL &子女..堆积丈夫 WITH 需夫模式.NMDF夫+需夫模式.NFDM夫+需夫模式.NN夫  ALL
	*	REPL &子女..NM0 WITH &子女..NM0-需夫模式.NMDF夫-需夫模式.NN夫  ALL
	*	REPL &子女..DM0 WITH &子女..DM0-需夫模式.NFDM夫  ALL
	*	REPL &子女..NM WITH &子女..NM-需夫模式.NMDF夫-需夫模式.NN夫  ALL
	*	REPL &子女..DM WITH &子女..DM-需夫模式.NFDM夫  ALL
	ENDFOR
CASE ND0>调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL	&&FOR &子女..堆积比例<>0	&&待2单独
		CASE 政策='普二'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积,&子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0
		ENDCASE
	ENDFOR 
ENDCASE 
RETURN 

*********************************
*	渐进普二堆积夫妇估计与剥离	*
*********************************
PROCEDURE 渐进普二堆积夫妇估计与剥离
DO CASE 
CASE ND0=调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		*SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM1,SF1 FOR &子女..堆积比例<>0
		存活子女=IIF(CX=1,'农农',IIF(CX=2,'农非',IIF(CX=3,'城农','城非')))-'存活子女'
		DO CASE
		CASE (政策='单独'.OR.政策='单独后').and.nd0=非农时机2
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对，对已经生过二孩的夫妇单独列出,只进行年龄移算*****
	*		REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例,&子女..已2男单 WITH &子女..NFDM*(1-&子女..堆积比例)  FOR &子女..堆积比例<>0
	*		REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例,&子女..已2女单 WITH &子女..NMDF*(1-&子女..堆积比例)  FOR &子女..堆积比例<>0
	*		REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积,&子女..已2单独 WITH &子女..已2男单+&子女..已2女单 ALL
			*****剔除堆积后的非堆积单独。
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
	*		REPL &子女..DF0 WITH &子女..DF0-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
	*		REPL &子女..NF0 WITH &子女..NF0-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
	*		REPL &子女..DF WITH &子女..DF-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
	*		REPL &子女..NF WITH &子女..NF-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独*****
	*		REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积-&子女..已2女单,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			
			REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N2D1N,现行N2D1C))  FOR &子女..堆积比例<>0
			REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N1D2N,现行N1D2C))   FOR &子女..堆积比例<>0
			REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积 ALL
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL &&FOR &子女..堆积比例<>0

			REPL &子女..单独堆积15  WITH &子女..单独堆积 FOR x<=29
			REPL &子女..单独堆积30  WITH &子女..单独堆积 FOR x>=30.AND.X<35
			REPL &子女..单独堆积35  WITH &子女..单独堆积 FOR x>=35
			*************************************************************************
			*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
			*	以下剥离方法,不完善的地方是：
			*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
			***************************************************************************************************************************
	*		REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
	*	 	RELE AR1
	*		COPY TO ARRA AR1 FIEL X,&子女..女单堆积,&子女..男单堆积  FOR &子女..堆积比例<>0
	*		COUN TO TS  FOR &子女..堆积比例<>0
	*		FOR NL=1 TO TS
	*			求夫NMDF=AR1(NL,2)
	*			求夫NFDM=AR1(NL,3)
	*			夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*			SUM &子女..DM0+&子女..NM0 TO SM
	*			IF SM<>0
	*				REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫NMDF*需夫模式.&夫FB ALL
	*				REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NFDM*需夫模式.&夫FB ALL
	*			ENDIF
	*		ENDFOR
	*		REPL &子女..堆积丈夫 WITH 需夫模式.NMDF夫+需夫模式.NFDM夫  ALL
	*		REPL &子女..NM0 WITH &子女..NM0-需夫模式.NMDF夫  ALL
	*		REPL &子女..DM0 WITH &子女..DM0-需夫模式.NFDM夫  ALL
	*		REPL &子女..NM WITH &子女..NM-需夫模式.NMDF夫  ALL
	*		REPL &子女..DM WITH &子女..DM-需夫模式.NFDM夫  ALL
		CASE 政策='普二'.and.nd0=非农时机3
	*		REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例  FOR &子女..堆积比例<>0
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
	*		REPL &子女..NF0 WITH &子女..NF0-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
	*		REPL &子女..NF WITH &子女..NF-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独和双非*****
	*		REPL &子女..NN WITH &子女..NN-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0

			REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行NNFN,现行NNFC)) FOR &子女..堆积比例<>0

			REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0

			REPL &子女..双非堆积15  WITH &子女..双非堆积 FOR x<=29
			REPL &子女..双非堆积30  WITH &子女..双非堆积 FOR x>=30.AND.X<35
			REPL &子女..双非堆积35  WITH &子女..双非堆积 FOR x>=35

*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*?子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积
			*************************************************************************
			*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
			*	以下剥离方法,不完善的地方是：
			*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
			***************************************************************************************************************************
	*		REPL 需夫模式.NN夫 WITH 0 ALL
	*	 	RELE AR1
	*		COPY TO ARRA AR1 FIEL X,&子女..双非堆积,&子女..已2双非  FOR &子女..堆积比例<>0
	*		COUN TO TS  FOR &子女..堆积比例<>0
	*		FOR NL=1 TO TS
	*			求夫NN=AR1(NL,2)+AR1(NL,3)
	*			夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*			SUM &子女..NM0 TO SM
	*			IF SM<>0
	*				REPL 需夫模式.NN夫 WITH 需夫模式.NN夫+求夫NN*需夫模式.&夫FB	ALL
	*			ENDIF
	*		ENDFOR
	*		REPL &子女..堆积丈夫 WITH &子女..堆积丈夫+需夫模式.NN夫  ALL
	*		REPL &子女..NM0 WITH &子女..NM0-需夫模式.NN夫  ALL
	*		REPL &子女..NM WITH &子女..NM-需夫模式.NN夫  ALL
		ENDCASE
	*SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM2,SF2 FOR &子女..堆积比例<>0
	*?ND0,DQB,政策,调整时机,子女,SM1,SM2,SM1-SM2,SF1,SF2,SF1-SF2
	*g
*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*LOCATE FOR x=49
*?nd0,调整时机,子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积,x,&子女..单独堆积 ,&子女..双非堆积
	ENDFOR
CASE ND0>调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		COPY TO ARRAY ARNMDF FIELDS &子女..NMDF
		COPY TO ARRAY AR女单堆积 FIELDS &子女..女单堆积
		COPY TO ARRAY ARNFDM FIELDS &子女..NFDM
		COPY TO ARRAY AR男单堆积 FIELDS &子女..男单堆积
		COPY TO ARRAY ARNN FIELDS &子女..NN
		COPY TO ARRAY AR双非堆积 FIELDS &子女..双非堆积
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			*REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
			REPL &子女..NMDF WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),ARNMDF(RECNO())-AR女单堆积(RECNO()),0),&子女..女单堆积 WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),&子女..女单堆积,AR女单堆积(RECNO())-ARNMDF(RECNO())) ALL
			REPL &子女..NFDM WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),ARNFDM(RECNO())-AR男单堆积(RECNO()),0),&子女..男单堆积 WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),&子女..男单堆积,AR男单堆积(RECNO())-ARNFDM(RECNO())) ALL
		CASE 政策='普二'
			*REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
			*REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL
			REPL &子女..NMDF WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),ARNMDF(RECNO())-AR女单堆积(RECNO()),0),&子女..女单堆积 WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),&子女..女单堆积,AR女单堆积(RECNO())-ARNMDF(RECNO())) ALL
			REPL &子女..NFDM WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),ARNFDM(RECNO())-AR男单堆积(RECNO()),0),&子女..男单堆积 WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),&子女..男单堆积,AR男单堆积(RECNO())-ARNFDM(RECNO())) ALL
			REPL &子女..NN WITH IIF(ARNN(RECNO())>=AR双非堆积(RECNO()),ARNN(RECNO())-AR双非堆积(RECNO()),0),&子女..双非堆积 WITH IIF(ARNN(RECNO())>=AR双非堆积(RECNO()),&子女..双非堆积,AR双非堆积(RECNO())-ARNN(RECNO())) ALL
		ENDCASE
*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*?nd0,调整时机,子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积

	ENDFOR
ENDCASE 
RETURN 


*********************************
PROCEDURE 分婚配模式按政策生育的孩子数
出生数=0
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女')))
	DO CASE 
	CASE  CX=1.OR.CX=3
		NMDFR=N1D2N
		NFDMR=N2D1N
		NNR=NNFN
		城乡生育模式='农村生育模式'
		无活产比=乡村无活产比	&&IIF(实现方式='可能生育',0.124423342,0)	&&农、林、牧、渔、水利业生产人员无活产比例	&&
	CASE  CX=2.OR.CX=4
		NMDFR=N1D2C
		NFDMR=N2D1C
		NNR=NNFC
		城乡生育模式='城镇生育模式'
		无活产比=城镇无活产比	&&IIF(实现方式='可能生育',0.270204021,0)	&&国家机关、党群组织、企业、事业单位负责人，专业技术人员，办事人员和有关人员，商业、服务业人员加和平均无活产比例	&&
	ENDCASE
	DO 各类夫妇分年龄生育孩子数
	REPL 政策双独B WITH 双独B,政策NNB WITH NNB,政策NMDFB WITH NMDFB,政策NFDMB WITH  NFDMB ALL 
	超生S=0
	******堆积释放计算*****
	 AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
	 *AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
	 AR突释模式=IIF(CX=1.OR.CX=3, 'AR农业突释模式','AR非农突释模式')
	 SUM 单独堆积+双非堆积  TO S堆积  FOR X<=49
	 IF S堆积<>0.and.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		 SELECT A
		 DO 不同政策实现程度下的堆积生育释放法
	 ELSE 
		 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
		 REPL 政策双独B WITH 双独B,政策NNB WITH NNB,政策NMDFB WITH NMDFB,政策NFDMB WITH  NFDMB,政策生育 WITH 政策双独B+政策NNB+政策NMDFB+政策NFDMB ALL 
	 ENDIF
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类婚配夫妇分年龄生育率
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类堆积释放数及后代分类
出生数=出生数+(Sdd1+SDD2+SNMDF1+SNFDM1+单独B2+SNN1+SNN2)
ENDFOR
SELECT A
	REPLACE 地区分龄政策生育.&BND2 WITH  地区农村非农子女.政策生育+地区城镇非农子女.政策生育+地区农村农业子女.政策生育+地区城镇农业子女.政策生育 ALL
   	REPLACE 地区农业子女.政策生育 WITH 地区农村农业子女.政策生育+地区城镇农业子女.政策生育,;
			地区非农子女.政策生育 WITH 地区农村非农子女.政策生育+地区城镇非农子女.政策生育,;
 			地区城镇子女.政策生育 WITH 地区城镇农业子女.政策生育+地区城镇非农子女.政策生育,;
 			地区农村子女.政策生育 WITH 地区农村农业子女.政策生育+地区农村非农子女.政策生育,;
 			地区子女.政策生育 WITH 地区农业子女.政策生育+地区非农子女.政策生育 ALL
	SUM 地区分龄政策生育.&BND2,地区农村非农子女.政策生育,地区城镇非农子女.政策生育,地区农村农业子女.政策生育,地区城镇农业子女.政策生育 TO DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育
*?"按孩次合计的出生数：",出生数,"政策生育:",DQ政策生育,"按孩次合计的出生数-政策生育:",出生数-DQ政策生育,单放B2
RETURN 

************************************
PROCEDURE 分婚配模式可能生育生育的孩子数
*****政策实现比可能生育**************
SELECT A
SUM 地区分龄.&MND2+地区分龄.&FND2,地区城镇分龄.&MND2+地区城镇分龄.&FND2 TO ZRK,CZRK
CSH水平2=CZRK/ZRK*100
地带=ARDQ多种参数(dqX,14)
*?dqb,现政比1,实现比
******按模型计算可实现TFR0*******
*DO CASE
*CASE 现政比1<0.99.and.地区政策TFR<=地区政策TFR1
*	可实现TFR0=地区政策TFR*(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR1-地区政策TFR))
*CASE 现政比1<0.99.and.地区政策TFR>地区政策TFR1
*	可实现TFR0=地区政策TFR*(现政比1-0.02067*(CSH水平2*100-CSH水平1))
*CASE 现政比1>1.01.and.现政比1<=2.and.地区政策TFR<=2
*	可实现TFR0=地区政策TFR*(现政比1-0.02067*(CSH水平2*100-CSH水平1)-1.072*(地区政策TFR-地区政策TFR1))
*CASE 现政比1>2.or.地区政策TFR>2
*	可实现TFR0=地区政策TFR
*CASE 现政比1=>0.99.AND.现政比1<=1.01
*	可实现TFR0=地区政策TFR
*ENDCASE

*最优模型=ARDQ多种参数(dqX,13)
最优模型='户SPSS模型'	&&20121021
DO CASE &&F:\分省生育政策仿真_基于普查\政策实现比预测模型.prg	&&20121021
CASE 最优模型='面板模型1'
	 可实现TFR0=地区政策TFR*(2.626-1.036*地区政策TFR)+ARDQ多种参数(dqX,15)
CASE 最优模型='面板模型2'
	 可实现TFR0=地区政策TFR*(现政比1+(2.197-1.219*地区政策TFR-0.011*CSH水平2+0.011*(CSH水平2-CSH水平1)))+ARDQ多种参数(dqX,15)
CASE 最优模型='常SPSS模型'
	 可实现TFR0=地区政策TFR*(3.375-0.017*CSH水平2-0.974*地区政策TFR)+ARDQ多种参数(dqX,15)
CASE 最优模型='户SPSS模型'
	 可实现TFR0=地区政策TFR*(2.486-0.011*CSH水平2-0.667*地区政策TFR)+ARDQ多种参数(dqX,16)
ENDCASE 	&&20121021
******按实际情况，调整可实现TFR0*******
*DO CASE 	&&20121021删
*CASE 地带='西部'.OR.地带='中部'
*	可实现TFR0=IIF(可实现TFR0<地区政策TFR,地区政策TFR,可实现TFR0)
*CASE (地带='东部'.OR.地带='东北').and.可实现TFR0<MITFR
*	可实现TFR0=MITFR
*CASE (地带='东部'.OR.地带='东北').and.可实现TFR0>MITFR.and.可实现TFR0<地区政策TFR
*	可实现TFR0=可实现TFR0
*CASE (地带='东部'.OR.地带='东北').and.可实现TFR0>MITFR.and.可实现TFR0>地区政策TFR
*	可实现TFR0=可实现TFR0
*ENDCASE

 
实现比=可实现TFR0/地区政策TFR
*?'实现比',STR(实现比,4,2),'现政比1',STR(现政比1,4,2),STR(CSH水平1,4,2),STR(CSH水平2*100,4,2),'CSH水平2*100-CSH水平1',STR(CSH水平2*100-CSH水平1,4,2),STR(可实现TFR0,4,2),STR(地区政策TFR,4,2),'可实现TFR0-地区政策TFR',STR(可实现TFR0-地区政策TFR,4,2)
*****可能生育生育的孩子数**************
出生数=0
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女')))
	DO CASE 
	CASE  CX=1.OR.CX=3   &&农村
		NMDFR=N1D2N*实现比
		NFDMR=N2D1N*实现比
		NNR=NNFN*实现比
		城乡生育模式='农村生育模式'
	CASE  CX=2.OR.CX=4
		NMDFR=N1D2C*实现比
		NFDMR=N2D1C*实现比
		NNR=NNFC*实现比
		城乡生育模式='城镇生育模式'
	ENDCASE
	*****************************************************************************************************
	DO 各类夫妇分年龄生育孩子数
	******分释放模式堆积释放计算*****
	 AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
	 *AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
	 AR突释模式=IIF(CX=1.OR.CX=3, 'AR农业突释模式','AR非农突释模式')
	 SUM 单独堆积+双非堆积  TO S堆积  FOR X<=49
	 IF S堆积<>0.and.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		 SELECT A
		 DO 不同政策实现程度下的堆积生育释放法
	 ELSE 
		SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
		REPL 单独释放B WITH 0,双非释放B WITH 0,政策释放B WITH 0,政策生育 WITH 政策双独B+政策NNB+政策NMDFB+政策NFDMB ALL
		REPL 超生双独B   WITH 双独B-政策双独B,;
			 超生NNB   WITH NNB-政策NNB,;
			 超生NMDFB WITH NMDFB-政策NMDFB,;
			 超生NFDMB WITH NFDMB-政策NFDMB,;
			 超生生育  WITH 超生双独B+超生NNB+超生NMDFB+超生NFDMB ALL
		STORE 0 TO 单放B2,非放B2
		单独B2=SNMDF2+SNFDM2
		SNN2=SNN2
	 ENDIF
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类婚配夫妇分年龄生育率
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类堆积释放数及后代分类
ENDFOR
SELECT A
	REPLACE 地区分龄政策生育.&BND2 WITH  地区农村非农子女.政策生育+地区城镇非农子女.政策生育+地区农村农业子女.政策生育+地区城镇农业子女.政策生育 ALL
   	REPLACE 地区农业子女.政策生育 WITH 地区农村农业子女.政策生育+地区城镇农业子女.政策生育,;
			地区非农子女.政策生育 WITH 地区农村非农子女.政策生育+地区城镇非农子女.政策生育,;
 			地区城镇子女.政策生育 WITH 地区城镇农业子女.政策生育+地区城镇非农子女.政策生育,;
 			地区农村子女.政策生育 WITH 地区农村农业子女.政策生育+地区农村非农子女.政策生育,;
 			地区子女.政策生育 WITH 地区农业子女.政策生育+地区非农子女.政策生育 ALL
	REPLACE 地区农村超生.&BND2 WITH  地区农村非农子女.超生生育+地区农村农业子女.超生生育 ALL
	REPLACE 地区城镇超生.&BND2 WITH  地区城镇非农子女.超生生育+地区城镇农业子女.超生生育 ALL
	REPLACE 地区农业超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
	REPLACE 地区非农超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育 ALL
	REPLACE 地区分龄超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育+地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
   	REPLACE 地区农业子女.超生生育 WITH 地区农村农业子女.超生生育+地区城镇农业子女.超生生育,;
		 地区非农子女.超生生育 WITH 地区农村非农子女.超生生育+地区城镇非农子女.超生生育,;
		 地区城镇子女.超生生育 WITH 地区城镇农业子女.超生生育+地区城镇非农子女.超生生育,;
		 地区农村子女.超生生育 WITH 地区农村农业子女.超生生育+地区农村非农子女.超生生育,;
		 地区子女.超生生育 WITH 地区农业子女.超生生育+地区非农子女.超生生育 ALL
	SUM 地区分龄超生.&BND2 TO 超生S
	SUM 地区分龄政策生育.&BND2 TO DQ政策生育
RETURN 


************************************
PROCEDURE 分婚配模式按意愿生育的孩子数
地带=ARDQ多种参数(dqX,14)
地带1=IIF(地带='西部','四川',IIF(地带='东北','东部',地带))
出生数=0
FOR CX=1 TO 4
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女')))
	DO CASE 
	CASE  CX=1.OR.CX=3   &&农村
		NMDFR=N1D2N
		NFDMR=N2D1N
		NNR=NNFN
		城乡生育模式='农村生育模式'
	CASE  CX=2.OR.CX=4
		NMDFR=N1D2C
		NFDMR=N2D1C
		NNR=NNFC
		城乡生育模式='城镇生育模式'
	ENDCASE
	SELECT A
	REPLACE &子女..夫妇合计 WITH &子女..双独+&子女..NMDF+&子女..NFDM+&子女..NN+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非 ALL
	DDFR1=IIF(DDFR<1,DDFR,1)
	NMDFFR1=IIF(NMDFR<1,NMDFR,1)
	NFDMFR1=IIF(NFDMR<1,NFDMR,1)
	NNFR1=IIF(NNR<1,NNR,1)
	DDFR2=IIF(DDFR<1,0,DDFR-1)
	NMDFFR2=IIF(NMDFR<1,0,NMDFR-1)
	NFDMFR2=IIF(NFDMR<1,0,NFDMR-1)
	NNFR2=IIF(NNR<1,0,NNR-1)
	SELECT A
	REPL &子女..双独B WITH &子女..双独*DDFR1*&子女..FR1+&子女..双独*DDFR2*&子女..FR2*想生二胎比.&地带1    ALL  			&&双独及按双独及其所生子女和享受同等政策的其他婚配模式所生子女，城乡无差异
	REPL &子女..NNB WITH &子女..NN*NNFR1*&子女..FR1+&子女..NN*NNFR2*&子女..FR2*想生二胎比.&地带1 	ALL				&&双非分一、二孩。一孩:NN*FR1(=NN*1*FR1)，二孩:(NNR-1)*TF2
	REPL &子女..NMDFB WITH  &子女..NMDF*NMDFFR1*&子女..FR1+&子女..NMDF*NMDFFR2*&子女..FR2*想生二胎比.&地带1  ALL		&&独女分一、二孩
	REPL &子女..NFDMB WITH  &子女..NFDM*NFDMFR1*&子女..FR1+&子女..NFDM*NFDMFR2*&子女..FR2*想生二胎比.&地带1  ALL		&&独男分一、二孩
	
	SUM  &子女..FRD,&子女..双独B,&子女..NMDFB,&子女..NFDMB,&子女..NNB TO SFRD,S双独B,SNMDFB,SNFDMB,SNNB          	&&总和生育率及各婚配模式生育的孩子数(含第一个与第二个)
	SUM  &子女..双独*DDFR1*&子女..FR1,&子女..NMDF*NMDFFR1*&子女..FR1,&子女..NFDM*NFDMFR1*&子女..FR1,&子女..NN*NNFR1*&子女..FR1 TO SDD1,SNMDF1,SNFDM1,SNN1    &&农村\城镇婚配各模式生育的第一个孩子数
	SUM  &子女..双独*DDFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NMDF*NMDFFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NFDM*NFDMFR2*&子女..FR2*想生二胎比.&地带1 ,;
		 &子女..NN*NNFR2*&子女..FR2*想生二胎比.&地带1  TO SDD2,SNMDF2,SNFDM2,SNN2	&&农村\城镇婚配各模式生育的第二个孩子数
	***********************
	REPL &子女..超生双独B WITH &子女..双独B-&子女..政策双独B,&子女..超生NNB WITH &子女..NNB-&子女..政策NNB,&子女..超生NMDFB WITH &子女..NMDFB-&子女..政策NMDFB,;
		 &子女..超生NFDMB WITH &子女..NFDMB-&子女..政策NFDMB,&子女..超生释放B WITH &子女..单独释放B+&子女..双非释放B-&子女..政策释放B,;
		 &子女..超生生育 WITH &子女..超生双独B+&子女..超生NNB+&子女..超生NMDFB+&子女..超生NFDMB+&子女..超生释放B ALL 
	******分释放模式堆积释放计算*****
	 AR缓释模式=IIF(CX=1.OR.CX=3,'AR农业缓释模式','AR非农缓释模式')
	 *AR释放模式=IIF(CX=1.OR.CX=3,'AR农业释放模式','AR非农释放模式')
	 AR突释模式=IIF(CX=1.OR.CX=3, 'AR农业突释模式','AR非农突释模式')
	 SUM &子女..单独堆积+&子女..双非堆积  TO S堆积  FOR X<=49
	 IF S堆积<>0.and.ND0>=调整时机.AND.(ND0-调整时机+1)<=35
		 SELECT A
		 DO 不同政策实现程度下的堆积生育释放法
	 ELSE 
		SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
		REPL 单独释放B WITH 0,双非释放B WITH 0,政策释放B WITH 0,政策生育 WITH 政策双独B+政策NNB+政策NMDFB+政策NFDMB ALL
		REPL 超生双独B   WITH 双独B-政策双独B,;
			 超生NNB   WITH NNB-政策NNB,;
			 超生NMDFB WITH NMDFB-政策NMDFB,;
			 超生NFDMB WITH NFDMB-政策NFDMB,;
			 超生生育  WITH 超生双独B+超生NNB+超生NMDFB+超生NFDMB ALL
		STORE 0 TO 单放B2,非放B2
		单独B2=SNMDF2+SNFDM2
		SNN2=SNN2
	 ENDIF
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类婚配夫妇分年龄生育率
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	 DO 各类堆积释放数及后代分类
	*出生数=出生数+(dd1+DD2+NMDF1+NFDM1+单独B2+NN1+NN2)
	 ENDFOR
SELECT A
	REPLACE 地区分龄政策生育.&BND2 WITH  地区农村非农子女.政策生育+地区城镇非农子女.政策生育+地区农村农业子女.政策生育+地区城镇农业子女.政策生育 ALL
   	REPLACE 地区农业子女.政策生育 WITH 地区农村农业子女.政策生育+地区城镇农业子女.政策生育,;
			地区非农子女.政策生育 WITH 地区农村非农子女.政策生育+地区城镇非农子女.政策生育,;
 			地区城镇子女.政策生育 WITH 地区城镇农业子女.政策生育+地区城镇非农子女.政策生育,;
 			地区农村子女.政策生育 WITH 地区农村农业子女.政策生育+地区农村非农子女.政策生育,;
 			地区子女.政策生育 WITH 地区农业子女.政策生育+地区非农子女.政策生育 ALL
	REPLACE 地区农村超生.&BND2 WITH  地区农村非农子女.超生生育+地区农村农业子女.超生生育 ALL
	REPLACE 地区城镇超生.&BND2 WITH  地区城镇非农子女.超生生育+地区城镇农业子女.超生生育 ALL
	REPLACE 地区农业超生.&BND2 WITH  地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
	REPLACE 地区非农超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育 ALL
	REPLACE 地区分龄超生.&BND2 WITH  地区农村非农子女.超生生育+地区城镇非农子女.超生生育+地区农村农业子女.超生生育+地区城镇农业子女.超生生育 ALL
   	REPLACE 地区农业子女.超生生育 WITH 地区农村农业子女.超生生育+地区城镇农业子女.超生生育,;
		 地区非农子女.超生生育 WITH 地区农村非农子女.超生生育+地区城镇非农子女.超生生育,;
		 地区城镇子女.超生生育 WITH 地区城镇农业子女.超生生育+地区城镇非农子女.超生生育,;
		 地区农村子女.超生生育 WITH 地区农村农业子女.超生生育+地区农村非农子女.超生生育,;
		 地区子女.超生生育 WITH 地区农业子女.超生生育+地区非农子女.超生生育 ALL
	SUM 地区分龄超生.&BND2 TO 超生S
	SUM 地区分龄政策生育.&BND2 TO DQ政策生育
*?"按孩次合计的出生数：",出生数,"政策生育:",DQ政策生育,超生S,"按孩次合计的出生数-政策生育-超生S:",出生数-DQ政策生育-超生S,单放B2
RETURN


****************************************
PROCEDURE 各类堆积释放数及后代分类
	 SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	****************************
	SUM  合J分龄SYL,双D分龄SYL,单D分龄SYL,双N分龄SYL TO STFR,DDTFR, 单DTFR,NNTFR
	SUM (双独B+NMDFB+NFDMB+NNB+单独释放B+双非释放B)/(双独+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非) TO 婚内TFR	FOR (双独+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非)<>0								&&所有婚配模式合计的分年龄生育率
	DO CASE		&&记录堆积释放结果
	CASE CX=1	 &&SELE 61 :地区农村农业子女
	  	农村农业一孩=SDD1+SNMDF1+SNFDM1+SNN1
	  	农村农业单放B2=单放B2
	  	农村农业非放B2=非放B2
	 	NCNY婚内TFR=婚内TFR
	 	SUM 单独堆积,双非堆积 TO 农村农业单独堆积,农村农业双非堆积  FOR X>=15.AND.X<=49
	CASE CX=2	 &&SELE62 :地区农村非农子女
	  	农村非农一孩=SDD1+SNMDF1+SNFDM1+SNN1
	  	农村非农单放B2=单放B2
	  	农村非农非放B2=非放B2
	  	NCFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 农村非农单独堆积,农村非农双非堆积 FOR X>=15.AND.X<=49
	CASE CX=3	 &&SELE63 :地区城镇农业子女
	  	城镇农业一孩=SDD1+SNMDF1+SNFDM1+SNN1
	  	城镇农业单放B2=单放B2
	  	城镇农业非放B2=非放B2
	  	CZNY婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇农业单独堆积,城镇农业双非堆积 FOR X>=15.AND.X<=49
	CASE CX=4	 &&SELE64 :地区城镇非农子女
	  	城镇非农一孩=SDD1+SNMDF1+SNFDM1+SNN1
	  	城镇非农单放B2=单放B2
	  	城镇非农非放B2=非放B2
	  	CZFN婚内TFR=婚内TFR
	  	SUM 单独堆积,双非堆积 TO 城镇非农单独堆积,城镇非农双非堆积 FOR X>=15.AND.X<=49
	ENDCASE
	
	
	**************************************************************************
	*	对所生孩子进行分类，确定下一代享受不同政策的人数（只对零岁组操作）   *
	**************************************************************************
	DO 按后代政策对出生孩子分类
	****************************
	SELE 50		&&USE &生育孩次数 ALIA 生育孩次
	LOCA FOR 年份=ND0
	DO CASE
	CASE CX=1	&&SELE 61 :地区农村农业子女
		REPL 乡NY双独B1 WITH SDD1,乡NY双独B2 WITH SDD2,乡NY单独B1 WITH SNMDF1+SNFDM1,乡NY单独B2 WITH 单独B2,乡NY双非B1 WITH SNN1,乡NY双非B2 WITH SNN2
	CASE CX=2	&&SELE 62 :地区农村非农子女
		REPL 乡FN双独B1 WITH SDD1,乡FN双独B2 WITH SDD2,乡FN单独B1 WITH SNMDF1+SNFDM1,乡FN单独B2 WITH 单独B2,乡FN双非B1 WITH SNN1,乡FN双非B2 WITH SNN2								
	CASE CX=3	&&SELE 63 :地区城镇农业子女
		REPL 城NY双独B1 WITH SDD1,城NY双独B2 WITH SDD2,城NY单独B1 WITH SNMDF1+SNFDM1,城NY单独B2 WITH 单独B2,城NY双非B1 WITH SNN1,城NY双非B2 WITH SNN2
	CASE CX=4	&&SELE 64 :地区城镇非农子女							
		REPL 城FN双独B1 WITH SDD1,城FN双独B2 WITH SDD2,城FN单独B1 WITH SNMDF1+SNFDM1,城FN单独B2 WITH 单独B2,城FN双非B1 WITH SNN1,城FN双非B2 WITH SNN2
	ENDCASE
*?dd1,DD2,NMDF1+NFDM1,单独B2,NN1,NN2,dd1+DD2+NMDF1+NFDM1+单独B2+NN1+NN2
RETURN 


************************************
PROCEDURE 各类夫妇分年龄生育孩子数
REPLACE 夫妇合计 WITH 双独+NMDF+NFDM+NN+单独堆积+双非堆积+已2单独+已2双非 ALL

DDFR1=IIF(DDFR<1,DDFR,1)*(1-无活产比)
NMDFFR1=IIF(NMDFR<1,NMDFR,1)*(1-无活产比)
NFDMFR1=IIF(NFDMR<1,NFDMR,1)*(1-无活产比)
NNFR1=IIF(NNR<1,NNR,1)*(1-无活产比)

DDFR2=IIF(DDFR<1,0,DDFR-DDFR1)
NMDFFR2=IIF(NMDFR<1,0,NMDFR-NMDFFR1)
NFDMFR2=IIF(NFDMR<1,0,NFDMR-NFDMFR1)
NNFR2=IIF(NNR<1,0,NNR-NNFR1)

REPL 双独B1 WITH 双独*DDFR1*FR1 ALL  			&&双独及按双独及其所生子女和享受同等政策的其他婚配模式所生子女，城乡无差异
REPL NNB1 WITH NN*NNFR1*FR1 ALL				&&双非分一、二孩。一孩:NN*FR1(=NN*1*FR1)，二孩:(NNR-1)*TF2
REPL NMDFB1 WITH  NMDF*NMDFFR1*FR1 ALL
REPL NFDMB1 WITH  NFDM*NFDMFR1*FR1 ALL
REPLACE 单独B1 WITH NMDFB1+NFDMB1 ALL
REPLACE B1 WITH 双独B1+NMDFB1+NFDMB1+NNB1 ALL

REPL 双独B2 WITH 双独*DDFR2*FR2   ALL  			&&双独及按双独及其所生子女和享受同等政策的其他婚配模式所生子女，城乡无差异
REPL NNB2 WITH NN*NNFR2*FR2	ALL				&&双非分一、二孩。一孩:NN*FR1(=NN*1*FR1)，二孩:(NNR-1)*TF2
REPL NMDFB2 WITH NMDF*NMDFFR2*FR2 ALL	&&独女分一、二孩
REPL NFDMB2 WITH NFDM*NFDMFR2*FR2 ALL	&&独男分一、二孩
REPLACE 单独B2 WITH NMDFB2+NFDMB2 ALL
REPLACE B2 WITH 双独B2+NMDFB2+NFDMB2+NNB2 ALL

REPL 双独B WITH 双独*DDFR1*FR1+双独*DDFR2*FR2   ALL  			&&双独及按双独及其所生子女和享受同等政策的其他婚配模式所生子女，城乡无差异
REPL NNB WITH NN*NNFR1*FR1+NN*NNFR2*FR2	ALL				&&双非分一、二孩。一孩:NN*FR1(=NN*1*FR1)，二孩:(NNR-1)*TF2
REPL NMDFB WITH  NMDF*NMDFFR1*FR1+NMDF*NMDFFR2*FR2 ALL	&&独女分一、二孩
REPL NFDMB WITH  NFDM*NFDMFR1*FR1+NFDM*NFDMFR2*FR2 ALL	&&独男分一、二孩
REPLACE 单独B2 WITH NMDFB+NFDMB ALL
REPLACE B WITH b1+B2 ALL

*SUM  FRD,DDB,NMDFB,NFDMB,NNB TO A,DB,B2,B3,B4          	&&总和生育率及各婚配模式生育的孩子数(含第一个与第二个)
*SUM  DD*DDFR1*FR1,NMDF*NMDFFR1*FR1,NFDM*NFDMFR1*FR1,NN*NNFR1*FR1 TO DD1,NMDF1,NFDM1,NN1    &&农村\城镇婚配各模式生育的第一个孩子数
*SUM  DD*DDFR2*FR2,NMDF*NMDFFR2*FR2,NFDM*NFDMFR2*FR2,NN*NNFR2*FR2 TO DD2,NMDF2,NFDM2,NN2	&&农村\城镇婚配各模式生育的第二个孩子数
SUM  FRD,双独B,NMDFB,NFDMB,NNB TO SFRD,S双独B,SNMDFB,SNFDMB,SNNB          	&&总和生育率及各婚配模式生育的孩子数(含第一个与第二个)
SUM  双独*DDFR1*FR1,NMDF*NMDFFR1*FR1,NFDM*NFDMFR1*FR1,NN*NNFR1*FR1 TO SDD1,SNMDF1,SNFDM1,SNN1    &&农村\城镇婚配各模式生育的第一个孩子数
SUM  双独*DDFR2*FR2,NMDF*NMDFFR2*FR2,NFDM*NFDMFR2*FR2,NN*NNFR2*FR2 TO SDD2,SNMDF2,SNFDM2,SNN2	&&农村\城镇婚配各模式生育的第二个孩子数
RETURN 

*********************************
PROC 常住人口同龄概率法
FOR CX=1 TO 4 
	SELE A	
	子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
	DO CASE
	CASE 	结婚率法='全员婚配'
		REPL &子女..DF0 WITH &子女..DF+&子女..DDBF,&子女..DM0 WITH &子女..DM+&子女..DDBM,&子女..NF0 WITH &子女..NF,&子女..NM0 WITH &子女..NM ALL
	CASE 	结婚率法='有偶率婚配'
		有偶率=IIF(农非=1,'乡村','城镇')-'有偶率'
		REPL &子女..DF0 WITH (&子女..DF+&子女..DDBF)*&有偶率..F有偶B,&子女..DM0 WITH (&子女..DM+&子女..DDBM)*&有偶率..M有偶B,&子女..NF0 WITH &子女..NF*&有偶率..F有偶B,&子女..NM0 WITH &子女..NM*&有偶率..M有偶B ALL
	ENDCASE
	REPL &子女..DF0 WITH 0 FOR &子女..DF0<0
	REPL &子女..DM0 WITH 0 FOR &子女..DM0<0
	REPL &子女..NF0 WITH 0 FOR &子女..NF0<0
	REPL &子女..NM0 WITH 0 FOR &子女..NM0<0
	RELE AR1
	COPY TO ARRA AR1 FIEL &子女..DF0,&子女..NF0
	REPL &子女..双独概率M WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..女单概率M WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..男单概率M WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..双非概率M WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	RELE AR1
	COPY TO ARRA AR1 FIEL &子女..DM0,&子女..NM0
	REPL &子女..双独概率F WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..男单概率F WITH AR1(RECN(),1)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..女单概率F WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..双非概率F WITH AR1(RECN(),2)/(AR1(RECN(),1)+AR1(RECN(),2)) FOR (AR1(RECN(),1)+AR1(RECN(),2))<>0
	REPL &子女..双独   WITH MIN(&子女..DM0*&子女..双独概率M,&子女..DF0*&子女..双独概率F)  ALL
	REPL &子女..NMDF WITH MIN(&子女..NM0*&子女..女单概率M,&子女..DF0*&子女..女单概率F)  ALL
	REPL &子女..NFDM WITH MIN(&子女..DM0*&子女..男单概率M,&子女..NF0*&子女..男单概率F) ALL
	REPL &子女..NN   WITH MIN(&子女..NM0*&子女..双非概率M,&子女..NF0*&子女..双非概率F)  ALL
	*****对同龄概率可以广使用如下语句，因为同年龄同性别应该平衡*****
	REPL &子女..DF10 WITH &子女..DF0-&子女..双独-&子女..NMDF ALL
	REPL &子女..DM10 WITH &子女..DM0-&子女..双独-&子女..NFDM ALL
	REPL &子女..NF10 WITH &子女..NF0-&子女..NN-&子女..NFDM ALL
	REPL &子女..NM10 WITH &子女..NM0-&子女..NN-&子女..NMDF ALL
	***对剩余女性为负数的进行调整
	RELE ARDF,ARNF
	COPY TO ARRA ARDF FIEL &子女..双独,&子女..NMDF
	REPL &子女..双独 WITH &子女..DF0*(ARDF(RECN(),1)/(ARDF(RECN(),1)+ARDF(RECN(),2))),&子女..NMDF WITH &子女..DF0*(ARDF(RECN(),2)/(ARDF(RECN(),1)+ARDF(RECN(),2))) FOR &子女..DF0-(ARDF(RECN(),1)+ARDF(RECN(),2))<0
	COPY TO ARRA ARNF FIEL &子女..NN,&子女..NFDM
	REPL &子女..NN WITH &子女..NF0*(ARNF(RECN(),1)/(ARNF(RECN(),1)+ARNF(RECN(),2))),&子女..NFDM WITH &子女..NF0*(ARNF(RECN(),2)/(ARNF(RECN(),1)+ARNF(RECN(),2))) FOR &子女..NF0-(ARNF(RECN(),1)+ARNF(RECN(),2))<0
	***对剩余男性为负数的进行调整
	RELE ARDM,ARNM
	COPY TO ARRA ARDM FIEL &子女..双独,&子女..NFDM
	REPL &子女..双独 WITH &子女..DM0*(ARDM(RECN(),1)/(ARDM(RECN(),1)+ARDM(RECN(),2))),&子女..NFDM WITH &子女..DM0*(ARDM(RECN(),2)/(ARDM(RECN(),1)+ARDM(RECN(),2))) FOR &子女..DM0-(ARDM(RECN(),1)+ARDM(RECN(),2))<0
	COPY TO ARRA ARNM FIEL &子女..NN,&子女..NMDF
	REPL &子女..NN WITH &子女..NM0*(ARNM(RECN(),1)/(ARNM(RECN(),1)+ARNM(RECN(),2))),&子女..NMDF WITH &子女..NM0*(ARNM(RECN(),2)/(ARNM(RECN(),1)+ARNM(RECN(),2))) FOR &子女..NM0-(ARNM(RECN(),1)+ARNM(RECN(),2))<0
	*****记录平均初婚年龄的各种婚配概率:双独概率M,双独概率F,男单概率M,男单概率F,女单概率M,女单概率F
	DO CASE	
		&&AR初婚年龄 FIEL 地区,城市M,城市F,乡村M,乡村F,
		&&子女=IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		CASE CX=1	&&CX=1,'农村农业'
			LOCA FOR X=ROUN(X初婚NLM,0)
			乡NY双独GLM=&子女..双独概率M
			乡NY男单GLM=&子女..男单概率M
			乡NY女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(X初婚NLF,0)
			乡NY双独GLF=&子女..双独概率F
			乡NY男单GLF=&子女..男单概率F
			乡NY女单GLF=&子女..女单概率F
		CASE CX=2	&&CX=2,'农村非农'
			LOCA FOR X=ROUN(C初婚NLM,0)
			乡FN双独GLM=&子女..双独概率M
			乡FN男单GLM=&子女..男单概率M
			乡FN女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(C初婚NLF,0)
			乡FN双独GLF=&子女..双独概率F
			乡FN男单GLF=&子女..男单概率F
			乡FN女单GLF=&子女..女单概率F
		CASE CX=3	&&IIF(CX=3,'城镇农业'
			LOCA FOR X=ROUN(X初婚NLM,0)
			城NY双独GLM=&子女..双独概率M
			城NY男单GLM=&子女..男单概率M
			城NY女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(X初婚NLF,0)
			城NY双独GLF=&子女..双独概率F
			城NY男单GLF=&子女..男单概率F
			城NY女单GLF=&子女..女单概率F
		CASE CX=4	&&(CX=4,'城镇非农'
			LOCA FOR X=ROUN(C初婚NLM,0)
			城FN双独GLM=&子女..双独概率M
			城FN男单GLM=&子女..男单概率M
			城FN女单GLM=&子女..女单概率M
			LOCA FOR X=ROUN(C初婚NLF,0)
			城FN双独GLF=&子女..双独概率F
			城FN男单GLF=&子女..男单概率F
			城FN女单GLF=&子女..女单概率F
	ENDCASE
ENDFOR
*********************************
*	农村农业与非农业子女婚配	*
*	城镇农业与非农业子女婚配	*
*********************************
SELE A
FOR CX=1 TO 2
	非农子女='地区'-IIF(CX=1,'农村','城镇')-'非农子女'
	农业子女='地区'-IIF(CX=1,'农村','城镇')-'农业子女'
	***** 1.非农独男、农业独女
	RELE AR农业F,AR非农M,AR非农F,AR农业M
	COPY TO ARRA AR农业F FIEL &农业子女..DF10,&农业子女..NF10
	COPY TO ARRA AR非农M FIEL &非农子女..DM10,&非农子女..NM10
	COPY TO ARRA AR非农F FIEL &非农子女..DF10,&非农子女..NF10
	COPY TO ARRA AR农业M FIEL &农业子女..DM10,&农业子女..NM10

	REPL &农业子女..NF双独GLF WITH AR非农M(RECN(),1)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &农业子女..NF双独GLM WITH AR非农F(RECN(),1)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &非农子女..NF双独GLF WITH AR农业M(RECN(),1)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0
	REPL &非农子女..NF双独GLM WITH AR农业F(RECN(),1)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &农业子女..DD0  WITH  MIN(&非农子女..DM10*&非农子女..NF双独GLM,&农业子女..DF10*&农业子女..NF双独GLF) ALL		&&1.非农独男、农业独女
	REPL &非农子女..DD0  WITH  MIN(&农业子女..DM10*&农业子女..NF双独GLM,&非农子女..DF10*&非农子女..NF双独GLF) ALL		&&2.非农独女、农业独男
	REPL &农业子女..双独   WITH  &农业子女..双独+&农业子女..DD0 ALL
	REPL &非农子女..双独   WITH  &非农子女..双独+&非农子女..DD0 ALL

	REPL &农业子女..NF女单GLM WITH AR非农F(RECN(),1)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &农业子女..NF男单GLM WITH AR非农F(RECN(),2)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &非农子女..NF女单GLM WITH AR农业F(RECN(),1)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &非农子女..NF男单GLM WITH AR农业F(RECN(),2)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0

	REPL &农业子女..NF女单GLF WITH AR非农M(RECN(),1)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &农业子女..NF男单GLF WITH AR非农M(RECN(),2)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &非农子女..NF女单GLF WITH AR农业M(RECN(),1)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0
	REPL &非农子女..NF男单GLF WITH AR农业M(RECN(),2)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0

	REPL &农业子女..NFDM WITH &农业子女..NFDM+MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL		&&非农独男、农业非独女
	REPL &非农子女..NMDF WITH &非农子女..NMDF+MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM) ALL		&&非农独女、农业非独男
	REPL &农业子女..NMDF WITH &农业子女..NMDF+MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM) ALL		&&非农非独男、农业独女
	REPL &非农子女..NFDM WITH &非农子女..NFDM+MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL		&&非农非独女、农业独男

	REPL &农业子女..NF双非GLM WITH AR非农F(RECN(),2)/(AR非农F(RECN(),1)+AR非农F(RECN(),2)) FOR (AR非农F(RECN(),1)+AR非农F(RECN(),2))<>0
	REPL &农业子女..NF双非GLF WITH AR非农M(RECN(),2)/(AR非农M(RECN(),1)+AR非农M(RECN(),2)) FOR (AR非农M(RECN(),1)+AR非农M(RECN(),2))<>0
	REPL &非农子女..NF双非GLM WITH AR农业F(RECN(),2)/(AR农业F(RECN(),1)+AR农业F(RECN(),2)) FOR (AR农业F(RECN(),1)+AR农业F(RECN(),2))<>0
	REPL &非农子女..NF双非GLF WITH AR农业M(RECN(),2)/(AR农业M(RECN(),1)+AR农业M(RECN(),2)) FOR (AR农业M(RECN(),1)+AR农业M(RECN(),2))<>0

	REPL  &农业子女..NN   WITH  &农业子女..NN+MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)  ALL		&&非农非独男、农业非独女
	REPL  &非农子女..NN   WITH  &非农子女..NN+MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF)  ALL		&&农业非独男、非农非独女
	*****农业和非农业夫妇后未婚*****
	REPL &农业子女..DF20 WITH &农业子女..DF10-&农业子女..DD0-MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM) ALL
	REPL &农业子女..DM20 WITH &农业子女..DM10-&非农子女..DD0-MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL
	REPL &农业子女..NF20 WITH &农业子女..NF10-MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)-MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL
	REPL &农业子女..NM20 WITH &农业子女..NM10-MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM)-MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF) ALL

	****
	REPL &非农子女..DM20 WITH &非农子女..DM10-&农业子女..DD0-MIN(&农业子女..NF10*&农业子女..NF男单GLF,&非农子女..DM10*&非农子女..NF男单GLM) ALL
	REPL &非农子女..DF20 WITH &非农子女..DF10-&非农子女..DD0-MIN(&非农子女..DF10*&非农子女..NF女单GLF,&农业子女..NM10*&农业子女..NF女单GLM) ALL
	REPL &非农子女..NF20 WITH &非农子女..NF10-MIN(&农业子女..NM10*&农业子女..NF双非GLM,&非农子女..NF10*&非农子女..NF双非GLF)-MIN(&非农子女..NF10*&非农子女..NF男单GLF,&农业子女..DM10*&农业子女..NF男单GLM) ALL
	REPL &非农子女..NM20 WITH &非农子女..NM10-MIN(&非农子女..NM10*&非农子女..NF双非GLM,&农业子女..NF10*&农业子女..NF双非GLF)-MIN(&农业子女..DF10*&农业子女..NF女单GLF,&非农子女..NM10*&非农子女..NF女单GLM)  ALL

	SUM  &农业子女..DF20,&农业子女..DM20,&农业子女..NF20,&农业子女..NM20,&非农子女..DF20,&非农子女..DM20,&非农子女..NF20,&非农子女..NM20 ;
		TO NY未婚DF,NY未婚DM,NY未婚NF,NY未婚NM,FN未婚DF,FN未婚DM,FN未婚NF,FN未婚NM FOR X>=30.AND.X<=49
	*****************************
	REPL &农业子女..NMDF WITH 0 FOR &农业子女..NMDF<0
	REPL &农业子女..NFDM WITH 0 FOR &农业子女..NFDM<0				
	REPL &非农子女..NFDM WITH 0 FOR &非农子女..NFDM<0
	REPL &非农子女..NMDF WITH 0 FOR &非农子女..NMDF<0
ENDFOR
RETU


*******************
PROCEDURE 分龄人口合计2
REPL 地区农村农业子女.D    WITH 地区农村农业子女.DF+地区农村农业子女.DM,地区农村农业子女.N    WITH 地区农村农业子女.NF+地区农村农业子女.NM ALL
REPL 地区农村非农子女.D    WITH 地区农村非农子女.DF+地区农村非农子女.DM,地区农村非农子女.N    WITH 地区农村非农子女.NF+地区农村非农子女.NM ALL
REPL 地区城镇农业子女.D    WITH 地区城镇农业子女.DF+地区城镇农业子女.DM,地区城镇农业子女.N    WITH 地区城镇农业子女.NF+地区城镇农业子女.NM ALL
REPL 地区城镇非农子女.D    WITH 地区城镇非农子女.DF+地区城镇非农子女.DM,地区城镇非农子女.N    WITH 地区城镇非农子女.NF+地区城镇非农子女.NM ALL

REPL 地区农村非农子女.HJM WITH 地区农村非农子女.DM+地区农村非农子女.DDBM+地区农村非农子女.NM,;
	 地区农村非农子女.HJF WITH 地区农村非农子女.DF+地区农村非农子女.DDBF+地区农村非农子女.NF,;
	 地区农村非农子女.HJ WITH 地区农村非农子女.HJM+地区农村非农子女.HJF ALL
	 
REPL 地区农村农业子女.HJM WITH 地区农村农业子女.DM+地区农村农业子女.DDBM+地区农村农业子女.NM+地区农村农业子女.堆积丈夫,;
	 地区农村农业子女.HJF WITH 地区农村农业子女.DF+地区农村农业子女.DDBF+地区农村农业子女.NF+地区农村农业子女.单独堆积+地区农村农业子女.双非堆积+地区农村农业子女.已2单独+地区农村农业子女.已2双非,;
	 地区农村农业子女.HJ WITH 地区农村农业子女.HJM+地区农村农业子女.HJF ALL
	 
REPL 地区城镇非农子女.HJM WITH 地区城镇非农子女.DM+地区城镇非农子女.DDBM+地区城镇非农子女.NM+地区城镇非农子女.堆积丈夫,;
	 地区城镇非农子女.HJF WITH 地区城镇非农子女.DF+地区城镇非农子女.DDBF+地区城镇非农子女.NF+地区城镇非农子女.单独堆积+地区城镇非农子女.双非堆积+地区城镇非农子女.已2单独+地区城镇非农子女.已2双非,;
	 地区城镇非农子女.HJ WITH 地区城镇非农子女.HJM+地区城镇非农子女.HJF ALL
	 
REPL 地区城镇农业子女.HJM WITH 地区城镇农业子女.DM+地区城镇农业子女.DDBM+地区城镇农业子女.NM+地区城镇农业子女.堆积丈夫,;
	 地区城镇农业子女.HJF WITH 地区城镇农业子女.DF+地区城镇农业子女.DDBF+地区城镇农业子女.NF+地区城镇农业子女.单独堆积+地区城镇农业子女.双非堆积+地区城镇农业子女.已2单独+地区城镇农业子女.已2双非,;
	 地区城镇农业子女.HJ WITH 地区城镇农业子女.HJM+地区城镇农业子女.HJF ALL

REPL 地区非农子女.DF  WITH 地区城镇非农子女.DF+地区农村非农子女.DF,地区非农子女.DM  WITH  地区城镇非农子女.DM+地区农村非农子女.DM,地区非农子女.D  WITH  地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.NF  WITH 地区城镇非农子女.NF+地区农村非农子女.NF,地区非农子女.NM  WITH  地区城镇非农子女.NM+地区农村非农子女.NM,地区非农子女.N  WITH  地区非农子女.NM+地区非农子女.NF ALL
REPL 地区非农子女.N   WITH 地区非农子女.NM+地区非农子女.NF,地区非农子女.D WITH 地区非农子女.DM+地区非农子女.DF ALL
REPL 地区非农子女.HJF WITH 地区城镇非农子女.HJF+地区农村非农子女.HJF,地区非农子女.HJM  WITH  地区城镇非农子女.HJM+地区农村非农子女.HJM,地区非农子女.HJ WITH  地区非农子女.HJF+地区非农子女.HJM ALL
REPL 地区农业子女.DF  WITH  地区城镇农业子女.DF+地区农村农业子女.DF,地区农业子女.DM  WITH  地区城镇农业子女.DM+地区农村农业子女.DM,地区农业子女.D  WITH  地区农业子女.DM+地区农业子女.DF ALL
REPL 地区农业子女.NF  WITH  地区城镇农业子女.NF+地区农村农业子女.NF,地区农业子女.NM  WITH  地区城镇农业子女.NM+地区农村农业子女.NM,地区农业子女.N  WITH  地区农业子女.NM+地区农业子女.NF ALL
REPL 地区农业子女.HJF WITH  地区城镇农业子女.HJF+地区农村农业子女.HJF,地区农业子女.HJM  WITH  地区城镇农业子女.HJM+地区农村农业子女.HJM,地区农业子女.HJ WITH  地区农业子女.HJF+地区农业子女.HJM ALL

REPL 地区子女.DF  WITH  地区城镇非农子女.DF+地区城镇农业子女.DF+地区农村非农子女.DF+地区农村农业子女.DF,;
	 地区子女.DM  WITH  地区城镇非农子女.DM+地区城镇农业子女.DM+地区农村非农子女.DM+地区农村农业子女.DM,地区子女.D  WITH  地区子女.DM+地区子女.DF ALL
REPL 地区子女.NF  WITH  地区城镇非农子女.NF+地区城镇农业子女.NF+地区农村非农子女.NF+地区农村农业子女.NF,;
 						 地区子女.NM  WITH  地区城镇非农子女.NM+地区城镇农业子女.NM+地区农村非农子女.NM+地区农村农业子女.NM,地区子女.N  WITH  地区子女.NM+地区子女.NF ALL
REPL 地区子女.HJF WITH  地区子女.DF+地区子女.NF,地区子女.HJM  WITH  地区子女.DM+地区子女.NM,地区子女.HJ  WITH  地区子女.HJM+地区子女.HJF ALL

REPL 地区农村非农分龄.&MND2 WITH 地区农村非农子女.HJM,地区农村非农分龄.&FND2 WITH 地区农村非农子女.HJF ALL
REPL 地区城镇非农分龄.&MND2 WITH 地区城镇非农子女.HJM,地区城镇非农分龄.&FND2 WITH 地区城镇非农子女.HJF ALL
REPL 地区非农分龄.&MND2 WITH 地区农村非农分龄.&MND2+地区城镇非农分龄.&MND2,地区非农分龄.&FND2 WITH 地区农村非农分龄.&FND2+地区城镇非农分龄.&FND2 ALL
REPL 地区农村农业分龄.&MND2 WITH 地区农村农业子女.HJM,地区农村农业分龄.&FND2 WITH 地区农村农业子女.HJF ALL
REPL 地区城镇农业分龄.&MND2 WITH 地区城镇农业子女.HJM,地区城镇农业分龄.&FND2 WITH 地区城镇农业子女.HJF ALL
REPL 地区农业分龄.&MND2 WITH 地区农村农业分龄.&MND2+地区城镇农业分龄.&MND2,地区农业分龄.&FND2 WITH 地区农村农业分龄.&FND2+地区城镇农业分龄.&FND2 ALL
RETU


********单独\双非堆积年龄移算********
*PROC  堆积年龄移算
COPY TO ARRA AR堆积1 FIEL 农村农业子女.单独堆积,农村农业子女.双非堆积
REPL 农村农业子女.单独堆积 WITH AR堆积1(RECN()-1,1)*(1-NYQM(RECN()-1)), 农村农业子女.双非堆积 WITH AR堆积1(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
COPY TO ARRA AR堆积2 FIEL 农村非农子女.单独堆积,农村非农子女.双非堆积
REPL 农村非农子女.单独堆积 WITH AR堆积2(RECN()-1,1)*(1-FNQM(RECN()-1)), 农村非农子女.双非堆积 WITH AR堆积2(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
COPY TO ARRA AR堆积3 FIEL 城镇农业子女.单独堆积,城镇农业子女.双非堆积
REPL 城镇农业子女.单独堆积 WITH AR堆积3(RECN()-1,1)*(1-NYQM(RECN()-1)), 城镇农业子女.双非堆积 WITH AR堆积3(RECN()-1,2)*(1-NYQF(RECN()-1)) FOR RECN()>1
COPY TO ARRA AR堆积4 FIEL 城镇非农子女.单独堆积,城镇非农子女.双非堆积
REPL 城镇非农子女.单独堆积 WITH AR堆积4(RECN()-1,1)*(1-FNQM(RECN()-1)), 城镇非农子女.双非堆积 WITH AR堆积4(RECN()-1,2)*(1-FNQF(RECN()-1)) FOR RECN()>1
RETU 
*************************************
PROC 记录各婚配模式夫妇人数
REPL 非农政策1 WITH ALLT(非农ZC1)-IIF(非农时机1=0,'',ALLT(STR(非农时机1,4))),;
	 农业政策1 WITH ALLT(农业ZC1)-IIF(农业时机1=0,'',ALLT(STR(农业时机1,4))),;
	 非农政策2 WITH ALLT(非农ZC2)-IIF(非农时机2=0,'',ALLT(STR(非农时机2,4))),;
	 农业政策2 WITH ALLT(农业ZC2)-IIF(农业时机2=0,'',ALLT(STR(农业时机2,4))),;
	 非农政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(非农ZC3)-ALLT(STR(非农时机3,4)),''),;
	 农业政策3 WITH IIF(调整方式时机='多地区渐进普二',ALLT(农业ZC3)-ALLT(STR(农业时机3,4)),''),;
	双独概率F WITH IIF(CX=1,乡NY双独GLF,IIF(CX=2,乡FN双独GLF,IIF(CX=3,城NY双独GLF,城FN双独GLF))),;
	双独概率M WITH IIF(CX=1,乡NY双独GLM,IIF(CX=2,乡FN双独GLM,IIF(CX=3,城NY双独GLM,城FN双独GLM))),;
	男单概率M  WITH IIF(CX=1,乡NY男单GLM,IIF(CX=2,乡FN男单GLM,IIF(CX=3,城NY男单GLM,城FN男单GLM))),;
	男单概率F  WITH IIF(CX=1,乡NY男单GLF,IIF(CX=2,乡FN男单GLF,IIF(CX=3,城NY男单GLF,城FN男单GLF))),;
	女单概率M  WITH IIF(CX=1,乡NY女单GLM,IIF(CX=2,乡FN女单GLM,IIF(CX=3,城NY女单GLM,城FN女单GLM))),;
	女单概率F  WITH IIF(CX=1,乡NY女单GLF,IIF(CX=2,乡FN女单GLF,IIF(CX=3,城NY女单GLF,城FN女单GLF))),;
	女单生育数 WITH SNMDFB, 男单生育数 WITH SNFDMB,单独生育数  WITH 女单生育数+男单生育数,双非生育数 WITH SNNB,TFR WITH SFRD,双独生育数  WITH S双独B,;
	非农双独 WITH DDFR,农业双独 WITH DDFR,非农女单 WITH N1D2C,农业女单 WITH N1D2N,;
	非农男单 WITH N2D1C,农业男单 WITH N2D1N,非农双非 WITH NNFC,农业双非 WITH NNFN,;
	DM WITH DMS,DF WITH DFS,NM WITH NMS,NF WITH NFS,双独夫妇数 WITH 双独S,女单夫妇数 WITH NMDFS,男单夫妇数 WITH NFDMS,单独夫妇数  WITH  女单夫妇数+男单夫妇数+单独堆积+已2单独,;
	双非夫妇数 WITH NNS ,D WITH DS,N WITH NS,可二后 WITH DDB,DDBM WITH DDBMS,DDBF WITH DDBFS,合计夫妇数 WITH 双独夫妇数+单独夫妇数+双非夫妇数,;
RETU



*************************************************************************
*	年龄分组和年龄结构													*
*	一般分为新生儿期、婴儿期、幼儿期和学龄儿童期。 							*
*	把孕满28周至产后满7天这一时期称为围产期；这一时期内的胎、婴儿称为围产儿。*
*	新生儿指出生至满28天													*
*	婴儿期指出生不满１岁的孩子。											*
*	幼儿期指1周岁至不满3周岁的儿童。										*
*	学龄前儿童是指：3周岁至不满7周岁的儿童。								*
*************************************************************************

*****************************
*	基年年龄分组和年龄结构	*
*****************************
PROC 基年年龄分组和年龄结构计算
SUM (M2009+F2009),M2009,F2009 TO 总人口T,总人口M,总人口F
男性07=50600000*总人口M/总人口T
女性07=50600000-男性07
REPL M2009 WITH 男性07*M2009/总人口M,F2009 WITH 女性07*F2009/总人口F ALL
SUM (M2009+F2009)/10000,M2009/10000,F2009/10000 TO 总人口T,总人口M,总人口F
SUM (M2009+F2009)/10000 TO T少年 FOR X<=14				&&国际标准
SUM (M2009+F2009)/10000 TO T劳年 FOR X>=15.AND.X<=64	&&国际标准
SUM (M2009+F2009)/10000 TO T青劳 FOR X>=15.AND.X<=44	&&国际标准
SUM (M2009+F2009)/10000 TO T老年 FOR X>=65				&&国际标准
SUM (M2009+F2009)/10000 TO T高龄 FOR X>=80				&&国际标准
SUM F2009/10000 TO 育龄F15 FOR X>=15.AND.X<=49
SUM M2009/10000 TO 婚育M15 FOR X>=15.AND.X<=49
SUM F2009/10000 TO 育龄F20 FOR X>=20.AND.X<=49
SUM M2009/10000 TO 婚育M20 FOR X>=20.AND.X<=49
SUM M2009/10000 TO 长寿M0 FOR X>=EM0
SUM F2009/10000 TO 长寿F0 FOR X>=EF0
SUM (M2009+F2009)/10000 TO T婴幼儿 FOR X<=2
SUM (M2009+F2009)/10000 TO XQ FOR X>=3.AND.X<=5	&&学前
SUM (M2009+F2009)/10000 TO XX FOR X>=6.AND.X<=11	&&小学
SUM (M2009+F2009)/10000 TO CZ FOR X>=12.AND.X<=14	&&初中
SUM (M2009+F2009)/10000 TO GZ FOR X>=15.AND.X<=17	&&高中
SUM (M2009+F2009)/10000 TO DX FOR X>=18.AND.X<=21	&&大学
SUM M2009/10000 TO JYM FOR X>=18.AND.X<=59	&&公安劳龄
SUM F2009/10000 TO JYF FOR X>=18.AND.X<=59	&&公安劳龄
FIEL0='M2009+F2009'
SUM &FIEL0 TO RKZ
IF RKZ>0
	GO TOP
	PZ=RKZ/2
	DO WHILE  PZ>0.AND.EOF()=.F.
	   PZ=PZ-(&FIEL0)
	   SKIP
	ENDDO
	SKIP -1										
	NLZ=X+(PZ+&FIEL0)/(&FIEL0)
ELSE
	NLZ=0
ENDIF
RETU


*************************************************************************
PROC 年龄分组和年龄结构	       	
SUM (&MND2+&FND2)/10000,&MND2/10000,&FND2/10000 TO 总人口T,总人口M,总人口F
SUM ((&MND1+&MND2+&FND1+&FND2)/2)/10000 TO 年中人口
SUM (&MND2-&MND1+&FND2-&FND1)/10000 TO 新增人口
人口增长L=新增人口/年中人口*1000
SUM (&MND2+&FND2)/10000 TO T少年 FOR X<=14	&&国际标准
SUM (&MND2+&FND2)/10000 TO T劳年 FOR X>=15.AND.X<=64	&&国际标准
SUM (&MND2+&FND2)/10000 TO T青劳 FOR X>=15.AND.X<=44	&&国际标准
SUM (&MND2+&FND2)/10000 TO T老年 FOR X>=65	&&国际标准
SUM &MND2/10000 TO T老年M FOR X>=65	&&国际标准
SUM &FND2/10000 TO T老年F FOR X>=65	&&国际标准
SUM (&MND2+&FND2)/10000 TO T高龄 FOR X>=80
SUM &MND2/10000 TO T高龄M FOR X>=80
SUM &FND2/10000 TO T高龄F FOR X>=80
SUM &FND2/10000 TO 育龄F15 FOR X>=15.AND.X<=49
SUM &MND2/10000 TO 婚育M15 FOR X>=15.AND.X<=49
SUM &FND2/10000 TO 育龄F20 FOR X>=20.AND.X<=49
SUM &MND2/10000 TO 婚育M20 FOR X>=20.AND.X<=49
SUM &MND2/10000 TO 长寿M0 FOR X>=EM0
SUM &FND2/10000 TO 长寿F0 FOR X>=EF0
SUM (&MND2+&FND2)/10000 TO T婴幼儿 FOR X<=2
SUM (&MND2+&FND2)/10000 TO XQ FOR X>=3.AND.X<=5	&&学前
SUM (&MND2+&FND2)/10000 TO XX FOR X>=6.AND.X<=11	&&小学
SUM (&MND2+&FND2)/10000 TO CZ FOR X>=12.AND.X<=14	&&初中
SUM (&MND2+&FND2)/10000 TO GZ FOR X>=15.AND.X<=17	&&高中
SUM (&MND2+&FND2)/10000 TO DX FOR X>=18.AND.X<=21	&&大学
SUM &MND2/10000 TO JYM FOR X>=18.AND.X<=59	&&公安劳龄
SUM &FND2/10000 TO JYF FOR X>=18.AND.X<=59	&&公安劳龄
SUM (&MND2+&FND2)/10000 TO GB劳前  FOR X<=15	&&'国标劳前'
SUM &MND2/10000 TO GB劳龄M FOR X>=16.AND.X<=59	&&'国标劳龄'
SUM &FND2/10000 TO GB劳龄F FOR X>=16.AND.X<=54
SUM (&MND2+&FND2)/10000 TO GB青劳 FOR X>=16.AND.X<=44 &&'国标青劳
SUM &MND2/10000 TO GB中劳M FOR X>=45.AND.X<=59
SUM &FND2/10000 TO GB中龄F FOR X>=45.AND.X<=54 &&'国标中劳'
SUM &MND2/10000 TO GB劳后M FOR X>=60
SUM &FND2/10000 TO GB劳后F FOR X>=55 &&'国标劳后'
SUM (&MND2+&FND2)/10000 TO G少年 FOR X<=15
SUM &MND2/10000 TO G劳年M FOR X>=16.AND.X<=59
SUM &MND2/10000 TO G老年M FOR X>=60
SUM &FND2/10000 TO G劳年F FOR X>=16.AND.X<=59
SUM &FND2/10000 TO G老年F FOR X>=60
CALC STD(&MND2+&FND2),AVG(&MND2+&FND2) TO ST,AV
FIEL0='&MND2+&FND2'
SUM &FIEL0 TO RKZ
IF RKZ>0
	GO TOP
	PZ=RKZ/2
	DO WHILE  PZ>0.AND.EOF()=.F.
	   PZ=PZ-(&FIEL0)
	   SKIP
	ENDDO
	SKIP -1										
	NLZ=X+(PZ+&FIEL0)/(&FIEL0)
ELSE
	NLZ=0
ENDIF
RETU
*********************************
*	 年龄分组和年龄结构摘要		*
*********************************
PROC 基年年龄分组和年龄结构保存
LOCA FOR 年份=2009
REPL 总人口 WITH 总人口T,男 WITH 总人口M,女 WITH 总人口F,;
国际少年 WITH T少年,国际劳年 WITH T劳年,国际青劳 WITH T青劳,国际老年 WITH T老年,高龄 WITH T高龄,长寿 WITH 长寿M0+长寿F0,长寿M WITH 长寿M0,长寿F WITH 长寿F0,中位 WITH NLZ,;
育龄女15  WITH 育龄F15,育龄女20  WITH 育龄F20, 婚育男15 WITH 婚育M15,婚育男20  WITH 婚育M20,婚育XBB15  WITH IIF(育龄女15=0,0,婚育男15/育龄女15*100),;
婚育XBB20  WITH IIF(育龄女20=0,0,婚育男20/育龄女20*100),;
婴幼儿 WITH T婴幼儿,学前 WITH XQ,小学 WITH XX,初中 WITH CZ,高中 WITH GZ,大学 WITH DX,公安劳龄 WITH JYM+JYF,公安劳龄M WITH JYM,公安劳龄F WITH JYF
RETURN 
************************************************************************
PROC 死亡人口和预期寿命计算
SELECT A
	CALCULATE MIN(X) TO MINLM FOR &分龄预测..&MND1=0.and.x>0
	CALCULATE MIN(X) TO MINLF FOR &分龄预测..&FND1=0.and.x>0
	COUNT TO CM0 FOR &分龄预测..&MND1=0
	COUNT TO CF0 FOR &分龄预测..&FND1=0
	MINLM=IIF(CM0>0,MINLM,110)
	MINLF=IIF(CF0>0,MINLF,110)
    DO CASE
    CASE CX=1
	    SUM &城乡死亡..&MND2/10000 TO DQ死亡人口M
	    SUM &城乡死亡..&FND2/10000 TO DQ死亡人口F
    CASE CX=2
	    SUM &城乡死亡..&MND2/10000 TO C死亡人口M
	    SUM &城乡死亡..&FND2/10000 TO C死亡人口F
    CASE CX=4
	    SUM &城乡死亡..&MND2/10000 TO X死亡人口M
	    SUM &城乡死亡..&FND2/10000 TO X死亡人口F
    ENDCASE
 	RELEASE QXM,QXF
	DIMENSION QXM(MINLM+1),QXF(MINLF+1)	&&,CSWRM(MINLM),CSWRF(MINLF)
   GO TOP
    FOR RE=1 TO MINLM
    	QXM(RE)=&城乡死亡..&MND2/&分龄预测..&MND1
		*CSWRM(RE)=&城乡死亡..&MND2
    	SKIP
    ENDFOR
    GO TOP
    FOR RE=1 TO MINLF
    	QXF(RE)=&城乡死亡..&FND2/&分龄预测..&FND1
		*CSWRF(RE)=&城乡死亡..&FND2
    	SKIP
    ENDFOR
	*****计算预期寿命
	*****M*****
	REC=MINLM
	DIME 生存人数M(REC)
	生存人数M(1)=1-QXM(1)
	FOR I0=2 TO REC
		生存人数M(I0)=生存人数M(I0-1)*(1-QXM(I0))
	ENDFOR
	DIME 生存年数M(REC)
	生存年数M(REC)=生存人数M(REC)	&&*AXM(REC)
	FOR I0=2 TO REC
		生存年数M(REC+1-I0)=生存年数M(REC+1-I0+1)+生存人数M(REC+1-I0)+生存人数M(REC+1-I0)*QXM(REC+1-I0)	&&*AXM(REC+1-I0)
	ENDFOR
	EM0=生存年数M(1)
	*****F*****
	REC=MINLF
	DIME 生存人数F(REC)
	生存人数F(1)=1-QXF(1)
	FOR I0=2 TO REC
		生存人数F(I0)=生存人数F(I0-1)*(1-QXF(I0))
	ENDFOR
	DIME 生存年数F(REC)
	生存年数F(REC)=生存人数F(REC)	&&*AXF(REC)
	FOR I0=2 TO REC
		生存年数F(REC+1-I0)=生存年数F(REC+1-I0+1)+生存人数F(REC+1-I0)+生存人数F(REC+1-I0)*QXF(REC+1-I0)	&&*AXF(REC+1-I0)
	ENDFOR
	EF0=生存年数F(1)

RETURN


*************************
*	计算预期寿命		*
*************************
PROC 计算预期寿命	
*****M*****
REC=MINLM
DIME 生存人数M(REC)
生存人数M(1)=1-QXM(1)
FOR I0=2 TO REC
	生存人数M(I0)=生存人数M(I0-1)*(1-QXM(I0))
ENDFOR
DIME 生存年数M(REC)
生存年数M(REC)=生存人数M(REC)	&&*AXM(REC)
FOR I0=2 TO REC
	生存年数M(REC+1-I0)=生存年数M(REC+1-I0+1)+生存人数M(REC+1-I0)+生存人数M(REC+1-I0)*QXM(REC+1-I0)	&&*AXM(REC+1-I0)
ENDFOR
EM0=生存年数M(1)
*****F*****
REC=MINLF
DIME 生存人数F(REC)
生存人数F(1)=1-QXF(1)
FOR I0=2 TO REC
	生存人数F(I0)=生存人数F(I0-1)*(1-QXF(I0))
ENDFOR
DIME 生存年数F(REC)
生存年数F(REC)=生存人数F(REC)	&&*AXF(REC)
FOR I0=2 TO REC
	生存年数F(REC+1-I0)=生存年数F(REC+1-I0+1)+生存人数F(REC+1-I0)+生存人数F(REC+1-I0)*QXF(REC+1-I0)	&&*AXF(REC+1-I0)
ENDFOR
EF0=生存年数F(1)
RETU
*************************
*	 预测结果摘要		*
*************************
*SELE IIF(CX=1,51,IIF(CX=2,52,IIF(CX=3,53,IIF(CX=4,54,55)))) &&SELE 51 地区概要；SELE 52 地区城镇概要;SELE 53 地区非农概要;SELE 54 地区农村概要；SELE 55  地区农业概要；							   	
PROC 预测结果摘要
APPEND BLANK 
REPL 年份 WITH ND0,地区 WITH IIF(CX=1,DQB,IIF(CX=2,'城镇',IIF(CX=3,'非农',IIF(CX=4,'农村','农业')))),总人口 WITH 总人口T,男 WITH 总人口M,女 WITH 总人口F,;
	死亡男 WITH IIF(CX=1,DQ死亡人口M,IIF(CX=2,C死亡人口M,IIF(CX=3,0,IIF(CX=4,X死亡人口M,0)))),; 
	死亡女 WITH IIF(CX=1,DQ死亡人口F,IIF(CX=2,C死亡人口F,IIF(CX=3,0,IIF(CX=4,X死亡人口F,0)))),; 
	政策生育率 WITH IIF(CX=1,地区政策TFR0,IIF(CX=2,城镇政策TFR0,IIF(CX=3,非农政策TFR0,IIF(CX=4,农村政策TFR0,农业政策TFR0)))),;
	婚N政策TFR WITH IIF(CX=1,地区婚内TFR,IIF(CX=2,城镇婚内TFR,IIF(CX=3,非农婚内TFR,IIF(CX=4,农村婚内TFR,农业婚内TFR)))),;
	可实现TFR  WITH IIF(CX=1,地区实现TFR0,IIF(CX=2,城镇实现TFR0,IIF(CX=3,非农实现TFR0,IIF(CX=4,农村实现TFR0,IIF(CX=5,农业实现TFR0,0))))),;
	实婚生育率 WITH IIF(CX=1,地区实婚TFR0,IIF(CX=2,城镇实婚TFR0,IIF(CX=3,非农实婚TFR0,IIF(CX=4,农村实婚TFR0,IIF(CX=5,农业实婚TFR0,0))))),;
	再生育率   WITH IIF(CX=1,地区再生育L,IIF(CX=2,城镇再生育L ,IIF(CX=3,非农再生育L,IIF(CX=4,农村再生育L,农业再生育L)))),;
	出生 WITH 出生人口,出生男 WITH 出生人口M,出生女 WITH 出生人口F,出生率 WITH 出生人口/年中人口*1000,超生数 WITH IIF(ND0>2009.and.ZC实现=2,超生S/10000,0),再生育数  WITH 再生D,;
	政策生育数 WITH IIF(CX=1,DQ政策生育,IIF(CX=2,XFN政策生育,IIF(CX=3,CFN政策生育,IIF(CX=4,XNY政策生育,CNY政策生育))))/10000,;
	死亡人数 WITH 死亡男+死亡女,死亡率 WITH 死亡人数/年中人口*1000,自增率 WITH 出生率-死亡率,;
	年均人口 WITH 年中人口,新增人数 WITH 新增人口,总增长率 WITH 新增人口/年中人口*1000,出生XBB WITH XBB*100,;
	净迁入数 WITH 净迁入RK,净迁男 WITH 净迁入M,净迁女 WITH 净迁入F,	净迁入率 WITH 净迁入RK/年中人口*1000,;
	城镇人口 WITH CRK/10000,农村人口 WITH XRK/10000,城镇人口比 WITH CSH水平,非农人口 WITH FNRK/10000,农业人口 WITH NYRK/10000,非农人口B WITH FNRK/(FNRK+NYRK)*100,;
	婴幼儿 WITH T婴幼儿,学前 WITH XQ,小学 WITH XX,初中 WITH CZ,高中 WITH GZ,大学 WITH DX,公安劳龄 WITH JYM+JYF,公安劳龄M WITH JYM,公安劳龄F WITH JYF,;
	国际少年 WITH T少年,国际劳年 WITH T劳年,国际青劳 WITH T青劳,国际老年 WITH T老年,国际老年M WITH T老年M,国际老年F WITH T老年F,;
	高龄 WITH T高龄,高龄M WITH T高龄M,高龄F WITH T高龄F,长寿 WITH 长寿M0+长寿F0,长寿M WITH 长寿M0,长寿F WITH 长寿F0,中位 WITH NLZ,年龄差异度 WITH ST/AV*100,;
	国标少年  WITH G少年,国标劳年F WITH G劳年F,国标老年F  WITH G老年F,国标劳年M WITH G劳年M,国标老年M  WITH G老年M,;
	国标劳前 WITH GB劳前,国标劳龄M WITH GB劳龄M,国标劳龄F WITH GB劳龄F,国标青劳 WITH GB青劳,国标中劳M WITH GB中劳M,国标中劳F WITH GB中龄F,国标劳后M WITH GB劳后M,国标劳后F WITH GB劳后F,;
	育龄女15  WITH 育龄F15,育龄女20  WITH 育龄F20, 婚育男15 WITH 婚育M15,婚育男20  WITH 婚育M20,婚育XBB15  WITH IIF(育龄女15=0,0,婚育男15/育龄女15*100),婚育XBB20  WITH IIF(育龄女20=0,0,婚育男20/育龄女20*100)
&&预期寿命M WITH EM0,预期寿命F WITH EF0

IF ND0>=调整时机.AND.(ND0-调整时机+1)<=35
	REPL 堆积生育 WITH IIF(CX=1,农村农业单放B2+农村农业非放B2+农村非农单放B2+农村非农非放B2+城镇农业单放B2+城镇农业非放B2+城镇非农单放B2+城镇非农非放B2,;
		IIF(CX=2,城镇农业单放B2+城镇农业非放B2+城镇非农单放B2+城镇非农非放B2,IIF(CX=3,城镇非农单放B2+城镇非农非放B2+农村非农单放B2+农村非农非放B2,;
		IIF(CX=4,农村农业单放B2+农村农业非放B2+农村非农单放B2+农村非农非放B2,农村农业单放B2+农村农业非放B2+城镇农业单放B2+城镇农业非放B2))))/10000
	REPL 堆积夫妇数 WITH IIF(CX=1,农村农业单独堆积+农村农业双非堆积+农村非农单独堆积+农村非农双非堆积+城镇农业单独堆积+城镇农业双非堆积+城镇非农单独堆积+城镇非农双非堆积,;
		IIF(CX=2,城镇农业单独堆积+城镇农业双非堆积+城镇非农单独堆积+城镇非农双非堆积,IIF(CX=3,城镇非农单独堆积+城镇非农双非堆积+农村非农单独堆积+农村非农双非堆积,;
		IIF(CX=4,农村农业单独堆积+农村农业双非堆积+农村非农单独堆积+农村非农双非堆积,农村农业单独堆积+农村农业双非堆积+城镇农业单独堆积+城镇农业双非堆积))))/10000
ENDIF
IF CX>5
	STORE 0 TO 城镇非农单独堆积,城镇非农单放B2,城镇非农非放B2,城镇非农双非堆积,城镇农业单独堆积,城镇农业单放B2,城镇农业非放B2,;
	城镇农业双非堆积,农村非农单独堆积,农村非农单放B2,农村非农非放B2,农村非农双非堆积,农村农业单独堆积,农村农业单放B2,农村农业非放B2,农村农业双非堆积
ENDIF

IF ND0=ND2	&&IIF(SFMS=1,2100,2100)
	REPL 总人口 WITH 男+女 FOR 总人口=0
	REPL 国标劳龄 	WITH 国标劳龄M+国标劳龄F ,国标中劳 WITH 国标中劳M+国标中劳F,国标劳后 WITH 国标劳后M+国标劳后F,国标劳年 WITH 国标劳年M+国标劳年F,国标老年 WITH 国标老年M+国标老年F  ALL
	REPL 国标少年比 WITH 国标少年/总人口*100,国标老年比 WITH 国标老年/总人口*100, 国标劳年B WITH 国标劳年/总人口*100,国际少年比 WITH 国际少年/总人口*100,国际老年比 WITH 国际老年/总人口*100, 国际劳年B WITH 国际劳年/总人口*100  FOR 总人口<>0
	REPL 国标高龄B  WITH 高龄/国标老年*100,国标长寿B WITH 长寿/国标老年*100 FOR 国标老年<>0
	REPL 国际高龄B  WITH 高龄/国际老年*100,国际长寿B WITH 长寿/国际老年*100 FOR 国际老年<>0
	REPL 国际老少比 WITH IIF(国际少年<>0,国际老年/国际少年*100,0) FOR 国际少年<>0
	REPL 国标老少比 WITH IIF(国标少年<>0,国标老年/国标少年*100,0) FOR 国标少年<>0
	REPL 国际负老 WITH IIF(国际劳年<>0,国际老年/国际劳年*100,0),国际负少 WITH IIF(国际劳年<>0,国际少年/国际劳年*100,0),国际总负 WITH 国际负老+国际负少,国际青劳B WITH IIF(国际劳年<>0,国际青劳/国际劳年*100,0) FOR 国际劳年<>0
	REPL 国标负老 WITH IIF(国标劳年<>0,国标老年/国标劳年*100,0),国标负少 WITH IIF(国标劳年<>0,国标少年/国标劳年*100,0),国标总负 WITH 国标负老+国标负少,国标青劳B WITH IIF(国标劳年<>0,国标青劳/国标劳年*100,0) FOR 国标劳年<>0
	REPL 人口密度 WITH 总人口*10000/(土地MJ*1000/100),人均水资源 WITH (水资源*10000)/总人口,人均耕地 WITH (耕地MJ*1000*15)/(总人口*10000) FOR 总人口<>0	&&原单位:千公顷,单位:亿立方米,计算后:人/平方公里,立方米/人,亩/人
	REPL 非堆生育 WITH 出生-堆积生育,自增人口 WITH 出生-死亡人数,迁移修正数 WITH  新增人数-自增人口 ALL

FIL1=SUBSTR(DBF(),1,LEN(DBF())-4)
*COPY TO &FIL1 XLS

ENDIF
*IF CX=1
*	SELE 56	&&地区婚配概率 ALIAS 地区婚配概率
*	APPEND BLANK
*	REPL 地区 WITH DQB,年份 WITH ND0,;
*	C非男单GLF WITH 城FN男单GLF,C非男单GLM WITH 城FN男单GLM,C非女单GLF WITH 城FN女单GLF,C非女单GLM WITH 城FN女单GLM,C非双独GLF WITH 城FN双独GLF,C非双独GLM WITH 城FN双独GLM,;
*	C农男单GLF WITH 城NY男单GLF,C农男单GLM WITH 城NY男单GLM,C农女单GLF WITH 城NY女单GLF,C农女单GLM WITH 城NY女单GLM,C农双独GLF WITH 城NY双独GLF,C农双独GLM WITH 城NY双独GLM,;
*	X非男单GLF WITH 乡FN男单GLF,X非男单GLM WITH 乡FN男单GLM,X非女单GLF WITH 乡FN女单GLF,X非女单GLM WITH 乡FN女单GLM,X非双独GLF WITH 乡FN双独GLF,X非双独GLM WITH 乡FN双独GLM,;
*	X农男单GLF WITH 乡NY男单GLF,X农男单GLM WITH 乡NY男单GLM,X农女单GLF WITH 乡NY女单GLF,X农女单GLM WITH 乡NY女单GLM,X农双独GLF WITH 乡NY双独GLF,X农双独GLM WITH 乡NY双独GLM
*ENDIF
SELE &SEL

RETU




*********************
*	 主要极值		*
*********************
PROC 主要极值
CALC MAX(总人口),MAX(净迁入数),MIN(总人口),MIN(净迁入数),MAX(可实现TFR) TO MA1,MA2,MI1,MI2,MATFR0 FOR 年份>ND1
LOCA FOR 总人口=MA1.AND.年份>ND1
MAXND1=年份
LOCA FOR 净迁入数=MA2.AND.年份>ND1
MAXND2=年份
LOCA FOR 总人口=MI1.AND.年份>ND1
MIND1=年份
LOCA FOR 净迁入数=MI2.AND.年份>ND1
MIND2=年份
LOCA FOR 可实现TFR=MATFR0.AND.年份>ND1
MAND3=年份
LOCA FOR 年份=2020
ZRK20=总人口
CSRK20=出生
CSL20=出生率
TFR20=可实现TFR
JQR20=净迁入率
ZZL20=总增长率
CSH20=城镇人口比
SEL=STR(SELE(),3)
*IF CX=1
	*?概率估算法,ALLT(STR(FN政策1,1)),ALLT(STR(FN时机1,1)),ALLT(STR(NY政策1,1)),ALLT(STR(NY时机1,1)),ALLT(STR(QY,2)),;
	ALLT(城乡B)-','-FA2-','-JA2-',',ALLT(STR(MAXND1)),',',ALLT(STR(MA1,10,2)),',',ALLT(STR(MIND1)),',',ALLT(STR(MI1,10,2)),',',;
	ALLT(STR(MAXND2)),',',ALLT(STR(MA2,10,2)),',',ALLT(STR(MIND2)),',',ALLT(STR(ROUN(MI2,2),8,2)),',',;
	ROUN(总人口,2),',',ALLT(STR(出生,10)),',',ALLT(STR(出生率,8,2)),',',ALLT(STR(可实现TFR,8,2)),',',ALLT(STR(净迁入率,8,2)),',',;
	ALLT(STR(总增长率,8,2)),',',ALLT(STR(城镇人口比,8,2)),',',ALLT(STR(MAND3,4)),',',ALLT(STR(MATFR0,10,2)),TIME()
*	?概率估算法,ALLT(STR(FN政策1,1)),ALLT(STR(FN时机1,1)),ALLT(STR(NY政策1,1)),ALLT(STR(NY时机1,1)),ALLT(STR(QY,2)),;
	ALLT(城乡B)-',',ROUN(总人口,2),',',ALLT(STR(出生,10)),',',ALLT(STR(城镇人口比,8,2)),',',ALLT(STR(MAND3,4)),',',ALLT(STR(MATFR0,10,2)),TIME()
*ENDIF
SELE 401
USE &生育政策模拟摘要
APPE BLANK
REPL 堆释模式 WITH 释放模式,实现程度 WITH 实现方式,;
	政策1 WITH 非农ZC1,时机1 WITH 非农时机1,政策2 WITH 非农ZC2,时机2 WITH 非农时机2,政策3 WITH IIF(调整方式时机='多地区渐进普二',非农ZC3,''),时机3 WITH IIF(调整方式时机='多地区渐进普二',非农时机3,0),;
	地区别 WITH DQB,城乡别 WITH 城乡B,生育方案 WITH FA,迁移方案 WITH IIF(QY=1,'低',IIF(QY=2,'中','高')),;
	最大规模ND WITH MAXND1,最大规模 WITH MA1,最小规模ND WITH MIND1,最小规模 WITH MI1,最大迁移ND WITH MAXND2,最大迁移 WITH MA2,最小迁移ND WITH MIND2,最小迁移 WITH MI2,;
	最高TFRND  WITH MAND3,最高TFR WITH MATFR0,总人口20 WITH ZRK20,出生20 WITH CSRK20,出生率20 WITH CSL20,生育率20 WITH TFR20,;
	净迁入率20 WITH JQR20,总增长率20 WITH ZZL20,城市化20 WITH CSH20
SELE &SEL


RETU


-&子女..男单堆积-&子女..已2男单-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
			*REPL &子女..DF WITH &子女..DF-&子女..女单堆积-&子女..已2女单  FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独和双非*****
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积,&子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0
		ENDCASE
		*************************************************************************
		*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
		*	以下剥离方法,不完善的地方是：
		*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
		***************************************************************************************************************************
	*	REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
	* 	RELE AR1
	*	COPY TO ARRA AR1 FIEL X,&子女..女单堆积,&子女..男单堆积,&子女..双非堆积  FOR &子女..堆积比例<>0
	*	COUN TO TS  FOR &子女..堆积比例<>0
	*	FOR NL=1 TO TS
	*		求夫NMDF=AR1(NL,2)
	*		求夫NFDM=AR1(NL,3)
	*		求夫NN=IIF((农业ZC1='普二').AND.(非农ZC1='普二'),AR1(NL,4),0)
	*		夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*		CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*		SUM &子女..DM0+&子女..NM0 TO SM
	*		IF SM<>0
	*			REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫NMDF*需夫模式.&夫FB ALL
	*			REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NFDM*需夫模式.&夫FB ALL
	*			REPL 需夫模式.NN夫 		WITH 需夫模式.NN夫+求夫NN*需夫模式.&夫FB ALL
	*		ENDIF
	*	ENDFOR
	*	REPL &子女..堆积丈夫 WITH 需夫模式.NMDF夫+需夫模式.NFDM夫+需夫模式.NN夫  ALL
	*	REPL &子女..NM0 WITH &子女..NM0-需夫模式.NMDF夫-需夫模式.NN夫  ALL
	*	REPL &子女..DM0 WITH &子女..DM0-需夫模式.NFDM夫  ALL
	*	REPL &子女..NM WITH &子女..NM-需夫模式.NMDF夫-需夫模式.NN夫  ALL
	*	REPL &子女..DM WITH &子女..DM-需夫模式.NFDM夫  ALL
	ENDFOR
CASE ND0>调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL	&&FOR &子女..堆积比例<>0	&&待2单独
		CASE 政策='普二'
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积,&子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0
		ENDCASE
	ENDFOR 
ENDCASE 
RETURN 

*********************************
*	渐进普二堆积夫妇估计与剥离	*
*********************************
PROCEDURE 渐进普二堆积夫妇估计与剥离
DO CASE 
CASE ND0=调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		*SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM1,SF1 FOR &子女..堆积比例<>0
		存活子女=IIF(CX=1,'农农',IIF(CX=2,'农非',IIF(CX=3,'城农','城非')))-'存活子女'
		DO CASE
		CASE (政策='单独'.OR.政策='单独后').and.nd0=非农时机2
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对，对已经生过二孩的夫妇单独列出,只进行年龄移算*****
	*		REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例,&子女..已2男单 WITH &子女..NFDM*(1-&子女..堆积比例)  FOR &子女..堆积比例<>0
	*		REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例,&子女..已2女单 WITH &子女..NMDF*(1-&子女..堆积比例)  FOR &子女..堆积比例<>0
	*		REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积,&子女..已2单独 WITH &子女..已2男单+&子女..已2女单 ALL
			*****剔除堆积后的非堆积单独。
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
	*		REPL &子女..DF0 WITH &子女..DF0-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
	*		REPL &子女..NF0 WITH &子女..NF0-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
	*		REPL &子女..DF WITH &子女..DF-&子女..女单堆积-&子女..已2女单 FOR &子女..堆积比例<>0
	*		REPL &子女..NF WITH &子女..NF-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独*****
	*		REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积-&子女..已2女单,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积-&子女..已2男单 FOR &子女..堆积比例<>0
			
			REPL &子女..男单堆积 WITH &子女..NFDM*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N2D1N,现行N2D1C))  FOR &子女..堆积比例<>0
			REPL &子女..女单堆积 WITH &子女..NMDF*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行N1D2N,现行N1D2C))   FOR &子女..堆积比例<>0
			REPL &子女..单独堆积 WITH &子女..男单堆积+&子女..女单堆积 ALL
			REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL &&FOR &子女..堆积比例<>0

			REPL &子女..单独堆积15  WITH &子女..单独堆积 FOR x<=29
			REPL &子女..单独堆积30  WITH &子女..单独堆积 FOR x>=30.AND.X<35
			REPL &子女..单独堆积35  WITH &子女..单独堆积 FOR x>=35
			*************************************************************************
			*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
			*	以下剥离方法,不完善的地方是：
			*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
			***************************************************************************************************************************
	*		REPL 需夫模式.DD夫 WITH 0,需夫模式.NMDF夫 WITH 0,需夫模式.NFDM夫 WITH 0,需夫模式.NN夫 WITH 0 ALL
	*	 	RELE AR1
	*		COPY TO ARRA AR1 FIEL X,&子女..女单堆积,&子女..男单堆积  FOR &子女..堆积比例<>0
	*		COUN TO TS  FOR &子女..堆积比例<>0
	*		FOR NL=1 TO TS
	*			求夫NMDF=AR1(NL,2)
	*			求夫NFDM=AR1(NL,3)
	*			夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*			SUM &子女..DM0+&子女..NM0 TO SM
	*			IF SM<>0
	*				REPL 需夫模式.NMDF夫 	WITH 需夫模式.NMDF夫+求夫NMDF*需夫模式.&夫FB ALL
	*				REPL 需夫模式.NFDM夫 	WITH 需夫模式.NFDM夫+求夫NFDM*需夫模式.&夫FB ALL
	*			ENDIF
	*		ENDFOR
	*		REPL &子女..堆积丈夫 WITH 需夫模式.NMDF夫+需夫模式.NFDM夫  ALL
	*		REPL &子女..NM0 WITH &子女..NM0-需夫模式.NMDF夫  ALL
	*		REPL &子女..DM0 WITH &子女..DM0-需夫模式.NFDM夫  ALL
	*		REPL &子女..NM WITH &子女..NM-需夫模式.NMDF夫  ALL
	*		REPL &子女..DM WITH &子女..DM-需夫模式.NFDM夫  ALL
		CASE 政策='普二'.and.nd0=非农时机3
	*		REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例  FOR &子女..堆积比例<>0
			*****从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇，使不再重复配对
			*	因有部分农和非农间的婚配,故下句计算结果,有部分年龄组,将会出现负数而农和非农之和则不会出现负数。
			********************************************************************************************
	*		REPL &子女..NF0 WITH &子女..NF0-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
	*		REPL &子女..NF WITH &子女..NF-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0
			*****剔除堆积后的非堆积单独和双非*****
	*		REPL &子女..NN WITH &子女..NN-&子女..双非堆积-&子女..已2双非 FOR &子女..堆积比例<>0

			REPL &子女..双非堆积 WITH &子女..NN*&子女..堆积比例*(2-IIF(CX=1.OR.CX=3,现行NNFN,现行NNFC)) FOR &子女..堆积比例<>0

			REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL &&FOR &子女..堆积比例<>0

			REPL &子女..双非堆积15  WITH &子女..双非堆积 FOR x<=29
			REPL &子女..双非堆积30  WITH &子女..双非堆积 FOR x>=30.AND.X<35
			REPL &子女..双非堆积35  WITH &子女..双非堆积 FOR x>=35

*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*?子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积
			*************************************************************************
			*	从独生子女和非独生子女中剔除堆积夫妇和已生二孩夫妇的丈夫，使不再重复配对
			*	以下剥离方法,不完善的地方是：
			*	1.在多龄概率的条件下，夫妇按妻子年龄记录和剥离。事实上夫妻是不同龄的，丈夫需要按丈夫的年龄记录与剥离
			***************************************************************************************************************************
	*		REPL 需夫模式.NN夫 WITH 0 ALL
	*	 	RELE AR1
	*		COPY TO ARRA AR1 FIEL X,&子女..双非堆积,&子女..已2双非  FOR &子女..堆积比例<>0
	*		COUN TO TS  FOR &子女..堆积比例<>0
	*		FOR NL=1 TO TS
	*			求夫NN=AR1(NL,2)+AR1(NL,3)
	*			夫FB='妻子'-ALLTRIM(STR(AR1(NL,1)))
	*			CALCULATE MAX(x),MIN(x) TO MAX1,MIX1 FOR 需夫模式.&夫FB<>0
	*			SUM &子女..NM0 TO SM
	*			IF SM<>0
	*				REPL 需夫模式.NN夫 WITH 需夫模式.NN夫+求夫NN*需夫模式.&夫FB	ALL
	*			ENDIF
	*		ENDFOR
	*		REPL &子女..堆积丈夫 WITH &子女..堆积丈夫+需夫模式.NN夫  ALL
	*		REPL &子女..NM0 WITH &子女..NM0-需夫模式.NN夫  ALL
	*		REPL &子女..NM WITH &子女..NM-需夫模式.NN夫  ALL
		ENDCASE
	*SUM &子女..DM+&子女..DDBM+&子女..NM+&子女..堆积丈夫,&子女..DF+&子女..DDBF+&子女..NF+&子女..单独堆积+&子女..双非堆积+&子女..已2单独+&子女..已2双非  TO SM2,SF2 FOR &子女..堆积比例<>0
	*?ND0,DQB,政策,调整时机,子女,SM1,SM2,SM1-SM2,SF1,SF2,SF1-SF2
	*g
*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*LOCATE FOR x=49
*?nd0,调整时机,子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积,x,&子女..单独堆积 ,&子女..双非堆积
	ENDFOR
CASE ND0>调整时机
	FOR CX=1 TO 4    &&
		SELE A	
		子女='地区'-IIF(CX=1,'农村农业',IIF(CX=2,'农村非农',IIF(CX=3,'城镇农业','城镇非农')))-'子女'
		COPY TO ARRAY ARNMDF FIELDS &子女..NMDF
		COPY TO ARRAY AR女单堆积 FIELDS &子女..女单堆积
		COPY TO ARRAY ARNFDM FIELDS &子女..NFDM
		COPY TO ARRAY AR男单堆积 FIELDS &子女..男单堆积
		COPY TO ARRAY ARNN FIELDS &子女..NN
		COPY TO ARRAY AR双非堆积 FIELDS &子女..双非堆积
		DO CASE
		CASE 政策='单独'.OR.政策='单独后'
			*REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
			REPL &子女..NMDF WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),ARNMDF(RECNO())-AR女单堆积(RECNO()),0),&子女..女单堆积 WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),&子女..女单堆积,AR女单堆积(RECNO())-ARNMDF(RECNO())) ALL
			REPL &子女..NFDM WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),ARNFDM(RECNO())-AR男单堆积(RECNO()),0),&子女..男单堆积 WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),&子女..男单堆积,AR男单堆积(RECNO())-ARNFDM(RECNO())) ALL
		CASE 政策='普二'
			*REPL &子女..NMDF WITH &子女..NMDF-&子女..女单堆积,&子女..NFDM WITH &子女..NFDM-&子女..男单堆积 ALL
			*REPL &子女..NN WITH &子女..NN-&子女..双非堆积 ALL
			REPL &子女..NMDF WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),ARNMDF(RECNO())-AR女单堆积(RECNO()),0),&子女..女单堆积 WITH IIF(ARNMDF(RECNO())>=AR女单堆积(RECNO()),&子女..女单堆积,AR女单堆积(RECNO())-ARNMDF(RECNO())) ALL
			REPL &子女..NFDM WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),ARNFDM(RECNO())-AR男单堆积(RECNO()),0),&子女..男单堆积 WITH IIF(ARNFDM(RECNO())>=AR男单堆积(RECNO()),&子女..男单堆积,AR男单堆积(RECNO())-ARNFDM(RECNO())) ALL
			REPL &子女..NN WITH IIF(ARNN(RECNO())>=AR双非堆积(RECNO()),ARNN(RECNO())-AR双非堆积(RECNO()),0),&子女..双非堆积 WITH IIF(ARNN(RECNO())>=AR双非堆积(RECNO()),&子女..双非堆积,AR双非堆积(RECNO())-ARNN(RECNO())) ALL
		ENDCASE
*SUM &子女..单独堆积 ,&子女..双非堆积 TO s单独堆积 ,s双非堆积
*?nd0,调整时机,子女,s单独堆积 ,s双非堆积,s单独堆积+s双非堆积

	ENDFOR
ENDCASE 
RETURN 


*********************************
PROCEDURE 分婚配模式按政策生育的孩子数
出生数=0
FOR CX=1 TO 4
	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
	子女=IIF(CX=1,'地区农村农业子女',IIF(CX=2,'地区农村非农子女',IIF(CX=3,'地区城镇农业子女','地区城镇非农子女'))