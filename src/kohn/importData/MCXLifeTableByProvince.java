package ds.importData;

import java.util.*;

import prclqz.core.Const;
import prclqz.core.enumLib.*;
import prclqz.lib.EnumMapTool;


/**
 *对应导入参数的分省分城乡生命表2010模块
 *@author DS
 *
 */
public class MCXLifeTableByProvince{
    
    int DQS = 32;
    public static void main(String argv[])throws Exception{
		MCXLifeTableByProvince m = new MCXLifeTableByProvince();
		
		
	   	//Map<Integer,Map<CXAll,Map<XBAll,double[]>>>FenlingCX = (Map<Integer,Map<CXAll,Map<XBAll,double[]>>>)globals.get("FenlingCX");//与下面几个Map一起,基础数据/分地区/xx省/xx分城乡户口性质年龄性别人口数推算_基于5岁组数据(2010).dbf
		//Map<Integer,Map<NY,Map<XBAll,double[]>>>FenlingNY = (Map<Integer,Map<NY,Map<XBAll,double[]>>>)globals.get("FenlingNY");
		//Map<Integer,Map<CX,Map<NY,Map<XBAll,Double>>>>FenlingCXNY = (Map<Integer,Map<CX,Map<NY,Map<XBAll,Double>>>>)globals.get("FenlingCXNY");
		//Map<Integer,Double[]> FenlingCZRKRate = (Map<Integer, Double[]>)globals.get("FenlingCZRKRate");//虽然好像不效率,还是整齐要紧=, =
		//Map<Integer,Map<CXAll,Map<XBAll,Integer>>> NExpectedAge = new EnumMap<XBAll,Map<Integer,Integer>>(XBAll.class);//全国分省城乡别预期寿命2010.DBF
		
		
		/*****构造生命表,这是输出*********/
		Map<Integer, Map<CXAll, Map<XBAll, Map<DPPredict, double[]>>>> Lives = EnumMapTool.createLives(Const.MAX_AGE);//空的
			
		/*************构造globals*************/
		Map<String,Object> globals = new HashMap<String,Object>();
		globals.put("Lives",Lives);
		
		m.calculate(globals);
    }
    public void calculate(Map<String,Object> globals){

    	int dqx=11;
	//输入部分	

    	Map<CXAll,Map<XBAll,double[]>>FenlingCXA = ((Map<Integer,Map<CXAll,Map<XBAll,double[]>>>)globals.get("FenlingCXA")).get(dqx);//与下面几个Map一起,基础数据/分地区/xx省/xx分城乡户口性质年龄性别人口数推算_基于5岁组数据(2010).dbf
    	Map<NY,Map<XBAll,double[]>>FenlingNY = ((Map<Integer,Map<NY,Map<XBAll,double[]>>>)globals.get("FenlingNY")).get(dqx);
    	Map<CX,Map<NY,Map<XBAll,Double>>>FenlingCXNY = ((Map<Integer,Map<CX,Map<NY,Map<XBAll,Double>>>>)globals.get("FenlingCXNY")).get(dqx);
    	Double[] FenlingCZRKRate = ((Map<Integer, Double[]>)globals.get("FenlingCZRKRate")).get(dqx);//虽然好像不效率,还是整齐要紧=, =

    	//'T6_04全国分年龄性别的死亡人口'-城乡
    	Map<Integer,Map<FLDeath,Map<XBAll,double[]>>> NLDeathPopulation = new HashMap<Integer,Map<FLDeath,Map<XBAll,double[]>>>();//输入/0死亡人口/xx分性别年龄的死亡人口_5岁组
    	Map<Integer,Map<CXAll,Map<XBAll,Double>>> NExpectedAge = (Map<Integer,Map<CXAll,Map<XBAll,Double>>>)globals.get("NExpectedAge");//全国分省城乡别预期寿命2010.DBF
    	
    	Map<Integer,Map<XBAll,double[]>> XBNLDeathPopulation = new HashMap<Integer,Map<XBAll,double[]>>();//基础数据/分地区/xx省/xx分性别年龄的死亡人口数推算(xx).dbf
    
    	Map<Integer,Map<CX,Map<XBAll,double[]>>>DeathCalculate = (Map<Integer, Map<CX,Map<XBAll,double[]>>>)globals.get("XBNLDeathCalculate");//对应死亡推算
    	Map<AgeDeathPopulation,double[]>AgeDeathPop = (Map<AgeDeathPopulation,double[]>)globals.get("AgeDeathPopulation");// /分年龄死亡人口.dbf

	//输出部分
    	Map<Integer,Map<CXAll,Map<XBAll,double[][]>>> DeathRateCalculate = EnumMapTool.createDeathRateCalculate();  //对应死亡率推算,二维数组的四列分别代表:城乡性别,年中人口,死亡人口,死亡率.
    	Map<Integer,Map<CXAll,Map<XBAll,Map<DPPredict,double[]>>>> Lives = (Map<Integer,Map<CXAll,Map<XBAll,Map<DPPredict,double[]>>>>)globals.get("Lives");//对应分省生命表,输出结果

	//临时变量
    	Map<CX,Map<XBAll,double[]>> PDeathCalculate = DeathCalculate.get(dqx);
	
		/********开始计算*************/
		int X;
		for(CXAll cxa:CXAll.values()){
		    for(XBAll xba:XBAll.values()){
				Map<DPPredict,double[]>ProvinceLives = Lives.get(dqx).get(cxa).get(xba);
				Map<CXAll,Map<XBAll,double[][]>> swlts = DeathRateCalculate.get(dqx);//二维数组的四列分别代表:城乡性别,年中人口,死亡人口,死亡率.
				Map<XBAll,double[]>swts = XBNLDeathPopulation.get(dqx);
				//工作区:a<=>分龄推算;b<=>死亡推算;c<=>死亡率推算
				if(dqx==0){//全国
				    //貌似暂时用不到
					//...又要用了，虽然foxpro出现dqx==0的情况被注释掉了
//					死亡率推算=死亡率PATH-'T6_04全国分年龄性别的死亡人口'-城乡
//							死亡人口='死亡人口'-性别
//							年中人口='年中人口'-性别
					swlts = new EnumMap<CXAll,Map<XBAll,double[][]>>(CXAll.class);
					swlts.put(cxa, new EnumMap<XBAll,double[][]>(XBAll.class));
					swlts.get(cxa).put(xba, new double[14][3]);//二维数组的3列分别代表:平均人口，死亡人口，死亡率，TODO 14可能不通用
				}
				else{					
				    //COPY TO &死亡率推算 FIELDS X,&总人口
					for(X=Const.MAX_AGE-1;X>=0;X--){
						swlts.get(cxa).get(xba)[0][X] = FenlingCXA.get(cxa).get(xba)[X];
					}
					
					//REPLACE 死亡率推算.死亡人口  WITH 死亡推算.&SW人口 all
					for(X=Const.MAX_AGE-1;X>=0;X--){
						swlts.get(cxa).get(xba)[2][X] = swts.get(xba)[X]; 
					}
			    
					//COPY TO ARRAY ar2 FIELDS 死亡率推算.&总人口
					double[] ar2 = new double[Const.MAX_AGE];
					for(X=Const.MAX_AGE-1;X>=0;X--){
						ar2[X] = swlts.get(cxa).get(xba)[0][X];
					}
					
					//REPLACE 年中人口 WITH (ar2(RECNO()-1)+ar2(RECNO()))/2 FOR RECNO()>1
					for(X=Const.MAX_AGE-1;X>=0;X--){
						if(X>0){
							double tmp = ar2[X];
							double tmp1 = ar2[X-1];
							swlts.get(cxa).get(xba)[1][X] = (tmp+tmp1)/2;
						}
					}
					
					//LOCATE FOR X=0
					X=0;
					//TODO REPLACE 年中人口 WITH ar2(RECNO()) 这里ar2数组是从0开始的还是从1开始的?
					swlts.get(cxa).get(xba)[1][X] = ar2[0];
					
					for(X=Const.MAX_AGE-1;X>=0;X--){
					//REPLACE 死亡率 WITH 死亡人口/年中人口 FOR 年中人口<>0
						if(swlts.get(cxa).get(xba)[1][X]!=0)
						swlts.get(cxa).get(xba)[3][X] = swlts.get(cxa).get(xba)[2][X]/swlts.get(cxa).get(xba)[1][X];
					}
			    }
				//第一步：建立关联
//				SELE C
//				USE &死亡率推算
//		   		INDEX ON X TO CIN
				int MANL = swlts.get(cxa).get(xba).length;
//				REC=RECC()//貌似没用
				
				Map<DPPredict,double[]> PLives = Lives.get(dqx).get(cxa).get(xba);//分省生命表
				//第二步:取基础数据
				//foxpro:100
				//SELE A
				//REPL AX WITH  B.&AX0  ALL
				//B工作区:USE H:\D\数据库（全国）\人口普查1990\分年龄死亡人口		&&使用1990年ax  可是这个表木有啊=, =
				for(X=Const.MAX_AGE-1;X>=0;X--){
					PLives.get(DPPredict.AX)[X] = AgeDeathPop.get(AgeDeathPopulation.getAN(xba))[X];
				}
				
				// TODO 
				//平均人口:'平均人口'+cxa
				//IF DQB='全国'
				//		REPL MX WITH  C.&死亡人口/C.&平均人口  ALL
				//	ELSE
				//		REPL MX WITH  C.死亡率  ALL
				//	ENDIF
				//TODO:这里的C.平均人口是神马意思???
				if(dqx == 0){
					for(X=Const.MAX_AGE-1;X>=0;X--){
						PLives.get(DPPredict.MX)[X] =  swlts.get(cxa).get(xba)[1][X]/swlts.get(cxa).get(xba)[0][X] ;
					}
				}
				else{
					for(X=Const.MAX_AGE-1;X>=0;X--){
						PLives.get(DPPredict.MX)[X] =  swlts.get(cxa).get(xba)[3][X];
					}
				}
				//******第三步:计算死亡概率******
				//REPL QX WITH  2*MX/(2+MX) ALL	&&	qx:x岁人口死亡概率
				for(X=Const.MAX_AGE-1;X>=0;X--){
					PLives.get(DPPredict.QX)[X] = 2*PLives.get(DPPredict.MX)[X]/(2+PLives.get(DPPredict.QX)[X]);
				}
				
				//******第四步:计算尚存人数******
				double[] Q = new double[Const.MAX_AGE];
				//COPY  TO ARRA Q  FIEL QX
				for(X=Const.MAX_AGE-1;X>=0;X--){
					Q[X] = PLives.get(DPPredict.QX)[X];
				}
				X=0;
				PLives.get(DPPredict.LX1)[X] = 100000;
				
				for(X=0;X<Const.MAX_AGE;X++){
					double L = PLives.get(DPPredict.LX1)[X]*(1-Q[X+1]);
					X++;
					PLives.get(DPPredict.LX1)[X] = L; 
				}
				
				//******第五步:计算死亡人数******
				for(X=Const.MAX_AGE-1;X>=0;X--){
					PLives.get(DPPredict.DX)[X] = PLives.get(DPPredict.QX)[X] * PLives.get(DPPredict.LX1)[X];
				}
				
				//******第六步:计算生存人年数******
				X=MANL-1;//TODO forxpro的原句是这样的:LOCA FOR X=MANL  &&  GO  BOTT
				PLives.get(DPPredict.LX2)[X] = PLives.get(DPPredict.LX1)[X] * PLives.get(DPPredict.AX)[X];
				while(X>0){
					double L0 = PLives.get(DPPredict.LX1)[X];
					X--;
					PLives.get(DPPredict.LX2)[X] = PLives.get(DPPredict.AX)[X] * PLives.get(DPPredict.DX)[X] + L0;   
				}
				
				//******第七步:X岁以后生存人年总数******
				X=Const.MAX_AGE-1;
				double TX0 = 0;
				while(X>=0){
					PLives.get(DPPredict.TX)[X] = TX0 + PLives.get(DPPredict.LX1)[X];
					TX0 = PLives.get(DPPredict.TX)[X];
					X--;
				}
				//******第八步:确切年龄X岁的平均预期寿命******
				for(X=Const.MAX_AGE-1;X>=0;X--){
					if(PLives.get(DPPredict.LX1)[X] != 0){
						PLives.get(DPPredict.EX)[X] = PLives.get(DPPredict.TX)[X]/PLives.get(DPPredict.LX1)[X];
					}
				}
				
				//******第九步:表上死亡率******
				for(X=Const.MAX_AGE-1;X>=0;X--){
					if(PLives.get(DPPredict.LX2)[X] != 0){
						PLives.get(DPPredict.MX2)[X] = PLives.get(DPPredict.DX)[X]/PLives.get(DPPredict.LX2)[X];
					}
				}
				Integer a = 20;
				a=10;
				
				
				NExpectedAge.get(dqx).get(cxa).put(xba,PLives.get(DPPredict.EX)[0]);
		    }
		}
    }
}