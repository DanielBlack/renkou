package ds.importData;

import java.util.Map;

import prclqz.core.Const;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.CXAll;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.XBAll;
import prclqz.core.enumLib.Year;

public class MAgeDistributionPatternOnRT {

	/**
	 * 迁移参数/分省人口城市化和迁移概率预测/基于留存率的总和迁移预测和年龄分布模式
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public void calculate(Map<String,Object> globals){
		int dqx = 11;
		//输入:各地区城市化水平预测2010-2160,每年有三列,分别是:地区,地区yc,地区zl ----- forxpro变量:各地城市化预测.
		Map<Year,double[]> dqUrbanizationPredict = (Map<Year,double[]>)globals.get("dqUrbanizationPredice"+dqx);
		// QY43各地区分城乡农非性别的总和迁移人次及弹性_基于留存率的分析 ----- foxpro变量: 总和迁移及弹性
		Map<XBAll,Map<CXAll,Double>>cxaRTResult = (Map<XBAll, Map<CXAll, Double>>)globals.get("cxRTResult"+dqx);
		Map<XBAll,Map<NY, Double>> nyRTResult = (Map<XBAll,Map<NY, Double>>)globals.get("nyRTResult"+dqx);
		Map<XBAll,Map<CX,Map<NY,Double>>> cxnyRTResult = (Map<XBAll,Map<CX,Map<NY,Double>>>)globals.get("cxnyRTResult"+dqx);
			//上面三组加后缀"弹x"
		Map<XBAll,Map<CXAll,Double>>cxaRTResultTX = (Map<XBAll, Map<CXAll, Double>>)globals.get("cxRTResultTX"+dqx);
		Map<XBAll,Map<NY, Double>> nyRTResultTX = (Map<XBAll,Map<NY, Double>>)globals.get("nyRTResultTX"+dqx);
		Map<XBAll,Map<CX,Map<NY,Double>>> cxnyRTResultTX = (Map<XBAll,Map<CX,Map<NY,Double>>>)globals.get("cxnyRTResultTX"+dqx);
		//czrk[3] 分别对应 czrk09, czrk10, czrkzl
		double[] czrk = (double[])globals.get("czrk"+dqx);
		//输入: 迁移参数\输入数据3\QY34'-DQB-'按城乡户口性质性别年龄分的净迁移概率_基于留存率的分析.DBF  ----- foxpro变量: 基于留存净迁概率
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosOnRT = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosOnRT"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosOnRT = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosOnRT"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosOnRT = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosOnRT"+dqx);
			//上面三组加"分布", 111行
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosOnRTDis = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosOnRTDis"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosOnRTDis = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosOnRTDis"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosOnRTDis = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosOnRTDis"+dqx);
		
		//输出:QY44xx省分城乡农非性别的总和迁移人次预测_基于留存率的分析 ----- foxpro变量: DQ总和迁移预测   年份:1990-2160
		Map<XBAll, Map<CXAll, Map<Year, Double>>> cxadqSumPredict = (Map<XBAll, Map<CXAll, Map<Year, Double>>>)globals.get("cxadqSumPredict"+dqx);
		Map<XBAll, Map<NY, Map<Year, Double>>> nydqSumPredict = (Map<XBAll, Map<NY, Map<Year, Double>>>)globals.get("nydqxSumPredict"+dqx);
		Map<XBAll, Map<CX, Map<NY, Map<Year, Double>>>> cxnydqSumPredict = (Map<XBAll, Map<CX, Map<NY, Map<Year, Double>>>>)globals.get("cxnydqSumPredict"+dqx); 
		//输出:QY45'-DQB-'按城乡户口性质性别分的净迁移概率年龄分布模式_基于留存率的分析.DBF ----- foxpro变量: 年龄分布模式
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosAgeDisOnRT = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosAgeDisOnRT"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosAgeDisOnRT = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosAgeDisOnRT"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosAgeDisOnRT = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosAgeDisOnRT"+dqx);
			//上面三组加"分布", 111行
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosAgeDisOnRTDis = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosAgeDisOnRTDis"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosAgeDisOnRTDis = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosAgeDisOnRTDis"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosAgeDisOnRTDis = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosAgeDisOnRTDis"+dqx);
		/************************************************************************************************************************************/
		//FOR F1=2 TO Fn
		for(XBAll xba:XBAll.values()){
			for(CXAll cxa:CXAll.values()){
				//弹XT=&FIETX
				double TXT = cxaRTResultTX.get(xba).get(cxa);
				//REPLACE 总和迁移预测.&FIE0 WITH 弹XT*城市化预测.&DQBZL all
				for(int year = 1960; year<=2160; year++){
					cxadqSumPredict.get(xba).get(cxa).put(Year.getYear(year), TXT*dqUrbanizationPredict.get(Year.getYear(year))[3]);
				}
			}
			for(NY ny:NY.values()){
				//弹XT=&FIETX
				double TXT = nyRTResultTX.get(xba).get(ny);
				//REPLACE 总和迁移预测.&FIE0 WITH 弹XT*城市化预测.&DQBZL all
				for(int year = 1960; year<=2160; year++){
					nydqSumPredict.get(xba).get(ny).put(Year.getYear(year), TXT*dqUrbanizationPredict.get(Year.getYear(year))[3]);
				}
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					//弹XT=&FIETX
					double TXT = cxnyRTResultTX.get(xba).get(cx).get(ny);
					//REPLACE 总和迁移预测.&FIE0 WITH 弹XT*城市化预测.&DQBZL all
					for(int year = 1960; year<=2160; year++){
						cxnydqSumPredict.get(xba).get(cx).get(ny).put(Year.getYear(year), TXT*dqUrbanizationPredict.get(Year.getYear(year))[3]);
					}
				}
			}
		}
		/************年龄分布模式**********************/
		//COPY FILE &基于留存净迁概率 TO &年龄分布模式
		for(XBAll xba:XBAll.values()){
			for(CXAll cxa:CXAll.values()){
				for(int i=0;i<Const.MAX_AGE;i++){
					cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i] = cxaNMPosOnRT.get(xba).get(cxa)[i];
					cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i] = cxaNMPosOnRTDis.get(xba).get(cxa)[i];
				}
			}
			for(NY ny:NY.values()){
				for(int i=0;i<Const.MAX_AGE;i++){
					nyNMPosAgeDisOnRT.get(xba).get(ny)[i] = nyNMPosOnRT.get(xba).get(ny)[i];
					nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i] = nyNMPosOnRTDis.get(xba).get(ny)[i];
				}
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					for(int i=0;i<Const.MAX_AGE;i++){
						cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i] = cxnyNMPosOnRT.get(xba).get(cx).get(ny)[i];
						cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i] = cxnyNMPosOnRTDis.get(xba).get(cx).get(ny)[i];
					}
				}
			}
		}

		for(XBAll xba:XBAll.values()){
			for(CXAll cxa:CXAll.values()){
				//REPLACE &fie1 WITH 0 FOR x>80
				for(int i=0;i<Const.MAX_AGE;i++){
					if(i>79){
						cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i] = 0;
						cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i] = 0;
					}
				}
				for(int cs=0;cs<2;cs++){
					//SUM &fie1 TO s1
					double s1 = 0;
					double s1Dis = 0;
					for(int i=0;i<Const.MAX_AGE;i++){
						s1 += cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i];
						s1Dis += cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i];
					}
					//REPLACE &fie1 WITH &fie1/s1 all
					for(int i=0;i<Const.MAX_AGE;i++){
						cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i] = cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i]/s1;
						cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i] = cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i]/s1Dis;
					}
					//REPLACE &fie1 WITH (ar2(RECNO()-1)+ar2(RECNO())+ar2(RECNO()+1))/3 FOR RECNO()>1.and.recn()<RECCOUNT()
					for(int i=1;i<Const.MAX_AGE-1;i++){
						cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i] = (cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i-1]+cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i]+cxaNMPosAgeDisOnRT.get(xba).get(cxa)[i+1])/3;
						cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i] = (cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i-1]+cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i]+cxaNMPosAgeDisOnRTDis.get(xba).get(cxa)[i+1])/3;
					}
				}
			}
			for(NY ny:NY.values()){
				//REPLACE &fie1 WITH 0 FOR x>80
				for(int i=0;i<Const.MAX_AGE;i++){
					if(i>79){
						nyNMPosAgeDisOnRT.get(xba).get(ny)[i] = 0;
						nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i] = 0;
					}
				}
				for(int cs=0;cs<2;cs++){
					//SUM &fie1 TO s1
					double s1 = 0;
					double s1Dis = 0;
					for(int i=0;i<Const.MAX_AGE;i++){
						s1 += nyNMPosAgeDisOnRT.get(xba).get(ny)[i];
						s1Dis += nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i];
					}
					//REPLACE &fie1 WITH &fie1/s1 all
					for(int i=0;i<Const.MAX_AGE;i++){
						nyNMPosAgeDisOnRT.get(xba).get(ny)[i] = nyNMPosAgeDisOnRT.get(xba).get(ny)[i]/s1;
						nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i] = nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i]/s1Dis;
					}
					//REPLACE &fie1 WITH (ar2(RECNO()-1)+ar2(RECNO())+ar2(RECNO()+1))/3 FOR RECNO()>1.and.recn()<RECCOUNT()
					for(int i=1;i<Const.MAX_AGE-1;i++){
						nyNMPosAgeDisOnRT.get(xba).get(ny)[i] = (nyNMPosAgeDisOnRT.get(xba).get(ny)[i-1]+nyNMPosAgeDisOnRT.get(xba).get(ny)[i]+nyNMPosAgeDisOnRT.get(xba).get(ny)[i+1])/3;
						nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i] = (nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i-1]+nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i]+nyNMPosAgeDisOnRTDis.get(xba).get(ny)[i+1])/3;
					}
				}
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					//REPLACE &fie1 WITH 0 FOR x>80
					for(int i=0;i<Const.MAX_AGE;i++){
						if(i>79){
							cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i] = 0;
							cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i] = 0;
						}
					}
					for(int cs=0;cs<2;cs++){
						//SUM &fie1 TO s1
						double s1 = 0;
						double s1Dis = 0;
						for(int i=0;i<Const.MAX_AGE;i++){
							s1 += cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i];
							s1Dis += cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i];
						}
						//REPLACE &fie1 WITH &fie1/s1 all
						for(int i=0;i<Const.MAX_AGE;i++){
							cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i] = cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i]/s1;
							cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i] = cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i]/s1Dis;
						}
						//REPLACE &fie1 WITH (ar2(RECNO()-1)+ar2(RECNO())+ar2(RECNO()+1))/3 FOR RECNO()>1.and.recn()<RECCOUNT()
						for(int i=1;i<Const.MAX_AGE-1;i++){
							cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i] = (cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i-1]+cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i]+cxnyNMPosAgeDisOnRT.get(xba).get(cx).get(ny)[i+1])/3;
							cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i] = (cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i-1]+cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i]+cxnyNMPosAgeDisOnRTDis.get(xba).get(cx).get(ny)[i+1])/3;
						}
					}
				}
			}
		}
		
	}
}
