package ds.importData;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.core.Const;
import prclqz.core.enumLib.Logistic;
import prclqz.core.enumLib.LogisticModelEnum;
import prclqz.core.enumLib.QYPredict;
import prclqz.core.enumLib.XBAll;
import prclqz.core.enumLib.Year;

public class MUrbanizationLogisticSim {

	/**
	 * 迁移参数/分省人口城市化和迁移概率预测/分省城市化逻辑斯蒂模拟与预测 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void calculate(IDAO database,HashMap<String,Object> globals) throws Exception {
		
		int dqx = 1;
		int X;
		String dqName = Const.getProvinceName(dqx);
		//input
		//各地区总人口及城乡构成49_10中的两个字段,年份从1949年开始
		Map<Year,Double> UrbanizedLevel = (Map<Year,Double>)globals.get("UrbanizedLevel");
		//从1990开始的计数
		int yearNum = UrbanizedLevel.keySet().size()-41;
		//Qy42各地区分省际省内城乡性别的总和迁移人次及弹性t/m/f,注意不完全和此表相同,只有QYPredict具有的几列.
		Map<XBAll,Map<QYPredict,Double>> qyRcGL = (Map<XBAll,Map<QYPredict,Double>>)globals.get("QYPopulationGL"+dqx);
		Map<XBAll,Map<QYPredict,Double>> qyRcTx = (Map<XBAll,Map<QYPredict,Double>>)globals.get("QYPopulationTX"+dqx);
		
		//output:
		//地区总和迁移预测TMF,结构:年份(1900-2160),QYPredict
		Map<XBAll,Map<Year,Map<QYPredict,Double>>> dqSumMigPredict  = (Map<XBAll,Map<Year,Map<QYPredict,Double>>>)globals.get("dqSumMigPredict"+dqx);
		//各地区城市化水平预测2010-2160,每年有三列,分别是:地区,地区yc,地区zl.
		Map<Year,double[]> dqUrbanizationPredict = (Map<Year,double[]>)globals.get("dqUrbanizationPredice"+dqx); 
		//分省城市化预测逻辑斯蒂模型90_10
		Map<LogisticModelEnum,Double> fsUrbanizePredictLogisticModel90_10 = (Map<LogisticModelEnum,Double>)globals.get("fsUrbanizePredictLogisticModel90_10"+dqx); 
		
		//Tempt:
		//对应AAA计算表,年份从1990开始到UrbanizedLevel的最后一年
		Map<Year,Map<Logistic,Double>> LogisticModel = new EnumMap<Year,Map<Logistic,Double>>(Year.class);
		for(int i=0;i<yearNum;i++)
			LogisticModel.put(Year.getYear(i+1990), new EnumMap<Logistic,Double>(Logistic.class));
		//////////////translated by Foxpro2Java Translator successfully:///////////////
	
		//工作区:21:T,22:N,23:F
		for(int year = 1990;year<=2160;year++){
			if(UrbanizedLevel.get(Year.getYear(year))!=null)
				LogisticModel.get(Year.getYear(year)).put(Logistic.Y, UrbanizedLevel.get(Year.getYear(year)));
		}
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				LogisticModel.get(Year.getYear(X+1990)).put(Logistic.X, (double) X);
			}
		}
		//K = IIF(DQB='上海',95,IIF(DQB='北京' or DQB='天津',90,IIF(DQB='江苏' or DQB='浙江',85,IIF(DQB='辽宁' or DQB='吉林' or DQB='黑龙江',80,75))));
		int K;
		if(dqName.equals("上海"))
			K = 95;
		else if(dqName.equals("北京") || dqName.equals("天津"))
			K = 90;
		else if(dqName.equals("江苏") || dqName.equals("浙江"))
			K = 85;
		else if(dqName.equals("辽宁") || dqName.equals("吉林") || dqName.equals("黑龙江"))
			K = 80;
		else 
			K = 75;
		int N;
		N = LogisticModel.size();
		/////////////end of translating, by Foxpro2Java Translator///////////////
		
		//////////////translated by Foxpro2Java Translator successfully:///////////////
		//REPLACE Y1 WITH LOG((K-Y)/Y) ALL
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				double Y = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y);
				LogisticModel.get(Year.getYear(X+1990)).put(Logistic.Y1, Math.log((K-Y)/Y));
			}
		}
		double AVX=0,AVY1=0;
		//TODO CALCULATE AVG(X),AVG(Y1) TO AVX,AVY1
		double tmpSumX=0,tmpSumY1=0;
		double XY1 = 0;
		double SX = 0;
		double SY1 = 0;
		double XX = 0;
		double Y1Y1 = 0;
		for(X = yearNum-1; X>=0; X--){
			double bX = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.X);
			double bY1 = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y1);
			
			tmpSumX += bX;
			tmpSumY1 += bY1;			
		}
		AVX = tmpSumX/yearNum;
		AVY1 = tmpSumY1/yearNum;
		
		
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				double x = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.X);
				double y1 = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y1);
				XY1 += x * y1;
				SX += x;
				SY1 += y1;
				XX += x * x;
				Y1Y1 += y1 * y1;
			}
		}
		
		double B = ( XY1 - SX * SY1 / N ) / ( XX - SX*SX / N );
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				double y1 = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y1);
				LogisticModel.get(Year.getYear(X+1990)).put(Logistic.Y2, y1 - AVY1);
			}
			if( true ){
				double x = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.X);
				LogisticModel.get(Year.getYear(X+1990)).put(Logistic.X1, x-AVX);
			}
		}
		/////////////end of translating, by Foxpro2Java Translator///////////////
		//CALCULATE AVG(Y2) TO AVY2
		double tmpSumY2=0;
		for(X=yearNum-1; X>=0; X--){
			tmpSumY2 += LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y2);
		}
		double AVY2 = tmpSumY2/yearNum;
		//////////////translated by Foxpro2Java Translator successfully:///////////////
		double A = Math.exp(AVY1-B*AVX);
		//SUM X1*X1,Y2*Y2,X1*Y2 TO X1X1,Y2Y2, X1Y2
		double X1X1 = 0;
		double Y2Y2 = 0;
		double X1Y2 = 0;
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				double x1 = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.X1);
				double y2 = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y2);
				X1X1 += x1 * x1;
				Y2Y2 += y2 * y2;
				X1Y2 += x1 * y2;
			}
		}
		//r=X1Y2/((X1X1*Y2Y2)**(1/2))	&&正确！  **(1/2)应该是1/2次方吧= ,= 
		double r = X1Y2 / ( Math.sqrt( X1X1 * Y2Y2 ));
		//REPLACE YC WITH K/(1+a*EXP(b*x)) all
		for(X = yearNum-1; X >= 0 ; X--){
			if( true ){
				double x = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.X);
				LogisticModel.get(Year.getYear(X+1990)).put(Logistic.YC,  K / ( 1.0 + A * Math.exp(B*x) ));
			}
		}
		//GO BOTTOM 
		//b10=y/yc
		X = yearNum-1;
		double y = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y);
		double yc = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.YC);
		double b10 = y / yc;
		
		//SELECT C:	USE D:\TODO\分省生育政策仿真部分基本参数预测\迁移参数\输出数据\分省城市化预测逻辑斯蒂模型90_10
		//LOCATE FOR 地区=DQB
		//REPLACE SQ WITH '1990_2010',RSQ WITH R,DF WITH N,BOUN WITH K,B0 WITH A,B1 WITH B,因变量 WITH '城市化插值'
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.start,(double) 1990);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.end,(double) 2010);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.Rsq,r);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.Df,(double) N);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.BOUN,(double) K);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.B0,A);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.B1,B);
		fsUrbanizePredictLogisticModel90_10.put(LogisticModelEnum.variable,(double) 0);
		
		//select 20
		for(X = 170; X >= 0; X--){
			if( X<yearNum-1 ){
				dqUrbanizationPredict.get(Year.getYear(X+1990))[0] = LogisticModel.get(Year.getYear(X+1990)).get(Logistic.Y);
			}
			else 
				dqUrbanizationPredict.get(Year.getYear(X+1990))[0] = 0;
			dqUrbanizationPredict.get(Year.getYear(X+1990))[1] = K/(1.0 + A*Math.exp(B*(X+1)));
		}
		// 1990年的dqzl要不要??foxpro里是空的.
		for(X=1;X<=170;X++){
			dqUrbanizationPredict.get(Year.getYear(X+1990))[2] = dqUrbanizationPredict.get(Year.getYear(X+1990))[1] - dqUrbanizationPredict.get(Year.getYear(X+1989))[1];
		}
		
		for(QYPredict qy:QYPredict.values()){
			for(XBAll xba:XBAll.values()){
				for(X=2160;X>=1990;X--){
					dqSumMigPredict.get(xba).get(Year.getYear(X)).put(qy,qyRcTx.get(xba).get(qy)*dqUrbanizationPredict.get(Year.getYear(X))[2]);
				}
			}
		}
	}
/////////////end of translating, by Foxpro2Java Translator///////////////

}
