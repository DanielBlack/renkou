package ds.importData;

import java.util.Map;

import prclqz.core.enumLib.QYPredict;
import prclqz.core.enumLib.XBAll;

public class MAgeDistributionPattern {

	/**
	 * 迁移参数/分省人口城市化和迁移概率预测/总和迁移人次年龄分布模式
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public void calculate(Map<String,Object> globals){
		int dqx = 11;
		//TODO: ATTENTION! foxpro中这个是复制了一个新表,此处是原地修改,不知道有木有问题.
		//D:\TODO\分省生育政策仿真部分基本参数预测\迁移参数\输入数据2\QY42北京省际省内城乡分年龄迁移概率T/M/F
		Map<XBAll,Map<QYPredict,double[]>> qygl = (Map<XBAll,Map<QYPredict,double[]>>)globals.get("MigrationPossibility"+dqx); 
		
		for(XBAll xba:XBAll.values()){
			for(QYPredict qyp:QYPredict.values()){
				if(qyp != QYPredict.ChengZQ){
					for(int i=0;i<2;i++){
						double sum = 0;
						for(int j=0; j<qygl.get(xba).get(qyp).length; j++)
							sum += qygl.get(xba).get(qyp)[j];
						for(int j=0; j<qygl.get(xba).get(qyp).length; j++)
							qygl.get(xba).get(qyp)[j] /= sum;
						double tmp[] = qygl.get(xba).get(qyp).clone();
						for(int j=1; j<qygl.get(xba).get(qyp).length-1; j++)
							qygl.get(xba).get(qyp)[j] = (tmp[j-1] + tmp[j] + tmp[j+1])/3;
					}
				}
			}
		}
	}
}
