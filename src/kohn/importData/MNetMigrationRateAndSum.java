package ds.importData;

import java.util.Map;

import ds.function.EnumMapTool;

import prclqz.core.Const;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.CXAll;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.QYPredict;
import prclqz.core.enumLib.XBAll;

public class MNetMigrationRateAndSum {

	/**
	 * 分省人口城市化和迁移概率预测/利用迁移留存率估计当年净迁移率和总和迁移人次
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	/**
	 * 调用的时候注意没有全国
	 * @param globals
	 */
	public void calculate(Map<String,Object> globals){
		int dqx = 11;
		//输出结果: QY43各地区分城乡农非性别的总和迁移人次及弹性_基于留存率的分析
		//RT = Retention Rate = 留存率
		Map<XBAll,Map<CXAll,Double>>cxaRTResult = (Map<XBAll, Map<CXAll, Double>>)globals.get("cxRTResult"+dqx);
		Map<XBAll,Map<NY, Double>> nyRTResult = (Map<XBAll,Map<NY, Double>>)globals.get("nyRTResult"+dqx);
		Map<XBAll,Map<CX,Map<NY,Double>>> cxnyRTResult = (Map<XBAll,Map<CX,Map<NY,Double>>>)globals.get("cxnyRTResult"+dqx);
		//上面三组加后缀"弹x"
		Map<XBAll,Map<CXAll,Double>>cxaRTResultTX = (Map<XBAll, Map<CXAll, Double>>)globals.get("cxRTResultTX"+dqx);
		Map<XBAll,Map<NY, Double>> nyRTResultTX = (Map<XBAll,Map<NY, Double>>)globals.get("nyRTResultTX"+dqx);
		Map<XBAll,Map<CX,Map<NY,Double>>> cxnyRTResultTX = (Map<XBAll,Map<CX,Map<NY,Double>>>)globals.get("cxnyRTResultTX"+dqx);
		//czrk[3] 分别对应 czrk09, czrk10, czrkzl
		double[] czrk = (double[])globals.get("czrk"+dqx);
		/*****************************************/
		//输入:
		//基础数据\分地区\xx省\xx省分城乡户口性质年龄性别人口数推算_基于5岁组数据（2010）.DBF ----- foxpro变量: 城乡人口
		Map<XBAll,Map<CXAll,double[]>>cxaPopulation = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxPopulation"+dqx);
		Map<XBAll,Map<NY, double[]>> nyPopulation = (Map<XBAll,Map<NY, double[]>>)globals.get("nyPopulation"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyPopulation = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyPopulation"+dqx);
		double[] czrkb = (double[])globals.get("RTczrkb"+dqx); 
		
		//迁移参数\QY33xx省'按城乡户口性质性别年龄分的净迁移概率.DBF(共111行)----- foxpro变量:净迁概率半_1
		Map<XBAll, Map<CXAll, double[]>> cxaNMPos = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPos"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPos = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPos"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPos = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPos"+dqx);
			//上面三组加"半_1","1_2"等后缀, double[][]是一个7*111的二维数组,分别对应"半_1","1_2","2_3","3_4","4_5","5_6","6"
		Map<XBAll, Map<CXAll, double[][]>> cxaNMPosH = (Map<XBAll, Map<CXAll, double[][]>>)globals.get("cxaNMPosH"+dqx);
		Map<XBAll,Map<NY, double[][]>> nyNMPosH = (Map<XBAll,Map<NY, double[][]>>)globals.get("nyNMPosH"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[][]>>> cxnyNMPosH = (Map<XBAll,Map<CX,Map<NY,double[][]>>>)globals.get("cxnyNMPosH"+dqx);
		
		//迁移参数\输入数据3\QY34xx省按城乡户口性质性别年龄分的迁移留存率.DBF ----- foxpro变量: 迁移留存率
		Map<XBAll, Map<CXAll, double[]>> cxaMigrationRT = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaMigrationRT"+dqx);
		Map<XBAll,Map<NY, double[]>> nyMigrationRT = (Map<XBAll,Map<NY, double[]>>)globals.get("nyMigrationRT"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyMigrationRT = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyMigrationRT"+dqx);
			//上面三组加"半_1","1_2"等后缀, double[][]是一个7*111的二维数组,分别对应"半_1","1_2","2_3","3_4","4_5","5_6","6"
		Map<XBAll, Map<CXAll, double[][]>> cxaMigrationRTH = (Map<XBAll, Map<CXAll, double[][]>>)globals.get("cxaMigrationRTH"+dqx);
		Map<XBAll,Map<NY, double[][]>> nyMigrationRTH = (Map<XBAll,Map<NY, double[][]>>)globals.get("nyMigrationRTH"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[][]>>> cxnyMigrationRTH = (Map<XBAll,Map<CX,Map<NY,double[][]>>>)globals.get("cxnyMigrationRTH"+dqx);
		
		//迁移参数\输入数据3\QY34'-DQB-'按城乡户口性质性别年龄分的净迁移概率_基于留存率的分析.DBF ----- foxpro变量: 基于留存净迁移概率
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosOnRT = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosOnRT"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosOnRT = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosOnRT"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosOnRT = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosOnRT"+dqx);
			//上面三组加"分布", 111行
		Map<XBAll, Map<CXAll, double[]>> cxaNMPosOnRTDis = (Map<XBAll, Map<CXAll, double[]>>)globals.get("cxaNMPosOnRTDis"+dqx);
		Map<XBAll,Map<NY, double[]>> nyNMPosOnRTDis = (Map<XBAll,Map<NY, double[]>>)globals.get("nyNMPosOnRTDis"+dqx);
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyNMPosOnRTDis = (Map<XBAll,Map<CX,Map<NY,double[]>>>)globals.get("cxnyNMPosOnRTDis"+dqx);
		
		/*************************************************************************************************************************************/
		//临时变量: 当年迁入人口
		Map<XBAll, Map<CXAll, double[]>> cxaDNin = EnumMapTool.creatCXANetMigrationPopulation();
		Map<XBAll,Map<NY, double[]>> nyDNin = EnumMapTool.creatNYNetMigrationPopulation();
		Map<XBAll,Map<CX,Map<NY,double[]>>> cxnyDNin = EnumMapTool.creatCXNYNetMigrationPopulation();
			//上面三组加"半_1","1_2"等后缀, double[][]是一个7*111的二维数组,分别对应"半_1","1_2","2_3","3_4","4_5","5_6","6"
		Map<XBAll, Map<CXAll, double[][]>> cxaDNinH = EnumMapTool.creatCXANetMigrationPopulation2d();
		Map<XBAll,Map<NY, double[][]>> nyDNinH = EnumMapTool.creatNYNetMigrationPopulation2d();
		Map<XBAll,Map<CX,Map<NY,double[][]>>> cxnyDNinH = EnumMapTool.creatCXNYNetMigrationPopulation2d();
		
		/******计算存留率******/
		for(XBAll xba:XBAll.values()){
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					//REPLACE 当年迁入.&FIE0 WITH 城乡人口.&ZRK*净迁概率半_1.&FIE0 ALL
					for(int i=0;i<Const.MAX_AGE;i++){
						cxnyDNinH.get(xba).get(cx).get(ny)[0][i] = cxnyPopulation.get(xba).get(cx).get(ny)[i]*cxnyNMPosH.get(xba).get(cx).get(ny)[0][i];//0表示"半_1" 
					}
					//REPLACE 当年迁入.&QYRK WITH 当年迁入.&FIE0 all
					for(int i=0;i<Const.MAX_AGE;i++){
						cxnyDNin.get(xba).get(cx).get(ny)[i] = cxnyDNinH.get(xba).get(cx).get(ny)[0][i];  
					}
					/*
					 * FOR T1=1 TO 6
					 * FIE1=CXB-XB-IIF(t1=1,'半_1',IIF(t1=2,'1_2',IIF(t1=3,'2_3',IIF(t1=4,'3_4',IIF(t1=5,'4_5',IIF(t1=6,'5_6','6'))))))
					 * FIE2=CXB-XB-IIF(t1=1,'1_2',IIF(t1=2,'2_3',IIF(t1=3,'3_4',IIF(t1=4,'4_5',IIF(t1=5,'5_6','6')))))
				     * COPY TO ARRAY AR1 FIELDS 当年迁入.&FIE1,留存率.&FIE1
				 	 * REPLACE 当年迁入.&FIE2 WITH ar1(RECNO()-1,1)*(ar1(RECNO()-1,2)-1) FOR RECNO()>1
					 * REPLACE 当年迁入.&QYRK WITH 当年迁入.&QYRK+当年迁入.&FIE2 all
					 * ENDFOR
					 */
					for(int i=0;i<6;i++){//遍历"半_1","1_2"...
						for(int j=1;j<Const.MAX_AGE;j++){
							cxnyDNinH.get(xba).get(cx).get(ny)[i+1][j] = cxnyDNinH.get(xba).get(cx).get(ny)[i][j-1]*cxnyMigrationRT.get(xba).get(cx).get(ny)[j-1];
						}
						for(int j=0;j<Const.MAX_AGE;j++){
							cxnyDNin.get(xba).get(cx).get(ny)[j] = cxnyDNin.get(xba).get(cx).get(ny)[j] + cxnyDNinH.get(xba).get(cx).get(ny)[i+1][j];
						}
					}
				}
			}
		}
		/*****************************/
		for(XBAll xba:XBAll.values()){
			for(CX cx:CX.values()){
				for(int i=0;i<Const.MAX_AGE;i++){
					cxaDNin.get(xba).get(cx.toCXAll())[i] = cxnyDNin.get(xba).get(cx).get(NY.Nongye)[i] + cxnyDNin.get(xba).get(cx).get(NY.Feinong)[i]; 
				}
			}
			for(NY ny:NY.values()){
				for(int i=0;i<Const.MAX_AGE;i++){
					nyDNin.get(xba).get(ny)[i] = cxnyDNin.get(xba).get(CX.Chengshi).get(ny)[i] + cxnyDNin.get(xba).get(CX.Nongchun).get(ny)[i];
				}
			}
			/*
			 * REPLACE 当年迁入.M WITH 当年迁入.城镇M+当年迁入.农村M ALL 
			 * REPLACE 当年迁入.F WITH 当年迁入.城镇F+当年迁入.农村F ALL 
			 * REPLACE 当年迁入.T WITH 当年迁入.城镇T+当年迁入.农村T ALL 
			 */
			for(int i=0;i<Const.MAX_AGE;i++){
				cxaDNin.get(xba).get(CXAll.Chengxiang)[i] = cxaDNin.get(xba).get(CXAll.Chengzhen)[i] + cxaDNin.get(xba).get(CXAll.Nongcun)[i];
			}
		}
		/*****************************/
		for(XBAll xba:XBAll.values()){
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					//REPLACE 留存净迁概率.&CXB WITH 当年迁入.&CXB/城乡人口.&ZRK FOR 城乡人口.&ZRK<>0
					for(int i=0;i<Const.MAX_AGE;i++){
						if(cxnyPopulation.get(xba).get(cx).get(ny)[i] != 0)
							cxnyNMPosOnRT.get(xba).get(cx).get(ny)[i] = cxnyDNin.get(xba).get(cx).get(ny)[i]/cxnyPopulation.get(xba).get(cx).get(ny)[i];
					}
					//SUM 留存净迁概率.&CXB TO SGL
					double SGL = 0;
					for(int i=0;i<Const.MAX_AGE;i++){
						SGL += cxnyNMPosOnRT.get(xba).get(cx).get(ny)[i];
					}
					//REPLACE 留存净迁概率.&分布 WITH 留存净迁概率.&CXB/SGL all
					for(int i=0;i<Const.MAX_AGE;i++){
						cxnyNMPosOnRTDis.get(xba).get(cx).get(ny)[i] = cxnyNMPosOnRT.get(xba).get(cx).get(ny)[i]/SGL;  
					}
				}
			}
			
			for(CX cx:CX.values()){
				//REPLACE 留存净迁概率.&CXB WITH 当年迁入.&CXB/城乡人口.&CXB FOR 城乡人口.&cxb<>0
				for(int i=0;i<Const.MAX_AGE;i++){
					if(cxaPopulation.get(xba).get(cx.toCXAll())[i] != 0){
						cxaNMPosOnRT.get(xba).get(cx.toCXAll())[i] = cxaDNin.get(xba).get(cx.toCXAll())[i]/cxaPopulation.get(xba).get(cx.toCXAll())[i];
					}
				}
				//SUM 留存净迁概率.&CXB TO SGL
				double SGL = 0;
				for(int i=0;i<Const.MAX_AGE;i++){
					SGL += cxaNMPosOnRT.get(xba).get(cx.toCXAll())[i];
				}
				//REPLACE 留存净迁概率.&分布 WITH 留存净迁概率.&CXB/SGL all
				for(int i=0;i<Const.MAX_AGE;i++){
					cxaNMPosOnRTDis.get(xba).get(cx.toCXAll())[i] = cxaNMPosOnRT.get(xba).get(cx.toCXAll())[i]/SGL;  
				}
			}
			for(NY ny:NY.values()){
				//REPLACE 留存净迁概率.&NYB WITH 当年迁入.&NYB/城乡人口.&NYB FOR 城乡人口.&nyb<>0
				for(int i=0;i<Const.MAX_AGE;i++){
					if(nyPopulation.get(xba).get(ny)[i] != 0){
						nyNMPosOnRT.get(xba).get(ny)[i] = nyDNin.get(xba).get(ny)[i]/nyPopulation.get(xba).get(ny)[i];
					}
				}
				//SUM 留存净迁概率.&NYB TO SGL
				double SGL = 0;
				for(int i=0;i<Const.MAX_AGE;i++){
					SGL += nyNMPosOnRT.get(xba).get(ny)[i];
				}
				//REPLACE 留存净迁概率.&分布 WITH 留存净迁概率.&NYB/SGL all
				for(int i=0;i<Const.MAX_AGE;i++){
					nyNMPosOnRTDis.get(xba).get(ny)[i] = nyNMPosOnRT.get(xba).get(ny)[i]/SGL;  
				}
			}
			
			/*
			 * 分布=XB-'分布'
			 * REPLACE 留存净迁概率.&XB WITH 当年迁入.&XB/城乡人口.&XB FOR 城乡人口.&xb<>0
			 * SUM 留存净迁概率.&XB TO SGL
			 * REPLACE 留存净迁概率.&分布 WITH 留存净迁概率.&XB/SGL all
			 */
			for(int i=0;i<Const.MAX_AGE;i++){
				if(cxaPopulation.get(xba).get(CXAll.Chengxiang)[i] != 0){
					cxaNMPosOnRT.get(xba).get(CXAll.Chengxiang)[i] = cxaDNin.get(xba).get(CXAll.Chengxiang)[i]/cxaPopulation.get(xba).get(CXAll.Chengxiang)[i];
				}
			}
			double SGL = 0;
			for(int i=0;i<Const.MAX_AGE;i++){
				SGL += cxaNMPosOnRT.get(xba).get(CXAll.Chengxiang)[i];
			}
			for(int i=0;i<Const.MAX_AGE;i++){
				cxaNMPosOnRTDis.get(xba).get(CXAll.Chengxiang)[i] = cxaNMPosOnRT.get(xba).get(CXAll.Chengxiang)[i]/SGL;  
			}
		}
		/*
		 * GATHER FROM ar1 FIELDS T,M,F,城镇T,城镇M,城镇F,农村T,农村M,农村F,农业T,农业M,农业F,非农T,非农M,非农F,;
		 * 城农T,城农M,城农F,城非T,城非M,城非F,农农T,农农M,农农F,农非T,农非M,农非F
		 */
		for(XBAll xba:XBAll.values()){
			for(CXAll cxa:CXAll.values()){
				double sum = 0;
				for(int i=0;i<Const.MAX_AGE;i++)
					sum += cxaNMPosOnRT.get(xba).get(cxa)[i];
				cxaRTResult.get(xba).put(cxa, sum);
			}
			for(NY ny:NY.values()){
				double sum = 0;
				for(int i=0;i<Const.MAX_AGE;i++)
					sum += nyNMPosOnRT.get(xba).get(ny)[i];
				nyRTResult.get(xba).put(ny, sum);
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					double sum = 0;
					for(int i=0;i<Const.MAX_AGE;i++)
						sum += cxnyNMPosOnRT.get(xba).get(cx).get(ny)[i];
					cxnyRTResult.get(xba).get(cx).put(ny, sum);
				}
			}
		}
		/**************************************/
		//TODO REPLACE CZRKB09 WITH aaa09.czrkb,CZRKB10 WITH aaa10.czrkb,CZRKZL WITH CZRKB10-CZRKB09 all
		
		for(XBAll xba:XBAll.values()){
			for(CXAll cxa:CXAll.values()){
				if(czrk[2] != 0)
					cxaRTResultTX.get(xba).put(cxa, cxaRTResult.get(xba).get(cxa)/czrk[2]);
			}
			for(NY ny:NY.values()){
				if(czrk[2] != 0){
					nyRTResultTX.get(xba).put(ny, nyRTResult.get(xba).get(ny)/czrk[2]);
				}
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					if(czrk[2] !=0){
						cxnyRTResultTX.get(xba).get(cx).put(ny, cxnyRTResult.get(xba).get(cx).get(ny)/czrk[2]);
					}
				}
			}
		}
	} 
}
