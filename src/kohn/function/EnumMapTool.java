package ds.function;

import java.util.EnumMap;
import java.util.Map;

import prclqz.core.Const;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.CXAll;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.XBAll;

public class EnumMapTool {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * AAA当年净迁移人数表结构之一, CX(T/F/M)
	 */
	public static Map<XBAll, Map<CXAll, double[]>> creatCXANetMigrationPopulation(){
		Map<XBAll, Map<CXAll, double[]>> m = new EnumMap<XBAll, Map<CXAll, double[]>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<CXAll, double[]> mm = new EnumMap<CXAll, double[]>(CXAll.class);
			for(CXAll cxa:CXAll.values()){
				mm.put(cxa, new double[Const.MAX_AGE]);
			}
			m.put(xba, mm);
		}
		return m;
	}
	/**
	 * AAA当年净迁移人数表结构之一, NY(T/F/M)
	 */
	
	public static Map<XBAll, Map<NY, double[]>> creatNYNetMigrationPopulation(){
		Map<XBAll, Map<NY, double[]>> m = new EnumMap<XBAll, Map<NY, double[]>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<NY, double[]> mm = new EnumMap<NY, double[]>(NY.class);
			for(NY ny:NY.values()){
				mm.put(ny, new double[Const.MAX_AGE]);
			}
			m.put(xba, mm);
		}
		return m;
	}
	/**
	 * AAA当年净迁移人数表结构之一, CXNY(T/F/M)
	 */

	public static Map<XBAll, Map<CX, Map<NY, double[]>>> creatCXNYNetMigrationPopulation(){
		Map<XBAll, Map<CX, Map<NY, double[]>>> m = new EnumMap<XBAll, Map<CX, Map<NY, double[]>>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<CX, Map<NY, double[]>> mm = new EnumMap<CX, Map<NY, double[]>>(CX.class);
			for(CX cx:CX.values()){
				Map<NY, double[]> mmm = new EnumMap<NY, double[]>(NY.class);
				for(NY ny:NY.values()){
					mmm.put(ny, new double[Const.MAX_AGE]);
				}
				mm.put(cx, mmm);
			}
			m.put(xba, mm);
		}
		return m;
	}
		/**
	 * AAA当年净迁移人数表结构之一, CX(T/F/M)(半_1/1_2/...)
	 */
	public static Map<XBAll, Map<CXAll, double[][]>> creatCXANetMigrationPopulation2d(){
		Map<XBAll, Map<CXAll, double[][]>> m = new EnumMap<XBAll, Map<CXAll, double[][]>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<CXAll, double[][]> mm = new EnumMap<CXAll, double[][]>(CXAll.class);
			for(CXAll cxa:CXAll.values()){
				mm.put(cxa, new double[7][Const.MAX_AGE]);
			}
			m.put(xba, mm);
		}
		return m;
	}
	/**
	 * AAA当年净迁移人数表结构之一, NY(T/F/M)(半_1/1_2/...)
	 */
	
	public static Map<XBAll, Map<NY, double[][]>> creatNYNetMigrationPopulation2d(){
		Map<XBAll, Map<NY, double[][]>> m = new EnumMap<XBAll, Map<NY, double[][]>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<NY, double[][]> mm = new EnumMap<NY, double[][]>(NY.class);
			for(NY ny:NY.values()){
				mm.put(ny, new double[7][Const.MAX_AGE]);
			}
			m.put(xba, mm);
		}
		return m;
	}
	/**
	 * AAA当年净迁移人数表结构之一, CXNY(T/F/M)(半_1/1_2/...)
	 */
	public static Map<XBAll, Map<CX, Map<NY, double[][]>>> creatCXNYNetMigrationPopulation2d(){
		Map<XBAll, Map<CX, Map<NY, double[][]>>> m = new EnumMap<XBAll, Map<CX, Map<NY, double[][]>>>(XBAll.class);
		for(XBAll xba:XBAll.values()){
			Map<CX, Map<NY, double[][]>> mm = new EnumMap<CX, Map<NY, double[][]>>(CX.class);
			for(CX cx:CX.values()){
				Map<NY, double[][]> mmm = new EnumMap<NY, double[][]>(NY.class);
				for(NY ny:NY.values()){
					mmm.put(ny, new double[7][Const.MAX_AGE]);
				}
				mm.put(cx, mmm);
			}
			m.put(xba, mm);
		}
		return m;
	}
}
