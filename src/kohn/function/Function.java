package ds.function;

import java.text.DecimalFormat;
import java.util.Map;

import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Const;
import prclqz.core.enumLib.HunpeiField;

public class Function {

	public static void main(String[] argv){
		double a=21.5328;
		System.out.println(Math.round(a));
		System.out.println(Round(a, 3));
		System.out.println(Chunk(a));
	}
	public static double Round(double num,int precision){
		return Math.round(num*Math.pow(10, precision))/Math.pow(10,precision);
	}
	public static double Chunk(double num){
		DecimalFormat df = new DecimalFormat("#############.0");
		String str = df.format(num);
		return Double.parseDouble(str);
	}
	//各类婚配夫妇分年龄生育率
	public static void coupleBornRateByAge(Map < HunpeiField , double [ ] > zn,TempVariablesBean tempVar){
		for(int X=Const.MAX_AGE-1;X>=0;X--){
			if(zn.get(HunpeiField.HJF)[X]!=0){
				zn.get(HunpeiField.SYL_SUM)[X] = (zn.get(HunpeiField.DDB)[X] + zn.get(HunpeiField.NMDFB)[X] + zn.get(HunpeiField.NFDMB)[X] + zn.get(HunpeiField.NNB)[X] + 
						zn.get(HunpeiField.SingleRls)[X] + zn.get(HunpeiField.ShuangFeiRls)[X])/zn.get(HunpeiField.HJF)[X];
			}
			if(zn.get(HunpeiField.DD)[X]!=0){
				zn.get(HunpeiField.SYL_DD)[X] = zn.get(HunpeiField.DDB)[X]/zn.get(HunpeiField.DD)[X];
			}
			if( (zn.get(HunpeiField.NN)[X]+zn.get(HunpeiField.ShuangfeiDJ)[X] + zn.get(HunpeiField.Yi2Shuangfei)[X]) !=0){
				zn.get(HunpeiField.SYL_NN)[X] = (zn.get(HunpeiField.NNB)[X]+zn.get(HunpeiField.ShuangFeiRls)[X])
						/(zn.get(HunpeiField.NN)[X]+zn.get(HunpeiField.ShuangfeiDJ)[X] + zn.get(HunpeiField.Yi2Shuangfei)[X]);
			}
			if((zn.get(HunpeiField.NMDF)[X] + zn.get(HunpeiField.NFDM)[X] + zn.get(HunpeiField.SingleDJ)[X] + zn.get(HunpeiField.Yi2Single)[X])!=0){
				zn.get(HunpeiField.SYL_D)[X] = (zn.get(HunpeiField.NMDFB)[X] + zn.get(HunpeiField.NFDMB)[X] + zn.get(HunpeiField.SingleRls)[X])
						/(zn.get(HunpeiField.NMDF)[X] + zn.get(HunpeiField.NFDM)[X] + zn.get(HunpeiField.SingleDJ)[X] + zn.get(HunpeiField.Yi2Single)[X]);
			}
		}
		
		//SUM 合J分龄SYL,双D分龄SYL,双N分龄SYL,单D分龄SYL TO 合JTFR,双DTFR,双NTFR,单DTFR
		//这句貌似没用啊,合JTFR,双DTFR,双NTFR,单DTFR好像是局部变量
	}
}
