package prclqz.lib;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import prclqz.core.enumLib.Abstract;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.Couple;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.HunpeiGL;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.QYPHField;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.SumBM;
import prclqz.core.enumLib.Summary;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;

public class EnumMapTool {
	
	public static Map<String,Map<SYSFMSField,double[]>> createBirthWill(int length){
		Map<String,Map<SYSFMSField,double[]>> m = new HashMap<String, Map<SYSFMSField,double[]>>();
		m.put("五省", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("东部", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("广东", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("浙江", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("中部", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("安徽", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("河南", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		m.put("四川", new EnumMap<SYSFMSField, double[]>(SYSFMSField.class));
		for(String k : m.keySet()){
			Map<SYSFMSField, double[]> mm = m.get(k);
			for(SYSFMSField sfms : SYSFMSField.values()){
				mm.put(sfms, new double[length]);
			}
		}
		
		return m;
	}
	
	public static Map<CX,Map<NY,Map<HunpeiField,double[]>>> creatCXNYZiNv(int length){
		
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> m = new EnumMap<CX, Map<NY,Map<HunpeiField,double[]>>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<NY, Map<HunpeiField,double[]>>(NY.class));
			Map<NY, Map<HunpeiField,double[]>> mm = m.get(cx);
			for(NY ny:NY.values()){
				mm.put(ny, new EnumMap<HunpeiField, double[]>(HunpeiField.class));
				Map<HunpeiField, double[]> mmm = mm.get(ny);
				for(HunpeiField zf : HunpeiField.values())
					mmm.put(zf, new double[length]);
			}
		}
		return m;
	}
	
	public static Map<CX,Map<NY,Map<HunpeiField,double[]>>> creatCXNYZiNvFromFile(int length) throws Exception{
		
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> m = new EnumMap<CX, Map<NY,Map<HunpeiField,double[]>>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<NY, Map<HunpeiField,double[]>>(NY.class));
			Map<NY, Map<HunpeiField,double[]>> mm = m.get(cx);
			for(NY ny:NY.values()){
				mm.put(ny, new EnumMap<HunpeiField, double[]>(HunpeiField.class));
				Map<HunpeiField, double[]> mmm = mm.get(ny);
				for(HunpeiField zf : HunpeiField.values())
					mmm.put(zf, new double[length]);
			}
		}
		
		for(HunpeiField hpf : HunpeiField.valuesOfZinv()){
			String chn_name = hpf.getChinese( hpf );
			
		}
		return m;
	}

	
	public static Map<NY,Map<HunpeiField,double[]>> creatNYFNZiNv(int length){
		
		Map<NY, Map<HunpeiField,double[]>> mm = new EnumMap<NY, Map<HunpeiField,double[]>>(NY.class);
		for(NY ny:NY.values()){
			mm.put(ny, new EnumMap<HunpeiField, double[]>(HunpeiField.class));
			Map<HunpeiField, double[]> mmm = mm.get(ny);
			for(HunpeiField zf : HunpeiField.values())
				mmm.put(zf, new double[length]);
		}
		return mm;
	}
	
	public static Map<CX,Map<HunpeiField,double[]>> creatCXZiNv(int length){
		
		Map<CX, Map<HunpeiField,double[]>> mm = new EnumMap<CX, Map<HunpeiField,double[]>>(CX.class);
		for(CX cx:CX.values()){
			mm.put(cx, new EnumMap<HunpeiField, double[]>(HunpeiField.class));
			Map<HunpeiField, double[]> mmm = mm.get(cx);
			for(HunpeiField zf : HunpeiField.values())
				mmm.put(zf, new double[length]);
		}
		return mm;
	}

	public static Map<HunpeiField,double[]> creatAllZiNv(int length){
		
		Map<HunpeiField, double[]> mmm = new EnumMap<HunpeiField, double[]>(HunpeiField.class);
		for(HunpeiField zf : HunpeiField.values())
			mmm.put(zf, new double[length]);
		return mmm;
	}
	
	public static Map<CX, Map<XB, double[][]>> createCXXBDoubleArrMap(int maxYear,int length){
		
		Map<CX, Map<XB, double[][]>> m = new EnumMap<CX, Map<XB,double[][]>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<XB,double[][]>(XB.class));
			Map<XB, double[][]> mm = m.get(cx);
			for(XB xb:XB.values()){
				double[][] arr = new double[maxYear][length];
				mm.put(xb, arr);
			}
		}
		return m;
	}

	public static Map<CX, Map<XB, double[]>> createCXXBDoubleArrMap(int length){
		
		Map<CX, Map<XB, double[]>> m = new EnumMap<CX, Map<XB,double[]>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<XB,double[]>(XB.class));
			Map<XB, double[]> mm = m.get(cx);
			for(XB xb:XB.values()){
				double[] arr = new double[length];
				mm.put(xb, arr);
			}
		}
		return m;
	}
	
	public static Map<CX, Map<Year,Map<XB, double[]>>> createCXYearXBDoubleArrMap(int length){
		
		Map<CX, Map<Year,Map<XB, double[]>>> m = new EnumMap<CX, Map<Year,Map<XB,double[]>>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<Year, Map<XB,double[]>>(Year.class));
			 Map<Year, Map<XB, double[]>> mm = m.get(cx);
			for(Year y: Year.getAllYears()){
				mm.put(y, new EnumMap<XB, double[]>(XB.class));
				Map<XB, double[]> mmm = mm.get(y);
				for(XB xb : XB.values()){
					mmm.put(xb, new double[length]);
				}
			}
		}
		return m;
	}
	public static Map<NY, Map<Year,Map<XB, double[]>>> createNYYearXBDoubleArrMap(int length){
		
		Map<NY, Map<Year,Map<XB, double[]>>> m = new EnumMap<NY, Map<Year,Map<XB,double[]>>>(NY.class);
		
		for(NY cx : NY.values()){
			m.put(cx, new EnumMap<Year, Map<XB,double[]>>(Year.class));
			Map<Year, Map<XB, double[]>> mm = m.get(cx);
			for(Year y: Year.getAllYears()){
				mm.put(y, new EnumMap<XB, double[]>(XB.class));
				Map<XB, double[]> mmm = mm.get(y);
				for(XB xb : XB.values()){
					mmm.put(xb, new double[length]);
				}
			}
		}
		return m;
	}
	
	public static Map<CX, Map<NY,Map<Year,Map<XB, double[]>>>> createCXNYYearXBDoubleArrMap(int length){
		
		Map<CX, Map<NY,Map<Year,Map<XB, double[]>>>> m = new EnumMap<CX, Map<NY,Map<Year,Map<XB,double[]>>>>(CX.class);
		
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<NY, Map<Year,Map<XB,double[]>>>(NY.class));
			Map<NY, Map<Year, Map<XB, double[]>>> m2 = m.get(cx);
			for(NY ny : NY.values()){
				m2.put(ny, new EnumMap<Year, Map<XB,double[]>>(Year.class));
				Map<Year, Map<XB, double[]>> mm = m2.get(ny);
				for(Year y: Year.getAllYears()){
					mm.put(y, new EnumMap<XB, double[]>(XB.class));
					Map<XB, double[]> mmm = mm.get(y);
					for(XB xb : XB.values()){
						mmm.put(xb, new double[length]);
					}
				}
			}
		}
		return m;
	}

	
	public static Map<XB, double[]> createXBDoubleArrMap(int length){
		
		Map<XB, double[]> mm = new EnumMap<XB, double[]>(XB.class);
		for(XB xb:XB.values()){
			double[] arr = new double[length];
			mm.put(xb, arr);
		}
		return mm;
	}
	
	public static Map<Year,Map<XB, double[]>> createYearXBDoubleArrMap(int length){
		
		Map<Year,Map<XB, double[]>> mm = new EnumMap<Year, Map<XB,double[]>>(Year.class);
		for(Year y: Year.getAllYears()){
			mm.put(y, new EnumMap<XB, double[]>(XB.class));
			Map<XB, double[]> mmm = mm.get(y);
			for(XB xb : XB.values()){
				mmm.put(xb, new double[length]);
			}
		}
		return mm;
	}

	public static Map<Year,Map<BabiesBorn,Double>> createBabiesBorn(){
		
		Map<Year,Map<BabiesBorn,Double>> m = new EnumMap<Year, Map<BabiesBorn,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<BabiesBorn, Double>(BabiesBorn.class));
			Map<BabiesBorn, Double> mm = m.get(y);
			for(BabiesBorn bb : BabiesBorn.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	
	public static Map<Year,Map<SumBM,Double>> createSumBM(){
		
		Map<Year,Map<SumBM,Double>> m = new EnumMap<Year, Map<SumBM,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<SumBM, Double>(SumBM.class));
			Map<SumBM, Double> mm = m.get(y);
			for(SumBM bb : SumBM.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	
	public static Map<Year,Map<Couple,Double>> createCouple(){
		
		Map<Year,Map<Couple,Double>> m = new EnumMap<Year, Map<Couple,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<Couple, Double>(Couple.class));
			Map<Couple, Double> mm = m.get(y);
			for(Couple bb : Couple.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	
	public static Map<Year,Map<Summary,Double>> createSummary(){
		
		Map<Year,Map<Summary,Double>> m = new EnumMap<Year, Map<Summary,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<Summary, Double>(Summary.class));
			Map<Summary, Double> mm = m.get(y);
			for(Summary bb : Summary.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	public static Map<Year,Map<PolicyBirth,Double>> createPolicyBirth(){
		
		Map<Year,Map<PolicyBirth,Double>> m = new EnumMap<Year, Map<PolicyBirth,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<PolicyBirth, Double>(PolicyBirth.class));
			Map<PolicyBirth, Double> mm = m.get(y);
			for(PolicyBirth bb : PolicyBirth.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	public static Map<Year,Map<HunpeiGL,Double>> createHunpeiGL(){
		
		Map<Year,Map<HunpeiGL,Double>> m = new EnumMap<Year, Map<HunpeiGL,Double>>(Year.class);
		for(Year y :Year.getAllYears()){
			m.put(y, new EnumMap<HunpeiGL, Double>(HunpeiGL.class));
			Map<HunpeiGL, Double> mm = m.get(y);
			for(HunpeiGL bb : HunpeiGL.values()){
				mm.put(bb, new Double(0));
			}
		}
		return m;
	}
	
	public static Map<CX,Map<Year,Map<Summary,Double>>> createCXSummary(){
		Map<CX,Map<Year,Map<Summary,Double>>> mmm = new EnumMap < CX , Map<Year,Map<Summary,Double>> > ( CX.class );
		for(CX c:CX.values ( )){
			Map<Year,Map<Summary,Double>> m = new EnumMap<Year, Map<Summary,Double>>(Year.class);
			mmm.put ( c , m );
			for(Year y :Year.getAllYears()){
				m.put(y, new EnumMap<Summary, Double>(Summary.class));
				Map<Summary, Double> mm = m.get(y);
				for(Summary bb : Summary.values()){
					mm.put(bb, new Double(0));
				}
			}
		}
		return mmm;
	}
	
	public static Map<NY,Map<Year,Map<Summary,Double>>> createNYSummary(){
		Map<NY,Map<Year,Map<Summary,Double>>> mmm = new EnumMap < NY , Map<Year,Map<Summary,Double>> > ( NY.class );
		for(NY c:NY.values ( )){
			Map<Year,Map<Summary,Double>> m = new EnumMap<Year, Map<Summary,Double>>(Year.class);
			mmm.put ( c , m );
			for(Year y :Year.getAllYears()){
				m.put(y, new EnumMap<Summary, Double>(Summary.class));
				Map<Summary, Double> mm = m.get(y);
				for(Summary bb : Summary.values()){
					mm.put(bb, new Double(0));
				}
			}
		}
		return mmm;
	}
	
	public static Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>> createCXNYCouple(){
		Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>> mmm = new EnumMap < CX , Map<NY,Map<Year,Map<Couple,Double>>> > ( CX.class );
		for(CX cx :CX.values ( )){
			Map<NY,Map<Year,Map<Couple,Double>>> mm = new EnumMap < NY , Map<Year,Map<Couple,Double>> > ( NY.class );
			mmm.put ( cx , mm );
			for(NY ny:NY.values ( )){
				Map<Year,Map<Couple,Double>> m = new EnumMap<Year, Map<Couple,Double>>(Year.class);
				mm.put ( ny , m );
				for(Year y :Year.getAllYears()){
					m.put(y, new EnumMap<Couple, Double>(Couple.class));
					Map<Couple, Double> mm1 = m.get(y);
					for(Couple bb : Couple.values()){
						mm1.put(bb, new Double(0));
					}
				}
			}
		}
		return mmm;
	}
	
	public static Map<NY,Map<Year,Map<Couple,Double>>> createCXCouple(){
			Map<NY,Map<Year,Map<Couple,Double>>> mm = new EnumMap < NY , Map<Year,Map<Couple,Double>> > ( NY.class );
			for(NY ny:NY.values ( )){
				Map<Year,Map<Couple,Double>> m = new EnumMap<Year, Map<Couple,Double>>(Year.class);
				mm.put ( ny , m );
				for(Year y :Year.getAllYears()){
					m.put(y, new EnumMap<Couple, Double>(Couple.class));
					Map<Couple, Double> mm1 = m.get(y);
					for(Couple bb : Couple.values()){
						mm1.put(bb, new Double(0));
					}
				}
			}
		return mm;
	}
	
	public static Map<CX,Map<Year,Map<Couple,Double>>> createNYCouple(){
		Map<CX,Map<Year,Map<Couple,Double>>> mm = new EnumMap < CX , Map<Year,Map<Couple,Double>> > ( CX.class );
		for(CX ny:CX.values ( )){
			Map<Year,Map<Couple,Double>> m = new EnumMap<Year, Map<Couple,Double>>(Year.class);
			mm.put ( ny , m );
			for(Year y :Year.getAllYears()){
				m.put(y, new EnumMap<Couple, Double>(Couple.class));
				Map<Couple, Double> mm1 = m.get(y);
				for(Couple bb : Couple.values()){
					mm1.put(bb, new Double(0));
				}
			}
		}
		return mm;
	}



	public static Map<Babies, double[]> createBabiesDoubleArrMap(int length){
		
		Map<Babies, double[]> m = new EnumMap<Babies,double[]>(Babies.class);
		for(Babies b: Babies.values()){
			m.put(b, new double[length]);
		}
		return m;
	}
	
	
	public static Map<Year, double[]> createYearDoubleArrMap(int length){
		
		Map<Year, double[]> m = new EnumMap<Year,double[]>(Year.class);
		for(Year b: Year.values()){
			m.put(b, new double[length]);
		}
		return m;
	}
	
	public static Map<CX,Map<Babies, double[]>> createCXBabiesDoubleArrMap(int length){
		Map<CX,Map<Babies, double[]>> mm = new EnumMap<CX,Map<Babies, double[]>>(CX.class);
		for(CX cx:CX.values ( )){
			Map<Babies, double[]> m = new EnumMap<Babies,double[]>(Babies.class);
			mm.put ( cx , m );
			for(Babies b: Babies.values()){
				m.put(b, new double[length]);
			}
		}
		return mm;
	}
	public static Map<NY,Map<Babies, double[]>> createNYBabiesDoubleArrMap(int length){
		Map<NY,Map<Babies, double[]>> mm = new EnumMap<NY,Map<Babies, double[]>>(NY.class);
		for(NY ny:NY.values ( )){
			Map<Babies, double[]> m = new EnumMap<Babies,double[]>(Babies.class);
			mm.put ( ny , m );
			for(Babies b: Babies.values()){
				m.put(b, new double[length]);
			}
		}
		return mm;
	}

	public static Map<XB, double[][]> createXBDoubleArrMap(int maxYear,int length){
		
		Map<XB, double[][]> mm = new EnumMap<XB, double[][]>(XB.class);
		for(XB xb:XB.values()){
			double[][] arr = new double[maxYear][length];
			mm.put(xb, arr);
		}
		return mm;
	}

	public static Map<CX,Map<NY,Map<XB, double[]>>> createCXNYXBDoubleArrMap(int length){
		Map<CX,Map<NY,Map<XB,double[]>>> m0 = new EnumMap<CX, Map<NY,Map<XB,double[]>>>(CX.class);
		Map<NY, Map<XB, double[]>> m;
		for(CX cx : CX.values())
		{
			m0.put(cx, new EnumMap<NY, Map<XB,double[]>>(NY.class));
			m = m0.get(cx);
			for(NY ny : NY.values())
			{
				m.put(ny, new EnumMap<XB,double[]>(XB.class));
				Map<XB, double[]> mm = m.get(cx);
				for(XB xb:XB.values()){
					double[] arr = new double[length];
					mm.put(xb, arr);
				}
			}
		}
		return m0;
	}
//	public static <T>Map<CX, Map<XB, T>> createCXXBMap() {
//		EnumMap<CX, Map<XB, T>> m = new EnumMap<CX,Map<XB,T>>(CX.class);
//		for(CX cx : CX.values()){
//			m.put(cx, new EnumMap<XB,T>(XB.class));
//			Map<XB, T> mm = m.get(cx);
//			for(XB xb: XB.values()) {
//				Integer d = new Integer(0);
//				T obj = (T) d;
//				mm.put(xb, obj);
//			}
//		}
//		return m;
//	}
	public static Map<CX, Map<XB, Double>> createCXXBMapDouble() {
		EnumMap<CX, Map<XB, Double>> m = new EnumMap<CX,Map<XB,Double>>(CX.class);
		for(CX cx : CX.values()){
			m.put(cx, new EnumMap<XB,Double>(XB.class));
			Map<XB, Double> mm = m.get(cx);
			for(XB xb: XB.values()) {
				mm.put(xb, new Double(0));
			}
		}
		return m;
	}
	
	public static Map<NY, Map<XB, double[]>> createNYXBMapDoubleArr(int length) {
		EnumMap<NY, Map<XB, double[]>> m = new EnumMap<NY,Map<XB,double[]>>(NY.class);
		for(NY ny : NY.values()){
			m.put(ny, new EnumMap<XB,double[]>(XB.class));
			Map<XB, double[]> mm = m.get(ny);
			for(XB xb: XB.values()) {
				double[] arr = new double[length];
				mm.put(xb, arr);
			}
		}
		return m;
	}

	public static <T>Map<CX, T> createCXMap() {
		return new EnumMap<CX,T>(CX.class);
	}
	
	public static Map<QYPHField,long[]> createQYPHArrMap(int length){
		EnumMap<QYPHField, long[]> m = new EnumMap<QYPHField, long[]>(QYPHField.class);
		for(QYPHField qf : QYPHField.values()){
			m.put(qf, new long[length]);
		}
		return m;
	}
	
	public static Map<CX,Map<SYSFMSField,double[]>> createSYSFMSMap(int length){
		Map<CX,Map<SYSFMSField,double[]>> m = new EnumMap<CX,Map<SYSFMSField, double[]>>(CX.class);
		for(CX cx:CX.values()){
			m.put(cx, new EnumMap<SYSFMSField,double[]>(SYSFMSField.class));
			Map<SYSFMSField, double[]> mm = m.get(cx);
			for(SYSFMSField sd:SYSFMSField.values()){
				mm.put(sd, new double[length]);
			}
		}
		return m;
	}
	public static <T> void copyXBMap(Map<XB,T> from,Map<XB,T> to){
		for(XB xb: XB.values())
		{
			to.put(xb, from.get(xb));
		}
	}
	
	public static <T> void copyCXXBMap(Map<CX, Map<XB, T>> from, Map<CX, Map<XB, T>> to){
		for(CX cx: CX.values()){
			copyXBMap(from.get(cx),to.get(cx));
		}
	}
	public static Map<CX, Map<NY, Double>> createSingleChild() {
		Map<CX, Map<NY, Double>> m = new EnumMap<CX, Map<NY,Double>>(CX.class);
		for(CX cx : CX.values()){
			 m.put(cx, new EnumMap<NY, Double>(NY.class));
			 Map<NY, Double> mm = m.get(cx);
			for(NY ny : NY.values()){
				mm.put(ny, new Double(0));
			}
		}
		
		return m;
	}
	
	public static Map<CX, Double> createCXSingleChild() {
		Map<CX, Double> m = new EnumMap<CX,Double>(CX.class);
		for(CX cx : CX.values()){
			m.put(cx, new Double(0));
		}
		
		return m;
	}
	public static Map<NY, Double> createNYSingleChild() {
		Map<NY, Double> m = new EnumMap<NY,Double>(NY.class);
		for(NY cx : NY.values()){
			m.put(cx, new Double(0));
		}
		
		return m;
	}
	
	public static Map<Abstract, Double> createAbstract() {
		Map<Abstract, Double> m = new EnumMap<Abstract,Double>(Abstract.class);
		for(Abstract cx : Abstract.values()){
			m.put(cx, new Double(0));
		}
		
		return m;
	}
}
