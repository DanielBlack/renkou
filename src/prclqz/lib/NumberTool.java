package prclqz.lib;

public class NumberTool {
	
	
	public static void main(String[] args) throws Exception {
		int i1 = -3;
		System.out.println(FmtLong(i1,10));
	}
	
	/**
	 * 用空格来格式化数字，前面不足补空格
	 * @param num
	 * @param length
	 * @return
	 */
	public static String FmtLong(long num, int length){
	    int curLength = (""+num).length();
	    if(curLength >= length)
	    	return (""+num);
	    else
	    {
	    	String tmp="";
	    	for(int i=0;i<length- curLength;i++)
	    		tmp=tmp.concat(" ");
	    	return tmp+num;
	    }
	}
}
