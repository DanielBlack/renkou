package prclqz.lib;

public class ArrayTools{

	public static void main(String[] args) throws Exception {
		System.out.println("hah");
		
		//为什么不能调用！！！！！！！！！！！！
//		ArrayTools.<double>arrayCopy2DimT(arr, arr2, 2, 2);
	}

	// 要让static 方法有 泛型一定要这么定义！
	/***
	 * 复制二维数组，支持各种基本数据类型——不支持对象
	 */
	public static <T>  void arrayCopy2DimT(T[][] fromArr , T[][] toArr, int width, int height) {
		int i,j;
		for(i=0;i<width;i++)
			for(j=0;j<height;j++){
				toArr[i][j] = fromArr[i][j];
			}
	}
	

	/**
	 * 输出二维数组
	 */
	public static void outputArray2DimD(double[][] arr,int width, int height){
		int i,j;
		for(i=0;i<1;i++){
			for(j=0;j<3;j++){
				System.out.println(arr[i][j]);
			}
			System.out.println();
		}
	}
	
	/***
	 * 复制二维数组，只支持double——不支持对象
	 */
	public static  void arrayCopy2DimD(double[][] jingQianProbability , double[][] newJingQianArr, int width, int height) {
		int i,j;
		for(i=0;i<width;i++)
			for(j=0;j<height;j++){
				newJingQianArr[i][j] = jingQianProbability[i][j];
			}
	}
	
	public static void arrayCopy1DimD(double[] from, double[] to, int width){
		int i;
		for(i=0;i<width;i++)
			to[i]=from[i];
	}
}
