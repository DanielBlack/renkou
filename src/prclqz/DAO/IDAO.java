package prclqz.DAO;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.Bean.*;
import prclqz.core.StringList;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.DuiJiGuSuanFa;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.QYPHField;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.Year;
import prclqz.parambeans.ParamBean3;
import prclqz.view.IView;

public interface IDAO {
	
	//TODO 给参数直接赋值的，小心溢出范围！比如给double[]赋值，需要确定超过了长度则退出！并且，如果超过长度还有结果集则给出Warning.
	public void addHistory(MainOperationHistoryBean m) throws Exception;
	public MainOperationHistoryBean getHistory(int id) throws Exception;
	public MainTaskBean getTask(String task_name)throws Exception;
	public HashMap<String,BornXbbBean> getARBornXbbBeans(MainTaskBean task)throws Exception;
	public void saveTask(MainTaskBean m) throws Exception;
	public MethodBean[] getMethods(int taskId)throws Exception;
	public void close() throws Exception;
	/**
	 * 1.设置临时变量bean里面的值
	 * 2.更新对应的临时变量表中的变量，使其一致
	 * 3.在MyDAOImpl中使用
	 * @param tb
	 * @param name
	 * @param value
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	public boolean setTempVariable(TempVariablesBean tb, String name, int value,
			int taskId) throws Exception;
	
	public TempVariablesBean getTempVariables(int taskId) throws SQLException;
	public HashMap<String,Integer> getMainMeanMap() throws SQLException;
	public boolean deleteAllResultOfHunPei(MainTaskBean task)throws SQLException;
	public void insertHunPeiResult(MainTaskBean task,HashMap<String, BornXbbBean> xbbMap)throws SQLException;
	void insertTempVariables(int taskId)throws SQLException;
	void createProvinceMigrationView(int taskId,int batch_id) throws SQLException;
	void deleteProvinceMigrationView(int taskId) throws SQLException;
	public void setZongQianRate(Map<CX, Map<XB, Double>> zongQianRate, String dq, int year, int method, int batchId)
			throws SQLException;
	public void setDiQuQianYiXiuZheng(Map<CX,Double> CXQB, int dqx, int batchId) throws SQLException;
	public double[] getQianYiXiuZheng(int dqx, int batchId,int param) throws SQLException;
	public void setJingQianGLFB(Map<CX,Map<XB,double[]>> arr, int province, int year, int paramId) throws SQLException;
	public void setZiNv(Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv, int dqx, int taskId)throws SQLException, MyDAOException;
	public void insertQuanGuoQianYiPingHeng(int taskid)throws SQLException;
//	public void saveZiNvQY(int[][][] ziNvQY,int dqx , int zc, int sfms)throws Exception;
	public void changeToMemoryEngine(String table)throws Exception;
	public void changeToMyISAMEngine(String table)throws Exception;
	public void insertNationQY(Map<QYPHField, long[]> QYPH, int id)throws Exception;
	public void getNationQY(Map<QYPHField,long[]> qYPH, int id)throws SQLException, MyDAOException;
	public void setShengYuMoShi(
			Map<CX, Map<SYSFMSField, double[]>> shengYuMoShi, int dqx,
			int paramID, DuiJiGuSuanFa tj)throws SQLException, MyDAOException;
	/**
	 * 1.获取死亡概率
	 * 2.在 PreparePredictionData 中使用
	 * @param deathRate
	 * @param dqx
	 * @param paramId
	 * @param fieldName
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void setDeathRate(Map<CX, Map<XB, double[]>> deathRate, int dqx,
			int paramId, String fieldName)throws SQLException, MyDAOException;
	/**
	 * 1.获取现行政策的终身政策生育率
	 * 2.在 PreparePredictionData 中使用
	 * @param presentBirthPolicy
	 * @param paramId
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void setPresentBirthPolicy(
			HashMap<String, Double> presentBirthPolicy, int paramId)throws SQLException, MyDAOException;
	/**
	 * 1.获取地区人口预测现状数据
	 * 2.在preparePredictionData中使用
	 * @param populationPredictOfCX
	 * @param dqx
	 * @param dataId
	 */
	public void setPredictOfCX(
			Map<CX,Map<Year,Map<XB,double[]>>> populationPredictOfCX, int dqx,
			int dataId)throws SQLException, MyDAOException;
	/**
	 * 1.获取妻子按年龄分布的丈夫分布概率
	 * 2.在preparePredictionData中使用
	 * @param husbandRate 要保存丈夫分布概率的二维数组，格式：husbandRate[husdAge][wifeAge]
	 * @param paramId	     数据库参数
	 */
	public void setHusbandRate(double[][] husbandRate, int paramId)throws SQLException, MyDAOException;
	/**
	 * 获取生育意愿
	 * 在preparePredictionData中使用
	 * @param birthWill
	 * @param taskId 当前任务id
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void setBirthWill(Map<String,Map<SYSFMSField,double[]>> birthWill,int batchId)throws SQLException, MyDAOException;
	/**
	 *  仿真结果_地区生育率与迁移摘要	*SummaryOfBirthAndMigration+dqx(包括0)
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveSumBM ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_分婚配模式生育孩次数	*"BabiesBorn"+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveBabiesBorn ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_分年龄人口预测	(预测)				*"PopulationPredictOf[All,NY,CXNY]"+dqx <br/>
	// 仿真结果_分年龄人口预测	(迁移)				*"PopulationMigrationOf[CX,All]"+dqx	
	// 仿真结果_分年龄人口预测	(死亡)				*"PopulationDeathOf[CX,All]"+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void savePredict ( HashMap< String , Object > predictVarMap, StringList strValues)throws SQLException, MyDAOException;
	/**
	 *  仿真结果_分年龄丧子人口预测(特扶)			"SonDiePopulationPredictOfTeFu"+dqx			
	// 仿真结果_分年龄丧子人口预测(一孩)			"SonDiePopulationPredictOfYiHai"+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveSonDie ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_分年龄生育预测(预测)				*BirthPredictOf[CX,NY,All]+dqx <br/>
	// 仿真结果_分年龄生育预测(超生)				*OverBirthPredictOf[CX,NY,All]+dqx <br/>
	// 仿真结果_分年龄生育预测(政策)				*"PolicyBabies"+dqx <br/>
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveBabies ( HashMap< String , Object > predictVarMap, StringList strValues)throws Exception;
	/**
	 *  仿真结果_夫妇及子女表结构					*CoupleAndChildrenOf[All,CX,NY,CXNY]+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveCouple ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_妇女分年龄生育率预测				*BirthRatePredict+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveBirthRatePredict ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_概要		*SummaryOf[All,CX,NY]+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveSummary ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 *  仿真结果_政策及政策生育率					*PolicyBirthRate+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void savePolicyBirth ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 * 仿真结果_婚配概率							*MarriageRate+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveMirriageRate ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;

	/**
	 * 仿真结果_婚配预测表结构！MProvinceMigration中完成		！provinceMigMap！* ""+dqx ,在！predictVarMap！HunpeiOfCXNY+dqx
	// 仿真结果_婚配预测表结构！MAgingCalculation中完成		！provinceMigMap！* "CX"+dqx,在！predictVarMap！HunpeiOfCX+dqx
	// 仿真结果_婚配预测表结构！MAgingCalculation中完成		！provinceMigMap！* "NY"+dqx,在！predictVarMap！HunpeiOfNY+dqx
	// 仿真结果_婚配预测表结构！在MAgingCalculation中完成		！provinceMigMap！* "All"+dqx,在！predictVarMap！HunpeiOfAll+dqx
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveHunpei ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 * 仿真结果_全国迁移平衡表结构		！tempVar.getQY() > 0 才有*(在provinceMigMap和predictVarMap中都有)，QYPH
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveQYPH ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 * 仿真结果_生育政策模拟摘要					*PolicySimulationAbstract
	 * @param predictVarMap
	 * @param strValues 
	 * @throws SQLException
	 * @throws MyDAOException
	 */
	public void saveAbstract ( HashMap< String , Object > predictVarMap, StringList strValues )throws SQLException, MyDAOException;
	/**
	 * 设置通用插入字段内容
	 * @param commonInsert
	 */
	void setCommonInsert ( String commonInsert );
	/**
	 * 设置任务ID
	 * @param taskID
	 */
	void setTaskID ( int taskID );
	/**
	 * 设置省份对应表
	 * @param xbbMap
	 */
	public void setXbbMap ( HashMap< String , BornXbbBean > xbbMap );
	/**
	 * 设置I/O类
	 * @param v
	 */
	public void setView ( IView v );
	/**
	 * 设置环境Bean
	 * @param envParm
	 */
	public void setEnvParam ( ParamBean3 envParm );
}
