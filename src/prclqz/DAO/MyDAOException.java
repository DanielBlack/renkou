package prclqz.DAO;

public class MyDAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String msg;
	int type; 
	/*
	 * 0: select 溢出
	 * 1: select 不足
	 */
	int level;//0:Warning 1:medium 2:important
	
	public MyDAOException(String msg, int type, int level) {
		super();
		this.msg = msg;
		this.type = type;
		this.level = level;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public MyDAOException(String msg, int level) {
		super();
		this.msg = msg;
		this.level = level;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public MyDAOException(String string) {
		msg = string;
	}
	public String getMsg() {
		return msg;
	}
	
	
	
}
