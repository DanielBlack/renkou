﻿package prclqz.DAO;

import static prclqz.core.Const.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.XB.*;
import static prclqz.core.enumLib.QYPHField.*;
import static prclqz.core.enumLib.SYSFMSField.*;

import prclqz.DAO.Bean.*;
import prclqz.Database.*;
import prclqz.core.Const;
import prclqz.core.StringList;
import prclqz.core.enumLib.Abstract;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.Couple;
import prclqz.core.enumLib.DuiJiGuSuanFa;
import prclqz.core.enumLib.HunpeiGL;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.QYPHField;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.Year;
import prclqz.parambeans.ParamBean3;
import prclqz.view.IView;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class MyDAOImpl implements IDAO
{
	// TODO
	// 从数据库给数组参数直接赋值的，小心溢出范围！比如给double[]赋值，需要确定超过了长度则退出！并且，如果超过长度还有结果集则给出Warning.
	// TODO 对传进来的数组要保存进数据库的需要判断长度！
	private Statement stmt;
	private MySQLDB db;
	private ResultSet rs;
	private String commonInsert;
	private StringBuilder sqlBuilder;
	private int maxSQLLength;
	private int taskID;
	private HashMap< String , BornXbbBean > xbbMap;
	private IView v;
	private ParamBean3 envparam;
	// private static double[] qianyiLv= new double[2];
	@Override
	public void setEnvParam ( ParamBean3 envParm ){
		this.envparam = envParm;
	}
	@Override
	public void setView ( IView v )
	{
		this.v = v;
	}
	@Override
	public void setXbbMap ( HashMap< String , BornXbbBean > xbbMap )
	{
		this.xbbMap = xbbMap;
	}

	@Override
	public void setTaskID ( int taskID )
	{
		this.taskID = taskID;
	}

	private void cleanSQLBuilder ()
	{
		sqlBuilder.delete( 0 , maxSQLLength );
	}

	@Override
	public void setCommonInsert ( String commonInsert )
	{
		this.commonInsert = commonInsert;
	}

	public MyDAOImpl ()
	{
		db = MySQLDB.instance();
		stmt = db.openStmt();
		// 256个字段*32个字符每个字段*128条记录每次插入*32个省份循环* 16 CXNY循环 536,870,912字节 ,即 546Mb
		// maxSQLLength = 256*32*128*32*16;// TODO 内存不够？！
		maxSQLLength = 256 * 32 * 128 * 32;
		sqlBuilder = new StringBuilder( maxSQLLength );
	}

	@Override
	public void addHistory ( MainOperationHistoryBean m ) throws Exception
	{
		stmt.execute( "INSERT INTO "
				+ "`新人口学`.`主_操作历史` (`id`, `type`, `value`, `time`) VALUES "
				+ "(NULL, " + m.getType() + ", '" + m.getValue() + "', '"
				+ m.getTime() + "');" );
	}

	@Override
	public MainOperationHistoryBean getHistory ( int id ) throws Exception
	{
		rs = stmt.executeQuery( "SELECT * FROM  `主_操作历史` where id =" + id + "" );
		if ( rs.next() )
		{
			MainOperationHistoryBean m = new MainOperationHistoryBean( rs
					.getInt( "id" ) , rs.getInt( "type" ) ,
					rs.getInt( "time" ) , rs.getString( "value" ) );
			rs.close();
			return m;
		} else
		{
			rs.close();
			return null;
		}
	}

	@Override
	public void setDiQuQianYiXiuZheng ( Map< CX , Double > CXQB , int dqx ,
			int batch_id ) throws SQLException
	{
		String sql = "SELECT C迁移修正B,X迁移修正B FROM `基本参数_地区迁移修正系数` where 省代码 = '"
				+ dqx + "' and 批次id='" + batch_id + "'";
		rs = stmt.executeQuery( sql );
		if ( rs.next() )
		{
			CXQB.put( Chengshi , rs.getDouble( 1 ) );
			CXQB.put( Nongchun , rs.getDouble( 2 ) );
		}
	}

	@Override
	public void setZongQianRate ( Map< CX , Map< XB , Double >> zqR ,
			String dq , int year , int method , int batch_id )
			throws SQLException
	{
		String sql = "SELECT " + dq + "城镇M," + dq + "农村M," + dq + "城镇F," + dq
				+ "农村F " + "FROM `基本参数_分省总和迁移率预测` " + "where 统计方法 = '" + method
				+ "' and " + " 批次id = '" + batch_id + "' and " + " 年份 = '"
				+ year + "'";
		rs = stmt.executeQuery( sql );
		if ( rs.next() )
		{
			zqR.get( Chengshi ).put( Male , rs.getDouble( 1 ) );
			zqR.get( Nongchun ).put( Male , rs.getDouble( 2 ) );
			zqR.get( Chengshi ).put( Female , rs.getDouble( 3 ) );
			zqR.get( Nongchun ).put( Female , rs.getDouble( 4 ) );
		}
	}

	@Override
	public MainTaskBean getTask ( String taskName ) throws Exception
	{
		rs = stmt.executeQuery( "select * FROM  `主_计算任务表` where `task_name`='"
				+ taskName + "'" );

		if ( rs.next() )
		{
			MainTaskBean m = new MainTaskBean( rs.getInt( "id" ) , rs
					.getString( "task_name" ) , rs.getInt( "status" ) , rs
					.getInt( "begin_order_id" ) , rs.getInt( "end_order_id" ) ,
					rs.getLong( "create_time" ) , rs.getLong( "begin_time" ) ,
					rs.getLong( "end_time" ) , 0 , 0 , rs
							.getFloat( "process_rate" ) );
			int id1 , id2;
			id1 = rs.getInt( "基本参数批次id" );
			id2 = rs.getInt( "基础数据批次id" );
			m.setParam_id( id1 );
			m.setData_id( id2 );
			// rs =
			// stmt.executeQuery("SELECT time FROM  `主_操作历史` where id ="+id1+"");
			// rs.next();
			// long time1 = rs.getLong("time");
			// rs =
			// stmt.executeQuery("SELECT time FROM  `主_操作历史` where id ="+id2+"");
			// rs.next();
			// long time2 = rs.getLong("time");
			// m.setParam_time(time1);
			// m.setData_time(time2);

			return m;
		}
		return null;
	}

	@Override
	public void saveTask ( MainTaskBean m ) throws Exception
	{
		String sql = "UPDATE  `新人口学`.`主_计算任务表` SET  status = " + m.getStatus()
				+ ",`begin_time` =  " + m.getBegin_time() + ",`end_time` = '"
				+ m.getEnd_time() + "',process_rate = " + m.getProcess_rate()
				+ " WHERE  `主_计算任务表`.`id` =" + m.getId();

		stmt.execute( sql );
	}

	@Override
	public void close () throws Exception
	{
		if ( rs != null )
			rs.close();
		db.close();
	}

	@Override
	public MethodBean[] getMethods ( int taskId ) throws Exception
	{
		rs = stmt.executeQuery( "SELECT *FROM  `主_计算方法设置表` where task_id="
				+ taskId + " order by order_id" );
		rs.last();
		int len = rs.getRow();
		rs.beforeFirst();
		MethodBean[] methods = new MethodBean[ len ];
		int i = 0;
		while ( rs.next() )
		{
			methods[ i++ ] = new MethodBean( rs.getInt( "id" ) , rs
					.getInt( "task_id" ) , rs.getInt( "order_id" ) , rs
					.getInt( "param_type" ) , rs.getString( "method" ) , rs
					.getString( "notes" ) , rs.getString( "parameters" ) );
			// System.out.println(methods[i-1]);
		}

		return methods;
	}

	@Override
	public void insertTempVariables ( int task_id ) throws SQLException
	{
		stmt.execute( "delete from `新人口学`.`主_临时变量表` where task_id=" + task_id );

		stmt
				.execute( "INSERT INTO `新人口学`.`主_临时变量表` (`id`, `name`, `value`, `task_id`, `notes`) VALUES "
						+ " (NULL, 'FeiNongPolicy'	, '0', '"
						+ task_id
						+ "', NULL) "
						+ ",(NULL, 'FeiNongTime1'		, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'FeiNongPolicy2'	, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'FeiNongTime2'		, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'FeiNongPolicy3'	, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'FeiNongTime3'		, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'QY'				, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'ZCimplement'		, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'SFMS'				, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'Year'				, '0', '"
						+ task_id
						+ "', NULL)"
						+ ",(NULL, 'Province'			, '0', '"
						+ task_id
						+ "', NULL);" );
	}

	@Override
	public boolean setTempVariable ( TempVariablesBean tb , String name ,
			int value , int task_id ) throws Exception
	{
		Class< ? > cls = Class.forName( "prclqz.DAO.Bean.TempVariablesBean" );
		// ms = cls.getMethods();

		try
		{
			Method md = cls
					.getMethod( "set" + name , new Class[] { int.class } );
			md.invoke( tb , new Object[] { value } );

			stmt.execute( "UPDATE  `新人口学`.`主_临时变量表` SET value  = " + value
					+ " WHERE  `主_临时变量表`.`name` ='" + name + "' and task_id = "
					+ task_id );

			return true;
		} catch ( SecurityException e )
		{
			e.printStackTrace();
			return false;
		} catch ( NoSuchMethodException e )
		{
			e.printStackTrace();
			return false;
		} catch ( IllegalArgumentException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch ( IllegalAccessException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch ( InvocationTargetException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public TempVariablesBean getTempVariables ( int task_id )
			throws SQLException
	{
		rs = stmt.executeQuery( "SELECT * FROM  `主_临时变量表` where task_id ="
				+ task_id + "" );
		int[] is = new int[ 20 ];
		int i = 0;
		while ( rs.next() )
		{
			is[ i++ ] = rs.getInt( "value" );
		}

		TempVariablesBean tb = new TempVariablesBean( is[ 0 ] , is[ 1 ] ,
				is[ 2 ] , is[ 3 ] , is[ 4 ] , is[ 5 ] , is[ 6 ] , is[ 7 ] ,
				is[ 8 ] , is[ 9 ] , is[ 10 ] , task_id );
		return tb;
	}

	@Override
	public HashMap< String , BornXbbBean > getARBornXbbBeans ( MainTaskBean task )
			throws Exception
	{

		HashMap< String , BornXbbBean > bbs = new HashMap< String , BornXbbBean >();
		int MAX = 31;
		int count;
		String sql = "SELECT"
				+ "`基本参数_分省净迁移率`.`省代码`,"
				+ "`基本参数_分省净迁移率`.`地区`,"
				+ "`基本参数_各地区代码和出生性别比`.`出生XBB05`,"
				+ "`基本参数_各地区代码和出生性别比`.`出生XBB90`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现政比09`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现政比年增`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现实09`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`政策09`,"
				+ "`基础数据_各地区人口城乡构成`.CZRKB,"
				+ "`基本参数_分省净迁移率`.`迁移率`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现实MI`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现政比09`,"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`现政比年增`,"
				+ "`其他数据_分省代码`.`三地带`,"
				+ "`其他数据_分省代码`.`土地面积`,"
				+ "`其他数据_分省代码`.`耕地06`,"
				+ "`其他数据_分省代码`.`水资源均量`"
				+ "FROM"
				+ "`基本参数_分省净迁移率`"
				+ "Inner Join `基本参数_各地区代码和出生性别比` ON `基本参数_分省净迁移率`.`省代码` = `基本参数_各地区代码和出生性别比`.`省代码`"
				+ "Inner Join `基本参数_现实生育率与政策生育率比较06_09` ON `基本参数_各地区代码和出生性别比`.`省代码` = `基本参数_现实生育率与政策生育率比较06_09`.`省代码`"
				+ "Inner Join `基础数据_各地区人口城乡构成` ON `基本参数_现实生育率与政策生育率比较06_09`.`地区` = `基础数据_各地区人口城乡构成`.`地区`"
				+ "Inner Join `其他数据_分省代码` ON `基础数据_各地区人口城乡构成`.`地区` = `其他数据_分省代码`.`地区` AND '' = ''"
				+ "WHERE" + "`其他数据_分省代码`.`批次id` =  '" + task.getParam_id()
				+ "' AND" + "`基本参数_分省净迁移率`.`批次id` =  '" + task.getParam_id()
				+ "' AND" + "`基本参数_各地区代码和出生性别比`.`批次id` =  '"
				+ task.getParam_id() + "' AND"
				+ "`基本参数_现实生育率与政策生育率比较06_09`.`批次id` =  '" + task.getParam_id()
				+ "' AND" + "`基础数据_各地区人口城乡构成`.`批次id` =  '" + task.getData_id()
				+ "'";

		rs = stmt.executeQuery( sql );

		int i;
		count = 0;
		while ( rs.next() )
		{
			count++;
			i = rs.getInt( 1 );
			bbs.put( "" + i ,
					new BornXbbBean( rs.getInt( 1 ) , rs.getString( 2 ) , rs
							.getDouble( 3 ) , rs.getDouble( 4 ) , rs
							.getDouble( 5 ) , rs.getDouble( 6 ) , rs
							.getDouble( 7 ) , rs.getDouble( 8 ) , rs
							.getDouble( 9 ) , rs.getDouble( 10 ) , rs
							.getDouble( 11 ) , rs.getDouble( 12 ) , rs
							.getDouble( 13 ) , rs.getString( 14 ) , rs
							.getDouble( 15 ) , rs.getDouble( 16 ) , rs
							.getDouble( 17 ) ) );
			// bbs.add(b);
		}

		// 不足报警
		if ( count < MAX )
			throw new MyDAOException( sql , 1 , 1 );

		// 溢出报警
		if ( rs.next() )
			throw new MyDAOException( sql , 0 , 0 );

		return bbs;
	}

	@Override
	public HashMap< String , Integer > getMainMeanMap () throws SQLException
	{
		rs = stmt.executeQuery( "SELECT * FROM `主_参数对应描述`" );
		HashMap< String , Integer > mmm = new HashMap< String , Integer >();

		while ( rs.next() )
		{
			mmm.put( rs.getString( "代表参数" ) + "-" + rs.getString( "描述表类" ) , rs
					.getInt( "id" ) );
		}

		return mmm;
	}

	// 建立分省迁移视图——已废弃，不用视图了
	// 原因有三：1，建立视图太大太麻烦，调试也不方便 2，大视图效率很低 3.根本性的，建立视图的话用update将破坏原有数据。。。
	@Override
	public void createProvinceMigrationView ( int task_id , int batch_id )
			throws SQLException
	{
		/*
		 * 主视图改为创建MySQL临时视图——调用 子程序 CREATE VIEW `NewView`AS select * FROM
		 * `仿真结果_婚配预测表结构` Inner Join `基本参数_迁移模式和概率` ON `仿真结果_婚配预测表结构`.X =
		 * `基本参数_迁移模式和概率`.X Inner Join `仿真结果_全国迁移平衡表结构` ON `基本参数_迁移模式和概率`.X =
		 * `仿真结果_全国迁移平衡表结构`.X AND `仿真结果_婚配预测表结构`.`对应任务id` =
		 * `仿真结果_全国迁移平衡表结构`.`对应任务id` WHERE `基本参数_迁移模式和概率`.`批次id` = '1' and
		 * `仿真结果_婚配预测表结构`.`对应任务id` = '1'
		 */
		// TODO 为什么不能用 if not exists？？？
		String sql = "CREATE VIEW `ProvinceMigrationView"
				+ task_id
				+ "` AS "
				+ "select 仿真结果_婚配预测表结构.* FROM `仿真结果_婚配预测表结构` "
				+ "Inner Join `基本参数_迁移模式和概率` ON `仿真结果_婚配预测表结构`.X = `基本参数_迁移模式和概率`.X "
				+ "Inner Join `仿真结果_全国迁移平衡表结构` ON `基本参数_迁移模式和概率`.X = `仿真结果_全国迁移平衡表结构`.X "
				+ "AND `仿真结果_婚配预测表结构`.`对应任务id` = `仿真结果_全国迁移平衡表结构`.`对应任务id` "
				+ "WHERE " + "`基本参数_迁移模式和概率`.`批次id` =  '" + batch_id + "' and "
				+ "`仿真结果_婚配预测表结构`.`对应任务id` = '" + task_id + "'";

		stmt.execute( sql );
	}

	@Override
	public void deleteProvinceMigrationView ( int task_id ) throws SQLException
	{
		stmt.execute( "drop view `ProvinceMigrationView" + task_id + "`" );
	}

	public static void main ( String[] args ) throws Exception
	{
		MyDAOImpl m = new MyDAOImpl();
		int i , j , k;
		// m.getMethods(1);
		// TempVariablesBean tb = m.getTempVariables(1);
		// System.out.println(tb.getQY());
		// m.setTempVariable(tb, "QY", 2, 1);
		// System.out.println(tb.getQY());
		// m.close();

		// int[][][] tar = new int[4][6][111];
		// try{
		//			
		// m.getZiNv(tar, 65, 1);
		// }catch(MyDAOException e){
		// System.out.print(e.getMessage());
		// }
		//
		// //m.getZiNv(tar, 65, 1);
		// for(k=0;k<4;k++){
		// for(i=0;i<6;i++){
		// for(j=0;j<111;j++)
		// {
		// System.out.print(tar[k][i][j]);
		// }
		// System.out.println();
		// }
		//		
		// System.out.println();
		// System.out.println();
		// }
		// System.exit(0);
		//		
		//		
		// double [] qyxz;
		// qyxz = m.getQianYiXiuZheng(11, 1, 167);
		// System.out.println(""+qyxz[0]+qyxz[1]+qyxz[2]);
		//		
		// double[][] t = new double[4][91];
		// m.getJingQianGLFB(t, 66, 2000, 1);
		//		
		// m.getDiQuQianYiXiuZheng(11, 1);
		// double[] arr = new double[4];
		// m.setZongQianRate(arr, "北京", 2007,78, 1);
		//		
		// //m.createProvinceMigrationView(1, 1);
		// m.deleteProvinceMigrationView(1);
		// HashMap<String, Integer> mmm = m.getMainMeanMap();
		// int ii=mmm.get("农业-子女统计参数");
		// System.out.println(ii);
		// m.insertHunPeiResult(1);
		// m.deleteAllResultOfBefore(null);
	}

	@Override
	public boolean deleteAllResultOfHunPei ( MainTaskBean task )
			throws SQLException
	{
		boolean result;
		result = stmt
				.execute( "delete from `新人口学`.`仿真结果_婚配预测表结构` where 对应任务id = "
						+ task.getId() );
		return ( !result );

		// stmt.execute("delete from `新人口学`.`仿真结果_婚配预测表结构` where 对应任务id = "+task.getId());
		// stmt.execute("delete from `新人口学`.`仿真结果_婚配预测表结构` where 对应任务id = "+task.getId());

	}

	@Override
	public void insertHunPeiResult ( MainTaskBean task ,
			HashMap< String , BornXbbBean > xbbMap ) throws SQLException
	{
		int level1 , level2 , level3 , level4 , level5;
		String sql;
		int dqx , param1;

		// 意愿模式和释放模式
		level4 = 144;
		level5 = 150;

		int param2 = 160; //

		for ( dqx = 11 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			level1 = dqx;
			for ( param1 = 163 ; param1 <= 166 ; param1++ )
			{

				level2 = level3 = 0;
				switch ( param1 )
				{
				case 163 :
					level2 = 123;
					level3 = 132;
					break;
				case 164 :
					level2 = 123;
					level3 = 130;
					break;
				case 165 :
					level2 = 120;
					level3 = 132;
					break;
				case 166 :
					level2 = 120;
					level3 = 130;
					break;
				}

				sql = "insert into `仿真结果_婚配预测表结构` "
						+ " (X,HJ,HJM,HJF,D,DM,DF,N,NM,NF,level1,level2,level3,level4,level5,`对应任务id`) "
						+ " select X,HJ,HJM,HJF,D,DM,DF,N,NM,NF, " + " "
						+ level1
						+ " AS level1,"
						+ level2
						+ " as level2, "
						+ " "
						+ level3
						+ " as level3,"
						+ level4
						+ " as level4,"
						+ " "
						+ level5
						+ " as level5,"
						+ task.getId()
						+ " as 对应任务id "
						+ " from 新人口学.`其他数据_现状估计中的婚配预测表`"
						+ " where "
						+ " (`其他数据_现状估计中的婚配预测表`.`省份`="
						+ dqx
						+ " and "
						+ " `其他数据_现状估计中的婚配预测表`.`对应任务id`= "
						+ task.getParam_id()
						+ " and "
						+ " `其他数据_现状估计中的婚配预测表`.`参数1`= "
						+ param1
						+ " and "
						+ " `其他数据_现状估计中的婚配预测表`.`参数2`=" + param2 + " );";
				// all = all.concat(tmp);
				// System.out.println(tmp);
				// boolean rrr;
				stmt.execute( sql );
				// rs = stmt.executeQuery("SELECT  ROW_COUNT();");
				// int ttt;
				// rs.next();
				// ttt = rs.getInt(1);
				// System.out.println(""+rrr+":"+ttt);
				//
				// 江西省和新疆省的数据不全，都只有100岁，故补上
				if ( dqx == 65 || dqx == 36 )
				{
					sql = "insert into `仿真结果_婚配预测表结构` "
							+ " (X,HJ,HJM,HJF,D,DM,DF,N,NM,NF,level1,level2,level3,level4,level5,`对应任务id`) values "
							+ " (101,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (102,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (103,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (104,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (105,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (106,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (107,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (108,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (109,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5
							+ ","
							+ task.getId()
							+ " ), "
							+ " (110,0,0,0,0,0,0,0,0,0,"
							+ level1
							+ ","
							+ level2
							+ ","
							+ level3
							+ ","
							+ level4
							+ ","
							+ level5 + "," + task.getId() + " ); ";

					stmt.execute( sql );
				}
			}
		}

		// System.out.print(all);
		// System.exit(0);

		// stmt.execute(all);
		/*
		 * level4 = 144; level5 = 150; for(level1=1;level1<=31;level1++)
		 * for(level2=123;level2<=155;level2+=(155-123))
		 * for(level3=130;level3<=132;level3+=(132-130)) { s =
		 * this.genSQL1(level1, level2, level3, level4, level5, task_id);
		 * stmt.execute(s); }
		 */

	}

	@Override
	public void setJingQianGLFB ( Map< CX , Map< XB , double[] >> arr ,
			int province , int year , int paramId ) throws SQLException
	{
		String sql = "SELECT `X`,`城乡`,`净迁GLFBM`,`净迁GLFBF` FROM `基本参数_迁移模式和概率` "
				+ " where `省份` = " + province + " and `年份`=" + year
				+ " and `批次id`= " + paramId + "";
		rs = stmt.executeQuery( sql );
		while ( rs.next() )
		{
			// if(rs.getInt(2)==72){ //Chengshi
			// arr[0][rs.getInt(1)] = rs.getDouble(3);
			// arr[2][rs.getInt(1)] = rs.getDouble(4);
			// }else{
			// arr[1][rs.getInt(1)] = rs.getDouble(3);
			// arr[3][rs.getInt(1)] = rs.getDouble(4);
			// }
			// if(rs.getInt(2)==Chengshi.getId()){

			CX cx = getCXFromId( rs.getInt( 2 ) );
			arr.get( cx ).get( Male )[ rs.getInt( 1 ) ] = rs.getDouble( 3 );
			arr.get( cx ).get( Female )[ rs.getInt( 1 ) ] = rs.getDouble( 4 );

		}

	}

	@Override
	public double[] getQianYiXiuZheng ( int dqx , int batchId , int param )
			throws SQLException
	{
		String sql = "SELECT C迁08B,X迁08B,平衡系数  FROM `基本参数_迁移修正系数` where 省代码= "
				+ dqx + " and 参数= " + param + " and 批次id = " + batchId + "";
		rs = stmt.executeQuery( sql );
		if ( rs.next() )
		{
			double[] qyxz = new double[ 3 ];
			qyxz[ 0 ] = rs.getDouble( 1 );
			qyxz[ 1 ] = rs.getDouble( 2 );
			qyxz[ 2 ] = rs.getDouble( 3 );
			return qyxz;
		} else
			return null;
	}

	@Override
	public void setZiNv (
			Map< CX , Map< NY , Map< HunpeiField , double[] >>> ziNv , int dqx ,
			int taskId ) throws SQLException , MyDAOException
	{
		int MAX = Const.MAX_AGE * 4;
		// if(dqx == 65 || dqx == 36 )
		// MAX = 101*4;
		String sql = "SELECT X,HJM,HJF,DM,DF,DDBM,DDBF,NM,NF,参数1 FROM `其他数据_现状估计中的婚配预测表` where 省份="
				+ dqx + " and 对应任务id=" + taskId + "";

		rs = stmt.executeQuery( sql );
		int i = 0;
		CX level2;
		NY level3;

		while ( rs.next() )
		{
			i++;
			level2 = getCXFromId( rs.getInt( "参数1" ) );
			level3 = getNYFromId( rs.getInt( "参数1" ) );

			for ( HunpeiField zf : HunpeiField.valuesOfZinv() )
			{
				ziNv.get( level2 ).get( level3 ).get( zf )[ rs.getInt( 1 ) ] = rs
						.getInt( zf.toString() );
			}
			// 防止溢出
			if ( i == MAX )
				break;
		}
		// 不足报警
		if ( i < MAX )
			throw new MyDAOException( sql , 1 , 1 );

		// 溢出报警
		if ( rs.next() )
			throw new MyDAOException( sql , 0 , 0 );
	}

	@Override
	public void insertQuanGuoQianYiPingHeng ( int taskid ) throws SQLException
	{
		// 删除
		stmt.execute( "delete from `仿真结果_全国迁移平衡表结构` where `对应任务id`=" + taskid );
		// 插入
		String sql = "insert into `仿真结果_全国迁移平衡表结构`(" + "X," + "level1,"
				+ "level2," + "level3," + "level4," + "level5," + "`扩展1`,"
				+ "`展扩2`," + "`对应任务id`)" + " select X,0,0,0,0,0,0,0," + taskid
				+ " from `仿真结果_全国迁移平衡表结构` where `对应任务id`=0";
		stmt.execute( sql );
	}

	/***
	 * //[0:城镇非农 1:城镇农业 2:农村非农 3：农村农业] [迁移子女- 0:DQYM 1:DQYF 2:DDBQYM 3:DDBQYF
	 * 4:NQYM 5:NQYF [X-年龄] // [0:M 1:F] [0:迁入 1:迁出] [X] // [0:M 1:F] [X]
	 * 
	 * @throws Exception
	 */
	// TODO 暂时不敢用了，这个怎么比FoxPro还慢!如何优化？

	public void saveZiNvQY ( int[][][] ziNvQY , int dqx , int zc , int sfms )
			throws Exception
	{

		String sql = "UPDATE `仿真结果_婚配预测表结构` SET "
				+ "`DQYM`=?,`DQYF`=?, `DDBQYM`=?, `DDBQYF`=?, `NQYM`=?, `NQYF`=? "
				+ "WHERE ( `X`=? and `level1`=? and `level2`=? and `level3`=? and `level4`="
				+ zc + " and `level5`=" + sfms + " )";
		PreparedStatement preStmtOfZiNv = db.openPreStmt( sql );

		// long start = System.currentTimeMillis();

		int cx , ny;
		for ( int i = 0 ; i < 111 ; i++ )
		{
			for ( int cxny = 0 ; cxny < 4 ; cxny++ )
			{
				cx = 120 + ( cxny / 2 ) * 3;
				ny = 130 + ( cxny % 2 ) * 2;
				preStmtOfZiNv.setInt( 1 , ziNvQY[ cxny ][ 0 ][ i ] );
				preStmtOfZiNv.setInt( 2 , ziNvQY[ cxny ][ 1 ][ i ] );
				preStmtOfZiNv.setInt( 3 , ziNvQY[ cxny ][ 2 ][ i ] );
				preStmtOfZiNv.setInt( 4 , ziNvQY[ cxny ][ 3 ][ i ] );
				preStmtOfZiNv.setInt( 5 , ziNvQY[ cxny ][ 4 ][ i ] );
				preStmtOfZiNv.setInt( 6 , ziNvQY[ cxny ][ 5 ][ i ] );

				preStmtOfZiNv.setInt( 7 , i );
				preStmtOfZiNv.setInt( 8 , dqx );
				preStmtOfZiNv.setInt( 9 , cx );
				preStmtOfZiNv.setInt( 10 , ny );

				// preStmt.addBatch();
				preStmtOfZiNv.executeUpdate();
			}
		}
		// System.out.println("保存到婚配到数据库费时:"+dqx+"*"+ +
		// (System.currentTimeMillis() - start));

		// preStmt.executeBatch();
	}

	@Override
	public void changeToMemoryEngine ( String table ) throws Exception
	{
		// 修改表引擎为内存表来加速
		String engine_change = "ALTER TABLE `" + table + "` ENGINE=MEMORY;";
		stmt.execute( engine_change );
	}

	@Override
	public void changeToMyISAMEngine ( String table ) throws Exception
	{
		// 改回MyISAM引擎
		String engine_change = "ALTER TABLE `" + table + "` ENGINE=MyISAM;";
		stmt.execute( engine_change );

	}

	/**
	 * // [0:M 1:F] [0:迁入 1:迁出] [X] nationQY // [0:M 1:F] [X] qyPingHeng
	 */
	@Override
	public void insertNationQY ( Map< QYPHField , long[] > QYPH , int id )
			throws Exception
	{
		// long start = System.currentTimeMillis();

		// 删除旧数据 TODO
//		stmt.executeUpdate( "delete from `仿真结果_全国迁移平衡表结构` where `对应任务id`="+ id );

		String sql = "insert into `仿真结果_全国迁移平衡表结构`("
				+ "X,`迁入M`,`迁出M`,`迁入F`,`迁出F`,`迁移平衡M`,`迁移平衡F`,`对应任务id`)values";
		StringBuilder sbr = new StringBuilder( sql );
		int X;
		// 大字符串用StringBuilder
		for ( X = 0 ; X < MAX_AGE ; X++ )
		{
			sbr.append( "(" ).append( X ).append( "," ).append(
					QYPH.get( MImmigrate )[ X ] ).append( "," ).append(
					QYPH.get( MEmigrate )[ X ] ).append( "," ).append(
					QYPH.get( FImmigrate )[ X ] ).append( "," ).append(
					QYPH.get( FEmigrate )[ X ] ).append( "," ).append(
					QYPH.get( MBalance )[ X ] ).append( "," ).append(
					QYPH.get( FBalance )[ X ] ).append( "," ).append( id )
					.append( ")" );
			if ( X == MAX_AGE - 1 )
				sbr.append( ";" );
			else
				sbr.append( "," );

		}

		sql = sbr.toString();
		stmt.executeUpdate( sbr.toString() );
		// System.out.println("保存全国迁移到数据库费时:"+ (System.currentTimeMillis() -
		// start));
	}

	/**
	 * [0:M 1:F] [0:迁入 1:迁出] [X] 迁移平衡[0:M 1:F] [X]
	 * 
	 * @throws MyDAOException
	 */
	@Override
	public void getNationQY ( Map< QYPHField , long[] > qYPH , int id )
			throws SQLException , MyDAOException
	{
		int Max = 111;

		String sql = "SELECT X,迁入M,迁出M,迁入F,迁出F,迁移平衡M,迁移平衡F  FROM `仿真结果_全国迁移平衡表结构` where 对应任务id="
				+ id;
		rs = stmt.executeQuery( sql );
		int X , i = 0;
		while ( rs.next() )
		{
			i++;
			X = rs.getInt( 1 );

			qYPH.get( MImmigrate )[ X ] = rs.getInt( 2 );
			qYPH.get( MEmigrate )[ X ] = rs.getInt( 3 );
			qYPH.get( FImmigrate )[ X ] = rs.getInt( 4 );
			qYPH.get( FEmigrate )[ X ] = rs.getInt( 5 );

			qYPH.get( MBalance )[ X ] = rs.getInt( 6 );
			qYPH.get( FBalance )[ X ] = rs.getInt( 7 );

			if ( i == MAX_AGE )
				break;
		}

		// 不足报警
		if ( i < Max )
			throw new MyDAOException( sql , 1 , 1 );

		// 溢出报警
		if ( rs.next() )
			throw new MyDAOException( sql , 0 , 0 );

	}

	@Override
	public void setShengYuMoShi (
			Map< CX , Map< SYSFMSField , double[] >> shengYuMoShi , int dqx ,
			int paramId , DuiJiGuSuanFa tj ) throws SQLException ,
			MyDAOException
	{
		int MIN = 10 , count = 0;
		int methodId = DuiJiGuSuanFa.getParamId( tj );

		String sql = "SELECT X,待生二孩比,缓释模式,突释模式,正常模式,城乡 FROM `基本参数_生育及释放模式` where 省份="
				+ dqx + " and 方法=" + methodId + " and 批次id=" + paramId;
		rs = stmt.executeQuery( sql );
		while ( rs.next() )
		{
			count++;
			shengYuMoShi.get( getCXFromId( rs.getInt( "城乡" ) ) ).get(
					wait2chdrn )[ rs.getInt( "X" ) ] = rs.getDouble( "待生二孩比" );
			shengYuMoShi.get( getCXFromId( rs.getInt( "城乡" ) ) ).get( huanshi )[ rs
					.getInt( "X" ) ] = rs.getDouble( "缓释模式" );
			shengYuMoShi.get( getCXFromId( rs.getInt( "城乡" ) ) ).get( tushi )[ rs
					.getInt( "X" ) ] = rs.getDouble( "突释模式" );
			shengYuMoShi.get( getCXFromId( rs.getInt( "城乡" ) ) ).get(
					zhengchang )[ rs.getInt( "X" ) ] = rs.getDouble( "正常模式" );
		}

		// 不足报警
		if ( count < MIN )
			throw new MyDAOException( sql , 1 , 1 );
	}

	@Override
	public void setDeathRate ( Map< CX , Map< XB , double[] >> deathRate ,
			int dqx , int paramId , String fieldName ) throws SQLException ,
			MyDAOException
	{
		int MIN = MAX_AGE * 4 , count = 0;
		String sql = "SELECT X," + fieldName
				+ ",城乡,性别 FROM `基本参数_死亡概率预测` where 省份=" + dqx + " and 批次id="
				+ paramId;
		rs = stmt.executeQuery( sql );
		while ( rs.next() )
		{
			count++;
			deathRate.get( getCXFromId( rs.getInt( "城乡" ) ) ).get(
					getXBFromId( rs.getInt( "性别" ) ) )[ rs.getInt( "X" ) ] = rs
					.getDouble( fieldName );
		}
		// 不足报警
		if ( count < MIN )
			throw new MyDAOException( sql , 1 , 1 );

		// 溢出报警
		if ( rs.next() )
			throw new MyDAOException( sql , 0 , 0 );
	}

	/**
	 * excellent !!!!!!!!!!!!!!!!!! 剩下的基本参数基础数据一定都要用这招！！！
	 */
	@Override
	public void setPresentBirthPolicy (
			HashMap< String , Double > presentBirthPolicy , int paramId )
			throws SQLException , MyDAOException
	{
		int MIN = 31 , count = 0;
		// 要全部获取出来，首先要知道字段名和个数——可以应付政策表结构变的情况！我是天才啊……
		rs = stmt.executeQuery( "desc `基本参数_各地区终身政策生育率`" );
		rs.last();
		int len = rs.getRow();
		rs.beforeFirst();
		int i = 0;
		String[] fieldArr = new String[ len ];
		while ( rs.next() )
		{
			fieldArr[ i++ ] = rs.getString( 1 );
		}

		String sql = "SELECT * FROM `基本参数_各地区终身政策生育率` where 批次id=" + paramId;
		rs = stmt.executeQuery( sql );

		while ( rs.next() )
		{
			count++;
			for ( i = 0 ; i < len ; i++ )
			{
				try
				{
					presentBirthPolicy.put( rs.getInt( "省代码" ) + fieldArr[ i ] ,
							rs.getDouble( i + 1 ) );
				} catch ( Exception e )
				{
					// 不成功仅仅只有可能是因为字段类型不是字符串……
					presentBirthPolicy.put( fieldArr[ i ] , 0.0 );
				}
			}
		}

		// System.out.println(presentBirthPolicy.toString());

		// 不足报警
		if ( count < MIN )
			throw new MyDAOException( sql , 1 , 1 );

		// 溢出报警
		if ( rs.next() )
			throw new MyDAOException( sql , 0 , 0 );
	}

	@Override
	public void setPredictOfCX (
			Map< CX , Map< Year , Map< XB , double[] >>> populationPredictOfCX ,
			int dqx , int dataId ) throws SQLException , MyDAOException
	{
		int MIN = MAX_AGE * 2 , count = 0;
		// 91-城镇 ；94-农村
		String sql = "SELECT X,分龄参数,M2005,F2005,M2006,F2006,M2007,F2007,M2008,F2008,M2009,F2009  FROM `基础数据_分年龄人口` "
				+ "where (分龄参数=91 or 分龄参数=94 )and 省份="
				+ dqx
				+ " and 批次id = "
				+ dataId;
		rs = stmt.executeQuery( sql );
		CX cx;
		int X , i;
		Year y;
		Map< CX , Map< Year , Map< XB , double[] >>> px = populationPredictOfCX;
		while ( rs.next() )
		{
			count++;
			if ( rs.getInt( 2 ) == 91 )
				cx = Chengshi;
			else
				cx = Nongchun;
			X = rs.getInt( 1 );
			for ( i = 2005 ; i <= 2009 ; i++ )
			{
				y = Year.getYear( i );
				px.get( cx ).get( y ).get( Male )[ X ] = rs.getInt( "M" + i );
				px.get( cx ).get( y ).get( Female )[ X ] = rs.getInt( "F" + i );
			}

		}

		this.alert( MIN , count , sql , rs );

	}

	private void alert ( int MIN , int count , String sql , ResultSet rs )
			throws MyDAOException , SQLException
	{
		// 不足报警
		if ( count < MIN )
		{
			System.out.println( sql + " " + count );
			throw new MyDAOException( sql , 1 , 1 );
		}
		// 溢出报警
		if ( rs.next() )
		{
			System.out.println( sql + " " + count );
			throw new MyDAOException( sql , 0 , 0 );
		}
	}

	@Override
	public void setHusbandRate ( double[][] husbandRate , int paramId )
			throws SQLException , MyDAOException
	{
		int MIN = 96 , count = 0;
		String sql = "SELECT * FROM `基本参数_全国按妻子年龄分的丈夫年龄分布模式`";
		rs = stmt.executeQuery( sql );

		int husdAge , wifeAge;

		while ( rs.next() )
		{
			count++;
			husdAge = rs.getInt( "丈夫年龄" );
			for ( wifeAge = 15 ; wifeAge < MAX_AGE ; wifeAge++ )
			{
				husbandRate[ husdAge ][ wifeAge ] = rs.getDouble( "妻子"
						+ wifeAge );
			}
		}

		alert( MIN , count , sql , rs );
	}

	@Override
	public void setBirthWill (
			Map< String , Map< SYSFMSField , double[] >> birthWill , int batchId )
			throws SQLException , MyDAOException
	{
		int MIN = ( 67 - 17 + 1 ) * 4 , count = 0;
		Map< SYSFMSField , double[] > ws = birthWill.get( "五省" );
		Map< SYSFMSField , double[] > db = birthWill.get( "东部" );
		Map< SYSFMSField , double[] > gd = birthWill.get( "广东" );
		Map< SYSFMSField , double[] > zj = birthWill.get( "浙江" );
		Map< SYSFMSField , double[] > zb = birthWill.get( "中部" );
		Map< SYSFMSField , double[] > ah = birthWill.get( "安徽" );
		Map< SYSFMSField , double[] > hn = birthWill.get( "河南" );
		Map< SYSFMSField , double[] > sc = birthWill.get( "四川" );

		String sql = "SELECT * FROM `基本参数_生育意愿` where 批次id = " + batchId;

		rs = stmt.executeQuery( sql );

		int X;

		while ( rs.next() )
		{
			count++;
			X = rs.getInt( "X" );
			SYSFMSField sfms = SYSFMSField.getSFMSfromId( rs.getInt( "bt参数" ) );
			ws.get( sfms )[ X ] = rs.getDouble( "五省" );
			db.get( sfms )[ X ] = rs.getDouble( "东部" );
			gd.get( sfms )[ X ] = rs.getDouble( "广东" );
			zj.get( sfms )[ X ] = rs.getDouble( "浙江" );
			zb.get( sfms )[ X ] = rs.getDouble( "中部" );
			ah.get( sfms )[ X ] = rs.getDouble( "安徽" );
			hn.get( sfms )[ X ] = rs.getDouble( "河南" );
			sc.get( sfms )[ X ] = rs.getDouble( "四川" );
		}

		alert( MIN , count , sql , rs );
	}

	@Override
	public void saveAbstract ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		// 生育政策模拟摘要
		Map< Abstract , Double > PolicySimulationAbstract = (Map< Abstract , Double >) predictVarMap
				.get( "PolicySimulationAbstract" );

		// 创建字段类型判断表——判断是否是 varchar类型
		Map< Abstract , Boolean > AbstractTypeMap = new EnumMap< Abstract , Boolean >(
				Abstract.class );
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_生育政策模拟摘要`" );
		rs.next();
		for ( Abstract ab : Abstract.values() )
		{
			rs.next();
			// System.out.println("&"+rs.getString( "Type" ));
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			AbstractTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_生育政策模拟摘要` "
						+ "(`id`, `堆释模式`, `概率估算法`, `政策1`, `时机1`, `政策2`, `时机2`, `政策3`, `时机3`, `迁移方案`, `生育方案`, "
						+ "`地区别`, `城乡别`, `最大规模ND`, `最大规模`, `最小规模ND`, `最小规模`, `最大迁移ND`, `最大迁移`, `最小迁移ND`, "
						+ "`最小迁移`, `最高TFRND`, `最高TFR`, `总人口20`, `出生20`, `出生率20`, `生育率20`, `净迁入率20`, `总增长率20`, "
						+ "`城市化20`, `对应任务id`, `释放模式0`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`) "
						+ "VALUES " );

		sqlBuilder.append( "( NULL," );
		for ( Abstract ab : Abstract.values() )
		{
			String tmp = ( PolicySimulationAbstract.get( ab ) > 0 && AbstractTypeMap
					.get( ab ) ) ? strValues.get( PolicySimulationAbstract
					.get( ab ) ) : PolicySimulationAbstract.get( ab )
					.toString();
			sqlBuilder.append( "'" + tmp + "'," );
		}

		sqlBuilder.append( taskID + "," + "'暂无','0','0','生育政策模拟摘要',"
				+ commonInsert + ")" );

		// System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );

	}

	@Override
	public void saveBabies ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws Exception
	{
		// 分年龄生育预测(生育，)
		Map< Babies , double[] > BirthPredict;
		// Map<CX,Map<Babies,double[]>> BirthPredictOfCX = ( Map < CX , Map <
		// Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfCX"+dqx
		// );
		// Map<NY,Map<Babies,double[]>> BirthPredictOfNY = ( Map < NY , Map <
		// Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfNY"+dqx
		// );
		// Map<Babies,double[]> BirthPredictOfAll = ( Map < Babies , double [ ]
		// > ) predictVarMap.get ( "BirthPredictOfAll"+dqx );
		// 全国
		// Map<CX,Map<Babies,double[]>> NationBirthPredictOfCX = ( Map < CX ,
		// Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfCX"
		// );
		// Map<NY,Map<Babies,double[]>> NationBirthPredictOfNY = ( Map < NY ,
		// Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfNY"
		// );
		// Map<Babies,double[]> NationBirthPredictOfAll = ( Map < Babies ,
		// double [ ] > ) predictVarMap.get ( "BirthPredictOfAll" );

		// 创建字段类型判断表——判断是否是 varchar类型
		Map< Babies , Boolean > BabiesTypeMap = new EnumMap< Babies , Boolean >(
				Babies.class );
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_分年龄生育预测`" );
		rs.next();
		for ( Babies ab : Babies.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			BabiesTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_分年龄生育预测` (`id`, `X`, `B2005`, `B2006`, `B2007`, `B2008`, `B2009`, `B2010`, "
						+ "`B2011`, `B2012`, `B2013`, `B2014`, `B2015`, `B2016`, `B2017`, `B2018`, `B2019`, `B2020`, `B2021`, `B2022`, `B2023`, `B2024`, "
						+ "`B2025`, `B2026`, `B2027`, `B2028`, `B2029`, `B2030`, `B2031`, `B2032`, `B2033`, `B2034`, `B2035`, `B2036`, `B2037`, `B2038`,"
						+ " `B2039`, `B2040`, `B2041`, `B2042`, `B2043`, `B2044`, `B2045`, `B2046`, `B2047`, `B2048`, `B2049`, `B2050`, `B2051`, `B2052`, "
						+ "`B2053`, `B2054`, `B2055`, `B2056`, `B2057`, `B2058`, `B2059`, `B2060`, `B2061`, `B2062`, `B2063`, `B2064`, `B2065`, `B2066`,"
						+ " `B2067`, `B2068`, `B2069`, `B2070`, `B2071`, `B2072`, `B2073`, `B2074`, `B2075`, `B2076`, `B2077`, `B2078`, `B2079`, `B2080`, "
						+ "`B2081`, `B2082`, `B2083`, `B2084`, `B2085`, `B2086`, `B2087`, `B2088`, `B2089`, `B2090`, `B2091`, `B2092`, `B2093`, `B2094`, "
						+ "`B2095`, `B2096`, `B2097`, `B2098`, `B2099`, `B2100`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`)"
						+ " VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
//			System.out.println( dq_name );
			String dqS = dqx == 0 ? "" : "" + dqx;
			Map< CX , Map< Babies , double[] >> BirthPredictOfCX = (Map< CX , Map< Babies , double[] >>) predictVarMap
					.get( "BirthPredictOfCX" + dqS );
			Map< NY , Map< Babies , double[] >> BirthPredictOfNY = (Map< NY , Map< Babies , double[] >>) predictVarMap
					.get( "BirthPredictOfNY" + dqS );
			Map< Babies , double[] > BirthPredictOfAll = (Map< Babies , double[] >) predictVarMap
					.get( "BirthPredictOfAll" + dqS );

			for ( CX cx : CX.values() )
			{
				BirthPredict = BirthPredictOfCX.get( cx );
				for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
				{
					sqlBuilder.append( "( NULL,'" + X + "'," );
					for ( Babies ab : Babies.values() )
					{
						String tmp = ( BirthPredict.get( ab )[ X ] > 0 && BabiesTypeMap
								.get( ab ) ) ? strValues.get( BirthPredict
								.get( ab )[ X ] ) : ""
								+ BirthPredict.get( ab )[ X ];
						sqlBuilder.append( "'" + tmp + "'," );
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ cx.getChinese() + "','分年龄生育预测'," + commonInsert
							+ ")," );
				}
			}
			for ( NY ny : NY.values() )
			{
				BirthPredict = BirthPredictOfNY.get( ny );
				for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
				{
					sqlBuilder.append( "( NULL,'" + X + "'," );
					for ( Babies ab : Babies.values() )
					{
						String tmp = ( BirthPredict.get( ab )[ X ] > 0 && BabiesTypeMap
								.get( ab ) ) ? strValues.get( BirthPredict
								.get( ab )[ X ] ) : ""
								+ BirthPredict.get( ab )[ X ];
						sqlBuilder.append( "'" + tmp + "'," );
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ ny.getChinese() + "','分年龄生育预测'," + commonInsert
							+ ")," );
				}
			}
			BirthPredict = BirthPredictOfAll;
			for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
			{
				sqlBuilder.append( "( NULL,'" + X + "'," );
				for ( Babies ab : Babies.values() )
				{
					String tmp = ( BirthPredict.get( ab )[ X ] > 0 && BabiesTypeMap
							.get( ab ) ) ? strValues
							.get( BirthPredict.get( ab )[ X ] ) : ""
							+ BirthPredict.get( ab )[ X ];
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','" + "全国"
						+ "','分年龄生育预测'," + commonInsert + ")," );
			}
		}

		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		// System.out.println(sqlBuilder.toString());
		stmt.executeUpdate( sqlBuilder.toString() );

	}

	@Override
	public void saveBabiesBorn ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
//		Map<Year,Map<BabiesBorn,Double>> GiveBabiesBorn;
		// 生育孩次数
//		 Map<Year,Map<BabiesBorn,Double>> GiveBabiesBorn= (Map< Year , Map<BabiesBorn , Double >>) predictVarMap.get( "BabiesBorn"+dqx );
		// 全国
//		 Map<Year,Map<BabiesBorn,Double>> NationGiveBabiesBorn= (Map< Year ,Map< BabiesBorn , Double >>) predictVarMap.get( "BabiesBorn" );

		// 创建字段类型判断表——判断是否是 varchar类型
		Map< BabiesBorn , Boolean > BabiesBornTypeMap = new EnumMap< BabiesBorn , Boolean >(
				BabiesBorn.class );
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_分婚配模式生育孩次数`" );
		rs.next();
		for ( BabiesBorn ab : BabiesBorn.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			BabiesBornTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_分婚配模式生育孩次数` (`id`, `年份`, `总HJB`, `城HJB`, `乡HJB`, `总HJB1`, `城HJB1`, `乡HJB1`, " +
						"`总FNB1`, `总NYB1`, `总HJB2`, `城HJB2`, `乡HJB2`, `总FNB2`, `总NYB2`, `总HJ双独B`, `城HJ双独B`, `乡HJ双独B`, `总FN双独B`, " +
						"`城FN双独B`, `乡FN双独B`, `总NY双独B`, `城NY双独B`, `乡NY双独B`, `总HJ双独B1`, `城HJ双独B1`, `乡HJ双独B1`, `总FN双独B1`, " +
						"`城FN双独B1`, `乡FN双独B1`, `总NY双独B1`, `城NY双独B1`, `乡NY双独B1`, `总HJ双独B2`, `城HJ双独B2`, `乡HJ双独B2`, `总FN双独B2`, " +
						"`城FN双独B2`, `乡FN双独B2`, `总NY双独B2`, `城NY双独B2`, `乡NY双独B2`, `总HJ单独B`, `城HJ单独B`, `乡HJ单独B`, `总FN单独B`, " +
						"`城FN单独B`, `乡FN单独B`, `总NY单独B`, `城NY单独B`, `乡NY单独B`, `总HJ单独B1`, `城HJ单独B1`, `乡HJ单独B1`, `总FN单独B1`, " +
						"`城FN单独B1`, `乡FN单独B1`, `总NY单独B1`, `城NY单独B1`, `乡NY单独B1`, `总HJ单独B2`, `城HJ单独B2`, `乡HJ单独B2`, `总FN单独B2`, " +
						"`城FN单独B2`, `乡FN单独B2`, `总NY单独B2`, `城NY单独B2`, `乡NY单独B2`, `总HJ双非B`, `城HJ双非B`, `乡HJ双非B`, `总FN双非B`, " +
						"`城FN双非B`, `乡FN双非B`, `总NY双非B`, `城NY双非B`, `乡NY双非B`, `总HJ双非B1`, `城HJ双非B1`, `乡HJ双非B1`, `总FN双非B1`," +
						" `城FN双非B1`, `乡FN双非B1`, `总NY双非B1`, `城NY双非B1`, `乡NY双非B1`, `总HJ双非B2`, `城HJ双非B2`, `乡HJ双非B2`, `总FN双非B2`, " +
						"`城FN双非B2`, `乡FN双非B2`, `总NY双非B2`, `城NY双非B2`, `乡NY双非B2`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, " +
						"`JAQY`, `FAZCSJ`) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<Year,Map<BabiesBorn,Double>> GiveBabiesBorn= (Map< Year , Map<BabiesBorn , Double >>) predictVarMap.get( "BabiesBorn"+dqS );

			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
				sqlBuilder.append( "( NULL,'" + Y + "'," );
				for ( BabiesBorn ab : BabiesBorn.values() )
				{
					String tmp = ( GiveBabiesBorn.get( Year.getYear( Y ) ).get( ab )> 0 && BabiesBornTypeMap
							.get( ab ) ) ? strValues
							.get( GiveBabiesBorn.get( Year.getYear( Y ) ).get( ab ) ) : ""
							+ GiveBabiesBorn.get( Year.getYear( Y ) ).get( ab );
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','" + "全国"
						+ "','分婚配模式生育孩次数'," + commonInsert + ")," );
			}
		}

		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );
	}

	@Override
	public void saveBirthRatePredict (
			HashMap< String , Object > predictVarMap , StringList strValues )
			throws SQLException , MyDAOException
	{
		Map< BabiesBorn , Boolean > BabiesBornTypeMap = new EnumMap< BabiesBorn , Boolean >(
				BabiesBorn.class );
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_妇女分年龄生育率预测`" );
		rs.next();
		for ( BabiesBorn ab : BabiesBorn.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			BabiesBornTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_妇女分年龄生育率预测` (`id`, `X`, `F2005`, `F2006`, `F2007`, `F2008`, `F2009`, `F2010`, "
						+ "`F2011`, `F2012`, `F2013`, `F2014`, `F2015`, `F2016`, `F2017`, `F2018`, `F2019`, `F2020`, `F2021`, `F2022`, `F2023`, `F2024`, "
						+ "`F2025`, `F2026`, `F2027`, `F2028`, `F2029`, `F2030`, `F2031`, `F2032`, `F2033`, `F2034`, `F2035`, `F2036`, `F2037`, `F2038`,"
						+ " `F2039`, `F2040`, `F2041`, `F2042`, `F2043`, `F2044`, `F2045`, `F2046`, `F2047`, `F2048`, `F2049`, `F2050`, `F2051`, `F2052`, "
						+ "`F2053`, `F2054`, `F2055`, `F2056`, `F2057`, `F2058`, `F2059`, `F2060`, `F2061`, `F2062`, `F2063`, `F2064`, `F2065`, `F2066`,"
						+ " `F2067`, `F2068`, `F2069`, `F2070`, `F2071`, `F2072`, `F2073`, `F2074`, `F2075`, `F2076`, `F2077`, `F2078`, `F2079`, `F2080`, "
						+ "`F2081`, `F2082`, `F2083`, `F2084`, `F2085`, `F2086`, `F2087`, `F2088`, `F2089`, `F2090`, `F2091`, `F2092`, `F2093`, `F2094`, "
						+ "`F2095`, `F2096`, `F2097`, `F2098`, `F2099`, `F2100`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`)"
						+ " VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<Year, double[]> BirthRatePredict = (Map<Year, double[]>)predictVarMap.get ( "BirthRatePredict"+dqS);
			

			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
				sqlBuilder.append( "( NULL,'" + Y + "'," );
				for(int i=0;i<Const.MAX_AGE;i++)
				{
					String tmp =  "" + BirthRatePredict.get( Year.getYear( Y ) )[i];
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','" + "全国"
						+ "','分婚配模式生育孩次数'," + commonInsert + ")," );
			}
		}

		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );
	}

	@Override
	public void saveCouple ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		Map< Couple , Boolean > CoupleTypeMap = new EnumMap< Couple , Boolean >(Couple.class );
		
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_夫妇及子女表结构`" );
		rs.next();
		for ( Couple ab : Couple.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			CoupleTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_夫妇及子女表结构` (`id`, `年份`, `非农政策1`, `农业政策1`, `非农政策2`, `农业政策2`, `非农政策3`, `农业政策3`, `TFR`, `双独TFR`, `单独TFR`, "+
						"`双非TFR`, `非农双独`, `非农男单`, `非农女单`, `非农双非`, `农业双独`, `农业男单`, `农业女单`, `农业双非`, `合计夫妇数`, `双独夫妇数`, `单独夫妇数`, `女单夫妇数`, `男单夫妇数`, `单独堆积`, "+
						"`已2单独`, `双非夫妇数`, `双非堆积`, `已2双非`, `双独生育数`, `单独生育数`, `女单生育数`, `男单生育数`, `双非生育数`, `C超生`, `X超生`, `D`, `DM`, `DF`, `N`, `NM`, `NF`, `可二后`, "+
						"`DDBM`, `DDBF`, `独生子女30`, `独生子30`, `独生女30`, `非独子女30`, `非独子30`, `非独女30`, `可二后30`, `可二后30M`, `可二后30F`, `独生子女SW`, `独生子SW`, `独生女SW`, `非独子女SW`, "+
						"`非独子SW`, `非独女SW`, `可二后SW`, `可二后SWM`, `可二后SWF`, `一孩父母`, `一孩父`, `一孩母`, `特扶`, `特扶M`, `特扶F`, `未婚F`, `未婚M`, `未婚DF`, `未婚DM`, `未婚NF`, `未婚NM`, `农业未婚F`,"+
						"`农业未婚M`, `非农未婚F`, `非农未婚M`, `农业未婚DF`, `农业未婚DM`, `农业未婚NF`, `农业未婚NM`, `非农未婚DF`, `非农未婚DM`, `非农未婚NF`, `非农未婚NM`, `双独概率M`, `双独概率F`, `男单概率M`, `男单概率F`, "+
						"`女单概率M`, `女单概率F`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`,) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>> CoupleAndChildrenOfCXNY = (Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>>)predictVarMap.get ( "CoupleAndChildrenOfCXNY"+dqS);
			Map<CX,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfCX = (Map<CX,Map<Year,Map<Couple,Double>>>)predictVarMap.get ( "CoupleAndChildrenOfCX"+dqS);
			Map<NY,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfNY = (Map<NY,Map<Year,Map<Couple,Double>>>)predictVarMap.get ( "CoupleAndChildrenOfNY"+dqS);
			Map<Year,Map<Couple,Double>> CoupleAndChildrenOfAll = (Map<Year,Map<Couple,Double>>)predictVarMap.get ( "CoupleAndChildrenOfAll"+dqS);
			
			Map<Couple,Double> CoupleAndChildren;
			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
				sqlBuilder.append( "( NULL,'" + Y + "'," );
				for ( CX cx : CX.values() )
				{
					CoupleAndChildren = CoupleAndChildrenOfCX.get( cx ).get(Year.getYear(Y));
					for ( Couple ab : Couple.values() )
					{
						String tmp = ( CoupleAndChildren.get( ab ) > 0 && CoupleTypeMap.get( ab ) ) ? strValues.get( CoupleAndChildren.get( ab ) ) : ""	+ CoupleAndChildren.get( ab );
						sqlBuilder.append( "'" + tmp + "'," );
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ cx.getChinese() + "','夫妇及子女表结构'," + commonInsert
							+ ")," );
				}
				for ( NY ny : NY.values() )
				{
					CoupleAndChildren = CoupleAndChildrenOfNY.get( ny ).get(Year.getYear(Y));
					for ( Couple ab : Couple.values() )
					{
						String tmp = ( CoupleAndChildren.get( ab ) > 0 && CoupleTypeMap.get( ab ) ) ? strValues.get( CoupleAndChildren.get( ab ) ) : ""	+ CoupleAndChildren.get( ab );
						sqlBuilder.append( "'" + tmp + "'," );
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ ny.getChinese() + "','夫妇及子女表结构'," + commonInsert
							+ ")," );
				}
				for(CX cx : CX.values()){
					for(NY ny : NY.values()){
						CoupleAndChildren = CoupleAndChildrenOfCXNY.get( cx ).get(ny).get(Year.getYear(Y));
						for ( Couple ab : Couple.values() )
						{
							String tmp = ( CoupleAndChildren.get( ab ) > 0 && CoupleTypeMap.get( ab ) ) ? strValues.get( CoupleAndChildren.get( ab ) ) : ""	+ CoupleAndChildren.get( ab );
							sqlBuilder.append( "'" + tmp + "'," );
						}
						sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
								+ cx.getChinese()+ny.getChinese() + "','夫妇及子女表结构'," + commonInsert
								+ ")," );
					}
				}
				CoupleAndChildren = CoupleAndChildrenOfAll.get(Year.getYear(Y));
				for ( Couple ab : Couple.values() )
				{
					String tmp = ( CoupleAndChildren.get( ab ) > 0 && CoupleTypeMap.get( ab ) ) ? strValues.get( CoupleAndChildren.get( ab ) ) : ""	+ CoupleAndChildren.get( ab );
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "'全国'" + "','夫妇及子女表结构'," + commonInsert
						+ ")," );
			}
		}

		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );
		
	}

	@Override
	public void saveHunpei ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
	
		 
		Map< HunpeiField , Boolean > HunpeiTypeMap = new EnumMap< HunpeiField , Boolean >(HunpeiField.class );
		
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_婚配预测表结构`" );
		rs.next();
		for ( HunpeiField ab : HunpeiField.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			HunpeiTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_婚配预测表结构` (`id`, `X`, `合J分龄SYL`, `双D分龄SYL`, `单D分龄SYL`, `双N分龄SYL`, `FRD`, `FR`, `FR1`, `FR2`, `独转非`, `DDB`, `NMDFB`,"+
						"`NFDMB`, `NNB`, `政策生育`, `政策DDB`, `政策NMDFB`, `政策NFDMB`, `政策NNB`, `政策释放B`, `超生生育`, `超生DDB`, `超生NMDFB`, `超生NFDMB`, `超生NNB`, `超生释放B`, `HJ`, `HJM`,"+
						"`HJF`, `D`, `DM`, `DF`, `N`, `NM`, `NF`, `DDBS`, `DDBM`, `DDBF`, `NMDFBS`, `NFDMBS`, `NNBS`, `HJQY`, `HJQYM`, `HJQYF`, `DQY`, `DQYM`, `DQYF`, `NQY`, `NQYM`,"+
						"`NQYF`,`DDBQYS`, `DDBQYM`, `DDBQYF`, `DM0`, `DF0`, `NM0`, `NF0`, `MIDM0`, `MIDF0`, `MINM0`, `MINF0`, `夫妇合计`, `DD`, `NMDF`, `NFDM`, `NN`, `DD_F`, `NMDF_F`, "+
						"`NFDM_F`, `NN_F`, `DD_M`, `NMDF_M`, `NFDM_M`, `NN_M`, `DD非F`, `NMDF非F`, `NFDM非F`, `NN非F`, `DD非F_F`, `NMDF非F_F`, `NFDM非F_F`, `NN非F_F`, `DD非F_M`, `NMDF非F_M`,"+
						"`NFDM非F_M`, `NN非F_M`, `DD农F`, `NMDF农F`, `NFDM农F`, `NN农F`, `DD农F_F`, `NMDF农F_F`, `NFDM农F_F`, `NN农F_F`, `DD农F_M`, `NMDF农F_M`, `NFDM农F_M`, `NN农F_M`, `DD_CFXF`,"+
						"`NMDF_CFXF`, `NFDM_CFXF`, `NN_CFXF`, `DD_CFXN`, `NMDF_CFXN`, `NFDM_CFXN`, `NN_CFXN`, `DD_CNXF`, `NMDF_CNXF`, `NFDM_CNXF`, `NN_CNXF`, `DD_CNXN`, `NMDF_CNXN`, "+
						"`NFDM_CNXN`, `NN_CNXN`, `DD_XFCF`, `NMDF_XFCF`, `NFDM_XFCF`, `NN_XFCF`, `DD_XFCN`, `NMDF_XFCN`, `NFDM_XFCN`, `NN_XFCN`, `DD_XNCF`, `NMDF_XNCF`, `NFDM_XNCF`, "+
						"`NN_XNCF`, `DD_XNCN`, `NMDF_XNCN`, `NFDM_XNCN`, `NN_XNCN`, `DD0`, `DM10`, `DM20`, `DM30`, `DM40`, `DF10`, `DF20`, `DF30`, `DF40`, `NM10`, `NM20`, `NM30`, `NM40`, "+
						"`NF10`, `NF20`, `NF30`, `NF40`, `FR201`, `FR202`, `FR203`, `FR204`, `FR205`, `FR206`, `FR207`, `FR208`, `FR209`, `FR210`, `FR211`, `FR212`, `SWD`, `SWDM`, `SWDF`,"+
						"`单独释放B`, `双非释放B`, `单独堆积`, `单独堆积15`, `单独堆积30`, `单独堆积35`, `堆积丈夫`, `女单堆积`, `男单堆积`, `已2单独`, `已2女单`, `已2男单`, `双非堆积`, `双非堆积15`, `双非堆积30`, `双非堆积35`,"+
						"`已2双非`, `堆积比例`, `释放模式`, `双独概率M`, `双独概率F`, `男单概率M`, `男单概率F`, `女单概率M`, `女单概率F`, `双非概率M`, `双非概率F`, `NF双独GLM`, `NF双独GLF`, `NF男单GLM`, `NF男单GLF`, `NF女单GLM`,"+
						"`NF女单GLF`, `NF双非GLM`, `NF双非GLF`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`,  `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`,"+
						"`JAQY`, `FAZCSJ`,) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<CX,Map<NY,Map<HunpeiField,double[]>>> HunpeiOfCXNY = (Map<CX,Map<NY,Map<HunpeiField,double[]>>>)predictVarMap.get ( "HunpeiOfCXNY"+dqS);
			Map<CX,Map<HunpeiField,double[]>> HunpeiOfCX = (Map<CX,Map<HunpeiField,double[]>>)predictVarMap.get ( "HunpeiOfCX"+dqS);
			Map<NY,Map<HunpeiField,double[]>> HunpeiOfNY = (Map<NY,Map<HunpeiField,double[]>>)predictVarMap.get ( "HunpeiOfNY"+dqS);
			Map<HunpeiField,double[]> HunpeiOfAll = (Map<HunpeiField,double[]>)predictVarMap.get ( "HunpeiOfAll"+dqS);
			
			Map<HunpeiField, double[]> Hunpei;
			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
				for ( CX cx : CX.values() )
				{
					Hunpei = HunpeiOfCX.get( cx );
					for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
					{
						sqlBuilder.append( "( NULL,'" + X + "'," );
						for ( HunpeiField ab : HunpeiField.values() )
						{
							String tmp = ( Hunpei.get( ab )[X] > 0 && HunpeiTypeMap.get( ab ) ) ? strValues.get( Hunpei.get( ab )[X] ) : ""	+ Hunpei.get( ab )[X];
							sqlBuilder.append( "'" + tmp + "'," );
						}
						sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
								+ cx.getChinese() + "','婚配预测表结构'," + commonInsert
								+ ")," );
					}
				}
				for ( NY ny : NY.values() )
				{
					Hunpei = HunpeiOfNY.get( ny );
					for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
					{
						sqlBuilder.append( "( NULL,'" + X + "'," );
						for ( HunpeiField ab : HunpeiField.values() )
						{
							String tmp = ( Hunpei.get( ab )[X] > 0 && HunpeiTypeMap.get( ab ) ) ? strValues.get( Hunpei.get( ab )[X] ) : ""	+ Hunpei.get( ab )[X];
							sqlBuilder.append( "'" + tmp + "'," );
						}
						sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
								+ ny.getChinese() + "','婚配预测表结构'," + commonInsert
								+ ")," );
					}
				}
				for(CX cx : CX.values()){
					for(NY ny : NY.values()){
						Hunpei = HunpeiOfCXNY.get( cx ).get(ny);
						for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
						{
							sqlBuilder.append( "( NULL,'" + X + "'," );
							for ( HunpeiField ab : HunpeiField.values() )
							{
								String tmp = ( Hunpei.get( ab )[X] > 0 && HunpeiTypeMap.get( ab ) ) ? strValues.get( Hunpei.get( ab )[X] ) : ""	+ Hunpei.get( ab )[X];
								sqlBuilder.append( "'" + tmp + "'," );
							}
							sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
									+ cx.getChinese()+ny.getChinese() + "','婚配预测表结构'," + commonInsert
									+ ")," );
						}
					}
				}
				Hunpei = HunpeiOfAll;
				for ( int X = MAX_AGE - 1 ; X >= 0 ; X-- )
				{
					sqlBuilder.append( "( NULL,'" + X + "'," );
					for ( HunpeiField ab : HunpeiField.values() )
					{
						String tmp = ( Hunpei.get( ab )[X] > 0 && HunpeiTypeMap.get( ab ) ) ? strValues.get( Hunpei.get( ab )[X] ) : ""	+ Hunpei.get( ab )[X];
						sqlBuilder.append( "'" + tmp + "'," );
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ "'全国'" + "','婚配预测表结构'," + commonInsert
							+ ")," );
				}
			}
		}

		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );
		
	}

	@Override
	public void saveMirriageRate ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		Map< HunpeiGL , Boolean > MarriageTypeMap = new EnumMap< HunpeiGL , Boolean >(HunpeiGL.class );
		
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_婚配概率`" );
		rs.next();
		for ( HunpeiGL ab : HunpeiGL.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			MarriageTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_婚配概率` (`id`, `年份`, `地区`, `X农双独GLM`, `X农双独GLF`, `X非双独GLM`, `X非双独GLF`, `X农男单GLM`, `X农男单GLF`, `X农女单GLM`, `X农女单GLF`,"+
						"`X非男单GLM`, `X非男单GLF`, `X非女单GLM`, `X非女单GLF`, `C农双独GLM`, `C农双独GLF`, `C非双独GLM`, `C非双独GLF`, `C农男单GLM`, `C农男单GLF`, `C农女单GLM`, `C农女单GLF`, `C非男单GLM`, "+
						"`C非男单GLF`, `C非女单GLM`, `C非女单GLF`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<Year,Map<HunpeiGL,Double>>MarriageRate = (Map<Year,Map<HunpeiGL,Double>>)predictVarMap.get ( "MarriageRate"+dqS);
			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
<<<<<<< HEAD
				sqlBuilder.append( "( NULL,'" + Y + "','"+dq_name+"'," );
				for(HunpeiGL hp : HunpeiGL.values()){
					String tmp = ( MarriageRate.get(Year.getYear(Y)).get( hp ) > 0 && MarriageTypeMap.get(hp) ) ? strValues.get( MarriageRate.get(Year.getYear(Y)).get( hp )) : ""
							+MarriageRate.get(Year.getYear(Y)).get( hp );
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "全国" + "','婚配概率'," + commonInsert
						+ ")," );
=======
				sqlBuilder.append( "( NULL,'" + Y + "'," );
>>>>>>> 5972abe2cea7028cad15e4d40056f74242f64fed
			}
		}
		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );
	}

	@Override
	public void savePolicyBirth ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		Map< PolicyBirth , Boolean > PolicyBirthTypeMap = new EnumMap< PolicyBirth , Boolean >(PolicyBirth.class );
		
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_政策及政策生育率`" );
		rs.next();
		for ( PolicyBirth ab : PolicyBirth.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			PolicyBirthTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_政策及政策生育率` (`id`, `年份`, `非农政策1`, `农业政策1`, `非农政策2`, `农业政策2`, `非农政策3`, `农业政策3`, `非农双独`, `农业双独`, `非农单独`, `农业单独`, `非农女单`, `农业女单`,"+
						"`非农男单`, `农业男单`, `非农双非`, `农业双非`, `农村NYTFR`, `农村FNTFR`, `农村TFR`, `城镇NYTFR`, `城镇FNTFR`, `城镇TFR`, `地区NYTFR`, `地区FNTFR`, `地区TFR`, `NCNY婚TFR`, `NCFN婚TFR`, "+
						"`NC婚TFR`, `CZNY婚TFR`, `CZFN婚TFR`, `CZ婚TFR`, `QSNY婚TFR`, `QSFN婚TFR`, `QS婚TFR`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<Year,Map<PolicyBirth,Double>> PolicyBirthRate = (Map<Year,Map<PolicyBirth,Double>>)predictVarMap.get ( "PolicyBirthRate"+dqS);
			for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ )
			{
				sqlBuilder.append( "( NULL,'" + Y + "','"+dq_name+"'," );
				for(PolicyBirth hp : PolicyBirth.values()){
					String tmp = ( PolicyBirthRate.get(Year.getYear(Y)).get( hp ) > 0 && PolicyBirthTypeMap.get(hp) ) ? strValues.get( PolicyBirthRate.get(Year.getYear(Y)).get( hp )) : ""
							+PolicyBirthRate.get(Year.getYear(Y)).get( hp );
					sqlBuilder.append( "'" + tmp + "'," );
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "全国" + "','政策及政策生育率'," + commonInsert
						+ ")," );
			}
		}
		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );

	}

	@Override
	public void savePredict ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		Map< PolicyBirth , Boolean > PolicyBirthTypeMap = new EnumMap< PolicyBirth , Boolean >(PolicyBirth.class );
		
		rs = stmt.executeQuery( "desc `新人口学`.`仿真结果_政策及政策生育率`" );
		rs.next();
		for ( PolicyBirth ab : PolicyBirth.values() )
		{
			rs.next();
			boolean tmp = rs.getString( "Type" ).matches( "varchar.*" );
			PolicyBirthTypeMap.put( ab , tmp );
		}

		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_婚配概率` (`id`, `X`, `M2005`, `F2005`, `M2006`, `F2006`, `M2007`, `F2007`, `M2008`, `F2008`, `M2009`, `F2009`, `M2010`, `F2010`,"+
						"`M2011`, `F2011`, `M2012`, `F2012`, `M2013`, `F2013`, `M2014`, `F2014`, `M2015`, `F2015`, `M2016`, `F2016`, `M2017`, `F2017`, `M2018`, `F2018`, `M2019`, `F2019`,"+
						"`M2020`, `F2020`, `M2021`, `F2021`, `M2022`, `F2022`, `M2023`, `F2023`, `M2024`, `F2024`, `M2025`, `F2025`, `M2026`, `F2026`, `M2027`, `F2027`, `M2028`, `F2028`, "+
						"`M2029`, `F2029`, `M2030`, `F2030`, `M2031`, `F2031`, `M2032`, `F2032`, `M2033`, `F2033`, `M2034`, `F2034`, `M2035`, `F2035`, `M2036`, `F2036`, `M2037`, `F2037`, "+
						"`M2038`, `F2038`, `M2039`, `F2039`, `M2040`, `F2040`, `M2041`, `F2041`, `M2042`, `F2042`, `M2043`, `F2043`, `M2044`, `F2044`, `M2045`, `F2045`, `M2046`, `F2046`, "+
						"`M2047`, `F2047`, `M2048`, `F2048`, `M2049`, `F2049`, `M2050`, `F2050`, `M2051`, `F2051`, `M2052`, `F2052`, `M2053`, `F2053`, `M2054`, `F2054`, `M2055`, `F2055`, "+
						"`M2056`, `F2056`, `M2057`, `F2057`, `M2058`, `F2058`, `M2059`, `F2059`, `M2060`, `F2060`, `M2061`, `F2061`, `M2062`, `F2062`, `M2063`, `F2063`, `M2064`, `F2064`, "+
						"`M2065`, `F2065`, `M2066`, `F2066`, `M2067`, `F2067`, `M2068`, `F2068`, `M2069`, `F2069`, `M2070`, `F2070`, `M2071`, `F2071`, `M2072`, `F2072`, `M2073`, `F2073`, "+
						"`M2074`, `F2074`, `M2075`, `F2075`, `M2076`, `F2076`, `M2077`, `F2077`, `M2078`, `F2078`, `M2079`, `F2079`, `M2080`, `F2080`, `M2081`, `F2081`, `M2082`, `F2082`, "+
						"`M2083`, `F2083`, `M2084`, `F2084`, `M2085`, `F2085`, `M2086`, `F2086`, `M2087`, `F2087`, `M2088`, `F2088`, `M2089`, `F2089`, `M2090`, `F2090`, `M2091`, `F2091`, "+
						"`M2092`, `F2092`, `M2093`, `F2093`, `M2094`, `F2094`, `M2095`, `F2095`, `M2096`, `F2096`, `M2097`, `F2097`, `M2098`, `F2098`, `M2099`, `F2099`, `M2100`, `F2100`, "+
						"`对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`) VALUES " );

		int dqx;
		for ( dqx = 0 ; dqx <= 65 ; dqx++ )
		{
			if ( !xbbMap.containsKey( "" + dqx ) )
				continue;
			String dq_name = xbbMap.get( "" + dqx ).getDqName();
			String dqS = dqx == 0 ? "" : "" + dqx;
			
			Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> PopulationPredictOfCXNY = (Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>>)predictVarMap.get ( "PopulationPredictOfCXNY"+dqS);
			Map<NY,Map<Year,Map<XB,double[]>>> PopulationPredictOfNY = (Map<NY,Map<Year,Map<XB,double[]>>>)predictVarMap.get ( "PopulationPredictOfNY"+dqS);
			Map<Year,Map<XB,double[]>>PopulationPredictOfAll = (Map<Year,Map<XB,double[]>>)predictVarMap.get ( "PopulationPredictOfAll"+dqS);
			Map<CX,Map<Year,Map<XB,double[]>>> PopulationMigrationOfCX = (Map<CX,Map<Year,Map<XB,double[]>>>)predictVarMap.get("PopulationMigrationOfCX"+dqS);
			Map<Year,Map<XB,double[]>>PopulationMigrationOfAll = (Map<Year,Map<XB,double[]>>)predictVarMap.get("PopulationMigrationOfAll"+dqS);
			Map<CX,Map<Year,Map<XB,double[]>>> PopulationDeathOfCX = (Map<CX,Map<Year,Map<XB,double[]>>>)predictVarMap.get ( "PopulationDeathOfCX"+dqS);
			Map<Year,Map<XB,double[]>> PopulationDeathOfAll = (Map<Year,Map<XB,double[]>>)predictVarMap.get ( "PopulationDeathOfAll"+dqS);
			
			for(CX cx:CX.values()){
				Map<Year,Map<XB,double[]>> population = PopulationMigrationOfCX.get(cx);
				for(int X=0;X<Const.MAX_AGE;X++){
					sqlBuilder.append("(NULL,'" + X + "',");
					for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
						for(XB xb:XB.values()){
							String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
							sqlBuilder.append("'"+tmp+"',");
						}
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ cx.toString() + "','政策及政策生育率'," + commonInsert
							+ ")," );
				}
				population = PopulationDeathOfCX.get(cx);
				for(int X=0;X<Const.MAX_AGE;X++){
					sqlBuilder.append("(NULL,'" + X + "',");
					for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
						for(XB xb:XB.values()){
							String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
							sqlBuilder.append("'"+tmp+"',");
						}
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ cx.toString() + "','政策及政策生育率'," + commonInsert
							+ ")," );
				}
			}
			for(NY ny:NY.values()){
				Map<Year,Map<XB,double[]>> population = PopulationPredictOfNY.get(ny);
				for(int X=0;X<Const.MAX_AGE;X++){
					sqlBuilder.append("(NULL,'" + X + "',");
					for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
						for(XB xb:XB.values()){
							String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
							sqlBuilder.append("'"+tmp+"',");
						}
					}
					sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
							+ ny.toString() + "','政策及政策生育率'," + commonInsert
							+ ")," );
				}
			}
			for(CX cx:CX.values()){
				for(NY ny:NY.values()){
					Map<Year,Map<XB,double[]>> population = PopulationPredictOfCXNY.get(cx).get(ny);
					for(int X=0;X<Const.MAX_AGE;X++){
						sqlBuilder.append("(NULL,'" + X + "',");
						for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
							for(XB xb:XB.values()){
								String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
								sqlBuilder.append("'"+tmp+"',");
							}
						}
						sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
								+ cx.toString()+ny.toString() + "','政策及政策生育率'," + commonInsert
								+ ")," );
					}
				}
			}
			Map<Year,Map<XB,double[]>> population = PopulationPredictOfAll;
			for(int X=0;X<Const.MAX_AGE;X++){
				sqlBuilder.append("(NULL,'" + X + "',");
				for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
					for(XB xb:XB.values()){
						String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
						sqlBuilder.append("'"+tmp+"',");
					}
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "全国" + "','政策及政策生育率'," + commonInsert
						+ ")," );
			}
			population = PopulationMigrationOfAll;
			for(int X=0;X<Const.MAX_AGE;X++){
				sqlBuilder.append("(NULL,'" + X + "',");
				for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
					for(XB xb:XB.values()){
						String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
						sqlBuilder.append("'"+tmp+"',");
					}
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "全国" + "','政策及政策生育率'," + commonInsert
						+ ")," );
			}
			population = PopulationDeathOfAll;
			for(int X=0;X<Const.MAX_AGE;X++){
				sqlBuilder.append("(NULL,'" + X + "',");
				for ( int Y = envparam.getBegin() ; Y <=envparam.getEnd() ; Y++ ){
					for(XB xb:XB.values()){
						String tmp = ""+population.get(Year.getYear(Y)).get(xb)[X];
						sqlBuilder.append("'"+tmp+"',");
					}
				}
				sqlBuilder.append( taskID + "," + "'" + dq_name + "','"
						+ "全国" + "','政策及政策生育率'," + commonInsert
						+ ")," );
			}
		}
		sqlBuilder.delete( sqlBuilder.length() - 1 , sqlBuilder.length() );
		// v.writelnToTmpFile( sqlBuilder.toString() );
		// v.saveTmpFile();
		 System.out.println(sqlBuilder.toString());
		// stmt.executeUpdate( sqlBuilder.toString() );

	}

	@Override
	public void saveQYPH ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		cleanSQLBuilder();
		sqlBuilder
				.append( "INSERT INTO `新人口学`.`仿真结果_全国迁移平衡表结构` (`id`, `X`, `迁出T`, `迁出M`, `迁出F`, `迁入T`, `迁入M`, `迁入F`, `迁移平衡T`, `迁移平衡M`, `迁移平衡F`, `平衡迁出T`, `平衡迁出M`, "+
						"`平衡迁出F`, `平衡迁入T`, `平衡迁入M`, `平衡迁入F`, `对应任务id`, `DQ`, `CXNY`, `Table`, `ImplEstm`, `SFMS`, `JAQY`, `FAZCSJ`) VALUES " );
		//TODO 这是要写入那个变量???
	}

	@Override
	public void saveSonDie ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void saveSumBM ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void saveSummary ( HashMap< String , Object > predictVarMap ,
			StringList strValues ) throws SQLException , MyDAOException
	{
		// TODO Auto-generated method stub
	}

}
