package prclqz.DAO.Bean;
/**
 * 对应数组 ARDQ出生XBB [31] , [14]
 * 字段顺序：
 * COPY TO ARRA ARDQ出生XBB FIEL 
 * 1省代码, 					dqx
 * 2地区, 					dqName
 * 3性别比.出生XBB05,		bornXbb05
 * 4性别比.出生XBB90,		bornXbb90
 * 5生育率实现比.现政比09,	nowPolicy09
 * 6生育率实现比.现政比年增,	nowPolicyInc
 * 7生育率实现比.现实09,;	now09
 * 8生育率实现比.政策09,		policy09
 * 9城乡构成.CZRKB,			CZRKB(城镇人口比)
 * 10净迁率08.迁移率,		qianYiRate
 * 11生育率实现比.现实mi,	nowMi
 * 12生育率实现比.现政比09,	nowPolicy09_2
 * 13生育率实现比.现政比年增,nowPolicyInc_2
 * 14分省代码.三地带			diDai	
 *  FOR 地区<>'全国
 * @author prclqz@zju.edu.cn
 *
 */
public class BornXbbBean {
	
	//以下顺序按照上面注释的顺序
	private int dqx;
	private String dqName;
	
	private double bornXbb05,bornXbb90,nowPolicy09,nowPolicyInc,
		now09,policy09,CZRKB,qianYiRate,nowMi,nowPolicy09_2,nowPolicyInc_2;
	private String diDai;
	
	/**
	 * 土地面积，耕地06，水资源均量
		土地面积		&&单位:千公顷
		耕地06			&&单位:千公顷
		水资源均量		&&单位:亿立方米
	 */
	private double land,field,water;
	//新增一个其他用途
	private double xbb;
	
	
	public double getXbb() {
		return xbb;
	}

	public void setXbb(double xbb) {
		this.xbb = xbb;
	}

	public int getDqx() {
		return dqx;
	}

	public void setDqx(int dqx) {
		this.dqx = dqx;
	}

	public String getDqName() {
		return dqName;
	}

	public void setDqName(String dqName) {
		this.dqName = dqName;
	}

	public String getDiDai() {
		return diDai;
	}

	public void setDiDai(String diDai) {
		this.diDai = diDai;
	}

	public double getBornXbb05() {
		return bornXbb05;
	}

	public void setBornXbb05(double bornXbb05) {
		this.bornXbb05 = bornXbb05;
	}

	public double getBornXbb90() {
		return bornXbb90;
	}

	public void setBornXbb90(double bornXbb90) {
		this.bornXbb90 = bornXbb90;
	}

	public double getNowPolicy09() {
		return nowPolicy09;
	}

	public void setNowPolicy09(double nowPolicy09) {
		this.nowPolicy09 = nowPolicy09;
	}

	public double getNowPolicyInc() {
		return nowPolicyInc;
	}

	public void setNowPolicyInc(double nowPolicyInc) {
		this.nowPolicyInc = nowPolicyInc;
	}

	public double getNow09() {
		return now09;
	}

	public void setNow09(double now09) {
		this.now09 = now09;
	}

	public double getPolicy09() {
		return policy09;
	}

	public void setPolicy09(double policy09) {
		this.policy09 = policy09;
	}

	public double getCZRKB() {
		return CZRKB;
	}

	public void setCZRKB(double cZRKB) {
		CZRKB = cZRKB;
	}

	public double getQianYiRate() {
		return qianYiRate;
	}

	public void setQianYiRate(double qianYiRate) {
		this.qianYiRate = qianYiRate;
	}

	public double getNowMi() {
		return nowMi;
	}

	public void setNowMi(double nowMi) {
		this.nowMi = nowMi;
	}

	public double getNowPolicy09_2() {
		return nowPolicy09_2;
	}

	public void setNowPolicy09_2(double nowPolicy09_2) {
		this.nowPolicy09_2 = nowPolicy09_2;
	}

	public double getNowPolicyInc_2() {
		return nowPolicyInc_2;
	}

	public void setNowPolicyInc_2(double nowPolicyInc_2) {
		this.nowPolicyInc_2 = nowPolicyInc_2;
	}

	public BornXbbBean(int dqx, String dqName, double bornXbb05,
			double bornXbb90, double nowPolicy09, double nowPolicyInc,
			double now09, double policy09, double cZRKB, double qianYiRate,
			double nowMi, double nowPolicy09_2, double nowPolicyInc_2,String diDai) {
		super();
		this.dqx = dqx;
		this.dqName = dqName;
		this.diDai = diDai;
		this.bornXbb05 = bornXbb05;
		this.bornXbb90 = bornXbb90;
		this.nowPolicy09 = nowPolicy09;
		this.nowPolicyInc = nowPolicyInc;
		this.now09 = now09;
		this.policy09 = policy09;
		CZRKB = cZRKB;
		this.qianYiRate = qianYiRate;
		this.nowMi = nowMi;
		this.nowPolicy09_2 = nowPolicy09_2;
		this.nowPolicyInc_2 = nowPolicyInc_2;
	}

	public BornXbbBean (int dqx, String dqName, double bornXbb05,
			double bornXbb90, double nowPolicy09, double nowPolicyInc,
			double now09, double policy09, double cZRKB, double qianYiRate,
			double nowMi, double nowPolicy09_2, double nowPolicyInc_2,String diDai ,
			double land, double field, double water)
	{
		super();
		this.dqx = dqx;
		this.dqName = dqName;
		this.diDai = diDai;
		this.bornXbb05 = bornXbb05;
		this.bornXbb90 = bornXbb90;
		this.nowPolicy09 = nowPolicy09;
		this.nowPolicyInc = nowPolicyInc;
		this.now09 = now09;
		this.policy09 = policy09;
		CZRKB = cZRKB;
		this.qianYiRate = qianYiRate;
		this.nowMi = nowMi;
		this.nowPolicy09_2 = nowPolicy09_2;
		this.nowPolicyInc_2 = nowPolicyInc_2;
	
		this.land = land;
		this.field = field;
		this.water = water;
	}

	public double getLand ()
	{
		return land;
	}

	public double getField ()
	{
		return field;
	}

	public double getWater ()
	{
		return water;
	}
	
}
