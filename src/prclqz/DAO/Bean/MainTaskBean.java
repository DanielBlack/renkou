package prclqz.DAO.Bean;

public class MainTaskBean {
	private String name;
	private int id,status,begin_order,end_order;
	private long create_time,begin_time,end_time;
	//基本参数id,基本数据id
	private int param_id,data_id;
	private double process_rate;
	
	public MainTaskBean(int id, String name,int status, int beginOrder, int endOrder,
			long createTime, long beginTime, long endTime, int paramTime,
			int dataTime, float processRate) {
		super();
		this.setName(name);
		this.id = id;
		this.status = status;
		begin_order = beginOrder;
		end_order = endOrder;
		create_time = createTime;
		begin_time = beginTime;
		end_time = endTime;
		param_id = paramTime;
		data_id = dataTime;
		process_rate = processRate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getBegin_order() {
		return begin_order;
	}
	public void setBegin_order(int beginOrder) {
		begin_order = beginOrder;
	}
	public int getEnd_order() {
		return end_order;
	}
	public void setEnd_order(int endOrder) {
		end_order = endOrder;
	}
	public long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(long createTime) {
		create_time = createTime;
	}
	public long getBegin_time() {
		return begin_time;
	}
	public void setBegin_time(long beginTime) {
		begin_time = beginTime;
	}
	public long getEnd_time() {
		return end_time;
	}
	public void setEnd_time(long endTime) {
		end_time = endTime;
	}
	public int getParam_id() {
		return param_id;
	}
	/**
	 * 基本参数批次，默认为1
	 * @param paramTime
	 */
	public void setParam_id(int paramTime) {
		param_id = paramTime;
	}
	public int getData_id() {
		return data_id;
	}
	/**
	 * 基本数据批次默认为2
	 * @param dataTime
	 */
	public void setData_id(int dataTime) {
		data_id = dataTime;
	}
	public double getProcess_rate() {
		return process_rate;
	}
	public void setProcess_rate(double d) {
		process_rate = d;
	}
	public int getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	


}
