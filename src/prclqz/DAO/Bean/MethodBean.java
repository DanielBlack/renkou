package prclqz.DAO.Bean;

import prclqz.methods.IMethod;

public class MethodBean {
	private int id,task_id,order_id,param_type;
	private String method,notes,parameters;
	private IMethod clsIns;
	
	public IMethod getClsIns() {
		return clsIns;
	}
	public void setClsIns(IMethod clsIns) {
		this.clsIns = clsIns;
	}
	
	public MethodBean(int id, int taskId, int orderId, int paramType,
			String method, String notes, String parameters) {
		super();
		this.id = id;
		task_id = taskId;
		order_id = orderId;
		param_type = paramType;
		this.method = method;
		this.notes = notes;
		this.parameters = parameters;
	}
	public int getTask_id() {
		return task_id;
	}
	public void setTask_id(int taskId) {
		task_id = taskId;
	}
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int orderId) {
		order_id = orderId;
	}
	public int getParam_type() {
		return param_type;
	}
	public void setParam_type(int paramType) {
		param_type = paramType;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getParameters() {
		return parameters;
	}
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	public int getId() {
		return id;
	} 
	
}
