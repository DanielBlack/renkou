package prclqz.DAO.Bean;

public class MainOperationHistoryBean {
	private int id;
	private int type;
	private long time;
	private String value;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public long getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public MainOperationHistoryBean(int id, int type, long time, String value) {
		super();
		this.id = id;
		this.type = type;
		this.time = time;
		this.value = value;
	}
	
}
