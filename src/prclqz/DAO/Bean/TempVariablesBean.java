package prclqz.DAO.Bean;

import java.util.Map;

import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.DuiJiGuSuanFa;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.Policy;

/**
 * 全局变量类——主类，还有副类 是BabiesBornBean
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class TempVariablesBean {
	//调整方式时机——目前为普二
	public String adjustType;
	
	//those below must be coherent with the database
	//非农时机 1-3
	public int feiNongTime1,feiNongTime2,feiNongTime3,
	//迁移程度，实现程度，释放模式，年份，省份
	QY,ZCimplement,SFMS,year,province;
	
	//农业时机1-3，农业政策1-3
	public int nongYeTime1,nongYeTime2,nongYeTime3,
			  nongYePolicy1,nongYePolicy2,nongYePolicy3;
	
	//those below are not related to the database
	//实现比
	public double implRate;
	
	public int cshLevel = 0;
	public int SQY = 0;
	public DuiJiGuSuanFa TJ;
	//非农政策1-3
	public int feiNongPolicy1,feiNongPolicy2,feiNongPolicy3;
	//现行政策生育率
	public double nowN1D2C,nowN2D1C,nowNNFC,nowN1D2X,nowN2D1X,nowNNFX,DDFR;
	//未来政策生育率
	public double N1D2C,N2D1C,NNFC,N1D2X,N2D1X,NNFX;
	//调整时机
	public int policyTime;
	public Policy policy;
	
	//分婚配模式按政策生育的孩子数,需要传递给二级子程序的参数
	public Map<HunpeiField, double[]> zn;
	public BabiesBornBean babiesBorn;
	public CX cx;
	public NY ny;
	public int cxI;

	//DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育
	public double DqPlyBorn,XFNPlyBorn,CFNPlyBorn,XNYPlyBorn,CNYPlyBorn;
	
	//CSH水平2
	public double CSHLevel2;
	//CSH水平1
	public double CSHLevel1;
	
	//现政比1
	public double nowPolicyRate1;
		
	//地区政策TFR
	public double dqPolicyTFR;
	//地区政策TFR1
	public double dqPolicyTFR1;
	//可实现TFR0
	public double implableTFR0;
	//全国可实现TFR0
	public double nationalImplableTFR0;
	//全国政策TFR0
	public double nationalPlyTFR0;
	//MITFR
	public double MITFR;
	//N1D2N,N2D1N,NNFN
	public double N1D2N,N2D1N,NNFN;
	//超生S
	public double overBirthS;
	//S堆积
	public double S_duiji;
	
	//public double NMDFR,NFDMR,NNR;
	public double NMDFR,NFDMR,NNR;
	
	//农村农业独子父，农村农业独子母，城镇农业，城镇非农，农村非农……
	public Map<CX,Map<NY,Double>> singleSonFaCN,singleSonMaCN;
	//S独子父，S独子母
	public double S_singleSonFa,S_singleSonMa;
	//农村农业特扶父，农村农业特扶母，城镇农业，城镇非农，农村非农……
	public Map<CX,Map<NY,Double>> teFuFaCN,teFuMaCN;
	
	
	//农村非农TFR,农村农业TFR,
	public Map<CX,Map<NY,Double>> CNTFR;
	//非农实婚TFR0,非农实现TFR0,农村实婚TFR0,农村实现TFR0,农业实婚TFR0,农业实现TFR0 农村TFR0
	public Map<NY,Double> nyPolicyTFR0,nyImplTFR0,nyShiHunTFR0;
	//城镇-农村政策……
	public Map<CX,Double> cxPolicyTFR0,cxImplTFR0,cxShiHunTFR0;
	//地区政策TFR0,地区可实现TFR0,地区实婚TFR0
	public double dqPolicyTFR0,dqImplTFR0,dqShiHunTFR0;
	//地区超生TFR,CSH水平，FNRK 	NYRK CRK, XRK
	public double dqOverBrithTFR,CSHLevel,FNRK,NYRK,CRK,XRK;
	
	//婚内TFR
	public Map<CX,Map<NY,Double>> CNHunneiTFR;
	public Map<NY,Double> nyHunneiTFR;
	public Map<CX,Double> cxHunneiTFR;
	public double dqHunneiTFR;
	
	//出生人口,出生人口M,出生人口F
	public double bornPopulation,bornPopulationM,bornPopulationF;
	
	//
	public double EM0,EF0,SDQQY,DQQY,CEM0,XEM0,CEF0,XEF0;
	
	//DQ死亡人口M,DQ死亡人口F,C死亡人口M,C死亡人口F,X死亡人口M,X死亡人口F
	public double dqDeathM,dqDeathF,cDeathM,cDeathF,xDeathM,xDeathF;
	
	//QXM
	public double[] QXM,QXF;
	
	//MINLM,MINLF
	public double MINLM,MINLF;
	
	//总人口T,总人口M,总人口F
	public double Popl_T,Popl_M,Popl_F;
	//年中人口，新增人口，人口增长
	public double nianzhongRK,xinzengRK,RKzengzhang;
	
	//T少年,T劳年,T青劳,T老年,T高龄,T老年M,T高龄M,T老年F,T高龄F
	public double T_teen,T_labor,T_younglabor,T_old,T_high,T_oldM,T_highM,T_oldF,T_highF;
	
	//育龄F15,婚育M15,育龄F20,婚育M20,长寿M,长寿F
	public double yuLing_F15,hunYu_M15,yuLing_F20,hunYu_M20,changshou_M,changshou_F;
	
	//长寿M0,长寿F0	
	public double changshou_M0,changshou_F0;
	
	//T婴幼儿,XQ,XX,CZ,GZ,DX,JYM,JYF
	public double T_yingyouer,XQ,XX,CZ,GZ,DX,JYM,JYF;
	
	//GB劳前,GB劳龄M,GB劳龄F,GB青劳,GB中劳M,GB中龄F,GB劳后M,GB劳后F,G劳年M,G老年M,G少年,G劳年F,G老年F
	public double GB_laoqian,GB_laolingM,GB_laolingF,GB_qinglao,GB_zhonglaoM,GB_zhonglingF,
		GB_laohouM,GB_laohouF,GB_labornianM,GB_oldnianM,GB_shounian,G_labornianF,G_oldnianF;
	
	// NLZ,人口增长L
	public double NLZ,RKzengZhangL;
	
	//净迁入RK
	public double inMigRK;
	//死亡人口M,死亡人口F,死亡人口
	public double deathM,deathF,death;
	
	//TODO ST AV
	public double ST,AV;
	
	//土地MJ=分省代码.土地面积		&&单位:千公顷
	//耕地MJ=分省代码.耕地06			&&单位:千公顷
	//水资源=分省代码.水资源均量		&&单位:亿立方米
	public double landMJ,fieldMJ,waterS;
/**************************************************************************/
	
	public double getImplRate() {
		return implRate;
	}

	public void setImplRate(double implRate) {
		this.implRate = implRate;
	}
	public CX getCx() {
		return cx;
	}

	public void setCx(CX cx) {
		this.cx = cx;
	}

	public NY getNy() {
		return ny;
	}

	public void setNy(NY ny) {
		this.ny = ny;
	}
	public double getDqPlyBorn() {
		return DqPlyBorn;
	}

	public void setDqPlyBorn(double dqPlyBorn) {
		DqPlyBorn = dqPlyBorn;
	}

	public double getXFNPlyBorn() {
		return XFNPlyBorn;
	}

	public void setXFNPlyBorn(double xFNPlyBorn) {
		XFNPlyBorn = xFNPlyBorn;
	}

	public double getCFNPlyBorn() {
		return CFNPlyBorn;
	}

	public void setCFNPlyBorn(double cFNPlyBorn) {
		CFNPlyBorn = cFNPlyBorn;
	}

	public double getXNYPlyBorn() {
		return XNYPlyBorn;
	}

	public void setXNYPlyBorn(double xNYPlyBorn) {
		XNYPlyBorn = xNYPlyBorn;
	}

	public double getCNYPlyBorn() {
		return CNYPlyBorn;
	}

	public void setCNYPlyBorn(double cNYPlyBorn) {
		CNYPlyBorn = cNYPlyBorn;
	}

	public BabiesBornBean getBabiesBorn() {
		return babiesBorn;
	}

	public void setBabiesBorn(BabiesBornBean babiesBorn) {
		this.babiesBorn = babiesBorn;
	}

	public Map<HunpeiField, double[]> getZn() {
		return zn;
	}

	public void setZn(Map<HunpeiField, double[]> zn) {
		this.zn = zn;
	}

	
	
	public double getNMDFR() {
		return NMDFR;
	}

	public void setNMDFR(double nMDFR) {
		NMDFR = nMDFR;
	}

	public double getNFDMR() {
		return NFDMR;
	}

	public void setNFDMR(double nFDMR) {
		NFDMR = nFDMR;
	}

	public double getNNR() {
		return NNR;
	}

	public void setNNR(double nNR) {
		NNR = nNR;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public int getPolicyTime() {
		return policyTime;
	}

	public void setPolicyTime(int policyTime) {
		this.policyTime = policyTime;
	}

	public double getNowN1D2C() {
		return nowN1D2C;
	}

	public void setNowN1D2C(double nowN1D2C) {
		this.nowN1D2C = nowN1D2C;
	}

	public double getNowN2D1C() {
		return nowN2D1C;
	}

	public void setNowN2D1C(double nowN2D1C) {
		this.nowN2D1C = nowN2D1C;
	}

	public double getNowNNFC() {
		return nowNNFC;
	}

	public void setNowNNFC(double nowNNFC) {
		this.nowNNFC = nowNNFC;
	}

	public double getNowN1D2X() {
		return nowN1D2X;
	}

	public void setNowN1D2X(double nowN1D2X) {
		this.nowN1D2X = nowN1D2X;
	}

	public double getNowN2D1X() {
		return nowN2D1X;
	}

	public void setNowN2D1X(double nowN2D1X) {
		this.nowN2D1X = nowN2D1X;
	}

	public double getNowNNFX() {
		return nowNNFX;
	}

	public void setNowNNFX(double nowNNFX) {
		this.nowNNFX = nowNNFX;
	}

	public double getDDFR() {
		return DDFR;
	}

	public void setDDFR(double dDFR) {
		DDFR = dDFR;
	}

	public double getN1D2C() {
		return N1D2C;
	}

	public void setN1D2C(double n1d2c) {
		N1D2C = n1d2c;
	}

	public double getN2D1C() {
		return N2D1C;
	}

	public void setN2D1C(double n2d1c) {
		N2D1C = n2d1c;
	}

	public double getNNFC() {
		return NNFC;
	}

	public void setNNFC(double nNFC) {
		NNFC = nNFC;
	}

	public double getN1D2X() {
		return N1D2X;
	}

	public void setN1D2X(double n1d2x) {
		N1D2X = n1d2x;
	}

	public double getN2D1X() {
		return N2D1X;
	}

	public void setN2D1X(double n2d1x) {
		N2D1X = n2d1x;
	}

	public double getNNFX() {
		return NNFX;
	}

	public void setNNFX(double nNFX) {
		NNFX = nNFX;
	}

	public int getFeiNongPolicy1() {
		return feiNongPolicy1;
	}

	public void setFeiNongPolicy1(int feiNongPolicy1) {
		this.feiNongPolicy1 = feiNongPolicy1;
	}

	public int getFeiNongPolicy2() {
		return feiNongPolicy2;
	}

	public void setFeiNongPolicy2(int feiNongPolicy2) {
		this.feiNongPolicy2 = feiNongPolicy2;
	}

	public int getFeiNongPolicy3() {
		return feiNongPolicy3;
	}

	public void setFeiNongPolicy3(int feiNongPolicy3) {
		this.feiNongPolicy3 = feiNongPolicy3;
	}

	public DuiJiGuSuanFa getTJ() {
		return TJ;
	}

	public void setTJ(DuiJiGuSuanFa tJ) {
		TJ = tJ;
	}

	public int getSQY() {
		return SQY;
	}

	public void setSQY(int sQY) {
		SQY = sQY;
	}

	public int getCshLevel() {
		return cshLevel;
	}

	public void setCshLevel(int cshLevel) {
		this.cshLevel = cshLevel;
	}

	public int taskId;


	public int getFeiNongTime1() {
		return feiNongTime1;
	}

	public void setFeiNongTime1(int feiNongTime1) {
		this.feiNongTime1 = feiNongTime1;
	}



	public int getFeiNongTime2() {
		return feiNongTime2;
	}

	public void setFeiNongTime2(int feiNongTime2) {
		this.feiNongTime2 = feiNongTime2;
	}


	public int getFeiNongTime3() {
		return feiNongTime3;
	}

	public void setFeiNongTime3(int feiNongTime3) {
		this.feiNongTime3 = feiNongTime3;
	}

	public int getQY() {
		return QY;
	}

	public void setQY(int qY) {
		QY = qY;
	}

	public int getZCimplement() {
		return ZCimplement;
	}

	public void setZCimplement(int zCimplement) {
		ZCimplement = zCimplement;
	}

	public int getSFMS() {
		return SFMS;
	}

	public void setSFMS(int sFMS) {
		SFMS = sFMS;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getProvince() {
		return province;
	}

	public void setProvince(int province) {
		this.province = province;
	}



	public int getTaskId() {
		return taskId;
	}

	public void setPlyBorn(double DqPlyBorn,double XFNPlyBorn,double CFNPlyBorn,double XNYPlyBorn,double CNYPlyBorn){
		this.DqPlyBorn = DqPlyBorn;
		this.XFNPlyBorn = XFNPlyBorn;
		this.CFNPlyBorn = CFNPlyBorn;
		this.XNYPlyBorn = XNYPlyBorn;
		this.CNYPlyBorn = CNYPlyBorn;
	}
	public TempVariablesBean(int feiNongPolicy1, int feiNongTime1,
			int feiNongPolicy2, int feiNongTime2, int feiNongPolicy3,
			int feiNongTime3, int qY, int zCimplement, int sFMS, int year,
			int province,  int taskId) {
		super();
		this.feiNongPolicy1 = feiNongPolicy1;
		this.feiNongTime1 = feiNongTime1;
		this.feiNongPolicy2 = feiNongPolicy2;
		this.feiNongTime2 = feiNongTime2;
		this.feiNongPolicy3 = feiNongPolicy3;
		this.feiNongTime3 = feiNongTime3;
		QY = qY;
		ZCimplement = zCimplement;
		SFMS = sFMS;
		this.year = year;
		this.province = province;
		this.taskId = taskId;
	}

	
}
