package prclqz.DAO.Bean;

import java.util.Map;

import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.NY;
import prclqz.lib.EnumMapTool;

/**
 * 全局变量类——副类，主要和生育相关
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class BabiesBornBean {
// 各类夫妇分年龄生育的孩子数 计算结果 全局变量
public double DDFR1,NMDFFR1,NFDMFR1,NNFR1,
	           DDFR2,NMDFFR2,NFDMFR2,NNFR2;
public double A, DB, B2,B3 , B4;
public double DD1, NMDF1 , NFDM1, NN1,
		       DD2, NMDF2 , NFDM2, NN2;
// 堆积生育释放法 计算 结果全局变量
// 单放B2,非放B2,单独B2
public double sglRlsB2,feiRlsB2,singleB2;
//合JTFR,双DTFR,双NTFR,单DTFR
public double sum_JTFR,dbl_DTFR,dbl_NTFR,sgl_DTFR;
//婚内TFR
public double marriageTFR;
//农村(城镇)农业(非农)  一孩，单放B2，非放B2,婚内TFR,单独堆积，双非堆积
//农村农业一孩 ，农村农业单方B2，农村农业非放B2,NCNY婚内TFR,农村农业单独堆积，农村农业双非堆积
public Map<CX,Map<NY,Double>> singleChildCN,singleRlsB2CN,feiRlsB2CN,marriageTFRCN,singleDJCN,shuangFeiDJCN;


public double getSum_JTFR() {
	return sum_JTFR;
}
public void setSum_JTFR(double sumJTFR) {
	sum_JTFR = sumJTFR;
}
public double getDbl_DTFR() {
	return dbl_DTFR;
}
public void setDbl_DTFR(double dblDTFR) {
	dbl_DTFR = dblDTFR;
}
public double getDbl_NTFR() {
	return dbl_NTFR;
}
public void setDbl_NTFR(double dblNTFR) {
	dbl_NTFR = dblNTFR;
}
public double getSgl_DTFR() {
	return sgl_DTFR;
}
public void setSgl_DTFR(double sglDTFR) {
	sgl_DTFR = sglDTFR;
}
public double getSingleB2() {
	return singleB2;
}
public void setSingleB2(double singleB2) {
	this.singleB2 = singleB2;
}
public double getSglRlsB2() {
	return sglRlsB2;
}
public void setSglRlsB2(double sglRlsB2) {
	this.sglRlsB2 = sglRlsB2;
}
public double getFeiRlsB2() {
	return feiRlsB2;
}
public void setFeiRlsB2(double feiRlsB2) {
	this.feiRlsB2 = feiRlsB2;
}
public double getDDFR1() {
	return DDFR1;
}
public void setDDFR1(double dDFR1) {
	DDFR1 = dDFR1;
}
public double getNMDFFR1() {
	return NMDFFR1;
}
public void setNMDFFR1(double nMDFFR1) {
	NMDFFR1 = nMDFFR1;
}
public double getNFDMFR1() {
	return NFDMFR1;
}
public void setNFDMFR1(double nFDMFR1) {
	NFDMFR1 = nFDMFR1;
}
public double getNNFR1() {
	return NNFR1;
}
public void setNNFR1(double nNFR1) {
	NNFR1 = nNFR1;
}
public double getDDFR2() {
	return DDFR2;
}
public void setDDFR2(double dDFR2) {
	DDFR2 = dDFR2;
}
public double getNMDFFR2() {
	return NMDFFR2;
}
public void setNMDFFR2(double nMDFFR2) {
	NMDFFR2 = nMDFFR2;
}
public double getNFDMFR2() {
	return NFDMFR2;
}
public void setNFDMFR2(double nFDMFR2) {
	NFDMFR2 = nFDMFR2;
}
public double getNNFR2() {
	return NNFR2;
}
public void setNNFR2(double nNFR2) {
	NNFR2 = nNFR2;
}
public double getA() {
	return A;
}
public void setA(double a) {
	A = a;
}
public double getDB() {
	return DB;
}
public void setDB(double dB) {
	DB = dB;
}
public double getB2() {
	return B2;
}
public void setB2(double b2) {
	B2 = b2;
}
public double getB3() {
	return B3;
}
public void setB3(double b3) {
	B3 = b3;
}
public double getB4() {
	return B4;
}
public void setB4(double b4) {
	B4 = b4;
}
public double getDD1() {
	return DD1;
}
public void setDD1(double dD1) {
	DD1 = dD1;
}
public double getNMDF1() {
	return NMDF1;
}
public void setNMDF1(double nMDF1) {
	NMDF1 = nMDF1;
}
public double getNFDM1() {
	return NFDM1;
}
public void setNFDM1(double nFDM1) {
	NFDM1 = nFDM1;
}
public double getNN1() {
	return NN1;
}
public void setNN1(double nN1) {
	NN1 = nN1;
}
public double getDD2() {
	return DD2;
}
public void setDD2(double dD2) {
	DD2 = dD2;
}
public double getNMDF2() {
	return NMDF2;
}
public void setNMDF2(double nMDF2) {
	NMDF2 = nMDF2;
}
public double getNFDM2() {
	return NFDM2;
}
public void setNFDM2(double nFDM2) {
	NFDM2 = nFDM2;
}
public double getNN2() {
	return NN2;
}
public void setNN2(double nN2) {
	NN2 = nN2;
}
public BabiesBornBean(double dDFR1, double nMDFFR1, double nFDMFR1,
		double nNFR1, double dDFR2, double nMDFFR2, double nFDMFR2,
		double nNFR2, double a, double dB, double b2, double b3, double b4,
		double dD1, double nMDF1, double nFDM1, double nN1, double dD2,
		double nMDF2, double nFDM2, double nN2) {
	super();
	DDFR1 = dDFR1;
	NMDFFR1 = nMDFFR1;
	NFDMFR1 = nFDMFR1;
	NNFR1 = nNFR1;
	DDFR2 = dDFR2;
	NMDFFR2 = nMDFFR2;
	NFDMFR2 = nFDMFR2;
	NNFR2 = nNFR2;
	A = a;
	DB = dB;
	B2 = b2;
	B3 = b3;
	B4 = b4;
	DD1 = dD1;
	NMDF1 = nMDF1;
	NFDM1 = nFDM1;
	NN1 = nN1;
	DD2 = dD2;
	NMDF2 = nMDF2;
	NFDM2 = nFDM2;
	NN2 = nN2;
	
	//创建重要对象
	singleChildCN = EnumMapTool.createSingleChild();
	singleRlsB2CN = EnumMapTool.createSingleChild();
	feiRlsB2CN = EnumMapTool.createSingleChild();
	marriageTFRCN = EnumMapTool.createSingleChild();
	singleDJCN = EnumMapTool.createSingleChild();
	shuangFeiDJCN = EnumMapTool.createSingleChild();
}
public double getMarriageTFR() {
	return marriageTFR;
}
public void setMarriageTFR(double marriageTFR) {
	this.marriageTFR = marriageTFR;
}
public Map<CX, Map<NY, Double>> getSingleChildCN() {
	return singleChildCN;
}
public void setSingleChildCN(Map<CX, Map<NY, Double>> singleChildCN) {
	this.singleChildCN = singleChildCN;
}
public Map<CX, Map<NY, Double>> getSingleRlsB2CN() {
	return singleRlsB2CN;
}
public void setSingleRlsB2CN(Map<CX, Map<NY, Double>> singleRlsB2CN) {
	this.singleRlsB2CN = singleRlsB2CN;
}
public Map<CX, Map<NY, Double>> getFeiRlsB2CN() {
	return feiRlsB2CN;
}
public void setFeiRlsB2CN(Map<CX, Map<NY, Double>> feiRlsB2CN) {
	this.feiRlsB2CN = feiRlsB2CN;
}
public Map<CX, Map<NY, Double>> getMarriageTFRCN() {
	return marriageTFRCN;
}
public void setMarriageTFRCN(Map<CX, Map<NY, Double>> marriageTFRCN) {
	this.marriageTFRCN = marriageTFRCN;
}
public Map<CX, Map<NY, Double>> getSingleDJCN() {
	return singleDJCN;
}
public void setSingleDJCN(Map<CX, Map<NY, Double>> singleDJCN) {
	this.singleDJCN = singleDJCN;
}
public Map<CX, Map<NY, Double>> getShuangFeiDJCN() {
	return shuangFeiDJCN;
}
public void setShuangFeiDJCN(Map<CX, Map<NY, Double>> shuangFeiDJCN) {
	this.shuangFeiDJCN = shuangFeiDJCN;
}

}
