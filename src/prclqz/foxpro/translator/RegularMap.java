package prclqz.foxpro.translator;

import java.util.HashMap;

/**
 * 翻译最后一步所适用的正则表达式表
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class RegularMap
{

	public static HashMap< String , String > getMap ()
	{
		HashMap< String , String > map = new HashMap< String , String >();
		
		map.put( "ARFND1\\( ([\\w\\+\\-]+) \\)" , "ARFND1[$1]" );
		map.put( "ARFND1\\(((RECNO\\(\\))*[\\w\\+\\-]+)\\)" , "ARFND1\\[$1\\]" );
		map.put( "RECNO\\(\\)" , "X" );
		map.put( "ARFND1\\[0\\]\\[X\\]" , "ARFND1[X]" );
		map.put( "\\(\\W*Nongchun" , "( CX.Nongchun" );
		map.put( "\\(\\W*Chengshi" , "( CX.Chengshi" );
		map.put( "\\(\\W*(Nongye)" , "( NY.$1" );
		map.put( "\\(\\W*(Feinong)" , "( NY.$1" );
		map.put( "\\(\\W*(Male)" , "( XB.$1" );
		map.put( "\\(\\W*(Female)" , "( XB.$1" );
//		map.put( "" , "" );
//		map.put( "" , "" );
		return map;
	}

}
