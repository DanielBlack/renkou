package prclqz.foxpro.translator.interf;

import java.util.ArrayList;
import prclqz.foxpro.translator.Token;
import prclqz.foxpro.translator.FoxproParseTree;

public interface IParser
{
	/**
	 * seperate ecah line of foxpro code, for testing
	 * @param codes
	 * @return
	 */
	public int[][] genLines( ArrayList < Token > codes );
	
	/**
	 * parse tokens and translate into java codes
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public FoxproParseTree parse( ArrayList < Token > codes ) throws Exception;
}
