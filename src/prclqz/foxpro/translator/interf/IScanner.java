package prclqz.foxpro.translator.interf;

import java.util.ArrayList;

import prclqz.foxpro.translator.Token;

public interface IScanner
{
	public ArrayList < Token > scan( String input ) throws Exception;
}
