package prclqz.foxpro.translator.interf;

import java.util.ArrayList;
import java.util.List;

import prclqz.foxpro.translator.FoxproParseTree;

public interface IFoxpro2Java
{
	public List< String > foxpro2Java( FoxproParseTree tree ) throws Exception;
}
