package prclqz.foxpro.translator;

import static prclqz.foxpro.translator.enums.TokenType.*;
import static prclqz.foxpro.translator.simpleImpl.SimpleParserSyntax.*;

import java.util.ArrayList;

import prclqz.foxpro.translator.Token;
import prclqz.foxpro.translator.simpleImpl.SimpleParserSyntax;

/**
 * Define a foxpro parse tree
 *  and implement a display method
 * @author prclqz@zju.edu.cn
 *
 */
public class FoxproParseTree
{

	public  SimpleParserSyntax type;
	public  Token terminal;
	public  ArrayList < FoxproParseTree > children;

	public FoxproParseTree( SimpleParserSyntax commands, Token terminal){
		this.type = commands;
		this.terminal = terminal;
		children = new ArrayList < FoxproParseTree > ( );
	}
	
	public String toString(){
		return display( this, 0);
	}
	
	public static void displayTree ( FoxproParseTree tree )
	{
		System.out.print ( display( tree, 0 ) );
	}
	
	private static String display ( FoxproParseTree tree , int level )
	{
		String rs = "";
		for(int i=0; i<level;i++ )
			rs +="\t";
//			System.out.print("   ");
		
		if( tree.type == ternimal )
			rs += tree.terminal+"\n";
//			System.out.println( tree.terminal );
		else{
//			System.out.println( tree.type );
			rs += tree.type+"\n";
			for(int i=0; i< tree.children.size ( ) ;i++)
				rs += display ( tree.children.get ( i ) , level+1 );
		}
		return rs;
	}

	/**
	 * testing case
	 * @param args
	 */
	public static void main ( String [ ] args )
	{
		FoxproParseTree tree = new FoxproParseTree ( commands , null );
		FoxproParseTree child1 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child2 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child3 = new FoxproParseTree ( assign_expression , null );
		tree.children.add ( child1 );
		tree.children.add ( child2 );
		tree.children.add ( child3 );
		
		FoxproParseTree child11 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child12 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child21 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child31 = new FoxproParseTree ( assign_expression , null );
		FoxproParseTree child32 = new FoxproParseTree ( assign_expression , null );
		
		child1.children.add ( child11 );
		child1.children.add ( child12 );
		child2.children.add ( child21 );
		child3.children.add ( child31 );
		child3.children.add ( child32 );
		
		FoxproParseTree t1 = new FoxproParseTree ( ternimal , new Token ( number , 100 , null ) );
		FoxproParseTree t2 = new FoxproParseTree ( ternimal , new Token ( number , 101 , null ) );
		FoxproParseTree t3 = new FoxproParseTree ( ternimal , new Token ( number , 102 , null ) );
		FoxproParseTree t4 = new FoxproParseTree ( ternimal , new Token ( number , 103 , null ) );
		FoxproParseTree t5 = new FoxproParseTree ( ternimal , new Token ( number , 104 , null ) );
		FoxproParseTree t6 = new FoxproParseTree ( ternimal , new Token ( number , 105 , null ) );
		FoxproParseTree t7 = new FoxproParseTree ( ternimal , new Token ( string , 0 , "haha" ) );
		
		child11.children.add ( t1 );
		child12.children.add ( t2 );
		child21.children.add ( t3 );
		child21.children.add ( t4 );
		child21.children.add ( t5 );
		child31.children.add ( t6 );
		child32.children.add ( t7 );

		displayTree( tree );
	}


}
