package prclqz.foxpro.translator;

import static prclqz.core.enumLib.CX.Chengshi;
import static prclqz.core.enumLib.NY.Nongye;
import static prclqz.core.enumLib.XB.Male;
import java.util.HashMap;
import prclqz.core.enumLib.Abstract;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.Summary;
import prclqz.core.enumLib.XB;

/**
 * 	&& 定义各种字段名字
Q=IIF(ND0<2101,'Q'+STR(ND0,4), 'Q2100')    &&死亡概率准备

TND1='T'+STR(ND0-1,4)  && 上一年  ND:年度
MND1='M'+STR(ND0-1,4)
FND1='F'+STR(ND0-1,4) 

TND2='T'+STR(ND0,4)    &&当年
MND2='M'+STR(ND0,4)
FND2='F'+STR(ND0,4)

QYMND2='M'+IIF(ND0<=2100,STR(ND0,4),'2100') &&年度迁移
QYFND2='F'+IIF(ND0<=2100,STR(ND0,4),'2100')

BND2='B'+STR(ND0,4)     &&分年龄孩子数

QCM='QCM'-IIF(ND0<=2100,STR(ND0,4),'2100') &&迁出M
QCF='QCF'-IIF(ND0<=2100,STR(ND0,4),'2100')
QRM='QRM'-IIF(ND0<=2100,STR(ND0,4),'2100') &&迁入M
QRF='QRF'-IIF(ND0<=2100,STR(ND0,4),'2100')

BX='B'+STR(ND0,4)	&&分年龄生育孩子数
FX='F'+STR(ND0,4)	&&分年龄妇女人数
 */

/**
 * 在 HashMap的基础上，防止覆盖错误！——原本HashMap当然也有，但不好用！
 * 可用继承HashMap来实现，但这样就没法使用异常机制……
 * @author prclqz@zju.edu.cn
 *
 */
public class VarMapData {
	
	public static HashMap<String, String> getVarMap() throws Exception{
		
		
		/********************
		 * 表字段 TODO mark
		 * 
		 * 现有：
		 * 婚配预测表 HunpeiFiled   地区xx子女
		 * 生育、超生预测表(分龄生育，分龄超生，政策生育) Babies 地区xx生育，地区xx超生，地区分龄政策生育
		 * 人口预测,丧子人口预测，Year ！！！！地区xx分龄，全国xx分龄！！！！，迁移，死亡，父母(特扶父母)，一孩(一孩父母) 
		 * 生育意愿  global
		 * 死亡概率  global
		 * 夫妇及子女表结构 Couple  地区xx夫妇
		 * 政策生育率  PolicyBirth 地区政策生育率
		 * 分婚配模式生育孩次数 BabiesBorn  地区生育孩次
		 * 摘要 Summary 地区xx摘要
		 * 地区生育率与迁移摘要 SumBM 各地区生育率与迁移摘要
		 * 生育政策模拟摘要   Abstract 生育政策模拟摘要
		 * 分年龄超生生育率-仿真结果_妇女分年龄生育率预测   Year 地区超生生育率
		 *******************/
		// 婚配预测表 
		putHunpei();
		
		//分年龄生育预测,超生预测,政策生育
		putBabies();
		
		//分年龄人口预测,丧子人口预测
		putPredict();
		
		//生育意愿
		putBirthWill();
		
		//死亡概率预测
		putDeathGL();

		//夫妇及子女表结构
		putCouple();
		
		//政策及政策生育率
		putPolicyBirth();
		
		//生育孩次数
		putBabiesBorn();
		
		//概要
		putSummary();
		
		//地区生育率与迁移摘要
		putSumBM();
		
		//生育政策模拟摘要
		putAbstract();
		
		//分年龄超生生育率-仿真结果_妇女分年龄生育率预测 
		putBirthRatePredict();
		
		/***************
		 * 全局变量
		 ***************/
		putGlobals();
		
		/***************
		 * 局部变量 
		 ***************/
		putLocals();
		
		return varMap.getHashMap ( );
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	private static void putLocals () throws Exception
	{
		varMap.put("S单独堆积", "s_singleDJ");
		varMap.put("S已2单独", "s_yi2Single");
		
		varMap.put("堆积单独", "duijiSingle");
		varMap.put("单独已2", "duijiYi2");
		varMap.put("堆积双非", "duijiShuangfei");
		varMap.put("双非已2", "shuangfeiYi2");
		varMap.put("母亲MINL", "momMINL");
		varMap.put("母亲MANL", "momMANL");
		varMap.put("新生一孩", "yiHai");
		varMap.put("妻子RS", "wifeRS");
		varMap.put("AR独转非", "ARDuToNot");
		varMap.put("D独转非", "DDuToNot");
		varMap.put("独转非母亲RS", "DuToNotMomRS");
		varMap.put("净迁入RK", "inMigRK");
		
		varMap.put("RECNO()", "X");
		varMap.put("CX", "cx");
		varMap.put("&AR突释模式(ND0-调整时机+1)", "TuShiAR[year - tempVar.getPolicyTime()+ 1]");
		varMap.put("&AR缓释模式(ND0-调整时机+1)", "HuanShiAR[year - tempVar.getPolicyTime()+ 1]");
//		varMap.put("", "");
//		varMap.put("", "");
//		varMap.put("", "");
//		varMap.put("", "");
		
		//数组转换——下次如还需要就用一个For循环完成TODO 用正则
//		varMap.put("ARFR(X1,2)", "ARFR[2][X1]");
//		varMap.put("ARFR(X2,2)", "ARFR[2][X2]");
//		varMap.put("ARS0(X+1,2)", "ARS0[2][X+1]");
	}

	private static void putGlobals () throws Exception
	{
// single variables
		
		// MethodBean和常用的
		varMap.put("ND0", "year");
		varMap.put("调整时机", "tempVar.getPolicyTime()");
		varMap.put("MIN(X)", "9999|X < MIX|X");  //TODO !!!! remember to use 初始值|赋值条件|赋值
		varMap.put("MAX(X)",    "0|X > MIX|X");  //TODO !!!! important! without this calculate MAX(X)... can not be translated!
		varMap.put("MAX(FR1)",    "0 | zn.get(FR1)[X] > MAFR1 | zn.get(FR1)[X]");  //TODO !!!! important! without this calculate MAX(X)... can not be translated!
		varMap.put("MAX(FR2)",    "0 | zn.get(FR2)[X] > MAFR2 | zn.get(FR2)[X]");  //TODO !!!! important! without this calculate MAX(X)... can not be translated!
		varMap.put("MAX(Y)",    	"0 | zn.get(FR2)[X] > MAFR2 | zn.get(FR2)[X]");  //TODO !!!! important! without this calculate MAX(X)... can not be translated!
		
		varMap.put("XBB", "xbb.getXbb ( )");
		
		//BabiesBornBean
		varMap.put("DB", "bBornBean.DB");
		varMap.put("NMDF1", "bBornBean.NMDF1");
		varMap.put("NFDM1", "bBornBean.NFDM1");
		varMap.put("单独B2", "bBornBean.singleB2");
		varMap.put("NN1", "bBornBean.NN1");
		varMap.put("NN2", "bBornBean.NN2");
		varMap.put("B2", "bBornBean.B2");
		varMap.put("B3", "bBornBean.B3");
		varMap.put("B4", "bBornBean.B4");
		varMap.put("A", "bBornBean.A");
		
		//TempVariableBean
		//CSH水平2
		varMap.put("CSH水平2", "tempVar.CSHLevel2");
		//CSH水平1
		varMap.put("CSH水平1", "tempVar.CSLevel1");
		
		//现政比1
		varMap.put("现政比1", "tempVar.nowPolicyRate1");
		//地区政策TFR
		varMap.put("地区政策TFR", "tempVar.dqPolicyTFR");
		//地区政策TFR1
		varMap.put("地区政策TFR1", "tempVar.dqPolicyTFR1");
		//可实现TFR0
		varMap.put("可实现TFR0", "tempVar.implableTFR0");
		//实现比
		varMap.put("实现比", "tempVar.implRate");
		//MITFR
		varMap.put("MITFR", "tempVar.MITFR");
		//超生S
		varMap.put("超生S", "tempVar.overBirthS");
		//S堆积
		varMap.put("S堆积", "tempVar.S_duiji");
		//N1D2N,N2D1N,NNFN
		varMap.put("N1D2N", "tempVar.N1D2N");
		varMap.put("N2D1N", "tempVar.N2D1N");
		varMap.put("NNFN", "tempVar.NNFN");
		//现行政策生育率 TOD
		varMap.put("现行N1D2C", "tempVar.nowN1D2C");
		varMap.put("现行N2D1C", "tempVar.nowN2D1C");
		varMap.put("现行NNFC", "tempVar.nowNNFC");
		varMap.put("现行N1D2X", "tempVar.nowN1D2X");
		varMap.put("现行N2D1X", "tempVar.nowN2D1X");
		varMap.put("现行NNFX", "tempVar.nowNNFX");
		varMap.put("DDFR", "tempVar.DDFR");

		//未来政策生育率
		varMap.put("N1D2C", "tempVar.N1D2C");
		varMap.put("N2D1C", "tempVar.N2D1C");
		varMap.put("NNFC", "tempVar.NNFC");
		varMap.put("N1D2X", "tempVar.N1D2X");
		varMap.put("N2D1X", "tempVar.N2D1X");
		varMap.put("NNFX", "tempVar.NNFX");
		//public double NMDFR,NFDMR,NNR;
		varMap.put("NMDFR", "tempVar.NMDFR");
		varMap.put("NFDMR", "tempVar.NFDMR");
		varMap.put("NNR", "tempVar.NNR");
		
		//农村农业独子父，农村农业独子母，城镇农业，城镇非农，农村非农……
//		public Map<CX,Map<NY,Double>> singleSonFaCN,singleSonMaCN;
		//tempVar.singleSonFaCN.get ( Nongchun ).put( Nongye, 0.0);
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = c.getChinese ( )+n.getChinese ( )+"独子父"+"*PUT";
				String k2 = c.getChinese ( )+n.getChinese ( )+"独子母"+"*PUT";
				String v1 = "tempVar.singleSonFaCN.get ( "+c+" ).put( "+n+",";
				String v2 = "tempVar.singleSonMaCN.get ( "+c+" ).put( "+n+",";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = c.getChinese ( )+n.getChinese ( )+"独子父";
				String k2 = c.getChinese ( )+n.getChinese ( )+"独子母";
				String v1 = "tempVar.singleSonFaCN.get ( "+c+" ).get( "+n+")";
				String v2 = "tempVar.singleSonMaCN.get ( "+c+" ).get( "+n+")";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		
		//农村农业特扶父，农村农业特扶母，城镇农业，城镇非农，农村非农……
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = c.getChinese ( )+n.getChinese ( )+"特扶父"+"*PUT";
				String k2 = c.getChinese ( )+n.getChinese ( )+"特扶母"+"*PUT";
				String v1 = "tempVar.teFuFaCN.get ( "+c+" ).put( "+n+",";
				String v2 = "tempVar.teFuMaCN.get ( "+c+" ).put( "+n+",";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = c.getChinese ( )+n.getChinese ( )+"特扶父";
				String k2 = c.getChinese ( )+n.getChinese ( )+"特扶母";
				String v1 = "tempVar.teFuFaCN.get ( "+c+" ).get( "+n+")";
				String v2 = "tempVar.teFuMaCN.get ( "+c+" ).get( "+n+")";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		
		//tempVar 有put 类型
		//农村TFR0,农村非农TFR,农村农业TFR,
//		public Map<CX,Map<NY,Double>> CNTFR;
		for(CX c: CX.values ( ))
			for(NY n: NY.values ( )){
				String k = c.getChinese ( )+n.getChinese ( )+"TFR"+"*PUT";
				String v = "tempVar.CNTFR.get("+c+").put("+n+",";
				varMap.put ( k , v );
			}
		for(CX c: CX.values ( ))
			for(NY n: NY.values ( )){
				String k = c.getChinese ( )+n.getChinese ( )+"TFR";
				String v = "tempVar.CNTFR.get("+c+").get("+n+")";
				varMap.put ( k , v );
			}
		//非农实婚TFR0,非农实现TFR0,农村实婚TFR0,农村实现TFR0,农业实婚TFR0,农业实现TFR0
//		public Map<NY,Double> nyPolicyTFR0,nyImplTFR0,nyShiHunTFR0;
		for(NY n: NY.values ( )){
			String k1 = n.getChinese ( )+"政策TFR0"+"*PUT";
			String k2 = n.getChinese ( )+"实现TFR0"+"*PUT";
			String k3 = n.getChinese ( )+"实婚TFR0"+"*PUT";
			String v1 = "tempVar.nyPolicyTFR0.put("+n+",";
			String v2 = "tempVar.nyImplTFR0.put("+n+",";
			String v3 = "tempVar.nyShiHunTFR0.put("+n+",";
			
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
			varMap.put ( k3 , v3 );
		}
		for(NY n: NY.values ( )){
			String k1 = n.getChinese ( )+"政策TFR0";
			String k2 = n.getChinese ( )+"实现TFR0";
			String k3 = n.getChinese ( )+"实婚TFR0";
			String v1 = "tempVar.nyPolicyTFR0.get("+n+")";
			String v2 = "tempVar.nyImplTFR0.get("+n+")";
			String v3 = "tempVar.nyShiHunTFR0.get("+n+")";
			
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
			varMap.put ( k3 , v3 );
		}
		//城镇-农村政策……
//		public Map<CX,Double> cxPolicyTFR0,cxImplTFR0,cxShiHunTFR0;
		for(CX n: CX.values ( )){
			String k1 = n.getChinese ( )+"政策TFR0"+"*PUT";
			String k2 = n.getChinese ( )+"实现TFR0"+"*PUT";
			String k3 = n.getChinese ( )+"实婚TFR0"+"*PUT";
			String v1 = "tempVar.cxPolicyTFR0.put("+n+",";
			String v2 = "tempVar.cxImplTFR0.put("+n+",";
			String v3 = "tempVar.cxShiHunTFR0.put("+n+",";
			
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
			varMap.put ( k3 , v3 );
		}
		for(CX n: CX.values ( )){
			String k1 = n.getChinese ( )+"政策TFR0";
			String k2 = n.getChinese ( )+"实现TFR0";
			String k3 = n.getChinese ( )+"实婚TFR0";
			String v1 = "tempVar.cxPolicyTFR0.get("+n+")";
			String v2 = "tempVar.cxImplTFR0.get("+n+")";
			String v3 = "tempVar.cxShiHunTFR0.get("+n+")";
			
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
			varMap.put ( k3 , v3 );
		}
		//地区政策……
//		public double dqPolicyTFR0,dqImplTFR0,dqShiHunTFR0;
		varMap.put ( "地区政策TFR0" , "tempVar.dqPolicyTFR0" );
		varMap.put ( "地区实现TFR0" , "tempVar.dqImplTFR0" );
		varMap.put ( "地区实婚TFR0" , "tempVar.dqShiHunTFR0" );
		
		//地区超生TFR,CSH水平，FNRK 	NYRK  CRK XRK
//		public double dqOverBrithTFR,CSHLevel,FNRK,NYRK;
		varMap.put("地区超生TFR", "tempVar.dqOverBrithTFR");
		varMap.put("CSH水平", "tempVar.CSHLevel");
		varMap.put("FNRK", "tempVar.FNRK");
		varMap.put("NYRK", "tempVar.NYRK");
		varMap.put("CRK", "tempVar.CRK");
		varMap.put("XRK", "tempVar.XRK");
		
		
		
		//////////////////////////////////////////////////////
		//婚内TFR CN,cx,ny,dq
		for(CX c: CX.values ( ))
			for(NY n: NY.values ( )){
				String c_s = c==Chengshi?"CZ":"NC";
				String n_s = n==Nongye?"NY":"FN";
				
				String k = c_s+n_s+"婚内TFR"+"*PUT";
				String v = "tempVar.CNHunneiTFR.get("+c+").put("+n+",";
				varMap.put ( k , v );
			}
		for(CX c: CX.values ( ))
			for(NY n: NY.values ( )){
				String c_s = c==Chengshi?"CZ":"NC";
				String n_s = n==Nongye?"NY":"FN";
				
				String k = c_s+n_s+"婚内TFR";
				String v = "tempVar.CNHunneiTFR.get("+c+").get("+n+")";
				varMap.put ( k , v );
			}
		for(NY n: NY.values ( )){
			String k = n.getChinese ( )+"婚内TFR"+"*PUT";
			String v = "tempVar.nyHunneiTFR.put("+n+",";
			varMap.put ( k , v );
		}
		for(NY n: NY.values ( )){
			String k = n.getChinese ( )+"婚内TFR";
			String v = "tempVar.nyHunneiTFR.get("+n+")";
			varMap.put ( k , v );
		}
		for(CX n: CX.values ( )){
			String k = n.getChinese ( )+"婚内TFR"+"*PUT";
			String v = "tempVar.cxHunneiTFR.put("+n+",";
			varMap.put ( k , v );
		}
		for(CX n: CX.values ( )){
			String k = n.getChinese ( )+"婚内TFR";
			String v = "tempVar.cxHunneiTFR.get("+n+")";
			varMap.put ( k , v );
		}
		varMap.put("地区婚内TFR", "tempVar.dqHunneiTFR");
		
		///////////////////////////////////////////////////////////////
		
		//S独子父，S独子母
		varMap.put("S独子父", "tempVar.S_singleSonFa");
		varMap.put("S独子母", "tempVar.S_singleSonMa");

		//出生人口 M F
		varMap.put("出生人口", "tempVar.bornPopulation");
		varMap.put("出生人口M", "tempVar.bornPopulationM");
		varMap.put("出生人口F", "tempVar.bornPopulationF");
		
		varMap.put("EM0", "tempVar.EM0");
		varMap.put("EF0", "tempVar.EF0");
		
		//DQ死亡人口M,DQ死亡人口F,C死亡人口M,C死亡人口F,X死亡人口M,X死亡人口F
//		public double dqDeathM,dqDeathF,cDeathM,cDeathF,xDeathM,xDeathF;
		varMap.put("DQ死亡人口M", "tempVar.dqDeathM");
		varMap.put("DQ死亡人口F", "tempVar.dqDeathF");
		varMap.put("C死亡人口M", "tempVar.cDeathM");
		varMap.put("C死亡人口F", "tempVar.cDeathF");
		varMap.put("X死亡人口M", "tempVar.xDeathM");
		varMap.put("X死亡人口F", "tempVar.xDeathF");
		
		
		//总人口T,总人口M,总人口F
//		public double Popl_T,Popl_M,Popl_F;
		varMap.put("总人口T", "tempVar.Popl_T");
		varMap.put("总人口M", "tempVar.Popl_M");
		varMap.put("总人口F", "tempVar.Popl_F");
		
		//年中人口，新增人口，人口增长
//		public double nianzhongRK,xinzengRK,RKzengzhang;
		varMap.put("年中人口", "tempVar.nianzhongRK");
		varMap.put("新增人口", "tempVar.xinzengRK");
		varMap.put("人口增长", "tempVar.RKzengzhang");
		
		//T少年,T劳年,T青劳,T老年,T高龄,T老年M,T高龄M,T老年F,T高龄F
//		public double T_teen,T_labor,T_younglabor,T_old,T_high,T_oldM,T_highM,T_oldF,T_highF;
		varMap.put("T少年", "tempVar.T_teen");
		varMap.put("T劳年", "tempVar.T_labor");
		varMap.put("T青劳", "tempVar.T_younglabor");
		varMap.put("T老年", "tempVar.T_old");
		varMap.put("T高龄", "tempVar.T_high");
		varMap.put("T老年M", "tempVar.T_oldM");
		varMap.put("T高龄M", "tempVar.T_highM");
		varMap.put("T老年F", "tempVar.T_oldF");
		varMap.put("T高龄F", "tempVar.T_highF");
		
		//育龄F15,婚育M15,育龄F20,婚育M20,长寿M,长寿F
//		public double yuLing_F15,hunYu_M15,yuLing_F20,hunYu_M20,changshou_M,changshou_F;
		varMap.put("育龄F15", "tempVar.yuLing_F15");
		varMap.put("婚育M15", "tempVar.hunYu_M15");
		varMap.put("育龄F20", "tempVar.yuLing_F20");
		varMap.put("婚育M20", "tempVar.hunYu_M20");
//		varMap.put("长寿M", "tempVar.changshou_M");
//		varMap.put("长寿F", "tempVar.changshou_F");
		
		//长寿M0,长寿F0	
//		public double changshou_M0,changshou_F0;
		varMap.put("长寿M0", "tempVar.changshou_M0");
		varMap.put("长寿F0", "tempVar.changshou_F0");
		
		//T婴幼儿,XQ,XX,CZ,GZ,DX,JYM,JYF
//		public double T_yingyouer,XQ,XX,CZ,GZ,DX,JYM,JYF;
		varMap.put("T婴幼儿", "tempVar.T_yingyouer");
		varMap.put("XQ", "tempVar.XQ");
		varMap.put("XX", "tempVar.XX");
		varMap.put("CZ", "tempVar.CZ");
		varMap.put("GZ", "tempVar.GZ");
		varMap.put("DX", "tempVar.DX");
		varMap.put("JYM", "tempVar.JYM");
		varMap.put("JYF", "tempVar.JYF");
		
		//GB劳前,GB劳龄M,GB劳龄F,GB青劳,GB中劳M,GB中龄F,
//		GB_laoqian,GB_laolingM,GB_laolingF,GB_qinglao,GB_zhonglaoM,GB_zhonglingF,
		varMap.put("GB劳前", "tempVar.GB_laoqian");
		varMap.put("GB劳龄M", "tempVar.GB_laolingM");
		varMap.put("GB劳龄F", "tempVar.GB_laolingF");
		varMap.put("GB青劳", "tempVar.GB_qinglao");
		varMap.put("GB中劳M", "tempVar.GB_zhonglaoM");
		varMap.put("GB中龄F", "tempVar.GB_zhonglingF");
		
		//GB劳后M,GB劳后F,G劳年M,G老年M,G少年,G劳年F,G老年F
//		GB_laohouM,GB_laohouF,GB_labornianM,GB_oldnianM,GB_shounian,G_labornianF,G_oldnianF;
		varMap.put("GB劳后M", "tempVar.GB_laohouM");
		varMap.put("GB劳后F", "tempVar.GB_laohouF");
		varMap.put("G劳年M", "tempVar.GB_labornianM");
		varMap.put("G老年M", "tempVar.GB_oldnianM");
		varMap.put("G少年", "tempVar.GB_shounian");
		varMap.put("G劳年F", "tempVar.G_labornianF");
		varMap.put("G老年F", "tempVar.G_oldnianF");
		
		// NLZ
//		public double NLZ;
		varMap.put("NLZ", "tempVar.NLZ");
		varMap.put("人口增长L", "tempVar.RKzengZhangL");
		
		////DQ政策生育,XFN政策生育,CFN政策生育,XNY政策生育,CNY政策生育
//		public double DqPlyBorn,XFNPlyBorn,CFNPlyBorn,XNYPlyBorn,CNYPlyBorn;
		varMap.put("DQ政策生育", "tempVar.DqPlyBorn");
		varMap.put("XFN政策生育", "tempVar.XFNPlyBorn");
		varMap.put("CFN政策生育", "tempVar.CFNPlyBorn");
		varMap.put("XNY政策生育", "tempVar.XNYPlyBorn");
		varMap.put("CNY政策生育", "tempVar.CNYPlyBorn");
//		varMap.put("", "");
//		varMap.put("", "");
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putBirthRatePredict () throws Exception
	{
		// 全国超生生育率.&FND2
		varMap.put( "全国超生生育率.&FND2" , "NationBirthRatePredict.get( Year.getYear( year ) )[X]" );
		
	}

	private static void putAbstract () throws Exception
	{
		//生育政策模拟摘要
		for(Abstract abs:Abstract.values ( )){
			String k = Abstract.getChinese ( abs )+"*PUT";
			String v = "pysim.put ( Abstract."+abs+" ,";
			varMap.put ( k , v );
		}
		for(Abstract abs:Abstract.values ( )){
			String k = Abstract.getChinese ( abs );
			String v = "pysim.get ( Abstract."+abs+" )";
			varMap.put ( k , v );
		}
	}

	/**
	 * 
	 */
	private static void putSumBM ()
	{
	////地区生育率与迁移摘要 只put,注意用完注释掉！
//		for(SumBM sbm:SumBM.values ( )){
//			String k = SumBM.getChinese ( sbm )+"*PUT";
//			String v = "sumBM.get ( Year.getYear ( year ) ).put ( "+sbm+" ,";
//			varMap.put ( k , v );
//		}
	}

	private static void putSummary () throws Exception
	{
		for(CX c: CX.values ( ))
			for(Summary sm:Summary.values ( )){
				String k = "地区"+c.getChinese ( )+"概要."+Summary.getChinese ( sm )+"*PUT";
				String v = "SummaryOfCX.get ( CX."+c+" ).get ( Year.getYear ( year ) ).put ( "+sm+" ,";
				varMap.put ( k , v );
			}
		for(NY c: NY.values ( ))
			for(Summary sm:Summary.values ( )){
				String k = "地区"+c.getChinese ( )+"概要."+Summary.getChinese ( sm )+"*PUT";
				String v = "SummaryOfNY.get ( "+c+" ).get ( Year.getYear ( year ) ).put ( "+sm+" ,";
				varMap.put ( k , v );
			}
		for(Summary sm:Summary.values ( )){
			String k = "地区概要."+Summary.getChinese ( sm )+"*PUT";
			String v = "SummaryOfAll.get ( Year.getYear ( year ) ).put ( "+sm+" ,";
			varMap.put ( k , v );
		}
		//TOD 易混
//		for(Summary sm:Summary.values ( )){
//			String k = Summary.getChinese ( sm )+"*PUT";
//			String v = "summary.get ( Year.getYear ( year ) ).put ( Summary."+sm+" ,";
//			varMap.put ( k , v );
//		}
//		for(Summary sm:Summary.values ( )){
//			String k = Summary.getChinese ( sm );
//			String v = "summary.get ( Year.getYear ( year ) ).get ( Summary."+sm+" )";
//			varMap.put ( k , v );
//		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putBabiesBorn () throws Exception
	{
		for(BabiesBorn bb:BabiesBorn.values ( )){
			String k = "地区生育孩次."+BabiesBorn.getChinese ( bb );
			String v = "babiesBorn.get ( Year.getYear ( year ) ).get ( "+bb+" )";
			varMap.put ( k , v );
		}
		for(BabiesBorn bb:BabiesBorn.values ( )){
			String k = "地区生育孩次."+BabiesBorn.getChinese ( bb )+"*PUT";
			String v = "babiesBorn.get ( Year.getYear ( year ) ).put ( "+bb+",";
			varMap.put ( k , v );
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putPolicyBirth () throws Exception
	{
		for(PolicyBirth cp:PolicyBirth.values ( )){
			String k = "地区政策生育率."+PolicyBirth.getChinese ( cp )+"*PUT";
			String v = "PolicyBirthRate.get ( Year.getYear ( year ) ).put ( PolicyBirth."+cp+","; // TODO注意put类型以逗号结束
			varMap.put ( k , v );
		}
		//政策及政策生育率
		for(PolicyBirth cp:PolicyBirth.values ( )){
			String k = "地区政策生育率."+PolicyBirth.getChinese ( cp );
			String v = "PolicyBirthRate.get ( Year.getYear ( year ) ).get ( PolicyBirth."+cp+")"; // TODO注意put类型以逗号结束
			varMap.put ( k , v );
		}
		for(PolicyBirth cp:PolicyBirth.values ( )){
			String k = "全国政策生育率."+PolicyBirth.getChinese ( cp )+"*PUT";
			String v = "NationPolicyBirthRate.get ( Year.getYear ( year ) ).put ( PolicyBirth."+cp+","; // TODO注意put类型以逗号结束
			varMap.put ( k , v );
		}
		//政策及政策生育率
		for(PolicyBirth cp:PolicyBirth.values ( )){
			String k = "全国政策生育率."+PolicyBirth.getChinese ( cp );
			String v = "NationPolicyBirthRate.get ( Year.getYear ( year ) ).get ( PolicyBirth."+cp+")"; // TODO注意put类型以逗号结束
			varMap.put ( k , v );
		}
	}

	/**
	 * 
	 */
	private static void putCouple ()
	{
		//这种容易冲突 TOD ,有PUT类型的，加个标志！+"*PUT"
		//couple.get ( Year.getYear ( year ) ).get ( Couple.NyPolicy1 )
//		for(Couple cp:Couple.values ( )){
//			String k = Couple.getChinese ( cp )+"*PUT";
//			String v = "couple.get ( Year.getYear ( year ) ).put ( Couple."+cp+","; // TODO注意put类型以逗号结束
//			varMap.put ( k , v );
//		}
//		for(Couple cp:Couple.values ( )){
//			String k = Couple.getChinese ( cp );
//			String v = "couple.get ( Year.getYear ( year ) ).get ( Couple."+cp+")"; // TODO注意put类型以逗号结束
//			varMap.put ( k , v );
//		}
	}


	/**
	 * 
	 * @throws Exception
	 */
	private static void putDeathGL () throws Exception
	{
//		varMap.put("NYQM(RECN())", "deathRate.get ( Nongchun ).get ( XB.Male )[X]");
		for(CX c: CX.values ( )){
			for(XB x: XB.values ( )){
				String ny = c == Chengshi? "FN":"NY";
				String xb = x == Male? "M":"F";
				String k = ny+"Q"+xb+"(RECN())";
				String v = "deathRate.get ( "+c+" ).get ( "+x+" )[X]";
				varMap.put ( k , v );
			}
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putBirthWill () throws Exception
	{
		//想生二胎比.&地带1
		for(SYSFMSField sf : SYSFMSField.values ( )){
			String k = SYSFMSField.getSYYYChinese(sf)+"二胎比.&地带1";
			String v = "pBirthWill.get("+sf.toString ( )+")[X]";
			varMap.put ( k , v );
		}

		
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putPredict () throws Exception
	{
		// 地区城镇农业分龄.&FND2  =>
		//PopulationPredictOfCXNY.get ( Chengshi ).get ( Nongye ).get ( Year.getYear ( year ) ).get ( XB.Female )[X] = 0;
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = "地区"+c.getChinese ( )+n.getChinese ( )+"分龄.&MND2";
				String k2 = "地区"+c.getChinese ( )+n.getChinese ( )+"分龄.&FND2";
				String v1 = "PopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
				String v2 = "PopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = "全国"+c.getChinese ( )+n.getChinese ( )+"分龄.&MND2";
				String k2 = "全国"+c.getChinese ( )+n.getChinese ( )+"分龄.&FND2";
				String v1 = "NationPopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
				String v2 = "NationPopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		for(CX n: CX.values ( )){
			String k1 = "地区"+n.getChinese ( )+"分龄.&MND2";
			String k2 = "地区"+n.getChinese ( )+"分龄.&FND2";
			String v1 = "PopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
			String v2 = "PopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		for(CX n: CX.values ( )){
			String k1 = "全国"+n.getChinese ( )+"分龄.&MND2";
			String k2 = "全国"+n.getChinese ( )+"分龄.&FND2";
			String v1 = "NationPopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
			String v2 = "NationPopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		for(NY n: NY.values ( )){
			String k1 = "地区"+n.getChinese ( )+"分龄.&MND2";
			String k2 = "地区"+n.getChinese ( )+"分龄.&FND2";
			String v1 = "PopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
			String v2 = "PopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		for(NY n: NY.values ( )){
			String k1 = "全国"+n.getChinese ( )+"分龄.&MND2";
			String k2 = "全国"+n.getChinese ( )+"分龄.&FND2";
			String v1 = "NationPopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Male )[X]";
			String v2 = "NationPopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		varMap.put ( "地区分龄.&MND2" , "PopulationPredictOfAll.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		varMap.put ( "地区分龄.&FND2" , "PopulationPredictOfAll.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		//全国分龄.&FND2
		varMap.put ( "全国分龄.&MND2" , "NationPopulationPredictOfAll.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		varMap.put ( "全国分龄.&FND2" , "NationPopulationPredictOfAll.get ( Year.getYear ( year) ).get ( XB.Female )[X]" );
		
		// MND1 FND1
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = "地区"+c.getChinese ( )+n.getChinese ( )+"分龄.&MND1";
				String k2 = "地区"+c.getChinese ( )+n.getChinese ( )+"分龄.&FND1";
				String v1 = "PopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
				String v2 = "PopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		for(CX n: CX.values ( )){
			String k1 = "地区"+n.getChinese ( )+"分龄.&MND1";
			String k2 = "地区"+n.getChinese ( )+"分龄.&FND1";
			String v1 = "PopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
			String v2 = "PopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		for(NY n: NY.values ( )){
			String k1 = "地区"+n.getChinese ( )+"分龄.&MND1";
			String k2 = "地区"+n.getChinese ( )+"分龄.&FND1";
			String v1 = "PopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
			String v2 = "PopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		varMap.put ( "地区分龄.&MND1" , "PopulationPredictOfAll.get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]" );
		varMap.put ( "地区分龄.&FND1" , "PopulationPredictOfAll.get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]" );
		
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				String k1 = "全国"+c.getChinese ( )+n.getChinese ( )+"分龄.&MND1";
				String k2 = "全国"+c.getChinese ( )+n.getChinese ( )+"分龄.&FND1";
				String v1 = "NationPopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
				String v2 = "NationPopulationPredictOfCXNY.get ( "+c.toString ( )+" ).get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		}
		for(CX n: CX.values ( )){
			String k1 = "全国"+n.getChinese ( )+"分龄.&MND1";
			String k2 = "全国"+n.getChinese ( )+"分龄.&FND1";
			String v1 = "NationPopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
			String v2 = "NationPopulationPredictOfCX.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		for(NY n: NY.values ( )){
			String k1 = "全国"+n.getChinese ( )+"分龄.&MND1";
			String k2 = "全国"+n.getChinese ( )+"分龄.&FND1";
			String v1 = "NationPopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]";
			String v2 = "NationPopulationPredictOfNY.get ( "+n.toString ( )+" ).get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]";
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		varMap.put ( "全国分龄.&MND1" , "NationPopulationPredictOfAll.get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]" );
		varMap.put ( "全国分龄.&FND1" , "NationPopulationPredictOfAll.get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]" );
		
		//TOD 易混乱 TOD
//		varMap.put ( "&MND1" , "pplPredict.get ( Year.getYear ( year-1 ) ).get ( XB.Male )[X]" );
//		varMap.put ( "&FND1" , "pplPredict.get ( Year.getYear ( year-1 ) ).get ( XB.Female )[X]" );
//		varMap.put ( "&MND2" , "pplPredict.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
//		varMap.put ( "&FND2" , "pplPredict.get ( Year.getYear ( year) ).get ( XB.Female )[X]" );
		
		varMap.put ( "&分龄预测..&MND2" , "pplPredict.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		varMap.put ( "&分龄预测..&FND2" , "pplPredict.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		
		varMap.put ( "&分龄预测..&MND1" , "pplPredict.get ( Year.getYear ( year - 1 ) ).get ( XB.Male )[X]" );
		varMap.put ( "&分龄预测..&FND1" , "pplPredict.get ( Year.getYear ( year - 1 ) ).get ( XB.Female )[X]" );
		//迁移人口，地区XX迁移人口预测,分年龄人口预测表
		varMap.put ( "&迁移人口..&MND2" , "pplMig.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		varMap.put ( "&迁移人口..&FND2" , "pplMig.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		varMap.put ( "&迁移人口..HJQY" , "pplMig.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		//死亡人口，地区XX死亡人口预测,分年龄人口预测表
		varMap.put ( "&城乡死亡..&MND2" , "pplDeath.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		varMap.put ( "&城乡死亡..&FND2" , "pplDeath.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		
		
		// 丧子、特扶父母
		for(CX c: CX.values ( ))
			for(NY n: NY.values ( ))
				for(XB x: XB.values ( ))
				{
					String x_c = x == Male? "M":"F";
					String k1 = "地区"+c.getChinese ( )+n.getChinese ( )+"父母.&"+x_c+"ND2";
					String k2 = "地区"+c.getChinese ( )+n.getChinese ( )+"特扶.&"+x_c+"ND2";
					String v1 = "yiHaiCN.get ( "+c+" ).get ( "+n+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
					String v2 = "tefuCN.get ( "+c+" ).get ( "+n+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
					
					varMap.put ( k1 , v1 );
					varMap.put ( k2 , v2 );
				}
		
		for(CX c: CX.values ( ))
			for(XB x: XB.values ( ))
			{
				String x_c = x == Male? "M":"F";
				String k1 = "地区"+c.getChinese ( )+"父母.&"+x_c+"ND2";
				String k2 = "地区"+c.getChinese ( )+"特扶.&"+x_c+"ND2";
				String v1 = "yiHaiC.get ( "+c+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
				String v2 = "tefuC.get ( "+c+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
				
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		
		for(NY n: NY.values ( ))
			for(XB x: XB.values ( ))
			{
				String x_c = x == Male? "M":"F";
				String k1 = "地区"+n.getChinese ( )+"父母.&"+x_c+"ND2";
				String k2 = "地区"+n.getChinese ( )+"特扶.&"+x_c+"ND2";
				String v1 = "yiHaiN.get ( "+n+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
				String v2 = "tefuN.get ( "+n+" ).get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
				
				varMap.put ( k1 , v1 );
				varMap.put ( k2 , v2 );
			}
		
		for(XB x: XB.values ( ))
		{
			String x_c = x == Male? "M":"F";
			String k1 = "地区父母.&"+x_c+"ND2";
			String k2 = "地区特扶.&"+x_c+"ND2";
			String v1 = "yiHaiA.get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
			String v2 = "tefuA.get ( Year.getYear ( year ) ).get ( "+x+" )[X]";
			
			varMap.put ( k1 , v1 );
			varMap.put ( k2 , v2 );
		}
		
		//分年龄丧子人口预测-一孩	&ALI2..&FND2 sondie.get ( Year.getYear ( year ) ).get ( XB.Female )[X] = 0;
		// TOD 注意这种没有特征的 Key 要用完后注释掉！
//		varMap.put ( "&ALI2..&FND2" , "sondie.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
//		varMap.put ( "&ALI2..&MND2" , "sondie.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
		
		
		//分年龄丧子人口预测-特扶
//		varMap.put ( "&ALI1..&MND2" , "tefu.get ( Year.getYear ( year ) ).get ( XB.Male )[X]" );
//		varMap.put ( "&ALI1..&FND2" , "tefu.get ( Year.getYear ( year ) ).get ( XB.Female )[X]" );
		
	}

	private static void putBabies () throws Exception
	{
		// 地区非农生育.&BND2 => BirthPredictOfNY.get ( Feinong ).get ( Babies.getBabies ( year ) )[X]
		for(NY c: NY.values ( )){
			String k = "地区"+c.getChinese ( )+"生育.&BND2";
			String v = "BirthPredictOfNY.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		for(NY c: NY.values ( )){
			String k = "全国"+c.getChinese ( )+"生育.&BND2";
			String v = "NationBirthPredictOfNY.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		for(CX c: CX.values ( )){
			String k = "地区"+c.getChinese ( )+"生育.&BND2";
			String v = "BirthPredictOfCX.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		for(CX c: CX.values ( )){
			String k = "全国"+c.getChinese ( )+"生育.&BND2";
			String v = "NationBirthPredictOfCX.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		varMap.put ( "地区生育.&BND2" , "BirthPredictOfAll.get ( Babies.getBabies ( year ) )[X]" );
		varMap.put ( "全国生育.&BND2" , "NationBirthPredictOfAll.get ( Babies.getBabies ( year ) )[X]" );
		
		
		
		// 地区非农超生.&BND2 => OverBirthPredictOfNY.get ( Feinong ).get ( Babies.getBabies ( year ) )[X]
		for(NY c: NY.values ( )){
			String k = "地区"+c.getChinese ( )+"超生.&BND2";
			String v = "OverBirthPredictOfNY.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		for(CX c: CX.values ( )){
			String k = "地区"+c.getChinese ( )+"超生.&BND2";
			String v = "OverBirthPredictOfCX.get ( "+c.toString ( )+" ).get ( Babies.getBabies ( year ) )[X]";
			varMap.put ( k , v );
		}
		varMap.put ( "地区分龄超生.&BND2" , "OverBirthPredictOfAll.get ( Babies.getBabies ( year ) )[X]" );
		varMap.put ( "全国分龄超生.&BND2" , "NationOverBirthPredictOfAll.get ( Babies.getBabies ( year ) )[X]" );
		//政策生育
		varMap.put ( "地区分龄政策生育.&BND2" , "policyBabies.get ( Babies.getBabies ( year ) )[X]" );
		varMap.put ( "全国分龄政策生育.&BND2" , "NationPolicyBabies.get ( Babies.getBabies ( year ) )[X]" );
		
		
	}

	/**
	 * 
	 * @throws Exception
	 */
	private static void putHunpei () throws Exception
	{
		//Hunpei Fullname Field, gen with loop!!!!
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				for(HunpeiField h:HunpeiField.values ( )){
					String k = "地区"+c.getChinese ( )+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
					String v = "ziNv.get("+c.toString ( )+").get("+n.toString ( )+").get("+h.toString ( )+")[X]";
					varMap.put ( k , v );
				}
			}
		}
		for(CX c: CX.values ( )){
			for(NY n: NY.values ( )){
				for(HunpeiField h:HunpeiField.values ( )){
					String k = "全国"+c.getChinese ( )+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
					String v = "NationziNv.get(CX."+c.toString ( )+").get(NY."+n.toString ( )+").get( HunpeiField."+h.toString ( )+")[X]";
					varMap.put ( k , v );
				}
			}
		}
		
		for(NY n: NY.values ( )){
			for(HunpeiField h:HunpeiField.values ( )){
				String k = "地区"+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
				String v = "ziNvOfNY.get("+n.toString ( )+").get("+h.toString ( )+")[X]";
				varMap.put ( k , v );
			}
		}
		for(CX n: CX.values ( )){
			for(HunpeiField h:HunpeiField.values ( )){
				String k = "地区"+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
				String v = "ziNvOfCX.get("+n.toString ( )+").get("+h.toString ( )+")[X]";
				varMap.put ( k , v );
			}
		}
		
		for(HunpeiField h:HunpeiField.values ( )){
			String k = "地区"+"子女."+HunpeiField.getChinese ( h );
			String v = "ziNvOfAll.get("+h.toString ( )+")[X]";
			varMap.put ( k , v );
		}
		
		//全国
		for(NY n: NY.values ( )){
			for(HunpeiField h:HunpeiField.values ( )){
				String k = "全国"+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
				String v = "NationziNvOfNY.get("+n.toString ( )+").get( HunpeiField."+h.toString ( )+")[X]";
				varMap.put ( k , v );
			}
		}
		for(CX n: CX.values ( )){
			for(HunpeiField h:HunpeiField.values ( )){
				String k = "全国"+n.getChinese ( )+"子女."+HunpeiField.getChinese ( h );
				String v = "NationziNvOfCX.get("+n.toString ( )+").get( HunpeiField."+h.toString ( )+")[X]";
				varMap.put ( k , v );
			}
		}
		
		for(HunpeiField h:HunpeiField.values ( )){
			String k = "全国"+"子女."+HunpeiField.getChinese ( h );
			String v = "NationziNvOfAll.get( HunpeiField."+h.toString ( )+")[X]";
			varMap.put ( k , v );
		}
		
		// &ALI1..D
//		for(HunpeiField h:HunpeiField.values ( )){
//			String k = "&ALI1.."+HunpeiField.getChinese ( h );
//			String v = "zn.get("+h.toString ( )+")[X]";
//			varMap.put ( k , v );
//		}
		// &ALI1..D
		for(HunpeiField h:HunpeiField.values ( )){
			String k = "&子女.."+HunpeiField.getChinese ( h );
			String v = "zn.get("+h.toString ( )+")[X]";
			varMap.put ( k , v );
		}
		/**
		 *给一些比如：
		 *	SELE IIF(CX=1,61,IIF(CX=2,62,IIF(CX=3,63,64)))
		 *	REPL 超生DDB WITH DDB-政策DDB,超生NNB WITH NNB-政策NNB,超生NMDFB WITH NMDFB-政策NMDFB,超生NFDMB WITH NFDMB-政策NFDMB,超生释放B WITH 单独释放B+双非释放B-政策释放B,超生生育 WITH 超生DDB+超生NNB+超生NMDFB+超生NFDMB+超生释放B ALL 
		 *的用 
		 */
		//全国！
		//这种容易冲突 TODO
//		for(HunpeiField h:HunpeiField.values ( )){
//			
//			String k = HunpeiField.getChinese ( h );
//			String v = "zn.get( HunpeiField."+h.toString ( )+")[X]";
//			varMap.put ( k , v );
//		}
	}

	private HashMap<String, String> map;
	
	
	public VarMapData ( )
	{
		super ( );
		map = new HashMap < String , String > ( );
	}
	
	public String get(String k){
		return map.get ( k );
	}
	
	public void put(String k,String v) throws Exception{
		if( map.containsKey ( k ))
			throw new Exception ( "duplicated key for VarMap--"+k );
		else
			map.put ( k , v );
	}
	
	public HashMap<String, String> getHashMap(){
		return map;
	}

	private static VarMapData varMap= new VarMapData ( );
}
