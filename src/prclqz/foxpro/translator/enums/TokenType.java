package prclqz.foxpro.translator.enums;
public enum TokenType{
	
	// special symbols
	
	plus,		// +
	minus,		// -
	multi,		// *
	divide,		// /
	
	equal,		// =
	less,		// <
	bigger,		// >
	notequal,	// <> ><
	lessOrEq,	// <= =<
	biggerOrEq,	// >= =>
	
	comma,		// ,
	semicolon, 	// ;
	dot,		// .
	newline,	// \n 换行
	left_parent,// (
	right_parent,//)
	
	
	// reserved words
	REPL,	// replace
	WITH,
	FOR,
	AND,
	OR,
	IF,
	THEN,
	ELSE,
	ENDI,	//ENDIF
	CASE,
	DO,
	SUM,
	CALC,	//calculate
	LOAC,	//locate
	TO,
	ENDC,	//endcase
	COPY,
	ENDF,	// ENDFOR
	OTHE,	// OTHERWISE
	FIEL,	// FIELDS 
	ARRA,	// ARRAY
	LOCA,	// locate  TODO: not supporting
	ALL,
	
	//others
	number, // only support Integer!
	dbNum,  // double number
	variable,
	string,
	comment;
	
	public static TokenType[] getReservedWords(){
		TokenType[] arr = {REPL,WITH,FOR,AND,OR,IF,THEN,ELSE,ENDI,CASE,DO,SUM,CALC,LOAC,TO,ENDC,COPY,ENDF,OTHE,FIEL,ARRA,LOCA,ALL};
		return arr;
	}
	
}