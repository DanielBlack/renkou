package prclqz.foxpro.translator.simpleImpl;

public enum SimpleParserSyntax
{
	
	commands,			// top of the parsing
	ternimal,			// end of the parsing
	
	if_expression,
	
	logical_expression, // important	
	value_expression,	// important
	
	assign_expression,
	
	calc_expression,
	value_expression_list,
	varlist,
	
	sum_expression,
	
	repl_expression,
	sub_replace,
	
	copy_expression,
	fieldlist,
	
	for_expression,
	
	do_expression,
}
