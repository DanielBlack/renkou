package prclqz.foxpro.translator.simpleImpl;

import static prclqz.foxpro.translator.enums.TokenType.*;
import static prclqz.foxpro.translator.simpleImpl.SimpleParserSyntax.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import prclqz.foxpro.translator.FoxproParseTree;
import prclqz.foxpro.translator.Token;
import prclqz.foxpro.translator.enums.TokenType;
import prclqz.foxpro.translator.interf.IFoxpro2Java;

/**
 * only support the following commands:
 *  	ASSIGN EXPRESSION
 *  	LOGICAL EXPRESSION
 *  	IF THEN ELSE ENDIF
 *  	REPLE WITH FOR
 *  	SUM TO FOR
 *  	FOR
 *  	DO CASE
 *  	COPY TO ARRAY
 *  	CALCULATE
 * @author prclqz@zju.edu.cn
 *
 */
public class SimpleFoxpro2Java implements IFoxpro2Java
{

	private HashMap < String , String > varMap;

	public SimpleFoxpro2Java ( HashMap < String , String > varMap )
	{
		this.varMap = varMap;
	}

	@Override
	public List < String > foxpro2Java ( FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > javacodes = new LinkedList < String > ( );
		
		if( tree.type != commands )
			throw new Exception ( "error parse tree, expecting commands at the top ");
		else
			javacodes = transCommands( tree );
		
		return javacodes;
	}

	/**
	 * if encounter one of the following expression-- replace, sum, calculate or copy to array
	 * then aggregate together and translate,
	 * else translate in a normal type.
	 *  
	 * @param tree
	 * @return javacodes
	 * @throws Exception 
	 */
	private LinkedList < String > transCommands ( FoxproParseTree tree) throws Exception
	{
		ArrayList < FoxproParseTree > set;
		Iterator < FoxproParseTree > it = tree.children.listIterator ( );
		LinkedList < String > javacodes = new LinkedList < String > ( );
		
		while(it.hasNext ( ))
		{
			FoxproParseTree head = it.next ( );
			if( isAggregating(head) ){
				
				set = new ArrayList < FoxproParseTree > ( );
				set.add ( head );
				
				// add others to set until end or encounter anther type
				boolean isProcess = false;
				while( it.hasNext ( ) ){
					head = it.next ( );
					if( isAggregating ( head ))
						set.add ( head );
					else{
						// process this head
						isProcess = true;
						break;
					}
				}
				javacodes.addAll ( transReplSet( set ) );
				
				if( isProcess )
					javacodes.addAll ( transOne( head ) );
					
			}else{
				javacodes.addAll ( transOne(head) );
			}
		}
		
		return javacodes;
	}

	/**
	 * ONLY support:
	 * 	IF ELSE THEN ENDIF expression
	 *  FOR ... ENDFOR expression
	 *  DO CASE ... ENDCASE expression
	 *  assignment expression
	 *  
	 *  sum_expression 
	 *  calc_expression
	 *  copy_expression
	 *   
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transOne ( FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > rs;
		
		switch ( tree.type ){
		case if_expression:
			rs = transIf( tree );
			break;
		case for_expression:
			rs = transFor( tree );
			break;
		case do_expression:
			rs = transDo( tree );
			break;
		case assign_expression:
			rs = transAssgn( tree );
			break;
		case sum_expression:
			rs = transSum( tree );
			break;
		case calc_expression:
			rs = transCalc( tree );
			break;
		case copy_expression:
			rs = transCopy( tree );
			break;
			default:
				throw new Exception ( "error: not supporting command type:"+ tree.type );
		}
		return rs;
	}

	/**
	 *     copy_expression
     *       0 COPY
     *       1 TO
     *       2 ARRA
     *       3 ARFND1
     *       4 FIEL
     *		 5 全国非农分龄.&FND1
     *		 6 全国非农分龄.&FND2
     *		[7 FOR
     *		 8 logical_expression]
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transCopy ( FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > rs = new LinkedList < String > ( );
		rs.add ( "for(X = MAX_AGE-1; X >= 0 ; X--){" );
		int rowNum = tree.children.size ( ) - 5;
		String arr = getVarName ( tree.children.get ( 3 ).terminal );
		
		String logic;
		if( tree.children.get ( tree.children.size ( )-1 ).type == logical_expression )
		{
			rowNum = rowNum -2;
			logic = transLogic ( tree.children.get ( tree.children.size ( )-1 ) );
		}else{
			logic = "true";
		}
		
		rs.add ( "if( "+logic+" ){" );
		for(int i=0; i<rowNum; i++){
			String field = getVarName ( tree.children.get ( 5+i ).terminal );
			rs.add ( arr + "["+i+"][X] = " + field+";" );
		}
			
		rs.add ( "}" );
		rs.add ( "}" );
		return rs;
	}

	/**
	 *   calc_expression
     *      0  CALC
     *      1  value_expression  !!!!!TODO Map Data  for MIN(X) to MIX, we should map a String like "9999|X < MIX|X" : 初始值|赋值条件|赋值
     *      2  value_expression
     *      3  TO
     *      4  MIX
     *      5  MIX
     *    [ 6  FOR
     *     7. logical_expression]
     *      
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transCalc ( FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > rs = new LinkedList < String > ( );
		int rowNum = (tree.children.size ( ) - 4)/2;
		
		String logic;
//		logic = transLogic ( tree.children.get ( tree.children.size ( )-1 ) );
		if( tree.children.get ( tree.children.size ( )-1 ).type == logical_expression )
			logic = transLogic ( tree.children.get ( tree.children.size ( )-1 ) );
		else{
			logic = "true";
			rowNum++;
		}
		String[][] vals_split = new String[rowNum][]; // should be [rowNum][3] 
		String[] vars = new String[rowNum];
		
		for(int i=0; i<rowNum;i++){
			
			vals_split[i] = transVal ( tree.children.get ( i+1 ) ).split ( "\\|" );
			if ( vals_split[i].length != 3 )
				throw new Exception ( "error Data Mapping for "+tree.children.get ( i+1 ).terminal );
			
			vars[i] = getVarName ( tree.children.get ( i+2+rowNum ).terminal );
			rs.add ( vars[i]+" = "+vals_split[i][0]+";" );
		}
		
		rs.add ( "for(X = MAX_AGE-1; X >= 0 ; X--){" );
		
		for(int i=0; i<rowNum;i++){
			rs.add ( "if( "+logic+" && "+vals_split[i][1]+" )" );
			rs.add ( vars[i]+" = "+ vals_split[i][2]+";" );
		}
		rs.add ( "}" );
		
		return rs;
	}

	/**
	 *   sum_expression
     *       0 SUM
     *       1 value_expression
     *       2 value_expression
     *       3 TO
     *       4 SDM_F
     *       5 SDM_F
     *      [6 FOR
     *       7 logical_expression]
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transSum ( FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > rs = new LinkedList < String > ( );
		int rowNum = (tree.children.size ( ) - 3)/2;
		
		boolean hasPut; //是否有put类型，有则优先使用
		
		String logic;
		if( tree.children.get ( tree.children.size ( )-1 ).type == logical_expression )
			logic = transLogic ( tree.children.get ( tree.children.size ( )-1 ) );
		else{
			logic = "true";
			rowNum++;
		}
		
		for(int i=0; i<rowNum; i++){
			hasPut = false;
			String var = getPutVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			if( var == null )
				var = getVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			else
				hasPut = true;
//			String var = getVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			
			if(hasPut)
				rs.add ( var+"0.0 );" );
			else
				rs.add ( var + " = 0;" );
			
		}
		
		rs.add ( "for(X = MAX_AGE-1; X >= 0 ; X--){" );
		
		rs.add ( "if( "+logic+" ){" );
		
		for(int i=0; i<rowNum; i++){
			String value = transVal ( tree.children.get ( 1+i ) );
			
			hasPut = false;
			String var = getPutVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			String varGet = "";
			if( var == null )
				var = getVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			else{
				hasPut = true;
				varGet = getVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			}
//			String var = getVarName ( tree.children.get ( 2+rowNum+i ).terminal );
			
//			rs.add ( var + " += " + value+";" );
			if(hasPut)
				rs.add ( var+"("+varGet+"+"+value+" ) );" );
			else
				rs.add ( var + " += " + value+";" );
//				rs.add ( var + " = 0;" );
		}
		rs.add ( "}" );
		
		rs.add ( "}" );
		return rs;
	}

	/**
	 * assignment_expression
	 * 
	 * @param head
	 * @return
	 * @throws Exception
	 */
	private LinkedList < String > transAssgn ( FoxproParseTree head ) throws Exception
	{
		FoxproParseTree var = head.children.get ( 0 );
		String varname;
		boolean hasPut = false;
		String pre = "error in assignment expression, expecting ";
		
		if( var.type == ternimal && var.terminal.getType ( ) == TokenType.variable )
		{
//			varname = getVarName( var.terminal);
			varname = getPutVarName ( var.terminal );
			if( varname == null )
				varname = getVarName ( var.terminal );
			else
				hasPut = true;
		}
		else
			throw new Exception ( pre+"a variable but encounter a "+ var.terminal.getType ( ) );
		
		FoxproParseTree equal = head.children.get ( 1 );
		if( equal.type != ternimal || equal.terminal.getType ( ) != TokenType.equal )
			throw new Exception ( pre+"an equal but encounter a "+ equal.terminal.getType ( ) );
		
		FoxproParseTree valTree = head.children.get ( 2 );
		if( valTree.type != value_expression )
			throw new Exception ( pre+"an value_expression but encounter a "+ valTree.type );
		
		String value = transVal( valTree ); 
		
		LinkedList < String > rs = new LinkedList < String > ( );
		if( hasPut )
		{
			rs.add( varname + value +" );" );
		}else{
			rs.add ( varname+" = "+value+";" );
		}
		return rs;
	}

	/***
	 * value_expression
	 * 
	 * @param valTree
	 * @return
	 * @throws Exception
	 */
	private String transVal ( FoxproParseTree valTree ) throws Exception
	{
		if( valTree.type != value_expression )
			throw new Exception ( "error in value expression, expecting an value_expression but encounter a "+ valTree.type );
		
		StringBuffer sb = new StringBuffer ( );
		for( int i=0; i< valTree.children.size ( ); i++ ){
			sb.append ( tranValueToken( valTree.children.get ( i ).terminal ) );
		}
		return sb.toString ( );
	}

	private String tranValueToken ( Token t ) throws Exception
	{
		switch( t.getType ( ) ){
		case variable:
			return getVarName ( t );
		case number:
			return t.toString ( );
		case string:
			return t.getStrval ( );
		case plus:
			return " + ";
		case minus:
			return " - ";
		case multi:
			return " * ";
		case divide:
			return " / ";
		case left_parent:
			return "( ";
		case right_parent:
			return " )";
			default:
				throw new Exception ( "error in translate value token, unexpected"+ t.getType ( ) );
		}
	}

	private String getVarName ( Token terminal )
	{
		if( varMap.containsKey ( terminal.toString ( ) ) )
				return varMap.get ( terminal.toString ( ) );
		else if( varMap.containsKey ( terminal.toString ( ).toUpperCase ( ) ) )
			return varMap.get ( terminal.toString ( ).toUpperCase ( ) );
		else
			return terminal.toString ( );
	}
	
	private String getPutVarName ( Token terminal )
	{
		if( varMap.containsKey ( terminal.toString ( )+"*PUT" ) )
			return varMap.get ( terminal.toString ( )+"*PUT" );
		else if( varMap.containsKey ( terminal.toString ( ).toUpperCase ( )+"*PUT" ) )
			return varMap.get ( terminal.toString ( ).toUpperCase ( )+"*PUT" );
		else
			return null;
	}	

	/***
	 * 				  do_expression
     *                   0	DO
     *                   1	CASE
     *                   2	CASE
     *                   3	logical_expression
     *                   4	commands
     *                   5	CASE
     *                   6	logical_expression
     *                   7	commands
     *                   8	OTHE
     *                   9	commands
     *                   10	ENDC
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transDo ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != do_expression )
			throw new Exception ( "error in transDo, expecting an do_expression" );
		
		LinkedList < String > rs = new LinkedList < String > ( );
		String firstL;
		boolean flg = false;
		
		for(int i=2; ;i+=3){
			
			if( tree.children.get ( i ).type == ternimal && tree.children.get ( i ).terminal.getType ( ) == ENDC)
				break;
			else if(tree.children.get ( i ).type == ternimal && tree.children.get ( i ).terminal.getType ( ) == OTHE ){
				rs.add ( "else{" );
				rs.addAll ( transCommands ( tree.children.get ( i+1 ) ) );
				rs.add ( "}" );
				break;
			}
			
			if( ! flg ){
				firstL =  "if( " ;
				flg = true;
			}else{
				firstL =  "else if(" ;
			}
			
			FoxproParseTree log_exp = tree.children.get ( i+1 );
			String logic = transLogic( log_exp );
			
			firstL += logic+"){";
			rs.add ( firstL );
			rs.addAll ( transCommands ( tree.children.get ( i+2 ) ) );
			rs.add ( "}" );
			
		}
		return rs;
	}

	/**
	 * Tree example:
	 * 
	 * for_expression
     *	  0	FOR
     *    1	X1
     *    2	equal
     *    3	value_expression
     *    4	TO
     *    5	value_expression
     *    6	commands
     *    7	ENDF
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transFor ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != for_expression )
			throw new Exception ( "error in transFor, expecting an for_expression" );
		
		String var = getVarName ( tree.children.get ( 1 ).terminal );
		String beg = transVal ( tree.children.get ( 3 ) );
		String end = transVal ( tree.children.get ( 5 ) );
		
		LinkedList < String > rs = new LinkedList < String > ( );
		rs.add ( "for( "+var+"= "+beg+"; "+var+" <= "+end+"; "+var+"++){" );
		rs.addAll ( transCommands ( tree.children.get ( 6 ) ) );
		rs.add ( "}" );
		
		return rs;
	}

	/**
	 * if_expression
     *       0	IF
     *       1	logical_expression
     *       2	commands
     *       3	ELSE
     *       4	commands
     *       5	ENDI
     *       
	 * @param tree
	 * @return
	 * @throws Exception
	 */
	private LinkedList < String > transIf ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != if_expression )
			throw new Exception ( "error in transIf, expecting an if_expression" );
		
		LinkedList < String > rs = new LinkedList < String > ( );
		
		FoxproParseTree log_exp = tree.children.get ( 1 );
		String logic = transLogic( log_exp );
		
		rs.add ( "if( "+logic+" ){" );
		
		rs.addAll ( transCommands ( tree.children.get ( 2 ) ) );
		if( tree.children.size ( ) > 4){
			rs.add ( "}else{ " );
			rs.addAll ( transCommands ( tree.children.get ( 4 ) ) );
		}
		rs.add ( "}" );
		
		return rs;
	}

	/**
	 * logic_expression
	 * 
	 * logical_expression
     *  	0	AND
     *  	1	value_expression
     *  	2	bigger
     *  	3	value_expression
     *  	4	AND
     *  	5	value_expression
     *  	6	notequal
     *  	7	value_expression
     *  
	 * @param tree
	 * @return
	 * @throws Exception
	 */
	private String transLogic ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != logical_expression )
			throw new Exception ( "error in transIf, expecting an logical_expression" );
		
		String rs = transVal ( tree.children.get ( 1 ) );
		rs += transCmpOpr( tree.children.get ( 2 ));
		rs += transVal( tree.children.get ( 3 ));
		
		for( int i=4; i < tree.children.size ( );i+=4 ){
			rs += transLogicOpr( tree.children.get ( i ));
			rs += transVal ( tree.children.get ( i+1 ) );
			rs += transCmpOpr( tree.children.get ( i+2 ));
			rs += transVal( tree.children.get ( i+3 ));
		}
		
		return rs;
	}

	/**
	 * logic operator, and or
	 * @param tree
	 * @return
	 * @throws Exception
	 */
	private String transLogicOpr ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != ternimal )
			throw new Exception ( "error in transLogicOpr, expecting a terminal" );
		switch( tree.terminal.getType ( ) ){
		case AND:
			return " && ";
		case OR:
			return " || ";
			default:
				throw new Exception ( "error in transLogicOpr, expecting a logicl opterator token" );
		}
	}

	/**
	 *  comparing operator : > < ==
	 * @param tree
	 * @return
	 * @throws Exception
	 */
	private String transCmpOpr ( FoxproParseTree tree ) throws Exception
	{
		if( tree.type != ternimal )
			throw new Exception ( "error in transCmpOpr, expecting a terminal" );
		
		switch( tree.terminal.getType ( ) ){
		case bigger:
			return " > ";
		case biggerOrEq:
			return " >= ";
		case less:
			return " < ";
		case lessOrEq:
			return " <= ";
		case equal:
			return " == ";
		case notequal:
			return "!=";
			default:
				throw new Exception ( "error in transLogicOpr, expecting a comparing opterator token" );
		}
	}

	/**
	 * set ONLY include:
	 * 	replace_expression
	 *  sum_expression
	 *  calculate_expression
	 *  copy_expression
	 *  
	 *  ::::::::::::::::::::::::::tree is like::::::::::::::::::::::::::::::
     *    repl_expression
     *       0 REPL
     *       1 sub_replace
     *       2 FOR
     *      [3 logical_expression]
     *    repl_expression
     *       0 REPL
     *       1 sub_replace
     *       2 FOR
     *      [3 logical_expression]
     *       
     *       
	 * @param set
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transReplSet (ArrayList < FoxproParseTree > set ) throws Exception
	{
		LinkedList < String > rs = new LinkedList < String > ( );
		rs.add ( "for(X = MAX_AGE-1; X >= 0 ; X--){" );
		for(int i=0; i< set.size ( );i++){
			FoxproParseTree repl = set.get ( i );
			String logic;
			if( repl.children.size ( )==4 )
				logic = transLogic ( repl.children.get ( 3 ) );
			else
				logic = "true";
			rs.add ( "if( "+logic+" ){" );
			rs.addAll ( transSubRepl(repl.children.get ( 1 )) );
			rs.add ( "}" );
		}
		rs.add ( "}" );
		return rs;
	}

	/***
	 * 		sub_replace
     *          0 DDBM
     *          1 WITH
     *          2 value_expression
     *          3 DDBF
     *          4 WITH
     *          5 value_expression
	 * @param tree
	 * @return
	 * @throws Exception 
	 */
	private LinkedList < String > transSubRepl (FoxproParseTree tree ) throws Exception
	{
		LinkedList < String > rs = new LinkedList < String > ( );
		int rowNum = tree.children.size ( ) / 3;
		
		boolean hasPut; //是否有put类型，有则优先使用
		
		for(int i=0;i<rowNum;i++){
			
//			hasPut = checkHasPut(tree.children.get ( 3*i ).terminal);
			hasPut = false;
			String var = getPutVarName ( tree.children.get ( 3*i ).terminal );
			if( var == null )
				var = getVarName ( tree.children.get ( 3*i ).terminal );
			else
				hasPut = true;
			
			String val = transVal ( tree.children.get ( 3*i+2 ) );
			
			if(hasPut)
				rs.add ( var+val+");" );
			else
				rs.add ( var+" = "+val+";" );
		}
		return rs;
	}

	/**
	 * 根据最后一个字符是否是逗号 TODO
	 * @param var
	 * @return
	 */
	private boolean checkIsPut ( String var )
	{
		char[] cs = var.toCharArray ( );
		if( cs[cs.length-1] == ',')
			return true;
		else
			return false;
	}

	private boolean isAggregating ( FoxproParseTree head )
	{
		SimpleParserSyntax type = head.type;
		if( type == repl_expression )
			return true;
		else
			return false;
	}

}
