package prclqz.foxpro.translator.simpleImpl;

import static java.lang.System.*;
import static prclqz.foxpro.translator.enums.TokenType.*;
import static prclqz.foxpro.translator.simpleImpl.SimpleScannerState.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import prclqz.foxpro.translator.Token;
import prclqz.foxpro.translator.enums.TokenType;
import prclqz.foxpro.translator.interf.IScanner;

/**
 * --------------------
 * 已支持浮点数，但由于没用使用正则来匹配……暂时无法扩展翻译IIF语句啊！
 * ---------------------
 * Not support:
 * 	float number!
 *  
 * @author prclqz@zju.edu.cn
 *
 */
public class SimpleFoxproScanner implements IScanner
{

	@Override
	public ArrayList < Token > scan ( String input ) throws Exception
	{
//		out.println( input );
		
		ArrayList < Token > output = new ArrayList < Token > ( );
		LinkedList < Character > list = new LinkedList < Character > ( );
		
		// pre scanning!!!
		//TODO 略去 comments是在scanning中，其实也可以在 pre-scanning中
		input = pre_scann(input);
		
		char[] chs = input.toCharArray ( );
		
		for( int i=0 ; i<chs.length; i++)
			list.addLast ( chs[i] );
		
		IsConsume flag = new IsConsume ( );
		char next,ahead,back;
		
		//大循环
		back = ' ';
		while( !list.isEmpty ( ) ){
			
			SimpleScannerState current = start;
			StringBuffer curStr = new StringBuffer ( );
			SimpleScannerState backState = null;
			while( current != end ){
				
				flag.setTrue ( );
				
				if( list.isEmpty ( ))
					break;
				else
					next = list.pollFirst ( );
				
//				out.println( ""+(int)next +" ");
				
				if( !list.isEmpty ( ))
					ahead = list.peek ( );
				else
					ahead = ' ';
				
				backState = current;
				
				current = nextState ( current , next , flag , ahead , back );
				if( current == inSemicolon )
				{
					char ch = list.pollFirst ( ); // remove newline
					if( ch != '\n' )
						throw new Exception ( "error character after ';' --"+list );
					break;
				}
				
				if( flag.val == true ){
					curStr.append ( next );
					back = next;
				}
				else
					list.addFirst ( next );
				
				if( current == start )
					break;
				else if( current == end){
					Token t;
					String tmp = curStr.toString ( );
					char ch;
					
					switch(backState){
					case inNum:
						int i = Integer.parseInt ( curStr.toString ( ) );
						t = new Token ( number , i , null );
						break;
					case inFloat:
						double d = Double.parseDouble ( curStr.toString ( ) );
						t = new Token ( number , d , null);
						break;
					case inCmp1:
					case inCmp2:
						if( tmp.equals ( "=" ))
							t = new Token ( equal , 0 , null );
						else if( tmp.equals ( ">" ) )
							t = new Token ( bigger , 0 , null );
						else if( tmp.equals ( "<" ))
							t = new Token ( less , 0 , null );
						else if( tmp.equals ( ">=" ) || tmp.equals ( "=>" ))
							t = new Token ( biggerOrEq , 0 , null );
						else if( tmp.equals ( "<=" ) || tmp.equals ( "=<" ))
							t = new Token ( lessOrEq , 0 , null );
						else if( tmp.equals ( "<>" ) || tmp.equals ( "><" ))
							t = new Token ( notequal , 0 , null );
						else{
							throw new Exception ( "error characters for incmpX state" );
						}
						break;
					case inComment2:
					case inComment3: // here skip the comments
//						t = new Token ( comment , 0 , tmp );
						t = null;
						break;
					case start:
						ch = tmp.charAt ( 0 );
						switch (ch){
						case '*':
							t = new Token ( multi , 0 , null ); 	break;
						case '+':
							t = new Token ( plus , 0 , null ); 		break;
						case '-':
							t = new Token ( minus , 0 , null ); 	break;
						case '/':
							t = new Token ( divide , 0 , null ); 	break;
						case ',':
							t = new Token ( comma , 0 , null ); 	break;
						case '.':
							t = new Token ( dot , 0 , null );	 	break;
						case ';':
							t = new Token ( semicolon , 0 , null ); break;
						case '(':
							t = new Token ( left_parent , 0 , null ); break;
						case ')':
							t = new Token ( right_parent , 0 , null ); break;
						case '=':
							t = new Token ( equal , 0 , null ); 	break;
							default:
								throw new Exception ( "error special character!" );
						}
						break;
					case inString:
						t = new Token ( string , 0 , tmp );
						break;
					case inNewline:
						t = new Token ( newline , 0 , "\n" );
						break;
					case inID:
						// 判断是否属于某个保留字 或者变量,也可能是函数
						String upper;
						if( tmp.length ( ) >4 )
							upper = tmp.toUpperCase ().substring ( 0 , 4 );
						else 
							upper = tmp.toUpperCase ( );
						
						TokenType type = null;
						for(TokenType tt : TokenType.getReservedWords ( )){
							if(upper.equals ( tt.toString ( ) ))
							{
								type = tt;
								break;
							}
						}
						if( type == null){
							t = new Token ( variable , 0 , tmp );
						}else
							t = new Token ( type , 0 , null );
						break;
					case inFunc:
						t = new Token ( variable , 0 , tmp );
						break;
						default:
							throw new  Exception("error last state for end state");
					}
					if (t != null)
						output.add ( t );
//					out.println("*"+t+"*");
				}
				
			}// in-loop
			
		}// big-loop
		
		// we have to skip the newline AGAIN since after skip the comments, there maybe some newline contacting 
		skipSurplusToken( output );
		
		return output;
	}

	private String pre_scann ( String input )
	{
		input = input.replace ( ".AND." , " AND " );
		input = input.replace ( ".OR." , " OR " );
		input = input.replace ( ".and." , " and " );
		input = input.replace ( ".or." , " or " );
		return input;
	}

	/**
	 * here we should skip the newline
	 * @param output
	 */
	private void skipSurplusToken ( ArrayList < Token > output )
	{
//		System.out.println( output);
		
		Iterator < Token > it = output.listIterator ( );
		
		while( it.hasNext ( ) && it.next ( ).getType ( ) != newline );
		
		while(it.hasNext ( )){
			if(it.next ( ).getType ( ) == newline)
			{
				it.remove ( );
			}else{
				while( it.hasNext ( ) && it.next ( ).getType ( ) != newline);
			}
		}
		
//		System.out.println( output);
	}

}
