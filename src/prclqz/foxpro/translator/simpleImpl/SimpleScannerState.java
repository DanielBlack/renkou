package prclqz.foxpro.translator.simpleImpl;

import static prclqz.foxpro.translator.Tools.*;
import static prclqz.foxpro.translator.enums.TokenType.*;
import java.util.LinkedList;
import java.util.Stack;

public enum SimpleScannerState
{
	start,
	
	inNum,
	inFloat,
	inID,
	inSemicolon,
	inCmp1,
	inCmp2,
	inComment1,
	inComment2,
	inComment3,
	inString,
	inNewline,
	inFunc,
	end;
	
	public static Stack < Character > funcStack;
	
	public static class IsConsume{
		public boolean val;
		public IsConsume(){
			val = true;
		}
		public void setTrue(){
			val = true;
		}
		public void setFalse(){
			val = false;
		}
	}
	public static SimpleScannerState nextState( SimpleScannerState currentState , char nextChar, IsConsume flag,
									char aheadChar,char backtrackChar ) throws Exception
	{
		//先假设是消耗式
		flag.setTrue ( );
		
		switch (currentState){
		case start:
			
			//空字符
			if ( isEmptyCharacter(nextChar))
				return start;
			
			if( isDigit(nextChar)) 
				return inNum;
			if( isLetter(nextChar) || isChinese(nextChar) )
				return inID;
			if( isSpecial(nextChar) )
				return end;
			if( nextChar == ';' )
				return inSemicolon;
			
			
			if( nextChar =='>' || nextChar=='<')
				return inCmp1;
			
			if( nextChar =='=' )
				return inCmp2;
			
			if( nextChar =='&')
				return inComment1;
			
			if( ( (nextChar =='*')||(nextChar =='?')) && (isEmptyCharacter ( backtrackChar ) || backtrackChar =='\n'))
				return inComment3;
			
			if( nextChar =='*' && !isEmptyCharacter ( backtrackChar ) )
				return end;
			
			if( nextChar =='\'' )
				return inString;
			
			if( nextChar =='\n')
				return inNewline;
			
//			throw new Exception ( "wrong input for start" );
			System.out.println("在start后面又奇怪的输入字符："+nextChar+" "+aheadChar);
			return start;
			
		case inNum:
			
			if( isDigit(nextChar) ) // || nextChar =='.' 
				return inNum;
			else if( nextChar == '.' && isDigit(aheadChar) )
				return inFloat;
			else{
				flag.setFalse ( );
				return end;
			}
		case inFloat:
			if( isDigit ( nextChar ))
				return inFloat;
			else{
				flag.setFalse ( );
				return end;
			}
		case inID:  
			// conflict occurs in logical expression, like  xxx .AND. x > y will recognize !!!!TODO how ugly Foxpro is!!! >_< I have to remove "." before scanning!!
			// 2012-03-26 已用 prescanning 来解决！
			if( isChinese(nextChar) || isLetter(nextChar)|| isDigit(nextChar) || nextChar=='_' || nextChar == '.' || nextChar == '&')
				return inID;
			else if( nextChar == '('){
				funcStack = new Stack < Character > ( );
				funcStack.push ( '(' );
				return inFunc;
			}else {
				flag.setFalse ( );
				return end;
			}
		case inFunc:
			if( nextChar == ')'){
				funcStack.pop ( );
				if( funcStack.isEmpty ( ))
					return end;
				else
					return inFunc;
			}
			else if( nextChar == '('){
				funcStack.push ( '(' );
				return inFunc;
			}else
				return inFunc;
		case inCmp1:
			if(nextChar == '=' || (backtrackChar == '<' && nextChar =='>'))
				return end;
			else{
				flag.setFalse ( );
				return end;
			}
		case inCmp2:
			if(nextChar=='>'||nextChar=='<')
				return end;
			else
			{
				flag.setFalse ( );
				return end;
			}
		case inComment1:
			if( nextChar=='&')
				return inComment2;
			else if( isLetter(nextChar) || isChinese ( nextChar ))
				return inID;
			else
				throw new Exception ( "wrong input for comment1" );
		case inComment2:
			if( nextChar =='\n'){
				flag.setFalse ( );
				return end;
			}else
				return inComment2;
		case inComment3:
			if( nextChar =='\n'){
				flag.setFalse ( );
				return end;
			}else
				return inComment3;
		case inString:
			if(nextChar=='\'')
				return end;
			else
				return inString;
		case inNewline:
			if(nextChar=='\n' || isEmptyCharacter ( nextChar ))
				return inNewline;
			else
			{
				flag.setFalse ( );
				return end;
			}
		default:
			throw new Exception( "wrong state" );
		}
	}
	
	private static boolean isEmptyCharacter ( char nextChar )
	{
		if ( nextChar ==' ' || nextChar=='\t' || nextChar=='\r' || nextChar == '\uFEFF' )
			return true;
		else
			return false;
	}

	private static boolean isSpecial( char ch)
	{
		if( ch=='+' || ch=='-' || ch=='/' || ch==',' || ch=='.' || ch=='('  || ch==')')
			return true;
		else
			return false;
	}
}
