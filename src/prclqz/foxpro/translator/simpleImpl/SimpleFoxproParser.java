package prclqz.foxpro.translator.simpleImpl;

import static prclqz.foxpro.translator.enums.TokenType.*;
import static prclqz.foxpro.translator.simpleImpl.SimpleParserSyntax.*;

import java.util.ArrayList;
import java.util.LinkedList;

import prclqz.foxpro.translator.FoxproParseTree;
import prclqz.foxpro.translator.Token;
import prclqz.foxpro.translator.enums.TokenType;
import prclqz.foxpro.translator.interf.IParser;

/**
 *  Simple Foxpro Parser using simple Top-down parsing technique 
 *  and only support the following commands:
 *  	ASSIGN EXPRESSION
 *  	LOGICAL EXPRESSION
 *  	IF THEN ELSE ENDIF
 *  	REPLE WITH FOR
 *  	SUM TO FOR
 *  	FOR
 *  	DO CASE
 *  	COPY TO ARRAY
 *  	CALCULATE
 *  
 * @author prclqz@zju.edu.cn
 *
 */
public class SimpleFoxproParser implements IParser
{
	
	@Override
	public FoxproParseTree parse ( ArrayList < Token > tokens) throws Exception
	{
		// construct the top of tree
		FoxproParseTree tree = new FoxproParseTree ( commands , null );
		// begin parsing
		LinkedList < Token > codes = new LinkedList < Token > ( tokens );
		while(codes.peek ( ).getType ( ) == newline )
			codes.pollFirst ( );
		
		if( codes.getLast ( ).getType ( )!= newline)
			codes.addLast ( new Token ( newline , 0 , null ) );
		parseCommands( tree, codes );
		
		return tree;
	}
	
	/**
	 * only support the following commands:
	 *  	ASSIGN EXPRESSION
	 *  	LOGICAL EXPRESSION
	 *  	IF THEN ELSE ENDIF
	 *  	REPLE WITH FOR
	 *  	SUM TO FOR
	 *  	FOR
	 *  	DO CASE
	 *  	COPY TO ARRAY
	 *  	CALCULATE
	 * @param tree
	 * @param codes
	 * @throws Exception 
	 */
	private void parseCommands ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		while( true )
		{
			//break if no codes
			if( codes.isEmpty ( ) )
				break;
			
			Token head = codes.peek ( );
			FoxproParseTree child;
			
			switch( head.getType ( )){
			case variable:
				child = new FoxproParseTree ( assign_expression , null );
				parseAssignExp( child, codes );
				break;
			case IF:
				child = new FoxproParseTree ( if_expression , null );
				parseIfExp( child, codes );
				break;
			case REPL:
				child = new FoxproParseTree ( repl_expression , null );
				parseReplExp( child, codes );
				break;
			case SUM:
				child = new FoxproParseTree ( sum_expression , null );
				parseSumExp( child, codes );
				break;
			case FOR:
				child = new FoxproParseTree ( for_expression , null );
				parseForExp( child, codes );
				break;
			case DO:  // if need to tranlsate the do procedure, we must revise here
				child = new FoxproParseTree ( do_expression , null );
				parseDoExp( child, codes );
				break;
			case COPY:
				child = new FoxproParseTree ( copy_expression , null );
				parseCopyExp( child, codes );
				break;
			case CALC:
				child = new FoxproParseTree ( calc_expression , null );
				parseCalcExp( child, codes );
				break;
			case ELSE: // stop parse following commands TODO anymore????
			case ENDI:
			case CASE:
			case ENDC:
			case OTHE:
			case ENDF:
				child = null;
				return; // return directly, stop parsing commands here !!
			default:
				throw new Exception ( "token parsing error: not supporting token in commands--rest:"+codes );
			}
			
			// add to children list
			tree.children.add ( child );		
		}
		
	}
	
	/***
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 * calculate value_expression_list to variable_list [for logical_expression,all] 
	 * 
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseCalcExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( CALC , codes , tree );
		matchValueExpList ( tree , codes );
		matchAndAdd2Child ( TO , codes , tree );
		matchVaribaleList ( tree , codes );
		
//		matchAndAdd2Child ( FOR , codes , tree );
//		FoxproParseTree child = new FoxproParseTree ( logical_expression , null );
//		parseLogicalExp ( child , codes );
//		tree.children.add ( child );
		
		FoxproParseTree child;
		if( codes.peek ( ).getType ( ) == FOR )
		{
			matchAndAdd2Child ( FOR , codes , tree );
			
			child = new FoxproParseTree ( logical_expression , null );
			parseLogicalExp( child , codes );
			tree.children.add ( child );
		}else if(codes.peek ( ).getType ( ) == ALL ){
			codes.poll ( );
		}
		
		
		nextCommand ( codes );
	}

	/***
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 * copy to array variable fields variable_list [for logical_expression,ALL]
	 * |
	 * copy fields variable_list to array variable [for logical_expression,ALL]
	 * 
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseCopyExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		FoxproParseTree child;
		
		matchAndAdd2Child ( COPY , codes , tree );
		
		if( codes.peek ( ).getType ( ) == TO ){
			matchAndAdd2Child ( TO , codes , tree );
			matchAndAdd2Child ( ARRA , codes , tree );
			matchAndAdd2Child ( variable , codes , tree );
			matchAndAdd2Child ( FIEL , codes , tree );
			
			matchVaribaleList ( tree , codes );
			
		}else{
			
			child = new FoxproParseTree ( logical_expression , null );
			matchAndAdd2Child ( FIEL , codes , child );
			matchVaribaleList ( child , codes );
			
			matchAndAdd2Child ( TO , codes , tree );
			matchAndAdd2Child ( ARRA , codes , tree );
			matchAndAdd2Child ( variable , codes , tree );
			
			for(int i=0; i<child.children.size ( );i++){
				tree.children.add ( child.children.get ( i ) );
			}
		}
		
		if(codes.peek ( ).getType ( ) == FOR )
		{
			child = new FoxproParseTree ( logical_expression , null );
			matchAndAdd2Child ( FOR , codes , tree );
			parseLogicalExp ( child , codes );
			tree.children.add ( child );
		}else if( codes.peek ( ).getType ( ) == ALL){
			codes.poll ( );
		}
		nextCommand ( codes );
	}

	/**
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 * do case
	 *  exp2
	 * endcase
	 * 
	 * exp2 = 
	 *  null
	 *   |
	 * 	case logical_expression1
	 * 		commands
	 * 	exp2 
	 * 	 |
	 * case logical_expression1
	 * 		commands
	 * exp3
	 * 
	 * exp3 =
	 * endcase
	 * 	|
	 * 	otherwise
	 * 		commands
	 *	endcase
	 *   
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseDoExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( DO , codes , tree );
		matchAndAdd2Child ( CASE , codes , tree );
		nextCommand ( codes );
		FoxproParseTree child;
		
		while( codes.peek ( ).getType ( ) == CASE || codes.peek ( ).getType ( ) == OTHE ){
			if(codes.peek ( ).getType ( ) == CASE)
			{
				matchAndAdd2Child ( CASE , codes , tree );
				child = new FoxproParseTree ( logical_expression , null );
				parseLogicalExp ( child , codes );
				tree.children.add ( child );
				nextCommand ( codes );
			}else{
				matchAndAdd2Child ( OTHE , codes , tree );
				nextCommand ( codes );
			}
			child = new FoxproParseTree ( commands , null );
			parseCommands ( child , codes );
			tree.children.add ( child );
		}
		
		matchAndAdd2Child ( ENDC , codes , tree );
		nextCommand ( codes );
	}

	/***
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 * FOR varialbe = value_expression to value_expression
	 * 	commands
	 * ENDOR
	 * 
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseForExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( FOR , codes , tree );
		matchAndAdd2Child ( variable , codes , tree );
		matchAndAdd2Child ( equal , codes , tree );
		
		FoxproParseTree child;
		
		child = new FoxproParseTree ( value_expression , null );
		parseValueExp( child , codes );
		tree.children.add ( child );
		
		matchAndAdd2Child ( TO , codes , tree );
		
		child = new FoxproParseTree ( value_expression , null );
		parseValueExp( child , codes );
		tree.children.add ( child );
		
		nextCommand ( codes );
		
		child = new FoxproParseTree ( commands , null );
		parseCommands ( child , codes );
		tree.children.add ( child );
		
		matchAndAdd2Child ( ENDF , codes , tree );
		nextCommand ( codes );
	}

	/***
	 *  ::::::::::::::::::::: BNF:::::::::::::::::::::
	 *  
	 *  exp = 
	 *  sum value_list to var_list [for logical_expression, ALL]
	 *  
	 *   ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseSumExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( SUM , codes , tree );
		
		matchValueExpList ( tree , codes );
		matchAndAdd2Child ( TO , codes , tree );
		matchVaribaleList ( tree , codes );
		
		
//		matchAndAdd2Child ( FOR , codes , tree );
//		FoxproParseTree child = new FoxproParseTree ( logical_expression , null );
//		parseLogicalExp ( child , codes );
//		tree.children.add ( child );

		FoxproParseTree child;
		if( codes.peek ( ).getType ( ) == FOR )
		{
			matchAndAdd2Child ( FOR , codes , tree );
			
			child = new FoxproParseTree ( logical_expression , null );
			parseLogicalExp( child , codes );
			tree.children.add ( child );
		}else if(codes.peek ( ).getType ( ) == ALL ){
			codes.poll ( );
		}
		
		nextCommand ( codes );
	}

	/**
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 *  REPL sub_repl [for logical_expression, ALL]
	 * 
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseReplExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( REPL , codes , tree );
		
		FoxproParseTree child = new FoxproParseTree ( sub_replace , null );
		parseSubreplExp( child , codes );
		tree.children.add ( child );
		
		if( codes.peek ( ).getType ( ) == FOR )
		{
			matchAndAdd2Child ( FOR , codes , tree );
			
			child = new FoxproParseTree ( logical_expression , null );
			parseLogicalExp( child , codes );
			tree.children.add ( child );
		}else if(codes.peek ( ).getType ( ) == ALL ){
			codes.poll ( );
		}
		
		nextCommand( codes );
	}

	/***
	 *  ::::::::::::::::::::: BNF:::::::::::::::::::::
	 *  
	 *  exp =
	 *  variable with value_expression exp2
	 *  
	 *  exp2 = 
	 *   null |
	 *  , variable with value_expression exp2
	 *  
	 *   ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseSubreplExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		FoxproParseTree child;
		codes.addFirst ( new Token(comma,0,null) );
		
		while( codes.peek ( ).getType ( )==comma )
		{
			codes.pollFirst ( );
			matchAndAdd2Child ( variable , codes , tree );
			matchAndAdd2Child ( WITH , codes , tree );
			
			child = new FoxproParseTree ( value_expression , null );
			parseValueExp( child , codes );
			tree.children.add ( child );			
		}
	}

	
	/**
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 *   IF logical_expression [THEN] 
	 *   	commands
	 *   [ELSE]
	 *   	commands
	 *   ENDIF
	 * 
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception 
	 */
	private void parseIfExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( IF , codes , tree );
		
		FoxproParseTree child = new FoxproParseTree ( logical_expression , null );
		parseLogicalExp( child , codes );
		tree.children.add ( child );
		
		// skip the non-essential token
		if( codes.peek ( ).getType ( ) == THEN )
			codes.pollFirst ( );
		
		nextCommand( codes );
		
		child = new FoxproParseTree ( commands , null );
		parseCommands( child , codes );
		tree.children.add ( child );
		
		if( codes.peek ( ).getType ( ) == ELSE ){
			tree.children.add ( new FoxproParseTree ( ternimal , codes.pollFirst ( ) ) );
			nextCommand( codes );
			
			child = new FoxproParseTree ( commands , null );
			parseCommands( child , codes );
			tree.children.add ( child );
		}
		
		matchAndAdd2Child ( ENDI , codes , tree );
		nextCommand( codes );
	}

	/**
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * exp = 
	 *  value_expressin =(!=,>,>=,<,<=)|
	 * 	value_expressin [.] and(or) [.]  exp
	 * 
	 * TODO NO nested situation LIKE (( a = b and b <= a) or c = d )
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseLogicalExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		codes.addFirst ( new Token ( AND , 0 , null ) );
		while( codes.peek ( ).getType ( ) == AND || codes.peek ( ).getType ( )==OR){
			
			tree.children.add ( new FoxproParseTree ( ternimal , codes.pollFirst ( ) ) );
			if( codes.peek ( ).getType ( ) == dot )
				codes.pollFirst ( );
			
			FoxproParseTree child = new FoxproParseTree ( value_expression , null );
			parseValueExp( child , codes );
			tree.children.add ( child );
			
			matchLogicalOpr ( tree , codes );
			
			child = new FoxproParseTree ( value_expression , null );
			parseValueExp( child , codes );
			tree.children.add ( child );

			if( codes.peek ( ).getType ( ) == dot )
				codes.pollFirst ( );
		}
		
	}

	private void matchLogicalOpr( FoxproParseTree tree ,
			LinkedList < Token > codes)throws Exception
	{
		TokenType type = codes.peek ( ).getType ( );
		if( type == equal  || type == notequal || type == bigger || type == biggerOrEq || type == less || type == TokenType.lessOrEq)
		{
			tree.children.add ( new FoxproParseTree ( ternimal , codes.pollFirst ( ) ) );
		}else
			throw new Exception( "error token for math logical operator " );
	}
	/**
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 *  variable = value_expression
	 *  
	 *  ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * @param tree
	 * @param codes
	 * @throws Exception
	 */
	private void parseAssignExp ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		matchAndAdd2Child ( variable , codes , tree );
		matchAndAdd2Child ( equal, codes , tree );
		
		FoxproParseTree child = new FoxproParseTree ( value_expression , null );
		parseValueExp( child, codes );
		
		tree.children.add ( child );
		
		nextCommand( codes );
	}

	
	/**
	 *::::::::::::::::::::: BNF:::::::::::::::::::::
	 * exp 	=   
	 * 	(exp) |
	 * 	exp +(-, *, /) exp |
	 * 	var(digit,func() )
	 * ::::::::::::::::::::: BNF:::::::::::::::::::::
	 * 
	 * BUT since foxpro 2 Java can be directly done in this expression ,
	 * so we don't need to parse into the sub tree!
	 * 
	 * @param child
	 * @param codes
	 */
	private void parseValueExp ( FoxproParseTree tree ,
			LinkedList < Token > codes )
	{
		Token head = codes.peek ( );
		while( isValueExpToken( head) )
		{
			tree.children.add ( new FoxproParseTree ( ternimal , head ) );
			codes.pollFirst ( );
			head = codes.peek ( );
		}
	}

	private void nextCommand( LinkedList < Token > codes ) throws Exception{
		if( codes.pollFirst ( ).getType ( ) != newline ){
//			System.out.println(codes);
			throw new Exception ( "error parsing nextline ");
		}
	}
	
	private boolean isValueExpToken ( Token head )
	{
		TokenType type = head.getType ( );
		if( type == variable || type == number || type == string || type == left_parent || type == right_parent 
				|| type == plus || type == minus || type == multi || type == divide )
			return true;
		else
			return false;
	}
	
	private void matchValueExpList( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception{
		FoxproParseTree child;
		codes.addFirst ( new Token ( comma , 0 , null ) );
		while ( codes.peek ( ).getType ( ) == comma ) 
		{
			codes.pollFirst ( );
			child = new FoxproParseTree ( value_expression , null );
			parseValueExp ( child , codes );
			tree.children.add ( child );
		}
	}
	
	private void matchVaribaleList ( FoxproParseTree tree ,
			LinkedList < Token > codes ) throws Exception
	{
		codes.addFirst ( new Token ( comma , 0 , null ) );
		while ( codes.peek ( ).getType ( ) == comma ) 
		{
			codes.pollFirst ( );
			matchAndAdd2Child ( variable , codes , tree );
		}
	}
	
	private void matchAndAdd2Child(TokenType type, LinkedList < Token > codes ,FoxproParseTree tree) throws Exception{
		matchToken ( type , codes );
		tree.children.add ( new FoxproParseTree ( ternimal , codes.pollFirst ( ) ) );
	}
	
	private void matchToken ( TokenType type , LinkedList < Token > codes ) throws Exception
	{
		if( type != codes.peekFirst ( ).getType ( ) )
			throw new Exception ( "token not match for expecting a "+type +" but encounter a "+codes.peek ( ).getType ( )+"--"+codes);
	}

	/**
	 * test the genLines function
	 * @param rs
	 * @param codes
	 */
	public static void testGenLines( int[][] rs, ArrayList<Token> codes){
		for(int i=0; i<rs.length;i++){
			for(int j=rs[i][0];j<=rs[i][1];j++)
				System.out.print( " "+codes.get ( j ));
			System.out.println();
		}
	}
	
	
	@Override
	public int [ ][ ] genLines ( ArrayList < Token > codes )
	{
		int[] arr = new int[codes.size ( )];
		for(int i=0;i<arr.length;i++){
			if( codes.get ( i ).getType ( ) == newline )
				arr[i] = 0;
			else
				arr[i] = 1;
		}
		int[][] rs = getSeperatedItems ( arr , 0 );

		return rs;
	}

	
	/**
	 * none-recursive version, efficient but maybe hard to understand the codes= =
	 * @param arr
	 * @param delim
	 * @return
	 */
	private static int[][] getSeperatedItems(int[] arr, int delim){
		ArrayList<Integer> begins = new ArrayList<Integer>(),ends = new ArrayList<Integer>();
		int last = 0;
		while(arr[last]== delim){
			last ++;
			if(last ==arr.length){
				break;
			}
		}
		if(last == arr.length)
			return new int[0][0];
		
		int j = last+1;
		while(j<= arr.length){
			if(j== arr.length || arr[j] == delim){
				begins.add(last);
				ends.add(j-1);
				
				last = j+1;
				if( last >= arr.length)
					break;
				while(arr[last]== delim){
					last ++;
					if(last ==arr.length){
						last = -1;
						break;
					}
				}
				if(last == -1)
					break;
				else
					j= last+1;
			}else{
				j++;
			}
		}
		int len = begins.size();
		int[][] rs = new int[len][2];
		for(int i=0;i<len;i++){
			rs[i][0] = begins.get(i);
			rs[i][1] = ends.get(i);
		}
		return rs;
	}
}
