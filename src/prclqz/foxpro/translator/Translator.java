package prclqz.foxpro.translator;
import static java.lang.System.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;


import prclqz.foxpro.translator.interf.IFoxpro2Java;
import prclqz.foxpro.translator.interf.IParser;
import prclqz.foxpro.translator.interf.IScanner;
import prclqz.foxpro.translator.simpleImpl.SimpleFoxpro2Java;
import prclqz.foxpro.translator.simpleImpl.SimpleFoxproParser;
import prclqz.foxpro.translator.simpleImpl.SimpleFoxproScanner;


/**
 * -----------------------------------------
 * 2012-04-15
 * 最后一步增加 RegularMap中的正则表达式来替换
 * 
 * -----------------------------------------
 * 2012-03-28
 *  REPLACE语句的翻译功能增加了 put类型。即若要赋值的
 *  类型不是数组(最后用[])而是用...put(xxx,xxx)来赋值的，
 *  需要做额外的判断
 *  
 * -----------------------------------------
 * 2012-03-26
 * 用pre-scanning来解决 .And. .OR. .and. .or. 的scanning错误
 * 
 * ---------------------------------------
 * Revised by JackLong on 2012-03-21 and 03-22:
 * Now support:
 *   ; then change line syntax
 *   float number
 * @author prclqz@zju.edu.cn
 *
 *-------OLD------------------------
 * 略去 comments是在scanning中，其实也可以在 pre-scanning中
 * NOT suport:
 *    
 *    ';'then change line
 *    float number
 * @author prclqz@zju.edu.cn
 *
 */
public class Translator {
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//1. input UTF-8 text code
		final String utf8File=new String("I:\\prclqz\\HyperV-XP-D\\Foxpro2JavaTraslator2.txt");
		StringBuffer sbContent = readUTF8FfromFile( utf8File );
//		out.print(new String(sbContent));
		
		
		/**********************************************************************
		 * now only implement the SimpleScanner,SimpleParser and SimpleFoxpro2Java
		 * but maybe simple is the best! ^_^ 
		 *********************************************************************/
		
		//2. scan string code into tokens, skip white character and comments
		IScanner scanner = new SimpleFoxproScanner ( );
		ArrayList < Token > tokens = scanner.scan ( sbContent.toString ( ) );
//		for( int i=0; i<tokens.size ( ); i++)
//			out.println( tokens.get ( i )+" type:"+tokens.get ( i ).getType ( ));
		
		//2.5 test the token--by line
		IParser parser = new SimpleFoxproParser ( );
//		int [][] lines = parser.genLines ( tokens );
//		out.println("**\n");
//		SimpleFoxproParser.testGenLines ( lines , tokens );
//		System.exit ( 0 );
		
		//3. parse into Foxpro syntax tree
		FoxproParseTree foxtree = parser.parse ( tokens );
//		out.println( foxtree );
		
		//4.translate into Java codes
		HashMap<String, String> varMap= VarMapData.getVarMap();
		IFoxpro2Java fox2java = new SimpleFoxpro2Java ( varMap );
		List < String > javacodes = fox2java.foxpro2Java ( foxtree );
		
		HashMap<String, String > regs = RegularMap.getMap();
		//5. replace By Reg and output the result
		out.println("//////////////translated by Foxpro2Java Translator successfully:///////////////");
		for( int i=0; i<javacodes.size ( ); i++){
			Iterator <Entry<String,String>> it =regs.entrySet().iterator();
			String tmp = javacodes.get ( i );
			while(it.hasNext()){
				Entry< String , String > ele = it.next();
				String k = ele.getKey();
				String v = ele.getValue();
				tmp = tmp.replaceAll( k , v );
			}
			out.println( tmp );
		}
		out.println("/////////////end of translating, by Foxpro2Java Translator///////////////");
		
		
	}

	private static StringBuffer readUTF8FfromFile ( String utf8File ) throws Exception
	{
		FileInputStream fis=new FileInputStream(utf8File);
		InputStreamReader isr=new InputStreamReader(fis,"UTF-8");
		BufferedReader br=new BufferedReader(isr);
		StringBuffer sbContent=new StringBuffer();
		String sLine;
		while((sLine=br.readLine())!=null){
		  sbContent.append(sLine);
		  sbContent.append ( '\n' );
		}
		return sbContent;
	}

}
