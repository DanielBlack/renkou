package prclqz.foxpro.translator;

public class Tools
{
	public static boolean isChinese( char ch)
	{
		byte[] bts = (""+ch).getBytes ( );
		if(bts.length >1 )
			return true;
		else
			return false;
	}
	
	public static boolean isDigit( char ch )
	{
		if( ch>= '0' && ch<='9')
			return true;
		else
			return false;
	}
	
	public static boolean isLetter( char ch )
	{
		if( (ch>= 'a' && ch<='z') || (ch>= 'A' && ch<='Z'))
			return true;
		else
			return false;
	}
	
}
