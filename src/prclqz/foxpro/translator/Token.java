package prclqz.foxpro.translator;

import static prclqz.foxpro.translator.enums.TokenType.*;
import prclqz.foxpro.translator.enums.TokenType;

public class Token
{
	private TokenType type;
	private double numval;
	private String strval;
	
	public TokenType getType ( )
	{
		return type;
	}
	protected double getInval ( )
	{
		return numval;
	}
	public String getStrval ( )
	{
		return strval;
	}
	public Token ( TokenType type , double dbval , String strval )
	{
		super ( );
		this.type = type;
		this.numval = dbval;
		this.strval = strval;
	}
	
	public String toString(){
		if( type == number )
			return ""+numval;
		else if( type == variable || type == comment )
			return strval;
		else
			return type.toString ( );
	}
	
}
