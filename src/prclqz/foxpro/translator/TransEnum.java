package prclqz.foxpro.translator;

import static java.lang.System.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class TransEnum
{

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		//1. input UTF-8 text code
		String utf8File=new String("D:\\prclqz\\PeopleSim\\Enum\\hunpei_e.txt");
		StringBuffer e_sbContent = readUTF8FfromFile( utf8File );
		
		utf8File=new String("D:\\prclqz\\PeopleSim\\Enum\\hunpei_c.txt");
		StringBuffer c_sbContent = readUTF8FfromFile( utf8File );
		
		String[] es = e_sbContent.toString ( ).split ( "\\,|\\t" ); //>0
//		String[] cs = c_sbContent.toString ( ).split ( "\\`" );  //>0 && indexOf(',') == -1
		String[] cs = c_sbContent.toString ( ).split ( "\\,|\\t" );  //>0 && indexOf(',') == -1
		
		ArrayList <String> evs = new ArrayList < String > ( ); 
		ArrayList <String> cvs = new ArrayList < String > ( ); 
		for(String s:es){
//			out.println(s + "---"+s.length ( ));
			if( s.length ( ) >0 )
				evs.add ( s );
		}
		for(String s:cs){
//			out.println(s + "---"+s.length ( ));
			if( s.length ( )>0 && s.indexOf ( ',' ) == -1)
				cvs.add ( s );
		}
//		out.println(evs.size ( )+"=="+cvs.size ( ));
//		for(int i=0;i<24;i++){ 
//			out.println( evs.get ( i )+"---"+cvs.get ( i ));
//		}
//		System.exit ( 0 );
		for(int i=0;i<196;i++){ 
			// Couple:92  PolicyBirth:34 	BabiesBorn:94  Summary:112
			// SumBM:24 Abstract:29
			// Hunpei 196
//			out.println( evs.get ( i )+"---"+cvs.get ( i ));
			out.println("case "+evs.get ( i )+" :");
			out.println("return \""+cvs.get ( i )+"\";");
		}

	}
	private static StringBuffer readUTF8FfromFile ( String utf8File ) throws Exception
	{
		FileInputStream fis=new FileInputStream(utf8File);
		InputStreamReader isr=new InputStreamReader(fis,"UTF-8");
		BufferedReader br=new BufferedReader(isr);
		StringBuffer sbContent=new StringBuffer();
		String sLine;
		while((sLine=br.readLine())!=null){
		  sbContent.append(sLine);
//		  sbContent.append ( '\n' );
		}
		return sbContent;
	}
}
