package prclqz.Database;
/**
 * Author: prclqz@zju.edu.cn
 * 
 * 1st edition:2011.08.24-普通单态模式
 * 2nd edition:2011.10.13-支持PrepareStatement,并将配置仿真conf/mysql.conf中
 */

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.MainOperationHistoryBean;

public class MySQLDB {
	//单态模式数据库连接
	private static MySQLDB db;
	//注意！mysql.conf必须要用ANSI编码！
	private static String mysql_conf = "conf/mysql.conf";
	//由于ANSI编码中中文的数据库名会变成乱码，故在这里设置！
	private static String mysql_db = "新人口学";
	private String driver;
	private String url;
	private String user;
	private String pass;
	private String other;
	private Connection conn;
	private Statement stmt;
	private PreparedStatement pstmt;

	public void initParam(String paramFile)throws Exception
	{
		//使用Properties类来加载属性文件
		Properties props = new Properties();
		props.load(new FileInputStream(paramFile));
		driver = props.getProperty("driver");
		url = props.getProperty("url");
		other = props.getProperty("other");
		user = props.getProperty("user");
		pass = props.getProperty("pass");
		
		url = url+mysql_db+other;
	}
	
	private MySQLDB()
	{}
	public static MySQLDB  instance()
	{
		if(db == null)
		{
			db = new MySQLDB();
			try {
				db.initParam(mysql_conf);
			} catch (Exception e) {
				System.err.println("创建数据库对象 异常！(是不是配置文件错误？)"+e.getMessage());
				e.printStackTrace();
			}
		}
		return db;
	}
	
	public void getConn()throws Exception
	{
		if (conn == null)
		{
			//加载驱动
			Class.forName(driver);
			//获取数据库连接
			conn = DriverManager.getConnection(url , user , pass);
		}
	}

	
	public static void main(String[] args) throws Exception {
		
		MyDAOImpl m = new MyDAOImpl();
		MainOperationHistoryBean h;
		h = m.getHistory(1);
		System.out.println(h.getValue());
		
		
//		MySQLDB db = MySQLDB.instance();
//		Statement stmt = db.openStmt();
//		ResultSet rs;
//		
//		rs = stmt.executeQuery("select * from test");
//		// 3.5. 显示结果集里面的数据
//		while (rs.next()) {
//			System.out.println("编号=" + rs.getInt("id"));
//			System.out.println("年龄=" + rs.getInt("value1"));
//		}
		
	}
	

	
	public Statement openStmt() 
	{
		if(stmt == null)
		{
			if(conn==null)
				try {
					this.getConn();
				} catch (Exception e1) {
					System.err.println("创建CONN 异常！"+e1.getMessage());
					e1.printStackTrace();
				}
			try
			{
				stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			}catch(Exception e)
			{
				System.err.println("创建Statement 异常！"+e.getMessage());
			}
		}
		return stmt;
	}
	
	public PreparedStatement openPreStmt(String sql) throws Exception
	{
		if(pstmt == null)
		{
			if(conn==null)
				this.getConn();
			pstmt = conn.prepareStatement(sql);
		}
		return pstmt;
	}
	
	public void close()
	{
		if(stmt != null)
		{
			try{
				stmt.close();
			}catch(Exception e)
			{
				System.err.println("关闭Statement异常");
			}
		}
		if(conn != null)
		{
			try{
				conn.close();
			}catch(Exception e)
			{
				System.err.println("数据库关闭异常");
			}
		}
	}
	
	


	
}
