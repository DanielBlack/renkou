package prclqz.core;

public class Message {
	private boolean result;
	private String  messageString;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getMessageString() {
		return messageString;
	}
	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}
	public Message(boolean result, String messageString) {
		super();
		this.result = result;
		this.messageString = messageString;
	}
	
}
