package prclqz.core;


import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.MethodBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.enumLib.DuiJiGuSuanFa;
import prclqz.core.enumLib.ZCImpl;
import prclqz.methods.IAbstractOfNationMethod;
import prclqz.methods.IAbstractOfPredictMethod;
import prclqz.methods.IBabiesBornByDuijiMethod;
import prclqz.methods.IBabiesBornByFutureMethod;
import prclqz.methods.IBabiesBornByParentTypeMethod;
import prclqz.methods.IBabiesBornMethod;
import prclqz.methods.IBabiesRateMethod;
import prclqz.methods.IBabiesReleaseClassifyMethod;
import prclqz.methods.ICalculateExpectedAgeMethod;
import prclqz.methods.IDeathCalculatingMethod;
import prclqz.methods.IGroupingByAgeMethod;
import prclqz.methods.IMainExtremeValuesMethod;
import prclqz.methods.IMethod;
import prclqz.methods.INationalBornRateMethod;
import prclqz.methods.IRecordNationalPolicyAndPolicyRateMethod;
import prclqz.methods.IResultOfAbstractMethod;
import prclqz.methods.MethodException;
import prclqz.parambeans.ParamBean3;
import prclqz.view.DefaultView;
import prclqz.view.IView;
import static prclqz.core.enumLib.Policy.*;
import static prclqz.core.enumLib.ZCImpl.*;
import java.util.*;
import test.Dumper;

import danielblack.patch.*;

import org.omg.CORBA.Environment;
/**
 * 主类
 * 人口仿真
 * @author prclqz@zju.edu.cn
 *
 */
public class PeopleSimulation {

	private static IView v;
	private static IDAO m;
	private static MainTaskBean task;
	private static HashMap<String,MethodBean> methods;
	
	// !!! important object !!!全局变量表，注意 Key的命名要表现出意义和类型
	private static HashMap<String,Object> globals;
	/**
	 * 已有的变量记录：
	 *  taskBean  			任务表映射表bean
	 *  tempVarBean  		重要循环参数和临时变量映射表bean,一部分与数据库对应  
	 *  bornXbbBeanMap  	在“基本参数基础数据”中出现的全局变量
	 *  mainMeanMap  		主参数对应表内容映射 [mmm.put(rs.getString("代表参数")+"-"+rs.getString("描述表类"), rs.getInt("id"));]
	 *  SetUpEnvParamBean 	MSetUpEvn方法参数
	 *  view				全局视图工具(当前为DefaultView的实例对象)
	 *  provinceMigMap      分省迁移,全国迁移结果 -----废弃！
	 *  						1) ""+dqx => Map<CX, Map<NY, Map<HunpeiField, int[]>>> ziNv ;
	 *  						2) ""+0=> Map<QYPHTotal, Integer> qianyiTotal
	 *  predictVarMap		全国预测数据表和地区数据表的准备表结构变量   
	 *  						1)"ShengYuMoShi"+dqx => Map<CX,Map<SYSFMSField,double[]>> shengYuMoShi								
	 *  strValues			由于Enmap表都统一是Double类型，因此若保存字符串，都保存此表对应的ID								
	 */
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		long start = System.currentTimeMillis();
		
		//创建程序输出实例
		v = new DefaultView();
		//创建数据库访问模型实例
		m = new MyDAOImpl();
//		System.exit(0);
		
		Message msg;
		
		//判断任务名是否合法,不合法则退出
		if(  !isTask(args) ){
			if(args.length==0)
				v.outputI(0, 0,"");
			else	
				v.outputI(0, 1,args[0]);
			
			close();
			System.exit(0);
		}
		//获取任务,启动
		task = m.getTask(args[0]);
		v.setTaskName(task.getName());
		
		//TODO 判断任务状态，提示任务将覆盖旧结果
		
		//开始计算
		v.outputI(0, 2);
		
		//获取任务的执行方法合集
		methods = getMethodsMap(task.getId());
		
		//设置全局变量表
		globals = new HashMap<String, Object>();
		globals.put("taskBean", task);
		globals.put("view", v);
		
		//开始计算任务 TODO:根据任务选择的政策选择不同的调度方法——反射调用类的静态方法技术
		if(methods != null)
			msg = calculatePuEr();
		
		//TODO 判断msg输出
		
		//退出任务
		close();
		
		System.out.println("费时:"+ (System.currentTimeMillis() - start));
	}

	//核心调度：调度各种方法的代码——“渐进普二政策”
	private static Message calculatePuEr() throws Exception {
		
		//先手动获取方法，其实可以自动获取- -
		
		//first level
		IMethod setUpEnv 	  =  methods.get("SetUpEnv").getClsIns();
		IMethod pMigration 	  =  methods.get("ProvinceMigration").getClsIns();
		IMethod pNoMigration  =  methods.get("ProvinceNoMigration").getClsIns();
		IMethod nMigration 	  =  methods.get("NationalMigration").getClsIns();
		IMethod prepareData   =  methods.get("PreparePredictionData").getClsIns();
		IMethod calXbbForAll  =  methods.get("CalculateXBB").getClsIns();
		IMethod confirmSFMS   =  methods.get("ConfirmSFMS").getClsIns();
		IMethod puErBirth     =  methods.get("SetBirthRateOfPuErPolicy").getClsIns();
		IMethod agingCal	  =  methods.get("AgingCalculation").getClsIns();
		IMethod marriage	  =  methods.get("MarriageOfAgePattern").getClsIns();
		IMethod depositCp	  =  methods.get("DepositedCouples").getClsIns();
		IBabiesBornMethod babiesBorn	  =  (IBabiesBornMethod) methods.get("BabiesBornByPolicy").getClsIns();
			// second level procedures
			IBabiesBornByParentTypeMethod bornByParentType = ( IBabiesBornByParentTypeMethod ) methods.get("BabiesBornByParentType").getClsIns();
			IBabiesBornByDuijiMethod bornByDuiji		 = ( IBabiesBornByDuijiMethod ) methods.get("BabiesBornByDuiji").getClsIns();
			IBabiesRateMethod babiesRate		 = ( IBabiesRateMethod ) methods.get("BabiesRate").getClsIns();
			IBabiesReleaseClassifyMethod bReleaAClssfy	 = (IBabiesReleaseClassifyMethod) methods.get("BabiesReleaseAndClassifyBabies").getClsIns();
				//third level procedures
				IBabiesBornByFutureMethod classification   = ( IBabiesBornByFutureMethod ) methods.get("BabiesCassificationByFuturePolicy").getClsIns();
			
		IMethod bBornSum = methods.get ( "BabiesBornSum" ).getClsIns ( );
		IMethod sumByAge = methods.get ( "SumAllByAge" ).getClsIns ( );
		IBabiesBornMethod bBornByHG = ( IBabiesBornMethod ) methods.get ( "BabiesBornByHuigui" ).getClsIns ( );
		IBabiesBornMethod bBornByYY = ( IBabiesBornMethod ) methods.get ( "BabiesBornByYiyuan" ).getClsIns ( );
		IMethod singleAndNot = methods.get ( "SingleAndNotSingle" ).getClsIns ( );
		IMethod estimateP = methods.get ( "EstimateOfYihaiAndTefuParent" ).getClsIns ( );
		IMethod sumOfTefuP 	  = methods.get ( "SumOfDqTefuParent" ).getClsIns ( );
		IMethod dqImplRate= methods.get ( "DqImplementableBornRate" ).getClsIns ( );
		IMethod	marryResult=methods.get ( "MarrageResultOfCopy" ).getClsIns ( );
		IMethod recordsPly =methods.get ( "RecordPolicyAndPolicyRate" ).getClsIns ( );
		IMethod sumOfBornB = methods.get ( "SumOfBornBabies" ).getClsIns ( );
		
		IAbstractOfPredictMethod absOfPred = ( IAbstractOfPredictMethod ) methods.get ( "AbstractOfPredict" ).getClsIns ( );
			//second level
			IDeathCalculatingMethod deathCal = ( IDeathCalculatingMethod ) methods.get ( "DeathCalculating" ).getClsIns ( );
			ICalculateExpectedAgeMethod calAge= ( ICalculateExpectedAgeMethod ) methods.get ( "CalculateExpectedAge" ).getClsIns ( );
			IGroupingByAgeMethod groupAge = ( IGroupingByAgeMethod ) methods.get ( "GroupingByAge" ).getClsIns ( );
			IResultOfAbstractMethod resultAbs = ( IResultOfAbstractMethod ) methods.get ( "ResultOfAbstract" ).getClsIns ( );
			IMainExtremeValuesMethod mainVals = ( IMainExtremeValuesMethod ) methods.get ( "MainExtremeValues" ).getClsIns ( );
		
		IMethod sumOfNat  = methods.get ( "SumOfNation" ).getClsIns ( );
		IMethod sumOfYihaiTefuP = methods.get ( "SumOfYihaiAndTefuParent" ).getClsIns ( );
		IAbstractOfNationMethod absOfNat  = (IAbstractOfNationMethod) methods.get ( "AbstractOfNation" ).getClsIns ( );
			INationalBornRateMethod nationBRate = (INationalBornRateMethod) methods.get( "NationalBornRate" ).getClsIns();
			IRecordNationalPolicyAndPolicyRateMethod recordPolicy = (IRecordNationalPolicyAndPolicyRateMethod) methods.get( "RecordNationalPolicyAndPolicyRate" ).getClsIns();
			
		IMethod savingData = methods.get( "SavingCalculatedData" ).getClsIns();
		
		//1.检查数据完整性和导入重要数据到变量中
		//TODO 循环执行所有方法的checkDataBase,并把setUpEnv.checkDatabase()的一部分内容移到这些地方
		Message msg = setUpEnv.checkDatabase(m,globals);
		if(! msg.isResult()){
			v.outputI(1, msg.getMessageString());
			return msg;
		}
		
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		ParamBean3 envparam = (ParamBean3) globals.get("SetUpEnvParamBean");
		//设置调整方式时机
		tempVar.adjustType = "多地区渐进普二";
		
		//设置实现比
		tempVar.setImplRate(0);
		//设置城市化水平
		tempVar.setCshLevel(0);
		//设置堆积估计法-传入枚举常量
		tempVar.setTJ( DuiJiGuSuanFa.cunhuoYihaiGujiFa );
		
		//2.政策、年份循环选择——当前只实现一种
		m.setTempVariable(tempVar, "FeiNongPolicy1"	, xianXing.getId()	, task.getId());
		m.setTempVariable(tempVar, "FeiNongTime1"	, 0					, task.getId());
		m.setTempVariable(tempVar, "FeiNongPolicy2"	, danDu.getId()		, task.getId());
		m.setTempVariable(tempVar, "FeiNongTime2"	, 2011				, task.getId());
		m.setTempVariable(tempVar, "FeiNongPolicy3"	, puEr.getId()		, task.getId());
		m.setTempVariable(tempVar, "FeiNongTime3"	, 2015				, task.getId());
		m.setTempVariable(tempVar, "QY"				, 2					, task.getId()); //0无 1低  2中 3高
		//m.setTempVariable(tempVar, "ZCimplement"	, 144				, task.getId()); //回归估计
		//m.setTempVariable(tempVar, "SFMS"			, 150				, task.getId()); //意愿模式
		m.setTempVariable(tempVar, "ZCimplement"	, 2					, task.getId()); //回归估计  Fixed by danielblack
		m.setTempVariable(tempVar, "SFMS"			, 1					, task.getId()); //意愿模式  Fixed by danielblack
		
		//TODO 设置农业参数——农业政策，农业时机，目前都保持和非农政策，非农时机一致！
		setNongYeParam(tempVar);
		
		//实现程度估计
		ZCImpl implEstm = ZCImpl.getZCImplFromId ( (int)(tempVar.getZCimplement()) );
		
		
		//循环年份 2010-2100
		for(int year = envparam.getBegin(); year <= envparam.getEnd() ; year ++)
		{	
			System.out.println("NOW complete:"+ (year-2010.0)/90.0*100.0 +"%! Year:"+year );
			
			m.setTempVariable(tempVar, "Year"			, year				, task.getId()); 
			
			//5.全国预测数据表 和 地区基础数据和预测文件(提取各种原始数据到全局变量中)——只在第一年这么做！
			if( tempVar.year == envparam.getBegin() )
				prepareData.calculate(m, globals);
			
		
			if(tempVar.getQY() > 0){
				//3.地区迁移
				pMigration.calculate(m,globals);
				//4.全国迁移平衡
				nMigration.calculate(m,globals);
			}else{
				//3.地区无迁移
				pNoMigration.calculate(m,globals);
			}
			
			//6.计算所有省份的性别比(本在后面的省份循环中，为提高效率和易维护性专门改成子程序)
			calXbbForAll.calculate(m, globals);
			tempVar.setSQY(0);
			// loop to calculate for every province
			HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
			int dqx;
			for(dqx=envparam.getDq1();dqx<=envparam.getDq2();dqx++)
			{
				HunpeiInitPatch.patch(globals, dqx);
				//System.out.println(dqx);
				if(!xbbMap.containsKey(""+dqx))
					continue;
				//to be coherent with the database
				m.setTempVariable(tempVar, "Province", dqx, task.getId());
				BornXbbBean xbb = xbbMap.get(""+dqx);
				tempVar.landMJ = xbb.getLand();
				tempVar.fieldMJ = xbb.getField();
				tempVar.waterS = xbb.getWater();
				
				/* 
				 * TEST 
				 */
				//Dumper dumper = new Dumper(globals, dqx);
				
				
				//7.释放模式确定
				confirmSFMS.calculate(m, globals);
				//8.渐进普二终身政策生育率
				//dumper.dump();
				puErBirth.calculate(m, globals);
				//9.年龄移算
				//dumper.dump();
				agingCal.calculate(m, globals);
				//10. 婚配
				//dumper.dump();
				marriage.calculate(m, globals);
				//11.堆积夫妇估计与剥离 (在大于或等于 政策调整时机 时执行)
				if(tempVar.getYear() >= tempVar.getPolicyTime())
					depositCp.calculate(m, globals);
				//dumper.dump();
				//12.分婚配按政策生育的孩子数(包含二级调度,releaAClssfy包含三级调度)
				babiesBorn.calculate( m , globals , bornByParentType , bornByDuiji ,
						babiesRate , bReleaAClssfy , classification );
				//17 各类夫妇生育孩子数
				//dumper.dump();
				bBornSum.calculate ( m , globals );
				//dumper.dump();
				//18 分龄人口合计
				sumByAge.calculate ( m , globals );
				//19-20 分婚配按     回归估计  或意愿  生育的孩子数 
				//dumper.dump();
				if ( implEstm == ByEstimation )
					bBornByHG.calculate( m , globals , bornByParentType ,
							bornByDuiji , babiesRate , bReleaAClssfy ,
							classification );
				else
					bBornByYY.calculate( m , globals , bornByParentType ,
							bornByDuiji , babiesRate , bReleaAClssfy ,
							classification );
				//dumper.dump();
				//18 分龄人口合计
				sumByAge.calculate ( m , globals );
				//dumper.dump();
				//21  独生子女和非独生子女人数调整(孩次递进法)
				singleAndNot.calculate ( m , globals );
				//dumper.dump();
				//22 分龄一孩父母和特扶父母估计
				estimateP.calculate ( m , globals );
				//dumper.dump();
				//23 地区特扶父母统计
				sumOfTefuP.calculate ( m , globals );
				//24 地区多种政策和可实现时期生育率
				//dumper.dump();
				dqImplRate.calculate ( m , globals );
				//25 考贝地区分年龄婚配结果
				marryResult.calculate ( m , globals );
				//26 记录各类夫妇政策内容和政策生育率
				recordsPly.calculate ( m , globals );
				//27 生育孩次合计
				sumOfBornB.calculate ( m , globals );
				//28 地区城乡预测结果摘要
				absOfPred.calculate( m , globals , deathCal , calAge , groupAge ,
					resultAbs , mainVals );
				//29 逐层全国地区累计
				//dumper.dump();
				sumOfNat.calculate ( m , globals );
				//dumper.dump();
				//30 全国一孩父母和特扶父母统计
				sumOfYihaiTefuP.calculate ( m , globals );
				//31 年度全国摘要
				absOfNat.calculate( m , globals , babiesRate , nationBRate ,
					recordPolicy , deathCal , calAge , groupAge , resultAbs ,
					mainVals );
			} // End of Province Loop
		
			
			//TODO 保存当年 计算的结果数据
			if( tempVar.year == envparam.getEnd() )	
			savingData.calculate( m , globals );
			
		}//END Year Loop
		
		
		
		
		////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		//查看剩余内存
		Runtime rt = Runtime.getRuntime();
		long fm = rt.freeMemory();
		long tm = rt.totalMemory();
		System.out.println("remaining :"+fm/1000000+"MB, total :"+tm/1000000+"MB");
		
		return null;
	}

	private static void setNongYeParam ( TempVariablesBean tv )
	{
		tv.nongYePolicy1 = tv.feiNongPolicy1;
		tv.nongYePolicy2 = tv.feiNongPolicy2;
		tv.nongYePolicy3 = tv.feiNongPolicy3;
		
		tv.nongYeTime1 = tv.feiNongTime1;
		tv.nongYeTime2 = tv.feiNongTime2;
		tv.nongYeTime3 = tv.feiNongTime3;
	}

	private static HashMap<String,MethodBean> getMethodsMap(int taskId) throws Exception 
	{
		HashMap<String,MethodBean> mmp = new HashMap<String, MethodBean>();
		MethodBean[] methods=m.getMethods(taskId);
		Class<IMethod> cls;
		IMethod method;
		for(MethodBean mb:methods){
			//System.out.println(mb.getMethod());
			//关键代码：加载每个方法所对应的类
			try{
				cls = (Class<IMethod>) Class.forName("prclqz.methods.M"+mb.getMethod());
				//实例化一个对象
				method = cls.newInstance();
				//设置方法参数
				method.setParam(mb.getParameters(), mb.getParam_type());
				//保存到methodBean中
				mb.setClsIns(method);
				//保存到Method Map中,方法名为key
				mmp.put(mb.getMethod(), mb);	
			}catch(ClassNotFoundException e ){
				v.outputI(0, 3,mb.getMethod());
				return null;
			} catch (MethodException e) {
				v.outputI(0, 4,mb.getMethod());
				return null;
			}
		}
		return mmp;
	}
	
	private static void close() throws Exception
	{
		m.close();
		v.close();
	}
	
	private static boolean isTask(String[] args) throws Exception{
		if(args.length<1 || m.getTask(args[0]) == null)
		{
			return false;
		}else{
			return true;
		}
	}
}
