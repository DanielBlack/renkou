package prclqz.core;

import java.util.ArrayList;

/**
 * 将字符串映射成Double数字的类
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class StringList
{
	private ArrayList<String> list;
	
	
	public StringList ( )
	{
		super ( );
		list = new ArrayList < String > ( );
		// Occupy the #0 element!
		list.add( "EMPTY in StringList!!" );
	}

	public double add(String value){
		list.add ( value );
		return list.size ( )-1;
	}
	
	public String get(int id){
		return list.get ( id );
	}

	/**
	 * @param args
	 */
	public static void main ( String [ ] args )
	{
		StringList l = new StringList ( );
		double k1 = l.add ( "ddd" );
		double k2 = l.add ( "fff" );
		System.out.println(""+k1+" "+l.get ( (int)k1 ));
		System.out.println(""+k2+" "+l.get ( (int)k2 ));
	}

	public String get ( double double1 )
	{
		return this.get( (int)double1 );
	}

}
