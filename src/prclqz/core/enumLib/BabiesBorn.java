package prclqz.core.enumLib;

/**
 * 分婚配模式生育孩次数-表字段映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum BabiesBorn {
/**
 * 城镇(乡村)农业(非农)
 * CNy, CFn, XNy, XFn
 * C(X)Ny(Fn)
 * 双独B1 DDB1 ,双独B2 DDB2  
 * 单独B1 D_B1 ,单独B2 D_B2 
 * 双非B1 NNB1 ,双非B2 NNB2
 * 
 */
//	CNyDDB1,	CNyDDB2,	CNyD_B1,	CNyD_B2,	CNyNNB1,	CNyNNB2,
//	CFnDDB1,	CFnDDB2,	CFnD_B1,	CFnD_B2,	CFnNNB1,	CFnNNB2,
//	XNyDDB1,	XNyDDB2,	XNyD_B1,	XNyD_B2,	XNyNNB1,	XNyNNB2,
//	XFnDDB1, 	XFnDDB2,	XFnD_B1,	XFnD_B2,	XFnNNB1,	XFnNNB2,
/**
 * --------------------------------
 * Add on 2012-03-29
 * 
  `总HJB` ,  `城HJB` ,  `乡HJB` ,  `总HJB1` ,
  `城HJB1` ,  `乡HJB1` ,  `总FNB1` ,  `总NYB1` ,
  `总HJB2` ,  `城HJB2` ,  `乡HJB2` ,  `总FNB2` ,
  `总NYB2` ,  `总HJ双独B` ,  `城HJ双独B` ,  `乡HJ双独B` ,
  `总FN双独B` ,  `城FN双独B` ,  `乡FN双独B` ,  `总NY双独B` ,
  `城NY双独B` ,  `乡NY双独B` ,  `总HJ双独B1` ,  `城HJ双独B1` ,
  `乡HJ双独B1` ,  `总FN双独B1` ,  `城FN双独B1` ,  `乡FN双独B1` ,
  `总NY双独B1` ,  `城NY双独B1` ,  `乡NY双独B1` ,  `总HJ双独B2` ,
  `城HJ双独B2` ,  `乡HJ双独B2` ,  `总FN双独B2` ,  `城FN双独B2` ,
  `乡FN双独B2` ,  `总NY双独B2` ,  `城NY双独B2` ,  `乡NY双独B2` ,
  `总HJ单独B` ,  `城HJ单独B` ,  `乡HJ单独B` ,  `总FN单独B` ,
  `城FN单独B` ,  `乡FN单独B` ,  `总NY单独B` ,  `城NY单独B` ,
  `乡NY单独B` ,  `总HJ单独B1` ,  `城HJ单独B1` ,  `乡HJ单独B1` ,
  `总FN单独B1` ,  `城FN单独B1` ,  `乡FN单独B1` ,  `总NY单独B1` ,
  `城NY单独B1` ,  `乡NY单独B1` ,  `总HJ单独B2` ,  `城HJ单独B2` ,
  `乡HJ单独B2` ,  `总FN单独B2` ,  `城FN单独B2` ,  `乡FN单独B2` ,
  `总NY单独B2` ,  `城NY单独B2` ,  `乡NY单独B2` ,  `总HJ双非B` ,
  `城HJ双非B` ,  `乡HJ双非B` ,  `总FN双非B` ,  `城FN双非B` ,
  `乡FN双非B` ,  `总NY双非B` ,  `城NY双非B` ,  `乡NY双非B` ,
  `总HJ双非B1` ,  `城HJ双非B1` ,  `乡HJ双非B1` ,  `总FN双非B1` ,
  `城FN双非B1` ,  `乡FN双非B1` ,  `总NY双非B1` ,  `城NY双非B1` ,
  `乡NY双非B1` ,  `总HJ双非B2` ,  `城HJ双非B2` ,  `乡HJ双非B2` ,
  `总FN双非B2` ,  `城FN双非B2` ,  `乡FN双非B2` ,  `总NY双非B2` ,
  `城NY双非B2` ,  `乡NY双非B2` ,
 * 
 */
	ZHjB,CHjB,XHjB,ZHjB1,
	CHjB1,XHjB1,ZFnB1,ZNyB1,
	ZHjB2,CHjB2,XHjB2,ZFnB2,
	ZNyB2,ZHjDDB,CHjDDB,XHjDDB,
	ZFnDDB,CFnDDB,XFnDDB,ZNyDDB,
	CNyDDB,XNyDDB,ZHjDDB1,CHjDDB1,
	XHjDDB1,ZFnDDB1,CFnDDB1,XFnDDB1,
	ZNyDDB1,CNyDDB1,XNyDDB1,ZHjDDB2,
	CHjDDB2,XHjDDB2,ZFnDDB2,CFnDDB2,
	XFnDDB2,ZNyDDB2,CNyDDB2,XNyDDB2,
	ZHjD_B,CHjD_B,XHjD_B,ZFnD_B,
	CFnD_B,XFnD_B,ZNyD_B,CNyD_B,
	XNyD_B,ZHjD_B1,CHjD_B1,XHjD_B1,
	ZFnD_B1,CFnD_B1,XFnD_B1,ZNyD_B1,
	CNyD_B1,XNyD_B1,ZHjD_B2,CHjD_B2,
	XHjD_B2,ZFnD_B2,CFnD_B2,XFnD_B2,
	ZNyD_B2,CNyD_B2,XNyD_B2,ZHjNNB,
	CHjNNB,XHjNNB,ZFnNNB,CFnNNB,
	XFnNNB,ZNyNNB,CNyNNB,XNyNNB,
	ZHjNNB1,CHjNNB1,XHjNNB1,ZFnNNB1,
	CFnNNB1,XFnNNB1,ZNyNNB1,CNyNNB1,
	XNyNNB1,ZHjNNB2,CHjNNB2,XHjNNB2,
	ZFnNNB2,CFnNNB2,XFnNNB2,ZNyNNB2,
	CNyNNB2,XNyNNB2,

;
	public static String getChinese ( BabiesBorn bb ) throws Exception
	{
		switch ( bb )
		{
		case ZHjB :
			return "总HJB";
		case CHjB :
			return "城HJB";
		case XHjB :
			return "乡HJB";
		case ZHjB1 :
			return "总HJB1";
		case CHjB1 :
			return "城HJB1";
		case XHjB1 :
			return "乡HJB1";
		case ZFnB1 :
			return "总FNB1";
		case ZNyB1 :
			return "总NYB1";
		case ZHjB2 :
			return "总HJB2";
		case CHjB2 :
			return "城HJB2";
		case XHjB2 :
			return "乡HJB2";
		case ZFnB2 :
			return "总FNB2";
		case ZNyB2 :
			return "总NYB2";
		case ZHjDDB :
			return "总HJ双独B";
		case CHjDDB :
			return "城HJ双独B";
		case XHjDDB :
			return "乡HJ双独B";
		case ZFnDDB :
			return "总FN双独B";
		case CFnDDB :
			return "城FN双独B";
		case XFnDDB :
			return "乡FN双独B";
		case ZNyDDB :
			return "总NY双独B";
		case CNyDDB :
			return "城NY双独B";
		case XNyDDB :
			return "乡NY双独B";
		case ZHjDDB1 :
			return "总HJ双独B1";
		case CHjDDB1 :
			return "城HJ双独B1";
		case XHjDDB1 :
			return "乡HJ双独B1";
		case ZFnDDB1 :
			return "总FN双独B1";
		case CFnDDB1 :
			return "城FN双独B1";
		case XFnDDB1 :
			return "乡FN双独B1";
		case ZNyDDB1 :
			return "总NY双独B1";
		case CNyDDB1 :
			return "城NY双独B1";
		case XNyDDB1 :
			return "乡NY双独B1";
		case ZHjDDB2 :
			return "总HJ双独B2";
		case CHjDDB2 :
			return "城HJ双独B2";
		case XHjDDB2 :
			return "乡HJ双独B2";
		case ZFnDDB2 :
			return "总FN双独B2";
		case CFnDDB2 :
			return "城FN双独B2";
		case XFnDDB2 :
			return "乡FN双独B2";
		case ZNyDDB2 :
			return "总NY双独B2";
		case CNyDDB2 :
			return "城NY双独B2";
		case XNyDDB2 :
			return "乡NY双独B2";
		case ZHjD_B :
			return "总HJ单独B";
		case CHjD_B :
			return "城HJ单独B";
		case XHjD_B :
			return "乡HJ单独B";
		case ZFnD_B :
			return "总FN单独B";
		case CFnD_B :
			return "城FN单独B";
		case XFnD_B :
			return "乡FN单独B";
		case ZNyD_B :
			return "总NY单独B";
		case CNyD_B :
			return "城NY单独B";
		case XNyD_B :
			return "乡NY单独B";
		case ZHjD_B1 :
			return "总HJ单独B1";
		case CHjD_B1 :
			return "城HJ单独B1";
		case XHjD_B1 :
			return "乡HJ单独B1";
		case ZFnD_B1 :
			return "总FN单独B1";
		case CFnD_B1 :
			return "城FN单独B1";
		case XFnD_B1 :
			return "乡FN单独B1";
		case ZNyD_B1 :
			return "总NY单独B1";
		case CNyD_B1 :
			return "城NY单独B1";
		case XNyD_B1 :
			return "乡NY单独B1";
		case ZHjD_B2 :
			return "总HJ单独B2";
		case CHjD_B2 :
			return "城HJ单独B2";
		case XHjD_B2 :
			return "乡HJ单独B2";
		case ZFnD_B2 :
			return "总FN单独B2";
		case CFnD_B2 :
			return "城FN单独B2";
		case XFnD_B2 :
			return "乡FN单独B2";
		case ZNyD_B2 :
			return "总NY单独B2";
		case CNyD_B2 :
			return "城NY单独B2";
		case XNyD_B2 :
			return "乡NY单独B2";
		case ZHjNNB :
			return "总HJ双非B";
		case CHjNNB :
			return "城HJ双非B";
		case XHjNNB :
			return "乡HJ双非B";
		case ZFnNNB :
			return "总FN双非B";
		case CFnNNB :
			return "城FN双非B";
		case XFnNNB :
			return "乡FN双非B";
		case ZNyNNB :
			return "总NY双非B";
		case CNyNNB :
			return "城NY双非B";
		case XNyNNB :
			return "乡NY双非B";
		case ZHjNNB1 :
			return "总HJ双非B1";
		case CHjNNB1 :
			return "城HJ双非B1";
		case XHjNNB1 :
			return "乡HJ双非B1";
		case ZFnNNB1 :
			return "总FN双非B1";
		case CFnNNB1 :
			return "城FN双非B1";
		case XFnNNB1 :
			return "乡FN双非B1";
		case ZNyNNB1 :
			return "总NY双非B1";
		case CNyNNB1 :
			return "城NY双非B1";
		case XNyNNB1 :
			return "乡NY双非B1";
		case ZHjNNB2 :
			return "总HJ双非B2";
		case CHjNNB2 :
			return "城HJ双非B2";
		case XHjNNB2 :
			return "乡HJ双非B2";
		case ZFnNNB2 :
			return "总FN双非B2";
		case CFnNNB2 :
			return "城FN双非B2";
		case XFnNNB2 :
			return "乡FN双非B2";
		case ZNyNNB2 :
			return "总NY双非B2";
		case CNyNNB2 :
			return "城NY双非B2";
		case XNyNNB2 :
			return "乡NY双非B2";
		default :
			throw new Exception ( "error param for getChinese in Babaies Born" );
		}
	}
	
/**
 * 根据 CX 和 NY的情况来选择 DDB1
 * @param cx
 * @param ny
 * @return
 */
 public static BabiesBorn getDDB1(CX cx,NY ny){
	 if(cx == CX.Chengshi){
		 if(ny == NY.Nongye)
			 return CNyDDB1;
		 else 
			 return CFnDDB1;
	 }else{
		 if(ny == NY.Nongye)
			 return XNyDDB1;
		 else 
			 return XFnDDB1;
	 }
 }
 
 /**
  * 根据 CX 和 NY的情况来选择 DDB2
  * @param cx
  * @param ny
  * @return
  */
  public static BabiesBorn getDDB2(CX cx,NY ny){
 	 if(cx == CX.Chengshi){
 		 if(ny == NY.Nongye)
 			 return CNyDDB2;
 		 else 
 			 return CFnDDB2;
 	 }else{
 		 if(ny == NY.Nongye)
 			 return XNyDDB2;
 		 else 
 			 return XFnDDB2;
 	 }
  }
  
  /**
   * 根据 CX 和 NY的情况来选择 D_B1
   * @param cx
   * @param ny
   * @return
   */
   public static BabiesBorn getD_B1(CX cx,NY ny){
  	 if(cx == CX.Chengshi){
  		 if(ny == NY.Nongye)
  			 return CNyD_B1;
  		 else 
  			 return CFnD_B1;
  	 }else{
  		 if(ny == NY.Nongye)
  			 return XNyD_B1;
  		 else 
  			 return XFnD_B1;
  	 }
   }
   
   /**
    * 根据 CX 和 NY的情况来选择 D_B2
    * @param cx
    * @param ny
    * @return
    */
    public static BabiesBorn getD_B2(CX cx,NY ny){
   	 if(cx == CX.Chengshi){
   		 if(ny == NY.Nongye)
   			 return CNyD_B2;
   		 else 
   			 return CFnD_B2;
   	 }else{
   		 if(ny == NY.Nongye)
   			 return XNyD_B2;
   		 else 
   			 return XFnD_B2;
   	 }
    }
    
    /**
     * 根据 CX 和 NY的情况来选择 NNB1
     * @param cx
     * @param ny
     * @return
     */
     public static BabiesBorn getNNB1(CX cx,NY ny){
    	 if(cx == CX.Chengshi){
    		 if(ny == NY.Nongye)
    			 return CNyNNB1;
    		 else 
    			 return CFnNNB1;
    	 }else{
    		 if(ny == NY.Nongye)
    			 return XNyNNB1;
    		 else 
    			 return XFnNNB1;
    	 }
     }
     
     /**
      * 根据 CX 和 NY的情况来选择 NNB2
      * @param cx
      * @param ny
      * @return
      */
      public static BabiesBorn getNNB2(CX cx,NY ny){
     	 if(cx == CX.Chengshi){
     		 if(ny == NY.Nongye)
     			 return CNyNNB2;
     		 else 
     			 return CFnNNB2;
     	 }else{
     		 if(ny == NY.Nongye)
     			 return XNyNNB2;
     		 else 
     			 return XFnNNB2;
     	 }
      }
      
      /**
       *  2~91
       */
      private static BabiesBorn[] bbs = {ZHjB,CHjB,XHjB,ZHjB1,
    		CHjB1,XHjB1,ZFnB1,ZNyB1,
    		ZHjB2,CHjB2,XHjB2,ZFnB2,
    		ZNyB2,ZHjDDB,CHjDDB,XHjDDB,
    		ZFnDDB,CFnDDB,XFnDDB,ZNyDDB,
    		CNyDDB,XNyDDB,ZHjDDB1,CHjDDB1,
    		XHjDDB1,ZFnDDB1,CFnDDB1,XFnDDB1,
    		ZNyDDB1,CNyDDB1,XNyDDB1,ZHjDDB2,
    		CHjDDB2,XHjDDB2,ZFnDDB2,CFnDDB2,
    		XFnDDB2,ZNyDDB2,CNyDDB2,XNyDDB2,
    		ZHjD_B,CHjD_B,XHjD_B,ZFnD_B,
    		CFnD_B,XFnD_B,ZNyD_B,CNyD_B,
    		XNyD_B,ZHjD_B1,CHjD_B1,XHjD_B1,
    		ZFnD_B1,CFnD_B1,XFnD_B1,ZNyD_B1,
    		CNyD_B1,XNyD_B1,ZHjD_B2,CHjD_B2,
    		XHjD_B2,ZFnD_B2,CFnD_B2,XFnD_B2,
    		ZNyD_B2,CNyD_B2,XNyD_B2,ZHjNNB,
    		CHjNNB,XHjNNB,ZFnNNB,CFnNNB,
    		XFnNNB,ZNyNNB,CNyNNB,XNyNNB,
    		ZHjNNB1,CHjNNB1,XHjNNB1,ZFnNNB1,
    		CFnNNB1,XFnNNB1,ZNyNNB1,CNyNNB1,
    		XNyNNB1,ZHjNNB2,CHjNNB2,XHjNNB2,
    		ZFnNNB2,CFnNNB2,XFnNNB2};
      public static BabiesBorn[] getStatisticsFields()	{
    	  return bbs;
      }
}
