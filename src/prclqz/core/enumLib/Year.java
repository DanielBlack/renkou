package prclqz.core.enumLib;
/***
 * 需要预测的年份——也可作为 	分年龄人口预测	和 	分年龄丧子人口预测	的表映射
 * @author prclqz@zju.edu.cn
 */
public enum Year {
Y2000,
Y2001,
Y2002,
Y2003,
Y2004,
Y2005,
Y2006,
Y2007,
Y2008,
Y2009,
Y2010,
Y2011,
Y2012,
Y2013,
Y2014,
Y2015,
Y2016,
Y2017,
Y2018,
Y2019,
Y2020,
Y2021,
Y2022,
Y2023,
Y2024,
Y2025,
Y2026,
Y2027,
Y2028,
Y2029,
Y2030,
Y2031,
Y2032,
Y2033,
Y2034,
Y2035,
Y2036,
Y2037,
Y2038,
Y2039,
Y2040,
Y2041,
Y2042,
Y2043,
Y2044,
Y2045,
Y2046,
Y2047,
Y2048,
Y2049,
Y2050,
Y2051,
Y2052,
Y2053,
Y2054,
Y2055,
Y2056,
Y2057,
Y2058,
Y2059,
Y2060,
Y2061,
Y2062,
Y2063,
Y2064,
Y2065,
Y2066,
Y2067,
Y2068,
Y2069,
Y2070,
Y2071,
Y2072,
Y2073,
Y2074,
Y2075,
Y2076,
Y2077,
Y2078,
Y2079,
Y2080,
Y2081,
Y2082,
Y2083,
Y2084,
Y2085,
Y2086,
Y2087,
Y2088,
Y2089,
Y2090,
Y2091,
Y2092,
Y2093,
Y2094,
Y2095,
Y2096,
Y2097,
Y2098,
Y2099,
Y2100;
	public static Year getYear(int year) {
		switch (year) {
		case 2000:
			return Y2000;
		case 2001:
			return Y2001;
		case 2002:
			return Y2002;
		case 2003:
			return Y2003;
		case 2004:
			return Y2004;
		case 2005:
			return Y2005;
		case 2006:
			return Y2006;
		case 2007:
			return Y2007;
		case 2008:
			return Y2008;
		case 2009:
			return Y2009;
		case 2010:
			return Y2010;
		case 2011:
			return Y2011;
		case 2012:
			return Y2012;
		case 2013:
			return Y2013;
		case 2014:
			return Y2014;
		case 2015:
			return Y2015;
		case 2016:
			return Y2016;
		case 2017:
			return Y2017;
		case 2018:
			return Y2018;
		case 2019:
			return Y2019;
		case 2020:
			return Y2020;
		case 2021:
			return Y2021;
		case 2022:
			return Y2022;
		case 2023:
			return Y2023;
		case 2024:
			return Y2024;
		case 2025:
			return Y2025;
		case 2026:
			return Y2026;
		case 2027:
			return Y2027;
		case 2028:
			return Y2028;
		case 2029:
			return Y2029;
		case 2030:
			return Y2030;
		case 2031:
			return Y2031;
		case 2032:
			return Y2032;
		case 2033:
			return Y2033;
		case 2034:
			return Y2034;
		case 2035:
			return Y2035;
		case 2036:
			return Y2036;
		case 2037:
			return Y2037;
		case 2038:
			return Y2038;
		case 2039:
			return Y2039;
		case 2040:
			return Y2040;
		case 2041:
			return Y2041;
		case 2042:
			return Y2042;
		case 2043:
			return Y2043;
		case 2044:
			return Y2044;
		case 2045:
			return Y2045;
		case 2046:
			return Y2046;
		case 2047:
			return Y2047;
		case 2048:
			return Y2048;
		case 2049:
			return Y2049;
		case 2050:
			return Y2050;
		case 2051:
			return Y2051;
		case 2052:
			return Y2052;
		case 2053:
			return Y2053;
		case 2054:
			return Y2054;
		case 2055:
			return Y2055;
		case 2056:
			return Y2056;
		case 2057:
			return Y2057;
		case 2058:
			return Y2058;
		case 2059:
			return Y2059;
		case 2060:
			return Y2060;
		case 2061:
			return Y2061;
		case 2062:
			return Y2062;
		case 2063:
			return Y2063;
		case 2064:
			return Y2064;
		case 2065:
			return Y2065;
		case 2066:
			return Y2066;
		case 2067:
			return Y2067;
		case 2068:
			return Y2068;
		case 2069:
			return Y2069;
		case 2070:
			return Y2070;
		case 2071:
			return Y2071;
		case 2072:
			return Y2072;
		case 2073:
			return Y2073;
		case 2074:
			return Y2074;
		case 2075:
			return Y2075;
		case 2076:
			return Y2076;
		case 2077:
			return Y2077;
		case 2078:
			return Y2078;
		case 2079:
			return Y2079;
		case 2080:
			return Y2080;
		case 2081:
			return Y2081;
		case 2082:
			return Y2082;
		case 2083:
			return Y2083;
		case 2084:
			return Y2084;
		case 2085:
			return Y2085;
		case 2086:
			return Y2086;
		case 2087:
			return Y2087;
		case 2088:
			return Y2088;
		case 2089:
			return Y2089;
		case 2090:
			return Y2090;
		case 2091:
			return Y2091;
		case 2092:
			return Y2092;
		case 2093:
			return Y2093;
		case 2094:
			return Y2094;
		case 2095:
			return Y2095;
		case 2096:
			return Y2096;
		case 2097:
			return Y2097;
		case 2098:
			return Y2098;
		case 2099:
			return Y2099;
		case 2100:
			return Y2100;
		default:
			return null;
		}
	}

	public static Year[] getAllYears() {
		Year[] years = { Y2000, Y2001, Y2002, Y2003, Y2004, Y2005, Y2006,
				Y2007, Y2008, Y2009, Y2010, Y2011, Y2012, Y2013, Y2014, Y2015,
				Y2016, Y2017, Y2018, Y2019, Y2020, Y2021, Y2022, Y2023, Y2024,
				Y2025, Y2026, Y2027, Y2028, Y2029, Y2030, Y2031, Y2032, Y2033,
				Y2034, Y2035, Y2036, Y2037, Y2038, Y2039, Y2040, Y2041, Y2042,
				Y2043, Y2044, Y2045, Y2046, Y2047, Y2048, Y2049, Y2050, Y2051,
				Y2052, Y2053, Y2054, Y2055, Y2056, Y2057, Y2058, Y2059, Y2060,
				Y2061, Y2062, Y2063, Y2064, Y2065, Y2066, Y2067, Y2068, Y2069,
				Y2070, Y2071, Y2072, Y2073, Y2074, Y2075, Y2076, Y2077, Y2078,
				Y2079, Y2080, Y2081, Y2082, Y2083, Y2084, Y2085, Y2086, Y2087,
				Y2088, Y2089, Y2090, Y2091, Y2092, Y2093, Y2094, Y2095, Y2096,
				Y2097, Y2098, Y2099, Y2100 };
		return years;
	}
}
