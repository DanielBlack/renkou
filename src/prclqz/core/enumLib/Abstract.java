package prclqz.core.enumLib;

/**
 * 生育政策模拟摘要表映射
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public enum Abstract
{
	
/**
 * enum类型，getChinese根据Abstract变量返回对应的字符串
  `堆释模式`,  `概率估算法`,  `政策1`,  `时机1` ,
  `政策2`,  `时机2` ,  `政策3`,  `时机3` ,
  `迁移方案`,  `生育方案` ,  `地区别`,  `城乡别`,
  `最大规模ND` ,  `最大规模` ,  `最小规模ND` ,  `最小规模` ,
  `最大迁移ND` ,  `最大迁移` ,  `最小迁移ND` ,  `最小迁移` ,
  `最高TFRND` ,  `最高TFR` ,  `总人口20` ,  `出生20` ,
  `出生率20` ,  `生育率20` ,  `净迁入率20` ,  `总增长率20` ,
  `城市化20` ,
 */
	DJMoShi,GLGuSuanFa,Policy1,Time1,
	Policy2,Time2,Policy3,Time3,
	MigratePlan,BornPlan,DiQuB,CXB,
	MaxGuimoND,MaxGuimo,MinGuimoND,MinGuimo,
	MaxMigrateND,MaxMigrate,MinMigrateND,MinMigrate,
	MaxTFRND,MaxTFR,T_PPL20,Born20,
	BornRate20,BirthRate20,IngrateRate20,T_incr_Rate20,
	CSH20;
	
	public static String getChinese(Abstract ab) throws Exception{
		switch(ab){
		case DJMoShi :
			return "堆释模式";
			case GLGuSuanFa :
			return "概率估算法";
			case Policy1 :
			return "政策1";
			case Time1 :
			return "时机1";
			case Policy2 :
			return "政策2";
			case Time2 :
			return "时机2";
			case Policy3 :
			return "政策3";
			case Time3 :
			return "时机3";
			case MigratePlan :
			return "迁移方案";
			case BornPlan :
			return "生育方案";
			case DiQuB :
			return "地区别";
			case CXB :
			return "城乡别";
			case MaxGuimoND :
			return "最大规模ND";
			case MaxGuimo :
			return "最大规模";
			case MinGuimoND :
			return "最小规模ND";
			case MinGuimo :
			return "最小规模";
			case MaxMigrateND :
			return "最大迁移ND";
			case MaxMigrate :
			return "最大迁移";
			case MinMigrateND :
			return "最小迁移ND";
			case MinMigrate :
			return "最小迁移";
			case MaxTFRND :
			return "最高TFRND";
			case MaxTFR :
			return "最高TFR";
			case T_PPL20 :
			return "总人口20";
			case Born20 :
			return "出生20";
			case BornRate20 :
			return "出生率20";
			case BirthRate20 :
			return "生育率20";
			case IngrateRate20 :
			return "净迁入率20";
			case T_incr_Rate20 :
			return "总增长率20";
			case CSH20 :
			return "城市化20";
			default:
				throw new Exception( "error param for getChinese in Abstract--"+ab );
		}
	}
}
