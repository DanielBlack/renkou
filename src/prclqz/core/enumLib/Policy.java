package prclqz.core.enumLib;

/**
 * 生育政策
 * @author prclqz@zju.edu.cn
 *
 */
public enum Policy {
//现行,单独,双独后,单独后,普二  --还是用拼音
	xianXing {
		@Override
		public int getId() {
			return 1;
		}
	},
	danDu {
		@Override
		public int getId() {
			return 2;
		}
	},
	shuangDuHou {
		@Override
		public int getId() {
			return 3;
		}
	},
	danDuHou {
		@Override
		public int getId() {
			return 4;
		}
	},
	puEr {
		@Override
		public int getId() {
			return 5;
		}
	};
	
	public static Policy getPolicyById(int id){
		switch(id){
		case 1:
		case 171:
			return xianXing;
		case 2:
		case 172:
			return danDu;
		case 3:
		case 173:
			return shuangDuHou;
		case 4:
		case 174:
			return danDuHou;
		case 5:
		case 175:
			return puEr;
		default:
			return null;
		}
	}
	
	public static String getChinese(Policy py)throws Exception{
		switch(py){
		case danDu:
			return "单独";
		case danDuHou:
			return "单独后";
		case puEr:
			return "普二";
		case shuangDuHou:
			return "双独后";
		case xianXing:
			return "现行";
			default:
				throw new Exception( "error param for getChinese in Policy"+py );
		}
	}
	
	public abstract int getId();
}
