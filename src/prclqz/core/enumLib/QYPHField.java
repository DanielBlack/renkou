package prclqz.core.enumLib;

/**
 * 表类型映射，慎重使用values()因为字段会有变化——可自创建类似values()操作
 * @author prclqz@zju.edu.cn
 *
 */

public enum QYPHField {
//[0:M  1:F][0:迁入  1:迁出]
// 迁移平衡[0:M  1:F] 
// [0:M  1:F][0:平衡迁入  1:平衡迁出]
	MImmigrate,MEmigrate,
	FImmigrate,FEmigrate,
	MBalance,FBalance,
	MBalanceIm,
	MBalanceEm,
	FBalanceIm,
	FBalanceEm;
	
	
	public static QYPHField getImFromHp(HunpeiField hp){
		switch(hp){
		case DQYM:
		case DDBQYM:
		case NQYM:
			return MImmigrate;
		case DQYF:
		case DDBQYF:
		case NQYF:
			return FImmigrate;
		default:
			return null;
		}
	}
	
	public static QYPHField getEmFromHp(HunpeiField hp){
		switch(hp){
		case DQYM:
		case DDBQYM:
		case NQYM:
			return MEmigrate;
		case DQYF:
		case DDBQYF:
		case NQYF:
			return FEmigrate;
		default:
			return null;
		}
	}
	public static QYPHField getBalanceFromHp(HunpeiField hp){
		switch(hp){
		case DQYM:
		case DDBQYM:
		case NQYM:
			return MBalance;
		
		case DQYF:
		case DDBQYF:
		case NQYF:
			return FBalance;
		default:
			return null;
		}
	}
	public static QYPHField getBalanceImFromHp(HunpeiField hp){
		switch(hp){
		case DQYM:
		case DDBQYM:
		case NQYM:
			return MBalanceIm;
		case DQYF:
		case DDBQYF:
		case NQYF:
			return FBalanceIm;
		default:
			return null;
		}
	}
	public static QYPHField getBalanceEmFromHp(HunpeiField hp){
		switch(hp){
		case DQYM:
		case DDBQYM:
		case NQYM:
			return MBalanceEm;
		case DQYF:
		case DDBQYF:
		case NQYF:
			return FBalanceEm;
		default:
			return null;
		}
	}
}
