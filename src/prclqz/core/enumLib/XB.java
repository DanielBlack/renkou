package prclqz.core.enumLib;

/**
 * 基本循环类型，慎重添加或删除元素，因为会影响到values()操作
 * @author prclqz@zju.edu.cn
 *
 */
public enum XB{ 
	Male {
		@Override
		public int getEditableNum() {
			return 0;
		}
	}, 
	Female {
		@Override
		public int getEditableNum() {
			return 1;
		}
	};
	abstract public int getEditableNum();
	public static XB getXBFromId(int id){
		switch(id){
		case 71:
			return Male;
		case 70:
			return Female;
		default:
			return null;
		}
	}
}