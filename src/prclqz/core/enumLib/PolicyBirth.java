package prclqz.core.enumLib;
/**
 * 政策及政策生育率-表映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum PolicyBirth
{
/*
 *    `非农政策1` ,  `农业政策1` ,  `非农政策2` ,
  `农业政策2` ,  `非农政策3` ,  `农业政策3` ,  `非农双独` ,
  `农业双独` ,  `非农单独` ,  `农业单独` ,  `非农女单` ,
  `农业女单` ,  `非农男单` ,  `农业男单` ,  `非农双非` ,
  `农业双非` ,  `农村NYTFR` ,  `农村FNTFR` ,  `农村TFR` ,
  `城镇NYTFR` ,  `城镇FNTFR` ,  `城镇TFR` ,  `地区NYTFR` ,
  `地区FNTFR` ,  `地区TFR` ,  `NCNY婚TFR` ,  `NCFN婚TFR` ,
  `NC婚TFR` ,  `CZNY婚TFR` ,  `CZFN婚TFR` ,  `CZ婚TFR` ,
  `QSNY婚TFR` ,  `QSFN婚TFR` ,  `QS婚TFR` ,
 */
//	Year,
	FnPolicy1,NyPolicy1,FnPolicy2,
	NyPolicy2,FnPolicy3,NyPolicy3,FnSD,
	NySD,FnDD,NyDD,FnFSingle,
	NyFSingle,FnMSingle,NyMSingle,FnSF,
	NySF,NcNYTFR,NcFNTFR,NcTFR,
	CsNYTFR,CsFNTFR,CsTFR,DqNYTFR,
	DqFNTFR,DqTFR,NCNYMarryTFR,NCFNMarryTFR,
	NCMarryTFR,CZNYMarryTFR,CZFNMarryTFR,CZMarryTFR,
	QSNYMarryTFR,QSFNMarryTFR,QSMarryTFR,;
	
	public static String getChinese ( PolicyBirth pb ) throws Exception
	{
		switch ( pb )
		{
		case FnPolicy1 :
			return "非农政策1";
		case NyPolicy1 :
			return "农业政策1";
		case FnPolicy2 :
			return "非农政策2";
		case NyPolicy2 :
			return "农业政策2";
		case FnPolicy3 :
			return "非农政策3";
		case NyPolicy3 :
			return "农业政策3";
		case FnSD :
			return "非农双独";
		case NySD :
			return "农业双独";
		case FnDD :
			return "非农单独";
		case NyDD :
			return "农业单独";
		case FnFSingle :
			return "非农女单";
		case NyFSingle :
			return "农业女单";
		case FnMSingle :
			return "非农男单";
		case NyMSingle :
			return "农业男单";
		case FnSF :
			return "非农双非";
		case NySF :
			return "农业双非";
		case NcNYTFR :
			return "农村NYTFR";
		case NcFNTFR :
			return "农村FNTFR";
		case NcTFR :
			return "农村TFR";
		case CsNYTFR :
			return "城镇NYTFR";
		case CsFNTFR :
			return "城镇FNTFR";
		case CsTFR :
			return "城镇TFR";
		case DqNYTFR :
			return "地区NYTFR";
		case DqFNTFR :
			return "地区FNTFR";
		case DqTFR :
			return "地区TFR";
		case NCNYMarryTFR :
			return "NCNY婚TFR";
		case NCFNMarryTFR :
			return "NCFN婚TFR";
		case NCMarryTFR :
			return "NC婚TFR";
		case CZNYMarryTFR :
			return "CZNY婚TFR";
		case CZFNMarryTFR :
			return "CZFN婚TFR";
		case CZMarryTFR :
			return "CZ婚TFR";
		case QSNYMarryTFR :
			return "QSNY婚TFR";
		case QSFNMarryTFR :
			return "QSFN婚TFR";
		case QSMarryTFR :
			return "QS婚TFR";
		default :
			throw new Exception ( "error param for getChinese in PolicyBirth-"
					+ pb );
		}
	}
}
