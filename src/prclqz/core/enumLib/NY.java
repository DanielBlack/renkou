package prclqz.core.enumLib;

/**
 * 基本循环类型，慎重添加或删除元素，因为会影响到values()操作
 * @author prclqz@zju.edu.cn
 *
 */
public enum NY{ 
	Nongye {
		@Override
		public String getChinese() {
			return "农业";
		}
	}, 
	Feinong {
		@Override
		public String getChinese() {
			return "非农";
		}
	};
	public static NY getNYFromId(int id){
		switch(id){
		case 132:
		case 163:
		case 165:
			return Nongye;
		case 130:
		case 164:
		case 166:
			return Feinong;
		default:
			return null;
		}
	}
	public abstract String getChinese();
	public static NY[] values2() {
		NY[] arr = {Feinong,Nongye};
		return arr;
	}
}