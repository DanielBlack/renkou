package prclqz.core.enumLib;

/**
 * 概要-表映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum Summary
{
/**
 *   `地区` ,  `总人口` ,  `男` ,  `女` ,  `年均人口` ,  `出生` ,
  `出生男` ,  `出生女` ,  `出生XBB` ,  `出生一孩` ,  `出生二孩` ,  `政策二孩` ,
  `政策生育数` ,  `超生数` ,  `堆积夫妇数` ,  `堆积生育` ,  `非堆生育` ,
  `出生率` ,  `死亡率` , `自增率` ,  `净迁入率` ,  `总增长率` ,
  `可实现TFR` ,  `政策生育率` ,  `实婚生育率` ,  `婚N政策TFR` ,
  `死亡人数` ,  `死亡男` ,  `死亡女` ,  `自增人口` ,
  `预期寿命M` ,  `预期寿命F` ,  `净迁入数` ,  `净迁男` ,
  `净迁女` ,  `新增人数` ,  `非农人口` ,  `农业人口` ,
  `非农人口B` ,  `城镇人口` ,  `农村人口` ,  `城镇人口比` ,
  `婴幼儿` ,  `学前` ,  `小学` ,  `初中` ,
  `高中` ,  `大学` ,  `国标劳前` ,  `国标劳龄` ,
  `国标劳龄M` ,  `国标劳龄F` ,  `国标青劳d` ,  `国标中劳` ,
  `国标中劳M` ,  `国标中劳F` ,  `国标劳后` ,  `国标劳后M` ,
  `国标劳后F` ,  `公安劳龄` ,  `公安劳龄M` ,  `公安劳龄F` ,
  `国标少年` ,  `国标劳年` ,  `国标劳年M` ,  `国标劳年F` ,
  `国标老年` ,  `国标老年M` ,  `国标老年F` ,  `高龄` ,
  `高龄M` ,  `高龄F` ,  `长寿` ,  `长寿M` ,  `长寿F` ,
  `中位` ,  `年龄差异度` ,  `国标少年比` ,  `国标老年比` ,
  `国标老少比` ,  `国标总负` ,  `国标负老` ,  `国标负少` ,
  `国标劳年B` ,  `国标青劳B` ,  `国标高龄B` ,  `国标长寿B` ,
  `国际少年` ,  `国际劳年` ,  `国际青劳` ,  `国际老年` ,
  `国际老年M` ,  `国际老年F` ,  `国际少年比` ,  `国际老年比` ,
  `国际老少比` ,  `国际总负` ,  `国际负老` ,  `国际负少` ,
  `国际劳年B` ,  `国际青劳B` ,  `国际高龄B` ,  `国际长寿B` ,
  `育龄女15` ,  `婚育男15` ,  `育龄女20` ,  `婚育男20` ,
  `婚育XBB15` ,  `婚育XBB20` ,  `人口密度` ,  `人均水资源` ,
  `人均耕地` , 
  */
	//Year, =>索引
	DQ,Popl,Male,Female,PoplAvg,Birth,
	BirthM,BirthF,BirthXBB,BirthYihai,BirthErhai,PolicyErhai,
	PolicyBirth,OverBirth,DJCouple,DJBirth,NDJBirth,
	BirthRate,DeathRate,AutoIncRate,InMigRate,TotalIncRate,
	ImplTFR,PolicyBirthRate,MarryBirthRate,MarryNPolicyTFR,
	Death,DeathM,DeathF,AutoInc,
	PredictAgeM,PredictAgeF,InMig,InMigM,
	InMigF,IncNum,FN,NY,
	FNB,Chengshi,Nongchun,ChengshiB,
	Babies,PreSchool,PrimarySch,MiddleSch,
	HighSch,University,NationPre,NationMid,
	NationMidM,NationMidF,NationYoungL,NationMidL,
	NationMidLM,NationMidLF,NationAfter,NationAfterM,
	NationAfterF,GAAge,GAAgeM,GAAgeF,
	NationYoung,NationLabor,NationLaborM,NationLaborF,
	NationOld,NationOldM,NationOldF,HighAge,
	HighAgeM,HighAgeF,LongAge,LongAgeM,LongAgeF,
	Mid,AgeDif,NationYoungRate,NationOldRate,
	NationOldYoungRate,NationAllNeg,NationNegOld,NationNegYoung,
	NationLaborB,NationQingLaborB,NationHighAgeB,NationLongAgeB,
	IntnYoung,IntnLabor,IntnQingLabor,IntnOld,
	IntnOldM,IntnOldF,IntnYoungB,IntnOldB,
	IntnOldYoungB,IntnAllNeg,IntnNegOld,IntnNegYoung,
	IntnLaborB,IntnQingLaborB,IntnHighAgeB,IntnLongAgeB,
	BirthF15,BirthM15,BirthF20,BirthM20,
	MarryXbb15,MarryXbb20,PeopDensity,WaterAvg,
	LandAvg,;
	
	public static String getChinese ( Summary sm ) throws Exception
	{
		switch ( sm )
		{
		case DQ :
			return "地区";
		case Popl :
			return "总人口";
		case Male :
			return "男";
		case Female :
			return "女";
		case PoplAvg :
			return "年均人口";
		case Birth :
			return "出生";
		case BirthM :
			return "出生男";
		case BirthF :
			return "出生女";
		case BirthXBB :
			return "出生XBB";
		case BirthYihai :
			return "出生一孩";
		case BirthErhai :
			return "出生二孩";
		case PolicyErhai :
			return "政策二孩";
		case PolicyBirth :
			return "政策生育数";
		case OverBirth :
			return "超生数";
		case DJCouple :
			return "堆积夫妇数";
		case DJBirth :
			return "堆积生育";
		case NDJBirth :
			return "非堆生育";
		case BirthRate :
			return "出生率";
		case DeathRate :
			return "死亡率";
		case AutoIncRate :
			return "自增率";
		case InMigRate :
			return "净迁入率";
		case TotalIncRate :
			return "总增长率";
		case ImplTFR :
			return "可实现TFR";
		case PolicyBirthRate :
			return "政策生育率";
		case MarryBirthRate :
			return "实婚生育率";
		case MarryNPolicyTFR :
			return "婚N政策TFR";
		case Death :
			return "死亡人数";
		case DeathM :
			return "死亡男";
		case DeathF :
			return "死亡女";
		case AutoInc :
			return "自增人口";
		case PredictAgeM :
			return "预期寿命M";
		case PredictAgeF :
			return "预期寿命F";
		case InMig :
			return "净迁入数";
		case InMigM :
			return "净迁男";
		case InMigF :
			return "净迁女";
		case IncNum :
			return "新增人数";
		case FN :
			return "非农人口";
		case NY :
			return "农业人口";
		case FNB :
			return "非农人口B";
		case Chengshi :
			return "城镇人口";
		case Nongchun :
			return "农村人口";
		case ChengshiB :
			return "城镇人口比";
		case Babies :
			return "婴幼儿";
		case PreSchool :
			return "学前";
		case PrimarySch :
			return "小学";
		case MiddleSch :
			return "初中";
		case HighSch :
			return "高中";
		case University :
			return "大学";
		case NationPre :
			return "国标劳前";
		case NationMid :
			return "国标劳龄";
		case NationMidM :
			return "国标劳龄M";
		case NationMidF :
			return "国标劳龄F";
		case NationYoungL :
			return "国标青劳";
		case NationMidL :
			return "国标中劳";
		case NationMidLM :
			return "国标中劳M";
		case NationMidLF :
			return "国标中劳F";
		case NationAfter :
			return "国标劳后";
		case NationAfterM :
			return "国标劳后M";
		case NationAfterF :
			return "国标劳后F";
		case GAAge :
			return "公安劳龄";
		case GAAgeM :
			return "公安劳龄M";
		case GAAgeF :
			return "公安劳龄F";
		case NationYoung :
			return "国标少年";
		case NationLabor :
			return "国标劳年";
		case NationLaborM :
			return "国标劳年M";
		case NationLaborF :
			return "国标劳年F";
		case NationOld :
			return "国标老年";
		case NationOldM :
			return "国标老年M";
		case NationOldF :
			return "国标老年F";
		case HighAge :
			return "高龄";
		case HighAgeM :
			return "高龄M";
		case HighAgeF :
			return "高龄F";
		case LongAge :
			return "长寿";
		case LongAgeM :
			return "长寿M";
		case LongAgeF :
			return "长寿F";
		case Mid :
			return "中位";
		case AgeDif :
			return "年龄差异度";
		case NationYoungRate :
			return "国标少年比";
		case NationOldRate :
			return "国标老年比";
		case NationOldYoungRate :
			return "国标老少比";
		case NationAllNeg :
			return "国标总负";
		case NationNegOld :
			return "国标负老";
		case NationNegYoung :
			return "国标负少";
		case NationLaborB :
			return "国标劳年B";
		case NationQingLaborB :
			return "国标青劳B";
		case NationHighAgeB :
			return "国标高龄B";
		case NationLongAgeB :
			return "国标长寿B";
		case IntnYoung :
			return "国际少年";
		case IntnLabor :
			return "国际劳年";
		case IntnQingLabor :
			return "国际青劳";
		case IntnOld :
			return "国际老年";
		case IntnOldM :
			return "国际老年M";
		case IntnOldF :
			return "国际老年F";
		case IntnYoungB :
			return "国际少年比";
		case IntnOldB :
			return "国际老年比";
		case IntnOldYoungB :
			return "国际老少比";
		case IntnAllNeg :
			return "国际总负";
		case IntnNegOld :
			return "国际负老";
		case IntnNegYoung :
			return "国际负少";
		case IntnLaborB :
			return "国际劳年B";
		case IntnQingLaborB :
			return "国际青劳B";
		case IntnHighAgeB :
			return "国际高龄B";
		case IntnLongAgeB :
			return "国际长寿B";
		case BirthF15 :
			return "育龄女15";
		case BirthM15 :
			return "婚育男15";
		case BirthF20 :
			return "育龄女20";
		case BirthM20 :
			return "婚育男20";
		case MarryXbb15 :
			return "婚育XBB15";
		case MarryXbb20 :
			return "婚育XBB20";
		case PeopDensity :
			return "人口密度";
		case WaterAvg :
			return "人均水资源";
		case LandAvg :
			return "人均耕地";
		default :
			throw new Exception ( "error param for getChinese in Summary" );
		}
	}
}
