package prclqz.core.enumLib;

import java.util.EnumMap;
import java.util.Map;

/**
 * 婚配预测表字段——最核心的计算数据保存在这里。所有的子女年龄婚配等
 * 表类型映射，慎重使用values()因为字段会有变化——可自创建类似values()操作
 * @time 2011-10-30 first edition
 * @time 2011-11-06 second edition: 增加  [堆积比例:duijiRate,释放模式:shifangMoshi]，学到一个教训——不要轻易给enum用abstract类型……
 * @author prclqz@zju.edu.cn
 *
 */
public enum HunpeiField {
//	 [子女数量-  0:DM 1:DF 2:DDBM 3:DDBF 4:NM 5:NF]
//	 [迁移子女-  0:DQYM 1:DQYF 2:DDBQYM 3:DDBQYF 4:NQYM 5:NQYF]
//   [迁移合计-  0:DQY 1:NQY 2:DDBQYS 3:HJQYF 4:HJQYM 5:HJQY]
//   [0:DQY 1:NQY 2:DDBQYS 3:HJQYF 4:HJQYM 5:HJQY]
//	 [堆积比例:duijiRate,释放模式:shifangMoshi]
//	 [HJM,HJF]
//	 [堆积丈夫:duijiHusband]
//	 [MSingleDJ:男单堆积, Yi2MSingle:已2男单, FSingleDJ:女单堆积,Yi2FSingle:已2女单,
//	 ShuangfeiDJ: 双非堆积,ShuangfeiDJ15: 双非堆积15,ShuangfeiDJ30: 双非堆积30,ShuangfeiDJ35: 双非堆积35, 
//	 Yi2Shuangfei:已2双非, 
//   SingleDJ15 : 单独堆积15, SinggleDJ30:单独堆积30，SingleDJ35:单独堆积35]
//   [SingleDJ:单独堆积,  Yi2Single:已2单独  ]
//   [婚配计算使用变量：
//		DD_2F,NMDF_2F,NFDM_2F,NN_2F,
//		DF0,DM0,NF0,NM0,
//		DD_F,NMDF_F,NFDM_F,NN_F,
//		DD  ,NMDF,  NFDM,  NN 
//		DF10, NF10,DM10,NM10
//		DF20, NF20,DM20,NM20]
//   [生育演算使用:
//		夫妇合计 coupleNum
//		各类生育  DDB, NNB , NMDFB , NFDMB 
//      奇怪的字段  FRD , FR1, FR2 ,
//		政策的   PlyDDB,PlyNNB,PlyNMDFB,PlyNFDMB,
//		政策释放B PlyRlsB, 政策生育 PlyBorn
//		单独释放B SingleRls , 双非释放B ShuangFeiRls]
//		堆积生育释放法 计算字段：
//		超生生育 Over_Birth[ 超生DDB,超生NNB,超生NMDFB,超生NFDMB,超生释放B,超生生育]
//	    [合J分龄SYL SYL_SUM, 双D分龄SYL SYL_DD ,单D分龄SYL SYL_D,双N分龄SYL SYL_NN ]
//		补充：DDBS,D,N,HJ
//		
//		独转非  SingleToNot
//		SWDM,SWDF,SWD
	/**
	  2        合J分龄SYL              
      3   双D分龄SYL              
      4   单D分龄SYL              
      5   双N分龄SYL              
      6   FRD                     
      7   FR                      
      8   FR1                     
      9   FR2                     
     10   独转非                  
     11   DDB                     
     12   NMDFB                   
     13   NFDMB                   
     14   NNB                     
     15   政策生育                
     16   政策DDB                 
     17   政策NMDFB               
     18   政策NFDMB               
     19   政策NNB                 
     20   政策释放B               
     21   超生生育                
     22   超生DDB                 
     23   超生NMDFB               
     24   超生NFDMB               
     25   超生NNB                 
     26   超生释放B               
     27   HJ                      
     28   HJM                     
     29   HJF                     
     30   D                       
     31   DM                      
     32   DF                      
     33   N                       
     34   NM                      
     35   NF                      
     36   DDBS                    
     37   DDBM                    
     38   DDBF                    
     39   NMDFBS                  
     40   NFDMBS                  
     41   NNBS                    
     42   HJQY                    
     43   HJQYM                   
     44   HJQYF                   
     45   DQY                     
     46   DQYM                    
     47   DQYF                    
     48   NQY                     
     49   NQYM                    
     50   NQYF                    
     51   DDBQYS                  
     52   DDBQYM                  
     53   DDBQYF                  
     54   DM0                     
     55   DF0                     
     56   NM0                     
     57   NF0                     
     58   MIDM0                   
     59   MIDF0                   
     60   MINM0                   
     61   MINF0                   
     62   夫妇合计                
     63   DD                      
     64   NMDF                    
     65   NFDM                    
     66   NN                      
     67   DD_F                    
     68   NMDF_F                  
     69   NFDM_F                  
     70   NN_F                    
     71   DD_M                    
     72   NMDF_M                  
     73   NFDM_M                  
     74   NN_M                    
     75   DD非F                   
     76   NMDF非F                 
     77   NFDM非F                 
     78   NN非F                   
     79   DD非F_F                 
     80   NMDF非F_F               
     81   NFDM非F_F               
     82   NN非F_F                 
     83   DD非F_M                 
     84   NMDF非F_M               
     85   NFDM非F_M               
     86   NN非F_M                 
     87   DD农F                   
     88   NMDF农F                 
     89   NFDM农F                 
     90   NN农F                   
     91   DD农F_F                 
     92   NMDF农F_F               
     93   NFDM农F_F               
     94   NN农F_F                 
     95   DD农F_M                 
     96   NMDF农F_M               
     97   NFDM农F_M               
     98   NN农F_M                 
     99   DD_CFXF                 
    100   NMDF_CFXF               
    101   NFDM_CFXF               
    102   NN_CFXF                 
    103   DD_CFXN                 
    104   NMDF_CFXN               
    105   NFDM_CFXN               
    106   NN_CFXN                 
    107   DD_CNXF                 
    108   NMDF_CNXF               
    109   NFDM_CNXF               
    110   NN_CNXF                 
    111   DD_CNXN                 
    112   NMDF_CNXN               
    113   NFDM_CNXN               
    114   NN_CNXN                 
    115   DD_XFCF                 
    116   NMDF_XFCF               
    117   NFDM_XFCF               
    118   NN_XFCF                 
    119   DD_XFCN                 
    120   NMDF_XFCN               
    121   NFDM_XFCN               
    122   NN_XFCN                 
    123   DD_XNCF                 
    124   NMDF_XNCF               
    125   NFDM_XNCF               
    126   NN_XNCF                 
    127   DD_XNCN                 
    128   NMDF_XNCN               
    129   NFDM_XNCN               
    130   NN_XNCN                 
    131   DD0                     
    132   DM10                    
    133   DM20                    
    134   DM30                    
    135   DM40                    
    136   DF10                    
    137   DF20                    
    138   DF30                    
    139   DF40                    
    140   NM10                    
    141   NM20                    
    142   NM30                    
    143   NM40                    
    144   NF10                    
    145   NF20                    
    146   NF30                    
    147   NF40                    
    148   FR201                   
    149   FR202                   
    150   FR203                   
    151   FR204                   
    152   FR205                   
    153   FR206                   
    154   FR207                   
    155   FR208                   
    156   FR209                   
    157   FR210                   
    158   FR211                   
    159   FR212                   
    160   SWD                     
    161   SWDM                    
    162   SWDF                    
    163   单独释放B               
    164   双非释放B               
    165   单独堆积                
    166   单独堆积15              
    167   单独堆积30              
    168   单独堆积35              
    169   堆积丈夫                
    170   女单堆积                
    171   男单堆积                
    172   已2单独                 
    173   已2女单                 
    174   已2男单                 
    175   双非堆积                
    176   双非堆积15              
    177   双非堆积30              
    178   双非堆积35              
    179   已2双非                 
    180   堆积比例                
    181   释放模式                
    182   双独概率M               
    183   双独概率F               
    184   男单概率M               
    185   男单概率F               
    186   女单概率M               
    187   女单概率F               
    188   双非概率M               
    189   双非概率F               
    190   NF双独GLM               
    191   NF双独GLF               
    192   NF男单GLM               
    193   NF男单GLF               
    194   NF女单GLM               
    195   NF女单GLF               
    196   NF双非GLM               
    197   NF双非GLF               
	 */
	SYL_SUM,
	SYL_DD,
	SYL_D,
	SYL_NN,
	
	FRD ,
	FR,
	FR1,
	FR2,
	
	SingleToNot,
	
	DDB,
	NMDFB,
	NFDMB,
	NNB,
	
	PlyBorn ,
	PlyDDB ,
	PlyNMDFB ,
	PlyNFDMB ,
	PlyNNB ,
	PlyRlsB ,
	
	Over_Birth,
	OverB_DDB ,
	OverB_NMDFB ,
	OverB_NFDMB ,
	OverB_NNB ,
	OverB_RlsB ,
	
	HJ,
	HJM,
	HJF,
	D,
	DM,
	DF,
	N,
	NM,
	NF,
	DDBS,
	DDBM,
	DDBF,
	NMDFBS,
	NFDMBS,
	NNBS,
	HJQY,
	HJQYM,
	HJQYF,
	DQY,
	DQYM,
	DQYF,
	NQY,
	NQYM,
	NQYF,
	DDBQYS,
	DDBQYM,
	DDBQYF,
	DM0,
	DF0,
	NM0,
	NF0,
	MIDM0,
	MIDF0,
	MINM0,
	MINF0,
	
	coupleNum ,
	
	DD,
	NMDF,
	NFDM,
	NN,
	
	DD_F,
	NMDF_F,
	NFDM_F,
	NN_F,
	
	DD_M,
	NMDF_M,
	NFDM_M,
	NN_M,
	
	/**
	 * 75~98 begin
	 */
	/**
	 * 非 F=》_NOT_F  农F=》_Nong_F
	 */
	DD_NOT_F,
	NMDF_NOT_F,
	NFDM_NOT_F,
	NN_NOT_F,
	
	DD_NOT_F_F,
	NMDF_NOT_F_F,
	NFDM_NOT_F_F,
	NN_NOT_F_F,
	
	DD_NOT_F_M,
	NMDF_NOT_F_M,
	NFDM_NOT_F_M,
	NN_NOT_F_M,
	
	DD_Nong_F,
	NMDF_Nong_F,
	NFDM_Nong_F,
	NN_Nong_F,
	
	DD_F_Nong_F,
	NMDF_F_Nong_F,
	NFDM_F_Nong_F,
	NN_F_Nong_F,
	
	DD_M_Nong_F,
	NMDF_M_Nong_F,
	NFDM_M_Nong_F,
	NN_M_Nong_F,
	
	/**
	 *  99~102 _CFXF
	 */
	DD_CFXF,
	NMDF_CFXF,
	NFDM_CFXF,
	NN_CFXF,
	
	/**
	 * 103~106 _CFXN
	 */
	DD_CFXN,
	NMDF_CFXN,
	NFDM_CFXN,
	NN_CFXN,
	
	/**
	 * 107~110 _CNXF
	 */
	DD_CNXF,
	NMDF_CNXF,
	NFDM_CNXF,
	NN_CNXF,
	
	/**
	 * 111~114 _CNXN
	 */
	DD_CNXN,
	NMDF_CNXN,
	NFDM_CNXN,
	NN_CNXN,
	
	/**
	 *  115~130 _Xxxx
	 */
	DD_XFXF,
	NMDF_XFXF,
	NFDM_XFXF,
	NN_XFXF,
	
	DD_XFXN,
	NMDF_XFXN,
	NFDM_XFXN,
	NN_XFXN,
	
	DD_XNXF,
	NMDF_XNXF,
	NFDM_XNXF,
	NN_XNXF,
	
	DD_XNXN,
	NMDF_XNXN,
	NFDM_XNXN,
	NN_XNXN,
	
	/**
	 * 131~147
	 */
	DD0,
	DM10,
	DM20,
	DM30,
	DM40,
	DF10,
	DF20,
	DF30,
	DF40,
	
	NM10,
	NM20,
	NM30,
	NM40,
	NF10,
	NF20,
	NF30,
	NF40,
	
	/**
	 * 148~159
	 */
	
	FR201,
	FR202,
	FR203,
	FR204,
	FR205,
	FR206,
	FR207,
	FR208,
	FR209,
	FR210,
	FR211,
	FR212,
	
	/**
	 * 160~162
	 */
	SWD,
	SWDM,
	SWDF,
	
	/**
	 * 163~169
	 */
	SingleRls  ,
	ShuangFeiRls ,
	SingleDJ,
	SingleDJ15,
	SingleDJ30,
	SingleDJ35,
	duijiHusband,
	
	/**
	 * 170~174
	 */
	FSingleDJ,
	MSingleDJ,
	Yi2Single,
	Yi2FSingle,
	Yi2MSingle,
	
	/**
	 * 175~179
	 */
	ShuangfeiDJ,
	ShuangfeiDJ15,
	ShuangfeiDJ30,
	ShuangfeiDJ35,
	Yi2Shuangfei,
	
	/**
	 * 180~189
	 */
	duijiRate ,
	shifangMoshi ,
	DD_GLM,
	DD_GLF,
	MSingle_GLM,
	MSingle_GLF,
	FSingle_GLM,
	FSingle_GLF,
	ShuangfeiGLM,
	ShuangfeiGLF,
	
	/**
	 * 190~197
	 */
	NF_DD_GLM,
	NF_DD_GLF,
	NF_M_S_GLM,
	NF_M_S_GLF,
	NF_F_S_GLM,
	NF_F_S_GLF,
	NF_SF_GLM,
	NF_SF_GLF,
	
//	DD_2F,
//	NMDF_2F,
//	NFDM_2F,
//	NN_2F,
	;
	
//	public abstract XB getXB();
//	public abstract HunpeiField getQYField();
	public static XB getXB( HunpeiField hf ){
		String str = hf.toString ( );
		char[] cs = str.toCharArray ( );
		if( cs[cs.length-1] == 'M')
			return XB.Male;
		else
			return XB.Female;
	}
	
	private static HunpeiField [] hps = {};
	public static HunpeiField[] getStatisticsFields(){
		return hps;
	}
	public static HunpeiField getQYField ( HunpeiField hf ) throws Exception
	{
		switch ( hf )
		{
		case DM :
			return DQYM;
		case DF :
			return DQYF;
		case DDBM :
			return DDBQYM;
		case DDBF :
			return DDBQYF;
		case NM :
			return NQYM;
		case NF :
			return NQYF;
		default :
			throw new Exception ( "error parm for getQYField--"
					+ hf.toString ( ) );
		}
	}
	
	/**
	 * 
	 * @param type 0:In  1:Out
	 * @return
	 */
	public static QYPHField getQYPHField ( HunpeiField hf , int type )
	{
		String str = hf.toString ( );
		char [ ] cs = str.toCharArray ( );
		if ( cs [ cs.length - 1 ] == 'M' )
		{
			if ( type == 0 )
				return QYPHField.MImmigrate;
			else
				return QYPHField.MEmigrate;
		} else
		{
			if ( type == 0 )
				return QYPHField.FImmigrate;
			else
				return QYPHField.FEmigrate;
		}
	}
	/**
	 * 
	 * @param type 0:In  1:Out
	 * @return
	 */
//	public abstract QYPHField getQYPHField(int type);
	public static HunpeiField[] valuesOfZinv(){
		HunpeiField[] znArr = {HJM,HJF,DM,DF,DDBM,DDBF,NM,NF};
		return znArr;
	}	
	public static HunpeiField[] valuesOfZinv3(){
		HunpeiField[] znArr = {HJ,HJM,HJF,D,DM,DF,DDBM,DDBF,N,NM,NF};
		return znArr;
	}
	public static HunpeiField[] valuesOfZinv2(){
		HunpeiField[] znArr = {DM,DF,DDBM,DDBF,NM,NF};
		return znArr;
	}
	
	public static HunpeiField[] valuesOfZinvQY(){
		HunpeiField[] znArr = {DQYM,DQYF,DDBQYM,DDBQYF,NQYM,NQYF};
		return znArr;
	}
	
//	 [MSingleDJ:男单堆积, Yi2MSingle:已2男单, FSingleDJ:女单堆积,Yi2FSingle:已2女单,
//	 ShuangfeiDJ: 双非堆积, Yi2Shuangfei:已2双非, 
//  SingleDJ15 : 单独堆积15, SinggleDJ30:单独堆积30，SingleDJ35:单独堆积35]
	public static HunpeiField[] valuesOfDJ(){
		HunpeiField[] znArr = {MSingleDJ,Yi2MSingle,FSingleDJ,Yi2FSingle,ShuangfeiDJ,Yi2Shuangfei,
								SingleDJ15,SingleDJ30,SingleDJ35};
		return znArr;
	}
	
	public static HunpeiField[] valuesOfHJ(){
		HunpeiField[] znArr = {DQY,NQY,DDBQYS,HJQYF,HJQYM,HJQY};
		return znArr;
	}
	
	
	public static String getChinese(HunpeiField hp) throws Exception{
		switch(hp){
		case SYL_SUM :
			return "合J分龄SYL";
		case SYL_DD :
			return "双D分龄SYL";
		case SYL_D :
			return "单D分龄SYL";
		case SYL_NN :
			return "双N分龄SYL";
		case FRD :
			return "FRD";
		case FR :
			return "FR";
		case FR1 :
			return "FR1";
		case FR2 :
			return "FR2";
		case SingleToNot :
			return "独转非";
		case DDB :
			return "DDB";
		case NMDFB :
			return "NMDFB";
		case NFDMB :
			return "NFDMB";
		case NNB :
			return "NNB";
		case PlyBorn :
			return "政策生育";
		case PlyDDB :
			return "政策DDB";
		case PlyNMDFB :
			return "政策NMDFB";
		case PlyNFDMB :
			return "政策NFDMB";
		case PlyNNB :
			return "政策NNB";
		case PlyRlsB :
			return "政策释放B";
		case Over_Birth :
			return "超生生育";
		case OverB_DDB :
			return "超生DDB";
		case OverB_NMDFB :
			return "超生NMDFB";
		case OverB_NFDMB :
			return "超生NFDMB";
		case OverB_NNB :
			return "超生NNB";
		case OverB_RlsB :
			return "超生释放B";
		case HJ :
			return "HJ";
		case HJM :
			return "HJM";
		case HJF :
			return "HJF";
		case D :
			return "D";
		case DM :
			return "DM";
		case DF :
			return "DF";
		case N :
			return "N";
		case NM :
			return "NM";
		case NF :
			return "NF";
		case DDBS :
			return "DDBS";
		case DDBM :
			return "DDBM";
		case DDBF :
			return "DDBF";
		case NMDFBS :
			return "NMDFBS";
		case NFDMBS :
			return "NFDMBS";
		case NNBS :
			return "NNBS";
		case HJQY :
			return "HJQY";
		case HJQYM :
			return "HJQYM";
		case HJQYF :
			return "HJQYF";
		case DQY :
			return "DQY";
		case DQYM :
			return "DQYM";
		case DQYF :
			return "DQYF";
		case NQY :
			return "NQY";
		case NQYM :
			return "NQYM";
		case NQYF :
			return "NQYF";
		case DDBQYS :
			return "DDBQYS";
		case DDBQYM :
			return "DDBQYM";
		case DDBQYF :
			return "DDBQYF";
		case DM0 :
			return "DM0";
		case DF0 :
			return "DF0";
		case NM0 :
			return "NM0";
		case NF0 :
			return "NF0";
		case MIDM0 :
			return "MIDM0";
		case MIDF0 :
			return "MIDF0";
		case MINM0 :
			return "MINM0";
		case MINF0 :
			return "MINF0";
		case coupleNum :
			return "夫妇合计";
		case DD :
			return "DD";
		case NMDF :
			return "NMDF";
		case NFDM :
			return "NFDM";
		case NN :
			return "NN";
		case DD_F :
			return "DD_F";
		case NMDF_F :
			return "NMDF_F";
		case NFDM_F :
			return "NFDM_F";
		case NN_F :
			return "NN_F";
		case DD_M :
			return "DD_M";
		case NMDF_M :
			return "NMDF_M";
		case NFDM_M :
			return "NFDM_M";
		case NN_M :
			return "NN_M";
		case DD_NOT_F :
			return "DD非F";
		case NMDF_NOT_F :
			return "NMDF非F";
		case NFDM_NOT_F :
			return "NFDM非F";
		case NN_NOT_F :
			return "NN非F";
		case DD_NOT_F_F :
			return "DD非F_F";
		case NMDF_NOT_F_F :
			return "NMDF非F_F";
		case NFDM_NOT_F_F :
			return "NFDM非F_F";
		case NN_NOT_F_F :
			return "NN非F_F";
		case DD_NOT_F_M :
			return "DD非F_M";
		case NMDF_NOT_F_M :
			return "NMDF非F_M";
		case NFDM_NOT_F_M :
			return "NFDM非F_M";
		case NN_NOT_F_M :
			return "NN非F_M";
		case DD_Nong_F :
			return "DD农F";
		case NMDF_Nong_F :
			return "NMDF农F";
		case NFDM_Nong_F :
			return "NFDM农F";
		case NN_Nong_F :
			return "NN农F";
		case DD_F_Nong_F :
			return "DD农F_F";
		case NMDF_F_Nong_F :
			return "NMDF农F_F";
		case NFDM_F_Nong_F :
			return "NFDM农F_F";
		case NN_F_Nong_F :
			return "NN农F_F";
		case DD_M_Nong_F :
			return "DD农F_M";
		case NMDF_M_Nong_F :
			return "NMDF农F_M";
		case NFDM_M_Nong_F :
			return "NFDM农F_M";
		case NN_M_Nong_F :
			return "NN农F_M";
		case DD_CFXF :
			return "DD_CFXF";
		case NMDF_CFXF :
			return "NMDF_CFXF";
		case NFDM_CFXF :
			return "NFDM_CFXF";
		case NN_CFXF :
			return "NN_CFXF";
		case DD_CFXN :
			return "DD_CFXN";
		case NMDF_CFXN :
			return "NMDF_CFXN";
		case NFDM_CFXN :
			return "NFDM_CFXN";
		case NN_CFXN :
			return "NN_CFXN";
		case DD_CNXF :
			return "DD_CNXF";
		case NMDF_CNXF :
			return "NMDF_CNXF";
		case NFDM_CNXF :
			return "NFDM_CNXF";
		case NN_CNXF :
			return "NN_CNXF";
		case DD_CNXN :
			return "DD_CNXN";
		case NMDF_CNXN :
			return "NMDF_CNXN";
		case NFDM_CNXN :
			return "NFDM_CNXN";
		case NN_CNXN :
			return "NN_CNXN";
		case DD_XFXF :
			return "DD_XFCF";
		case NMDF_XFXF :
			return "NMDF_XFCF";
		case NFDM_XFXF :
			return "NFDM_XFCF";
		case NN_XFXF :
			return "NN_XFCF";
		case DD_XFXN :
			return "DD_XFCN";
		case NMDF_XFXN :
			return "NMDF_XFCN";
		case NFDM_XFXN :
			return "NFDM_XFCN";
		case NN_XFXN :
			return "NN_XFCN";
		case DD_XNXF :
			return "DD_XNCF";
		case NMDF_XNXF :
			return "NMDF_XNCF";
		case NFDM_XNXF :
			return "NFDM_XNCF";
		case NN_XNXF :
			return "NN_XNCF";
		case DD_XNXN :
			return "DD_XNCN";
		case NMDF_XNXN :
			return "NMDF_XNCN";
		case NFDM_XNXN :
			return "NFDM_XNCN";
		case NN_XNXN :
			return "NN_XNCN";
		case DD0 :
			return "DD0";
		case DM10 :
			return "DM10";
		case DM20 :
			return "DM20";
		case DM30 :
			return "DM30";
		case DM40 :
			return "DM40";
		case DF10 :
			return "DF10";
		case DF20 :
			return "DF20";
		case DF30 :
			return "DF30";
		case DF40 :
			return "DF40";
		case NM10 :
			return "NM10";
		case NM20 :
			return "NM20";
		case NM30 :
			return "NM30";
		case NM40 :
			return "NM40";
		case NF10 :
			return "NF10";
		case NF20 :
			return "NF20";
		case NF30 :
			return "NF30";
		case NF40 :
			return "NF40";
		case FR201 :
			return "FR201";
		case FR202 :
			return "FR202";
		case FR203 :
			return "FR203";
		case FR204 :
			return "FR204";
		case FR205 :
			return "FR205";
		case FR206 :
			return "FR206";
		case FR207 :
			return "FR207";
		case FR208 :
			return "FR208";
		case FR209 :
			return "FR209";
		case FR210 :
			return "FR210";
		case FR211 :
			return "FR211";
		case FR212 :
			return "FR212";
		case SWD :
			return "SWD";
		case SWDM :
			return "SWDM";
		case SWDF :
			return "SWDF";
		case SingleRls :
			return "单独释放B";
		case ShuangFeiRls :
			return "双非释放B";
		case SingleDJ :
			return "单独堆积";
		case SingleDJ15 :
			return "单独堆积15";
		case SingleDJ30 :
			return "单独堆积30";
		case SingleDJ35 :
			return "单独堆积35";
		case duijiHusband :
			return "堆积丈夫";
		case FSingleDJ :
			return "女单堆积";
		case MSingleDJ :
			return "男单堆积";
		case Yi2Single :
			return "已2单独";
		case Yi2FSingle :
			return "已2女单";
		case Yi2MSingle :
			return "已2男单";
		case ShuangfeiDJ :
			return "双非堆积";
		case ShuangfeiDJ15 :
			return "双非堆积15";
		case ShuangfeiDJ30 :
			return "双非堆积30";
		case ShuangfeiDJ35 :
			return "双非堆积35";
		case Yi2Shuangfei :
			return "已2双非";
		case duijiRate :
			return "堆积比例";
		case shifangMoshi :
			return "释放模式";
		case DD_GLM :
			return "双独概率M";
		case DD_GLF :
			return "双独概率F";
		case MSingle_GLM :
			return "男单概率M";
		case MSingle_GLF :
			return "男单概率F";
		case FSingle_GLM :
			return "女单概率M";
		case FSingle_GLF :
			return "女单概率F";
		case ShuangfeiGLM :
			return "双非概率M";
		case ShuangfeiGLF :
			return "双非概率F";
		case NF_DD_GLM :
			return "NF双独GLM";
		case NF_DD_GLF :
			return "NF双独GLF";
		case NF_M_S_GLM :
			return "NF男单GLM";
		case NF_M_S_GLF :
			return "NF男单GLF";
		case NF_F_S_GLM :
			return "NF女单GLM";
		case NF_F_S_GLF :
			return "NF女单GLF";
		case NF_SF_GLM :
			return "NF双非GLM";
		case NF_SF_GLF :
			return "NF双非GLF";
			default:
				throw new Exception ( "not imple for enum"+hp.toString ( ) );
		}
	}
	
	public static HunpeiField getZinvFromZinvqy(HunpeiField hp)
	{
		//0:DQYM 1:DQYF 2:DDBQYM 3:DDBQYF 4:NQYM 5:NQYF
		switch(hp){
		case DQYM:
			return DM;
		case DQYF:
			return DF;
		case DDBQYM:
			return DDBM;
		case DDBQYF:
			return DDBF;
		case NQYM:
			return NM;
		case NQYF:
		default:
			return NF;
		}
	}
	
	private static HunpeiField[] hps2={SingleToNot,
		DDB,
		NMDFB,
		NFDMB,
		NNB,
		
		PlyBorn ,
		PlyDDB ,
		PlyNMDFB ,
		PlyNFDMB ,
		PlyNNB ,
		PlyRlsB ,
		
		Over_Birth,
		OverB_DDB ,
		OverB_NMDFB ,
		OverB_NFDMB ,
		OverB_NNB ,
		OverB_RlsB ,
		
		HJ,
		HJM,
		HJF,
		D,
		DM,
		DF,
		N,
		NM,
		NF,
		DDBS,
		DDBM,
		DDBF,
		NMDFBS,
		NFDMBS,
		NNBS,
		HJQY,
		HJQYM,
		HJQYF,
		DQY,
		DQYM,
		DQYF,
		NQY,
		NQYM,
		NQYF,
		DDBQYS,
		DDBQYM,
		DDBQYF,
		DM0,
		DF0,
		NM0,
		NF0,
		MIDM0,
		MIDF0,
		MINM0,
		MINF0,
		
		coupleNum ,
		
		DD,
		NMDF,
		NFDM,
		NN,
		
		DD_F,
		NMDF_F,
		NFDM_F,
		NN_F,
		
		DD_M,
		NMDF_M,
		NFDM_M,
		NN_M,
		
		/**
		 * 75~98 begin
		 */
		/**
		 * 非 F=》_NOT_F  农F=》_Nong_F
		 */
		DD_NOT_F,
		NMDF_NOT_F,
		NFDM_NOT_F,
		NN_NOT_F,
		
		DD_NOT_F_F,
		NMDF_NOT_F_F,
		NFDM_NOT_F_F,
		NN_NOT_F_F,
		
		DD_NOT_F_M,
		NMDF_NOT_F_M,
		NFDM_NOT_F_M,
		NN_NOT_F_M,
		
		DD_Nong_F,
		NMDF_Nong_F,
		NFDM_Nong_F,
		NN_Nong_F,
		
		DD_F_Nong_F,
		NMDF_F_Nong_F,
		NFDM_F_Nong_F,
		NN_F_Nong_F,
		
		DD_M_Nong_F,
		NMDF_M_Nong_F,
		NFDM_M_Nong_F,
		NN_M_Nong_F,
		
		/**
		 *  99~102 _CFXF
		 */
		DD_CFXF,
		NMDF_CFXF,
		NFDM_CFXF,
		NN_CFXF,
		
		/**
		 * 103~106 _CFXN
		 */
		DD_CFXN,
		NMDF_CFXN,
		NFDM_CFXN,
		NN_CFXN,
		
		/**
		 * 107~110 _CNXF
		 */
		DD_CNXF,
		NMDF_CNXF,
		NFDM_CNXF,
		NN_CNXF,
		
		/**
		 * 111~114 _CNXN
		 */
		DD_CNXN,
		NMDF_CNXN,
		NFDM_CNXN,
		NN_CNXN,
		
		/**
		 *  115~130 _Xxxx
		 */
		DD_XFXF,
		NMDF_XFXF,
		NFDM_XFXF,
		NN_XFXF,
		
		DD_XFXN,
		NMDF_XFXN,
		NFDM_XFXN,
		NN_XFXN,
		
		DD_XNXF,
		NMDF_XNXF,
		NFDM_XNXF,
		NN_XNXF,
		
		DD_XNXN,
		NMDF_XNXN,
		NFDM_XNXN,
		NN_XNXN,
		
		/**
		 * 131~147
		 */
		DD0,
		DM10,
		DM20,
		DM30,
		DM40,
		DF10,
		DF20,
		DF30,
		DF40,
		
		NM10,
		NM20,
		NM30,
		NM40,
		NF10,
		NF20,
		NF30,
		NF40,
		
		/**
		 * 148~159
		 */
		
		FR201,
		FR202,
		FR203,
		FR204,
		FR205,
		FR206,
		FR207,
		FR208,
		FR209,
		FR210,
		FR211,
		FR212,
		
		/**
		 * 160~162
		 */
		SWD,
		SWDM,
		SWDF,
		
		/**
		 * 163~169
		 */
		SingleRls  ,
		ShuangFeiRls ,
		SingleDJ,
		SingleDJ15,
		SingleDJ30,
		SingleDJ35,
		duijiHusband,
		
		/**
		 * 170~174
		 */
		FSingleDJ,
		MSingleDJ,
		Yi2Single,
		Yi2FSingle,
		Yi2MSingle,
		
		/**
		 * 175~179
		 */
		ShuangfeiDJ,
		ShuangfeiDJ15,
		ShuangfeiDJ30,
		ShuangfeiDJ35,
		Yi2Shuangfei,};
	
	/**
	 * FOR F1=10 TO 179	&&89
	 * @return
	 */
	public static HunpeiField[] getARHunpeiFileds()
	{
		return hps2;
	}
}
