﻿package prclqz.core.enumLib;

/**
 * 生育及释放模式表字段——未全，慎用values 
 * @author prclqz@zju.edu.cn
 *
 */
public enum SYSFMSField {
	//待生二孩比,缓释模式,突释模式,正常模式
	//意愿模式,分释模式
	wait2chdrn,
	/**
	 * 缓释=二五年生
	 */
	huanshi,
	/**
	 * 突释=二年内生
	 */
	tushi,
	/**
	 * 正常
	 */
	zhengchang,
	/**
	 * 意愿-想生
	 */
	yiyuan,
	/**
	 * 分释=晚释=五年后生
	 */
	fenshi,//晚释
	SYSMB,
	SYSMB1,
	SYSMB2;
	public static SYSFMSField getSFMSfromId(int id){
		switch(id){
		case 147:
			return fenshi;
		case 148:
			return huanshi;
		case 149:
			return tushi;
		case 150:
			return yiyuan;
		case 151:
			return zhengchang;
			
		case 1:
			return yiyuan;
		case 2:
			return tushi;
		case 3:
			return huanshi;
		case 4:
			return fenshi;
		default:
			return null;	
		}
	}

	public static String getChinese(SYSFMSField sf) throws Exception{
		switch(sf){
		case fenshi:
			return "分释";
		case huanshi:
			return "缓释";
		case tushi:
			return "突释";
		case wait2chdrn:
			return "待生二孩比";
		case zhengchang:
			return "正常";
		case yiyuan:
			return "想生";
			default:
				throw new Exception ( "error for getChinese in SYSFMS " );
		}
	}
	
	/**
	 * 生育意愿
	 * @param sf
	 * @return
	 * @throws Exception
	 */
	public static String getSYYYChinese(SYSFMSField sf) throws Exception{
		switch(sf){
		case fenshi:
			return "晚释";
		case huanshi:
			return "缓释";
		case tushi:
			return "突释";
		case yiyuan:
			return "想生";
		case wait2chdrn:
			return "待生二孩比";
		case zhengchang:
			return "正常";
			default:
				throw new Exception ( "error for getChinese in SYSFMS " );
		}
	}
}

