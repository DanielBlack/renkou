package prclqz.core.enumLib;
/**
 * 地区生育率与迁移摘要-表映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum SumBM
{
/***
 * `年份` smallint(6) DEFAULT NULL,==》作为索引
 * 
 `地区`,
  `政策方案`,
  `可实现TFR`,
  `政策生育率`,
  `超生生育率`,
  `超生孩子数`,
  `FN女单终身`,
  `FN男单终身`,
  `FN双非终身`,
  `NY女单终身`,
  `NY男单终身`,
  `NY双非终身`,
  `DQ时期TFR`,
  `CZ时期TFR`,
  `FN时期TFR`,
  `NC时期TFR`,
  `NY时期TFR`,
  `DQ迁移合计`,
  `城镇迁移`,
  `非农迁移`,
  `农村迁移`,
  `农业迁移`,
  `本地迁移`,
  `QG累计迁移`,
 */
	Dq,
	Policy,
	ImplTFR,
	PolicyBirtRate,
	OverBirthRate,
	OverBirthNum,
	FNFSingle,
	FNMSingle,
	FNSFSingle,
	NYFSingle,
	NYMSingle,
	NYSFSingle,
	DQTFR,
	CZTFR,
	FNTFR,
	NCTFR,
	NYTFR,
	DQMigS,
	/**
	 * 原来的错误：
	 * ChengshiMig,
	   NongchunMig,
	   NongyeMig,
	   FeinongMig,
	 */
	ChengshiMig,
	FeinongMig,
	NongchunMig,
	NongyeMig,
	
	LocalMig,
	QGMigS,
	;
	
	public static String getChinese ( SumBM sbm ) throws Exception
	{
		switch ( sbm )
		{
		case Dq :
			return "地区";
		case Policy :
			return "政策方案";
		case ImplTFR :
			return "可实现TFR";
		case PolicyBirtRate :
			return "政策生育率";
		case OverBirthRate :
			return "超生生育率";
		case OverBirthNum :
			return "超生孩子数";
		case FNFSingle :
			return "FN女单终身";
		case FNMSingle :
			return "FN男单终身";
		case FNSFSingle :
			return "FN双非终身";
		case NYFSingle :
			return "NY女单终身";
		case NYMSingle :
			return "NY男单终身";
		case NYSFSingle :
			return "NY双非终身";
		case DQTFR :
			return "DQ时期TFR";
		case CZTFR :
			return "CZ时期TFR";
		case FNTFR :
			return "FN时期TFR";
		case NCTFR :
			return "NC时期TFR";
		case NYTFR :
			return "NY时期TFR";
		case DQMigS :
			return "DQ迁移合计";
		case ChengshiMig :
			return "城镇迁移";
		case FeinongMig :
			return "非农迁移";
		case NongchunMig :
			return "农村迁移";
		case NongyeMig :
			return "农业迁移";
		case LocalMig :
			return "本地迁移";
		case QGMigS :
			return "QG累计迁移";
		default :
			throw new Exception ( "error param for getChinese in SumBM" + sbm );
		}
	}
	
}
