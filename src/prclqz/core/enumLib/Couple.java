package prclqz.core.enumLib;
/**
 * 夫妇及子女表结构 --表映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum Couple
{
/**
 *   `非农政策1` ,  `农业政策1` ,  `非农政策2` ,
  `农业政策2` ,  `非农政策3` ,  `农业政策3` ,  `TFR` ,
  `双独TFR` ,  `单独TFR` ,  `双非TFR` ,  `非农双独` ,
  `非农男单` ,  `非农女单` ,  `非农双非` ,  `农业双独` ,
  `农业男单` ,  `农业女单` ,  `农业双非` ,  `合计夫妇数` ,
  `双独夫妇数` ,  `单独夫妇数` ,  `女单夫妇数` ,  `男单夫妇数` ,
  `单独堆积` ,  `已2单独` ,  `双非夫妇数` ,  `双非堆积` ,
  `已2双非` ,  `双独生育数` ,  `单独生育数` ,  `女单生育数` ,
  `男单生育数` ,  `双非生育数` ,  `C超生` ,  `X超生` ,
  `D` ,  `DM` ,  `DF` ,  `N` ,  `NM` ,  `NF` ,  `可二后` ,  `DDBM` ,
  `DDBF` ,  `独生子女30` ,  `独生子30` ,  `独生女30` ,  `非独子女30` ,  `非独子30` ,
  `非独女30` ,  `可二后30` ,  `可二后30M` ,  `可二后30F` ,
  `独生子女SW` ,  `独生子SW` ,  `独生女SW` ,  `非独子女SW` ,
  `非独子SW` ,  `非独女SW` ,  `可二后SW` ,  `可二后SWM` ,
  `可二后SWF` ,  `一孩父母` ,  `一孩父` ,  `一孩母` ,
  `特扶` ,  `特扶M` ,  `特扶F` ,  `未婚F` ,  `未婚M` ,  `未婚DF` ,
  `未婚DM` ,  `未婚NF` ,  `未婚NM` ,  `农业未婚F` ,  `农业未婚M` ,  `非农未婚F` ,
  `非农未婚M` ,  `农业未婚DF` ,  `农业未婚DM` ,  `农业未婚NF` ,
  `农业未婚NM` ,  `非农未婚DF` ,  `非农未婚DM` ,  `非农未婚NF` ,
  `非农未婚NM` ,  `双独概率M` ,  `双独概率F` ,  `男单概率M` ,
  `男单概率F` ,  `女单概率M` ,  `女单概率F` , 
  */
	//Year, ——年份是索引！！
	FnPolicy1,NyPolicy1,FnPolicy2,
	NyPolicy2,FnPolicy3,NyPolicy3,TFR,
	SDTFR,DDTFR,SFTFR,FNShuangD,
	FNMaleD,FNFemalD,FNShuangF,NYShuangD,
	NYMaleD,NYFemalD,NYShuangF,CoupleS,
	SDCouple,DDCouple,FDCouple,MDCouple,
	SingleDJ,Yi2Single,SFCouple,SFDJ,
	Yi2SF,SDBirth,DDBirth,FDBirth,
	MDBirth,SFBirth,COverBirth,XOverBirth,
	D,DM,DF,N,NM,NF,Ke2Hou,DDBM,
	DDBF,Du30,DuM30,DuF30,FeiDu30,FeiDuM30,
	FeiDuF30,Ke2Hou30,Ke2Hou30M,Ke2Hou30F,
	Du_SW,DuM_SW,DuF_SW,Feidu_SW,
	FeiduM_SW,FeiduF_SW,Ke2Hou_SW,Ke2Hou_SWM,
	Ke2Hou_SWF,YiHaiP,YiHaiFa,YiHaiMa,
	TeFu,TeFuM,TeFuF,WeiHunF,WeiHunM,WeiHunDF,
	WeiHunDM,WeiHunNF,WeiHunNM,NyWeiHunF,NyWeiHunM,FnWeiHunF,
	FnWeiHunM,NyWeiHunDF,NyWeiHunDM,NyWeiHunNF,
	NyWeiHunNM,FnWeiHunDF,FnWeiHunDM,FnWeiHunNF,
	FnWeiHunNM,SDGLM,SDGLF,MSingleGLM,
	MSingleGLF,FSingleGLM,FSingleGLF,;
	
	
	public static String getChinese(Couple cp) throws Exception{
		switch(cp){
		case FnPolicy1 :
			return "非农政策1";
			case NyPolicy1 :
			return "农业政策1";
			case FnPolicy2 :
			return "非农政策2";
			case NyPolicy2 :
			return "农业政策2";
			case FnPolicy3 :
			return "非农政策3";
			case NyPolicy3 :
			return "农业政策3";
			case TFR :
			return "TFR";
			case SDTFR :
			return "双独TFR";
			case DDTFR :
			return "单独TFR";
			case SFTFR :
			return "双非TFR";
			case FNShuangD :
			return "非农双独";
			case FNMaleD :
			return "非农男单";
			case FNFemalD :
			return "非农女单";
			case FNShuangF :
			return "非农双非";
			case NYShuangD :
			return "农业双独";
			case NYMaleD :
			return "农业男单";
			case NYFemalD :
			return "农业女单";
			case NYShuangF :
			return "农业双非";
			case CoupleS :
			return "合计夫妇数";
			case SDCouple :
			return "双独夫妇数";
			case DDCouple :
			return "单独夫妇数";
			case FDCouple :
			return "女单夫妇数";
			case MDCouple :
			return "男单夫妇数";
			case SingleDJ :
			return "单独堆积";
			case Yi2Single :
			return "已2单独";
			case SFCouple :
			return "双非夫妇数";
			case SFDJ :
			return "双非堆积";
			case Yi2SF :
			return "已2双非";
			case SDBirth :
			return "双独生育数";
			case DDBirth :
			return "单独生育数";
			case FDBirth :
			return "女单生育数";
			case MDBirth :
			return "男单生育数";
			case SFBirth :
			return "双非生育数";
			case COverBirth :
			return "C超生";
			case XOverBirth :
			return "X超生";
			case D :
			return "D";
			case DM :
			return "DM";
			case DF :
			return "DF";
			case N :
			return "N";
			case NM :
			return "NM";
			case NF :
			return "NF";
			case Ke2Hou :
			return "可二后";
			case DDBM :
			return "DDBM";
			case DDBF :
			return "DDBF";
			case Du30 :
			return "独生子女30";
			case DuM30 :
			return "独生子30";
			case DuF30 :
			return "独生女30";
			case FeiDu30 :
			return "非独子女30";
			case FeiDuM30 :
			return "非独子30";
			case FeiDuF30 :
			return "非独女30";
			case Ke2Hou30 :
			return "可二后30";
			case Ke2Hou30M :
			return "可二后30M";
			case Ke2Hou30F :
			return "可二后30F";
			case Du_SW :
			return "独生子女SW";
			case DuM_SW :
			return "独生子SW";
			case DuF_SW :
			return "独生女SW";
			case Feidu_SW :
			return "非独子女SW";
			case FeiduM_SW :
			return "非独子SW";
			case FeiduF_SW :
			return "非独女SW";
			case Ke2Hou_SW :
			return "可二后SW";
			case Ke2Hou_SWM :
			return "可二后SWM";
			case Ke2Hou_SWF :
			return "可二后SWF";
			case YiHaiP :
			return "一孩父母";
			case YiHaiFa :
			return "一孩父";
			case YiHaiMa :
			return "一孩母";
			case TeFu :
			return "特扶";
			case TeFuM :
			return "特扶M";
			case TeFuF :
			return "特扶F";
			case WeiHunF :
			return "未婚F";
			case WeiHunM :
			return "未婚M";
			case WeiHunDF :
			return "未婚DF";
			case WeiHunDM :
			return "未婚DM";
			case WeiHunNF :
			return "未婚NF";
			case WeiHunNM :
			return "未婚NM";
			case NyWeiHunF :
			return "农业未婚F";
			case NyWeiHunM :
			return "农业未婚M";
			case FnWeiHunF :
			return "非农未婚F";
			case FnWeiHunM :
			return "非农未婚M";
			case NyWeiHunDF :
			return "农业未婚DF";
			case NyWeiHunDM :
			return "农业未婚DM";
			case NyWeiHunNF :
			return "农业未婚NF";
			case NyWeiHunNM :
			return "农业未婚NM";
			case FnWeiHunDF :
			return "非农未婚DF";
			case FnWeiHunDM :
			return "非农未婚DM";
			case FnWeiHunNF :
			return "非农未婚NF";
			case FnWeiHunNM :
			return "非农未婚NM";
			case SDGLM :
			return "双独概率M";
			case SDGLF :
			return "双独概率F";
			case MSingleGLM :
			return "男单概率M";
			case MSingleGLF :
			return "男单概率F";
			case FSingleGLM :
			return "女单概率M";
			case FSingleGLF :
			return "女单概率F";
			default:
				throw new Exception ( "error param for getChinese in Couple--"+cp );
		}
	}
	
	
	/**
	 * 20 TO 86
	 * @return
	 */
	private static Couple[] cps = {CoupleS,
		SDCouple,DDCouple,FDCouple,MDCouple,
		SingleDJ,Yi2Single,SFCouple,SFDJ,
		Yi2SF,SDBirth,DDBirth,FDBirth,
		MDBirth,SFBirth,COverBirth,XOverBirth,
		D,DM,DF,N,NM,NF,Ke2Hou,DDBM,
		DDBF,Du30,DuM30,DuF30,FeiDu30,FeiDuM30,
		FeiDuF30,Ke2Hou30,Ke2Hou30M,Ke2Hou30F,
		Du_SW,DuM_SW,DuF_SW,Feidu_SW,
		FeiduM_SW,FeiduF_SW,Ke2Hou_SW,Ke2Hou_SWM,
		Ke2Hou_SWF,YiHaiP,YiHaiFa,YiHaiMa,
		TeFu,TeFuM,TeFuF,WeiHunF,WeiHunM,WeiHunDF,
		WeiHunDM,WeiHunNF,WeiHunNM,NyWeiHunF,NyWeiHunM,FnWeiHunF,
		FnWeiHunM,NyWeiHunDF,NyWeiHunDM,NyWeiHunNF,
		NyWeiHunNM,FnWeiHunDF,FnWeiHunDM,FnWeiHunNF};
	public static Couple[] getCopyCouple(){
		return cps;
	}
	
	
	private static Couple[] cps2 = {CoupleS,
		SDCouple,DDCouple,FDCouple,MDCouple,
		SingleDJ,Yi2Single,SFCouple,SFDJ,
		Yi2SF,SDBirth,DDBirth,FDBirth,
		MDBirth,SFBirth,COverBirth,XOverBirth,
		D,DM,DF,N,NM,NF,Ke2Hou,DDBM,
		DDBF,Du30,DuM30,DuF30,FeiDu30,FeiDuM30,
		FeiDuF30,Ke2Hou30,Ke2Hou30M,Ke2Hou30F,
		Du_SW,DuM_SW,DuF_SW,Feidu_SW,
		FeiduM_SW,FeiduF_SW,Ke2Hou_SW,Ke2Hou_SWM,
		Ke2Hou_SWF,YiHaiP,YiHaiFa,YiHaiMa,
		TeFu,TeFuM,TeFuF,WeiHunF,WeiHunM,WeiHunDF,
		WeiHunDM,WeiHunNF,WeiHunNM,NyWeiHunF,NyWeiHunM,FnWeiHunF,
		FnWeiHunM,NyWeiHunDF,NyWeiHunDM,NyWeiHunNF,
		NyWeiHunNM,FnWeiHunDF,FnWeiHunDM,FnWeiHunNF
		,FnWeiHunNM};
	/**
	 * 20 TO 87
	 * @return
	 */
	public static Couple[] getCopyCouple2(){
		return cps2;
	}
}
