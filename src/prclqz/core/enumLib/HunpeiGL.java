package prclqz.core.enumLib;
/**
 * 婚配概率--表结构映射
 * @author prclqz@zju.edu.cn
 *
 */
public enum HunpeiGL
{
/*
 *`  `地区` ,  `X农双独GLM` ,  `X农双独GLF` ,
  `X非双独GLM` ,  `X非双独GLF` ,  `X农男单GLM` ,  `X农男单GLF` ,
  `X农女单GLM` ,  `X农女单GLF` ,  `X非男单GLM` ,  `X非男单GLF` ,
  `X非女单GLM` ,  `X非女单GLF` ,  
  
  `C农双独GLM` ,  `C农双独GLF` ,
  `C非双独GLM` ,  `C非双独GLF` ,  `C农男单GLM` ,  `C农男单GLF` ,
  `C农女单GLM` ,  `C农女单GLF` ,  `C非男单GLM` ,  `C非男单GLF` ,
  `C非女单GLM` ,  `C非女单GLF` ,
   */
//	Year,
	Dq,XNongSDGLM,XNongSDGLF,
	XFeiSDGLM,XFeiSDGLF,XNongMDGLM,XNongMDGLF,
	XNongFDGLM,XNongFDGLF,XFeiMDGLM,XFeiMDGLF,
	XFeiFDGLM,XFeiFDGLF,
	
	CNongSDGLM,CNongSDGLF,
	CFeiSDGLM,CFeiSDGLF,CNongMDGLM,CNongMDGLF,
	CNongFDGLM,CNongFDGLF,CFeiMDGLM,CFeiMDGLF,
	CFeiFDGLM,CFeiFDGLF,
}
