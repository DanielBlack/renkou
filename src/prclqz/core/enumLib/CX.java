package prclqz.core.enumLib;

/**
 * 基本循环类型，慎重添加或删除元素，因为会影响到values()操作
 * @author prclqz@zju.edu.cn
 *
 */
public enum CX{ 
	Chengshi {
		@Override
		public int getEditableNum() {
			return 0;
		}

		@Override
		public int getId(String type) {
			//TODO 是否需要根据数据库来修改？
			if(type.equals("死亡概率预测"))
				return 72;
			else //仿真结果level2
				return 120;
		}

		@Override
		public String getChinese() {
			return "城镇";
		}
	},Nongchun {
		@Override
		public int getEditableNum() {
			return 1;
		}

		@Override
		public int getId(String type) {
			if(type.equals("死亡概率预测"))
				return 73;
			else
				return 123;
		}

		@Override
		public String getChinese() {
			return "农村";
		}
	};
	abstract public int getEditableNum();
	//返回对应的主_参数对应表的id
	abstract public int getId(String type);
	abstract public String getChinese();
	public static CX getCXFromId(int id){
		switch(id)
		{
		case 72:
		case 120:
		case 165:
		case 166:
			return Chengshi;
		case 73:
		case 123:
		case 163:
		case 164:
			return Nongchun;
		default:
			return null;
		}
	}
	
	}