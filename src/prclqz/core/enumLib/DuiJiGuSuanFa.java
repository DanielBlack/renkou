package prclqz.core.enumLib;
/**
 * 堆积估算法
 * @author prclqz@zju.edu.cn
 *
 */
public enum DuiJiGuSuanFa {
//堆积估算法=IIF(TJ=1,'生育模式堆积估计法',IIF(TJ=2,'平均存活堆积估计法','存活一孩堆积估计法'))
	shengyuMoshiGusuanFa,
	pingjunCunhuoGujiFa,
	cunhuoYihaiGujiFa;
	public static int getParamId(DuiJiGuSuanFa df){
		switch(df){
		case shengyuMoshiGusuanFa:
			return 76;
		case pingjunCunhuoGujiFa:
			return 75;
		case cunhuoYihaiGujiFa:
			return 74;
		default:
			return 0;
		}
	}
	public static DuiJiGuSuanFa getFromId(int id){
		switch(id)
		{
		case 76:return DuiJiGuSuanFa.shengyuMoshiGusuanFa;
		case 75:return DuiJiGuSuanFa.pingjunCunhuoGujiFa;
		case 74:return DuiJiGuSuanFa.cunhuoYihaiGujiFa;
		default:return DuiJiGuSuanFa.cunhuoYihaiGujiFa;
		}
	}
}
