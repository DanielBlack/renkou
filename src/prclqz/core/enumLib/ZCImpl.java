package prclqz.core.enumLib;

/**
 * 实现程度估计 类型对应
 * @author prclqz@zju.edu.cn
 * 
 */
public enum ZCImpl {
	// 政策生育 ByPolicy，回归估计 ByEstimation，意愿生育 ByWill
	ByPolicy,ByEstimation,ByWill;
	public static ZCImpl getZCImplFromId(int id){
		switch(id){
		case 146:
		case 1:
			return ByPolicy;
		case 144:
		case 2:
			return ByEstimation;
		case 145:
		case 3:
			return ByWill;
		default:
			return null;
		}
	}
}
