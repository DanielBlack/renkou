package prclqz.core.enumLib;

/**
 * 分年龄生育预测表结构映射
 * 根据年返回对应的Babies变量
 * @author prclqz@zju.edu.cn
 *
 */
public enum Babies {
	B2005,
	B2006,
	B2007,
	B2008,
	B2009,
	B2010,
	B2011,
	B2012,
	B2013,
	B2014,
	B2015,
	B2016,
	B2017,
	B2018,
	B2019,
	B2020,
	B2021,
	B2022,
	B2023,
	B2024,
	B2025,
	B2026,
	B2027,
	B2028,
	B2029,
	B2030,
	B2031,
	B2032,
	B2033,
	B2034,
	B2035,
	B2036,
	B2037,
	B2038,
	B2039,
	B2040,
	B2041,
	B2042,
	B2043,
	B2044,
	B2045,
	B2046,
	B2047,
	B2048,
	B2049,
	B2050,
	B2051,
	B2052,
	B2053,
	B2054,
	B2055,
	B2056,
	B2057,
	B2058,
	B2059,
	B2060,
	B2061,
	B2062,
	B2063,
	B2064,
	B2065,
	B2066,
	B2067,
	B2068,
	B2069,
	B2070,
	B2071,
	B2072,
	B2073,
	B2074,
	B2075,
	B2076,
	B2077,
	B2078,
	B2079,
	B2080,
	B2081,
	B2082,
	B2083,
	B2084,
	B2085,
	B2086,
	B2087,
	B2088,
	B2089,
	B2090,
	B2091,
	B2092,
	B2093,
	B2094,
	B2095,
	B2096,
	B2097,
	B2098,
	B2099,
	B2100;
	public static Babies getBabies(int year) {
		switch (year) {
		case 2005:
			return B2005;
		case 2006:
			return B2006;
		case 2007:
			return B2007;
		case 2008:
			return B2008;
		case 2009:
			return B2009;
		case 2010:
			return B2010;
		case 2011:
			return B2011;
		case 2012:
			return B2012;
		case 2013:
			return B2013;
		case 2014:
			return B2014;
		case 2015:
			return B2015;
		case 2016:
			return B2016;
		case 2017:
			return B2017;
		case 2018:
			return B2018;
		case 2019:
			return B2019;
		case 2020:
			return B2020;
		case 2021:
			return B2021;
		case 2022:
			return B2022;
		case 2023:
			return B2023;
		case 2024:
			return B2024;
		case 2025:
			return B2025;
		case 2026:
			return B2026;
		case 2027:
			return B2027;
		case 2028:
			return B2028;
		case 2029:
			return B2029;
		case 2030:
			return B2030;
		case 2031:
			return B2031;
		case 2032:
			return B2032;
		case 2033:
			return B2033;
		case 2034:
			return B2034;
		case 2035:
			return B2035;
		case 2036:
			return B2036;
		case 2037:
			return B2037;
		case 2038:
			return B2038;
		case 2039:
			return B2039;
		case 2040:
			return B2040;
		case 2041:
			return B2041;
		case 2042:
			return B2042;
		case 2043:
			return B2043;
		case 2044:
			return B2044;
		case 2045:
			return B2045;
		case 2046:
			return B2046;
		case 2047:
			return B2047;
		case 2048:
			return B2048;
		case 2049:
			return B2049;
		case 2050:
			return B2050;
		case 2051:
			return B2051;
		case 2052:
			return B2052;
		case 2053:
			return B2053;
		case 2054:
			return B2054;
		case 2055:
			return B2055;
		case 2056:
			return B2056;
		case 2057:
			return B2057;
		case 2058:
			return B2058;
		case 2059:
			return B2059;
		case 2060:
			return B2060;
		case 2061:
			return B2061;
		case 2062:
			return B2062;
		case 2063:
			return B2063;
		case 2064:
			return B2064;
		case 2065:
			return B2065;
		case 2066:
			return B2066;
		case 2067:
			return B2067;
		case 2068:
			return B2068;
		case 2069:
			return B2069;
		case 2070:
			return B2070;
		case 2071:
			return B2071;
		case 2072:
			return B2072;
		case 2073:
			return B2073;
		case 2074:
			return B2074;
		case 2075:
			return B2075;
		case 2076:
			return B2076;
		case 2077:
			return B2077;
		case 2078:
			return B2078;
		case 2079:
			return B2079;
		case 2080:
			return B2080;
		case 2081:
			return B2081;
		case 2082:
			return B2082;
		case 2083:
			return B2083;
		case 2084:
			return B2084;
		case 2085:
			return B2085;
		case 2086:
			return B2086;
		case 2087:
			return B2087;
		case 2088:
			return B2088;
		case 2089:
			return B2089;
		case 2090:
			return B2090;
		case 2091:
			return B2091;
		case 2092:
			return B2092;
		case 2093:
			return B2093;
		case 2094:
			return B2094;
		case 2095:
			return B2095;
		case 2096:
			return B2096;
		case 2097:
			return B2097;
		case 2098:
			return B2098;
		case 2099:
			return B2099;
		case 2100:
			return B2100;
		default:
			return null;
		}
	}
}
