package prclqz.view;

import java.io.IOException;

public interface IView {
	public void outputSimple(String tag, String msg);
	public void setTaskName(String name);
	public void outputI(String tag, String msg, String taskName) throws Exception;
	public void outputI(String tag, String msg)throws Exception;
	public void outputI(int tagType,int msgType)throws Exception;
	public void outputI(int tagType,int msgType,String msg2)throws Exception;
	public void outputI(int tagType,String msg)throws Exception;
	public void close() throws Exception;
	public void writeToTmpFile(String str) throws IOException;
	void writelnToTmpFile(String str) throws IOException;
	void saveTmpFile() throws IOException;

}
