package prclqz.view;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.*;

public class DefaultView implements IView {

	private String taskName;
	private static IDAO m;
	private static ArrayList<String> tagArr;
	private static ArrayList<String> msgArr;
	private static FileWriter fw ;
	private static String tmpFilePath = "D:\\分省生育政策仿真1102\\新人口学\\测试结果文件\\";
	
	static{
		tagArr = new ArrayList<String>();
		msgArr = new ArrayList<String>();
		
		tagArr.add(0,"启动任务");
		tagArr.add(1,"计算中");
		
		msgArr.add(0,"未输入任务名");
		msgArr.add(1,"没有该任务名");
		msgArr.add(2,"开始任务");
		msgArr.add(3,"找不到方法");
		msgArr.add(4,"方法的参数类型和参数格式不一致");
		msgArr.add(5,"");
		//msgArr.add(,"");
	}
	
	public static void setTmpFilePath(String path){
		tmpFilePath = path;
	}
	
	public DefaultView() {
		if(m == null){
			m = new MyDAOImpl();
		}
	}

	@Override
	public void outputSimple(String tag, String msg) {
		System.out.println(tag+":"+msg);
	}
	
	/***
	 * @param tag 任务进行阶段
	 * @param msg 信息
	 * @param taskName 所属任务名
	 */
	public void outputI(String tag, String msg, String taskName) throws Exception{
		this.taskName = taskName;
		outputI(tag,msg);
	}

	/***
	 * @param tag 任务进行阶段
	 * @param msg 信息
	 */
	public void outputI(String tag, String msg) throws Exception{		
		long time = System.currentTimeMillis()/1000;
		MainOperationHistoryBean mob = new MainOperationHistoryBean(0, 3, time, tag+":"+msg+":"+taskName);
		m.addHistory(mob);
		outputSimple(tag, msg);
	}

	@Override
	public void close() throws Exception {
		this.saveTmpFile();
		m.close();
		
	}

	@Override
	public void outputI(int tagType, int msgType) throws Exception {
		// TODO Auto-generated method stub
		String tag = tagArr.get(tagType);
		String msg = msgArr.get(msgType);
		outputI(tag,msg);
	}

	@Override
	public void setTaskName(String name) {
		// TODO Auto-generated method stub
		taskName = name;
	}

	@Override
	public void outputI(int tagType, String msg)throws Exception {
		// TODO Auto-generated method stub
		String tag = tagArr.get(tagType);
		outputI(tag,msg);
	}

	@Override
	public void outputI(int tagType, int msgType, String msg2) throws Exception {
		// TODO Auto-generated method stub
		String tag = tagArr.get(tagType);
		String msg = msgArr.get(msgType)+"-"+msg2;
		outputI(tag,msg);
	}

	@Override
	public void writeToTmpFile(String str) throws IOException {
		if(fw == null){
			long time = System.currentTimeMillis()/1000;
			int time2 = (int) time;
			Integer in = new Integer(time2);
			fw = new FileWriter(tmpFilePath+"javatmp-"+in.toString()+".txt");
		}
		
		fw.write(str);
	}
	
	@Override
	public void writelnToTmpFile(String str) throws IOException {
		if(fw == null){
			long time = System.currentTimeMillis()/1000;
			int time2 = (int) time;
			Integer in = new Integer(time2);
			fw = new FileWriter(tmpFilePath+"javatmp-"+in.toString()+".txt");
		}
		fw.write(str+"\r\n");
	}
	@Override
	public void saveTmpFile() throws IOException{
		if(fw!=null)
			fw.close();
		fw = null;
	}
	
	public static void main(String[] args) throws Exception {
		IView v = new DefaultView();
		v.writelnToTmpFile("ahaha");
		v.writelnToTmpFile("xxidddd");
		v.saveTmpFile();
	}

}
