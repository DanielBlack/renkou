package prclqz.methods;

import java.util.HashMap;
import java.util.Map;
import static prclqz.core.Const.*;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.HunpeiField.*;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.ZCImpl;
import static prclqz.core.enumLib.ZCImpl.*;
import static prclqz.core.enumLib.SYSFMSField.*;
import prclqz.view.IView;
/**
 * 不同政策实现程度下的堆积生育释放法
 * @author prclqz@zju.edu.cn
 *
 */
public class MBabiesBornByDuiji implements IBabiesBornByDuijiMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {

		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		IView v = (IView) globals.get("view");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		//define local variables
		int dqx = tempVar.getProvince();
		//地区 子女婚配预测表
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//provincMigMap.get(""+dqx);
		Map<CX,Map<SYSFMSField,double[]>> shengYuMoShi = (Map<CX, Map<SYSFMSField, double[]>>) predictVarMap.get("ShengYuMoShi"+dqx);
		SYSFMSField sfms = SYSFMSField.getSFMSfromId(tempVar.getSFMS());
		ZCImpl zcImpl = ZCImpl.getZCImplFromId(tempVar.getZCimplement());
		int X;
		
		double[] NYTuShiAR,NYHuanShiAR,NYShiFangAR,FNTuShiAR,FNHuanShiAR,FNShiFangAR;
		NYTuShiAR = (double[]) predictVarMap.get("NYTuShiAR");
		NYHuanShiAR = (double[]) predictVarMap.get("NYHuanShiAR");
		NYShiFangAR = (double[]) predictVarMap.get("NYShiFangAR");
		FNTuShiAR = (double[]) predictVarMap.get("FNTuShiAR");
		FNHuanShiAR = (double[]) predictVarMap.get("FNHuanShiAR");
		FNShiFangAR = (double[]) predictVarMap.get("FNShiFangAR");
		
		BornXbbBean xbb = xbbMap.get(""+dqx);
		String diDai = xbb.getDiDai();
		diDai = (diDai.equals( "西部") )? "四川":diDai;
		double implRate = ( ZCImpl.getZCImplFromId(tempVar.getZCimplement()) == ByEstimation )? tempVar.getImplRate() : 1 ;
		int nNYHuanShi,nNYTuShi,nFNHuanShi,nFNTuShi;
		double[] HuanShiAR,TuShiAR;
		int nHuanShi,nTuShi;
		Map<String,Map<SYSFMSField,double[]>> birthWill = (Map<String, Map<SYSFMSField, double[]>>) predictVarMap.get("BirthWill");
		Map<SYSFMSField,double[]> pBirthWill = birthWill.get(diDai);
		int year = tempVar.getYear();
		BabiesBornBean bBornBean = tempVar.getBabiesBorn();
		double sglRlsB2 = 0 , feiRlsB2 = 0 ,singleB2; 
		Map<HunpeiField, double[]> zn = ziNv.get(tempVar.getCx()).get(tempVar.getNy());
		
		
		//////////////正式计算//////////////////////////////////
		for(CX cx : CX.values())
			for(NY ny : NY.values()){
				Map<HunpeiField, double[]> zn2 = ziNv.get(cx).get(ny);
				for(X = 0; X<MAX_AGE; X++){
					zn2.get(duijiRate)[X] = shengYuMoShi.get(cx).get(wait2chdrn)[X];
				}
			}// CX-NY-loop-end
		
		nNYHuanShi = nNYTuShi = nFNHuanShi = nFNTuShi = 0;
		for(X = 0; X<MAX_AGE; X++){
			if(shengYuMoShi.get(Nongchun).get(huanshi)[X] != 0)
			{
				NYHuanShiAR[++nNYHuanShi] = shengYuMoShi.get(Nongchun).get(huanshi)[X];
//				nNYHuanShi++;
			}
			if(shengYuMoShi.get(Nongchun).get(tushi)[X] != 0)
			{
				NYTuShiAR[++nNYTuShi] = shengYuMoShi.get(Nongchun).get(tushi)[X];
//				nNYTuShi++;
			}
			if(shengYuMoShi.get(Chengshi).get(huanshi)[X] != 0)
			{
				FNHuanShiAR[++nFNHuanShi] = shengYuMoShi.get(Chengshi).get(huanshi)[X];
//				nFNHuanShi++;
			}
			if(shengYuMoShi.get(Chengshi).get(tushi)[X] != 0)
			{
				FNHuanShiAR[++nFNTuShi] = shengYuMoShi.get(Chengshi).get(tushi)[X];
//				nFNTuShi++;
			}
		}
		//选择
		HuanShiAR = (tempVar.getCx() == Nongchun ) ? NYHuanShiAR : FNHuanShiAR;
		TuShiAR   = (tempVar.getCx() == Nongchun ) ? NYTuShiAR : FNTuShiAR;
		nHuanShi  = (tempVar.getCx() == Nongchun ) ? nNYHuanShi : nFNHuanShi;
		nTuShi	  = (tempVar.getCx() == Nongchun ) ? nNYTuShi : nFNTuShi;
		
		
		//计算
//		for(X = 17; X <= 67; X++){
		for(X = MAX_AGE-1; X >= 0 ; X--){
			switch(zcImpl){
			case ByPolicy:
				switch(sfms){
				case yiyuan:
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* pBirthWill.get(huanshi)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ)[X]
								* pBirthWill.get(tushi)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ)[X]
								* pBirthWill.get(fenshi)[X] * zn.get(FR2)[X];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(huanshi)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(tushi)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(fenshi)[X] * zn.get(FR2)[X];
					}
					if (true) {
						zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X]
								+ zn.get(ShuangFeiRls)[X];
						zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X]
								+ zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
								+ zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////
					
					break;
				case fenshi:
					
//					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ30)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ30)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1];
//					}
//					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
//						zn.get(SingleRls)[X] += zn.get(SingleDJ35)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ35)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1];
//					}
//					zn.get(SingleRls)[X] += zn.get(SingleDJ15)[X] * zn.get(FR2)[X];
//					zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ15)[X] * zn.get(FR2)[X];
//					
//					zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
//					zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
//					                   + zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
//					
//					break;
					
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ30)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ35)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ15)[X] * zn.get(FR2)[X];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ30)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ35)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ15)[X] * zn.get(FR2)[X];
					}
					if (true) {
						zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X]
								+ zn.get(ShuangFeiRls)[X];
						zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X]
								+ zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
								+ zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				case huanshi:
//					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]* HuanShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1];
//					}
//					zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
//					zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
//					                   + zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X]
								+ zn.get(ShuangFeiRls)[X];
						zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X]
								+ zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
								+ zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				case tushi:
					
//					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]* TuShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1];
//					}
//					zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
//					zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
//					                   + zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (true) {
						zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X]
								+ zn.get(ShuangFeiRls)[X];
						zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X]
								+ zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
								+ zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////
					
					break;
				case zhengchang:
//					
//					if(X<=49){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]*zn.get(FR2)[X]* implRate;
//   	                    zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]*zn.get(FR2)[X]* implRate;
//					}
//					zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
//					zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
//					                   + zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (X <= 49.0) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* zn.get(FR2)[X] * tempVar.implRate;
					}
					if (X <= 49.0) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* zn.get(FR2)[X] * tempVar.implRate;
					}
					if (true) {
						zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X]
								+ zn.get(ShuangFeiRls)[X];
						zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X]
								+ zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X]
								+ zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				default:
					break;
				}
				break;
				
				
			case ByEstimation:
				switch(sfms){
				case yiyuan:
					//////////////translated by Foxpro2Java Translator successfully:///////////////TODO
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* pBirthWill.get(huanshi)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ)[X]
								* pBirthWill.get(tushi)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (X <= 49.0) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ)[X]
								* pBirthWill.get(fenshi)[X] * zn.get(FR2)[X]
								* tempVar.implRate;
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(huanshi)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(tushi)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (X <= 49.0) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ)[X]
								* pBirthWill.get(fenshi)[X] * zn.get(FR2)[X]
								* tempVar.implRate;
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////
					break;
				case fenshi:
//					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ30)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ30)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1];
//					}
//					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
//						zn.get(SingleRls)[X] += zn.get(SingleDJ35)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1];
//						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ35)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1];
//						
//					}
//					if(X<=49){
//						zn.get(SingleRls)[X] += zn.get(SingleDJ15)[X] * zn.get(FR2)[X] * implRate;
//						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ)[X] * zn.get(FR2)[X] * implRate;
//					}
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ30)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ35)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (X <= 49.0) {
						zn.get(SingleRls)[X] = zn.get(SingleRls)[X]
								+ zn.get(SingleDJ15)[X] * zn.get(FR2)[X]
								* tempVar.implRate;
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ30)[X]
								* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ35)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if (X <= 49.0) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangFeiRls)[X]
								+ zn.get(ShuangfeiDJ15)[X] * zn.get(FR2)[X]
								* tempVar.implRate;
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				case huanshi:
//					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1] * implRate;
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * HuanShiAR[year - tempVar.getPolicyTime() + 1]*implRate;
//					}
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					for (X = MAX_AGE - 1; X >= 0; X--) {
						if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
							zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
									* HuanShiAR[year - tempVar.getPolicyTime()
											+ 1] * tempVar.implRate;
						}
						if (year - tempVar.getPolicyTime() + 1.0 <= nHuanShi) {
							zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
									* HuanShiAR[year - tempVar.getPolicyTime()
											+ 1] * tempVar.implRate;
						}
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				case tushi:
//					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1] * implRate;
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * TuShiAR[year - tempVar.getPolicyTime() + 1]*implRate;
//					}
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1]
								* tempVar.implRate;
					}
					if (year - tempVar.getPolicyTime() + 1.0 <= nTuShi) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* TuShiAR[year - tempVar.getPolicyTime() + 1]
								* tempVar.implRate;
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				case zhengchang:
//					if( X<=49 ){
//						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * zn.get(FR2)[X] * implRate;
//						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * zn.get(FR2)[X] * implRate;
//					}
					//////////////translated by Foxpro2Java Translator successfully:///////////////
					if (X <= 49.0) {
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X]
								* zn.get(FR2)[X] * tempVar.implRate;
					}
					if (X <= 49.0) {
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X]
								* zn.get(FR2)[X] * tempVar.implRate;
					}
					/////////////end of translating, by Foxpro2Java Translator///////////////

					break;
				default:
					break;
				}
				break;
			case ByWill:
				//TODO 暂不用运行，也未测试！！！
				switch(sfms){
				case yiyuan:
					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X]* pBirthWill.get(tushi)[X]  
											 *	TuShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* pBirthWill.get(tushi)[X]  
						                        *	TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
						zn.get(SingleRls)[X] += zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X]* pBirthWill.get(huanshi)[X]  
						                      *	HuanShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* pBirthWill.get(huanshi)[X]  
						                        *	HuanShiAR[year - tempVar.getPolicyTime() + 1];
						
					}
					if(X<=49){
						zn.get(SingleRls)[X] += zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X] * pBirthWill.get(fenshi)[X]* zn.get(FR2)[X];
						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* pBirthWill.get(fenshi)[X] * zn.get(FR2)[X];
					}
					break;
				case fenshi:
					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
						zn.get(SingleRls)[X] = zn.get(SingleDJ30)[X] * pBirthWill.get(yiyuan)[X]  
											 *	HuanShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ30)[X] * pBirthWill.get(yiyuan)[X]  
						                        *	HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
						zn.get(SingleRls)[X] += zn.get(SingleDJ35)[X] * pBirthWill.get(yiyuan)[X]  
						                      *	TuShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ35)[X] * pBirthWill.get(yiyuan)[X]  
						                        *	TuShiAR[year - tempVar.getPolicyTime() + 1];
						
					}
					if(X<=49){
						zn.get(SingleRls)[X] += zn.get(SingleDJ15)[X] * pBirthWill.get(yiyuan)[X]* zn.get(FR2)[X];
						zn.get(ShuangFeiRls)[X] += zn.get(ShuangfeiDJ15)[X] * pBirthWill.get(yiyuan)[X]* zn.get(FR2)[X];
					}
					break;
				case huanshi:
					if( year - tempVar.getPolicyTime() + 1 <= nHuanShi){
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X]* HuanShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* HuanShiAR[year - tempVar.getPolicyTime() + 1];
					}
					break;
				case tushi:
					if( year - tempVar.getPolicyTime() + 1 <= nTuShi){
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X]* TuShiAR[year - tempVar.getPolicyTime() + 1];
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* TuShiAR[year - tempVar.getPolicyTime() + 1];
					}
					break;
				case zhengchang:
					if( X<=49 ){
						zn.get(SingleRls)[X] = zn.get(SingleDJ)[X] * pBirthWill.get(yiyuan)[X]* zn.get(FR2)[X];
						zn.get(ShuangFeiRls)[X] = zn.get(ShuangfeiDJ)[X] * pBirthWill.get(yiyuan)[X]* zn.get(FR2)[X];
					}
					break;
				default:
					break;
				}// end -switch - sfms
				 break;
			default:
				break;
			}// end -switch - impl
			
			zn.get(OverB_DDB)[X] = zn.get(DDB)[X] - zn.get(PlyDDB)[X];
			zn.get(OverB_NNB)[X] = zn.get(NNB)[X] - zn.get(PlyNNB)[X];
			zn.get(OverB_NMDFB)[X] = zn.get(NMDF)[X] - zn.get(PlyNMDFB)[X];
			zn.get(OverB_NFDMB)[X] = zn.get(NFDM)[X] - zn.get(PlyNFDMB)[X];
			
			zn.get(OverB_RlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X] - zn.get(PlyRlsB)[X];
			zn.get(Over_Birth)[X] = zn.get(OverB_DDB)[X] + zn.get(OverB_NNB)[X] + zn.get(OverB_NMDFB)[X] + zn.get(OverB_NFDMB)[X] + zn.get(OverB_RlsB)[X];

			zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
			zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNNB)[X] + zn.get(PlyNMDFB)[X] + zn.get(PlyNFDMB)[X] + zn.get(PlyRlsB)[X];

			sglRlsB2 += zn.get(SingleRls)[X];
			feiRlsB2 += zn.get(ShuangFeiRls)[X];
		} // end - loop -X
		
		bBornBean.setSglRlsB2(sglRlsB2);
		bBornBean.setFeiRlsB2(feiRlsB2);
		singleB2 = sglRlsB2 + bBornBean.getNMDF2() + bBornBean.getNFDM2();
		bBornBean.setSingleB2(singleB2);
		bBornBean.setNN2(bBornBean.getNN2()+feiRlsB2);
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
