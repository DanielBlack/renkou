package prclqz.methods;
import static prclqz.lib.EnumMapTool.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.XB.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.Const.*;

import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;
import prclqz.parambeans.ParamBean3;
import prclqz.view.DefaultView;
import prclqz.view.IView;
/**
 * 年龄移算
 * 新建：地区城镇(农村)子女婚配预测表——仿真结果_婚配预测表结构
 * 新建：地区农业(非农)子女婚配预测表——仿真结果_婚配预测表结构
 * 新建：地区子女婚配预测表——仿真结果_婚配预测表结构
 * @author prclqz@zju.edu.cn
 *
 */
public class MAgingCalculation implements IMethod {

	public static void main(String[] args) throws Exception{
		HashMap<String, Object> globals = new HashMap< String , Object >();
		IDAO m = new MyDAOImpl();
		IView v = new DefaultView();
		globals.put("view", v);
		
		MainTaskBean task;
		task = m.getTask(args[0]);
		v.setTaskName(task.getName());
		
		HashMap<String, BornXbbBean> xbbMap = m.getARBornXbbBeans(task);
		globals.put("bornXbbBeanMap", xbbMap);
		
		TempVariablesBean tempVar = new TempVariablesBean( 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 );
		globals.put( "tempVarBean" , tempVar );
		
		HashMap<String,Object> predictVarMap = new HashMap< String , Object >();
		globals.put( "predictVarMap" , predictVarMap );
		
		
		
		v.writelnToTmpFile("ahaha");
		v.writelnToTmpFile("xxidddd");
		v.saveTmpFile();
		////////////setup///////////////////
		tempVar.province = 11;
		tempVar.year = 2010;
		
		ParamBean3 pb3 = new ParamBean3();
		pb3.setDq1( 11 );
		pb3.setDq2( 65 );
		int dqx;
		//生育及释放模式
		Map<CX,Map<SYSFMSField,double[]>> shengYuMoShi;
		//死亡概率预测
		Map<CX,Map<XB,double[]>> deathRate;
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			//生育及释放模式
			shengYuMoShi = createSYSFMSMap(MAX_AGE);
			m.setShengYuMoShi(shengYuMoShi,dqx,task.getParam_id(),tempVar.getTJ());
			predictVarMap.put("ShengYuMoShi"+dqx, shengYuMoShi);
			
			//死亡概率
			deathRate = createCXXBDoubleArrMap(MAX_AGE);
			String fieldName = tempVar.getYear()<2101? "Q"+tempVar.getYear():"Q2100";
			m.setDeathRate(deathRate,dqx,task.getParam_id(),fieldName);
			predictVarMap.put("DeathRate"+dqx, m);
			
			//婚配预测表
			Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = creatCXNYZiNvFromFile( MAX_AGE );
			
		}
	}
	
	@SuppressWarnings ( "unchecked" )
	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		
		//1.普通年龄移算
		Map<CX,Map<XB,double[]>> deathRate = (Map<CX, Map<XB, double[]>>) predictVarMap.get("DeathRate"+dqx);
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );
		Map<NY,Map<HunpeiField,double[]>> ziNvOfNY =  (Map< NY , Map< HunpeiField , double[] >>) predictVarMap.get( "HunpeiOfNY"+dqx );//creatNYFNZiNv(MAX_AGE);
		Map<CX,Map<HunpeiField,double[]>> ziNvOfCX = (Map< CX , Map< HunpeiField , double[] >>) predictVarMap.get( "HunpeiOfCX"+dqx ); //creatCXZiNv(MAX_AGE);
		Map<HunpeiField, double[]> ziNvOfAll =  (Map< HunpeiField , double[] >) predictVarMap.get( "HunpeiOfAll"+dqx );//creatAllZiNv( MAX_AGE );
		//迁移人口统计
		Map<CX,Map<Year,Map<XB,double[]>>>populationMigrationfCX = (Map<CX,Map<Year,Map<XB,double[]>>>) predictVarMap.get("PopulationMigrationOfCX"+dqx);
		Map<Year,Map<XB,double[]>> populationMigrationOfAll = (Map<Year,Map<XB,double[]>>) predictVarMap.get("PopulationMigrationOfAll"+dqx);
		//分年龄丧子人口预测
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> sonDiePopulationPredictOfYiHai = (Map<CX, Map<NY, Map<Year, Map<XB, double[]>>>>) predictVarMap.get("SonDiePopulationPredictOfYiHaiCXNY"+dqx);//一孩父母
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> sonDiePopulationPredictOfTeFu  = (Map<CX, Map<NY, Map<Year, Map<XB, double[]>>>>) predictVarMap.get("SonDiePopulationPredictOfTeFuCXNY"+dqx); //特扶父母
		
		
		
		int X;
		for(HunpeiField hpf : HunpeiField.valuesOfZinv()){//{HJM,HJF,DM,DF,DDBM,DDBF,NM,NF}
			for ( X = MAX_AGE-1 ; X > 0; X--){// 倒着计算从 110 到 1岁，0岁要置0
				// 农村农业
				//System.out.println(hpf);
//				if(ziNv==null)
//					System.out.println("heheh");
				ziNv.get(Nongchun).get(Nongye).get(hpf)[X] = ziNv.get(Nongchun).get(Nongye).get(hpf)[X-1] * 
						( 1 - deathRate.get(Nongchun).get(HunpeiField.getXB(hpf))[X-1] );
				//农村非农
				ziNv.get(Nongchun).get(Feinong).get(hpf)[X] = ziNv.get(Nongchun).get(Feinong).get(hpf)[X-1] * 
						( 1 - deathRate.get(Chengshi).get(HunpeiField.getXB(hpf))[X-1] );
				//城镇农业
				ziNv.get(Chengshi).get(Nongye).get(hpf)[X] = ziNv.get(Chengshi).get(Nongye).get(hpf)[X-1] * 
						( 1 - deathRate.get(Nongchun).get(HunpeiField.getXB(hpf))[X-1] );
				//城镇非农
				ziNv.get(Chengshi).get(Feinong).get(hpf)[X] = ziNv.get(Chengshi).get(Feinong).get(hpf)[X-1] * 
						( 1 - deathRate.get(Chengshi).get(HunpeiField.getXB(hpf))[X-1] );
			}
			//0岁置0
			ziNv.get(Nongchun).get(Nongye).get(hpf)[0] = 0;
			ziNv.get(Nongchun).get(Feinong).get(hpf)[0]= 0;
			ziNv.get(Chengshi).get(Nongye).get(hpf)[0] = 0;
			ziNv.get(Chengshi).get(Feinong).get(hpf)[0]= 0;
			
			//合计
			ziNvOfNY.get(Nongye).get(hpf)[X] = ziNv.get(Nongchun).get(Nongye).get(hpf)[X] + 
						ziNv.get(Chengshi).get(Nongye).get(hpf)[X]; 
			ziNvOfNY.get(Feinong).get(hpf)[X] = ziNv.get(Nongchun).get(Feinong).get(hpf)[X] + 
						ziNv.get(Chengshi).get(Feinong).get(hpf)[X]; 
			ziNvOfAll.get(hpf)[X] = ziNvOfNY.get(Nongye).get(hpf)[X] + ziNvOfNY.get(Feinong).get(hpf)[X];
			
		}
		
		//2. 堆积生育特殊人群的年龄移算
		for(CX cx: CX.values()){
			for( X = MAX_AGE - 1 ; X >0 ; X-- ){
				//农业堆积丈夫
				ziNv.get(cx).get(Nongye).get(duijiHusband)[X] = ziNv.get(cx).get(Nongye).get(duijiHusband)[X-1] *
				              ( 1 - deathRate.get(Nongchun).get(Male)[X-1] );                                    
				//非农堆积丈夫
				ziNv.get(cx).get(Feinong).get(duijiHusband)[X] = ziNv.get(cx).get(Feinong).get(duijiHusband)[X-1] *
							  ( 1 - deathRate.get(Chengshi).get(Male)[X-1] );                                    
				
			}
			//单独/双非年龄移算
			for( HunpeiField hpf : HunpeiField.valuesOfDJ() ){
				for( X = MAX_AGE - 1 ; X > 0 ; X --){
					ziNv.get(cx).get(Nongye).get(hpf)[X] = ziNv.get(cx).get(Nongye).get(hpf)[X-1] *
							( 1 - deathRate.get(Nongchun).get(Female)[X-1] ); 
					ziNv.get(cx).get(Feinong).get(hpf)[X] = ziNv.get(cx).get(Feinong).get(hpf)[X-1] *
					( 1 - deathRate.get(Chengshi).get(Female)[X-1] ); 
				}
			}
			//合计
			for( X = MAX_AGE - 1 ; X > 0 ; X-- ){
				//农业
				ziNv.get(cx).get(Nongye).get(SingleDJ)[X] = ziNv.get(cx).get(Nongye).get(FSingleDJ)[X] +
							ziNv.get(cx).get(Nongye).get(MSingleDJ)[X];
				ziNv.get(cx).get(Nongye).get(Yi2FSingle)[X] = ziNv.get(cx).get(Nongye).get(Yi2MSingle)[X] +
						ziNv.get(cx).get(Nongye).get(Yi2FSingle)[X];
				//非农
				ziNv.get(cx).get(Feinong).get(SingleDJ)[X] = ziNv.get(cx).get(Feinong).get(FSingleDJ)[X] +
						ziNv.get(cx).get(Feinong).get(MSingleDJ)[X];
				ziNv.get(cx).get(Feinong).get(Yi2FSingle)[X] = ziNv.get(cx).get(Feinong).get(Yi2MSingle)[X] +
						ziNv.get(cx).get(Feinong).get(Yi2FSingle)[X];
				
			}
		}
		// 3. 加上迁移的人口
		for(CX cx : CX.values()){
			for(NY ny : NY.values()){
				for(X = 0 ; X < MAX_AGE ; X++){
					//3.1
					if( ( ziNv.get(cx).get(ny).get(DM)[X] + ziNv.get(cx).get(ny).get(DQYM)[X] ) > 0)
						ziNv.get(cx).get(ny).get(DM)[X] = ziNv.get(cx).get(ny).get(DM)[X] + ziNv.get(cx).get(ny).get(DQYM)[X];
					
					if( ( ziNv.get(cx).get(ny).get(NM)[X] + ziNv.get(cx).get(ny).get(NQYM)[X] ) > 0)
						ziNv.get(cx).get(ny).get(NM)[X] = ziNv.get(cx).get(ny).get(NM)[X] + ziNv.get(cx).get(ny).get(NQYM)[X];
					
					if( ( ziNv.get(cx).get(ny).get(DDBM)[X] + ziNv.get(cx).get(ny).get(DDBQYM)[X] ) > 0)
						ziNv.get(cx).get(ny).get(DDBM)[X] = ziNv.get(cx).get(ny).get(DDBM)[X] + ziNv.get(cx).get(ny).get(DDBQYM)[X];
					
					if( ( ziNv.get(cx).get(ny).get(DDBF)[X] + ziNv.get(cx).get(ny).get(DDBQYF)[X] ) > 0)
						ziNv.get(cx).get(ny).get(DDBF)[X] = ziNv.get(cx).get(ny).get(DDBF)[X] + ziNv.get(cx).get(ny).get(DDBQYF)[X];
					
					if( ( ziNv.get(cx).get(ny).get(DF)[X] + ziNv.get(cx).get(ny).get(DQYF)[X] ) > 0)
						ziNv.get(cx).get(ny).get(DF)[X] = ziNv.get(cx).get(ny).get(DF)[X] + ziNv.get(cx).get(ny).get(DQYF)[X];
					
					if( ( ziNv.get(cx).get(ny).get(NF)[X] + ziNv.get(cx).get(ny).get(NQYF)[X] ) > 0)
						ziNv.get(cx).get(ny).get(NF)[X] = ziNv.get(cx).get(ny).get(NF)[X] + ziNv.get(cx).get(ny).get(NQYF)[X];
					
					//3.2
					ziNv.get(cx).get(ny).get(DQY)[X] = ziNv.get(cx).get(ny).get(DQYF)[X] + 
							ziNv.get(cx).get(ny).get(DQYM)[X];
					ziNv.get(cx).get(ny).get(NQY)[X] = ziNv.get(cx).get(ny).get(NQYF)[X] + 
							ziNv.get(cx).get(ny).get(NQYM)[X];
					ziNv.get(cx).get(ny).get(DDBQYS)[X] = ziNv.get(cx).get(ny).get(DDBQYF)[X] + 
							ziNv.get(cx).get(ny).get(DDBQYM)[X];
					ziNv.get(cx).get(ny).get(HJQYF)[X] = ziNv.get(cx).get(ny).get(DQYF)[X] + 
							ziNv.get(cx).get(ny).get(NQYF)[X] + ziNv.get(cx).get(ny).get(DDBQYF)[X];
					ziNv.get(cx).get(ny).get(HJQYM)[X] = ziNv.get(cx).get(ny).get(DQYM)[X] + 
							ziNv.get(cx).get(ny).get(NQYM)[X] + ziNv.get(cx).get(ny).get(DDBQYM)[X];
					ziNv.get(cx).get(ny).get(HJQY)[X] = ziNv.get(cx).get(ny).get(HJQYF)[X] + 
							ziNv.get(cx).get(ny).get(HJQYM)[X];
				}
			}
		}
		// 3.3 ziNvOfCX
		for(CX cx : CX.values()){ // ziNvOfCX 是空的……
			for(X = 0 ; X < MAX_AGE ; X++){
				ziNvOfCX.get(cx) .get(DQY)[X] = ziNvOfCX.get(cx) .get(DQYF)[X] + 
						ziNvOfCX.get(cx) .get(DQYM)[X];
				ziNvOfCX.get(cx) .get(NQY)[X] = ziNvOfCX.get(cx) .get(NQYF)[X] + 
						ziNvOfCX.get(cx) .get(NQYM)[X];
				ziNvOfCX.get(cx) .get(DDBQYS)[X] = ziNvOfCX.get(cx) .get(DDBQYF)[X] + 
						ziNvOfCX.get(cx) .get(DDBQYM)[X];
				ziNvOfCX.get(cx) .get(HJQYF)[X] = ziNvOfCX.get(cx) .get(DQYF)[X] + 
						ziNvOfCX.get(cx) .get(NQYF)[X] + ziNvOfCX.get(cx) .get(DDBQYF)[X];
				ziNvOfCX.get(cx) .get(HJQYM)[X] = ziNvOfCX.get(cx) .get(DQYM)[X] + 
						ziNvOfCX.get(cx) .get(NQYM)[X] + ziNvOfCX.get(cx) .get(DDBQYM)[X];
				ziNvOfCX.get(cx) .get(HJQY)[X] = ziNvOfCX.get(cx) .get(HJQYF)[X] + 
						ziNvOfCX.get(cx) .get(HJQYM)[X];
			}
		}
		// 3.4 ziNvOfNYFY
		for(NY ny : NY.values()){ 
			for(X = 0 ; X < MAX_AGE ; X++){
				ziNvOfNY.get(ny) .get(DQY)[X] = ziNvOfNY.get(ny) .get(DQYF)[X] + 
						ziNvOfNY.get(ny) .get(DQYM)[X];
				ziNvOfNY.get(ny) .get(NQY)[X] = ziNvOfNY.get(ny) .get(NQYF)[X] + 
						ziNvOfNY.get(ny) .get(NQYM)[X];
				ziNvOfNY.get(ny) .get(DDBQYS)[X] = ziNvOfNY.get(ny) .get(DDBQYF)[X] + 
						ziNvOfNY.get(ny) .get(DDBQYM)[X];
				ziNvOfNY.get(ny) .get(HJQYF)[X] = ziNvOfNY.get(ny) .get(DQYF)[X] + 
						ziNvOfNY.get(ny) .get(NQYF)[X] + ziNvOfNY.get(ny) .get(DDBQYF)[X];
				ziNvOfNY.get(ny) .get(HJQYM)[X] = ziNvOfNY.get(ny) .get(DQYM)[X] + 
						ziNvOfNY.get(ny) .get(NQYM)[X] + ziNvOfNY.get(ny) .get(DDBQYM)[X];
				ziNvOfNY.get(ny) .get(HJQY)[X] = ziNvOfNY.get(ny) .get(HJQYF)[X] + 
						ziNvOfNY.get(ny) .get(HJQYM)[X];
			}
		}
		// 3.5 ziNvOfAll
		for(X = 0 ; X < MAX_AGE ; X++){
			ziNvOfAll .get(DQY)[X] = ziNvOfAll .get(DQYF)[X] + 
					ziNvOfAll .get(DQYM)[X];
			ziNvOfAll .get(NQY)[X] = ziNvOfAll .get(NQYF)[X] + 
					ziNvOfAll .get(NQYM)[X];
			ziNvOfAll .get(DDBQYS)[X] = ziNvOfAll .get(DDBQYF)[X] + 
					ziNvOfAll .get(DDBQYM)[X];
			ziNvOfAll .get(HJQYF)[X] = ziNvOfAll .get(DQYF)[X] + 
					ziNvOfAll .get(NQYF)[X] + ziNvOfAll .get(DDBQYF)[X];
			ziNvOfAll .get(HJQYM)[X] = ziNvOfAll .get(DQYM)[X] + 
					ziNvOfAll .get(NQYM)[X] + ziNvOfAll .get(DDBQYM)[X];
			ziNvOfAll .get(HJQY)[X] = ziNvOfAll .get(HJQYF)[X] + 
					ziNvOfAll .get(HJQYM)[X];
		}
		//4.迁移人口统计
		Map<CX, Map<Year, Map<XB, double[]>>> pplMigCX = populationMigrationfCX;
		Map<Year,Map<XB,double[]>> pplMig = populationMigrationOfAll;
		for(CX cx : CX.values()){
			for(X = 0 ; X<MAX_AGE ; X++)
			{
				//Male
				pplMigCX.get(cx).get(Year.getYear(year)).get(Male)[X] = 
						  ziNv.get(cx).get(Feinong).get(DQYM)[X]
						+ ziNv.get(cx).get(Feinong).get(NQYM)[X]
						+ ziNv.get(cx).get(Feinong).get(DDBQYM)[X]
						+ ziNv.get(cx).get(Nongye).get(DQYM)[X]
						+ ziNv.get(cx).get(Nongye).get(NQYM)[X]
						+ ziNv.get(cx).get(Nongye).get(DDBQYM)[X];
				//Female
				pplMigCX.get(cx).get(Year.getYear(year)).get(Female)[X] = 
						  ziNv.get(cx).get(Feinong).get(DQYF)[X]
						+ ziNv.get(cx).get(Feinong).get(NQYF)[X]
						+ ziNv.get(cx).get(Feinong).get(DDBQYF)[X]
						+ ziNv.get(cx).get(Nongye).get(DQYF)[X]
						+ ziNv.get(cx).get(Nongye).get(NQYF)[X]
						+ ziNv.get(cx).get(Nongye).get(DDBQYF)[X];
			}
		}
		//合计
		for(X = 0 ; X<MAX_AGE ; X++)
		{
			pplMig.get(Year.getYear(year)).get(Male)[X] =  pplMigCX.get(Chengshi).get(Year.getYear(year)).get(Male)[X]
			                                            +  pplMigCX.get(Nongchun).get(Year.getYear(year)).get(Male)[X];
			pplMig.get(Year.getYear(year)).get(Female)[X] =  pplMigCX.get(Chengshi).get(Year.getYear(year)).get(Female)[X]
			                                              +  pplMigCX.get(Nongchun).get(Year.getYear(year)).get(Female)[X];
		}
		//5.一孩和特扶父母的年龄移算
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> sDYiHai = sonDiePopulationPredictOfYiHai, 
													sDTeFu = sonDiePopulationPredictOfTeFu;
		CX drCX;
		for(CX cx : CX.values()){
			for(NY ny : NY.values()){
				for(XB xb : XB.values()){
					if( ny == Nongye){
						drCX = Nongchun;
					}else{
						drCX = Chengshi;
					}
					for(X = MAX_AGE-1 ; X>0;X--){
						sDYiHai.get(cx).get(ny).get(Year.getYear(year)).get(xb)[X] = sDYiHai.get(cx).get(ny).get(Year.getYear(year)).get(xb)[X-1]
						                                                           * (1 - deathRate.get(drCX).get(xb)[X-1]);
						sDTeFu.get(cx).get(ny).get(Year.getYear(year)).get(xb)[X] = sDTeFu.get(cx).get(ny).get(Year.getYear(year)).get(xb)[X-1]
						                                                          *  (1 - deathRate.get(drCX).get(xb)[X-1]);
					}
				}
			}
		}
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
