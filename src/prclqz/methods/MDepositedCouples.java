package prclqz.methods;

import java.util.HashMap;
import java.util.Map;
import static prclqz.core.Const.*;
import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Const;
import prclqz.core.Message;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.CX.*;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.Policy;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;
import prclqz.view.IView;
import test.EnumTools;
/**
 * 渐进普二堆积夫妇估计与剥离
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class MDepositedCouples implements IMethod {
	public static void main(String[] args) throws Exception {
		HashMap<String, Object> globals = new HashMap<String, Object>();
		IDAO m = new MyDAOImpl();

		
		MainTaskBean task;
		task = m.getTask(args[0]);
		
		HashMap<String, BornXbbBean> xbbMap = m.getARBornXbbBeans(task);

		
		TempVariablesBean tempVar = new TempVariablesBean(0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0);

		HashMap<String, Object> predictVarMap = new HashMap<String, Object>();
		globals.put("predictVarMap", predictVarMap);
		int dqx = 10;
		int year = 2011;
		// //////////setup///////////////////
		tempVar.setProvince(dqx);
		tempVar.setYear(year);
		tempVar.setPolicy(Policy.danDu);
		tempVar.setPolicyTime(2011);
		tempVar.setNowN1D2X(2.0);
		tempVar.setNowN2D1C(1.0);
		tempVar.setNowN1D2X(2.0);
		tempVar.setNowN1D2C(1.0);
		tempVar.setNNFX(1.0);
		tempVar.setNNFC(1.0);

		Map<CX, Map<NY, Map<HunpeiField, double[]>>> ziNv = EnumTools
				.creatCXNYZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<CX, Map<HunpeiField, double[]>> ziNvOfCX = EnumTools
				.creatCXZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<NY, Map<HunpeiField, double[]>> ziNvOfNY = EnumTools
				.creatNYFNZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<HunpeiField, double[]> ziNvOfAll = EnumTools.creatAllZiNvFromFile(
				MAX_AGE, tempVar.getYear(), dqx);
		predictVarMap.put("HunpeiOfCXNY" + dqx, ziNv);
		predictVarMap.put("HunpeiOfCX" + dqx, ziNvOfCX);
		predictVarMap.put("HunpeiOfNY" + dqx, ziNvOfNY);
		predictVarMap.put("HunpeiOfAll" + dqx, ziNvOfAll);

		globals.put("tempVarBean", tempVar);	
		globals.put("bornXbbBeanMap", xbbMap);
		MDepositedCouples hehe = new MDepositedCouples();
		hehe.calculate(m, globals);

	}
	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		IView v = (IView) globals.get("view");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		//define local variables
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//(Map<CX, Map<NY, Map<HunpeiField, double[]>>>) provincMigMap.get(""+dqx);
		Policy py = tempVar.getPolicy();
//		System.out.println(py);
		int X;
		double pyRtM,pyRtF;
		
		//&& 在调整时机那年，只做分离  
		//&& 分离出原政策不允许而新政策允许生育的夫妇
		if(year == tempVar.getPolicyTime()){
			for(CX cx : CX.values())
				for(NY ny: NY.values()){
					Map<HunpeiField, double[]> zn = ziNv.get(cx).get(ny);
					
					switch(py){
					case danDu:
					case danDuHou:
						pyRtM = (ny == NY.Nongye )? (2 - tempVar.getNowN2D1X()) : (2 - tempVar.getNowN2D1C());
						pyRtF = (ny == NY.Nongye )? (2 - tempVar.getNowN1D2X()) : (2 - tempVar.getNowN1D2C());
						for(X = 0 ; X< MAX_AGE ; X++){
							if(zn.get(duijiRate)[X] != 0){
								zn.get(MSingleDJ)[X] = zn.get(NFDM)[X] * zn.get(duijiRate)[X]* pyRtM;
								zn.get(FSingleDJ)[X] = zn.get(NMDF)[X] * zn.get(duijiRate)[X]* pyRtF;
							}
							zn.get(SingleDJ)[X] = zn.get(MSingleDJ)[X] + zn.get(FSingleDJ)[X];
							zn.get(NMDF)[X] -= zn.get(FSingleDJ)[X];
							zn.get(NFDM)[X] -= zn.get(MSingleDJ)[X];
						}
						break;
					case puEr:
						pyRtM = (ny == NY.Nongye )? (2 - tempVar.getNNFX()) : (2 - tempVar.getNNFC());
						
						for(X = 0 ; X< MAX_AGE ; X++){
							if(zn.get(duijiRate)[X] != 0){
								zn.get(ShuangfeiDJ)[X] = zn.get(NN)[X] * zn.get(duijiRate)[X] * pyRtM;
							}
							zn.get(NN)[X] -= zn.get(ShuangfeiDJ)[X];
						}
						break;
					default:
						break;
					}
				}// CX-NY For-end
		}else{//&& 大于调整时机后，堆积人数一年年地减少
			for(CX cx : CX.values())
				for(NY ny: NY.values()){
					Map<HunpeiField, double[]> zn = ziNv.get(cx).get(ny);
					switch(py){
					case danDu:
					case danDuHou:
						for(X = 0; X< MAX_AGE; X++){
							zn.get(NMDF)[X] -= zn.get(FSingleDJ)[X];
							zn.get(NFDM)[X] -= zn.get(MSingleDJ)[X];
						}
						break;
					case puEr:
						for(X = 0; X< MAX_AGE; X++){
							zn.get(NMDF)[X] -= zn.get(FSingleDJ)[X];
							zn.get(NFDM)[X] -= zn.get(MSingleDJ)[X];
							zn.get(NN)[X] -= zn.get(ShuangfeiDJ)[X];
						}
						break;
					default:
							break;
					}
				}
		}
 
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
