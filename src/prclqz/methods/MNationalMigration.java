package prclqz.methods;

import static prclqz.core.enumLib.QYPHField.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.Const.*;
import static prclqz.lib.EnumMapTool.createQYPHArrMap;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOException;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.core.Message;
import prclqz.core.enumLib.*;
import prclqz.lib.NumberTool;
import prclqz.parambeans.ParamBean3;
import prclqz.view.IView;

/**
 * National Migration Balancing
 * @author prclqz@zju.edu.cn
 * @time   2011-09-02 first edition
 * @time   2011-10-28 second edition- change to enumMap
 */
public class MNationalMigration implements IMethod {

	@Override
	public void calculate(IDAO database,HashMap<String,Object> globals) throws SQLException, MyDAOException, IOException {
		///get essential variables from globals
		ParamBean3 pb3 = (ParamBean3) globals.get("SetUpEnvParamBean");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		IView v = (IView) globals.get("view");
//		HashMap<String, Object> provincMigMap = (HashMap<String, Object>) globals.get("provinceMigMap");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		
		///define local variables
		BornXbbBean dqXbb;
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv;
		Map<QYPHField,long[]> QYPH= (Map< QYPHField , long[] >) predictVarMap.get( "QYPH" );//createQYPHArrMap(MAX_AGE);
//		provincMigMap.put( "QYPH" , QYPH );
		
		int dqx,X;
		long tmp;
		
		//get content from database
//		database.getNationQY(QYPH,task.getId());
		
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			
			//compute the primary data of the province
			dqXbb = xbbMap.get(""+dqx);
			ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//(Map<CX, Map<NY, Map<HunpeiField, double[]>>>) provincMigMap.get(""+dqx);
			
			//for debug
//			v.writelnToTmpFile(dqBB+"begin");
			
			for(CX cx: CX.values())
				for(NY ny: NY.values())
				{
					for(HunpeiField hpqy : HunpeiField.valuesOfZinvQY())
					{
						//for debug
//						v.writelnToTmpFile("地区"+cx.getChinese()+ny.getChinese()+"子女"+hpqy);
						
						for(X=0;X<MAX_AGE;X++){
							if(ziNv.get(cx).get(ny).get(hpqy)[X] > 0 && QYPH.get(getImFromHp(hpqy))[X] != 0)
								ziNv.get(cx).get(ny).get(hpqy)[X] = QYPH.get(getBalanceFromHp(hpqy))[X] * ziNv.get(cx).get(ny).get(hpqy)[X]  
																  / QYPH.get(getImFromHp(hpqy))[X];
						}
						
						for(X=0;X<MAX_AGE;X++){
							if(ziNv.get(cx).get(ny).get(hpqy)[X] > 0)
								QYPH.get(getBalanceImFromHp(hpqy))[X] += ziNv.get(cx).get(ny).get(hpqy)[X]; 
						}
						for(X=0;X<MAX_AGE;X++){
							if(ziNv.get(cx).get(ny).get(hpqy)[X] < 0 && QYPH.get(getEmFromHp(hpqy))[X] != 0){
								tmp = (long) Math.min( ziNv.get(cx).get(ny).get(getZinvFromZinvqy(hpqy))[X],
										QYPH.get(getBalanceFromHp(hpqy))[X] * ziNv.get(cx).get(ny).get(hpqy)[X]  
											/ QYPH.get(getEmFromHp(hpqy))[X]);
								ziNv.get(cx).get(ny).get(hpqy)[X] = -tmp;
								
							}
						}
						
						for(X=0;X<MAX_AGE;X++){
							if(ziNv.get(cx).get(ny).get(hpqy)[X] < 0)
								QYPH.get(getBalanceEmFromHp(hpqy))[X] += ziNv.get(cx).get(ny).get(hpqy)[X]; 
						}
						
					}
				
					for(X=0;X<MAX_AGE;X++){
						ziNv.get(cx).get(ny).get(DQY)[X] = ziNv.get(cx).get(ny).get(DQYF)[X] + ziNv.get(cx).get(ny).get(DQYM)[X];
						ziNv.get(cx).get(ny).get(NQY)[X] = ziNv.get(cx).get(ny).get(NQYF)[X] + ziNv.get(cx).get(ny).get(NQYM)[X];
						ziNv.get(cx).get(ny).get(DDBQYS)[X] = ziNv.get(cx).get(ny).get(DDBQYF)[X] + ziNv.get(cx).get(ny).get(DDBQYM)[X];
						
						ziNv.get(cx).get(ny).get(HJQYF)[X] = ziNv.get(cx).get(ny).get(DDBQYF)[X] + ziNv.get(cx).get(ny).get(DQYF)[X]+
																ziNv.get(cx).get(ny).get(NQYF)[X];
						
						ziNv.get(cx).get(ny).get(HJQYM)[X] = ziNv.get(cx).get(ny).get(DDBQYM)[X] + ziNv.get(cx).get(ny).get(DQYM)[X]+
																ziNv.get(cx).get(ny).get(NQYM)[X];
						
						ziNv.get(cx).get(ny).get(HJQY)[X] = ziNv.get(cx).get(ny).get(HJQYF)[X] + ziNv.get(cx).get(ny).get(HJQYM)[X];
					}
					
				}
		
		}
		
		//for debug
//		v.saveTmpFile();
//		System.out.println("over!");
//		System.exit(0);
	}


	@Override
	public void setParam(String params, int type) throws MethodException {
	}


	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		return null;
	}

}
