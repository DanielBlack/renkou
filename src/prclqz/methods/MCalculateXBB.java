package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.parambeans.ParamBean3;
/**
 * Calculate the XBB for Every Province
 * @author prclqz@zju.edu.cn
 *
 */
public class MCalculateXBB implements IMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		ParamBean3 pb3 = (ParamBean3) globals.get("SetUpEnvParamBean");
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		
		int dqx;
		BornXbbBean dqXbb;
		int year;
		double xbbRate;
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			
			//primary data of province 
			dqXbb = xbbMap.get(""+dqx);
			
			year = tempVar.getYear();
			//  xbbRate =  xbb05- abs(xbb05-xbb90)/15*(year-2005)   xbbRate = (XbbRate>108? xbbRate: 108) /100
			xbbRate = dqXbb.getBornXbb05() - Math.abs(dqXbb.getBornXbb05()-dqXbb.getBornXbb90()) / 15.0 * (year - 2005);
			xbbRate = (xbbRate>108? xbbRate: 108) /100;
			
			dqXbb.setXbb(xbbRate);
		}
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
