package prclqz.methods;


import java.util.Map;

import static prclqz.core.enumLib.SumBM.*;
import static prclqz.core.enumLib.Summary.*;
import static prclqz.core.enumLib.SYSFMSField.*;
import static prclqz.core.enumLib.BabiesBorn.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.XB.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.Policy.*;
import static prclqz.core.Const.*;

import java.util.HashMap;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.StringList;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.Couple;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.SumBM;
import prclqz.core.enumLib.Summary;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;
import prclqz.lib.EnumMapTool;
import prclqz.parambeans.ParamBean3;

/**
 * 计算预期寿命
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class MCalculateExpectedAge implements ICalculateExpectedAgeMethod
{

	@Override
	public void calculate ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		//下面的代码貌似都用不着
//		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
//		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
//		StringList strValues = ( StringList ) globals.get ( "strValues" );
//		
//		//get running status
//		int dqx = tempVar.getProvince();
//		int year = tempVar.getYear();
//		BornXbbBean xbb = xbbMap.get(""+dqx);
//		String diDai = xbb.getDiDai();
//		String dqB = xbb.getDqName ( );
//		//define local variables
//		int X=0;
//		BabiesBornBean bBornBean = tempVar.getBabiesBorn();
//		//生育意愿
//		Map<String,Map<SYSFMSField,double[]>> birthWill = (Map<String, Map<SYSFMSField, double[]>>) predictVarMap.get("BirthWill");
//		Map<SYSFMSField,double[]> pBirthWill = birthWill.get(diDai);
//		
//		//地区分龄政策生育
//		Map<Babies,double[]> policyBabies = (Map<Babies, double[]>) predictVarMap.get("PolicyBabies"+dqx);
//		//分年龄生育预测(生育，超生，政策)
//		Map<CX,Map<Babies,double[]>> BirthPredictOfCX = ( Map < CX , Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfCX"+dqx );
//		Map<NY,Map<Babies,double[]>> BirthPredictOfNY = ( Map < NY , Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfNY"+dqx );
//		Map<Babies,double[]> BirthPredictOfAll = ( Map < Babies , double [ ] > ) predictVarMap.get ( "BirthPredictOfAll"+dqx );
//		Map<CX,Map<Babies,double[]>> OverBirthPredictOfCX = ( Map < CX , Map < Babies , double [ ] >> ) predictVarMap.get ( "OverBirthPredictOfCX"+dqx );
//		Map<NY,Map<Babies,double[]>> OverBirthPredictOfNY = ( Map < NY , Map < Babies , double [ ] >> ) predictVarMap.get ( "OverBirthPredictOfNY"+dqx );
//		Map<Babies,double[]> OverBirthPredictOfAll = ( Map < Babies , double [ ] > ) predictVarMap.get ( "OverBirthPredictOfAll"+dqx );
//		Map<Babies,double[]> PolicyBabies = ( Map < Babies , double [ ] > ) predictVarMap.get ( "PolicyBabies"+dqx );
//		
//		//分龄人口预测
//		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> PopulationPredictOfCXNY = ( Map < CX , Map < NY , Map < Year , Map < XB , double [ ] >>> > ) predictVarMap.get ( "PopulationPredictOfCXNY"+dqx );
//		Map<CX,Map<Year,Map<XB,double[]>>>PopulationPredictOfCX = ( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationPredictOfCX"+dqx );
//		Map<NY,Map<Year,Map<XB,double[]>>> PopulationPredictOfNY = ( Map < NY , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationPredictOfNY"+dqx );
//		Map<Year,Map<XB,double[]>> PopulationPredictOfAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "PopulationPredictOfAll"+dqx );
//
//		//分龄迁移人口
//		Map<CX,Map<Year,Map<XB,double[]>>> PopulationMigrationOfCX = ( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationMigrationOfCX"+dqx );
//		Map<NY,Map<Year,Map<XB,double[]>>> PopulationMigrationOfNY = ( Map < NY , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationMigrationOfNY"+dqx );
//		Map<Year,Map<XB,double[]>> PopulationMigrationOfAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "PopulationMigrationOfAll"+dqx );
//		
//		//分龄死亡人口
//		Map<CX,Map<Year,Map<XB,double[]>>> PopulationDeathOfCX = ( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationDeathOfCX"+dqx );
//		Map<Year,Map<XB,double[]>> PopulationDeathOfAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "PopulationDeathOfAll"+dqx );
//		
//		
//		//死亡概率预测
//		Map<CX,Map<XB,double[]>> deathRate = ( Map < CX , Map < XB , double [ ] >> ) predictVarMap.get ( "DeathRate"+dqx );
//		
//		//夫妇及子女表结构
//		Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>> CoupleAndChildrenOfCXNY = ( Map < CX , Map < NY , Map < Year , Map < Couple , Double >>> > ) predictVarMap.get ( "CoupleAndChildrenOfCXNY"+dqx );
//		Map<CX,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfCX = ( Map < CX , Map < Year , Map < Couple , Double >>> ) predictVarMap.get ( "CoupleAndChildrenOfCX"+dqx );
//		Map<NY,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfNY = ( Map < NY , Map < Year , Map < Couple , Double >>> ) predictVarMap.get ( "CoupleAndChildrenOfNY"+dqx );
//		Map<Year,Map<Couple,Double>> CoupleAndChildrenOfAll = ( Map < Year , Map < Couple , Double >> ) predictVarMap.get ( "CoupleAndChildrenOfAll"+dqx );
//		
//		//丧子人口预测--一孩
//		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> SonDiePopulationPredictOfYiHaiCXNY = ( Map < CX , Map < NY , Map < Year , Map < XB , double [ ] >>> > ) predictVarMap.get ( "SonDiePopulationPredictOfYiHaiCXNY"+dqx );
//		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> yiHaiCN = SonDiePopulationPredictOfYiHaiCXNY;
////		yiHaiCN.get ( Chengshi ).get ( Nongye ).get ( Year.getYear ( year ) ).get ( Male )[X] = 0;
//		
//		//丧子人口预测--特扶
//		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> SonDiePopulationPredictOfTeFuCXNY = ( Map < CX , Map < NY , Map < Year , Map < XB , double [ ] >>> > ) predictVarMap.get ( "SonDiePopulationPredictOfTeFuCXNY"+dqx );
//		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> tefuCN = SonDiePopulationPredictOfTeFuCXNY;
//		
//		// 一孩、特扶的 CX,NY和All
//		Map<CX,Map<Year,Map<XB,double[]>>>SonDiePopulationPredictOfTeFuCX = ( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "SonDiePopulationPredictOfTeFuCX"+dqx ),
//			SonDiePopulationPredictOfYiHaiCX=( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "SonDiePopulationPredictOfYiHaiCX"+dqx );
//		
//		Map<CX,Map<Year,Map<XB,double[]>>> yiHaiC = SonDiePopulationPredictOfYiHaiCX,
//			tefuC = SonDiePopulationPredictOfTeFuCX;
//		
//		Map<NY,Map<Year,Map<XB,double[]>>> SonDiePopulationPredictOfTeFuNY = ( Map < NY , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "SonDiePopulationPredictOfTeFuNY"+dqx ),
//			SonDiePopulationPredictOfYiHaiNY = ( Map < NY , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "SonDiePopulationPredictOfYiHaiNY"+dqx );
//		Map<NY,Map<Year,Map<XB,double[]>>> yiHaiN = SonDiePopulationPredictOfYiHaiNY,
//			tefuN = SonDiePopulationPredictOfTeFuNY;
//		Map<Year,Map<XB,double[]>> SonDiePopulationPredictOfTeFuAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "SonDiePopulationPredictOfTeFuAll"+dqx ),
//			SonDiePopulationPredictOfYiHaiAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "SonDiePopulationPredictOfYiHaiAll"+dqx );
//		Map<Year,Map<XB,double[]>> yiHaiA = SonDiePopulationPredictOfYiHaiAll,
//			tefuA = SonDiePopulationPredictOfTeFuAll;
//		
//		
//		//需夫模式
//		double[][] husbandRate = ( double [ ][ ] ) predictVarMap.get("HusbandRate");
//		
//		//主任务参数
//		ParamBean3 envParm = (ParamBean3) globals.get("SetUpEnvParamBean");
//		
//		//政策及政策生育率					
//		Map<Year,Map<PolicyBirth,Double>> PolicyBirthRate = ( Map < Year , Map < PolicyBirth , Double >> ) predictVarMap.get ( "PolicyBirthRate"+dqx );
//		
//		//生育孩次数
//		Map<Year,Map<BabiesBorn,Double>> babiesBorn = ( Map < Year , Map < BabiesBorn , Double >> ) predictVarMap.get ( "BabiesBorn"+dqx );
//		
//		//仿真结果_概要
//		Map<Year,Map<Summary,Double >> SummaryOfAll = ( Map < Year , Map < Summary , Double >> ) predictVarMap.get ( "SummaryOfAll"+dqx );
//		Map<CX,Map<Year,Map<Summary,Double >>> SummaryOfCX = ( Map < CX , Map < Year , Map < Summary , Double >>> ) predictVarMap.get ( "SummaryOfCX"+dqx );
//		Map<NY,Map<Year,Map<Summary,Double >>> SummaryOfNY = ( Map < NY , Map < Year , Map < Summary , Double >>> ) predictVarMap.get ( "SummaryOfNY"+dqx );
//		
//		//地区生育率与迁移摘要				
//		Map<Year,Map<SumBM,Double>> SummaryOfBirthAndMigration = ( Map < Year , Map < SumBM , Double >> ) predictVarMap.get ( "SummaryOfBirthAndMigration");
//		Map<Year,Map<SumBM,Double>> sumBM = SummaryOfBirthAndMigration;
		
		/////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		//////////////translated by Foxpro2Java Translator successfully:///////////////
		int REC = (int) tempVar.MINLM;
		double[] livePopulationM = new double[ REC + 1 ];
		double[] liveAgeM = new double[ REC + 1 ];
		livePopulationM[ 1 ] = 1.0 - tempVar.QXM[ 1 ];
		int I0;
		for ( I0 = 2 ; I0 <= REC ; I0++ )
		{
			livePopulationM[ I0 ] = livePopulationM[ I0 - 1 ]
					* ( 1.0 - tempVar.QXM[ I0 ] );
			
		}
		liveAgeM[ REC ] = livePopulationM[ REC ];
		for ( I0 = 2 ; I0 <= REC ; I0++ )
		{
			liveAgeM[ REC + 1 - I0 ] = liveAgeM[ REC + 1 - I0 + 1 ]
					+ livePopulationM[ REC + 1 - I0 ]
					+ livePopulationM[ REC + 1 - I0 ]
					* tempVar.QXM[ REC + 1 - I0 ];
		}
		tempVar.EM0 = liveAgeM[ 1 ];
		
		REC = (int) tempVar.MINLF;
		double[] livePopulationF = new double[ REC + 1 ];
		double[] liveAgeF = new double[ REC + 1 ];
		livePopulationF[ 1 ] = 1.0 - tempVar.QXF[ 1 ];
		for ( I0 = 2 ; I0 <= REC ; I0++ )
		{
			livePopulationF[ I0 ] = livePopulationF[ I0 - 1 ]
					* ( 1.0 - tempVar.QXF[ I0 ] );
		}
		liveAgeF[ REC ] = livePopulationF[ REC ];
		for ( I0 = 2 ; I0 <= REC ; I0++ )
		{
			liveAgeF[ REC + 1 - I0 ] = liveAgeF[ REC + 1 - I0 + 1 ]
					+ livePopulationF[ REC + 1 - I0 ]
					+ livePopulationF[ REC + 1 - I0 ]
					* tempVar.QXF[ REC + 1 - I0 ];
		}
		tempVar.EF0 = liveAgeF[ 1 ];
		/////////////end of translating, by Foxpro2Java Translator///////////////
		
		/*********test部分,只要看cxI=4的情况下的值就行了**********/
//		System.out.println("**********"+tempVar.cxI+"**********");
//		System.out.println("QXM:"+tempVar.QXM[1]);
//		System.out.println("生存人数M:");
//		for(int i=1; i<=tempVar.MINLM; i++){
//			System.out.println(i+": "+livePopulationM[i]);
//		}
//		System.out.println("生存年数M:");
//		for(int i=1; i<=tempVar.MINLM; i++){
//			System.out.println(i+": "+liveAgeM[i]);
//		}
//		System.out.println("生存人数F:");
//		for(int i=1; i<=tempVar.MINLF; i++){
//			System.out.println(i+": "+livePopulationF[i]);
//		}
//		System.out.println("生存年数F:");
//		for(int i=1; i<=tempVar.MINLF; i++){
//			System.out.println(i+": "+liveAgeF[i]);
//		}
//		System.out.println(tempVar.EM0);
//		System.out.println(tempVar.EF0);
		/********test END*********/

	}

	@Override
	public Message checkDatabase ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		return null;
	}

	@Override
	public void setParam ( String params , int type ) throws MethodException
	{

	}

}
