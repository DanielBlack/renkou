package prclqz.methods;

import static prclqz.core.enumLib.Policy.getPolicyById;

import java.util.HashMap;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.StringList;
import prclqz.core.enumLib.Policy;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.ZCImpl;
import prclqz.parambeans.ParamBean3;
import prclqz.view.IView;


/**
 * 保存年度演算数据到数据库中！
 * @author Jack Long
 * @email  prclqz@zju.edu.cn
 *
 */
public class MSavingCalculatedData implements IMethod
{

	@Override
	public void calculate ( IDAO database , HashMap< String , Object > globals )
			throws Exception
	{
		//define variables
		HashMap<String,Object> predictVarMap = (HashMap< String , Object >) globals.get( "predictVarMap" );
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		StringList strValues = ( StringList ) globals.get ( "strValues" );
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		IView v = (IView) globals.get("view");
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		ParamBean3 envParm = (ParamBean3) globals.get("SetUpEnvParamBean");
		
		//加入缺少的全国项
		if(! xbbMap.containsKey( "0" ))
			xbbMap.put( "0" , new BornXbbBean( 0 , "全国" , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , "" ) );
		
		
		 //设置保存数据时通用的Insert内容，
		 //包括调整时机，迁移模式，政策内容等
		String commonInsert = createCommonInsert(globals);
		database.setCommonInsert( commonInsert );
		database.setTaskID( task.getId() );
		database.setXbbMap( xbbMap );
		database.setView(v);
		database.setEnvParam(envParm);
		
		// 仿真结果_地区生育率与迁移摘要				*SummaryOfBirthAndMigration+dqx(包括0)
		database.saveSumBM(predictVarMap , strValues );
		
		// 仿真结果_分婚配模式生育孩次数				*"BabiesBorn"+dqx
		database.saveBabiesBorn(predictVarMap , strValues);
		
		// 仿真结果_分年龄人口预测	(预测)				*"PopulationPredictOf[All,NY,CXNY]"+dqx
		// 仿真结果_分年龄人口预测	(迁移)				*"PopulationMigrationOf[CX,All]"+dqx	
		// 仿真结果_分年龄人口预测	(死亡)				*"PopulationDeathOf[CX,All]"+dqx
		database.savePredict(predictVarMap , strValues );
		
		// 仿真结果_分年龄丧子人口预测(特扶)			"SonDiePopulationPredictOfTeFu"+dqx			
		// 仿真结果_分年龄丧子人口预测(一孩)			"SonDiePopulationPredictOfYiHai"+dqx
		database.saveSonDie(predictVarMap , strValues );
		
		// 仿真结果_分年龄生育预测(预测)				*BirthPredictOf[CX,NY,All]+dqx
		// 仿真结果_分年龄生育预测(超生)				*OverBirthPredictOf[CX,NY,All]+dqx
		// 仿真结果_分年龄生育预测(政策)				*"PolicyBabies"+dqx
		database.saveBabies(predictVarMap , strValues );
		
		// 仿真结果_夫妇及子女表结构					*CoupleAndChildrenOf[All,CX,NY,CXNY]+dqx
		database.saveCouple(predictVarMap , strValues );
		
		// 仿真结果_妇女分年龄生育率预测				*BirthRatePredict+dqx
		database.saveBirthRatePredict(predictVarMap , strValues );
		
		// 仿真结果_概要								*SummaryOf[All,CX,NY]+dqx
		database.saveSummary(predictVarMap , strValues );
		
		// 仿真结果_政策及政策生育率					*PolicyBirthRate+dqx
		database.savePolicyBirth(predictVarMap , strValues );
		
		//仿真结果_婚配概率							*MarriageRate+dqx
		database.saveMirriageRate(predictVarMap , strValues );
		
		//仿真结果_婚配预测表结构！MProvinceMigration中完成		！provinceMigMap！* ""+dqx ,在！predictVarMap！HunpeiOfCXNY+dqx
		// 仿真结果_婚配预测表结构！MAgingCalculation中完成		！provinceMigMap！* "CX"+dqx,在！predictVarMap！HunpeiOfCX+dqx
		// 仿真结果_婚配预测表结构！MAgingCalculation中完成		！provinceMigMap！* "NY"+dqx,在！predictVarMap！HunpeiOfNY+dqx
		// 仿真结果_婚配预测表结构！在MAgingCalculation中完成		！provinceMigMap！* "All"+dqx,在！predictVarMap！HunpeiOfAll+dqx
		database.saveHunpei(predictVarMap , strValues );
		
		//仿真结果_全国迁移平衡表结构		！tempVar.getQY() > 0 才有*(在provinceMigMap和predictVarMap中都有)，QYPH
		database.saveQYPH(predictVarMap , strValues );
		
		//仿真结果_生育政策模拟摘要					*PolicySimulationAbstract
		database.saveAbstract(predictVarMap , strValues );
		
		
	}


	/**
	 * 创建保存数据时通用的Insert内容，
	 * 包括调整时机，迁移模式，政策内容等
	 * @param globals
	 * @return
	 * @throws Exception
	 */
	private String createCommonInsert (HashMap< String , Object > globals) throws Exception
	{
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		String JA,FA,ZCimpl,SFMS;
		
		ZCimpl = ZCImpl.getZCImplFromId( tempVar.ZCimplement ).toString();
		SFMS = SYSFMSField.getSFMSfromId( tempVar.SFMS ).toString();
		
		JA = Policy.getChinese( getPolicyById ( tempVar.feiNongPolicy1 ) )
		+ ( tempVar.feiNongTime1 == 0 ? "" : "" + tempVar.feiNongTime1 )
		+ Policy.getChinese( getPolicyById ( tempVar.feiNongPolicy2 ) )
		+ ( tempVar.feiNongTime2 == 0 ? "" : "" + tempVar.feiNongTime2 )
		+ Policy.getChinese( getPolicyById ( tempVar.feiNongPolicy3 ) )
		+ ( tempVar.feiNongTime3 == 0 ? "" : "" + tempVar.feiNongTime3 );
		
		FA =  tempVar.QY == 0 ? "无迁" : ( tempVar.QY == 1 ? "低迁"
				: ( tempVar.QY == 2 ? "中迁" : "高迁" ) ); 
		return "'"+ZCimpl+"','"+SFMS+"','"+JA+"','"+FA+"'";
	}


	@Override
	public Message checkDatabase ( IDAO m , HashMap< String , Object > globals )
			throws Exception
	{
		return null;
	}

	@Override
	public void setParam ( String params , int type ) throws MethodException
	{
	}

}
