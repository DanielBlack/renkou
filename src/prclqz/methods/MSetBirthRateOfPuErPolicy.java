package prclqz.methods;

import java.util.HashMap;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.Policy;
import prclqz.view.IView;
/**
 * 渐进普二终身政策生育率-未审核结果
 * @author prclqz@zju.edu.cn
 *
 */
public class MSetBirthRateOfPuErPolicy implements IMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		HashMap<String,Integer> mmm = (HashMap<String,Integer>)globals.get("mainMeanMap");
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		IView v = (IView) globals.get("view");
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		HashMap<String,Double> psntBPy = (HashMap<String, Double>) predictVarMap.get("PresentBirthPolicy");
		
		//1. for NOW
		//1.1. get minzuBirthRate
		double mzBthRt;
		int prv = tempVar.getProvince();
		if( prv == mmm.get("广西-省份/地区")){
			mzBthRt = 1- ( psntBPy.get(prv+"汉族B") + psntBPy.get(prv+"壮族B"));
		}else{
			mzBthRt = 1-  psntBPy.get(prv+"汉族B");
		}
		//1.2. get others and save
		tempVar.setNowN1D2C(psntBPy.get(prv+"城居"));
		tempVar.setNowN2D1C(psntBPy.get(prv+"城居"));
		tempVar.setNowNNFC (psntBPy.get(prv+"城居"));
		tempVar.setDDFR(2);
		double sx = psntBPy.get(prv+"农村单独");
		double dx = psntBPy.get(prv+"农村双非");
		if(  (psntBPy.get(prv+"双少民族户") == 2) || (psntBPy.get(prv+"少数民族") == 2) ){
			double srate = sx*(1-mzBthRt)+2*mzBthRt;
			double drate = dx*(1-mzBthRt)+2*mzBthRt;
			tempVar.setNowN1D2X(srate);
			tempVar.setNowN2D1X(srate);
			tempVar.setNowNNFX(drate);
		}else{
			tempVar.setNowN1D2X(sx);
			tempVar.setNowN2D1X(sx);
			tempVar.setNowNNFX(dx);
		}
		
		//2. For Future
		//2.1 get the policy and set the policyTime(政策调整时机)
		Policy policy;
		if(tempVar.getYear()<tempVar.getFeiNongTime2() && tempVar.getFeiNongTime1()==0){
			policy = Policy.xianXing;
			tempVar.setPolicyTime(0);
		}else if(tempVar.getYear() >= tempVar.getFeiNongTime1() && tempVar.getYear()<tempVar.getFeiNongTime2()){
			policy = Policy.getPolicyById(tempVar.getFeiNongPolicy1());
			tempVar.setPolicyTime(tempVar.getFeiNongTime1());
		}else if(tempVar.getYear() >= tempVar.getFeiNongTime2() && tempVar.getYear()<tempVar.getFeiNongTime3()){
			policy = Policy.getPolicyById(tempVar.getFeiNongPolicy2());
			tempVar.setPolicyTime(tempVar.getFeiNongTime2());
		}else if(tempVar.getYear() >  tempVar.getFeiNongTime3()){
			policy = Policy.getPolicyById(tempVar.getFeiNongPolicy3());
			tempVar.setPolicyTime(tempVar.getFeiNongTime3());
		}else{
			//TODO 
			policy = null;
//			tempVar.setPolicyTime(0);
//			throw new MethodException("找不到对应年份的政策 in MSetBirthRateOfPuEr");
		}
		//保存当前政策
		if(policy != null)
		{
			tempVar.setPolicy(policy);
//		System.out.println(policy);
		//2.2
		
			switch(policy){
			case shuangDuHou:
			case xianXing:
				tempVar.setN1D2C(psntBPy.get(prv+"城居"));
				tempVar.setN2D1C(psntBPy.get(prv+"城居"));
				tempVar.setNNFC (psntBPy.get(prv+"城居"));
				sx = psntBPy.get(prv+"农村单独");
				dx = psntBPy.get(prv+"农村双非");
				if(  (psntBPy.get(prv+"双少民族户") == 2) || (psntBPy.get(prv+"少数民族") == 2) ){
					double srate = sx*(1-mzBthRt)+2*mzBthRt;
					double drate = dx*(1-mzBthRt)+2*mzBthRt;
					tempVar.setN1D2X(srate);
					tempVar.setN2D1X(srate);
					tempVar.setNNFX(drate);
				}else{
					tempVar.setN1D2X(sx);
					tempVar.setN2D1X(sx);
					tempVar.setNNFX(dx);
				}
				break;
			case danDu:
				tempVar.setN1D2C(0);
				tempVar.setN2D1C(0);
				tempVar.setN1D2X(0);
				tempVar.setN2D1X(0);
				tempVar.setNNFC (1);
				dx = psntBPy.get(prv+"农村双非");
				if(  (psntBPy.get(prv+"双少民族户") == 2) || (psntBPy.get(prv+"少数民族") == 2) ){
					double drate = dx*(1-mzBthRt)+2*mzBthRt;
					tempVar.setNNFX(drate);
				}else{
					tempVar.setNNFX(dx);
				}
				break;
			case danDuHou:
				tempVar.setN1D2C(2);
				tempVar.setN2D1C(2);
				tempVar.setN1D2X(2);
				tempVar.setN2D1X(2);
				tempVar.setNNFC (1);
				dx = psntBPy.get(prv+"农村双非");
				if(  (psntBPy.get(prv+"双少民族户") == 2) || (psntBPy.get(prv+"少数民族") == 2) ){
					double drate = dx*(1-mzBthRt)+2*mzBthRt;
					tempVar.setNNFX(drate);
				}else{
					tempVar.setNNFX(dx);
				}
				break;
			case puEr:
				tempVar.setN1D2C(2);
				tempVar.setN2D1C(2);
				tempVar.setN1D2X(2);
				tempVar.setN2D1X(2);
				tempVar.setNNFC (2);
				tempVar.setNNFX(2);
				break;
			default:
	//			throw new MethodException("找不到对应的政策的计算方法 in MSetBirthRateOfPuEr");
			}
		}
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
