package prclqz.methods;

import java.util.HashMap;

import net.sf.json.JSONObject;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.core.Message;
import prclqz.core.StringList;
import prclqz.parambeans.ParamBean3;

/**
 * 初始化计算环境(初步检查数据库表结构),设置任务并发模型等参数环境
 * @author prclqz@zju.edu.cn
 * @time 2011-09-02 first edition
 * @time 2011-11-06 second edition: delete the code which copy 婚配现状 to 婚配预测;
 * 		   因为不需要先拷贝到婚配预测再用，而是直接存取到内存
 */
public class MSetUpEnv implements IMethod{
	private ParamBean3 pb;
	
	public void calculate(IDAO m,HashMap<String,Object> globals) {
		System.out.println("SetUpEnv calculate!"+pb.getAuthor());
	}

	public void setParam(String params, int type) throws MethodException  {
		if(type!=3)
			throw new MethodException("MSetUpEnv 方法的 参数不匹配！");
//		System.out.println(params);
//		String json ="{\"author\":\"LLLab-LS\",\"begin\":2000,\"end\":2100}"; 
//		System.out.println(json);
		JSONObject jsonObject = JSONObject.fromObject(params);    
		pb = (ParamBean3) JSONObject.toBean( jsonObject, ParamBean3.class );
	}

	/**
	 * 0.检查基本参数和基础数据,若不完整则返回错误信息
	 * 1.若之前任务未完全建立结果集，则删除旧的任务结果，并新建新的结果
	 * 2.建立临时表中的全局变量
	 */
	@Override
	public Message checkDatabase(IDAO m,HashMap<String,Object> globals)throws Exception {
		// TODO 0.检查基本参数和基础数据,若不完整则返回错误信息
		
		// 1.获取重要的全局对象表
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		
		//生成并保存重要二位数组    “ARDQ出生XBB” 并保存到全局对象表
		HashMap<String, BornXbbBean> xbbMap = m.getARBornXbbBeans(task);
		globals.put("bornXbbBeanMap", xbbMap);
		
		//设置任务完成状态
		task.setProcess_rate( 5 );
		m.saveTask(task);
		
		// TODO 判断当前基本参数、基础数据、现状数据 是否 合法有效安全
		
		//生成并保存重要循环参数bean——并在数据库中插入相应记录
		m.insertTempVariables(task.getId());
		globals.put("tempVarBean", m.getTempVariables(task.getId()));
		
		//生成并保存 主参数对应表
		globals.put("mainMeanMap", m.getMainMeanMap());
		
//		//用来保存出了TempVariablesBean以外的临时全局变量并存入全局变量表——丢弃
//		HashMap<String,Integer> tempVars2 = new HashMap<String, Integer>();
//		globals.put("tempVarMap", tempVars2);
		
		//由于别处的需要，特将方法的参数Bean存入全局变量表
		globals.put("SetUpEnvParamBean", this.pb);
		
		
		return new Message(true, null);
	}


}
