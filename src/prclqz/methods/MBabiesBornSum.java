package prclqz.methods;


import static prclqz.core.enumLib.Babies.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import java.util.HashMap;
import java.util.Map;
import static prclqz.core.Const.*;

import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.view.IView;
import test.EnumTools;
/**
 * 各类夫妇生育孩子数  &&可实现生育孩子数	&&
 * @author prclqz@zju.edu.cn
 *
 */
public class MBabiesBornSum implements IMethod
{
	public static void main(String[] argv) throws Exception
	{
		IDAO m =new MyDAOImpl();
		HashMap<String,Object> globals = new HashMap<String,Object>();
		TempVariablesBean tempVar = new TempVariablesBean(0,0,0,0,0,0,0,0,0,0,0,0);
		int dqx=10;
		int year=2010;
		/***构造tempvar***/
		tempVar.setProvince(dqx);
		tempVar.setYear(year);
		globals.put("tempVarBean", tempVar);
		/***构造predictVarMap***/
		HashMap<String,Object> predictVarMap = new HashMap<String,Object>();
		Map<CX, Map<NY, Map<HunpeiField, double[]>>> ziNv = EnumTools
				.creatCXNYZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<CX, Map<HunpeiField, double[]>> ziNvOfCX = EnumTools
				.creatCXZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<NY, Map<HunpeiField, double[]>> ziNvOfNY = EnumTools
				.creatNYFNZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		Map<HunpeiField, double[]> ziNvOfAll = EnumTools
				.creatAllZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		predictVarMap.put("HunpeiOfCXNY" + dqx, ziNv);
		predictVarMap.put("HunpeiOfCX" + dqx, ziNvOfCX);
		predictVarMap.put("HunpeiOfNY" + dqx, ziNvOfNY);
		predictVarMap.put("HunpeiOfAll" + dqx, ziNvOfAll);

		
		Map<CX,Map<Babies,double[]>> BirthPredictOfCX = EnumTools.createBirthPredictOfCX(MAX_AGE, tempVar.getYear(), dqx);
		Map<NY,Map<Babies,double[]>> BirthPredictOfNY = EnumTools.createBirthPredictOfNY(MAX_AGE, tempVar.getYear(), dqx);
		Map<Babies,double[]> BirthPredictOfAll = EnumTools.createBirthPredictALL(MAX_AGE, tempVar.getYear(), dqx);
		//not used 
		//Map<Babies,double[]> policyBabies = new Map<Babies,double[]>();
		//TODO 突然想到有可能put里的字符串输入错了,但是由于测试的时候字符串是按照方法来设置的肯定不会错,所以如果最后明明单个模块都能通过,但是整体运行出错的话可能是这个问题
		predictVarMap.put("BirthPredictOfNY" + dqx, BirthPredictOfNY);
		predictVarMap.put("BirthPredictOfCX" + dqx, BirthPredictOfCX);
		predictVarMap.put("BirthPredictOfAll" + dqx, BirthPredictOfAll);
		
		globals.put("predictVarMap", predictVarMap);
		MBabiesBornSum hehe = new MBabiesBornSum();
		hehe.calculate(m, globals);
	}
	@Override
	public void calculate ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		//define local variables
		int X=0;
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//provincMigMap.get(""+dqx);
		//地区分龄政策生育,not used
		Map<Babies,double[]> policyBabies = (Map<Babies, double[]>) predictVarMap.get("PolicyBabies"+dqx);
		//地区 子女婚配预测表
		Map<NY,Map<HunpeiField,double[]>> znNY = (Map<NY, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfNY"+dqx );//provincMigMap.get("NY"+dqx);
		Map<CX,Map<HunpeiField,double[]>> znCX = (Map<CX, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfCX"+dqx );//provincMigMap.get("CX"+dqx);
		Map<HunpeiField,double[]> znAll = (Map<HunpeiField, double[]>) predictVarMap.get( "HunpeiOfAll"+dqx );//provincMigMap.get("All"+dqx);
		//分年龄生育预测(生育，超生，政策)
		Map<CX,Map<Babies,double[]>> BirthPredictOfCX = ( Map < CX , Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfCX"+dqx );
		Map<NY,Map<Babies,double[]>> BirthPredictOfNY = ( Map < NY , Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfNY"+dqx );
		Map<Babies,double[]> BirthPredictOfAll = ( Map < Babies , double [ ] > ) predictVarMap.get ( "BirthPredictOfAll"+dqx );
		
		for(X = MAX_AGE - 1; X>=0;X-- ){
//			ny:Nongye和Feinong	在农业子女和非农子女中循环
			for( NY ny: NY.values ( )){
				Map < HunpeiField , double [ ] > zn = znNY.get ( ny );
				zn.get ( DDB )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( DDB )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( DDB )[X];
				zn.get ( NMDFB )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( NMDFB )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( NMDFB )[X];
				zn.get ( NFDMB )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( NFDMB )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( NFDMB )[X];
				zn.get ( NNB )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( NNB )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( NNB )[X];
				//SingleRls:单独释放B,ShuangfeiRls:双飞释放B
				zn.get ( SingleRls )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( SingleRls )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( SingleRls )[X];
				zn.get ( ShuangFeiRls )[X] = ziNv.get ( Chengshi ).get ( ny ).get ( ShuangFeiRls )[X]+
									ziNv.get ( Nongchun ).get ( ny ).get ( ShuangFeiRls )[X];
			}
			
		}
		
		/////////////translated by Foxpro2Java Translator successfully:///////////////
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				BirthPredictOfNY.get ( Feinong ).get ( Babies.getBabies ( year ) ) [ X ] = 
						( ziNv.get ( Nongchun ).get ( Feinong ).get ( DDB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Nongchun ).get ( Feinong ).get (	SingleRls ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( ShuangFeiRls ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Feinong ).get ( DDB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Feinong ).get ( SingleRls ) [ X ] 
						+ ziNv.get ( Chengshi ).get (Feinong ).get ( ShuangFeiRls ) [ X ] );
				//System.out.println(BirthPredictOfNY.get ( Feinong ).get ( Babies.getBabies ( year ) ) [ X ]);//done!
			}
			if ( true )
			{
				BirthPredictOfNY.get ( Nongye ).get ( Babies.getBabies ( year ) ) [ X ] = 
						( ziNv.get ( Nongchun ).get ( Nongye ).get ( DDB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Nongchun ).get ( Nongye ).get ( SingleRls ) [ X ] 
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( ShuangFeiRls ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Nongye ).get ( DDB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Nongye ).get ( SingleRls ) [ X ] 
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( ShuangFeiRls ) [ X ] );
				//System.out.println(BirthPredictOfNY.get ( Nongye ).get ( Babies.getBabies ( year ) ) [ X ]);done
			}
			if ( true )
			{
				BirthPredictOfCX.get ( Nongchun ).get (Babies.getBabies ( year ) ) [ X ] = 
						( ziNv.get (Nongchun ).get ( Nongye ).get ( DDB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Nongchun ).get ( Feinong ).get ( DDB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NFDMB ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Nongchun ).get ( Nongye ).get ( SingleRls ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Nongye ).get ( ShuangFeiRls ) [ X ]
						+ ziNv.get ( Nongchun ).get ( Feinong ).get ( SingleRls ) [ X ] 
						+ ziNv.get ( Nongchun ).get ( Feinong ).get (ShuangFeiRls ) [ X ] );
				//有个问题:foxpro的输出文件中x=16时,2010年的数据是83,这里的83虽然也是X=16,但是可以到0,foxpro的x可以等于0?
				//System.out.println(BirthPredictOfCX.get ( Nongchun ).get (Babies.getBabies ( year ) ) [ X ]);
			}
			if ( true )
			{
				BirthPredictOfCX.get ( Chengshi ).get ( Babies.getBabies ( year ) ) [ X ] = 
						( ziNv.get ( Chengshi ).get ( Nongye ).get ( DDB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Feinong ).get ( DDB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NMDFB ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NFDMB ) [ X ] 
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( NNB ) [ X ] )
						+ ( ziNv.get ( Chengshi ).get ( Nongye ).get ( SingleRls ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Nongye ).get ( ShuangFeiRls ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( SingleRls ) [ X ]
						+ ziNv.get ( Chengshi ).get ( Feinong ).get ( ShuangFeiRls ) [ X ] );
				//System.out.println(BirthPredictOfCX.get ( Chengshi ).get ( Babies.getBabies ( year ) ) [ X ]);done!
			}
			if ( true )
			{
				BirthPredictOfAll.get ( Babies.getBabies ( year ) ) [ X ] 
						= BirthPredictOfCX.get ( Nongchun ).get ( Babies.getBabies ( year ) ) [ X ]
						+ BirthPredictOfCX.get ( Chengshi ).get ( Babies.getBabies ( year ) ) [ X ];
				//System.out.println(BirthPredictOfAll.get ( Babies.getBabies ( year ) ) [ X ]);
			}
		}

		//System.out.println("End");
	}

	@Override
	public Message checkDatabase ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		return null;
	}

	@Override
	public void setParam ( String params , int type ) throws MethodException
	{
	}

}
