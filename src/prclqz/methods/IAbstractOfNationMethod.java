package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;

public interface IAbstractOfNationMethod extends IMethod
{
	void calculate(IDAO m, HashMap<String, Object> globals,
			IBabiesRateMethod bRate,
			INationalBornRateMethod nationBRate,
			IRecordNationalPolicyAndPolicyRateMethod recordPolicy,
			IDeathCalculatingMethod deathCal, 
			ICalculateExpectedAgeMethod calAge,
			IGroupingByAgeMethod groupAge,
			IResultOfAbstractMethod resultAbs,
			IMainExtremeValuesMethod mainVals )throws Exception;

}
