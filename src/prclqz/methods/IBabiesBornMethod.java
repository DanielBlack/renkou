package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;
/**
 * 二级接口
 *  在计算分婚配按政策生育的孩子数时特别设计的接口，用来实现多层调度
 * @author prclqz@zju.edu.cn
 *
 */
public interface IBabiesBornMethod extends IMethod {
	
	/**
	 * 调度二级子程序及计算
	 * @param m
	 * @param globals
	 * @param bornByDuiji
	 * @param bornByParentType
	 * @param babiesRate
	 * @param releaAClssfy
	 * @param classification
	 */
	void calculate(IDAO m, HashMap<String, Object> globals,
			IBabiesBornByParentTypeMethod bornByParentType,IBabiesBornByDuijiMethod bornByDuiji,  IBabiesRateMethod babiesRate,
			IBabiesReleaseClassifyMethod releaAClssfy, IBabiesBornByFutureMethod classification)throws Exception;
	
}
