package prclqz.methods;

import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.Const.*;

import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.Year;

/**
 * 各类堆积释放数及后代分类
 * @author prclqz@zju.edu.cn
 *
 */
public class MBabiesReleaseAndClassifyBabies implements IBabiesReleaseClassifyMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals,
			IMethod classification) throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		
		//define local variables
		int dqx = tempVar.getProvince();
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//provincMigMap.get(""+dqx);
		int X;
		int year = tempVar.getYear();
		BabiesBornBean bBornBean = tempVar.getBabiesBorn();
		Map<HunpeiField, double[]> zn = ziNv.get(tempVar.getCx()).get(tempVar.getNy());
		double marriageTFR = 0;
		Map<CX,Map<NY,Double>> singleChildCN = bBornBean.getSingleChildCN(),
							   singleRlsB2CN = bBornBean.getSingleRlsB2CN(),
							   feiRlsB2CN  = bBornBean.getFeiRlsB2CN(),
							   marriageTFRCN= bBornBean.getMarriageTFRCN(),
							   singleDJCN    = bBornBean.getSingleDJCN(),
							   shuangFeiDJCN = bBornBean.getShuangFeiDJCN();
		double sglDJ = 0,sfDJ = 0;
		Map<Year,Map<BabiesBorn,Double>> babiesBorn = (Map<Year, Map<BabiesBorn, Double>>) predictVarMap.get("BabiesBorn"+dqx);
		Map<BabiesBorn,Double> bBorn = babiesBorn.get(Year.getYear(year));
		
		//正式计算
		for(X = 0; X<= MAX_AGE-1 ;X++){
			double tmp = zn.get(DD)[X]+zn.get(NMDF)[X]+zn.get(NFDM)[X]+zn.get(NN)[X]+zn.get(SingleDJ)[X]+
						 zn.get(ShuangfeiDJ)[X]+zn.get(Yi2Single)[X]+zn.get(Yi2Shuangfei)[X];
			if(tmp !=0 )
			{
				marriageTFR += 
				 (zn.get(DDB)[X]+zn.get(NMDFB)[X]+zn.get(NFDMB)[X]+zn.get(NNB)[X]+zn.get(SingleRls)[X]+
						 zn.get(ShuangFeiRls)[X])/tmp;
			}
		}
		
		//记录堆积释放结果
		
		/**
		 * revised by Jack Long on 2012.08.27
		 */
		bBornBean.setMarriageTFR(marriageTFR);
		
		singleChildCN.get(tempVar.getCx()).put(tempVar.getNy(), 
				bBornBean.getDD1() + bBornBean.getNMDF1() + bBornBean.getNFDM1() + bBornBean.getNN1());
		
		singleRlsB2CN.get(tempVar.getCx()).put(tempVar.getNy(),bBornBean.getSglRlsB2());
		
		feiRlsB2CN.get(tempVar.getCx()).put(tempVar.getNy(),bBornBean.getFeiRlsB2());
		
		marriageTFRCN.get(tempVar.getCx()).put(tempVar.getNy(),bBornBean.getMarriageTFR());
		
		for(X = 15;X<=49;X++){
			sglDJ += zn.get(SingleDJ)[X];
			sfDJ  += zn.get(ShuangfeiDJ)[X]; 
		}
		singleDJCN.get(tempVar.getCx()).put(tempVar.getNy(), sglDJ);
		shuangFeiDJCN.get(tempVar.getCx()).put(tempVar.getNy(), sfDJ);
		
		// call level-3 procedure :按后代政策对出生孩子分类
		classification.calculate(m, globals);
		
		bBorn.put(BabiesBorn.getDDB1(tempVar.getCx(), tempVar.getNy()),bBornBean.getDD1());
		bBorn.put(BabiesBorn.getDDB2(tempVar.getCx(), tempVar.getNy()),bBornBean.getDD2());
		bBorn.put(BabiesBorn.getD_B1(tempVar.getCx(), tempVar.getNy()),bBornBean.getNMDF1()+bBornBean.getNFDM1());
		bBorn.put(BabiesBorn.getD_B2(tempVar.getCx(), tempVar.getNy()),bBornBean.getSingleB2());
		bBorn.put(BabiesBorn.getNNB1(tempVar.getCx(), tempVar.getNy()),bBornBean.getNN1());
		bBorn.put(BabiesBorn.getNNB2(tempVar.getCx(), tempVar.getNy()),bBornBean.getNN2());
		
//		System.out.println("hah");
	}

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
