package prclqz.methods;
import static prclqz.core.Const.MAX_AGE;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.lib.NumberTool;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.HunpeiField.*;

import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.SYSFMSField.*;
import prclqz.view.IView;
/**
 * 释放模式确定
 * @author prclqz@zju.edu.cn
 *
 *TODO:
 *NYTuShiAR ，NYHuanShiAR 等数组的翻译有误，但原程序亦有错误……
 */
public class MConfirmSFMS implements IMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		IView v = (IView) globals.get("view");
//		HashMap<String, Object> provincMigMap = (HashMap<String, Object>) globals.get("provinceMigMap");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//define local variables
		int dqx = tempVar.getProvince();
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//(Map<CX, Map<NY, Map<HunpeiField, double[]>>>) provincMigMap.get(""+dqx);
		Map<CX,Map<SYSFMSField,double[]>> shengYuMoShi = (Map<CX, Map<SYSFMSField, double[]>>) predictVarMap.get("ShengYuMoShi"+dqx);
		SYSFMSField sfms = SYSFMSField.getSFMSfromId(tempVar.getSFMS());
		int X;
		double[] NYTuShiAR,NYHuanShiAR,NYShiFangAR,FNTuShiAR,FNHuanShiAR,FNShiFangAR;
		NYTuShiAR = (double[]) predictVarMap.get("NYTuShiAR");
		NYHuanShiAR = (double[]) predictVarMap.get("NYHuanShiAR");
		NYShiFangAR = (double[]) predictVarMap.get("NYShiFangAR");
		FNTuShiAR = (double[]) predictVarMap.get("FNTuShiAR");
		FNHuanShiAR = (double[]) predictVarMap.get("FNHuanShiAR");
		FNShiFangAR = (double[]) predictVarMap.get("FNShiFangAR");
		
		// for debug
//		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
//		BornXbbBean dqXbb;
//		String dqBB;
//		dqXbb = xbbMap.get(""+dqx);
//		dqBB  = dqXbb.getDqName();
		
		// copy data to ziNvMap according to the SFMS
		switch(sfms){
		case fenshi:
		case yiyuan:
			for(X=0;X<MAX_AGE;X++)
			{
				//1.1
				ziNv.get(Nongchun).get(Nongye).get(duijiRate)[X] = shengYuMoShi.get(Nongchun).get(wait2chdrn)[X];
				//1.2
				if(X>=30 && X<=35)
					ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(huanshi)[X];
				else if(X>35)
					ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(tushi)[X];
				else
					ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(zhengchang)[X];
				//2.1
				ziNv.get(Nongchun).get(Feinong).get(duijiRate)[X] = shengYuMoShi.get(Chengshi).get(wait2chdrn)[X];
				//2.2
				if(X>=30 && X<=35)
					ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(huanshi)[X];
				else if(X>35)
					ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(tushi)[X];
				else
					ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(zhengchang)[X];
				//3.1
				ziNv.get(Chengshi).get(Nongye).get(duijiRate)[X] = shengYuMoShi.get(Nongchun).get(wait2chdrn)[X];
				//3.2
				if(X>=30 && X<=35)
					ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(huanshi)[X];
				else if(X>35)
					ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(tushi)[X];
				else
					ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(zhengchang)[X];
				//4.1
				ziNv.get(Chengshi).get(Feinong).get(duijiRate)[X] = shengYuMoShi.get(Chengshi).get(wait2chdrn)[X];
				//2.2
				if(X>=30 && X<=35)
					ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(huanshi)[X];
				else if(X>35)
					ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(tushi)[X];
				else
					ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(zhengchang)[X];
				
				//ARR
				if(shengYuMoShi.get(Nongchun).get(huanshi)[X] != 0)
					NYHuanShiAR[X] = shengYuMoShi.get(Nongchun).get(huanshi)[X];
				if(shengYuMoShi.get(Nongchun).get(tushi)[X] != 0)
					NYTuShiAR[X] = shengYuMoShi.get(Nongchun).get(tushi)[X];
				if(shengYuMoShi.get(Chengshi).get(huanshi)[X] != 0)
					FNHuanShiAR[X] = shengYuMoShi.get(Chengshi).get(huanshi)[X];
				if(shengYuMoShi.get(Chengshi).get(tushi)[X] != 0)
					FNTuShiAR[X] = shengYuMoShi.get(Chengshi).get(tushi)[X];
			}
			break;
		case huanshi:
		case tushi:
			for(X=0;X<MAX_AGE;X++){
				//1
				ziNv.get(Nongchun).get(Nongye).get(duijiRate)[X] = shengYuMoShi.get(Nongchun).get(wait2chdrn)[X];
				ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(sfms)[X];
				//2
				ziNv.get(Nongchun).get(Feinong).get(duijiRate)[X] = shengYuMoShi.get(Chengshi).get(wait2chdrn)[X];
				ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(sfms)[X];
				//3
				ziNv.get(Chengshi).get(Nongye).get(duijiRate)[X] = shengYuMoShi.get(Nongchun).get(wait2chdrn)[X];
				ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X] = shengYuMoShi.get(Nongchun).get(sfms)[X];
				//4
				ziNv.get(Chengshi).get(Feinong).get(duijiRate)[X] = shengYuMoShi.get(Chengshi).get(wait2chdrn)[X];
				ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X] = shengYuMoShi.get(Chengshi).get(sfms)[X];
				
				//ARR
				if(shengYuMoShi.get(Nongchun).get(sfms)[X] != 0)
					NYShiFangAR[X] = shengYuMoShi.get(Nongchun).get(sfms)[X];
				if(shengYuMoShi.get(Chengshi).get(sfms)[X] != 0)
					FNShiFangAR[X] = shengYuMoShi.get(Chengshi).get(sfms)[X];
			}
			break;
		case zhengchang:
			for(X=0;X<MAX_AGE;X++){
				//1
				ziNv.get(Nongchun).get(Nongye).get(duijiRate)[X] 	= 0;
				ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X] = 0;
				//2
				ziNv.get(Nongchun).get(Feinong).get(duijiRate)[X]	= 0;
				ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X]= 0;
				//3
				ziNv.get(Chengshi).get(Nongye).get(duijiRate)[X] 	= 0;
				ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X] = 0;
				//4
				ziNv.get(Chengshi).get(Feinong).get(duijiRate)[X] 	= 0;
				ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X]= 0;
				
				//ARR
				if(shengYuMoShi.get(Nongchun).get(sfms)[X] != 0)
					NYShiFangAR[X] = shengYuMoShi.get(Nongchun).get(sfms)[X];
				if(shengYuMoShi.get(Chengshi).get(sfms)[X] != 0)
					FNShiFangAR[X] = shengYuMoShi.get(Chengshi).get(sfms)[X];
			}
			break;
		default:
			throw new MethodException("Error Occur in MConfirm SFMS:没有对应的释放模式来计算");
		}
		
		//for debug
//		for(X=0;X<MAX_AGE;X++){
//			v.writelnToTmpFile(dqBB+ziNv.get(Nongchun).get(Nongye).get(duijiRate)[X]+"*"+
//					ziNv.get(Nongchun).get(Nongye).get(shifangMoshi)[X]+"*"+
//					ziNv.get(Nongchun).get(Feinong).get(duijiRate)[X]+"*"+
//					ziNv.get(Nongchun).get(Feinong).get(shifangMoshi)[X]+"*"+
//					ziNv.get(Chengshi).get(Nongye).get(duijiRate)[X]+"*"+
//					ziNv.get(Chengshi).get(Nongye).get(shifangMoshi)[X]+"*"+
//					ziNv.get(Chengshi).get(Feinong).get(duijiRate)[X]+"*"+
//					ziNv.get(Chengshi).get(Feinong).get(shifangMoshi)[X]+dqBB);
//			}
		
		//copy some data to ARRs
		//TODO
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {

	}

}
