package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;

/**
 *  三级接口
 *  在计算各类堆积释放数及后代分类时特别设计的接口，用来实现多层调度
 * @author prclqz@zju.edu.cn
 *
 */
public interface IBabiesReleaseClassifyMethod extends IMethod {

	void calculate(IDAO m, HashMap<String, Object> globals,
			IMethod classification)throws Exception;

}
