﻿package prclqz.methods;

import java.util.HashMap;
import java.util.Map;
import static prclqz.core.Const.*;
import static prclqz.core.enumLib.HunpeiField.*;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
/**
 * 各类夫妇分年龄生育孩子数
 * @author prclqz@zju.edu.cn
 * Revised by JackLong on Aug. 28th
 */
public class MBabiesBornByParentType implements IBabiesBornByParentTypeMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );
		Map<HunpeiField, double[]> zn = ziNv.get(tempVar.getCx()).get(tempVar.getNy());
//		Map<HunpeiField, double[]> zn = tempVar.getZn();
		
		//define local vairables
		int X;
		double DDFR1,NMDFFR1,NFDMFR1,NNFR1,
			   DDFR2,NMDFFR2,NFDMFR2,NNFR2;
		double A, DB, B2,B3 , B4;
		double DD1, NMDF1 , NFDM1, NN1,
			   DD2, NMDF2 , NFDM2, NN2;
		
		for(X = 0; X< MAX_AGE ; X++){
			/**
			 * Revised by JackLong on Aug. 28th
			 */
			zn.get(coupleNum)[X] = zn.get(DD)[X] + zn.get(NMDF)[X] + zn.get(NFDM)[X] + zn.get(SingleDJ)[X]+zn.get(NN)[X]
			                     +zn.get(SingleDJ)[X]+ zn.get(ShuangfeiDJ)[X] + zn.get(Yi2Single)[X] + zn.get(Yi2Shuangfei)[X];
		}
		
		DDFR1 = (tempVar.getDDFR() < 1 )? tempVar.getDDFR() : 1;
		NMDFFR1 = (tempVar.getNMDFR() < 1)? tempVar.getNMDFR() : 1;
		NFDMFR1 = (tempVar.getNFDMR() < 1)? tempVar.getNFDMR() : 1;
		NNFR1 = (tempVar.getNNR() < 1)? tempVar.getNNR() : 1;
		
		DDFR2 = (tempVar.getDDFR() < 1 )? 0 : tempVar.getDDFR()-1;
		NMDFFR2 = (tempVar.getNMDFR() < 1)? 0 :tempVar.getNMDFR()-1;
		NFDMFR2 = (tempVar.getNFDMR() < 1)? 0 :tempVar.getNFDMR()-1;
		NNFR2 = (tempVar.getNNR() < 1)? 0 :tempVar.getNNR()-1;
		
		for(X = 0; X< MAX_AGE ; X++){
			zn.get(DDB)[X] = zn.get(DD)[X] * DDFR1 * zn.get(FR1)[X] + zn.get(DD)[X] * DDFR2 * zn.get(FR2)[X];
			zn.get(NNB)[X] = zn.get(NN)[X] * NNFR1 * zn.get(FR1)[X] + zn.get(NN)[X] * NNFR2 * zn.get(FR2)[X];
			zn.get(NMDFB)[X] = zn.get(NMDF)[X] * NMDFFR1 * zn.get(FR1)[X] + zn.get(NMDF)[X] * NMDFFR2 * zn.get(FR2)[X];
			zn.get(NFDMB)[X] = zn.get(NFDM)[X] * NFDMFR1 * zn.get(FR1)[X] + zn.get(NFDM)[X] * NFDMFR2 * zn.get(FR2)[X];
			
		}
		
		A = DB = B2 = B3 = B4 = 0;
		DD1 = NMDF1 = NFDM1 = NN1 = 0;
		DD2 = NMDF2 = NFDM2 = NN2 = 0;
		for(X = 0; X< MAX_AGE ; X++){
			A += zn.get(FRD)[X];
			DB += zn.get(DDB)[X];
			B2 += zn.get(NMDFB)[X];
			B3 += zn.get(NFDMB)[X];
			B4 += zn.get(NNB)[X];
			//1
			DD1 += zn.get(DD)[X] * DDFR1 * zn.get(FR1)[X];
			NMDF1 += zn.get(NMDF)[X] * NMDFFR1 * zn.get(FR1)[X];
			NFDM1 += zn.get(NFDM)[X] * NFDMFR1 * zn.get(FR1)[X];
			NN1 += zn.get(NN)[X] * NNFR1 * zn.get(FR1)[X];
			//2
			DD2 += zn.get(DD)[X] * DDFR2 * zn.get(FR2)[X];
			NMDF2 += zn.get(NMDF)[X] * NMDFFR2 * zn.get(FR2)[X];
			NFDM2 += zn.get(NFDM)[X] * NFDMFR2 * zn.get(FR2)[X];
			NN2 += zn.get(NN)[X] * NNFR2 * zn.get(FR2)[X];
		}
		
		BabiesBornBean bbb = new BabiesBornBean(DDFR1, NMDFFR1, NFDMFR1, NNFR1, DDFR2, NMDFFR2, NFDMFR2, NNFR2, A, DB, B2, B3, B4, 
												DD1, NMDF1, NFDM1, NN1, DD2, NMDF2, NFDM2, NN2);
		tempVar.setBabiesBorn(bbb);
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
