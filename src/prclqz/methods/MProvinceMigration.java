package prclqz.methods;
import static prclqz.core.Const.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.XB.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.QYPHField.*;
import static prclqz.core.enumLib.QYPHTotal.*;

import static prclqz.lib.EnumMapTool.*;

import prclqz.core.enumLib.*;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.parambeans.ParamBean1;
import prclqz.parambeans.ParamBean3;
import prclqz.view.IView;

/**
 * Migration for every province
 * @author prclqz@zju.edu.cn
 * @time 2011-09-02 first edition
 * @time 2011-10-23 second edition: use the EnumMap instead of the Array
 * @time 2011-10-28 third edition:  Debug the wrong result and convert comments into English version
 */
public class MProvinceMigration implements IMethod {

	///define class member variables
	private ParamBean1 paramBean; 
	//a format tool
	private static DecimalFormat df5 = new DecimalFormat("####0.0000000000");
	
	/***
	 * the main function of the province migration . 
	 * For a Unit test, a globals with essential variables is required
	 * 新建：地区城镇(农村)农业(非农)子女婚配预测表——仿真结果_婚配预测表结构
	 * @throws Exception 
	 */
	@Override
	public void calculate(IDAO database,HashMap<String,Object> globals) throws Exception {
		
		///get essential variables from globals
		ParamBean3 pb3 = (ParamBean3) globals.get("SetUpEnvParamBean");
		HashMap<String,Integer> mmm = (HashMap<String,Integer>)globals.get("mainMeanMap");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		
		//define local variables
		Map<CX,Double> CXQB = createCXMap();
		BornXbbBean dqXbb;
		String dqB,dqBB;
		String diDai;
		int dqx;
		Map<CX,Map<XB,Double>> zongQianRate = createCXXBMapDouble();
		Map<CX,Map<XB,Double>> zongQianRateOfBeijing = createCXXBMapDouble();
		//whether 全国城市化为背景 or not
		int type; 
			ArrayList<String> cityArr = this.getNationCity();
		//[cshLevel——0:cl<80,1:80<cl<90,2:cl>=90,3:other situations][Province][Param——0:C总迁率M,1:X总迁率M,2:C总迁率F,3:X总迁率F]
		//——TODO to be migrated to databases 
		double[][][] editTable= new double[4][66][4];
			this.getEditTable(editTable, mmm);
		Map<CX,Map<XB,double[]>> jingQianProbability = createCXXBDoubleArrMap(MAX_AGE);
		Map<CX,Map<XB,double[]>> jingQianGLFB;
		Map<CX,Map<XB,Double>> SQY = createCXXBMapDouble();
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv;
		HashMap<String,Map<CX,Map<XB,double[]>>> jingQianGLFBBMap = new HashMap<String, Map<CX,Map<XB,double[]>>>();
		boolean debug = paramBean.getDebug() == 1;
		Map<CX,Double> QYB;
		Map<QYPHField,long[]> QYPH = (Map< QYPHField , long[] >) predictVarMap.get( "QYPH" );//createQYPHArrMap(MAX_AGE);
		Map<QYPHTotal,Long> qianyiTotal = new EnumMap<QYPHTotal, Long>(QYPHTotal.class);
		//save the province result to globals 
//		HashMap<String, Object> provinceMigMap=new HashMap<String, Object>();
		
		
		//TODO Next Optimization: fetch all essential data from databases only one time
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			//primary data of province migration
			dqXbb = xbbMap.get(""+dqx);
			dqBB  = dqXbb.getDqName();
			dqB   = dqBB.substring(0, 2);
			diDai = dqXbb.getDiDai();
			
			type = cityArr.contains(dqXbb.getDqName())?79:78;
			database.setZongQianRate(zongQianRate, dqB, tempVar.getYear(), type, task.getParam_id());
			
			if("北京".equals(dqB))
				copyCXXBMap(zongQianRate, zongQianRateOfBeijing);
			else if("上海".equals(dqB))
				copyCXXBMap(zongQianRateOfBeijing, zongQianRate);
			else if("广东".equals(dqB))
				getGuangDongZongQian(zongQianRate,tempVar.getYear(),type,task.getParam_id(),database);
			else if("宁夏".equals(dqB))
				getNingXiaZongQian(zongQianRate,tempVar.getYear(),type,task.getParam_id(),database);
				
			//the ZongQianRate must be multiplied to the according edition parameters 
			this.calculateZongQianRate(zongQianRate, editTable, dqx, tempVar);
			//get the CXQB -- Cheng Xiang Qian Yi Xiu Zheng Bi
			 database.setDiQuQianYiXiuZheng(CXQB ,dqx, task.getParam_id());
			
			//1st Step Of Computation：calculate the qianyiProbalility of every 城镇、农村、M、F 
			//1.1 get the Qian Yi Gai Lv Fen Bu
			if(!jingQianGLFBBMap.containsKey(diDai)){ 
				jingQianGLFB = createCXXBDoubleArrMap(MAX_AGE);
				database.setJingQianGLFB( jingQianGLFB,mmm.get(diDai+"-省份/地区"),2000,task.getParam_id() );
				jingQianGLFBBMap.put(diDai, jingQianGLFB);
			}
			else{
				// using a cache
				jingQianGLFB = jingQianGLFBBMap.get(diDai);
			}
			
			//1.2 calculate the Jing Qian Probability and the Sum of it
			//    from zongQianRate , CXQB and jingQianGLFB 
			int X;
			double jqp,sumJqp;
			for(XB xb: XB.values())
				for(CX cx : CX.values())
				{
					sumJqp = 0;
					for(X=0;X<MAX_AGE;X++)
					{
						jqp = jingQianGLFB.get(cx).get(xb)[X] * zongQianRate.get(cx).get(xb) * CXQB.get(cx);
						
						// for debug 
//						if(X<=90)
//						v.writelnToTmpFile(dqBB+":^   "+df5.format(jqp)+"^   "+
//								df5.format(jingQianGLFB.get(cx).get(xb)[X])+"^   "+
//								df5.format(zongQianRate.get(cx).get(xb))+"^   "+df5.format(CXQB.get(cx))+"^");
						
						jingQianProbability.get(cx).get(xb)[X] = jqp;
						sumJqp += jqp;
					}
					SQY.get(cx).put(xb, sumJqp);
				}
			
			
			//2nd Step of computation
			//2.1 get the QYB---Qian Yi Bi
			QYB = createCXMap();
			if( (!dqB.equals("西藏")) && (tempVar.getQY()>=1) ){
				double qyxz[];
				qyxz = database.getQianYiXiuZheng(dqx, task.getParam_id(), mmm.get("0428-各地区城乡迁移修正系数参数"));
				
				double qyy;
				switch(tempVar.getQY()){
				case 1:
					qyy = 0.8; 	break;
				case 2:
					qyy = 1; 	break;
				default:
					qyy = 1.2; 	break;
				}
				QYB.put(Chengshi, qyxz[0]*qyy/qyxz[2]);
				QYB.put(Nongchun, qyxz[1]*qyy/qyxz[2]);
			}else{
				QYB.put(Chengshi, 0.0);
				QYB.put(Nongchun, 0.0);
			} 
			
			//for debug
//			v.writelnToTmpFile("省份:"+dqBB);
			
			//2.2 Formal Computation  - Loop With Chengshi Nongchun Nongye Feinong
			ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//creatCXXBZiNv(MAX_AGE);
			for(CX cx: CX.values())
				for(NY ny:NY.values())
					for(HunpeiField zf : HunpeiField.valuesOfZinv2())
					{
						double qygl,jqygl;
						Double qyCount;
						long qyInt;
						double tmpQYB;
						
						//for debug
//						v.writelnToTmpFile("         "+(cx.ordinal()+1)+"*"+"         "+(ny.ordinal()+1)+"地区"+cx.getChinese()+ny.getChinese()+"子女"+zf.toString());
						
						for(X=0;X<MAX_AGE;X++)
						{
							jqygl 	= jingQianProbability.get(cx).get(HunpeiField.getXB(zf))[X];
							tmpQYB  = (QYB.get(cx)<0 && SQY.get(cx).get(HunpeiField.getXB(zf)) < 0)? -QYB.get(cx) :QYB.get(cx); 
							qygl  	= tmpQYB*jqygl > 1? 1 : tmpQYB*jqygl;
							qyCount = qygl * ziNv.get(cx).get(ny).get(zf)[X];
							qyInt	= (long)Math.round(qyCount);
							
							//for debug
//							v.writelnToTmpFile(df5.format(qyInt)+"^");
							
							ziNv.get(cx).get(ny).get(HunpeiField.getQYField(zf))[X] = qyInt;
							
							if(qyInt > 0 )
							{
								//Immigration
								QYPH.get(HunpeiField.getQYPHField(zf,0))[X] += qyInt ;
							}else
							{
								//Emigration
								QYPH.get(HunpeiField.getQYPHField(zf,1))[X] += qyInt ;
							}
						}
						
						//for debug
//						v.writelnToTmpFile("");
						
					}
			//save the result of this province into the provinces result map
//			provinceMigMap.put(""+dqx, ziNv);
		}
		
		
		// 3th Step of Computation: Prepare something for the national Migration Balancing
		long tImSumM0=0,tEmSumM0=0,tImSumF0=0,tEmSumF0=0,tQYPHSumM=0,tQYPHSumF=0;
		for(int X=0; X<MAX_AGE;X++){
			QYPH.get(MBalance)[X] = (QYPH.get(MImmigrate)[X]+ Math.abs(QYPH.get(MEmigrate)[X]) )/2;
			QYPH.get(FBalance)[X] = (QYPH.get(FImmigrate)[X]+ Math.abs(QYPH.get(FEmigrate)[X]) )/2;
			
			tImSumM0  +=  QYPH.get(MImmigrate)[X];
			tEmSumM0  +=  QYPH.get(MEmigrate)[X];
			tImSumF0  +=  QYPH.get(FImmigrate)[X];
			tEmSumF0  +=  QYPH.get(FEmigrate)[X];
			tQYPHSumM +=  QYPH.get(MBalance)[X];
			tQYPHSumF +=  QYPH.get(FBalance)[X];
		}
		qianyiTotal.put(ImSumM0,tImSumM0);
		qianyiTotal.put(EmSumM0,tEmSumM0);
		qianyiTotal.put(ImSumF0,tImSumF0);
		qianyiTotal.put(EmSumF0,tEmSumF0);
		qianyiTotal.put(QYPHSumM,tQYPHSumM);
		qianyiTotal.put(QYPHSumF,tQYPHSumF);
		
		//Save to province map as the national key--""+0 
//		provinceMigMap.put("0", qianyiTotal);
		predictVarMap.put("qianyiTotal", qianyiTotal);
		//save the province result map to the globals
//		globals.put("provinceMigMap", provinceMigMap);
		//save the result of national migration balancing to databases
		// TODO 改到savingdata 
//		database.insertNationQY(QYPH,task.getId());
		
		//for debug
//		v.saveTmpFile();
//		System.out.println("over!");
//		System.exit(0);
		
	}


	
	private void getNingXiaZongQian(Map<CX, Map<XB, Double>> zongQianRate, int year, int type,
			int paramId, IDAO m) throws SQLException {
		m.setZongQianRate(zongQianRate, "宁夏", year, type, paramId);
		double cm = zongQianRate.get(Nongchun).get(Male);
		zongQianRate.get(Chengshi).put(Female, -cm);
		zongQianRate.get(Chengshi).put(Male, -cm);
		zongQianRate.get(Nongchun).put(Female, cm);
	}



	private void getGuangDongZongQian(Map<CX, Map<XB, Double>> zongQianRate, int year,
			int type, int paramId, IDAO m) throws SQLException {
		Map<CX, Map<XB, Double>> zj=createCXXBMapDouble(),js = createCXXBMapDouble();
		m.setZongQianRate(zj, "浙江", year, type, paramId);
		m.setZongQianRate(js, "江苏", year, type, paramId);
		
		for(CX cx: CX.values())
			for(XB xb: XB.values())
				zongQianRate.get(cx).put(xb, ( zj.get(cx).get(xb)+js.get(cx).get(xb)) /2.0 );
	}


	/**
	 * 
	 * @param zongQianRate
	 * @param editTable [csh水平——0:csh水平<80,1:80<csh水平<90,2:csh水平>=90,3:其他情况][省份][参数——0:C总迁率M,1:X总迁率M,2:C总迁率F,3:X总迁率F]
	 * @param dqx
	 * @param tempVar
	 */
	private void calculateZongQianRate(Map<CX, Map<XB, Double>> zongQianRate,double[][][] editTable,
			int dqx,TempVariablesBean tempVar)
	{
		int type;
		if(tempVar.getCshLevel()<80){ // case 1
			type = 0;
				
		}else if(tempVar.getCshLevel()>80 && tempVar.getCshLevel()<90){ //case2
			type = 1;
		}else if(tempVar.getCshLevel()>90){//case3
			type = 2;
		}else{//case4
			type = 3;
		}
		
		int param;
		for(CX cx:CX.values())	
			for(XB xb: XB.values()){
				param = cx.getEditableNum()+xb.getEditableNum()*2;
//				System.out.println(type+" "+dqx+" "+param+" "+zongQianRate.get(cx).get(xb));
				double newzqr = zongQianRate.get(cx).get(xb) * editTable[type][dqx][param];
				zongQianRate.get(cx).put(xb,newzqr);
			}
	}
	
	/**
	 * 是否要以城市化为背景表——TODO 可迁移到数据库中
	 * @return
	 */
	private ArrayList<String> getNationCity(){
		ArrayList<String> tmpArr = new ArrayList<String>(); 
		tmpArr.add("北京");
		tmpArr.add("辽宁");
		tmpArr.add("上海");
		tmpArr.add("浙江");
		tmpArr.add("广东");
//		tmpArr.add("");
		return tmpArr;
	}
	/**
	 * 参数修正表——TODO 可迁移到数据库中  
	 * [csh水平——0:csh水平<80,1:80<csh水平<90,2:csh水平>=90,3:其他情况][省份][参数——0:C总迁率M,1:X总迁率M,2:C总迁率F,3:X总迁率F]
	 * @param editTable
	 * @param mmm
	 */
	private void getEditTable(double[][][] editTable,HashMap<String,Integer> mmm)
	{
		int i,j,k;
		for(i=0;i<4;i++)
			for(j=11;j<66;j++)
				for(k=0;k<4;k++)
				{
					switch(i){
					case 0:
						editTable[i][j][k] = 1;
						break;
					case 2:
					case 3:
					case 1:
						editTable[i][j][k] = 0;
						break;
						
					}
					
				}
		//i=0 csh水平<80 
		editTable[0][mmm.get("黑龙江-省份/地区")][1]=0.81545;
		editTable[0][mmm.get("黑龙江-省份/地区")][3]=0.81545;
		editTable[0][mmm.get("河北-省份/地区")][1]=1.115;
		editTable[0][mmm.get("河北-省份/地区")][3]=1.115;
		editTable[0][mmm.get("江苏-省份/地区")][0]=0.25;
		editTable[0][mmm.get("江苏-省份/地区")][1]=0.25;
		editTable[0][mmm.get("江苏-省份/地区")][2]=0.25;
		editTable[0][mmm.get("江苏-省份/地区")][3]=0.25;
		editTable[0][mmm.get("重庆-省份/地区")][1]=0.782;
		editTable[0][mmm.get("重庆-省份/地区")][3]=0.782;
		
		editTable[0][mmm.get("江西-省份/地区")][0]=1.0/1.5;  
		editTable[0][mmm.get("江西-省份/地区")][2]=1.0/1.5;
		
		editTable[0][mmm.get("山东-省份/地区")][1]=2*0.606;
		editTable[0][mmm.get("山东-省份/地区")][3]=2*0.606;
		//editTable[0][mmm.get("广东-省份/地区")][3]=0.;--有特例
		
		
		editTable[0][mmm.get("海南-省份/地区")][0]=1.4*1.058;
		editTable[0][mmm.get("海南-省份/地区")][2]=1.4*1.058;
		
		editTable[0][mmm.get("青海-省份/地区")][0]=2.059;
		editTable[0][mmm.get("青海-省份/地区")][2]=2.059;
		///////////////////////////宁夏也是特殊情况！！！！！！！！！！
		
		
		//i=1 csh水平>80 and <90
		for(i=0;i<4;i++)
			editTable[1][mmm.get("北京-省份/地区")][i]=1;
		for(i=0;i<4;i++)
			editTable[1][mmm.get("辽宁-省份/地区")][i]=1;
		for(i=0;i<4;i++)
			editTable[1][mmm.get("上海-省份/地区")][i]=1;
		for(i=0;i<4;i++)
			editTable[1][mmm.get("浙江-省份/地区")][i]=1;
		for(i=0;i<4;i++)
			editTable[1][mmm.get("广东-省份/地区")][i]=1;
		for(i=0;i<4;i++)
			editTable[1][mmm.get("天津-省份/地区")][i]=1;
		
		//i=2 csh水平>=90
		editTable[2][mmm.get("北京-省份/地区")][0]=1;
		editTable[2][mmm.get("辽宁-省份/地区")][0]=1;
		editTable[2][mmm.get("上海-省份/地区")][0]=1;
		editTable[2][mmm.get("浙江-省份/地区")][0]=1;
		editTable[2][mmm.get("广东-省份/地区")][0]=1;
		editTable[2][mmm.get("天津-省份/地区")][0]=1;
		editTable[2][mmm.get("北京-省份/地区")][2]=1;
		editTable[2][mmm.get("辽宁-省份/地区")][2]=1;
		editTable[2][mmm.get("上海-省份/地区")][2]=1;
		editTable[2][mmm.get("浙江-省份/地区")][2]=1;
		editTable[2][mmm.get("广东-省份/地区")][2]=1;
		editTable[2][mmm.get("天津-省份/地区")][2]=1;
		//i=3 特例：都为0
		//editTable[0][mmm.get("-省份/地区")][3]=0.;
	}
	

	@Override
	public void setParam(String params, int type) throws MethodException {
		JSONObject jsonObject = JSONObject.fromObject(params);    
		paramBean = (ParamBean1) JSONObject.toBean( jsonObject, ParamBean1.class ); 
//		System.out.println("debug:"+paramBean.getDebug());
//		System.exit(0);
	}


	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO 检查是否数据完善，是否有重复冲突的数据
		return null;
	}
	
}

