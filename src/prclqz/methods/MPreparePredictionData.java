package prclqz.methods;

import static prclqz.lib.EnumMapTool.*;
import static prclqz.core.Const.*;

import java.util.HashMap;
import java.util.Map;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.StringList;
import prclqz.core.enumLib.Abstract;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.BabiesBorn;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.Couple;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.HunpeiGL;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.QYPHField;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.SumBM;
import prclqz.core.enumLib.Summary;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;
import prclqz.lib.EnumMapTool;
import prclqz.parambeans.ParamBean3;
/**
 * 全国预测数据表 和 地区基础数据和预测文件 
 * 2.设计上本代码应该放在 MSetUpEnv 中，但是怕会和一些程序有冲突——
 *   因为和Foxpro程序中的位置差别太大，故没有那么做
 * 3.延迟翻译许多表的导入部分——append from。。。。——希望能找个人帮忙写这个
 * 4.有不少可以精简代码的地方，比如根据单、全9、半5等来区分，封装一些重复的代码——将来有时间再做
 * 
 * @time   2011-11-06 first edition
 * @author prclqz@zju.edu.cn
 *
 */
public class MPreparePredictionData implements IMethod {

	@Override
	public void calculate(IDAO database, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		ParamBean3 pb3 = (ParamBean3) globals.get("SetUpEnvParamBean");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
//		HashMap<String, Object> provinceMigMap = (HashMap<String, Object>) globals.get("provinceMigMap");
		int year = tempVar.getYear();
		
		//define variables
		int dqx;
		HashMap<String,Object> predictVarMap = new HashMap<String, Object>();
		globals.put("predictVarMap", predictVarMap);
		
		/**********************************
		 * 变量声明准备					  *
		 **********************************/
		/***********
		 * 获取	   *
		 ***********/
		//终身政策生育率
		HashMap<String,Double> presentBirthPolicy = new HashMap<String, Double>();
		//需夫模式
		double[][] husbandRate = new double[111][111];
		//农业(非农)突(缓\释)放模式AR
		double[] NYTuShiAR,NYHuanShiAR,NYShiFangAR,FNTuShiAR,FNHuanShiAR,FNShiFangAR;
		//想生(突释\缓释\晚释)二胎比
		Map<String,Map<SYSFMSField,double[]>> birthWill;
		
		//生育及释放模式
		Map<CX,Map<SYSFMSField,double[]>> shengYuMoShi;
		//死亡概率预测
		Map<CX,Map<XB,double[]>> deathRate;
		/***********
		 * 新建	   *
		 ***********/
		//地区生育率与迁移摘要				
		Map<Year,Map<SumBM,Double>> SummaryOfBirthAndMigration; 
		
		//生育孩次数
		Map<Year,Map<BabiesBorn,Double>> BabiesBorn;
//		PopulationPredictOf[All,CX,NY,CXNY]
		//分龄人口预测,丧子人口预测,死亡,特扶,一孩，迁移
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> PopulationPredictOfCXNY,SonDiePopulationPredictOfTeFuCXNY,SonDiePopulationPredictOfYiHaiCXNY;
		Map<CX,Map<Year,Map<XB,double[]>>>PopulationPredictOfCX,PopulationMigrationOfCX,PopulationDeathOfCX,SonDiePopulationPredictOfTeFuCX,SonDiePopulationPredictOfYiHaiCX;
		Map<NY,Map<Year,Map<XB,double[]>>> PopulationPredictOfNY,SonDiePopulationPredictOfTeFuNY,SonDiePopulationPredictOfYiHaiNY;
		Map<Year,Map<XB,double[]>> PopulationPredictOfAll,PopulationMigrationOfAll,PopulationDeathOfAll,SonDiePopulationPredictOfTeFuAll,SonDiePopulationPredictOfYiHaiAll;
		
		//分年龄生育预测(生育，超生，政策)
		Map<CX,Map<Babies,double[]>> BirthPredictOfCX,OverBirthPredictOfCX;
		Map<NY,Map<Babies,double[]>> BirthPredictOfNY,OverBirthPredictOfNY;
		Map<Babies,double[]> PolicyBabies,BirthPredictOfAll,OverBirthPredictOfAll;
		
		
		//夫妇及子女表结构
		Map<CX,Map<NY,Map<Year,Map<Couple,Double>>>> CoupleAndChildrenOfCXNY;
		Map<CX,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfCX;
		Map<NY,Map<Year,Map<Couple,Double>>> CoupleAndChildrenOfNY;
		Map<Year,Map<Couple,Double>> CoupleAndChildrenOfAll;
		
		//妇女分年龄生育率预测
		Map<Year,double[]> BirthRatePredict;
		
		//仿真结果_概要
		Map<Year,Map<Summary,Double >> SummaryOfAll;
		Map<CX,Map<Year,Map<Summary,Double >>> SummaryOfCX;
		Map<NY,Map<Year,Map<Summary,Double >>> SummaryOfNY;
		
		//政策及政策生育率					
		Map<Year,Map<PolicyBirth,Double>> PolicyBirthRate;
		
		//婚配概率							
		Map<Year,Map<HunpeiGL,Double>>MarriageRate;
		
		//婚配预测表结构	 
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> HunpeiOfCXNY;
		Map<CX,Map<HunpeiField,double[]>> HunpeiOfCX;
		Map<NY,Map<HunpeiField,double[]>> HunpeiOfNY;
		Map<HunpeiField,double[]> HunpeiOfAll;
		
		//生育政策模拟摘要
		Map<Abstract,Double> PolicySimulationAbstract;
		
		//全国迁移平衡
		Map<QYPHField,long[]> QYPH;
		/**
		 * ------------------------------------新建并从数据库获取数据--------------------------------------------------------------------------
		 * TODO mark0
		 * 新建并获取：终身政策生育率								——基本参数_各地区终身政策生育率 批次id=1		*PresentBirthPolicy 
		 * 新建并获取：需夫模式									——基本参数_全国按妻子年龄分的丈夫年龄分布模式	*HusbandRate
		 * 新建：AR农业缓释模式，AR农业突释模式，AR非农缓释模式，AR非农突释模式，AR农业释放模式，AR非农释放模式	*NYTuShiAR..............
		 * 新建并获取：想生(突释\缓释\晚释)二胎比					——基本参数_生育意愿 参数：bt参数，批次id=1		*BirthWill
		 * 
		 * 
		 * ------------------------------------新建--------------------------------------------------------------------------------------------
		 * 
		 * 新建：全国生育孩次数									——仿真结果_分婚配模式生育孩次数				*BabiesBorn
		 * 
		 * 新建：全国[全9]分龄人口预测							——仿真结果_分年龄人口预测(预测)				*PopulationPredictOf[All,CX,NY,CXNY]
		 * 新建：全国[城镇,农村,合计]分龄人口迁移					——仿真结果_分年龄人口预测(迁移)				*PopulationMigrationOf[All,CX]
		 * 新建：全国[城镇,农村,合计]分龄人口死亡					——仿真结果_分年龄人口预测(死亡)				*PopulationDeathOf[All,CX]
		 * 新建：全国[全9]分年龄人口预测							——仿真结果_分年龄人口预测	(特扶)				*SonDiePopulationPredictOfTeFu[All,CX,NY,CXNY]
		 * 新建：全国[全9]分年龄人口预测							——仿真结果_分年龄人口预测	(一孩)				*SonDiePopulationPredictOfYiHai[All,CX,NY,CXNY]
		 * 
		 * 新建：全国[半5]分年龄生育预测							——仿真结果_分年龄生育预测(生育)				*BirthPredict[All,CX,NY]
		 * 新建：全国[半5]分年龄超生预测							——仿真结果_分年龄生育预测(超生)				*OverBirthPredict[All,CX,NY]
		 * 新建：全国分年龄政策生育 								——仿真结果_分年龄生育预测(政策)				*PolicyBabies
		 * 
		 * 新建：全国[全9]夫妇及子女表结构						——仿真结果_夫妇及子女表结构					*CoupleAndChildrenOf[All,CX,NY,CXNY]
		 * 
		 * 新建：全国分年龄超生生育率								——仿真结果_妇女分年龄生育率预测				*BirthRatePredict
		 * 
		 * 新建：全国[半5]概要									——仿真结果_概要								+*SummaryOf[All,CX,NY]
		 * 
		 * 新建：全国政策生育率									——仿真结果_政策及政策生育率					+*PolicyBirthRate
		 * 
		 * 新建：全国婚配概率										——仿真结果_婚配概率							*MarriageRate
		 * 
		 * 新建：全国[全9]婚配预测表结构							——仿真结果_婚配预测表结构						*HunpeiOf[All,CX,NY,CXNY]
		 * 
		 * 新建：字符串值对应表																	！保存在 globals 中！strValues
		 * 
		 * 新建：生育政策模拟摘要									——仿真结果_生育政策模拟摘要					*PolicySimulationAbstract
		 * 
		 * 新建：全国迁移平衡										——仿真结果_全国迁移平衡表结构		！tempVar.getQY() > 0 才有*(在provinceMigMap和predictVarMap中都有)，QYPH
		 * -------------------------------------------------------------------------------------------------------------------------------------					
		 */
		

		/***********
		 * 获取	   *
		 ***********/
		////// 获取全国数据并保存到全局变量！！！！！！predictVarMap
		//终身政策生育率
		database.setPresentBirthPolicy(presentBirthPolicy,task.getParam_id());
		predictVarMap.put("PresentBirthPolicy", presentBirthPolicy);
		//需夫模式
		database.setHusbandRate(husbandRate,task.getParam_id());
		predictVarMap.put("HusbandRate", husbandRate);
		//农业(非农)突(缓\释)放模式AR
		NYTuShiAR   = new double[MAX_AGE];
		NYHuanShiAR = new double[MAX_AGE];
		NYShiFangAR = new double[MAX_AGE];
		FNTuShiAR   = new double[MAX_AGE];
		FNHuanShiAR = new double[MAX_AGE];
		FNShiFangAR = new double[MAX_AGE];
		predictVarMap.put("NYTuShiAR", NYTuShiAR);
		predictVarMap.put("NYHuanShiAR", NYHuanShiAR);
		predictVarMap.put("NYShiFangAR", NYShiFangAR);
		predictVarMap.put("FNTuShiAR", FNTuShiAR);
		predictVarMap.put("FNHuanShiAR", FNHuanShiAR);
		predictVarMap.put("FNShiFangAR", FNShiFangAR);
		//想生(突释\缓释\晚释)二胎比 
		birthWill = createBirthWill(MAX_AGE);
		database.setBirthWill(birthWill, task.getData_id());
		predictVarMap.put("BirthWill", birthWill);
		/***********
		 * 新建	   *
		 ***********/
		////// 新建全国数据并保存到全局变量！！！！！！predictVarMap TODO mark
		
		//全国生育孩次数
		BabiesBorn = createBabiesBorn();
		predictVarMap.put("BabiesBorn", BabiesBorn);
		//分龄人口预测,,死亡,特扶,一孩
		PopulationPredictOfCXNY = createCXNYYearXBDoubleArrMap ( MAX_AGE );
		SonDiePopulationPredictOfTeFuCXNY = createCXNYYearXBDoubleArrMap ( MAX_AGE );
		SonDiePopulationPredictOfYiHaiCXNY = createCXNYYearXBDoubleArrMap ( MAX_AGE );
		
		PopulationPredictOfCX = createCXYearXBDoubleArrMap(MAX_AGE);
		PopulationMigrationOfCX = createCXYearXBDoubleArrMap(MAX_AGE);
		PopulationDeathOfCX = createCXYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfTeFuCX = createCXYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfYiHaiCX = createCXYearXBDoubleArrMap(MAX_AGE);
		
		PopulationPredictOfNY = createNYYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfTeFuNY = createNYYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfYiHaiNY = createNYYearXBDoubleArrMap(MAX_AGE);
		
		PopulationPredictOfAll = createYearXBDoubleArrMap(MAX_AGE);
		PopulationMigrationOfAll = createYearXBDoubleArrMap(MAX_AGE);
		PopulationDeathOfAll = createYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfTeFuAll = createYearXBDoubleArrMap(MAX_AGE);
		SonDiePopulationPredictOfYiHaiAll = createYearXBDoubleArrMap(MAX_AGE);
		
//		PopulationPredictOfCXNY.get ( Chengshi ).get(Nongye).get(Year.getYear ( year )).get(F)
		predictVarMap.put ( "PopulationPredictOfCXNY" , PopulationPredictOfCXNY );
		predictVarMap.put ( "SonDiePopulationPredictOfTeFuCXNY" , SonDiePopulationPredictOfTeFuCXNY );
		predictVarMap.put ( "SonDiePopulationPredictOfYiHaiCXNY" , SonDiePopulationPredictOfYiHaiCXNY );
		predictVarMap.put ( "PopulationPredictOfCX" , PopulationPredictOfCX );
		predictVarMap.put ( "PopulationMigrationOfCX" , PopulationMigrationOfCX );
		predictVarMap.put ( "PopulationDeathOfCX" , PopulationDeathOfCX );
		predictVarMap.put ( "SonDiePopulationPredictOfTeFuCX" , SonDiePopulationPredictOfTeFuCX );
		predictVarMap.put ( "SonDiePopulationPredictOfYiHaiCX" , SonDiePopulationPredictOfYiHaiCX );
		predictVarMap.put ( "PopulationPredictOfNY" , PopulationPredictOfNY );
		predictVarMap.put ( "SonDiePopulationPredictOfTeFuNY" , SonDiePopulationPredictOfTeFuNY );
		predictVarMap.put ( "SonDiePopulationPredictOfYiHaiNY" , SonDiePopulationPredictOfYiHaiNY );
		predictVarMap.put ( "PopulationPredictOfAll" , PopulationPredictOfAll );
		predictVarMap.put ( "PopulationMigrationOfAll" , PopulationMigrationOfAll );
		predictVarMap.put ( "PopulationDeathOfAll" , PopulationDeathOfAll );
		predictVarMap.put ( "SonDiePopulationPredictOfTeFuAll" , SonDiePopulationPredictOfTeFuAll );
		predictVarMap.put ( "SonDiePopulationPredictOfYiHaiAll" , SonDiePopulationPredictOfYiHaiAll );
		
		//分年龄生育预测(生育，超生，政策)
		BirthPredictOfCX = createCXBabiesDoubleArrMap ( MAX_AGE );
		OverBirthPredictOfCX = createCXBabiesDoubleArrMap ( MAX_AGE );
		
		BirthPredictOfNY = createNYBabiesDoubleArrMap ( MAX_AGE );
		OverBirthPredictOfNY = createNYBabiesDoubleArrMap ( MAX_AGE );
		
		PolicyBabies = createBabiesDoubleArrMap ( MAX_AGE );
		BirthPredictOfAll = createBabiesDoubleArrMap ( MAX_AGE );
		OverBirthPredictOfAll = createBabiesDoubleArrMap ( MAX_AGE );
		
		predictVarMap.put ( "BirthPredictOfCX" , BirthPredictOfCX );
		predictVarMap.put ( "OverBirthPredictOfCX" , OverBirthPredictOfCX );
		predictVarMap.put ( "BirthPredictOfNY" , BirthPredictOfNY );
		predictVarMap.put ( "OverBirthPredictOfNY" ,  OverBirthPredictOfNY);
		predictVarMap.put ( "PolicyBabies" , PolicyBabies );
		predictVarMap.put ( "BirthPredictOfAll" ,BirthPredictOfAll  );
		predictVarMap.put ( "OverBirthPredictOfAll" , OverBirthPredictOfAll );
		
		//夫妇及子女表结构
		CoupleAndChildrenOfCXNY = createCXNYCouple ( );
		CoupleAndChildrenOfCX = createNYCouple ( );
		CoupleAndChildrenOfNY = createCXCouple ( );
		CoupleAndChildrenOfAll = createCouple ( );
		predictVarMap.put ( "CoupleAndChildrenOfCXNY" , CoupleAndChildrenOfCXNY );
		predictVarMap.put ( "CoupleAndChildrenOfCX" , CoupleAndChildrenOfCX );
		predictVarMap.put ( "CoupleAndChildrenOfNY" , CoupleAndChildrenOfNY );
		predictVarMap.put ( "CoupleAndChildrenOfAll" , CoupleAndChildrenOfAll );
		
		//妇女分年龄生育率预测
		BirthRatePredict = createYearDoubleArrMap ( MAX_AGE );
		predictVarMap.put ( "BirthRatePredict" , BirthRatePredict );

		//仿真结果_概要
		SummaryOfAll = createSummary ( );
		SummaryOfCX = createCXSummary ( );
		SummaryOfNY = createNYSummary ( );
		predictVarMap.put ( "SummaryOfAll" , SummaryOfAll );
		predictVarMap.put ( "SummaryOfCX" , SummaryOfCX );
		predictVarMap.put ( "SummaryOfNY" , SummaryOfNY );
		
		//政策及政策生育率					
		PolicyBirthRate = createPolicyBirth ( );
		predictVarMap.put ( "PolicyBirthRate" , PolicyBirthRate );
		
		//婚配概率							
		MarriageRate = createHunpeiGL ( );
		predictVarMap.put ( "MarriageRate" ,MarriageRate  );

		//婚配预测表结构	 
		HunpeiOfCXNY = creatCXNYZiNv ( MAX_AGE );
		HunpeiOfCX = creatCXZiNv ( MAX_AGE ); 
		HunpeiOfNY = creatNYFNZiNv ( MAX_AGE );
		HunpeiOfAll = creatAllZiNv ( MAX_AGE );		
//		provinceMigMap.put ( "" , HunpeiOfCXNY );
//		provinceMigMap.put ( "CX" , HunpeiOfCX );
//		provinceMigMap.put ( "NY" , HunpeiOfNY );
//		provinceMigMap.put ( "All" ,HunpeiOfAll  );
			//双重映射，以保持向上兼容 
		predictVarMap.put ( "HunpeiOfCXNY" , HunpeiOfCXNY );
		predictVarMap.put ( "HunpeiOfCX" , HunpeiOfCX );
		predictVarMap.put ( "HunpeiOfNY" , HunpeiOfNY );
		predictVarMap.put ( "HunpeiOfAll" ,HunpeiOfAll  );
		
		
		
		//新增 strValues
		StringList strValues = new StringList();
		globals.put ( "strValues" , strValues );
		
		// 生育政策模拟摘要
		PolicySimulationAbstract = createAbstract();
		predictVarMap.put( "PolicySimulationAbstract" , PolicySimulationAbstract );
		
		//全国迁移平衡——双重映射
//		QYPH= (Map< QYPHField , long[] >) provinceMigMap.get( "QYPH" );
		QYPH = createQYPHArrMap( MAX_AGE );
		predictVarMap.put( "QYPH" , QYPH );
		
		
//		predictVarMap.put ( "" ,  );
		//TODO mark1
		
		/********************************************************分省数据开始**** TODO ****************************************************************/
		
		
		
		/**
		 * ------------------------------------新建并从数据库获取数据--------------------------------------------------------------------------
		 * 
		 * 获取：农村(城镇)生育模式								——基本参数_生育及释放模式						*"ShengYuMoShi"+dqx
		 * 获取：城镇(乡村)死亡概率M(F).Q-ND0						——基本参数_死亡概率预测 tempVar.getYear()		*"DeathRate"+dqx
		 * 获取：地区[城市,农村]分年龄人口预测			 		——仿真结果_分年龄人口预测	(预测)				*"PopulationPredictOfCX"+dqx
		 * 
		 * 获取：土地面积，耕地06，水资源均量 保存到 tempVar
		 * 
		 * 获取：地区城镇(农村)农业(非农)子女婚配预测表			——仿真结果_婚配预测表结构						* HunpeiOfCXNY+dqx
		 * ------------------------------------新建--------------------------------------------------------------------------------------------
		 * 新建：各地区生育率与迁移摘要							——仿真结果_地区生育率与迁移摘要				*SummaryOfBirthAndMigration+dqx(包括0)
		 * 
		 * 新建：地区生育孩次数									——仿真结果_分婚配模式生育孩次数				*"BabiesBorn"+dqx
		 * 
		 * 新建：地区[全9除CX]分年龄人口预测				 		——仿真结果_分年龄人口预测	(预测)				*"PopulationPredictOf[All,NY,CXNY]"+dqx
		 * 新建：地区[城镇,农村,合计]分年龄迁移					——仿真结果_分年龄人口预测	(迁移)				*"PopulationMigrationOf[CX,All]"+dqx	
		 * 新建：地区[城镇,农村,合计]分年龄死亡			   		——仿真结果_分年龄人口预测	(死亡)				*"PopulationDeathOf[CX,All]"+dqx
		 * 
		 * 新建：地区[全9]特扶父母								——仿真结果_分年龄丧子人口预测(特扶)			"SonDiePopulationPredictOfTeFu"+dqx			
		 * 新建：地区[全9]一孩父母								——仿真结果_分年龄丧子人口预测(一孩)			"SonDiePopulationPredictOfYiHai"+dqx			
		 * 
		 * 新建：地区[半5]分年龄生育预测							——仿真结果_分年龄生育预测(预测)				*BirthPredictOf[CX,NY,All]+dqx
		 * 新建：地区[半5]分年龄超生预测							——仿真结果_分年龄生育预测(超生)				*OverBirthPredictOf[CX,NY,All]+dqx
		 * 新建：地区分龄政策生育									——仿真结果_分年龄生育预测(政策)				*"PolicyBabies"+dqx
		 * 
		 * 新建：地区[全9]夫妇及子女表结构						——仿真结果_夫妇及子女表结构					*CoupleAndChildrenOf[All,CX,NY,CXNY]+dqx
		 * 
		 * 新建：地区分年龄超生生育率								——仿真结果_妇女分年龄生育率预测				*BirthRatePredict+dqx
		 * 
		 * 新建：地区[半5]概要									——仿真结果_概要								*SummaryOf[All,CX,NY]+dqx
		 * 
		 * 新建：地区政策生育率									——仿真结果_政策及政策生育率					*PolicyBirthRate+dqx
		 * 
		 * 新建：地区婚配概率										——仿真结果_婚配概率							*MarriageRate+dqx
		 * 
		 * 
		 * 新建：地区城镇(农村)子女婚配预测表						——仿真结果_婚配预测表结构						* HunpeiOfCX+dqx
		 * 新建：地区农业(非农)子女婚配预测表						——仿真结果_婚配预测表结构						* HunpeiOfNY+dqx
		 * 新建：地区子女婚配预测表								——仿真结果_婚配预测表结构						* HunpeiOfAll+dqx
		 * 
		 * -------------------------------------------------------------------------------------------------------------------------------------	
		 */
		
		
		//特殊，全国字段
		SummaryOfBirthAndMigration = createSumBM ( );
		predictVarMap.put ( "SummaryOfBirthAndMigration"+"0" , SummaryOfBirthAndMigration );
				
		/////////////////// 获取各个省的数据并保存到全局变量 ！！predictVarMap！！
		//loop for each province
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			
			/***********
			 * 获取	   *
			 ***********/
			
			//生育及释放模式
			shengYuMoShi = createSYSFMSMap(MAX_AGE);
			database.setShengYuMoShi(shengYuMoShi,dqx,task.getParam_id(),tempVar.getTJ());
			predictVarMap.put("ShengYuMoShi"+dqx, shengYuMoShi);
			
			//死亡概率
			deathRate = createCXXBDoubleArrMap(MAX_AGE);
			String fieldName = tempVar.getYear()<2101? "Q"+tempVar.getYear():"Q2100";
			database.setDeathRate(deathRate,dqx,task.getParam_id(),fieldName);
			predictVarMap.put("DeathRate"+dqx, deathRate);
			
			//	分年龄人口预测-预测, 地区城镇(农村)分年龄人口预测
			PopulationPredictOfCX = createCXYearXBDoubleArrMap(MAX_AGE);
			database.setPredictOfCX(PopulationPredictOfCX,dqx,task.getData_id());
			predictVarMap.put("PopulationPredictOfCX"+dqx, PopulationPredictOfCX);
			
			BornXbbBean xbb = xbbMap.get(""+dqx);
//			tempVar.landMJ = xbb.getLand();
//			tempVar.fieldMJ = xbb.getField();
//			tempVar.waterS = xbb.getWater();
			
			/***********
			 * 新建	   *
			 ***********/
			// TODO mark
			//地区生育率与迁移摘要				
			SummaryOfBirthAndMigration = createSumBM ( );
			predictVarMap.put ( "SummaryOfBirthAndMigration"+dqx , SummaryOfBirthAndMigration );
			
			//地区生育孩次数
			BabiesBorn = createBabiesBorn();
			predictVarMap.put("BabiesBorn"+dqx, BabiesBorn);
			
			//分年龄人口预测-预测, 地区城镇(农村)分年龄人口预测
			PopulationPredictOfCXNY = createCXNYYearXBDoubleArrMap ( MAX_AGE );
			PopulationPredictOfNY = createNYYearXBDoubleArrMap ( MAX_AGE );
			PopulationPredictOfAll = createYearXBDoubleArrMap ( MAX_AGE );
			predictVarMap.put ( "PopulationPredictOfCXNY"+dqx , PopulationPredictOfCXNY );
			predictVarMap.put ( "PopulationPredictOfNY"+dqx , PopulationPredictOfNY );
			predictVarMap.put ( "PopulationPredictOfAll"+dqx , PopulationPredictOfAll );
			
			
			//	分年龄人口预测-迁移, 地区城镇(农村)分年龄迁移人口  
			PopulationMigrationOfCX = createCXYearXBDoubleArrMap(MAX_AGE);
			PopulationMigrationOfAll = createYearXBDoubleArrMap(MAX_AGE);
			predictVarMap.put("PopulationMigrationOfCX"+dqx, PopulationMigrationOfCX);
			predictVarMap.put("PopulationMigrationOfAll"+dqx, PopulationMigrationOfAll);
			
			//分年龄人口预测	(死亡)							
			PopulationDeathOfCX = createCXYearXBDoubleArrMap ( MAX_AGE );
			PopulationDeathOfAll = createYearXBDoubleArrMap ( MAX_AGE );
			predictVarMap.put ( "PopulationDeathOfCX"+dqx , PopulationDeathOfCX );
			predictVarMap.put ( "PopulationDeathOfAll"+dqx , PopulationDeathOfAll );
			
			
			//	分年龄丧子人口预测 (特扶，一孩)
			SonDiePopulationPredictOfTeFuCXNY = createCXNYYearXBDoubleArrMap(MAX_AGE);
			SonDiePopulationPredictOfTeFuCX = createCXYearXBDoubleArrMap ( MAX_AGE );
			SonDiePopulationPredictOfTeFuNY = createNYYearXBDoubleArrMap ( MAX_AGE );
			SonDiePopulationPredictOfTeFuAll = createYearXBDoubleArrMap ( MAX_AGE );
			
			SonDiePopulationPredictOfYiHaiCXNY = createCXNYYearXBDoubleArrMap(MAX_AGE);
			SonDiePopulationPredictOfYiHaiCX = createCXYearXBDoubleArrMap ( MAX_AGE );
			SonDiePopulationPredictOfYiHaiNY = createNYYearXBDoubleArrMap ( MAX_AGE );
			SonDiePopulationPredictOfYiHaiAll = createYearXBDoubleArrMap ( MAX_AGE );
			
			predictVarMap.put("SonDiePopulationPredictOfTeFuCXNY"+dqx, SonDiePopulationPredictOfTeFuCXNY);
			
			predictVarMap.put ( "SonDiePopulationPredictOfTeFuCX"+dqx , SonDiePopulationPredictOfTeFuCX );
			predictVarMap.put ( "SonDiePopulationPredictOfTeFuNY"+dqx , SonDiePopulationPredictOfTeFuNY );
			predictVarMap.put ( "SonDiePopulationPredictOfTeFuAll"+dqx ,SonDiePopulationPredictOfTeFuAll  );
			
			predictVarMap.put("SonDiePopulationPredictOfYiHaiCXNY"+dqx, SonDiePopulationPredictOfYiHaiCXNY);
			
			predictVarMap.put ( "SonDiePopulationPredictOfYiHaiCX"+dqx , SonDiePopulationPredictOfYiHaiCX );
			predictVarMap.put ( "SonDiePopulationPredictOfYiHaiNY"+dqx , SonDiePopulationPredictOfYiHaiNY );
			predictVarMap.put ( "SonDiePopulationPredictOfYiHaiAll"+dqx , SonDiePopulationPredictOfYiHaiAll );
			
			//分年龄生育预测(预测,超生)
			BirthPredictOfCX = createCXBabiesDoubleArrMap ( MAX_AGE );
			OverBirthPredictOfCX = createCXBabiesDoubleArrMap ( MAX_AGE );
			
			BirthPredictOfNY = createNYBabiesDoubleArrMap ( MAX_AGE );
			OverBirthPredictOfNY = createNYBabiesDoubleArrMap ( MAX_AGE );
			
			BirthPredictOfAll = createBabiesDoubleArrMap ( MAX_AGE );
			OverBirthPredictOfAll = createBabiesDoubleArrMap ( MAX_AGE );
			
			predictVarMap.put ( "BirthPredictOfCX"+dqx  , BirthPredictOfCX );
			predictVarMap.put ( "OverBirthPredictOfCX"+dqx  , OverBirthPredictOfCX );
			predictVarMap.put ( "BirthPredictOfNY"+dqx  , BirthPredictOfNY );
			predictVarMap.put ( "OverBirthPredictOfNY"+dqx  ,  OverBirthPredictOfNY);
			predictVarMap.put ( "BirthPredictOfAll"+dqx  ,BirthPredictOfAll  );
			predictVarMap.put ( "OverBirthPredictOfAll"+dqx  , OverBirthPredictOfAll );
			
			//分年龄生育预测(政策) --地区分龄政策生育
			PolicyBabies = createBabiesDoubleArrMap(MAX_AGE);
			predictVarMap.put("PolicyBabies"+dqx, PolicyBabies);
			
			//夫妇及子女表结构
			CoupleAndChildrenOfCXNY = createCXNYCouple ( );
			CoupleAndChildrenOfCX = createNYCouple ( );
			CoupleAndChildrenOfNY = createCXCouple ( );
			CoupleAndChildrenOfAll = createCouple ( );
			predictVarMap.put ( "CoupleAndChildrenOfCXNY"+dqx , CoupleAndChildrenOfCXNY );
			predictVarMap.put ( "CoupleAndChildrenOfCX"+dqx , CoupleAndChildrenOfCX );
			predictVarMap.put ( "CoupleAndChildrenOfNY"+dqx , CoupleAndChildrenOfNY );
			predictVarMap.put ( "CoupleAndChildrenOfAll"+dqx , CoupleAndChildrenOfAll );
			
			
			//妇女分年龄生育率预测
			BirthRatePredict = createYearDoubleArrMap ( MAX_AGE );
			predictVarMap.put ( "BirthRatePredict"+dqx , BirthRatePredict );

			//仿真结果_概要
			SummaryOfAll = createSummary ( );
			SummaryOfCX = createCXSummary ( );
			SummaryOfNY = createNYSummary ( );
			predictVarMap.put ( "SummaryOfAll"+dqx , SummaryOfAll );
			predictVarMap.put ( "SummaryOfCX"+dqx , SummaryOfCX );
			predictVarMap.put ( "SummaryOfNY"+dqx , SummaryOfNY );
			
			//政策及政策生育率					
			PolicyBirthRate = createPolicyBirth ( );
			predictVarMap.put ( "PolicyBirthRate"+dqx , PolicyBirthRate );
			
			//婚配概率							
			MarriageRate = createHunpeiGL ( );
			predictVarMap.put ( "MarriageRate"+dqx ,MarriageRate  );
			
			
			//婚配预测表 =>做双重映射
//			HunpeiOfCXNY = ( Map < CX , Map < NY , Map < HunpeiField , double [ ] >>> ) provinceMigMap.get ( ""+dqx );
			HunpeiOfCXNY = creatCXNYZiNv ( MAX_AGE );
			database.setZiNv(HunpeiOfCXNY,dqx,task.getId());
			
			HunpeiOfCX = creatCXZiNv ( MAX_AGE ); 
			HunpeiOfNY = creatNYFNZiNv ( MAX_AGE );
			HunpeiOfAll = creatAllZiNv ( MAX_AGE );		
//			HunpeiOfCX = ( Map < CX , Map < HunpeiField , double [ ] >> ) provinceMigMap.get ( "CX"+dqx );
//			HunpeiOfNY = ( Map < NY , Map < HunpeiField , double [ ] >> ) provinceMigMap.get ( "NY"+dqx );
//			HunpeiOfAll = ( Map < HunpeiField , double [ ] > ) provinceMigMap.get ( "All"+dqx );
			predictVarMap.put ( "HunpeiOfCXNY"+dqx , HunpeiOfCXNY );
			predictVarMap.put ( "HunpeiOfCX"+dqx , HunpeiOfCX );
			predictVarMap.put ( "HunpeiOfNY"+dqx , HunpeiOfNY );
			predictVarMap.put ( "HunpeiOfAll"+dqx , HunpeiOfAll );
//			Map < HunpeiField , double [ ] > HunpeiOfAll2 = (Map< HunpeiField , double[] >) predictVarMap.get( "HunpeiOfAll"+dqx );
//			HunpeiOfAll2.get( HunpeiField.SingleToNot )[3] = 3;
			
//			predictVarMap.put ( ""+dqx ,  );
			//TODO end mark
		}//end of province loop
		
		/**
		 * 初始化一些tempVar的成员变量
		 */
		tempVar.CNTFR = EnumMapTool.createSingleChild ( );
		tempVar.nyPolicyTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.nyImplTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.nyShiHunTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.cxPolicyTFR0 = EnumMapTool.createCXSingleChild ( );
		tempVar.cxImplTFR0 = EnumMapTool.createCXSingleChild ( );
		tempVar.cxShiHunTFR0  = EnumMapTool.createCXSingleChild ( );
		tempVar.teFuFaCN = EnumMapTool.createSingleChild ( );
		tempVar.teFuMaCN = EnumMapTool.createSingleChild ( );
		tempVar.CNHunneiTFR = EnumMapTool.createSingleChild ( );
		tempVar.nyHunneiTFR = EnumMapTool.createNYSingleChild ( );
		tempVar.cxHunneiTFR = EnumMapTool.createCXSingleChild ( );
		tempVar.QXM = new double[MAX_AGE];
		tempVar.QXF = new double[MAX_AGE];
		//婚内TFR
//		public Map<CX,Map<NY,Double>> CNHunneiTFR;
//		public Map<NY,Double> nyHunneiTFR;
//		public Map<CX,Double> cxHunneiTFR;
		
	}


	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {

	}

}
