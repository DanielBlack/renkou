package prclqz.methods;

import java.util.HashMap;
import java.util.Map;
import static prclqz.core.Const.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.HunpeiField.*;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.view.IView;

/**
 * 分婚配按政策生育的孩子数
 * @author prclqz@zju.edu.cn
 *
 */
public class MBabiesBornByPolicy implements IBabiesBornMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals,
			IBabiesBornByParentTypeMethod bornByParentType,IBabiesBornByDuijiMethod bornByDuiji,  IBabiesRateMethod babiesRate,
			IBabiesReleaseClassifyMethod releaAClssfy, IBabiesBornByFutureMethod classification)throws Exception {
		
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		IView v = (IView) globals.get("view");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		//define local variables
		int X;
		//地区分龄政策生育
		Map<Babies,double[]> policyBabies = (Map<Babies, double[]>) predictVarMap.get("PolicyBabies"+dqx);
		//地区 子女婚配预测表
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//(Map<CX, Map<NY, Map<HunpeiField, double[]>>>) provincMigMap.get(""+dqx);
		Map<NY,Map<HunpeiField,double[]>> znNY = (Map<NY, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfNY"+dqx );//provincMigMap.get("NY"+dqx);
		Map<CX,Map<HunpeiField,double[]>> znCX = (Map<CX, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfCX"+dqx );//provincMigMap.get("CX"+dqx);
		Map<HunpeiField,double[]> znAll = (Map<HunpeiField, double[]>) predictVarMap.get( "HunpeiOfAll"+dqx );//provincMigMap.get("All"+dqx);
		double DqPlyBorn,XFNPlyBorn,CFNPlyBorn,XNYPlyBorn,CNYPlyBorn;
		double[] NYTuShiAR,NYHuanShiAR,NYShiFangAR,FNTuShiAR,FNHuanShiAR,FNShiFangAR;
		NYTuShiAR = (double[]) predictVarMap.get("NYTuShiAR");
		NYHuanShiAR = (double[]) predictVarMap.get("NYHuanShiAR");
		NYShiFangAR = (double[]) predictVarMap.get("NYShiFangAR");
		FNTuShiAR = (double[]) predictVarMap.get("FNTuShiAR");
		FNHuanShiAR = (double[]) predictVarMap.get("FNHuanShiAR");
		FNShiFangAR = (double[]) predictVarMap.get("FNShiFangAR");
//		double[] TuShiAR,HuanShiAR,ShiFangAR;
		double SDJ; 
		
		
		for(CX cx : CX.values())
			for(NY ny : NY.values()){
				//设置参数通过tempVar传递给二级子程序
				Map<HunpeiField, double[]> zn = ziNv.get(cx).get(ny);
				tempVar.setZn(zn);
				tempVar.setCx(cx);
				tempVar.setNy(ny);
				// Fixed by danielblack
				if(ny == Nongye){
					tempVar.setNMDFR(tempVar.getN1D2X());
					tempVar.setNFDMR(tempVar.getN2D1X());
					tempVar.setNNR(  tempVar.getNNFX() );
				}else{
					tempVar.setNMDFR(tempVar.getN1D2C());
					tempVar.setNFDMR(tempVar.getN2D1C());
					tempVar.setNNR(  tempVar.getNNFC() );
				}
				// call level-2 procedure
				bornByParentType.calculate(m, globals);
				
				for(X = 0; X < MAX_AGE ; X++){
					zn.get(PlyDDB)[X] = zn.get(DDB)[X];
					zn.get(PlyNNB)[X] = zn.get(NNB)[X];
					zn.get(PlyNMDFB)[X] = zn.get(NMDFB)[X];
					zn.get(PlyNFDMB)[X] = zn.get(NFDMB)[X];
					zn.get(PlyRlsB)[X] = zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X];
					zn.get(PlyBorn)[X] = zn.get(PlyDDB)[X] + zn.get(PlyNFDMB)[X] +zn.get(PlyNMDFB)[X] 
					                   + zn.get(PlyNNB)[X] + zn.get(PlyRlsB)[X];
				}
				
				//选择数组---后面字程序再选择
//				HuanShiAR = (cx == Nongchun )? NYHuanShiAR : FNHuanShiAR;
//				TuShiAR = (cx == Nongchun )? NYTuShiAR : FNTuShiAR;
//				ShiFangAR = (cx == Nongchun )? NYShiFangAR : FNShiFangAR;
				
				//让堆积中的人数进行生育
				for(X = 0; X <= 49; X++){
					 tempVar.S_duiji += zn.get(SingleDJ)[X] + zn.get(ShuangfeiDJ)[X];
				}
				if(tempVar.S_duiji != 0 && year >= tempVar.getPolicyTime() && (year - tempVar.getPolicyTime()+1)<= 35)
				{	//call level-2 procedures
					bornByDuiji.calculate(m, globals);
				}
				
				//call level-2 procedures
				babiesRate.calculate(m, globals);
				releaAClssfy.calculate(m, globals , classification);
				
			}// CX-NY-Loop-end
		
		//CX == 4 执行合计——应该放在外面比较好
		Babies BND2 = Babies.getBabies(year);
		DqPlyBorn = XFNPlyBorn = CFNPlyBorn = XNYPlyBorn = CNYPlyBorn = 0;
		for(X = 0; X< MAX_AGE ; X++){
			policyBabies.get(BND2)[X] = ziNv.get(Nongchun).get(Feinong).get(PlyBorn)[X]+
										ziNv.get(Chengshi).get(Feinong).get(PlyBorn)[X]+
										ziNv.get(Nongchun).get(Nongye).get(PlyBorn)[X]+
										ziNv.get(Chengshi).get(Nongye).get(PlyBorn)[X];
			znNY.get(Nongye).get(PlyBorn)[X] = ziNv.get(Nongchun).get(Nongye).get(PlyBorn)[X]+ziNv.get(Chengshi).get(Nongye).get(PlyBorn)[X];
			znNY.get(Feinong).get(PlyBorn)[X] = ziNv.get(Nongchun).get(Feinong).get(PlyBorn)[X]+ziNv.get(Chengshi).get(Feinong).get(PlyBorn)[X];
			znCX.get(Chengshi).get(PlyBorn)[X] = ziNv.get(Chengshi).get(Nongye).get(PlyBorn)[X]+ziNv.get(Chengshi).get(Feinong).get(PlyBorn)[X];
			znCX.get(Nongchun).get(PlyBorn)[X] = ziNv.get(Nongchun).get(Nongye).get(PlyBorn)[X]+ziNv.get(Nongchun).get(Feinong).get(PlyBorn)[X];
			znAll.get(PlyBorn)[X] = znNY.get(Nongye).get(PlyBorn)[X]+znNY.get(Feinong).get(PlyBorn)[X];
			
			DqPlyBorn += policyBabies.get(BND2)[X];
			XFNPlyBorn += ziNv.get(Nongchun).get(Feinong).get(PlyBorn)[X];
			CFNPlyBorn += ziNv.get(Chengshi).get(Feinong).get(PlyBorn)[X];
			XNYPlyBorn += ziNv.get(Nongchun).get(Nongye).get(PlyBorn)[X];
			CNYPlyBorn += ziNv.get(Chengshi).get(Nongye).get(PlyBorn)[X];
		}
		//保存到全局变量
		tempVar.setPlyBorn(DqPlyBorn, XFNPlyBorn, CFNPlyBorn, XNYPlyBorn, CNYPlyBorn);
		
	}// end calculation

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
