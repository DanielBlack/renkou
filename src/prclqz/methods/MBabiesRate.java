package prclqz.methods;

import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.Const.*;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
/**
 * 各类婚配夫妇分年龄生育率
 * @author prclqz@zju.edu.cn
 *
 */
public class MBabiesRate implements IBabiesRateMethod {

	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		
		//define local variables
		int dqx = tempVar.getProvince();
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//provincMigMap.get(""+dqx);
		Map<HunpeiField, double[]> zn = ziNv.get(tempVar.getCx()).get(tempVar.getNy());
		int X;
		double sum_JTFR = 0,dbl_DTFR = 0,dbl_NTFR = 0,sgl_DTFR = 0;
		BabiesBornBean bBornBean = tempVar.getBabiesBorn();
		
		for(X = 0; X < MAX_AGE ;X++){
			
			if(zn.get(HJF)[X] != 0)
			zn.get(SYL_SUM)[X] = (zn.get(DDB)[X] + zn.get(NMDFB)[X] + zn.get(NFDMB)[X] + zn.get(NNB)[X]
			                      + zn.get(SingleRls)[X] + zn.get(ShuangFeiRls)[X])/zn.get(HJF)[X];
			
			if(zn.get(DD)[X] != 0)
			zn.get(SYL_DD)[X] = zn.get(DDB)[X]/zn.get(DD)[X];
			
			if( zn.get(NN)[X] + zn.get(ShuangfeiDJ)[X] + zn.get(Yi2Shuangfei)[X] != 0)
			zn.get(SYL_NN)[X] = (zn.get(NNB)[X] + zn.get(ShuangFeiRls)[X]) /
								(zn.get(NN)[X] + zn.get(ShuangfeiDJ)[X] + zn.get(Yi2Shuangfei)[X]);
			
			if(zn.get(NMDF)[X] + zn.get(NFDM)[X] + zn.get(SingleDJ)[X] + zn.get(Yi2Single)[X] != 0)
			zn.get(SYL_D)[X] = (zn.get(NMDFB)[X] + zn.get(NFDMB)[X] + zn.get(SingleRls)[X])/
							(zn.get(NMDF)[X] + zn.get(NFDM)[X] + zn.get(SingleDJ)[X] + zn.get(Yi2Single)[X]);
			
			sum_JTFR += zn.get(SYL_SUM)[X];
			dbl_DTFR += zn.get(SYL_DD)[X];
			dbl_NTFR += zn.get(SYL_NN)[X];
			sgl_DTFR += zn.get(SYL_D)[X];
		}
		
		bBornBean.setSum_JTFR(sum_JTFR);
		bBornBean.setDbl_DTFR(dbl_DTFR);
		bBornBean.setDbl_NTFR(dbl_NTFR);
		bBornBean.setSgl_DTFR(sgl_DTFR);
		
//		System.out.println("4TFRs:"+sum_JTFR+" "+dbl_DTFR+" "+dbl_NTFR+" "+sgl_DTFR);
	}

	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub

	}

}
