package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;
import prclqz.core.Message;

public interface IMethod {
	/**
	 * 设置方法参数
	 * @param params
	 * @param type
	 * @throws MethodException
	 */
	public void setParam(String params,int type) throws MethodException;
	/**
	 * 正式计算
	 * @param m
	 * @param globals
	 * @throws Exception
	 */
	public void calculate(IDAO m, HashMap<String, Object> globals) throws Exception;
	/***
	 * 确定数据库数据完整性(基本参数和基础数据)，并初始化仿真结果数据
	 * @return
	 */
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals) throws Exception;
	
}
