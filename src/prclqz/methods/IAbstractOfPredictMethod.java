package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;

public interface IAbstractOfPredictMethod extends IMethod
{
	void calculate(IDAO m, HashMap<String, Object> globals,
			IDeathCalculatingMethod deathCal, ICalculateExpectedAgeMethod calAge,
			IGroupingByAgeMethod groupAge,
			IResultOfAbstractMethod resultAbs,IMainExtremeValuesMethod mainVals )throws Exception;
}
