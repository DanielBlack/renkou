package prclqz.methods;
import static prclqz.core.Const.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.lib.EnumMapTool.*;
import prclqz.core.Message;
import prclqz.core.enumLib.*;

import java.text.DecimalFormat;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONObject;
import prclqz.DAO.IDAO;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.MainTaskBean;
import prclqz.parambeans.ParamBean3;
import prclqz.parambeans.ParamBean1;
import prclqz.view.IView;

/**
 * Migration for every province-无迁移的情况
 * @author prclqz@zju.edu.cn
 * @time 2011-09-02 first edition
 * @time 2011-10-23 second edition: use the EnumMap instead of the Array
 * @time 2011-10-28 third edition:  Debug the wrong result and convert comments into English version
 */
public class MProvinceNoMigration implements IMethod {

	///define class member variables
	private ParamBean1 paramBean; 
	//a format tool
	private static DecimalFormat df5 = new DecimalFormat("####0.0000000000");
	
	/***
	 * the main function of the province migration . 
	 * For a Unit test, a globals with essential variables is required
	 * @throws Exception 
	 */
	@Override
	public void calculate(IDAO m,HashMap<String,Object> globals) throws Exception {
		
		///get essential variables from globals
		ParamBean3 pb3 = (ParamBean3) globals.get("SetUpEnvParamBean");
		HashMap<String,BornXbbBean> xbbMap = (HashMap<String, BornXbbBean>) globals.get("bornXbbBeanMap");
		MainTaskBean task = (MainTaskBean) globals.get("taskBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		IView v = (IView) globals.get("view");
		
		
		//define local variables
		int dqx;
		//whether 全国城市化为背景 or not
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv;
		boolean debug = paramBean.getDebug() == 1;
		Map<QYPHField,long[]> QYPH = createQYPHArrMap(MAX_AGE);
		Map<QYPHTotal,Long> qianyiTotal = new EnumMap<QYPHTotal, Long>(QYPHTotal.class);
		//save the province result to globals 
//		HashMap<String, Object> provincMigMap=new HashMap<String, Object>();
		
		//TODO Next Optimization: fetch all essential data from databases only one time
		
		for(dqx=pb3.getDq1();dqx<=pb3.getDq2();dqx++)
		{
			if(!xbbMap.containsKey(""+dqx))
				continue;
			//primary data of province migration
			
			ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//creatCXXBZiNv(MAX_AGE);
//			m.setZiNv(ziNv,dqx,task.getId());
			
			//save the result of this province into the provinces result map
//			provincMigMap.put(""+dqx, ziNv);
		}
		
		//Save to province map as the national key--""+0 
//		provincMigMap.put("0", qianyiTotal);
		predictVarMap.put("qianyiTotal", qianyiTotal);
		//save the province result map to the globals
//		globals.put("provinceMigMap", provincMigMap);
		//save the result of national migration balancing to databases
		m.insertNationQY(QYPH,task.getId());
		
	}


	

	@Override
	public void setParam(String params, int type) throws MethodException {
		JSONObject jsonObject = JSONObject.fromObject(params);    
		paramBean = (ParamBean1) JSONObject.toBean( jsonObject, ParamBean1.class ); 
//		System.out.println("debug:"+paramBean.getDebug());
//		System.exit(0);
	}


	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO 检查是否数据完善，是否有重复冲突的数据
		return null;
	}
	
}

