package prclqz.methods;

import java.util.HashMap;

import prclqz.DAO.IDAO;
import prclqz.core.Message;

public interface INationalBornRateMethod extends IMethod
{
	public void calculate(IDAO m, HashMap<String, Object> globals,
			IBabiesRateMethod bRate) throws Exception;
}
