package prclqz.parambeans;

import net.sf.json.JSONObject;


public class ParamBean3 extends ParamBean{
	private String author;
	private int begin,end;
	private int dq1,dq2;
	private int type;
	
	public int getDq1() {
		return dq1;
	}


	public void setDq1(int dq1) {
		this.dq1 = dq1;
	}


	public int getDq2() {
		return dq2;
	}


	public void setDq2(int dq2) {
		this.dq2 = dq2;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String json ="{\"author\":\"LLLab-LS\",\"begin\":2000,\"end\":2100,\"dq1\":1,\"dq2\":31}";
		// {"author":"LLLab-LS","begin":2000,"end":2100,"dq1":1,"dq2":31}
//		String s = 'ddd';
				   //"{\"author\":\"LLLab-LS\",\"begin_year\":2000,\"end_year\":2100}" 
		JSONObject jsonObject = JSONObject.fromObject(json);    
		ParamBean3 bean = (ParamBean3) JSONObject.toBean( jsonObject, ParamBean3.class );       
		System.out.println("author:"+bean.getAuthor()+"&end:"+bean.getEnd()+"dq1:"+bean.getDq1());  
	}
	
	
	public ParamBean3() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ParamBean3(String value, int col, int row) {
		super();
		this.author = value;
		this.begin = col;
		this.end = row;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String value) {
		this.author = value;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int col) {
		this.begin = col;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int row) {
		this.end = row;
	}
	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return type;
	}


	@Override
	public void constructor() {
		// TODO Auto-generated method stub
		
	}

	
}
