package prclqz.parambeans;

import net.sf.json.JSONObject;


public class ParamBean1 extends ParamBean{
	
	private int debug;


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String json ="{\"author\":\"LLLab-LS\",\"begin\":2000,\"end\":2100,\"dq1\":1,\"dq2\":31}";
		// {"author":"LLLab-LS","begin":2000,"end":2100,"dq1":1,"dq2":31}
//		String s = 'ddd';
				   //"{\"author\":\"LLLab-LS\",\"begin_year\":2000,\"end_year\":2100}"
		String json = "{\"debug\":1}";
		JSONObject jsonObject = JSONObject.fromObject(json);    
		ParamBean1 bean = (ParamBean1) JSONObject.toBean( jsonObject, ParamBean1.class );       
		System.out.println("debug:"+bean.getDebug());  
	}

	//注意！这个是必须的，否则Json转换无法完成！
	public ParamBean1(int debug) {
		super();
		this.debug = debug;
	}


	public ParamBean1() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getDebug() {
		return debug;
	}


	public void setDebug(int debug) {
		this.debug = debug;
	}

	@Override
	public void constructor() {
		// TODO Auto-generated method stub
		
	}
	
	

}
