package prclqz.parambeans;

import net.sf.json.JSONObject;

public class ParamBean2 extends ParamBean {

	private String desc;
	private int marriageType;
	
	public ParamBean2(String desc, int marriageType) {
		super();
		this.desc = desc;
		this.marriageType = marriageType;
	}

	public String getDesc() {
		return desc;
	}

	public ParamBean2() {
		super();
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getMarriageType() {
		return marriageType;
	}

	public void setMarriageType(int marriageType) {
		this.marriageType = marriageType;
	}

	public static void main(String[] args) {
		
		String json ="{\"marriageType\":1,\"desc\":\"全员婚配\"}";
		
		JSONObject jsonObject = JSONObject.fromObject(json);    
		ParamBean2 bean = (ParamBean2) JSONObject.toBean( jsonObject, ParamBean2.class );       
		System.out.println("desc:"+bean.getDesc()+"&type:"+bean.getMarriageType());  
	}
	
	@Override
	public void constructor() {
		// TODO Auto-generated method stub
	
	}

}
