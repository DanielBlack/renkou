package prclqz.parambeans;

/**
 * 参数Bean 抽象类
 * @author prclqz@zju.edu.cn
 * 具体类定义：
 * 参量类型：
 * 	0,空
 *  1,int debug 
 * 	2.int marriageType,String desc 
 * 	3.String:Author,Int:begin,Int:end
 */
public abstract class ParamBean {
	
	/**
	 * 用来提醒您一定要生成对应的构造函数，否则Json工具无法完成工作！
	 * 一定要构造参数为空的构造函数！
	 * 并且，对应的变量一定要由 Get 和 Set 方法！
	 */
	public abstract void constructor();
	
	protected int getType(){
		return 1;
	}
}
