package danielblack.patch;

import java.util.HashMap;
import java.util.Map;

import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.SYSFMSField;

/**
 * 没有给HunpeiField的HJF, HJM, HJ做初计算
 * 对应的foxpro
 * 	 FOR SE=61 TO 69
 *	 	SELECT IIF(SE=SE,SE,1)
 *	 	REPLACE HJF WITH DF+NF,HJM WITH DM+NM,HJ WITH HJF+HJM ALL
 *	 ENDFOR
 * @author DanielBlack
 *
 */
public class HunpeiInitPatch {
	public static void patch(HashMap<String,Object> globals, int dqx){
		Map<CX, Map<NY, Map<HunpeiField, double[]>>> cx_ny = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) ((HashMap<String, Object>) globals
				.get("predictVarMap")).get("HunpeiOfCXNY" + dqx);
		Map<NY, Map<HunpeiField, double[]>> ny = (Map<NY, Map<HunpeiField, double[]>>) ((HashMap<String, Object>) globals
				.get("predictVarMap")).get("HunpeiOfNY" + dqx);
		Map<CX, Map<HunpeiField, double[]>> cx = (Map<CX, Map<HunpeiField, double[]>>) ((HashMap<String, Object>) globals
				.get("predictVarMap")).get("HunpeiOfCX" + dqx);
		Map<HunpeiField, double[]> all = (Map<HunpeiField, double[]>) ((HashMap<String, Object>) globals
				.get("predictVarMap")).get("HunpeiOfAll" + dqx);
		Map<CX,Map<SYSFMSField,double[]>> syms = (Map<CX,Map<SYSFMSField,double[]>>) ((HashMap<String, Object>) globals
				.get("predictVarMap")).get("ShengYuMoShi"+dqx);
		for(CX c : CX.values()){
			for(NY n : NY.values()){
				Map<HunpeiField, double[]> h = cx_ny.get(c).get(n);
				
				int l = h.get(HunpeiField.DD).length;
				for(int i = 0; i != l; ++i){
					h.get(HunpeiField.HJF)[i] = h.get(HunpeiField.DF)[i] + h.get(HunpeiField.NF)[i];
					h.get(HunpeiField.HJM)[i] = h.get(HunpeiField.DM)[i] + h.get(HunpeiField.NM)[i];
					h.get(HunpeiField.HJ)[i] = h.get(HunpeiField.HJF)[i] + h.get(HunpeiField.HJM)[i];
					if(n == NY.Nongye){
						h.get(HunpeiField.FR)[i] = syms.get(CX.Nongchun).get(SYSFMSField.SYSMB)[i];
						h.get(HunpeiField.FR1)[i] = syms.get(CX.Nongchun).get(SYSFMSField.SYSMB1)[i];
						h.get(HunpeiField.FR2)[i] = syms.get(CX.Nongchun).get(SYSFMSField.SYSMB2)[i];
					}else{
						h.get(HunpeiField.FR)[i] = syms.get(CX.Chengshi).get(SYSFMSField.SYSMB)[i];
						h.get(HunpeiField.FR1)[i] = syms.get(CX.Chengshi).get(SYSFMSField.SYSMB1)[i];
						h.get(HunpeiField.FR2)[i] = syms.get(CX.Chengshi).get(SYSFMSField.SYSMB2)[i];
					}
				}
			}
		}
		for(NY n : NY.values()){
			Map<HunpeiField, double[]> h = ny.get(n);
			int l = h.get(HunpeiField.DD).length;
			for(int i = 0; i != l; ++i){
				h.get(HunpeiField.HJF)[i] = h.get(HunpeiField.DF)[i] + h.get(HunpeiField.NF)[i];
				h.get(HunpeiField.HJM)[i] = h.get(HunpeiField.DM)[i] + h.get(HunpeiField.NM)[i];
				h.get(HunpeiField.HJ)[i] = h.get(HunpeiField.HJF)[i] + h.get(HunpeiField.HJM)[i];
			}
		}
		for(CX c : CX.values()){
				Map<HunpeiField, double[]> h = cx.get(c);
				int l = h.get(HunpeiField.DD).length;
				for(int i = 0; i != l; ++i){
					h.get(HunpeiField.HJF)[i] = h.get(HunpeiField.DF)[i] + h.get(HunpeiField.NF)[i];
					h.get(HunpeiField.HJM)[i] = h.get(HunpeiField.DM)[i] + h.get(HunpeiField.NM)[i];
					h.get(HunpeiField.HJ)[i] = h.get(HunpeiField.HJF)[i] + h.get(HunpeiField.HJM)[i];
				}
		}
		{
			Map<HunpeiField, double[]> h = all;
			int l = h.get(HunpeiField.DD).length;
			for(int i = 0; i != l; ++i){
//				h.get(HunpeiField.HJF)[i] = h.get(HunpeiField.DF)[i] + h.get(HunpeiField.NF)[i];
				h.get(HunpeiField.HJF)[i] = 
						ny.get(NY.Nongye).get(HunpeiField.HJF)[i] + 
						ny.get(NY.Feinong).get(HunpeiField.HJF)[i];
				h.get(HunpeiField.HJM)[i] = h.get(HunpeiField.DM)[i] + h.get(HunpeiField.NM)[i];
				h.get(HunpeiField.HJ)[i] = h.get(HunpeiField.HJF)[i] + h.get(HunpeiField.HJM)[i];
				
			}
		}
	}
}
