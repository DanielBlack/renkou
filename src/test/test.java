package test;
import static prclqz.core.Const.MAX_AGE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.DuiJiGuSuanFa;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.Policy;
import prclqz.lib.EnumMapTool;

public class test {
	
	public static TempVariablesBean parse(int year, int dqx) throws Exception
	{
		TempVariablesBean tempVar = new TempVariablesBean(0,0,0,0,0,0,0,0,0,0,0,0);
		tempVar.CNTFR = EnumMapTool.createSingleChild ( );
		tempVar.nyPolicyTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.nyImplTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.nyShiHunTFR0 = EnumMapTool.createNYSingleChild ( );
		tempVar.cxPolicyTFR0 = EnumMapTool.createCXSingleChild ( );
		tempVar.cxImplTFR0 = EnumMapTool.createCXSingleChild ( );
		tempVar.cxShiHunTFR0  = EnumMapTool.createCXSingleChild ( );
		tempVar.teFuFaCN = EnumMapTool.createSingleChild ( );
		tempVar.teFuMaCN = EnumMapTool.createSingleChild ( );
		tempVar.CNHunneiTFR = EnumMapTool.createSingleChild ( );
		tempVar.nyHunneiTFR = EnumMapTool.createNYSingleChild ( );
		tempVar.cxHunneiTFR = EnumMapTool.createCXSingleChild ( );
		tempVar.singleSonFaCN = EnumMapTool.createSingleChild ( );
		tempVar.singleSonMaCN = EnumMapTool.createSingleChild ( );
		
		tempVar.QXM = new double[MAX_AGE];
		tempVar.QXF = new double[MAX_AGE];
		FillTempVarFromFile(tempVar,"d:\\prclqz\\地区城乡预测结果摘要\\输入\\variable_"+year+"_"+dqx+".txt");
		return tempVar;
	}
public static void FillTempVarFromFile(TempVariablesBean tempVar,String path) throws IOException
{
	File file = new File(path);
	try{
		FileInputStream fis=new FileInputStream(file);
		InputStreamReader isr=new InputStreamReader(fis,"GBK");
		BufferedReader br=new BufferedReader(isr);

		String s;
		while((s=br.readLine())!=null)
		{
			String temp=s;
			String[] as=temp.split(" +");
			process(tempVar,as,br);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
}
	private static void process(TempVariablesBean tempVar,String[] as,BufferedReader br) throws Exception
	{
		if(as[0].equals(""))
			return;
		String x=as[0];
		if(as[0].equals("调整方式时机"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.adjustType="多地区渐进普二";//TODO 现在固定了；as[3]里存的是这个字符串还多两个双引号
			return;
		}
		
		//those below must be coherent with the database
		//非农时机 1-3
		if(as[0].equals("非农时机1"))
		{
			tempVar.feiNongTime1=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("非农时机2"))
		{
			tempVar.feiNongTime2=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("非农时机3"))
		{
			tempVar.feiNongTime3=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		//迁移程度，实现程度，释放模式，年份，省份
		if(as[0].equals("迁移程度"))
		{
			tempVar.QY=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("ZC实现"))
		{
			tempVar.ZCimplement=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("SFMS"))
		{
			tempVar.SFMS=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("ND0"))
		{
			tempVar.year=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("DQX"))
		{
			tempVar.province=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("农业时机1"))
		{
			tempVar.nongYeTime1=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("农业时机2"))
		{
			tempVar.nongYeTime2=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("农业时机3"))
		{
			tempVar.nongYeTime3=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("FN政策1"))
		{
			tempVar.feiNongPolicy1=tempVar.nongYePolicy1=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("FN政策2"))
		{
			tempVar.feiNongPolicy2=tempVar.nongYePolicy2=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("FN政策3"))
		{
			tempVar.feiNongPolicy3=tempVar.nongYePolicy3=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("实现比"))
		{
			tempVar.implRate=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}

		if(as[0].equals("调整时机"))
		{
			tempVar.policyTime=(as[3].equals(".F."))?0:Integer.parseInt(as[3]);
			return;
		}
		if(as[0].equals("政策"))//调整政策
		{
			if(as[3].equals("\"现行\""))
				tempVar.policy=Policy.xianXing;
			else if(as[3].equals("\"单独\""))
				tempVar.policy=Policy.danDu;
			else if(as[3].equals("\"单独后\""))
				tempVar.policy=Policy.danDuHou;
			else if(as[3].equals("\"普二\""))
				tempVar.policy=Policy.puEr;
			else if(as[3].equals("\"双读后\""))
				tempVar.policy=Policy.shuangDuHou;
			return;
		}
		
		if(as[0].equals("CSH水平"))
		{
			tempVar.CSHLevel = (as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			tempVar.cshLevel=(int) ((as[3].equals(".F."))?0:Double.parseDouble(as[3]));
			return;
		}
		if(as[0].equals("SQY"))
		{
			tempVar.SQY=(int) ((as[3].equals(".F."))?0:Double.parseDouble(as[3]));
			return;
		}
		if(as[0].equals("堆积估算法"))//
		{
			if(as[3].equals("\"存活一孩堆积估计法\""))
				tempVar.TJ=DuiJiGuSuanFa.cunhuoYihaiGujiFa;
			else if(as[3].equals("\"生育模式堆积估计法\""))
				tempVar.TJ=DuiJiGuSuanFa.shengyuMoshiGusuanFa;
			else if(as[3].equals("\"平均存活堆积估计法\""))
				tempVar.TJ=DuiJiGuSuanFa.pingjunCunhuoGujiFa;
			return;
		}
		
		if(as[0].equals("现行N1D2C"))
		{
			tempVar.nowN1D2C=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("现行N2D1C"))
		{
			tempVar.nowN2D1C=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("现行NNFC"))
		{
			tempVar.nowNNFC=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("现行N1D2X"))
		{
			tempVar.nowN1D2X=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("现行N2D1X"))
		{
			tempVar.nowN2D1X=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("现行NNFX"))
		{
			tempVar.nowNNFX=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("DDFR"))
		{
			tempVar.DDFR=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("N1D2C"))
		{
			tempVar.N1D2C=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("N2D1C"))
		{
			tempVar.N2D1C=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("NNFC"))
		{
			tempVar.NNFC=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		//未来政策生育率
		//public double N1D2X,N2D1X,NNFX;
//		if(as[0].equals("N1D2X")
//		{
//			tempVar.N1D2X=(as[3].equals(".F.")?0:Double.parseDouble(as[3]);
//			return;
//		}
//		if(as[0].equals("N2D1X")
//		{
//			tempVar.N2D1X=(as[3].equals(".F.")?0:Double.parseDouble(as[3]);
//			return;
//		}
//		if(as[0].equals("NNFX")
//		{
//			tempVar.NNFX=(as[3].equals(".F.")?0:Double.parseDouble(as[3]);
//			return;
//		}
		
		
		/**************局部变量******************/
		//分婚配模式按政策生育的孩子数,需要传递给二级子程序的参数
		
//		public Map<HunpeiField, double[]> zn;
//		public BabiesBornBean babiesBorn;
//		public CX cx;
//		public NY ny;
//		public int cxI;
		//public double nationalImplableTFR0;//没找到 TODO 可实现TFR0
		//全国政策TFR0
		//public double nationalPlyTFR0;//没找到
		//public double NMDFR,NFDMR,NNR;
		//public double NMDFR,NFDMR,NNR;//没找到 TODO 只赋值没用到
		//S独子父，S独子母
		//public double S_singleSonFa,S_singleSonMa;//没找到
		//出生人口
		//public double bornPopulation,;//出生人口没找到，其他两个没用到
		//净迁入RK
		//public double inMigRK;//没找到
		//死亡人口M,死亡人口F,死亡人口
		//public double deathM,deathF,death;
				
		/************************************/
		
		
		if(as[0].equals("CSH水平1"))
		{
			tempVar.CSHLevel1=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("现政比1"))
		{
			tempVar.nowPolicyRate1=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
			
		if(as[0].equals("地区政策TFR1"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqPolicyTFR1=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("MITFR"))
		{
			tempVar.MITFR=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("N1D2N"))
		{
			tempVar.N1D2N=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("N2D1N"))
		{
			tempVar.N2D1N=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("NNFN"))
		{
			tempVar.NNFN=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CSH水平"))
		{
			tempVar.CSHLevel=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("EM0"))
		{
			tempVar.EM0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("EF0"))
		{
			tempVar.EF0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("SDQQY"))
		{
			tempVar.SDQQY=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("DQQY"))
		{
			tempVar.DQQY=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CEM0"))
		{
			tempVar.CEM0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XEM0"))
		{
			tempVar.XEM0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CEF0"))
		{
			tempVar.CEF0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XEF0"))
		{
			tempVar.XEF0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CSH水平2"))
		{
			tempVar.CSHLevel2=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("地区政策TFR"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqPolicyTFR=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("可实现TFR0")) 
		{
			tempVar.implableTFR0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("可实现TFR0")) 
		{
			tempVar.implableTFR0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("超生S")) 
		{
			tempVar.overBirthS=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("S堆积")) 
		{
			tempVar.S_duiji=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("农村农业独子父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonFaCN.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村农业独子母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonMaCN.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇农业独子父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonFaCN.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇农业独子母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonMaCN.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村非农独子父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonFaCN.get(CX.Nongchun).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村非农独子母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonMaCN.get(CX.Nongchun).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇非农独子父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonFaCN.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇非农独子母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.singleSonMaCN.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}

		
		if(as[0].equals("农村农业特扶父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuFaCN.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村农业特扶母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuMaCN.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇农业特扶父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuFaCN.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇农业特扶母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuMaCN.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村非农特扶父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuFaCN.get(CX.Nongchun).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村非农特扶母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			return;
		}
		if(as[0].equals("城镇非农特扶父")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuFaCN.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇非农特扶母")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.teFuMaCN.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		
		
		
		if(as[0].equals("农村农业TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNTFR.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇农业TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNTFR.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村非农TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNTFR.get(CX.Nongchun).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇非农TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNTFR.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}

		
		if(as[0].equals("非农实婚TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyShiHunTFR0.put( NY.Feinong,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农业实婚TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyShiHunTFR0.put( NY.Nongye,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("非农实现TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyImplTFR0.put( NY.Feinong,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农业实现TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyImplTFR0.put( NY.Nongye,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("非农政策TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyPolicyTFR0.put( NY.Feinong,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农业政策TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyPolicyTFR0.put( NY.Nongye,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇实婚TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxShiHunTFR0.put( CX.Chengshi,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村实婚TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxShiHunTFR0.put( CX.Nongchun,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇实现TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxImplTFR0.put( CX.Chengshi,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村实现TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");

			tempVar.cxImplTFR0.put( CX.Nongchun,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇政策TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxPolicyTFR0.put( CX.Chengshi,as[3].equals(".F.")?0.0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村政策TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxPolicyTFR0.put( CX.Nongchun,(as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("地区政策TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqPolicyTFR0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("地区实现TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqImplTFR0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("地区实婚TFR0"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqShiHunTFR0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("地区超生TFR"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqOverBrithTFR=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("FNRK"))
		{
			tempVar.FNRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CRK"))
		{
			tempVar.CRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XRK"))
		{
			tempVar.XRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("NYRK"))
		{
			tempVar.NYRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		//婚内TFR
		if(as[0].equals("NCNY婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNHunneiTFR.get(CX.Nongchun).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("CXNY婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNHunneiTFR.get(CX.Chengshi).put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("NCFN婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNHunneiTFR.get(CX.Nongchun).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("CZFN婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNHunneiTFR.get(CX.Chengshi).put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农业婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyHunneiTFR.put(NY.Nongye, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("非农婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.nyHunneiTFR.put(NY.Feinong, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("城镇婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxHunneiTFR.put(CX.Chengshi, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("农村婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.cxHunneiTFR.put(CX.Chengshi, (as[3].equals(".F."))?0:Double.parseDouble(as[3]) );
			return;
		}
		if(as[0].equals("地区婚内TFR")) 
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqHunneiTFR=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		/****死亡人口****/
		if(as[0].equals("DQ死亡人口M"))//分行
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqDeathM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("DQ死亡人口F"))//分行
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.dqDeathF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("C死亡人口M"))
		{
			tempVar.cDeathM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("C死亡人口F"))
		{
			tempVar.cDeathF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("X死亡人口M"))
		{
			tempVar.xDeathM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("X死亡人口F"))
		{
			tempVar.xDeathF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		//QXM
		//public double[] QXM,QXF;TODO
		/********QXM和QXF其实只要是0就可以了,具体数值会在DeathCalculate里计算,而且初始化也不方便,有时候90几行有时候100多行的
		 * 一定要用的话得根据具体情况设置这里的循环次数
		 */
//		if(as[0].equals("QXM"))
//		{
//			for(int i=1;i<=94;i++)
//			{
//				String str = br.readLine();
//				as=str.split(" +");
//				tempVar.QXM[i]=(as[4].equals(".F."))?0:Double.parseDouble(as[4]);
//			}
//			return;
//		}
//		
//		if(as[0].equals("QXF"))
//		{
//			for(int i=1;i<=99;i++)
//			{
//				String str = br.readLine();
//				as=str.split(" +");
//				tempVar.QXF[i]=(as[4].equals(".F."))?0:Double.parseDouble(as[4]);
//			}
//			return;
//		}
		
		if(as[0].equals("MINLM"))
		{
			tempVar.MINLM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("MINLF"))
		{
			tempVar.MINLF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("总人口T"))
		{
			tempVar.Popl_T=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("总人口M"))
		{
			tempVar.Popl_M=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("总人口F"))
		{
			tempVar.Popl_F=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("年中人口"))
		{
			tempVar.nianzhongRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("新增人口"))
		{
			tempVar.xinzengRK=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("人口增长L"))
		{
			tempVar.RKzengzhang=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("T少年"))
		{
			tempVar.T_teen=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T劳年"))
		{
			tempVar.T_labor=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T青劳"))
		{
			tempVar.T_younglabor=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T老年"))
		{
			tempVar.T_old=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T高龄"))
		{
			tempVar.T_high=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T老年M"))
		{
			tempVar.T_oldM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T高龄M"))
		{
			tempVar.T_highM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T老年F"))
		{
			tempVar.T_oldF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("T高龄F"))
		{
			tempVar.T_highF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		/************育龄************/
		if(as[0].equals("育龄F15"))
		{
			tempVar.yuLing_F15=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("婚育M15"))
		{
			tempVar.hunYu_M15=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("育龄F20"))
		{
			tempVar.yuLing_F20=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("婚育M20"))
		{
			tempVar.hunYu_M20=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("长寿M"))
		{
			tempVar.changshou_M=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("长寿F"))
		{
			tempVar.changshou_F=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("长寿M0"))
		{
			tempVar.changshou_M0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("长寿F0"))
		{
			tempVar.changshou_F0=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("T婴幼儿"))
		{
			tempVar.T_yingyouer=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XQ"))
		{
			tempVar.XQ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XX"))
		{
			tempVar.XX=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CZ"))
		{
			tempVar.CZ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GZ"))
		{
			tempVar.GZ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("DX"))
		{
			tempVar.DX=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("JYM"))
		{
			tempVar.JYM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("JYF"))
		{
			tempVar.JYF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		
		if(as[0].equals("GB劳前"))
		{
			tempVar.GB_laoqian=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB劳龄M"))
		{
			tempVar.GB_laolingM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB劳龄F"))
		{
			tempVar.GB_laolingF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB青劳"))
		{
			tempVar.GB_qinglao=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB中劳M"))
		{
			tempVar.GB_zhonglaoM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB中龄F"))
		{
			tempVar.GB_zhonglingF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB劳后M"))
		{
			tempVar.GB_laohouM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("GB劳后F"))
		{
			tempVar.GB_laohouF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("G劳年M"))
		{
			tempVar.GB_labornianM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("G老年M"))
		{
			tempVar.GB_oldnianM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("G少年"))
		{
			tempVar.GB_shounian=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("G劳年F"))
		{
			tempVar.G_labornianF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("G老年F"))
		{
			tempVar.G_oldnianF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		
		if(as[0].equals("NLZ"))
		{
			tempVar.NLZ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("人口增长L"))
		{
			tempVar.RKzengZhangL=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("出生人口M"))
		{
			tempVar.bornPopulationM=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("出生人口F"))
		{
			tempVar.bornPopulationF=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		if(as[0].equals("ST"))
		{
			tempVar.ST=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("AV"))
		{
			tempVar.AV=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("DQ政策生育"))
		{
			tempVar.DqPlyBorn=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XFN政策生育"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.XFNPlyBorn=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CFN政策生育"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CFNPlyBorn=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("XNY政策生育"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.XNYPlyBorn=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("CNY政策生育"))
		{
			String str=br.readLine();
			as=str.split(" +");
			tempVar.CNYPlyBorn=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		
		
		
		//土地MJ=分省代码.土地面积		&&单位:千公顷
		//耕地MJ=分省代码.耕地06			&&单位:千公顷
		//水资源=分省代码.水资源均量		&&单位:亿立方米
		if(as[0].equals("土地MJ"))
		{
			tempVar.landMJ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("耕地MJ"))
		{
			tempVar.fieldMJ=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		if(as[0].equals("水资源"))
		{
			tempVar.waterS=(as[3].equals(".F."))?0:Double.parseDouble(as[3]);
			return;
		}
		//System.out.println("not found:"+x);
	}
}