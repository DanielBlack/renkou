package test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.*;
import java.io.*;
import java.util.*;

import static prclqz.core.Const.*;
public class Parser{
	
	// args的常量下标
	public static final int CHENG_ZHEN = 0;
	public static final int NONG_CUN = 1;
	public static final int NONG_YE = 2;
	public static final int FEI_NONG = 3;
	
	// 模式在foxpro中对应的区域
	public static final int NC_NY = 61;
	public static final int NC_FN = 62;
	public static final int CZ_NY = 63;
	public static final int CZ_FN = 64;
	public static final int NC = 65;
	public static final int CZ = 66;
	public static final int NY = 67;
	public static final int FN = 68;
	public static final int ALL = 69;
	
//	Test path
//	private static final String PREFIX = "D:\\prclqz\\地区城乡预测结果摘要\\";
	private static final String PREFIX = "D:\\DB\\data\\逐层全国地区累计\\";
	
	private static final String INPUT_HEADER_PATH = "输入\\表结构_zinv_";
//	private static final String INPUT_HEADER_PATH = "输入\\表结构_";

	private static final String INPUT_DATA_PATH = "输入\\表内容_zinv_";
//	private static final String INPUT_DATA_PATH = "输入\\表内容_";
	
//	private static final String OUTPUT_DATA_PATH = "输出\\表内容_";
//	private static final String OUTPUT_HEADER_PATH = "输出\\表结构_";
	
	//最大容量
	
	public static void main(String[] _args) throws Exception{
		int args[] = new int[4];
		args[NONG_CUN] = 1;
		args[CHENG_ZHEN] = 0;
		args[NONG_YE] = 1;
		args[FEI_NONG] = 0;
		parse(77, 2014, 28);
	}
	// args: 城镇(CHENG_ZHEN)、农村(NONG_CUN)、农业(NONG_YE)、非农(FEI_NONG)的组合
	// year: 年份
	// dqx: 地区
	/**
	 * 从表内容文件和表结构文件中读取数据,以prclqz标准形式返回表,即:String:字段名,double[]:该字段下MAX_AGE行的数据,此字段即彼enum变量
	 * !!!!注意用于不同的模块中要修改prefix和另外两个路径
	 * @param dn
	 * 工作区号
	 * @param year
	 * @param dqx
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, double[]> parse (
			int dn,
			int year,
			int dqx
	)throws Exception{
		String postfix = Integer.toString(dn) + "_" + year + "_" + dqx + ".txt";
		File header_file = new File(PREFIX + INPUT_HEADER_PATH + postfix);
		File data_in_file = new File(PREFIX + INPUT_DATA_PATH + postfix);
		
		//表结构
		FileInputStream header = null;
		try{
			header = new FileInputStream(header_file);
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
		}
		//表内容
		FileInputStream data_in = null;
		try{
			data_in = new FileInputStream(data_in_file);
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
		}
		BufferedReader header_br = new BufferedReader(new InputStreamReader(header, "GBK"));
		BufferedReader data_in_br = new BufferedReader(new InputStreamReader(data_in, "GBK"));
		
		ArrayList<String> header_arr = parseHeader(header_br);
		ArrayList<ArrayList<Double>> data_arr = parseData(data_in_br);
		HashMap<String, double[]> res = new HashMap<String, double[]>();

		//res的格式:String:字段名,double[],该字段的数据
		for(int i = 0; i != header_arr.size(); ++i){
			ArrayList<Double> tmp_db_arr = data_arr.get(i);
			double[] tmp_db = new double[MAX_AGE];
			for(int j = 0; j < MAX_AGE; ++j){
				if(j >= tmp_db_arr.size()){
					tmp_db[j] = 0.0;
				}
				else{
					tmp_db[j] = tmp_db_arr.get(j);
				}
			}
			res.put(header_arr.get(i), tmp_db);
			
		}	
		return res;
		
	}
		
	public static ArrayList<String> parseHeader(BufferedReader br) throws Exception{
		// 去除7行
		for(int ignore_i = 0; ignore_i < 7; ++ignore_i ){
			br.readLine();
		}
		
		ArrayList<String> header = new ArrayList<String>();
		
		while(br.ready()){
			String curr_line = br.readLine();
			String[] tmp_str_list = curr_line.split("\\s+");
			//到达最后一行
			if(tmp_str_list.length <= 4) 
				break;
//			System.out.println(tmp_str_list[2].trim());
			header.add(tmp_str_list[2].trim());
		}
		
		return header;
	}
/**
 * 返回一个二维数组,保存了br指向的文件里的里的内容,第一列不要
 * @param br
 * @return
 * @throws Exception
 */
	//DB版parseData
	public static ArrayList<ArrayList<Double>> parseData(BufferedReader br) 
			throws Exception{
		ArrayList<ArrayList<Double>> res = new ArrayList<ArrayList<Double>>();
		boolean first = true;
		int count = 0;
		while(br.ready()){
			String curr_line = br.readLine();
			String[] tmp_str_list = curr_line.split(",");
			if(first){
				first = false;
				//第一列不要
				for(int i = 0; i != tmp_str_list.length; ++i){
					res.add(new ArrayList<Double>());
				}
			}
			for(int i = 1; i != tmp_str_list.length; ++i){
				// 按表头放入ArrayList中
				try{
					res.get(i - 1).add(Double.parseDouble(tmp_str_list[i]));
				}
				catch(NumberFormatException e){
					System.out.println("输入的数据中有字符串类型！已自动转为0.0，请确认是否正确！");
					res.get(i - 1).add(0.0);
				}
			}
		}
		return res;
	}
	public static ArrayList<String> getHeader(String path)throws Exception{
		File file = new File(path);
		FileInputStream fs = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fs, "GBK"));
		
		for(int ignore_i = 0; ignore_i < 7; ++ignore_i ){
			br.readLine();
		}
		ArrayList<String> header = new ArrayList<String>();
		
		while(br.ready()){
			String curr_line = br.readLine();
			String[] tmp_str_list = curr_line.split("\\s+");
			//到达最后一行
			if(tmp_str_list.length <= 4) 
				break;
			header.add(tmp_str_list[2].trim());
		}
		return header;
	}

public static HashMap<String, double[]> parse_DS (
		int dn,
		int year,
		int dqx,
		int offset
)throws Exception{
	String postfix = Integer.toString(dn) + "_" + year + "_" + dqx + ".txt";
	File header_file = new File(PREFIX + INPUT_HEADER_PATH + postfix);
	File data_in_file = new File(PREFIX + INPUT_DATA_PATH + postfix);
	
	//表结构
	FileInputStream header = null;
	try{
		header = new FileInputStream(header_file);
	}
	catch (FileNotFoundException e){
		e.printStackTrace();
	}
	//表内容
	FileInputStream data_in = null;
	try{
		data_in = new FileInputStream(data_in_file);
	}
	catch (FileNotFoundException e){
		e.printStackTrace();
	}
	BufferedReader header_br = new BufferedReader(new InputStreamReader(header, "GBK"));
	BufferedReader data_in_br = new BufferedReader(new InputStreamReader(data_in, "GBK"));
	
	ArrayList<String> header_arr = parseHeader_DS(header_br,offset);
	ArrayList<ArrayList<Double>> data_arr = parseData_DS(data_in_br,offset);
	HashMap<String, double[]> res = new HashMap<String, double[]>();
	//res的格式:String:字段名,double[],该字段的数据
	for(int i = 0; i != header_arr.size(); ++i){
		ArrayList<Double> tmp_db_arr = data_arr.get(i);
		double[] tmp_db = new double[MAX_AGE];
		for(int j = 0; j != MAX_AGE; ++j){
			if(j >= tmp_db_arr.size()){
				tmp_db[j] = 0.0;
			}
			else{
				tmp_db[j] = tmp_db_arr.get(j);
			}
		}
		res.put(header_arr.get(i), tmp_db);
		
	}	
	return res;
	
}

	public static ArrayList<ArrayList<Double>> parseData_DS(BufferedReader br,int offset) 
			throws Exception{
		ArrayList<ArrayList<Double>> res = new ArrayList<ArrayList<Double>>();
		boolean first = true;
		while(br.ready()){
			String curr_line = br.readLine();
			String[] tmp_str_list = curr_line.split(",");
			if(first){
				first = false;
				// 前offset列不要
				for(int i = offset; i != tmp_str_list.length; ++i){
					res.add(new ArrayList<Double>());
				}
			}
			for(int i = offset; i != tmp_str_list.length; ++i){
				// 按表头放入ArrayList中
				res.get(i - offset).add(Double.parseDouble(tmp_str_list[i]));
			}
		}
		return res;
	}
	public static ArrayList<String> parseHeader_DS(BufferedReader br,int offset) throws Exception{
		// 去除7行
		for(int ignore_i = 0; ignore_i < 6+offset; ++ignore_i ){
			br.readLine();
		}
		
		ArrayList<String> header = new ArrayList<String>();
		
		while(br.ready()){
			String curr_line = br.readLine();
			String[] tmp_str_list = curr_line.split("\\s+");
			//到达最后一行
			if(tmp_str_list.length <= 4) 
				break;
//			System.out.println(tmp_str_list[2].trim());
			header.add(tmp_str_list[2].trim());
		}
		
		return header;
	}
	
	
}

