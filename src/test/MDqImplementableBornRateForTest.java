package test;


import java.util.Map;

import static prclqz.core.enumLib.SYSFMSField.*;
//import static prclqz.core.enumLib.Couple.*;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.XB.*;
import static prclqz.core.enumLib.CX.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.Const.*;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.BabiesBornBean;
import prclqz.DAO.Bean.BornXbbBean;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.StringList;
import prclqz.core.enumLib.Babies;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.Couple;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.core.enumLib.Policy;
import prclqz.core.enumLib.PolicyBirth;
import prclqz.core.enumLib.SYSFMSField;
import prclqz.core.enumLib.XB;
import prclqz.core.enumLib.Year;

import prclqz.lib.EnumMapTool;
import prclqz.methods.IMethod;
import prclqz.methods.MethodException;
import prclqz.parambeans.ParamBean3;
/**
 * 地区多种政策和可实现时期生育率
 * @author prclqz@zju.edu.cn
 *
 */
public class MDqImplementableBornRateForTest implements IMethod
{
	public static void main(String[] argv)throws Exception{
		HashMap<String,Object>globals = new HashMap<String,Object>();
		IDAO m = new MyDAOImpl();
		TempVariablesBean tempVar = new TempVariablesBean(0,0,0,0,0,0,0,0,0,0,0,0);
		int dqx=3;
		int year=2010;
		/***构造tempvar***/
		tempVar.setProvince(dqx);
		tempVar.setYear(year);
		tempVar.CNTFR = new EnumMap<CX,Map<NY,Double>>(CX.class);
		for(CX cx: CX.values()){
			tempVar.CNTFR.put(cx, new EnumMap<NY,Double>(NY.class));
		}
		tempVar.nyPolicyTFR0 = new EnumMap<NY,Double>(NY.class);
		tempVar.cxPolicyTFR0 = new EnumMap<CX,Double>(CX.class);
		tempVar.nyImplTFR0 = new EnumMap<NY,Double>(NY.class);
		tempVar.cxImplTFR0 = new EnumMap<CX,Double>(CX.class);
		tempVar.nyShiHunTFR0 = new EnumMap<NY,Double>(NY.class);
		tempVar.cxShiHunTFR0 = new EnumMap<CX,Double>(CX.class);
		/***读取表内容***/
		HashMap<String,Object> predictVarMap = new HashMap<String,Object>();
		Map<CX, Map<NY, Map<HunpeiField, double[]>>> ziNv = EnumTools
				.creatCXNYZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		predictVarMap.put("HunpeiOfCXNY" + dqx, ziNv);
//		double[][] husbandRate = EnumTools.createHusbandRate(MAX_AGE, year, dqx);
//		predictVarMap.put("HusbandRate", husbandRate);
		Map<NY,Map<HunpeiField,double[]>> ziNvOfNY = EnumTools.creatNYFNZiNvFromFile(MAX_AGE, year, dqx);
		predictVarMap.put("HunpeiOfNY"+dqx, ziNvOfNY);
		Map<CX,Map<HunpeiField,double[]>> ziNvOfCX = EnumTools.creatCXZiNvFromFile(MAX_AGE, year, dqx);
		predictVarMap.put("HunpeiOfCX"+dqx, ziNvOfCX);
		Map<HunpeiField,double[]> ziNvOfAll = EnumTools.creatAllZiNvFromFile(MAX_AGE, year, dqx);
		predictVarMap.put("HunpeiOfAll"+dqx, ziNvOfAll);
		Map<CX,Map<Babies,double[]>> BirthPredictOfCX = EnumTools.createBirthPredictOfCX(MAX_AGE, year, dqx);
		predictVarMap.put("BirthPredictOfCX"+dqx, BirthPredictOfCX);
		Map<Babies,double[]> BirthPredictOfAll = EnumTools.createBirthPredictALL(MAX_AGE, year, dqx);
		predictVarMap.put("BirthPredictOfAll"+dqx, BirthPredictOfAll);
		Map<Babies,double[]> OverBirthPredictOfAll = EnumTools.createOverBirthPredictALL(MAX_AGE,year,dqx);
		predictVarMap.put ( "OverBirthPredictOfAll"+dqx ,OverBirthPredictOfAll);
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> PopulationPredictOfCXNY = EnumTools.creatPopulationPredictOfCXNY(MAX_AGE, year, dqx);
		predictVarMap.put ( "PopulationPredictOfCXNY"+dqx ,PopulationPredictOfCXNY);
		Map<CX,Map<Year,Map<XB,double[]>>>PopulationPredictOfCX = EnumTools.creatPopulationPredictOfCX(MAX_AGE, year, dqx);
		predictVarMap.put ( "PopulationPredictOfCX"+dqx ,PopulationPredictOfCX);
		Map<NY,Map<Year,Map<XB,double[]>>> PopulationPredictOfNY = EnumTools.creatPopulationPredictOfNY(MAX_AGE, year, dqx);
		predictVarMap.put ( "PopulationPredictOfNY"+dqx ,PopulationPredictOfNY);
		Map<Year,Map<XB,double[]>> PopulationPredictOfAll = EnumTools.creatPopulationPredictOfAll(MAX_AGE, year, dqx);
		predictVarMap.put ( "PopulationPredictOfAll"+dqx ,PopulationPredictOfAll);
		Map<Babies,double[]> policyBabies = EnumTools.createPolicyBabies(MAX_AGE, year, dqx);
		predictVarMap.put("PolicyBabies"+dqx, policyBabies);
		/***构造globals***/
		globals.put("tempVarBean", tempVar);
		globals.put("predictVarMap", predictVarMap);
		/***开始运行***/
		MDqImplementableBornRateForTest hehe = new MDqImplementableBornRateForTest();
		hehe.calculate(m, globals);
	}
	@Override
	public void calculate ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		//define local variables
		int X=0;
		//婚配
		//地区分龄政策生育
		Map<Babies,double[]> policyBabies = (Map<Babies, double[]>) predictVarMap.get("PolicyBabies"+dqx);
		//地区 子女婚配预测表
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map<CX, Map<NY, Map<HunpeiField, double[]>>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//provincMigMap.get(""+dqx);
		Map<NY,Map<HunpeiField,double[]>> ziNvOfNY = (Map<NY, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfNY"+dqx );//provincMigMap.get("NY"+dqx);
		Map<CX,Map<HunpeiField,double[]>> ziNvOfCX = (Map<CX, Map<HunpeiField, double[]>>) predictVarMap.get( "HunpeiOfCX"+dqx );//provincMigMap.get("CX"+dqx);
		Map<HunpeiField,double[]> ziNvOfAll = (Map<HunpeiField, double[]>) predictVarMap.get( "HunpeiOfAll"+dqx );//provincMigMap.get("All"+dqx);
		//分年龄生育预测(生育，超生，政策)
		Map<CX,Map<Babies,double[]>> BirthPredictOfCX = ( Map < CX , Map < Babies , double [ ] >> ) predictVarMap.get ( "BirthPredictOfCX"+dqx );
		Map<Babies,double[]> BirthPredictOfAll = ( Map < Babies , double [ ] > ) predictVarMap.get ( "BirthPredictOfAll"+dqx );
		Map<Babies,double[]> OverBirthPredictOfAll = ( Map < Babies , double [ ] > ) predictVarMap.get ( "OverBirthPredictOfAll"+dqx );
		//分龄人口预测
		Map<CX,Map<NY,Map<Year,Map<XB,double[]>>>> PopulationPredictOfCXNY = ( Map < CX , Map < NY , Map < Year , Map < XB , double [ ] >>> > ) predictVarMap.get ( "PopulationPredictOfCXNY"+dqx );
		Map<CX,Map<Year,Map<XB,double[]>>>PopulationPredictOfCX = ( Map < CX , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationPredictOfCX"+dqx );
		Map<NY,Map<Year,Map<XB,double[]>>> PopulationPredictOfNY = ( Map < NY , Map < Year , Map < XB , double [ ] >>> ) predictVarMap.get ( "PopulationPredictOfNY"+dqx );
		Map<Year,Map<XB,double[]>> PopulationPredictOfAll = ( Map < Year , Map < XB , double [ ] >> ) predictVarMap.get ( "PopulationPredictOfAll"+dqx );

		
//		//农村TFR0,农村非农TFR,农村农业TFR,
//		public Map<CX,Map<NY,Double>> CNTFR;
//		//非农实婚TFR0,非农实现TFR0,农村实婚TFR0,农村实现TFR0,农业实婚TFR0,农业实现TFR0
//		public Map<NY,Double> nyPolicyTFR0,nyImplTFR0,nyShiHunTFR0;
//		//城镇-农村政策……
//		public Map<CX,Double> cxPolicyTFR0,cxImplTFR0,cxShiHunTFR0;
		
		
//		policyBabies.clear ( );
		
		double [] ARFND1 = new double[MAX_AGE];
		//////////////translated by Foxpro2Java Translator successfully:///////////////
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCXNY.get ( Nongchun ).get (
						Nongye ).get ( Year.getYear ( year - 1 ) )
						.get ( Female ) [ X ];
			}
		}
		tempVar.CNTFR.get ( Nongchun ).put ( Nongye , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY.get (
							Nongchun ).get ( Nongye ).get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.CNTFR
						.get ( Nongchun )
						.put (
								Nongye ,
								( tempVar.CNTFR.get ( Nongchun ).get ( Nongye ) + ( ziNv
										.get ( Nongchun ).get ( Nongye ).get (
												DDB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Nongye )
												.get ( NMDFB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Nongye )
												.get ( NFDMB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Nongye )
												.get ( NNB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Nongye )
												.get ( SingleRls ) [ X ] + ziNv
										.get ( Nongchun ).get ( Nongye ).get (
												ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY
												.get ( Nongchun ).get ( Nongye )
												.get ( Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCXNY.get ( Nongchun ).get (
						Feinong ).get ( Year.getYear ( year - 1 ) ).get (
						Female ) [ X ];
			}
		}
		tempVar.CNTFR.get ( Nongchun ).put ( Feinong , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY.get (
							Nongchun ).get ( Feinong ).get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.CNTFR
						.get ( Nongchun )
						.put (
								Feinong ,
								( tempVar.CNTFR.get ( Nongchun ).get ( Feinong ) + ( ziNv
										.get ( Nongchun ).get ( Feinong ).get (
												DDB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Feinong )
												.get ( NMDFB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Feinong )
												.get ( NFDMB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Feinong )
												.get ( NNB ) [ X ]
										+ ziNv.get ( Nongchun ).get ( Feinong )
												.get ( SingleRls ) [ X ] + ziNv
										.get ( Nongchun ).get ( Feinong ).get (
												ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY
												.get ( Nongchun )
												.get ( Feinong ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCXNY.get ( Chengshi ).get (
						Nongye ).get ( Year.getYear ( year - 1 ) )
						.get ( Female ) [ X ];
			}
		}
		tempVar.CNTFR.get ( Chengshi ).put ( Nongye , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY.get (
							Chengshi ).get ( Nongye ).get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.CNTFR
						.get ( Chengshi )
						.put (
								Nongye ,
								( tempVar.CNTFR.get ( Chengshi ).get ( Nongye ) + ( ziNv
										.get ( Chengshi ).get ( Nongye ).get (
												DDB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Nongye )
												.get ( NMDFB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Nongye )
												.get ( NFDMB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Nongye )
												.get ( NNB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Nongye )
												.get ( SingleRls ) [ X ] + ziNv
										.get ( Chengshi ).get ( Nongye ).get (
												ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY
												.get ( Chengshi ).get ( Nongye )
												.get ( Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCXNY.get ( Chengshi ).get (
						Feinong ).get ( Year.getYear ( year - 1 ) ).get (
						Female ) [ X ];
			}
		}
		tempVar.CNTFR.get ( Chengshi ).put ( Feinong , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY.get (
							Chengshi ).get ( Feinong ).get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.CNTFR
						.get ( Chengshi )
						.put (
								Feinong ,
								( tempVar.CNTFR.get ( Chengshi ).get ( Feinong ) + ( ziNv
										.get ( Chengshi ).get ( Feinong ).get (
												DDB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Feinong )
												.get ( NMDFB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Feinong )
												.get ( NFDMB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Feinong )
												.get ( NNB ) [ X ]
										+ ziNv.get ( Chengshi ).get ( Feinong )
												.get ( SingleRls ) [ X ] + ziNv
										.get ( Chengshi ).get ( Feinong ).get (
												ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCXNY
												.get ( Chengshi )
												.get ( Feinong ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfNY.get ( Nongye ).get (
						Year.getYear ( year - 1 ) ).get ( Female ) [ X ];
			}
		}
		tempVar.nyPolicyTFR0.put ( Nongye , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
							Nongye ).get ( Year.getYear ( year ) )
							.get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.nyPolicyTFR0.put ( Nongye , ( tempVar.nyPolicyTFR0
						.get ( Nongye ) + ( ziNvOfNY.get ( Nongye ).get (
						PlyBorn ) [ X ] )
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
								Nongye ).get ( Year.getYear ( year ) ).get (
								Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.nyImplTFR0.put ( Nongye , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
							Nongye ).get ( Year.getYear ( year ) )
							.get ( Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.nyImplTFR0
						.put (
								Nongye ,
								( tempVar.nyImplTFR0.get ( Nongye ) + ( ziNvOfNY
										.get ( Nongye ).get ( DDB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NMDFB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NFDMB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NNB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get (
												SingleRls ) [ X ] + ziNvOfNY
										.get ( Nongye ).get ( ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY
												.get ( Nongye ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.nyShiHunTFR0.put ( Nongye , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( ziNvOfNY.get ( Nongye ).get ( coupleNum ) [ X ] != 0.0
					&& ( ziNvOfNY.get ( Nongye ).get ( DDB ) [ X ]
							+ ziNvOfNY.get ( Nongye ).get ( NMDFB ) [ X ]
							+ ziNvOfNY.get ( Nongye ).get ( NFDMB ) [ X ]
							+ ziNvOfNY.get ( Nongye ).get ( NNB ) [ X ]
							+ ziNvOfNY.get ( Nongye ).get ( SingleRls ) [ X ] + ziNvOfNY
							.get ( Nongye ).get ( ShuangFeiRls ) [ X ] ) > 0.0 )
			{
				tempVar.nyShiHunTFR0.put ( Nongye ,( 
										tempVar.nyShiHunTFR0.get ( Nongye ) 
										+ ( ziNvOfNY.get ( Nongye ).get ( DDB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NMDFB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NFDMB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( NNB ) [ X ]
										+ ziNvOfNY.get ( Nongye ).get ( SingleRls ) [ X ] 
										+ ziNvOfNY.get ( Nongye ).get ( ShuangFeiRls ) [ X ] )/ ziNvOfNY.get ( Nongye ).get ( coupleNum ) [ X ] ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfNY.get ( Feinong ).get (
						Year.getYear ( year - 1 ) ).get ( Female ) [ X ];
			}
		}
		tempVar.nyPolicyTFR0.put ( Feinong , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
							Feinong ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.nyPolicyTFR0.put ( Feinong , ( tempVar.nyPolicyTFR0
						.get ( Feinong ) + ( ziNvOfNY.get ( Feinong ).get (
						PlyBorn ) [ X ] )
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
								Feinong ).get ( Year.getYear ( year ) ).get (
								Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.nyImplTFR0.put ( Feinong , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY.get (
							Feinong ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 ) != 0.0 )
			{
				tempVar.nyImplTFR0
						.put (
								Feinong ,
								( tempVar.nyImplTFR0.get ( Feinong ) + ( ziNvOfNY
										.get ( Feinong ).get ( DDB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NMDFB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NFDMB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NNB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get (
												SingleRls ) [ X ] + ziNvOfNY
										.get ( Feinong ).get ( ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfNY
												.get ( Feinong ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.nyShiHunTFR0.put ( Feinong , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( ziNvOfNY.get ( Feinong ).get ( coupleNum ) [ X ] != 0.0
					&& ( ziNvOfNY.get ( Feinong ).get ( DDB ) [ X ]
							+ ziNvOfNY.get ( Feinong ).get ( NMDFB ) [ X ]
							+ ziNvOfNY.get ( Feinong ).get ( NFDMB ) [ X ]
							+ ziNvOfNY.get ( Feinong ).get ( NNB ) [ X ]
							+ ziNvOfNY.get ( Feinong ).get ( SingleRls ) [ X ] + ziNvOfNY
							.get ( Feinong ).get ( ShuangFeiRls ) [ X ] ) > 0.0 )
			{
				tempVar.nyShiHunTFR0
						.put (
								Feinong ,
								( tempVar.nyShiHunTFR0.get ( Feinong ) + ( ziNvOfNY
										.get ( Feinong ).get ( DDB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NMDFB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NFDMB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get ( NNB ) [ X ]
										+ ziNvOfNY.get ( Feinong ).get (
												SingleRls ) [ X ] + ziNvOfNY
										.get ( Feinong ).get ( ShuangFeiRls ) [ X ] )
										/ ( ziNvOfNY.get ( Feinong ).get (
												coupleNum ) [ X ] ) ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCX.get ( Nongchun ).get (
						Year.getYear ( year - 1 ) ).get ( Female ) [ X ];
			}
		}
		tempVar.cxPolicyTFR0.put ( Nongchun , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
							Nongchun ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.cxPolicyTFR0.put ( Nongchun , ( tempVar.cxPolicyTFR0
						.get ( Nongchun ) + ziNvOfCX.get ( Nongchun ).get (
						PlyBorn ) [ X ]
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
								Nongchun ).get ( Year.getYear ( year ) ).get (
								Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.cxImplTFR0.put ( Nongchun , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
							Nongchun ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.cxImplTFR0
						.put (
								Nongchun ,
								( tempVar.cxImplTFR0.get ( Nongchun ) + ( ziNvOfCX
										.get ( Nongchun ).get ( DDB ) [ X ]
										+ ziNvOfCX.get ( Nongchun )
												.get ( NMDFB ) [ X ]
										+ ziNvOfCX.get ( Nongchun )
												.get ( NFDMB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get ( NNB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get (
												SingleRls ) [ X ] + ziNvOfCX
										.get ( Nongchun ).get ( ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCX
												.get ( Nongchun ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.cxShiHunTFR0.put ( Nongchun , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( ziNvOfCX.get ( Nongchun ).get ( coupleNum ) [ X ] != 0.0
					&& BirthPredictOfCX.get ( Nongchun ).get ( Babies.getBabies ( year ) ) [ X ] > 0.0 )
			{
				tempVar.cxShiHunTFR0
						.put (Nongchun ,( tempVar.cxShiHunTFR0.get ( Nongchun ) 
										+ ( ziNvOfCX.get ( Nongchun ).get ( DDB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get ( NMDFB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get ( NFDMB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get ( NNB ) [ X ]
										+ ziNvOfCX.get ( Nongchun ).get ( SingleRls ) [ X ] 
										+ ziNvOfCX.get ( Nongchun ).get ( ShuangFeiRls ) [ X ] )/ ziNvOfCX.get ( Nongchun ).get ( coupleNum ) [ X ] ) );
			}
		}
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfCX.get ( Chengshi ).get (
						Year.getYear ( year - 1 ) ).get ( Female ) [ X ];
			}
		}
		tempVar.cxPolicyTFR0.put ( Chengshi , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
							Chengshi ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.cxPolicyTFR0.put ( Chengshi , ( tempVar.cxPolicyTFR0
						.get ( Chengshi ) + ziNvOfCX.get ( Chengshi ).get (
						PlyBorn ) [ X ]
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
								Chengshi ).get ( Year.getYear ( year ) ).get (
								Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.cxImplTFR0.put ( Chengshi , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfCX.get (
							Chengshi ).get ( Year.getYear ( year ) ).get (
							Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.cxImplTFR0
						.put (
								Chengshi ,
								( tempVar.cxImplTFR0.get ( Chengshi ) + ( ziNvOfCX
										.get ( Chengshi ).get ( DDB ) [ X ]
										+ ziNvOfCX.get ( Chengshi )
												.get ( NMDFB ) [ X ]
										+ ziNvOfCX.get ( Chengshi )
												.get ( NFDMB ) [ X ]
										+ ziNvOfCX.get ( Chengshi ).get ( NNB ) [ X ]
										+ ziNvOfCX.get ( Chengshi ).get (
												SingleRls ) [ X ] + ziNvOfCX
										.get ( Chengshi ).get ( ShuangFeiRls ) [ X ] )
										/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfCX
												.get ( Chengshi ).get (
														Year.getYear ( year ) )
												.get ( Female ) [ X ] ) / 2.0 ) ) );
			}
		}
		
		tempVar.cxShiHunTFR0.put ( Chengshi , 0.0 );
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( ziNv.get ( Chengshi ).get ( Nongye ).get ( coupleNum ) [ X ]
					+ ziNv.get ( Chengshi ).get ( Feinong ).get ( coupleNum ) [ X ] != 0.0
					&& ( BirthPredictOfCX.get ( Chengshi ).get (
							Babies.getBabies ( year ) ) [ X ] ) > 0.0 )
			{
				tempVar.cxShiHunTFR0
						.put (
								Chengshi ,
								( tempVar.cxShiHunTFR0.get ( Chengshi ) + ( ziNvOfCX
										.get ( Chengshi ).get ( DDB ) [ X ]
										+ ziNvOfCX.get ( Chengshi )
												.get ( NMDFB ) [ X ]
										+ ziNvOfCX.get ( Chengshi )
												.get ( NFDMB ) [ X ]
										+ ziNvOfCX.get ( Chengshi ).get ( NNB ) [ X ]
										+ ziNvOfCX.get ( Chengshi ).get (
												SingleRls ) [ X ] + ziNvOfCX
										.get ( Chengshi ).get ( ShuangFeiRls ) [ X ] )
										/ ( ziNv.get ( Chengshi ).get ( Nongye )
												.get ( coupleNum ) [ X ] + ziNv
												.get ( Chengshi )
												.get ( Feinong ).get (
														coupleNum ) [ X ] ) ) );
			}
		}
	
		
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				ARFND1 [ X ] = PopulationPredictOfAll.get (
						Year.getYear ( year - 1 ) ).get ( Female ) [ X ];
			}
			
		}
		tempVar.dqPolicyTFR0 = 0;
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.dqPolicyTFR0 += policyBabies.get ( Babies
						.getBabies ( year ) ) [ X ]
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
								Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 );
			}
		}
		
		tempVar.dqImplTFR0 = 0;
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 != 0.0 )
			{
				tempVar.dqImplTFR0 += ( ziNvOfAll.get ( DDB ) [ X ]
						+ ziNvOfAll.get ( NMDFB ) [ X ]
						+ ziNvOfAll.get ( NFDMB ) [ X ]
						+ ziNvOfAll.get ( NNB ) [ X ]
						+ ziNvOfAll.get ( SingleRls ) [ X ] + ziNvOfAll
						.get ( ShuangFeiRls ) [ X ] )
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
								Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 );
			}
		}
		
		tempVar.dqShiHunTFR0 = 0;
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( ( ziNvOfAll.get ( coupleNum ) [ X ] ) != 0.0
					&& BirthPredictOfAll.get ( Babies.getBabies ( year ) ) [ X ] > 0.0 )
			{
				tempVar.dqShiHunTFR0 += ( ziNvOfAll.get ( DDB ) [ X ]
						+ ziNvOfAll.get ( NMDFB ) [ X ]
						+ ziNvOfAll.get ( NFDMB ) [ X ]
						+ ziNvOfAll.get ( NNB ) [ X ]
						+ ziNvOfAll.get ( SingleRls ) [ X ] + ziNvOfAll
						.get ( ShuangFeiRls ) [ X ] )
						/ ziNvOfAll.get ( coupleNum ) [ X ];
			}
		}
		
		tempVar.dqOverBrithTFR = 0;
		//TODO 地区超生
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( X > 1.0
					&& ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
							Year.getYear ( year ) ).get ( Female ) [ X ] ) > 0.0 )
			{
				tempVar.dqOverBrithTFR += OverBirthPredictOfAll.get ( Babies
						.getBabies ( year ) ) [ X ]
						/ ( ( ARFND1 [ X - 1 ] + PopulationPredictOfAll.get (
								Year.getYear ( year ) ).get ( Female ) [ X ] ) / 2.0 );
			}
		}
		
		tempVar.CRK = 0;
		tempVar.XRK = 0;
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				tempVar.CRK += PopulationPredictOfCX.get ( Chengshi ).get (
						Year.getYear ( year ) ).get ( Male ) [ X ]
						+ PopulationPredictOfCX.get ( Chengshi ).get (
								Year.getYear ( year ) ).get ( Female ) [ X ];
				tempVar.XRK += PopulationPredictOfCX.get ( Nongchun ).get (
						Year.getYear ( year ) ).get ( Male ) [ X ]
						+ PopulationPredictOfCX.get ( Nongchun ).get (
								Year.getYear ( year ) ).get ( Female ) [ X ];
			}
		}
		
		tempVar.CSHLevel = tempVar.CRK / ( tempVar.CRK + tempVar.XRK ) * 100.0;
		
		tempVar.FNRK = 0;
		tempVar.NYRK = 0;
		for ( X = MAX_AGE - 1;X >= 0;X -- )
		{
			if ( true )
			{
				tempVar.FNRK += PopulationPredictOfNY.get ( Feinong ).get (
						Year.getYear ( year ) ).get ( Male ) [ X ]
						+ PopulationPredictOfNY.get ( Feinong ).get (
								Year.getYear ( year ) ).get ( Female ) [ X ];
				tempVar.NYRK += PopulationPredictOfNY.get ( Nongye ).get (
						Year.getYear ( year ) ).get ( Male ) [ X ]
						+ PopulationPredictOfNY.get ( Nongye ).get (
								Year.getYear ( year ) ).get ( Female ) [ X ];
			}
		}
		/////////////end of translating, by Foxpro2Java Translator///////////////
		
		/*******test***********/
//		System.out.println("tempVar.CNTFR.Nongcun.Nongye: "+tempVar.CNTFR.get(Nongchun).get(Nongye));
//		System.out.println("tempVar.CNTFR.Nongcun.Feinong: "+tempVar.CNTFR.get(Nongchun).get(Feinong));
//		System.out.println("tempVar.CNTFR.Chengshi.Nongye: "+tempVar.CNTFR.get(Chengshi).get(Nongye));
//		System.out.println("tempVar.CNTFR.Chengshi.Feinong: "+tempVar.CNTFR.get(Chengshi).get(Feinong));
//		System.out.println("tempVar.nyPolicyTFR0.Nongye: "+tempVar.nyPolicyTFR0.get(Nongye));
//		System.out.println("tempVar.nyImplTFR0.Nongye: "+tempVar.nyImplTFR0.get(Nongye));
//		System.out.println("tempVar.nyShiHunTFR0.Nongye: "+tempVar.nyShiHunTFR0.get(Nongye));
//		System.out.println("tempVar.nyPolicyTFR0.Feinong: "+tempVar.nyPolicyTFR0.get(Feinong));
//		System.out.println("tempVar.nyImplTFR0.Feinong: "+tempVar.nyImplTFR0.get(Feinong));
//		System.out.println("tempVar.nyShiHunTFR0.Feinong: "+tempVar.nyShiHunTFR0.get(Feinong));
//		System.out.println("tempVar.cxPolicyTFR0.Nongcun: "+tempVar.cxPolicyTFR0.get(Nongchun));
//		System.out.println("tempVar.cxImplTFR0.Nongcun: "+tempVar.cxImplTFR0.get(Nongchun));
//		System.out.println("tempVar.cxShiHunTFR0.Nongcun: "+tempVar.cxShiHunTFR0.get(Nongchun));
//		System.out.println("tempVar.cxPolicyTFR0.Chegnshi: "+tempVar.cxPolicyTFR0.get(Chengshi));
//		System.out.println("tempVar.cxImplTFR0.Chegnshi: "+tempVar.cxImplTFR0.get(Chengshi));
//		System.out.println("tempVar.cxShiHunTFR0.Chegnshi: "+tempVar.cxShiHunTFR0.get(Chengshi));
//		System.out.println("tempVar.dqPolicyTFR0: "+tempVar.dqPolicyTFR0);
//		System.out.println("tempVar.dqImplTFR0: "+tempVar.dqImplTFR0);
//		System.out.println("tempVar.dqShiHunTFR0: "+tempVar.dqShiHunTFR0);
//		System.out.println("tempVar.dqOverBirthTFR: "+tempVar.dqOverBrithTFR);
//		System.out.println("tempVar.CRK: "+tempVar.CRK);
//		System.out.println("tempVar.XRK: "+tempVar.XRK);
//		System.out.println("tempVar.CHSLevel: "+tempVar.CSHLevel);
//		System.out.println("tempVar.FNRK: "+tempVar.FNRK);
//		System.out.println("tempVar.NYRK: "+tempVar.NYRK);
//		System.out.println("ARFND1:");
//		for(X=0;X<MAX_AGE;X++){
//			System.out.println(X+":    "+ARFND1[X]);
//		}
		/****************测试结束****************/

	}

	@Override
	public Message checkDatabase ( IDAO m , HashMap < String , Object > globals )
			throws Exception
	{
		return null;
	}

	@Override
	public void setParam ( String params , int type ) throws MethodException
	{

	}

}
