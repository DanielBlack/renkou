package test;

import java.util.HashMap;
import java.util.Map;
import static prclqz.core.enumLib.HunpeiField.*;
import static prclqz.core.enumLib.NY.*;
import static prclqz.core.enumLib.CX.*;
import net.sf.json.JSONObject;
import static prclqz.core.Const.*;
import prclqz.DAO.IDAO;
import prclqz.DAO.MyDAOImpl;
import prclqz.DAO.Bean.TempVariablesBean;
import prclqz.core.Message;
import prclqz.core.enumLib.CX;
import prclqz.core.enumLib.HunpeiField;
import prclqz.core.enumLib.NY;
import prclqz.methods.IMethod;
import prclqz.methods.MethodException;
import prclqz.parambeans.ParamBean2;
import prclqz.view.IView;
import test.EnumTools;

/**
 * 婚配——女性最大多龄概率法
 * @author prclqz@zju.edu.cn
 *
 */
public class MMarriageOfAgeParrernForTest implements IMethod {

	private ParamBean2 paramBean;
	public static void main(String[] args)throws Exception{
		HashMap<String,Object>globals = new HashMap<String,Object>();
		IDAO m = new MyDAOImpl();
		TempVariablesBean tempVar = new TempVariablesBean(0,0,0,0,0,0,0,0,0,0,0,0);
		int dqx=10;
		int year=2010;
		/***构造tempvar***/
		tempVar.setProvince(dqx);
		tempVar.setYear(year);
		globals.put("tempVarBean", tempVar);
		/***读取表内容***/
		HashMap<String,Object> predictVarMap = new HashMap<String,Object>();
		Map<CX, Map<NY, Map<HunpeiField, double[]>>> ziNv = EnumTools
				.creatCXNYZiNvFromFile(MAX_AGE, tempVar.getYear(), dqx);
		predictVarMap.put("HunpeiOfCXNY" + dqx, ziNv);
		double[][] husbandRate = EnumTools.createHusbandRate(MAX_AGE, year, dqx);
		predictVarMap.put("HusbandRate", husbandRate);
		/***开始运行***/
		globals.put("predictVarMap", predictVarMap);
		MMarriageOfAgeParrernForTest hehe = new MMarriageOfAgeParrernForTest();
		hehe.calculate(m, globals);
	}
	@Override
	public void calculate(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		//get variables from globals
		TempVariablesBean tempVar = (TempVariablesBean) globals.get("tempVarBean");
		IView v = (IView) globals.get("view");
//		HashMap<String, Object> provincMigMap = (HashMap<String, Object>) globals.get("provinceMigMap");
		HashMap<String,Object> predictVarMap = (HashMap<String, Object>) globals.get("predictVarMap");
		//get running status
		int dqx = tempVar.getProvince();
		int year = tempVar.getYear();
		//define local variables
		Map<CX,Map<NY,Map<HunpeiField,double[]>>> ziNv = (Map< CX , Map< NY , Map< HunpeiField , double[] >>>) predictVarMap.get( "HunpeiOfCXNY"+dqx );//(Map<CX, Map<NY, Map<HunpeiField, double[]>>>) provincMigMap.get(""+dqx);
		double[][] husbandRate = (double[][]) predictVarMap.get("HusbandRate");
		double[] DDhusd,NMDFhusd,NFDMhusd,NNhusd;
		int X,Y;
		double HDD,HNMDF,HNFDM,HNN;
		double wanthdDF,wanthdNF,SM;
		
		//第一阶段、农村和城镇各自的农业、非农优先婚配
		for(CX cx : CX.values()){
			for(NY ny : NY.values()){
				Map<HunpeiField, double[]> zn = ziNv.get(cx).get(ny);
				//System.out.println(cx.getChinese()+"    "+ny.getChinese());
				//DONE 测试的时候直接true了,测完了记得改回来
				if(paramBean.getMarriageType()==1){//全员婚配
					for(X = 0; X<MAX_AGE; X++){
						zn.get(DF0)[X] = zn.get(DF)[X]+zn.get(DDBF)[X];
						zn.get(DM0)[X] = zn.get(DM)[X]+zn.get(DDBM)[X];
						zn.get(NF0)[X] = zn.get(NF)[X];
						zn.get(NM0)[X] = zn.get(NM)[X];

						zn.get(HJF)[X] = zn.get(DF0)[X]+zn.get(NF0)[X];
						zn.get(HJM)[X] = zn.get(DM0)[X]+zn.get(NM0)[X];
						zn.get(HJ)[X] = zn.get(HJF)[X]+zn.get(HJM)[X];
						
//						System.out.print(zn.get(DF0)[X]+"    ");
//						System.out.print(zn.get(DM0)[X]+"    ");
//						System.out.print(zn.get(NF0)[X]+"    ");
//						System.out.println(zn.get(NM0)[X]);
					}
				}else{
					Exception exp = new MethodException("没有对应的结婚率法@女性最大多龄概率法");
					throw exp;
				}
				//TODO 修改过
				for(X = 0; X<MAX_AGE; X++){
					if( zn.get(DF)[X] < 0 )
						zn.get(DF0)[X] = 0;
					if( zn.get(DM)[X] < 0 )
						zn.get(DM0)[X] = 0;
					if( zn.get(NF)[X] < 0 )
						zn.get(NF0)[X] = 0;
					if( zn.get(NM)[X] < 0 )
						zn.get(NM0)[X] = 0;
				}
				//偷懒做法……不应该每次循环都生成新的数组
				//DS:已经初始化为0,初学java伤不起= =
				DDhusd   = new double[111];
				NMDFhusd = new double[111];
				NFDMhusd = new double[111];
				NNhusd   = new double[111];
				SM=0;
				
				for(int i=0;i<111;i++){
					SM += zn.get(DM0)[i]+zn.get(NM0)[i];
				}
				
				if(SM != 0 )
				for(X = 15 ; X <= 64 ; X++ ) //妻子年龄循环
				{
					wanthdDF = zn.get(DF0)[X];
					wanthdNF = zn.get(NF0)[X];
					//对应foxpro里的HDFDM  HNFDM HNMDF HNN
					HDD = HNFDM = HNMDF = HNN = 0;
					double max=0,min=1000;
					for(Y=15;Y<64;Y++){
						if(husbandRate[Y][X]!=0){
							if(Y<min){
								min=Y;
							}
							if(Y>max){
								max=Y;
							}
						}
					}
					//TODO 四个变量的值不对
					for(Y = 15 ; Y<111 ; Y++)//丈夫年龄循环
					{
						if(( zn.get(DM0)[Y] + zn.get(NM0)[Y] != 0)&& (zn.get(DM0)[Y]/(zn.get(DM0)[Y] + zn.get(NM0)[Y])>0)
							&&(Y<=max)&&(Y>=min)){
							DDhusd[Y] += wanthdDF * ( zn.get(DM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
							HDD += wanthdDF * ( zn.get(DM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
						}
						if(( zn.get(DM0)[Y] + zn.get(NM0)[Y] != 0)&& (zn.get(DM0)[Y]/(zn.get(DM0)[Y] + zn.get(NM0)[Y])>0)
							&&Y<=max&&Y>=min){
							NMDFhusd[Y] += wanthdDF * ( zn.get(NM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
							HNMDF += wanthdDF * ( zn.get(NM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
						}
						
						if(( zn.get(DM0)[Y] + zn.get(NM0)[Y] != 0)&& (zn.get(DM0)[Y]/(zn.get(DM0)[Y] + zn.get(NM0)[Y])>0)
							&&Y<=max&&Y>=min){
							NFDMhusd[Y] += wanthdNF * ( zn.get(DM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
							HNFDM += wanthdNF * ( zn.get(DM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
						}
						
						if(( zn.get(DM0)[Y] + zn.get(NM0)[Y] != 0)&& (zn.get(DM0)[Y]/(zn.get(DM0)[Y] + zn.get(NM0)[Y])>0)
							&&Y<=max&&Y>=min){
							NNhusd[Y] += wanthdNF * ( zn.get(NM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
							HNN += wanthdNF * ( zn.get(NM0)[Y] / (zn.get(DM0)[Y]+zn.get(NM0)[Y] ) * husbandRate[Y][X]);
						}
					}// Y - 丈夫
					//保存婚配结果
					zn.get(DD_F)[X] = HDD;
					zn.get(NMDF_F)[X] = HNMDF;
					zn.get(NFDM_F)[X] = HNFDM;
					zn.get(NN_F)[X] = HNN;

				}// X -妻子
				//冗余步奏……
				for(X=0;X<111;X++){
					zn.get(DD)[X]  = zn.get(DD_F)[X];
					zn.get(NMDF)[X] = zn.get(NMDF_F)[X];
					zn.get(NFDM)[X] = zn.get(NFDM_F)[X];
					zn.get(NN)[X]   = zn.get(NN_F)[X];
					
					//计算剩男剩女
					zn.get(DF10)[X] = zn.get(DF0)[X] - zn.get(DD)[X] - zn.get(NMDF)[X];
					zn.get(NF10)[X] = zn.get(NF0)[X] - zn.get(NN)[X] - zn.get(NFDM)[X];
					zn.get(DM10)[X] = zn.get(DM0)[X] - DDhusd[X] - NFDMhusd[X];
					zn.get(NM10)[X] = zn.get(NM0)[X] - NNhusd[X] - NMDFhusd[X];
				}
			}//cx
		}//ny
		//EnumTools.outputCXNYZinv(ziNv, year, dqx, "D:\\prclqz\\女性最大多龄概率法");
		//DS:第一阶段测试完毕
		
		// 第二阶段、剩余的，农村中农业和非农婚配，城镇中农业与非农婚配
		for(CX cx:CX.values()){
			
			Map<HunpeiField, double[]> fn = ziNv.get(cx).get(Feinong);
			Map<HunpeiField, double[]> ny = ziNv.get(cx).get(Nongye);
			
			//非农女与农业男婚配
			DDhusd   = new double[111];
			NMDFhusd = new double[111];
			NFDMhusd = new double[111];
			NNhusd   = new double[111];
			for(X = 15; X <=64; X++){ //妻子年龄循环
				wanthdDF = fn.get(DF10)[X];
				wanthdNF = fn.get(NF10)[X];
				HDD = HNFDM = HNMDF = HNN = 0;
				
				SM=0;
				for(int i=0; i<111; i++)
					SM += ny.get(DM10)[X] + ny.get(NM10)[X];
				if(SM > 0)
				for(Y = 15; Y<=64; Y++)//丈夫年龄循环 TODO丈夫年龄应该不止64岁,要不要改成111?
				{
					if(( ny.get(DM10)[Y] + ny.get(NM10)[Y] != 0)&& (ny.get(DM10)[Y]/(ny.get(DM10)[Y] + ny.get(NM10)[Y])>0)){
						DDhusd[Y] += wanthdDF * ( ny.get(DM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
						HDD += wanthdDF * ( ny.get(DM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
					}
					if(( ny.get(DM10)[Y] + ny.get(NM10)[Y] != 0)&& (ny.get(DM10)[Y]/(ny.get(DM10)[Y] + ny.get(NM10)[Y])>0)){
						NMDFhusd[Y] += wanthdDF * ( ny.get(NM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
						HNMDF += wanthdDF * ( ny.get(NM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
					}
					
					if(( ny.get(DM10)[Y] + ny.get(NM10)[Y] != 0)&& (ny.get(DM10)[Y]/(ny.get(DM10)[Y] + ny.get(NM10)[Y])>0)){
						NFDMhusd[Y] += wanthdNF * ( ny.get(DM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
						HNFDM += wanthdNF * ( ny.get(DM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
					}
					
					if(( ny.get(DM10)[Y] + ny.get(NM10)[Y] != 0)&& (ny.get(DM10)[Y]/(ny.get(DM10)[Y] + ny.get(NM10)[Y])>0)){
						NNhusd[Y] += wanthdNF * ( ny.get(NM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
						HNN += wanthdNF * ( ny.get(NM10)[Y] / (ny.get(DM10)[Y]+ny.get(NM10)[Y] ) * husbandRate[Y][X]);
					}
				}// Y - 丈夫
				//保存婚配结果
				fn.get(DD_NOT_F_F)[X] = HDD;
				fn.get(NMDF_NOT_F_F)[X] = HNMDF;
				fn.get(NFDM_NOT_F_F)[X] = HNFDM;
				fn.get(NN_NOT_F_F)[X] = HNN;
			}
			for(X=0;X<111;X++){
				//ls:有错误待解决！！！
				fn.get(DD_NOT_F)[X]   = Math.min(fn.get(DD_NOT_F_F)[X], 0); //DD妻 从来没改过？？ TODO !!!
				fn.get(NMDF_NOT_F)[X] = Math.min(fn.get(NMDF_NOT_F_F)[X], 0);//DD妻 从来没改过？？ TODO !!!
				fn.get(NFDM_NOT_F)[X] = Math.min(fn.get(NFDM_NOT_F_F)[X], 0);//DD妻 从来没改过？？ TODO !!!
				fn.get(NN_NOT_F)[X]   = Math.min(fn.get(NN_NOT_F_F)[X], 0);//DD妻 从来没改过？？ TODO !!!
				
				fn.get(DD)[X]   += fn.get(DD_NOT_F)[X]; 
				fn.get(NMDF)[X] += fn.get(NMDF_NOT_F)[X];
				fn.get(NFDM)[X] += fn.get(NFDM_NOT_F)[X];
				fn.get(NN)[X]   += fn.get(NN_NOT_F)[X];
				
				//计算剩男剩女\各类未婚人数
				fn.get(DF20)[X] = fn.get(DF0)[X] - fn.get(DD)[X] - fn.get(NMDF)[X];
				fn.get(NF20)[X] = fn.get(NF0)[X] - fn.get(NN)[X] - fn.get(NFDM)[X];
				ny.get(DM20)[X] = ny.get(DM10)[X] - DDhusd[X] - NFDMhusd[X];
				ny.get(NM20)[X] = ny.get(NM10)[X] - NNhusd[X] - NMDFhusd[X];
			}
			//农业女与非农男婚配
			DDhusd   = new double[111];//TODO 是不是要改成MAX_AGE?
			NMDFhusd = new double[111];
			NFDMhusd = new double[111];
			NNhusd   = new double[111];
			for(X = 15; X <=64; X++){ //妻子年龄循环
				wanthdDF = ny.get(DF10)[X];
				wanthdNF = ny.get(NF10)[X];
				HDD = HNFDM = HNMDF = HNN = 0;
				
				SM=0;
				for(int i=0; i<111; i++)
					SM += fn.get(DM0)[X] + fn.get(NM0)[X];
				if(SM > 0)
				for(Y = 15; Y<=64; Y++)//丈夫年龄循环
				{
					if(( fn.get(DM10)[Y] + fn.get(NM10)[Y] != 0)&& (fn.get(DM0)[Y]/(fn.get(DM0)[Y] + fn.get(NM0)[Y])>0)){
						DDhusd[Y] += wanthdDF * ( fn.get(DM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
						HDD += wanthdDF * ( fn.get(DM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
					}
					if(( fn.get(DM10)[Y] + fn.get(NM10)[Y] != 0)&& (fn.get(DM0)[Y]/(fn.get(DM0)[Y] + fn.get(NM0)[Y])>0)){
						NMDFhusd[Y] += wanthdDF * ( fn.get(NM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
						HNMDF += wanthdDF * ( fn.get(NM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
					}
					
					if(( fn.get(DM10)[Y] + fn.get(NM10)[Y] != 0)&& (fn.get(DM0)[Y]/(fn.get(DM0)[Y] + fn.get(NM0)[Y])>0)){
						NFDMhusd[Y] += wanthdNF * ( fn.get(DM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
						HNFDM += wanthdNF * ( fn.get(DM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
					}
					
					if(( fn.get(DM10)[Y] + fn.get(NM10)[Y] != 0)&& (fn.get(DM0)[Y]/(fn.get(DM0)[Y] + fn.get(NM0)[Y])>0)){
						NNhusd[Y] += wanthdNF * ( fn.get(NM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
						HNN += wanthdNF * ( fn.get(NM0)[Y] / (fn.get(DM0)[Y]+fn.get(NM0)[Y] ) * husbandRate[Y][X]);
					}
				}// Y - 丈夫
				//保存婚配结果
				ny.get(DD)[X]   += HDD; 	
				ny.get(NMDF)[X] += HNMDF;
				ny.get(NFDM)[X] += HNFDM;
				ny.get(NN)[X]   += HNN;
			}
			for(X=0;X<111;X++){
				//计算剩男剩女\各类未婚人数
				ny.get(DF20)[X] = ny.get(DF0)[X] - ny.get(DD)[X] - ny.get(NMDF)[X];
				ny.get(NF20)[X] = ny.get(NF0)[X] - ny.get(NN)[X] - ny.get(NFDM)[X];
				ny.get(DM20)[X] = ny.get(DM10)[X] - DDhusd[X] - NFDMhusd[X];
				ny.get(NM20)[X] = ny.get(NM10)[X] - NNhusd[X] - NMDFhusd[X];
			}
		}
		//EnumTools.outputCXNYZinv(ziNv, year, dqx, "D:\\prclqz\\女性最大多龄概率法");
		//DS:第二阶段测试完毕
		
		//第三阶段、统计剩余的人数
		//*******对婚配剩余女性处理：独生女计入女单，非独生女计入双非
		System.out.println("NMDF"+"	"+"NN");
		for(CX cx : CX.values()){
			System.out.println(cx.getChinese());
			for(NY ny : NY.values()){
				System.out.println(ny.getChinese());
				for(X = 15; X<=49;X++){
					System.out.println("输入:"+ziNv.get(cx).get(ny).get(NMDF)[X]+"    "+ziNv.get(cx).get(ny).get(NN)[X]);
					ziNv.get(cx).get(ny).get(NMDF)[X] += ziNv.get(cx).get(ny).get(DF20)[X];
					ziNv.get(cx).get(ny).get(NN)[X]   += ziNv.get(cx).get(ny).get(NF20)[X];
					System.out.println(ziNv.get(cx).get(ny).get(NMDF)[X]+"    "+ziNv.get(cx).get(ny).get(NN)[X]);
				}
			}
		}
		//EnumTools.outputCXNYZinv(ziNv, year, dqx, "D:\\prclqz\\女性最大多龄概率法");
		//DS:第三阶段测试完毕
	}//end
	@Override
	public void setParam(String params, int type) throws MethodException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Message checkDatabase(IDAO m, HashMap<String, Object> globals)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
